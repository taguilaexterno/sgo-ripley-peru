package com.ripley.dao;

/**DAO para operaciones de caja
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ICajaDAO {
	
	/**Obtiene transacción para el número de caja
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param fecha
	 * @return
	 */
	Long getTrxCaja(Integer caja, Integer sucursal, String fecha);
	
	/**Actualiza el numero de transacción para el número de caja
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param fecha
	 * @param trx
	 * @return
	 */
	boolean updTrxCaja(Integer caja, Integer sucursal, String fecha, Long trx);

}
