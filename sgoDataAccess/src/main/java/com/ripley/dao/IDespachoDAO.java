package com.ripley.dao;

import com.ripley.dao.dto.DespachoResponse;
import com.ripley.dao.dto.MarcaDespachoResponse;

/**
 * @author Boris Parra Luman [Aligare]
 * @since 25 may. 2018
 */
public interface IDespachoDAO {
	
	DespachoResponse obtenerOcPendienteDespacho() throws Exception;

	MarcaDespachoResponse marcarModeloExtendido(String xmlIn);
}
