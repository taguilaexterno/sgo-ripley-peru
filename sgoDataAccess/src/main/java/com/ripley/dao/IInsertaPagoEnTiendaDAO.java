package com.ripley.dao;

import com.ripley.dao.dto.InsertaPagoEnTiendaDTO;
import com.ripley.dao.dto.InsertaPagoEnTiendaRespDTO;

/**DAO para inserción de Pago en Tienda y actualización de medio de pago en el Modelo Extendido
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface IInsertaPagoEnTiendaDAO {

	
	/**Llamada a procedure de ME para insertar pago en tienda y actualizar medio de pago en ME.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 12-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	InsertaPagoEnTiendaRespDTO insertaPagoEnTienda(InsertaPagoEnTiendaDTO dto) throws Exception;
	
}
