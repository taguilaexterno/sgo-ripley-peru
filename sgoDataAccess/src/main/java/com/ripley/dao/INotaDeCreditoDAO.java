package com.ripley.dao;

import com.ripley.dao.dto.DetalleTrxBoResponse;

/**DAO para manejo de notas de crédito y anulaciones
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface INotaDeCreditoDAO {

	
	DetalleTrxBoResponse getDetalleTrxBo(Long correlativoVenta);
	
}
