package com.ripley.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ripley.dao.dto.DetalleTrxBoDTO;
import com.ripley.dao.dto.DocumentoElectronicoDTO;
import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.dao.dto.TraceRposDTO;

/**DAO para operaciones sobre órdenes de compra.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 27-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface IOrdenCompraDAO {

	/**Obtiene OC por estado y caja. Además, se debe indicar cuántos registros debe devolver.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 27-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param estado
	 * @param caja
	 * @param cantidad
	 * @return
	 * @throws DataAccessException
	 */
	List<Long> getOrdenesByEstadoAndCaja(Integer estado, Integer caja, Integer sucursal, Integer cantidad, Integer mailActive) throws DataAccessException;
	List<Long> getOrdenesByEstadoAndCaja(Integer estado, Integer caja, Integer cantidad, Integer mailActive) throws DataAccessException;
	
	/**Obtiene lista de documentos electrónicos por correlativo de venta.
	 * Lo normal es que tenga solo uno.
	 *
	 * @param correlativoVenta
	 * @return
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	List<DocumentoElectronicoDTO> getDocsElectronicosByCorrelativoVenta(Long correlativoVenta) throws DataAccessException;
	
	/**Obtiene detalle trx BO por correlativo de venta
	 *
	 * @param correlativoVenta
	 * @return
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	List<DetalleTrxBoDTO> getDetalleTrxBoByCorrelativoVenta(Long correlativoVenta) throws DataAccessException;
	
	/**Inserta en tabla cv_json_broker a modo de respaldo y usarlo en contingencias
	 *
	 * @param correlativoVenta
	 * @param tipoLlamada
	 * @param json
	 * @return
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	void insertJsonBk(Long correlativoVenta, String tipoLlamada, String json) throws DataAccessException;
	
	/**Obtiene objeto de traza del proceso de impresión por OC
	 *
	 * @param correlativoVenta
	 * @return
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	List<TraceImpresionDTO> getTraceImpresionByOC(Long correlativoVenta) throws DataAccessException;
	
	/**Actualiza la traza de impresión de boleta de la OC
	 *
	 * @param dto
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	void upsertTraceImpresion(TraceImpresionDTO dto) throws DataAccessException;
	
	/**Consulta el id proveedor utilizando la tabla SGO_Bigt_Local_Bodega
	 *
	 * @param correlativoVenta
	 * @throws DataAccessException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	String getIdProveedor(Long correlativoVenta) throws DataAccessException;

	/**Obtiene objeto de traza del proceso de Kiosko
	 *
	 * @param correlativoVenta
	 * @return
	 * @throws DataAccessException
	 *
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 05-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	List<TraceRposDTO> getTraceRPOS(Long correlativoVenta) throws DataAccessException;
	
	/**Actualiza la traza de Kiosko
	 *
	 * @param dto
	 * @throws DataAccessException
	 *
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 05-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	void upsertTraceRPOS(TraceRposDTO dto) throws DataAccessException;

	void reprocTraceRPOS(TraceRposDTO dto) throws DataAccessException;

	List<OcTraceRposDTO> getOrdenesByEstadoAndCajaRpos(Integer estado, Integer caja, Integer cantidad, Integer sucursal) throws DataAccessException;
	
}
