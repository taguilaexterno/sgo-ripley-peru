package com.ripley.dao;

import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.ripley.dao.dto.DatosReservaResponse;
import com.ripley.dao.dto.EstadoReservaResponse;
import com.ripley.dao.dto.FechaVigenciaResponse;
import com.ripley.dao.dto.TopeProductoResponse;

/**Interface de Reservas (Pago en Tienda)
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 25-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface IReservasDAO {

	/**Obtiene fecha vigencia por numero de reserva (correlativo venta)
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param numeroReserva
	 * @return
	 */
	FechaVigenciaResponse fechaVigencia(String numeroReserva);
	
	/**Obtiene tope productos
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	TopeProductoResponse topeProductos();

	/**Obtiene información de la reserva indicada por el numero de reserva asociado.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param numeroReserva
	 * @return
	 */
	DatosReservaResponse getReservaOC(String numeroReserva);
	
	/**Obtiene datos de la o las reservas asociadas al rut del cliente.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param rutCliente
	 * @return
	 */
	DatosReservaResponse getReservaRut(String rutCliente);

	/**Obtiene el estado de la reserva.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param numeroReserva
	 * @return
	 */
	EstadoReservaResponse getEstadoOC(String numeroReserva);
	
	/**Actualiza el estado de las OC cuya fecha de vigencia está vencida.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 25-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws DataAccessException
	 */
	Map<String, Object> updateReservasVencidas() throws DataAccessException;
	
}
