package com.ripley.dao;

import com.ripley.dao.dto.Parametros;

/**Operaciones para TEST
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 07-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ITestDAO {

	/**Obtiene parámetros para inserciones de OC de TEST
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 07-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	Parametros getParametros();
	
}
