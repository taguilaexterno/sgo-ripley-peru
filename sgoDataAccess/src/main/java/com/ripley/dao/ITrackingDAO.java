package com.ripley.dao;

import java.sql.SQLException;

import com.ripley.dao.dto.ArticuloDTO;
import com.ripley.dao.dto.TrackingLoadResponse;

/**
 * Interface para manejo de tracking
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 * <br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
// Se extrae funcionalidad "tracking" de SGO Rest Services y se define en trackingJob como parte de mejoras al proceso de sincronización de estados de OCs entre BT y ME.
@Deprecated
public interface ITrackingDAO {

    /**
     * Carga datos de tracking en Modelo Extendido
     *
     * @param load
     * @return
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 02-05-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     */
    @Deprecated
    TrackingLoadResponse trackingLoad(String load);

    /**
     * Metodo registrarTracking quedó deprecado como parte de extracción de
     * funcionalidad de "tracking" (Sincronización de estados de OCs entre BT y
     * ME)
     *
     * @param articulo
     * @return
     * @throws SQLException
     * @deprecated
     */
    @Deprecated
    int registrarTracking(ArticuloDTO articulo) throws SQLException;

}
