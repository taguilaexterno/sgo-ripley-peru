package com.ripley.dao;

import com.ripley.dao.dto.GetLastIdDTO;
import com.ripley.dao.dto.GetLastInsertOCTimeDTO;
import com.ripley.dao.dto.InsertaModelExtendDTO;
import com.ripley.dao.dto.UpdLastIdDTO;
import com.ripley.dao.dto.UpdLastInserOCTimeDTO;

/**DAO para inserción de OCs en el Modelo Extendido
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface InsertaModeloExtendidoDAO {

	
	/**Inserta XML con información de OC en modelo extendido
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 26-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param xml
	 * @param correlativoVenta
	 * @return
	 * @throws Exception
	 */
	InsertaModelExtendDTO insertaModeloExtendido(String xml, Long correlativoVenta) throws Exception;
	
	
	/**Obtiene ultimo id de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 16-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws Exception
	 */
	GetLastIdDTO getLastId() throws Exception;
	
	/**Actualiza ultimo id de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 16-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param lastId Último Id de la TV que se rescató
	 * @return
	 * @throws Exception
	 */
	UpdLastIdDTO updLastId(String lastId) throws Exception;
	
	/**Obtiene la fecha y hora de la última ejecución para TV
	 *
	 * @return
	 * @throws Exception
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	GetLastInsertOCTimeDTO getLastInsertOCDateTime() throws Exception;
	
	/**Actualiza la última fecha y hora en que se realizó una inserción de OC en ME.
	 *
	 * @param dateTime
	 * @return
	 * @throws Exception
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	UpdLastInserOCTimeDTO updLastInsertOCDateTime(String dateTime) throws Exception;
	
}
