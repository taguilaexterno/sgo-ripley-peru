package com.ripley.dao;

import com.ripley.dao.dto.OrdenesCSDTO;

public interface OrdenesCSDAO {
	OrdenesCSDTO actualizaEstadoCS(String xml, Long oc) throws Exception;
}
