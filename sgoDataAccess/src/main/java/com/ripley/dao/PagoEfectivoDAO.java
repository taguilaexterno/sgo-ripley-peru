package com.ripley.dao;

import org.joda.time.DateTime;

import com.ripley.dao.dto.InsertaPagoEfectivoDTO;

public interface PagoEfectivoDAO {
	public InsertaPagoEfectivoDTO registraPagoEfectivo (String cip, 
														String moneda, 
														Float monto_venta, 
														String estado_cip, 
														String codigo_evento, 
														String fecha_pago,
														String codigo_transaccion
														) throws Exception; 
}
