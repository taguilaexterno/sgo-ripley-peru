package com.ripley.dao.config;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.ripley.dao.dto.DespachoDTO;
import com.ripley.dao.dto.DetalleTrxBoDTO;
import com.ripley.dao.dto.DocumentoElectronicoDTO;
import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.dao.dto.TraceRposDTO;
import com.ripley.dao.procedure.GenericProcedure;
import com.ripley.dao.util.Constantes;

import oracle.jdbc.OracleTypes;

/**
 * Configuration class para capa DAO de SGO Corp.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 * <br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Configuration
public class ConfigDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @SuppressWarnings("rawtypes")
    @Autowired
    private ResultSetExtractor reservaResultSetExtractor;

    @SuppressWarnings("rawtypes")
    @Autowired
    private RowMapper longRowMapper;

    @Autowired
    private RowMapper<DespachoDTO> despachoRowMapper;

    @Autowired
    private RowMapper<DetalleTrxBoDTO> detalleTrxBoRowMapper;

    @Autowired
    private RowMapper<DocumentoElectronicoDTO> documentoElectronicoDTORowMapper;

    @Autowired
    private RowMapper<TraceImpresionDTO> traceImpresionRowMapper;

    @Autowired
    private RowMapper<TraceRposDTO> traceRposRowMapper;

    @Autowired
    private RowMapper<OcTraceRposDTO> ocEstadoCajaRposRowMapper;

    @Bean("insertaModeloExtendido")
    public StoredProcedure insertaModeloExtendido() {

        GenericProcedure proc = new GenericProcedure(null, null, Constantes.PROCEDURE_INSERTA_ME, jdbcTemplate);
        proc.setParameters(new SqlParameter(Constantes.ORDER_ID_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.ORDER_XML_PARAM, Types.VARCHAR),
                new SqlParameter(Constantes.ORDER_ESTADO_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.ORDER_MSG_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.ORDER_EST_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.ORDER_MSG_OUT_PARAM, Types.VARCHAR));

        return proc;

    }

    @Bean
    public StoredProcedure getParam() {

        GenericProcedure proc = new GenericProcedure(null, Constantes.PKG_SGO_PARAMETROS, Constantes.PROCEDURE_GET_PARAM, jdbcTemplate);
        proc.setParameters(new SqlParameter(Constantes.PARAM_NAME_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.PARAM_VALUE_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR));

        return proc;

    }

    @Bean
    public StoredProcedure updParam() {

        GenericProcedure proc = new GenericProcedure(null, Constantes.PKG_SGO_PARAMETROS, Constantes.PROCEDURE_UPD_PARAM, jdbcTemplate);
        proc.setParameters(new SqlParameter(Constantes.PARAM_NAME_PARAM, Types.VARCHAR),
                new SqlParameter(Constantes.PARAM_VALUE_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR));

        return proc;

    }

    @Bean("insertaPagoEnTiendaSP")
    public StoredProcedure insertaPagoEnTiendaSP() {

        GenericProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_PAGO_TIENDA,
                Constantes.PROCEDURE_INSERTA_PAGO_TIENDA, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.INSERT_PAGO_TIENDA_STRUC_PARAM, Types.STRUCT, Constantes.INSERT_PAGO_TIENDA_STRUC_TYPE_NAME),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;

    }

    @Bean("updateReservasVencidasSP")
    public StoredProcedure updateReservasVencidasSP() {

        GenericProcedure proc = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_ACTUALIZA_ESTADO_VIGENCIA, jdbcTemplate);

        proc.setParameters(
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;

    }

    @Bean("obtenerFechaVigencia")
    public StoredProcedure obtenerFechaVigencia() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_GET_FECHA_VIG, jdbcTemplate);
        sp.declareParameter(new SqlParameter(Constantes.NUM_RESERVA, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.FEC_VIGENCIA, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));

        return sp;
    }

    @Bean("obtenerTopeProductos")
    public StoredProcedure obtenerTopeProductos() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_GET_TOPE_PRODUCTOS, jdbcTemplate);
        sp.declareParameter(new SqlOutParameter(Constantes.TOPE_PRODUCTOS, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));

        return sp;
    }

    @Bean("obtenerReservaOC")
    public StoredProcedure obtenerReservaOC() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_GET_RESERVA_OC, jdbcTemplate);
        sp.declareParameter(new SqlParameter(Constantes.NUM_RESERVA, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.CONSULTA_RES, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.RESERVA, OracleTypes.CURSOR, reservaResultSetExtractor));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));

        return sp;
    }

    @Bean("obtenerReservaRut")
    public StoredProcedure obtenerReservaRut() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_GET_RESERVA_RUT, jdbcTemplate);
        sp.declareParameter(new SqlParameter(Constantes.RUT_CLIENTE, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.CONSULTA_RES, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.RESERVA, OracleTypes.CURSOR, reservaResultSetExtractor));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));

        return sp;
    }

    @Bean("obtenerEstadoOC")
    public StoredProcedure obtenerEstadoOC() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_SGO_RESERVAS,
                Constantes.PROCEDURE_PROC_GET_ESTADO, jdbcTemplate);
        sp.declareParameter(new SqlParameter(Constantes.NUM_RESERVA, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.ESTADO_OUT, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));

        return sp;
    }

    @Bean("getOcByEstadoAndCajaSP")
    public StoredProcedure getOcByEstadoAndCajaSP() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROCEDURE_PROC_GET_OC_BY_CAJA_ESTADO, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.ESTADO_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.CANTIDAD_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.MAIL_ACTIVE_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, longRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean("getOcByEstadoCajaSucUrlDoceSP")
    public StoredProcedure getOcByEstadoCajaSucUrlDoceSP() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROCEDURE_PROC_GET_OC_URL_DOCE, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.SUCURSAL_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.ESTADO_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.CANTIDAD_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.MAIL_ACTIVE_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, longRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getTrxCajaSP() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_CAJA,
                Constantes.PROCEDURE_OBT_TRX_CAJA, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.SUCURSAL_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.FECHA_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.TRX_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure updTrxCajaSP() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_CAJA,
                Constantes.PROCEDURE_UPD_TRX_CAJA, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.SUCURSAL_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.FECHA_PARAM, Types.VARCHAR),
                new SqlParameter(Constantes.TRX_IN_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure buscarParametros() {

        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_TEST, Constantes.GET_INI_INSERT_OC, jdbcTemplate);
        sp.declareParameter(new SqlOutParameter(Constantes.CORRELATIVO_VENTA_OUT, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.ID_DESPACHO_OUT, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.FECHA_ACTUAL_OUT, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.FECHA_DESPACHO_OUT, Types.VARCHAR));

        return sp;
    }
    
    
    /*Se Agrega para PagoEfectivo*/
    @Bean("registraPagoEfectivo")
    public StoredProcedure registraPagoEfectivo() {

        StoredProcedure sp = new GenericProcedure(null, null,
                Constantes.PROCEDURE_REGISTRA_PAGO_EFECTIVO, jdbcTemplate);
        sp.declareParameter(new SqlParameter(Constantes.IN_CIP, Types.VARCHAR));
        sp.declareParameter(new SqlParameter(Constantes.IN_MONEDA, Types.VARCHAR));
        sp.declareParameter(new SqlParameter(Constantes.IN_MONTO_VENTA, Types.NUMERIC));
        sp.declareParameter(new SqlParameter(Constantes.IN_ESTADO_CIP, Types.VARCHAR));
        sp.declareParameter(new SqlParameter(Constantes.IN_CODIGO_EVENTO, Types.VARCHAR));
        sp.declareParameter(new SqlParameter(Constantes.IN_FECHA_PAGO, Types.VARCHAR));
        sp.declareParameter(new SqlParameter(Constantes.IN_CODIGO_TRANSACCION, Types.VARCHAR));
        sp.declareParameter(new SqlOutParameter(Constantes.COD_ERROR, Types.NUMERIC));
        sp.declareParameter(new SqlOutParameter(Constantes.MSG_ERROR, Types.VARCHAR));
        
        return sp;
    }
    
    
    // Se extrae funcionalidad "tracking" de SGO Rest Services y se define en trackingJob como parte de mejoras al proceso de sincronización de estados de OCs entre BT y ME.
    @Deprecated
    @Bean
    public StoredProcedure trackingLoad() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_TV_TRACKING,
                Constantes.PROCEDURE_PRC_TRACKING_LOAD, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.CLOB),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean("obtenerOcPendienteDespachoSP")
    public StoredProcedure obtenerOcPendienteDespachoSP() {
        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_TV_DESPACHO, Constantes.PRC_OCS_PENDIENTES, jdbcTemplate);
        sp.setParameters(
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, despachoRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );
        return sp;
    }

    @Bean("marcarModeloExtendidoSP")
    public StoredProcedure marcarModeloExtendido() {
        StoredProcedure sp = new GenericProcedure(null, Constantes.PKG_TV_DESPACHO, Constantes.PRC_OCS_PROCESAR, jdbcTemplate);
        sp.setParameters(new SqlParameter(Constantes.P_XML, Types.CLOB),
                new SqlOutParameter(Constantes.P_ERROR_ID, Types.NUMERIC),
                new SqlOutParameter(Constantes.P_MENSAJE, Types.VARCHAR));
        return sp;
    }

    @Bean
    public StoredProcedure getDetalleTrxBo() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.CAVIRA_MANEJO_NC,
                Constantes.OBT_TRX_XDETALLE_BO, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, detalleTrxBoRowMapper),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getDocElecByCorrelativoVenta() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_GET_DOC_ELEC_BY_CORR_VEN, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, documentoElectronicoDTORowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getDetalleTrxBoByCorrelativoVenta() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_GET_DET_TRX_BO_BY_OC, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, detalleTrxBoRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure insertJsonBk() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_INSERT_JSON_BK, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlParameter(Constantes.IN_TIPO_LLAMDA, Types.VARCHAR),
                new SqlParameter(Constantes.IN_JSON, Types.CLOB),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getTraceImpresionByOC() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_GET_TRACE_IMPR_BY_OC, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, traceImpresionRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure upsertTraceImpresion() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_UPSERT_TRACE_IMPR, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlParameter(Constantes.IN_ESTADO_PPL, Types.NUMERIC),
                new SqlParameter(Constantes.IN_ESTADO_BO, Types.NUMERIC),
                new SqlParameter(Constantes.IN_ESTADO_BT, Types.NUMERIC),
                new SqlParameter(Constantes.IN_ESTADO_BCV, Types.NUMERIC),
                new SqlParameter(Constantes.IN_ESTADO_MKP, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getTraceRpos() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_GET_TRACE_RPOS, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, traceRposRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR));
        return proc;
    }

    @Bean
    public StoredProcedure upsertTraceRpos() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_UPSERT_TRACE_RPOS, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlParameter(Constantes.I_FECHA_INI, Types.DATE),
                new SqlParameter(Constantes.I_GET_OC_TV, Types.NUMERIC),
                new SqlParameter(Constantes.I_T_GET_OC_TV, Types.VARCHAR),
                new SqlParameter(Constantes.I_INSERTA_OC, Types.NUMERIC),
                new SqlParameter(Constantes.I_T_INSERTA_OC, Types.VARCHAR),
                new SqlParameter(Constantes.I_VALIDA_OC, Types.NUMERIC),
                new SqlParameter(Constantes.I_T_VALIDA_OC, Types.VARCHAR),
                new SqlParameter(Constantes.I_IMPR_OC, Types.NUMERIC),
                new SqlParameter(Constantes.I_T_IMPR_OC, Types.VARCHAR),
                new SqlParameter(Constantes.I_GENERA_JSON, Types.NUMERIC),
                new SqlParameter(Constantes.I_T_GENERA_JSON, Types.VARCHAR),
                new SqlParameter(Constantes.I_FECHA_FIN, Types.DATE),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR));
        return proc;
    }

    @Bean
    public StoredProcedure reprocTraceRPOS() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_REPROC_TRACE_RPOS, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlParameter(Constantes.I_VALIDA_OC, Types.NUMERIC),
                new SqlParameter(Constantes.I_IMPR_OC, Types.NUMERIC),
                new SqlParameter(Constantes.I_GENERA_JSON, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR));
        return proc;
    }

    @Bean("getOcByEstadoAndCajaRposSP")
    public StoredProcedure getOcByEstadoAndCajaRposSP() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROCEDURE_PROC_GET_OC_BY_CAJA_ESTADO_RPOS, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.CAJA_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.SUCURSAL_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.ESTADO_PARAM, Types.NUMERIC),
                new SqlParameter(Constantes.CANTIDAD_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.CURSOR_OUT_PARAM, OracleTypes.CURSOR, ocEstadoCajaRposRowMapper),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.MENSAJE_OUT_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    @Bean
    public StoredProcedure getIdProveedor() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_CAVIRA_MANEJO_BOLETAS,
                Constantes.PROC_GET_ID_PROVEEDOR, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.IN_CORRELATIVO_VENTA, Types.NUMERIC),
                new SqlOutParameter(Constantes.QUERY_OUT_PARAM, Types.VARCHAR),
                new SqlOutParameter(Constantes.OUT_RUT_COMERCIAL_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.OUT_COD_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.OUT_MJE_PARAM, Types.VARCHAR)
        );

        return proc;
    }

    // Se extrae funcionalidad "tracking" de SGO Rest Services y se define en trackingJob como parte de mejoras al proceso de sincronización de estados de OCs entre BT y ME.
    @Deprecated
    @Bean
    public StoredProcedure trackingLoadNuevo() {

        StoredProcedure proc = new GenericProcedure(null, Constantes.PKG_TV_TRACKING,
                Constantes.PROCEDURE_PRC_TRACKING_LOAD_BT, jdbcTemplate);

        proc.setParameters(
                new SqlParameter(Constantes.P_CUD, Types.VARCHAR),
                new SqlParameter(Constantes.P_FECHA_CAMBIO, Types.DATE),
                new SqlParameter(Constantes.P_CODIGO_ESTADO, Types.VARCHAR),
                new SqlParameter(Constantes.P_DSC_ESTADO, Types.VARCHAR),
                new SqlParameter(Constantes.P_ORDEN_COMPRA, Types.VARCHAR),
                new SqlOutParameter(Constantes.COD_OUT_PARAM, Types.NUMERIC),
                new SqlOutParameter(Constantes.P_MENSAJE_OUT, Types.VARCHAR)
        );

        return proc;
    }
    
    @Bean("actualizaEstadoCS")
    public StoredProcedure actualizaEstadoCS() {
        GenericProcedure proc = new GenericProcedure(null, null, Constantes.PROCEDURE_ACTUALIZA_OC_CS, jdbcTemplate);
         
        //StoredProcedure proc = new GenericProcedure(null, null, Constantes.PROCEDURE_ACTUALIZA_OC_CS, jdbcTemplate);
        proc.setParameters(
                new SqlParameter(Constantes.IN_nCORRELATIVO_VENTA, Types.NUMERIC),
                new SqlParameter(Constantes.IN_cMsgXml, Types.CLOB),
                new SqlOutParameter(Constantes.OUT_nCodEstProc, Types.NUMERIC),
                new SqlOutParameter(Constantes.OUT_vMsgProc, Types.VARCHAR));
        
        return proc;
    }
    
}
