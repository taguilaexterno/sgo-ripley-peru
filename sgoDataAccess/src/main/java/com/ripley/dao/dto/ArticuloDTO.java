package com.ripley.dao.dto;

import java.sql.Date;

public class ArticuloDTO {
	private String cud;
	private Date fechaCambio;
	private String codEstado;
	private String descripcionEstado;
	private String ordCompra;
	
	public String getCud() {
		return cud;
	}
	public void setCud(String cud) {
		this.cud = cud;
	}
	public Date getFechaCambio() {
		return fechaCambio;
	}
	public void setFechaCambio(Date fechaCambio) {
		this.fechaCambio = fechaCambio;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}
	public String getOrdCompra() {
		return ordCompra;
	}
	public void setOrdCompra(String ordCompra) {
		this.ordCompra = ordCompra;
	}
	
	

}
