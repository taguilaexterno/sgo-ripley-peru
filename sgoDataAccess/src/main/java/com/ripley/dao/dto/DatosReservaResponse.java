package com.ripley.dao.dto;

import java.util.List;

public class DatosReservaResponse extends ResponseBase {
	
	private List<Reserva> reserva;
	
	public List<Reserva> getReserva() {
		return reserva;
	}
	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DatosReservaResponse [reserva=").append(reserva).append(", cod=").append(cod).append(", msg=")
				.append(msg).append("]");
		return builder.toString();
	}
	

			
}
