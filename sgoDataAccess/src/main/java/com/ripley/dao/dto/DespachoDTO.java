package com.ripley.dao.dto;

public class DespachoDTO {
	
//	 private Long correlativoVenta;
//	 private Integer estado;
//	 private Timestamp fechaCreacion;
//	 private String ejecutivoVta;
//	 private String rutCliente;
//	 private String nombreCliente;
//	 private String apellidoPatCliente;
//	 private String apellidoMatCliente;
//	 private String direccionCliente;
//	 private String telefonoCliente;
//	 private String emailCliente;
//	 private String rutDespacho;
//	 private String nombreDespacho;
//	 private Integer regionDespacho;
//	 private Integer comunaDespacho;
//	 private String direccionDespacho;
//	 private String observacion;
//	 private String tipoPasarela;
//	 private Integer tipo_pago;
//	 private String codDespacho;
//	 private String tipoDespacho;
//	 private Integer items;
//	 private Integer status;
	 
	private String pComercio;
	private String pTipoVta;     
	private String pPgmOrigen;
	private String pPgmVersion;
	private String pCodTrx;
	private String pCocAcc;        
	private String pSucVta;        
	private String pCaja;        
	private String pTipDocto;
	private String pNumDocto;      
	private String pFecVta;      
	private String pRutVendedor;
	private String pEstIni;
	private String pCodRegalo;
	private String pTipCli;     
	private String pIdeCli;        
	private String pNomCli;        
	private String pDirCli;        
	private String pFonCli;        
	private String pEmailCliente;
	private String pFormaPago;  
	private String pMedioPago;     
	private String pTipCliDesp;
	private String pRutDesp;    
	private String pNombreDesp;
	private String pEmail;
	private String pCodRegion;
	private String pCodComuna;
	private String pDireccionDesp;
	private String pCodRefUno; 
	private String pCodRefDos;    
	private String pObservaciones;
	private String pObservaciones2;
	private String pObservaciones3;
	private String pObservaciones4;
	private String pObservaciones5;
	private String pObservaciones6;
	private String pObservaciones7;
	private String pNumOcu;
	private String pDespacho;
	
	public String getpComercio() {
		return pComercio;
	}
	public void setpComercio(String pComercio) {
		this.pComercio = pComercio;
	}
	public String getpTipoVta() {
		return pTipoVta;
	}
	public void setpTipoVta(String pTipoVta) {
		this.pTipoVta = pTipoVta;
	}
	public String getpPgmOrigen() {
		return pPgmOrigen;
	}
	public void setpPgmOrigen(String pPgmOrigen) {
		this.pPgmOrigen = pPgmOrigen;
	}
	public String getpPgmVersion() {
		return pPgmVersion;
	}
	public void setpPgmVersion(String pPgmVersion) {
		this.pPgmVersion = pPgmVersion;
	}
	public String getpCodTrx() {
		return pCodTrx;
	}
	public void setpCodTrx(String pCodTrx) {
		this.pCodTrx = pCodTrx;
	}
	public String getpCocAcc() {
		return pCocAcc;
	}
	public void setpCocAcc(String pCocAcc) {
		this.pCocAcc = pCocAcc;
	}
	public String getpSucVta() {
		return pSucVta;
	}
	public void setpSucVta(String pSucVta) {
		this.pSucVta = pSucVta;
	}
	public String getpCaja() {
		return pCaja;
	}
	public void setpCaja(String pCaja) {
		this.pCaja = pCaja;
	}
	public String getpTipDocto() {
		return pTipDocto;
	}
	public void setpTipDocto(String pTipDocto) {
		this.pTipDocto = pTipDocto;
	}
	public String getpNumDocto() {
		return pNumDocto;
	}
	public void setpNumDocto(String pNumDocto) {
		this.pNumDocto = pNumDocto;
	}
	public String getpFecVta() {
		return pFecVta;
	}
	public void setpFecVta(String pFecVta) {
		this.pFecVta = pFecVta;
	}
	public String getpRutVendedor() {
		return pRutVendedor;
	}
	public void setpRutVendedor(String pRutVendedor) {
		this.pRutVendedor = pRutVendedor;
	}
	public String getpEstIni() {
		return pEstIni;
	}
	public void setpEstIni(String pEstIni) {
		this.pEstIni = pEstIni;
	}
	public String getpCodRegalo() {
		return pCodRegalo;
	}
	public void setpCodRegalo(String pCodRegalo) {
		this.pCodRegalo = pCodRegalo;
	}
	public String getpTipCli() {
		return pTipCli;
	}
	public void setpTipCli(String pTipCli) {
		this.pTipCli = pTipCli;
	}
	public String getpIdeCli() {
		return pIdeCli;
	}
	public void setpIdeCli(String pIdeCli) {
		this.pIdeCli = pIdeCli;
	}
	public String getpNomCli() {
		return pNomCli;
	}
	public void setpNomCli(String pNomCli) {
		this.pNomCli = pNomCli;
	}
	public String getpDirCli() {
		return pDirCli;
	}
	public void setpDirCli(String pDirCli) {
		this.pDirCli = pDirCli;
	}
	public String getpFonCli() {
		return pFonCli;
	}
	public void setpFonCli(String pFonCli) {
		this.pFonCli = pFonCli;
	}
	public String getpEmailCliente() {
		return pEmailCliente;
	}
	public void setpEmailCliente(String pEmailCliente) {
		this.pEmailCliente = pEmailCliente;
	}
	public String getpFormaPago() {
		return pFormaPago;
	}
	public void setpFormaPago(String pFormaPago) {
		this.pFormaPago = pFormaPago;
	}
	public String getpMedioPago() {
		return pMedioPago;
	}
	public void setpMedioPago(String pMedioPago) {
		this.pMedioPago = pMedioPago;
	}
	public String getpTipCliDesp() {
		return pTipCliDesp;
	}
	public void setpTipCliDesp(String pTipCliDesp) {
		this.pTipCliDesp = pTipCliDesp;
	}
	public String getpRutDesp() {
		return pRutDesp;
	}
	public void setpRutDesp(String pRutDesp) {
		this.pRutDesp = pRutDesp;
	}
	public String getpNombreDesp() {
		return pNombreDesp;
	}
	public void setpNombreDesp(String pNombreDesp) {
		this.pNombreDesp = pNombreDesp;
	}
	public String getpEmail() {
		return pEmail;
	}
	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}
	public String getpCodRegion() {
		return pCodRegion;
	}
	public void setpCodRegion(String pCodRegion) {
		this.pCodRegion = pCodRegion;
	}
	public String getpCodComuna() {
		return pCodComuna;
	}
	public void setpCodComuna(String pCodComuna) {
		this.pCodComuna = pCodComuna;
	}
	public String getpDireccionDesp() {
		return pDireccionDesp;
	}
	public void setpDireccionDesp(String pDireccionDesp) {
		this.pDireccionDesp = pDireccionDesp;
	}
	public String getpCodRefUno() {
		return pCodRefUno;
	}
	public void setpCodRefUno(String pCodRefUno) {
		this.pCodRefUno = pCodRefUno;
	}
	public String getpCodRefDos() {
		return pCodRefDos;
	}
	public void setpCodRefDos(String pCodRefDos) {
		this.pCodRefDos = pCodRefDos;
	}
	public String getpObservaciones() {
		return pObservaciones;
	}
	public void setpObservaciones(String pObservaciones) {
		this.pObservaciones = pObservaciones;
	}
	public String getpObservaciones2() {
		return pObservaciones2;
	}
	public void setpObservaciones2(String pObservaciones2) {
		this.pObservaciones2 = pObservaciones2;
	}
	public String getpObservaciones3() {
		return pObservaciones3;
	}
	public void setpObservaciones3(String pObservaciones3) {
		this.pObservaciones3 = pObservaciones3;
	}
	public String getpObservaciones4() {
		return pObservaciones4;
	}
	public void setpObservaciones4(String pObservaciones4) {
		this.pObservaciones4 = pObservaciones4;
	}
	public String getpObservaciones5() {
		return pObservaciones5;
	}
	public void setpObservaciones5(String pObservaciones5) {
		this.pObservaciones5 = pObservaciones5;
	}
	public String getpObservaciones6() {
		return pObservaciones6;
	}
	public void setpObservaciones6(String pObservaciones6) {
		this.pObservaciones6 = pObservaciones6;
	}
	public String getpObservaciones7() {
		return pObservaciones7;
	}
	public void setpObservaciones7(String pObservaciones7) {
		this.pObservaciones7 = pObservaciones7;
	}
	public String getpNumOcu() {
		return pNumOcu;
	}
	public void setpNumOcu(String pNumOcu) {
		this.pNumOcu = pNumOcu;
	}
	public String getpDespacho() {
		return pDespacho;
	}
	public void setpDespacho(String pDespacho) {
		this.pDespacho = pDespacho;
	}
	
	@Override
	public String toString() {
		return "DespachoDTO [pComercio=" + pComercio + ", pTipoVta=" + pTipoVta + ", pPgmOrigen=" + pPgmOrigen
				+ ", pPgmVersion=" + pPgmVersion + ", pCodTrx=" + pCodTrx + ", pCocAcc=" + pCocAcc + ", pSucVta="
				+ pSucVta + ", pCaja=" + pCaja + ", pTipDocto=" + pTipDocto + ", pNumDocto=" + pNumDocto + ", pFecVta="
				+ pFecVta + ", pRutVendedor=" + pRutVendedor + ", pEstIni=" + pEstIni + ", pCodRegalo=" + pCodRegalo
				+ ", pTipCli=" + pTipCli + ", pIdeCli=" + pIdeCli + ", pNomCli=" + pNomCli + ", pDirCli=" + pDirCli
				+ ", pFonCli=" + pFonCli + ", pEmailCliente=" + pEmailCliente + ", pFormaPago=" + pFormaPago
				+ ", pMedioPago=" + pMedioPago + ", pTipCliDesp=" + pTipCliDesp + ", pRutDesp=" + pRutDesp
				+ ", pNombreDesp=" + pNombreDesp + ", pEmail=" + pEmail + ", pCodRegion=" + pCodRegion + ", pCodComuna="
				+ pCodComuna + ", pDireccionDesp=" + pDireccionDesp + ", pCodRefUno=" + pCodRefUno + ", pCodRefDos="
				+ pCodRefDos + ", pObservaciones=" + pObservaciones + ", pObservaciones2=" + pObservaciones2
				+ ", pObservaciones3=" + pObservaciones3 + ", pObservaciones4=" + pObservaciones4 + ", pObservaciones5="
				+ pObservaciones5 + ", pObservaciones6=" + pObservaciones6 + ", pObservaciones7=" + pObservaciones7
				+ ", pNumOcu=" + pNumOcu + ", pDespacho=" + pDespacho + "]";
	}
	
	
	
}
