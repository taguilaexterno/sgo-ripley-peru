package com.ripley.dao.dto;

import java.util.List;

public class DespachoResponse extends ResponseBase {
	
	private List<DespachoDTO> listaDespacho;

	public List<DespachoDTO> getListaDespacho() {
		return listaDespacho;
	}

	public void setListaDespacho(List<DespachoDTO> listaDespacho) {
		this.listaDespacho = listaDespacho;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DespachoResponse [listaDespacho=").append(listaDespacho).append("]");
		return builder.toString();
	}


}
