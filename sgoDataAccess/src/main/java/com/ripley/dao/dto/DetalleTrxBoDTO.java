package com.ripley.dao.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class DetalleTrxBoDTO {

	
	private Long correlativoVenta;
	private Long nroTrx;
	private Date fechaGeneracion;
	private Integer estadoIniOC;
	private String estadoGenOC;
	private String observacionGenOC;
	private String codigoPanCte;
	private String tipoPgoBol;
	private BigDecimal montoPago;
	private Long idAperturaCierre;
	private Long igv;
	private BigDecimal montoNeto;
	private String serieCorr;
	private Long nroBolNC;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Long getNroTrx() {
		return nroTrx;
	}
	public void setNroTrx(Long nroTrx) {
		this.nroTrx = nroTrx;
	}
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public Integer getEstadoIniOC() {
		return estadoIniOC;
	}
	public void setEstadoIniOC(Integer estadoIniOC) {
		this.estadoIniOC = estadoIniOC;
	}
	public String getEstadoGenOC() {
		return estadoGenOC;
	}
	public void setEstadoGenOC(String estadoGenOC) {
		this.estadoGenOC = estadoGenOC;
	}
	public String getObservacionGenOC() {
		return observacionGenOC;
	}
	public void setObservacionGenOC(String observacionGenOC) {
		this.observacionGenOC = observacionGenOC;
	}
	public String getCodigoPanCte() {
		return codigoPanCte;
	}
	public void setCodigoPanCte(String codigoPanCte) {
		this.codigoPanCte = codigoPanCte;
	}
	public String getTipoPgoBol() {
		return tipoPgoBol;
	}
	public void setTipoPgoBol(String tipoPgoBol) {
		this.tipoPgoBol = tipoPgoBol;
	}
	public BigDecimal getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public Long getIgv() {
		return igv;
	}
	public void setIgv(Long igv) {
		this.igv = igv;
	}
	public BigDecimal getMontoNeto() {
		return montoNeto;
	}
	public void setMontoNeto(BigDecimal montoNeto) {
		this.montoNeto = montoNeto;
	}
	public String getSerieCorr() {
		return serieCorr;
	}
	public void setSerieCorr(String serieCorr) {
		this.serieCorr = serieCorr;
	}
	public Long getNroBolNC() {
		return nroBolNC;
	}
	public void setNroBolNC(Long nroBolNC) {
		this.nroBolNC = nroBolNC;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"correlativoVenta\":\"").append(correlativoVenta).append("\",\"nroTrx\":\"").append(nroTrx)
				.append("\",\"fechaGeneracion\":\"").append(fechaGeneracion).append("\",\"estadoIniOC\":\"")
				.append(estadoIniOC).append("\",\"estadoGenOC\":\"").append(estadoGenOC)
				.append("\",\"observacionGenOC\":\"").append(observacionGenOC).append("\",\"codigoPanCte\":\"")
				.append(codigoPanCte).append("\",\"tipoPgoBol\":\"").append(tipoPgoBol).append("\",\"montoPago\":\"")
				.append(montoPago).append("\",\"idAperturaCierre\":\"").append(idAperturaCierre).append("\",\"igv\":\"")
				.append(igv).append("\",\"montoNeto\":\"").append(montoNeto).append("\",\"serieCorr\":\"")
				.append(serieCorr).append("\",\"nroBolNC\":\"").append(nroBolNC).append("\"}");
		return builder.toString();
	}
	
	
}
