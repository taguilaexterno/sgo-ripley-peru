package com.ripley.dao.dto;

import java.util.List;

public class DetalleTrxBoResponse extends ResponseBase {

	private List<DetalleTrxBoDTO> detalles;

	public List<DetalleTrxBoDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleTrxBoDTO> detalles) {
		this.detalles = detalles;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"detalles\":\"").append(detalles).append("\",\"cod\":\"").append(cod).append("\",\"msg\":\"")
				.append(msg).append("\"}");
		return builder.toString();
	}
	
	
}
