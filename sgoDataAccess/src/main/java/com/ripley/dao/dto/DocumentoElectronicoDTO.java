package com.ripley.dao.dto;

public class DocumentoElectronicoDTO {

	private Long correlativoVenta;
	private Integer sucursal;
	private Integer nroCaja;
	private String tramaDTE;
	private String xmlDTE;
	private String xmlMail;
	private String pdf417;
	
	public String getPdf417() {
		return pdf417;
	}
	public void setPdf417(String pdf417) {
		this.pdf417 = pdf417;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	public Integer getNroCaja() {
		return nroCaja;
	}
	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}
	public String getTramaDTE() {
		return tramaDTE;
	}
	public void setTramaDTE(String tramaDTE) {
		this.tramaDTE = tramaDTE;
	}
	public String getXmlDTE() {
		return xmlDTE;
	}
	public void setXmlDTE(String xmlDTE) {
		this.xmlDTE = xmlDTE;
	}
	public String getXmlMail() {
		return xmlMail;
	}
	public void setXmlMail(String xmlMail) {
		this.xmlMail = xmlMail;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"correlativoVenta\":\"").append(correlativoVenta).append("\",\"sucursal\":\"")
				.append(sucursal).append("\",\"nroCaja\":\"").append(nroCaja).append("\",\"tramaDTE\":\"")
				.append(tramaDTE).append("\",\"xmlDTE\":\"").append(xmlDTE).append("\",\"xmlMail\":\"").append(xmlMail)
				.append("\",\"pdf417\":\"").append(pdf417).append("\"}");
		return builder.toString();
	}
	
}
