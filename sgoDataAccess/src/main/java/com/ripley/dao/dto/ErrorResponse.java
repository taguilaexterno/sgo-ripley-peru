package com.ripley.dao.dto;

public class ErrorResponse extends ResponseBase {
	
	@Override
	public String toString() {
		return "ErrorResponse [cod=" + cod + ", msg=" + msg + "]";
	}
	

}
