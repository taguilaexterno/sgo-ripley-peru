package com.ripley.dao.dto;

public class EstadoReservaResponse extends ResponseBase {

	private Long	numeroReserva;
	private Integer estado;
	private Boolean anulable;
	private String	descripcion;
	
	public Long getNumeroReserva() {
		return numeroReserva;
	}
	public void setNumeroReserva(Long numeroReserva) {
		this.numeroReserva = numeroReserva;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Boolean getAnulable() {
		return anulable;
	}
	public void setAnulable(Boolean anulable) {
		this.anulable = anulable;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "EstadoReservaResponse [cod=" + cod + ", msg=" + msg
				+ ", numeroReserva=" + numeroReserva + ", estado=" + estado
				+ ", anulable=" + anulable + ", descripcion=" + descripcion
				+ "]";
	}
	
	
	
	
}
