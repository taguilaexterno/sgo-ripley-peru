package com.ripley.dao.dto;

public class FechaVigenciaResponse extends ResponseBase {

	private String	fechaVigencia;
	


	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FechaVigenciaResponse [fechaVigencia=").append(fechaVigencia).append(", cod=").append(cod)
				.append(", msg=").append(msg).append("]");
		return builder.toString();
	}



	
}
