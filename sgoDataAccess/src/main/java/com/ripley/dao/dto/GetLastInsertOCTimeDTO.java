package com.ripley.dao.dto;

public class GetLastInsertOCTimeDTO {

	private Integer codigo;
	private String mensaje;
	private String lastDateTime;
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getLastDateTime() {
		return lastDateTime;
	}
	public void setLastDateTime(String lastDateTime) {
		this.lastDateTime = lastDateTime;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"codigo\":\"").append(codigo).append("\",mensaje\":\"").append(mensaje)
				.append("\",lastDateTime\":\"").append(lastDateTime).append("}");
		return builder.toString();
	}
	
	
	
}
