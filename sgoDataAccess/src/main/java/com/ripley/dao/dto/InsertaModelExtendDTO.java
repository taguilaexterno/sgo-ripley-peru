package com.ripley.dao.dto;

public class InsertaModelExtendDTO {

	private Integer codigo;
	private String mensaje;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InsertaModelExtendDTO [codigo=").append(codigo).append(", mensaje=").append(mensaje)
				.append("]");
		return builder.toString();
	}
	
	
}
