package com.ripley.dao.dto;

import java.math.BigDecimal;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import com.ripley.dao.util.Constantes;

/**DTO con campos que se deben enviar al procedure de inserción de pago en tienda en ME.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 12-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class InsertaPagoEnTiendaDTO implements SQLData {

	private Long correlativoVenta;
	private String fechaHoraTrx;
	private String fechaTrx;
	private Integer sucursal;
	private Integer nroCaja;
	private Long nroTrx;
	private Long nroDocumento;
	private Integer tipoTrx;
	private String recaudadorNombres;
	private String recaudadorDni;
	private String recaudadorCodigo;
	private String glosa;
	private BigDecimal montoDescuentoRipley;
	private Integer plazo;
	private Integer diferido;
	private BigDecimal valorCuota;
	private String fechaPrimerVencto;
	private Long pan;
	private Integer tipoTarjeta;
	private String tipoPasarela;
	private String binNumber;
	private String ultimosDigitos;
	private String idTrxPago;
	private String codigoAutorizacion;
	private Integer tipoPago;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getFechaHoraTrx() {
		return fechaHoraTrx;
	}
	public void setFechaHoraTrx(String fechaHoraTrx) {
		this.fechaHoraTrx = fechaHoraTrx;
	}
	public String getFechaTrx() {
		return fechaTrx;
	}
	public void setFechaTrx(String fechaTrx) {
		this.fechaTrx = fechaTrx;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	public Integer getNroCaja() {
		return nroCaja;
	}
	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}
	public Long getNroTrx() {
		return nroTrx;
	}
	public void setNroTrx(Long nroTrx) {
		this.nroTrx = nroTrx;
	}
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public Integer getTipoTrx() {
		return tipoTrx;
	}
	public void setTipoTrx(Integer tipoTrx) {
		this.tipoTrx = tipoTrx;
	}
	public String getRecaudadorNombres() {
		return recaudadorNombres;
	}
	public void setRecaudadorNombres(String recaudadorNombres) {
		this.recaudadorNombres = recaudadorNombres;
	}
	public String getRecaudadorDni() {
		return recaudadorDni;
	}
	public void setRecaudadorDni(String recaudadorDni) {
		this.recaudadorDni = recaudadorDni;
	}
	public String getRecaudadorCodigo() {
		return recaudadorCodigo;
	}
	public void setRecaudadorCodigo(String recaudadorCodigo) {
		this.recaudadorCodigo = recaudadorCodigo;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public BigDecimal getMontoDescuentoRipley() {
		return montoDescuentoRipley;
	}
	public void setMontoDescuentoRipley(BigDecimal montoDescuentoRipley) {
		this.montoDescuentoRipley = montoDescuentoRipley;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public Integer getDiferido() {
		return diferido;
	}
	public void setDiferido(Integer diferido) {
		this.diferido = diferido;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public String getFechaPrimerVencto() {
		return fechaPrimerVencto;
	}
	public void setFechaPrimerVencto(String fechaPrimerVencto) {
		this.fechaPrimerVencto = fechaPrimerVencto;
	}
	public Long getPan() {
		return pan;
	}
	public void setPan(Long pan) {
		this.pan = pan;
	}
	public Integer getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(Integer tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getTipoPasarela() {
		return tipoPasarela;
	}
	public void setTipoPasarela(String tipoPasarela) {
		this.tipoPasarela = tipoPasarela;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public String getUltimosDigitos() {
		return ultimosDigitos;
	}
	public void setUltimosDigitos(String ultimosDigitos) {
		this.ultimosDigitos = ultimosDigitos;
	}
	public String getIdTrxPago() {
		return idTrxPago;
	}
	public void setIdTrxPago(String idTrxPago) {
		this.idTrxPago = idTrxPago;
	}
	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public Integer getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InsertaPagoEnTiendaDTO [correlativoVenta=").append(correlativoVenta).append(", fechaHoraTrx=")
				.append(fechaHoraTrx).append(", fechaTrx=").append(fechaTrx).append(", sucursal=").append(sucursal)
				.append(", nroCaja=").append(nroCaja).append(", nroTrx=").append(nroTrx).append(", nroDocumento=")
				.append(nroDocumento).append(", tipoTrx=").append(tipoTrx).append(", recaudadorNombres=")
				.append(recaudadorNombres).append(", recaudadorDni=").append(recaudadorDni)
				.append(", recaudadorCodigo=").append(recaudadorCodigo).append(", glosa=").append(glosa)
				.append(", montoDescuentoRipley=").append(montoDescuentoRipley).append(", plazo=").append(plazo)
				.append(", diferido=").append(diferido).append(", valorCuota=").append(valorCuota)
				.append(", fechaPrimerVencto=").append(fechaPrimerVencto).append(", pan=").append(pan)
				.append(", tipoTarjeta=").append(tipoTarjeta).append(", tipoPasarela=").append(tipoPasarela)
				.append(", binNumber=").append(binNumber).append(", ultimosDigitos=").append(ultimosDigitos)
				.append(", idTrxPago=").append(idTrxPago).append(", codigoAutorizacion=").append(codigoAutorizacion)
				.append(", tipoPago=").append(tipoPago).append("]");
		return builder.toString();
	}
	@Override
	public String getSQLTypeName() throws SQLException {
		return Constantes.INSERT_PAGO_TIENDA_STRUC_TYPE_NAME;
	}
	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		
		BigDecimal aux = null;
		aux = stream.readBigDecimal();
		correlativoVenta = aux != null ? aux.longValue() : null;
		fechaHoraTrx = stream.readString();
		fechaTrx = stream.readString();
		aux = stream.readBigDecimal();
		sucursal = aux != null ? aux.intValue() : null;
		aux = stream.readBigDecimal();
		nroCaja = aux != null ? aux.intValue() : null;
		aux = stream.readBigDecimal();
		nroTrx = aux != null ? aux.longValue() : null;
		aux = stream.readBigDecimal();
		nroDocumento = aux != null ? aux.longValue() : null;
		aux = stream.readBigDecimal();
		tipoTrx = aux != null ? aux.intValue() : null;
		recaudadorNombres = stream.readString();
		recaudadorDni = stream.readString();
		recaudadorCodigo = stream.readString();
		glosa = stream.readString();
		montoDescuentoRipley = stream.readBigDecimal();
		aux = stream.readBigDecimal();
		plazo = aux != null ? aux.intValue() : null;
		aux = stream.readBigDecimal();
		diferido = aux != null ? aux.intValue() : null;
		valorCuota = stream.readBigDecimal();
		fechaPrimerVencto = stream.readString();
		aux = stream.readBigDecimal();
		pan = aux != null ? aux.longValue() : null;
		aux = stream.readBigDecimal();
		tipoTarjeta = aux != null ? aux.intValue() : null;
		tipoPasarela = stream.readString();
		binNumber = stream.readString();
		ultimosDigitos = stream.readString();
		idTrxPago = stream.readString();
		codigoAutorizacion = stream.readString();
		aux = stream.readBigDecimal();
		tipoPago = aux != null ? aux.intValue() : null;
		
		
	}
	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
		
		stream.writeBigDecimal(correlativoVenta != null ? new BigDecimal(correlativoVenta) : null);
		stream.writeString(fechaHoraTrx);
		stream.writeString(fechaTrx);
		stream.writeBigDecimal(sucursal != null ? new BigDecimal(sucursal) : null);
		stream.writeBigDecimal(nroCaja != null ? new BigDecimal(nroCaja) : null);
		stream.writeBigDecimal(nroTrx != null ? new BigDecimal(nroTrx) : null);
		stream.writeBigDecimal(nroDocumento != null ? new BigDecimal(nroDocumento) : null);
		stream.writeBigDecimal(tipoTrx != null ? new BigDecimal(tipoTrx) : null);
		stream.writeString(recaudadorNombres);
		stream.writeString(recaudadorDni);
		stream.writeString(recaudadorCodigo);
		stream.writeString(glosa);
		stream.writeBigDecimal(montoDescuentoRipley);
		stream.writeBigDecimal(plazo != null ? new BigDecimal(plazo) : null);
		stream.writeBigDecimal(diferido != null ? new BigDecimal(diferido) : null);
		stream.writeBigDecimal(valorCuota);
		stream.writeString(fechaPrimerVencto);
		stream.writeBigDecimal(pan != null ? new BigDecimal(pan) : null);
		stream.writeBigDecimal(tipoTarjeta != null ? new BigDecimal(tipoTarjeta) : null);
		stream.writeString(tipoPasarela);
		stream.writeString(binNumber);
		stream.writeString(ultimosDigitos);
		stream.writeString(idTrxPago);
		stream.writeString(codigoAutorizacion);
		stream.writeBigDecimal(tipoPago != null ? new BigDecimal(tipoPago) : null);
		
	}
	
	
	
}
