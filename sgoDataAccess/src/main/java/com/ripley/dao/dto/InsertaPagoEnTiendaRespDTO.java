package com.ripley.dao.dto;

/**DTO respuesta de llamada a procedure de inserción de pago en tienda.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 12-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class InsertaPagoEnTiendaRespDTO {

	private Integer codigo;
	private String mensaje;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InsertaModelExtendDTO [codigo=").append(codigo).append(", mensaje=").append(mensaje)
				.append("]");
		return builder.toString();
	}
	
	
}
