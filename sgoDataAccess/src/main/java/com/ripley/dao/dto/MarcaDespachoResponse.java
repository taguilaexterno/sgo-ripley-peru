package com.ripley.dao.dto;

public class MarcaDespachoResponse {

	private Integer codError;
	private String 	msgError;
	public Integer getCodError() {
		return codError;
	}
	public void setCodError(Integer codError) {
		this.codError = codError;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	@Override
	public String toString() {
		return "MarcaDespachoResponse [codError=" + codError + ", msgError=" + msgError + "]";
	}
	
	
}
