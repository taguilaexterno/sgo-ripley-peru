package com.ripley.dao.dto;

public class OcTraceRposDTO {
	private Long oc;
	private Integer ocTV;
	private Integer insertaOC;
	private Integer validaOC;
	private Integer imprimeOC;
	private Integer generaJson;
	private Integer ppl;
	
	public Integer getPpl() {
		return ppl;
	}
	public void setPpl(Integer ppl) {
		this.ppl = ppl;
	}
	public Long getOc() {
		return oc;
	}
	public void setOc(Long oc) {
		this.oc = oc;
	}
	public Integer getOcTV() {
		return ocTV;
	}
	public void setOcTV(Integer ocTV) {
		this.ocTV = ocTV;
	}
	public Integer getInsertaOC() {
		return insertaOC;
	}
	public void setInsertaOC(Integer insertaOC) {
		this.insertaOC = insertaOC;
	}
	public Integer getValidaOC() {
		return validaOC;
	}
	public void setValidaOC(Integer validaOC) {
		this.validaOC = validaOC;
	}
	public Integer getImprimeOC() {
		return imprimeOC;
	}
	public void setImprimeOC(Integer imprimeOC) {
		this.imprimeOC = imprimeOC;
	}
	public Integer getGeneraJson() {
		return generaJson;
	}
	public void setGeneraJson(Integer generaJson) {
		this.generaJson = generaJson;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OcTraceRpos [oc=").append(oc).append(", ocTV=").append(ocTV).append(", insertaOC=")
				.append(insertaOC).append(", validaOC=").append(validaOC).append(", imprimeOC=").append(imprimeOC)
				.append(", generaJson=").append(generaJson).append(", ppl=").append(ppl).append("]");
		return builder.toString();
	}

}
