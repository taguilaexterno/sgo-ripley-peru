package com.ripley.dao.dto;

public class Parametros {

	private Long ordenCompra;
	private Long idDespacho;
	private String fechaActual;
	private String fechaDespacho;
	
	public Long getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(Long ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public Long getIdDespacho() {
		return idDespacho;
	}
	public void setIdDespacho(Long idDespacho) {
		this.idDespacho = idDespacho;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public String getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(String fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	@Override
	public String toString() {
		return "Parametros [ordenCompra=" + ordenCompra + ", idDespacho="
				+ idDespacho + ", fechaActual=" + fechaActual
				+ ", fechaDespacho=" + fechaDespacho + "]";
	}
}
