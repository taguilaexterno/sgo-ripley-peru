package com.ripley.dao.dto;

public class Productos {

	private Long correlativoVenta;
	private String sku;
	private String descripcionProducto;
	private String tipoEntrega;
	private String direccionEntrega;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}
	public String getTipoEntrega() {
		return tipoEntrega;
	}
	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}
	public String getDireccionEntrega() {
		return direccionEntrega;
	}
	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}
	
	@Override
	public String toString() {
		return "Productos [correlativoVenta=" + correlativoVenta + ", sku="
				+ sku + ", descripcionProducto=" + descripcionProducto
				+ ", tipoEntrega=" + tipoEntrega + ", direccionEntrega="
				+ direccionEntrega + "]";
	}
	
	

}
