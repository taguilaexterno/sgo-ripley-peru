package com.ripley.dao.dto;

import java.math.BigDecimal;
import java.util.List;

public class Reserva {

	public String  nombreCliente;
	public String  rutCliente;
	public Long    ordenCompra;
	public String  fechaReserva;
	public String  fechaVigencia;
	public BigDecimal monto;
	public BigDecimal montoTarjetaRipley;
	public Integer codEstado;
	public String  descEstado;
	public Integer cantidadProductos;
	public String  direccionCorreo;
	public List<Productos> productos;
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public Long getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(Long ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public String getFechaReserva() {
		return fechaReserva;
	}
	public void setFechaReserva(String fechaReserva) {
		this.fechaReserva = fechaReserva;
	}
	public String getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public BigDecimal getMontoTarjetaRipley() {
		return montoTarjetaRipley;
	}
	public void setMontoTarjetaRipley(BigDecimal montoTarjetaRipley) {
		this.montoTarjetaRipley = montoTarjetaRipley;
	}
	public Integer getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(Integer codEstado) {
		this.codEstado = codEstado;
	}
	public String getDescEstado() {
		return descEstado;
	}
	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}
	public Integer getCantidadProductos() {
		return cantidadProductos;
	}
	public void setCantidadProductos(Integer cantidadProductos) {
		this.cantidadProductos = cantidadProductos;
	}
	public String getDireccionCorreo() {
		return direccionCorreo;
	}
	public void setDireccionCorreo(String direccionCorreo) {
		this.direccionCorreo = direccionCorreo;
	}
	public List<Productos> getProductos() {
		return productos;
	}
	public void setProductos(List<Productos> productos) {
		this.productos = productos;
	}
	@Override
	public String toString() {
		return "Reserva [nombreCliente=" + nombreCliente + ", rutCliente="
				+ rutCliente + ", ordenCompra=" + ordenCompra
				+ ", fechaReserva=" + fechaReserva + ", fechaVigencia="
				+ fechaVigencia + ", monto=" + monto + ", montoTarjetaRipley="
				+ montoTarjetaRipley + ", codEstado=" + codEstado
				+ ", descEstado=" + descEstado + ", cantidadProductos="
				+ cantidadProductos + ", direccionCorreo=" + direccionCorreo
				+ ", productos=" + productos + "]";
	}
	
	
}
