package com.ripley.dao.dto;

public class ResponseBase {

	protected Integer	cod;
	protected String	msg;
	
	public Integer getCod() {
		return cod;
	}
	public void setCod(Integer cod) {
		this.cod = cod;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseBase [cod=").append(cod).append(", msg=").append(msg).append("]");
		return builder.toString();
	}
	
}
