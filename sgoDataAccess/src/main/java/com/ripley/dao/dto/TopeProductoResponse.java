package com.ripley.dao.dto;

public class TopeProductoResponse extends ResponseBase {
	
	public Integer topeProductos;

	public Integer getTopeProductos() {
		return topeProductos;
	}

	public void setTopeProductos(Integer topeProductos) {
		this.topeProductos = topeProductos;
	}

	@Override
	public String toString() {
		return "TopeProductoResponse [cod=" + cod + ", msg=" + msg
				+ ", topeProductos=" + topeProductos + "]";
	}

	
}
