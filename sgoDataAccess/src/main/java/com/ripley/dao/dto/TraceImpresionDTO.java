package com.ripley.dao.dto;

public class TraceImpresionDTO {

	private Long correlativoVenta;
	private Integer ppl;
	private Integer bo;
	private Integer bt;
	private Integer bcv;
	private Integer mkp;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getPpl() {
		return ppl;
	}
	public void setPpl(Integer ppl) {
		this.ppl = ppl;
	}
	public Integer getBo() {
		return bo;
	}
	public void setBo(Integer bo) {
		this.bo = bo;
	}
	public Integer getBt() {
		return bt;
	}
	public void setBt(Integer bt) {
		this.bt = bt;
	}
	public Integer getBcv() {
		return bcv;
	}
	public void setBcv(Integer bcv) {
		this.bcv = bcv;
	}
	public Integer getMkp() {
		return mkp;
	}
	public void setMkp(Integer mkp) {
		this.mkp = mkp;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"correlativoVenta\":\"").append(correlativoVenta).append("\",\"ppl\":\"").append(ppl)
				.append("\",\"bo\":\"").append(bo).append("\",\"bt\":\"").append(bt).append("\",\"bcv\":\"").append(bcv)
				.append("\",\"mkp\":\"").append(mkp).append("\"}");
		return builder.toString();
	}
	
	
	
}
