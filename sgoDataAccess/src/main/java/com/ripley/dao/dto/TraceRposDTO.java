package com.ripley.dao.dto;

import java.util.Date;

public class TraceRposDTO {

	private Long correlativoVenta;
	private Date fechaInicio;
	private Integer getOcTv;
	private String tiempoGetOcTv;
	private Integer insertaOc;
	private String tiempoInsertaOc;
	private Integer validaOc;
	private String tiempoValidaOc;
	private Integer imprimirOc;
	private String tiempoImprimirOc;
	private Integer generaJson;
	private String tiempoGeneraJson;
	private Date fechaFin;
	
	public Integer getGeneraJson() {
		return generaJson;
	}
	public void setGeneraJson(Integer generaJson) {
		this.generaJson = generaJson;
	}
	public String getTiempoGeneraJson() {
		return tiempoGeneraJson;
	}
	public void setTiempoGeneraJson(String tiempoGeneraJson) {
		this.tiempoGeneraJson = tiempoGeneraJson;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Integer getGetOcTv() {
		return getOcTv;
	}
	public void setGetOcTv(Integer getOcTv) {
		this.getOcTv = getOcTv;
	}
	public String getTiempoGetOcTv() {
		return tiempoGetOcTv;
	}
	public void setTiempoGetOcTv(String tiempoGetOcTv) {
		this.tiempoGetOcTv = tiempoGetOcTv;
	}
	public Integer getInsertaOc() {
		return insertaOc;
	}
	public void setInsertaOc(Integer insertaOc) {
		this.insertaOc = insertaOc;
	}
	public String getTiempoInsertaOc() {
		return tiempoInsertaOc;
	}
	public void setTiempoInsertaOc(String tiempoInsertaOc) {
		this.tiempoInsertaOc = tiempoInsertaOc;
	}
	public Integer getValidaOc() {
		return validaOc;
	}
	public void setValidaOc(Integer validaOc) {
		this.validaOc = validaOc;
	}
	public String getTiempoValidaOc() {
		return tiempoValidaOc;
	}
	public void setTiempoValidaOc(String tiempoValidaOc) {
		this.tiempoValidaOc = tiempoValidaOc;
	}
	public Integer getImprimirOc() {
		return imprimirOc;
	}
	public void setImprimirOc(Integer imprimirOc) {
		this.imprimirOc = imprimirOc;
	}
	public String getTiempoImprimirOc() {
		return tiempoImprimirOc;
	}
	public void setTiempoImprimirOc(String tiempoImprimirOc) {
		this.tiempoImprimirOc = tiempoImprimirOc;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TraceRposDTO [correlativoVenta=");
		builder.append(correlativoVenta);
		builder.append(", fechaInicio=");
		builder.append(fechaInicio);
		builder.append(", getOcTv=");
		builder.append(getOcTv);
		builder.append(", tiempoGetOcTv=");
		builder.append(tiempoGetOcTv);
		builder.append(", insertaOc=");
		builder.append(insertaOc);
		builder.append(", tiempoInsertaOc=");
		builder.append(tiempoInsertaOc);
		builder.append(", validaOc=");
		builder.append(validaOc);
		builder.append(", tiempoValidaOc=");
		builder.append(tiempoValidaOc);
		builder.append(", imprimirOc=");
		builder.append(imprimirOc);
		builder.append(", tiempoImprimirOc=");
		builder.append(tiempoImprimirOc);
		builder.append(", generaJson=");
		builder.append(generaJson);
		builder.append(", tiempoGeneraJson=");
		builder.append(tiempoGeneraJson);
		builder.append(", fechaFin=");
		builder.append(fechaFin);
		builder.append("]");
		return builder.toString();
	}
}
