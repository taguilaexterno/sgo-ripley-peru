package com.ripley.dao.dto;

/**Respuesta Obtención último Id TV
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 16-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class UpdLastIdDTO {

	private Integer codigo;
	private String mensaje;
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdLastIdDTO [codigo=").append(codigo).append(", mensaje=").append(mensaje).append("]");
		return builder.toString();
	}
	
}
