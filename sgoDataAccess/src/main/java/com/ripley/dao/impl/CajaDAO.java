package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.ICajaDAO;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementación de {@linkplain ICajaDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class CajaDAO implements ICajaDAO {

	private static final AriLog LOG = new AriLog(CajaDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private StoredProcedure getTrxCajaSP;
	
	@Autowired
	private StoredProcedure updTrxCajaSP;
	
	@Override
	public Long getTrxCaja(Integer caja, Integer sucursal, String fecha) {
		LOG.initTrace("getTrxCaja", "Integer estado, Integer caja, Integer cantidad", 
				new KeyLog("Caja", String.valueOf(caja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)),
				new KeyLog("Fecha", String.valueOf(fecha))
				);
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.CAJA_PARAM, caja);
		in.put(Constantes.SUCURSAL_PARAM, sucursal);
		in.put(Constantes.FECHA_PARAM, fecha);
		
		Map<String, Object> out = getTrxCajaSP.execute(in);
		
		BigDecimal trx = (BigDecimal) out.get(Constantes.TRX_OUT_PARAM);
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getTrxCaja", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getTrxCaja", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getTrxCaja", "Codigo [" + codigo + "]");
		LOG.traceInfo("getTrxCaja", "Trx [" + trx + "]");
		
		LOG.endTrace("getTrxCaja", "Finalizado", "Trx = " + String.valueOf(trx));
		
		return trx.longValue();
	}
	
	@Override
	public boolean updTrxCaja(Integer caja, Integer sucursal, String fecha, Long trx) {
		LOG.initTrace("updTrxCaja", "Integer estado, Integer caja, Integer cantidad", 
				new KeyLog("Caja", String.valueOf(caja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)),
				new KeyLog("Fecha", String.valueOf(fecha)),
				new KeyLog("Trx", String.valueOf(trx))
				);
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.CAJA_PARAM, caja);
		in.put(Constantes.SUCURSAL_PARAM, sucursal);
		in.put(Constantes.FECHA_PARAM, fecha);
		in.put(Constantes.TRX_IN_PARAM, trx);
		
		Map<String, Object> out = updTrxCajaSP.execute(in);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("updTrxCaja", "Query ejecutada [" + query + "]");
		LOG.traceInfo("updTrxCaja", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("updTrxCaja", "Codigo [" + codigo + "]");
		
		LOG.endTrace("updTrxCaja", "Finalizado", "Trx = " + String.valueOf(trx));
		
		return codigo.intValue() == Constantes.NRO_CERO;
	}

}
