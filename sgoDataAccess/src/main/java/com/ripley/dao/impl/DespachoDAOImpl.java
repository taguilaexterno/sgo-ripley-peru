package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.IDespachoDAO;
import com.ripley.dao.dto.DespachoDTO;
import com.ripley.dao.dto.DespachoResponse;
import com.ripley.dao.dto.MarcaDespachoResponse;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

@Repository
public class DespachoDAOImpl implements IDespachoDAO {

	private static final AriLog LOG = new AriLog(DespachoDAOImpl.class,Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	@Qualifier("obtenerOcPendienteDespachoSP")
	private StoredProcedure obtenerOcPendienteDespachoSP;
	
	@Autowired
	@Qualifier("marcarModeloExtendidoSP")
	private StoredProcedure marcarModeloExtendidoSP;
	
	@SuppressWarnings("unchecked")
	@Override
	public DespachoResponse obtenerOcPendienteDespacho() throws Exception {

		LOG.initTrace("obtenerOcPendienteDespachoSP", "Sin parametros de entrada");
		DespachoResponse listaDespacho = new DespachoResponse();
		listaDespacho.setCod(Constantes.COD_OK);
		listaDespacho.setMsg(Constantes.MSG_OK);
		Map<String, Object> out = null;
		try {
			out = obtenerOcPendienteDespachoSP.execute();	
		} catch (Exception e) {
			listaDespacho.setCod(Constantes.NRO_UNO*-1);
			listaDespacho.setMsg(Constantes.MSG_ERROR +": "+ e.getMessage());
		}		
		
		listaDespacho.setListaDespacho((List<DespachoDTO>) out.get(Constantes.CURSOR_OUT_PARAM));
		
		LOG.endTrace("obtenerOcPendienteDespachoSP", "Finalizado","DespachoResponse = ");
		return listaDespacho;
	}

	@Override
	public MarcaDespachoResponse marcarModeloExtendido(String xmlIn) {

		LOG.initTrace("marcarModeloExtendido", "String: "+xmlIn, new KeyLog("xmlIn", xmlIn));
		MarcaDespachoResponse retorno = new MarcaDespachoResponse();
		Map<String, Object> in = new HashMap<String, Object>();
		in.put(Constantes.P_XML, xmlIn);
		Map<String, Object> out = null;
		
		try {
			out = marcarModeloExtendidoSP.execute(in);
			retorno.setCodError(((BigDecimal) out.get(Constantes.P_ERROR_ID)).intValue());
			retorno.setMsgError((String) out.get(Constantes.P_MENSAJE));
		} catch (Exception e) {
			retorno.setCodError(Constantes.NRO_UNO*-1);
			retorno.setMsgError(Constantes.MSG_ERROR +": "+ e.getMessage());
		}		
		
		LOG.endTrace("marcarModeloExtendido", "Finalizado", retorno.toString());
		return retorno;
	}

}
