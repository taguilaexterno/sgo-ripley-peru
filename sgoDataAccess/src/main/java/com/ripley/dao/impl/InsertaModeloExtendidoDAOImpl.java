package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.InsertaModeloExtendidoDAO;
import com.ripley.dao.dto.GetLastIdDTO;
import com.ripley.dao.dto.GetLastInsertOCTimeDTO;
import com.ripley.dao.dto.InsertaModelExtendDTO;
import com.ripley.dao.dto.UpdLastIdDTO;
import com.ripley.dao.dto.UpdLastInserOCTimeDTO;
import com.ripley.dao.procedure.GenericProcedure;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementación DAO {@linkplain InsertaModeloExtendidoDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class InsertaModeloExtendidoDAOImpl implements InsertaModeloExtendidoDAO {

	private static final AriLog LOG = new AriLog(InsertaModeloExtendidoDAOImpl.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	@Qualifier("insertaModeloExtendido")
	private StoredProcedure insertaModeloExtendido;

	@Autowired
	private StoredProcedure getParam;

	@Autowired
	private StoredProcedure updParam;

	@Override
	public InsertaModelExtendDTO insertaModeloExtendido(String xml, Long correlativoVenta) throws Exception {
		LOG.initTrace("insertaModeloExtendido", "String xml, Long correlativoVenta", 
				new KeyLog("xml", xml),
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));

		InsertaModelExtendDTO dto = new InsertaModelExtendDTO();

		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.ORDER_ID_PARAM, correlativoVenta);
		in.put(Constantes.ORDER_XML_PARAM, xml);
		in.put(Constantes.ORDER_ESTADO_PARAM, Constantes.UNO_LONG);
		in.put(Constantes.ORDER_MSG_PARAM, Constantes.MSG_OK);

		Map<String, Object> out = insertaModeloExtendido.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.ORDER_EST_OUT_PARAM);
		String msg = (String) out.get(Constantes.ORDER_MSG_OUT_PARAM);

		LOG.traceInfo("insertaModeloExtendido", "Procedure " + ((GenericProcedure) insertaModeloExtendido).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)));

		dto.setCodigo(cod.intValue());
		dto.setMensaje(msg);


		LOG.endTrace("insertaModeloExtendido", "Finalizado", "InsertaModeloExtendidoDTO = " + String.valueOf(dto));
		return dto;
	}

	@Override
	public GetLastIdDTO getLastId() throws Exception {
		LOG.initTrace("getLastId", "Inicio");

		GetLastIdDTO dto = new GetLastIdDTO();

		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.PARAM_NAME_PARAM, Constantes.LAST_ID_TV);

		Map<String, Object> out = getParam.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String msg = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);

		LOG.traceInfo("getLastId", "Procedure " + ((GenericProcedure) getParam).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)),
				new KeyLog("Query", String.valueOf(query)));

		dto.setCodigo(cod.intValue());
		dto.setMensaje(msg);

		if(cod.intValue() == Constantes.NRO_CERO) {
			dto.setLastId((String) out.get(Constantes.PARAM_VALUE_PARAM));
		}


		LOG.endTrace("getLastId", "Finalizado", "getLastId = " + String.valueOf(dto));
		return dto;
	}

	@Override
	public UpdLastIdDTO updLastId(String lastId) throws Exception {
		LOG.initTrace("updLastId", "Inicio", new KeyLog("lastId", String.valueOf(lastId)));

		UpdLastIdDTO dto = new UpdLastIdDTO();

		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.PARAM_NAME_PARAM, Constantes.LAST_ID_TV);
		in.put(Constantes.PARAM_VALUE_PARAM, lastId);

		Map<String, Object> out = updParam.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String msg = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);

		LOG.traceInfo("updLastId", "Procedure " + ((GenericProcedure) updParam).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)),
				new KeyLog("Query", String.valueOf(query)));

		dto.setCodigo(cod.intValue());
		dto.setMensaje(msg);

		LOG.endTrace("updLastId", "Finalizado", "getLastId = " + String.valueOf(dto));
		return dto;
	}

	@Override
	public GetLastInsertOCTimeDTO getLastInsertOCDateTime() throws Exception {
		LOG.initTrace("getLastInsertOCDateTime", "Inicio");

		GetLastInsertOCTimeDTO dto = new GetLastInsertOCTimeDTO();

		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.PARAM_NAME_PARAM, Constantes.LAST_DATETIME_TV);

		Map<String, Object> out = getParam.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String msg = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);

		LOG.traceInfo("getLastInsertOCDateTime", "Procedure " + ((GenericProcedure) getParam).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)),
				new KeyLog("Query", String.valueOf(query)));

		dto.setCodigo(cod.intValue());
		dto.setMensaje(msg);

		if(cod.intValue() == Constantes.NRO_CERO) {
			dto.setLastDateTime((String) out.get(Constantes.PARAM_VALUE_PARAM));
		}


		LOG.endTrace("getLastInsertOCDateTime", "Finalizado", "getLastInsertOCDateTime = " + String.valueOf(dto));
		return dto;
	}

	@Override
	public UpdLastInserOCTimeDTO updLastInsertOCDateTime(String dateTime) throws Exception {
		LOG.initTrace("updLastInsertOCDateTime", "Inicio", new KeyLog("dateTime", String.valueOf(dateTime)));

		UpdLastInserOCTimeDTO dto = new UpdLastInserOCTimeDTO();

		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.PARAM_NAME_PARAM, Constantes.LAST_DATETIME_TV);
		in.put(Constantes.PARAM_VALUE_PARAM, dateTime);

		Map<String, Object> out = updParam.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String msg = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);

		LOG.traceInfo("updLastInsertOCDateTime", "Procedure " + ((GenericProcedure) updParam).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)),
				new KeyLog("Query", String.valueOf(query)));

		dto.setCodigo(cod.intValue());
		dto.setMensaje(msg);

		LOG.endTrace("updLastInsertOCDateTime", "Finalizado", "updLastInsertOCDateTime = " + String.valueOf(dto));
		return dto;
	}

}
