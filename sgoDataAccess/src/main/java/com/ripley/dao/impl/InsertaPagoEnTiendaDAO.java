package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.IInsertaPagoEnTiendaDAO;
import com.ripley.dao.dto.InsertaPagoEnTiendaDTO;
import com.ripley.dao.dto.InsertaPagoEnTiendaRespDTO;
import com.ripley.dao.procedure.GenericProcedure;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementación DAO {@linkplain IInsertaPagoEnTiendaDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class InsertaPagoEnTiendaDAO implements IInsertaPagoEnTiendaDAO {
	
	private static final AriLog LOG = new AriLog(InsertaPagoEnTiendaDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	@Qualifier("insertaPagoEnTiendaSP")
	private StoredProcedure insertaPagoEnTiendaSP;
	
	@Override
	public InsertaPagoEnTiendaRespDTO insertaPagoEnTienda(InsertaPagoEnTiendaDTO dto) throws Exception {
		LOG.initTrace("insertaPagoEnTienda", "InsertaPagoEnTiendaDTO dto", 
				new KeyLog("dto", String.valueOf(dto)));
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.INSERT_PAGO_TIENDA_STRUC_PARAM, dto);
		
		Map<String, Object> out = insertaPagoEnTiendaSP.execute(in);
		
		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String msg = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		
		LOG.traceInfo("insertaPagoEnTienda", "Procedure " + ((GenericProcedure) insertaPagoEnTiendaSP).getProcedure() + " ejecutado", 
						new KeyLog("Codigo Salida", String.valueOf(cod)),
						new KeyLog("Mensaje Salida", String.valueOf(msg)));
		
		InsertaPagoEnTiendaRespDTO dtoResp = new InsertaPagoEnTiendaRespDTO();
		dtoResp.setCodigo(cod.intValue());
		dtoResp.setMensaje(msg);
		
		
		LOG.endTrace("insertaPagoEnTienda", "Finalizado", "InsertaPagoEnTiendaRespDTO = " + String.valueOf(dtoResp));
		return dtoResp;
	}

}
