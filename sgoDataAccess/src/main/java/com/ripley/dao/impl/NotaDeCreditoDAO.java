package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.INotaDeCreditoDAO;
import com.ripley.dao.dto.DetalleTrxBoDTO;
import com.ripley.dao.dto.DetalleTrxBoResponse;
import com.ripley.dao.procedure.GenericProcedure;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementacion de {@linkplain INotaDeCreditoDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class NotaDeCreditoDAO implements INotaDeCreditoDAO {

	
	private static final AriLog LOG = new AriLog(NotaDeCreditoDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private StoredProcedure getDetalleTrxBo;
	
	@SuppressWarnings("unchecked")
	@Override
	public DetalleTrxBoResponse getDetalleTrxBo(Long correlativoVenta) {
		LOG.initTrace("getDetalleTrxBo", "Long correlativoVenta", new KeyLog("correlativoVenta", String.valueOf(correlativoVenta)));
		
		
		Map<String, Object> out = getDetalleTrxBo.execute(correlativoVenta);
		
		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		List<DetalleTrxBoDTO> list = (List<DetalleTrxBoDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
		
		LOG.traceInfo("getDetalleTrxBo", "Procedure " + ((GenericProcedure) getDetalleTrxBo).getProcedure() + " ejecutado", 
				new KeyLog("cod", String.valueOf(cod)),
				new KeyLog("mensaje", String.valueOf(mensaje)),
				new KeyLog("query", String.valueOf(query)),
				new KeyLog("DetalleTrxBoDTO List", String.valueOf(list)));
		
		DetalleTrxBoResponse resp = new DetalleTrxBoResponse();
		resp.setCod(cod.intValue());
		resp.setMsg(mensaje);
		resp.setDetalles(list);
		
		LOG.endTrace("getDetalleTrxBo", "Finalizado", "resp = " + resp);
		return resp;
	}

}
