package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.IOrdenCompraDAO;
import com.ripley.dao.dto.DetalleTrxBoDTO;
import com.ripley.dao.dto.DocumentoElectronicoDTO;
import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.dao.dto.TraceRposDTO;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementación de {@linkplain IOrdenCompraDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 27-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class OrdenCompraDAO implements IOrdenCompraDAO {

	private static final AriLog LOG = new AriLog(OrdenCompraDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	@Qualifier("getOcByEstadoAndCajaSP")
	private StoredProcedure getOcByEstadoAndCajaSP;

	@Autowired
	@Qualifier("getOcByEstadoCajaSucUrlDoceSP")
	private StoredProcedure getOcByEstadoCajaSucUrlDoceSP;
	
	@Autowired
	private StoredProcedure getDocElecByCorrelativoVenta;
	
	@Autowired
	private StoredProcedure getDetalleTrxBoByCorrelativoVenta;
	
	@Autowired
	private StoredProcedure insertJsonBk;
	
	@Autowired
	private StoredProcedure getTraceImpresionByOC;
	
	@Autowired
	private StoredProcedure upsertTraceImpresion;
	
	@Autowired
	private StoredProcedure getIdProveedor;

	@Autowired
	private StoredProcedure getTraceRpos;
	
	@Autowired
	private StoredProcedure upsertTraceRpos;
	
	@Autowired
	private StoredProcedure reprocTraceRPOS;
	
	@Autowired
	private StoredProcedure getOcByEstadoAndCajaRposSP;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getOrdenesByEstadoAndCaja(Integer estado, Integer caja, Integer sucursal, Integer cantidad, Integer mailActive)
			throws DataAccessException {
		
		LOG.initTrace("getOrdenesByEstadoAndCaja", "Integer estado, Integer caja, Integer sucursal, Integer cantidad", 
				new KeyLog("Estado", String.valueOf(estado)),
				new KeyLog("Caja", String.valueOf(caja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)),
				new KeyLog("Cantidad Registros", String.valueOf(cantidad))
				);
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.CAJA_PARAM, caja);
		in.put(Constantes.SUCURSAL_PARAM, sucursal);
		in.put(Constantes.ESTADO_PARAM, estado);
		in.put(Constantes.CANTIDAD_PARAM, cantidad);
		in.put(Constantes.MAIL_ACTIVE_PARAM, mailActive);
		
		Map<String, Object> out = getOcByEstadoCajaSucUrlDoceSP.execute(in);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Codigo [" + codigo + "]");
		
		List<Long> ocs = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			ocs = (List<Long>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getOrdenesByEstadoAndCaja", "Finalizado", "List<Long> ocs = " + String.valueOf(ocs));
		
		return ocs;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getOrdenesByEstadoAndCaja(Integer estado, Integer caja, Integer cantidad, Integer mailActive)
			throws DataAccessException {
		
		LOG.initTrace("getOrdenesByEstadoAndCaja", "Integer estado, Integer caja, Integer cantidad", 
				new KeyLog("Estado", String.valueOf(estado)),
				new KeyLog("Caja", String.valueOf(caja)),
				new KeyLog("Cantidad Registros", String.valueOf(cantidad))
				);
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.CAJA_PARAM, caja);
		in.put(Constantes.ESTADO_PARAM, estado);
		in.put(Constantes.CANTIDAD_PARAM, cantidad);
		in.put(Constantes.MAIL_ACTIVE_PARAM, mailActive);
		
		Map<String, Object> out = getOcByEstadoAndCajaSP.execute(in);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Codigo [" + codigo + "]");
		
		List<Long> ocs = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			ocs = (List<Long>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getOrdenesByEstadoAndCaja", "Finalizado", "List<Long> ocs = " + String.valueOf(ocs));
		
		return ocs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoElectronicoDTO> getDocsElectronicosByCorrelativoVenta(Long correlativoVenta)
			throws DataAccessException {
		
		LOG.initTrace("getDocsElectronicosByCorrelativoVenta", "Long correlativoVenta", 
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
		
		Map<String, Object> out = getDocElecByCorrelativoVenta.execute(correlativoVenta);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getDocsElectronicosByCorrelativoVenta", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getDocsElectronicosByCorrelativoVenta", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getDocsElectronicosByCorrelativoVenta", "Codigo [" + codigo + "]");
		
		List<DocumentoElectronicoDTO> docElectronicos = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			docElectronicos = (List<DocumentoElectronicoDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getDocsElectronicosByCorrelativoVenta", "Finalizado", "List<DocumentoElectronicoDTO> docElectronicos = " + String.valueOf(docElectronicos));
		
		return docElectronicos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DetalleTrxBoDTO> getDetalleTrxBoByCorrelativoVenta(Long correlativoVenta) throws DataAccessException {
		LOG.initTrace("getDetalleTrxBoByCorrelativoVenta", "Long correlativoVenta", 
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
		
		Map<String, Object> out = getDetalleTrxBoByCorrelativoVenta.execute(correlativoVenta);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getDetalleTrxBoByCorrelativoVenta", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getDetalleTrxBoByCorrelativoVenta", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getDetalleTrxBoByCorrelativoVenta", "Codigo [" + codigo + "]");
		
		List<DetalleTrxBoDTO> detalleTrxBo = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			detalleTrxBo = (List<DetalleTrxBoDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getDetalleTrxBoByCorrelativoVenta", "Finalizado", "List<DetalleTrxBoDTO> detalleTrxBo = " + String.valueOf(detalleTrxBo));
		
		return detalleTrxBo;
	}

	@Override
	public void insertJsonBk(Long correlativoVenta, String tipoLlamada, String json)
			throws DataAccessException {
		LOG.initTrace("insertJsonBk", "Long correlativoVenta, String tipoLlamada, String json", 
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)),
				new KeyLog("Tipo Llamada", tipoLlamada));//,
				//new KeyLog("JSON", json));
		
		Map<String, Object> out = insertJsonBk.execute(	correlativoVenta,
														tipoLlamada,
														json);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("insertJsonBk", "Query ejecutada [" + query + "]");
		LOG.traceInfo("insertJsonBk", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("insertJsonBk", "Codigo [" + codigo + "]");
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			LOG.traceInfo("insertJsonBk", "Insertado OK");
			
		} else {
			
			LOG.traceInfo("insertJsonBk", "NO Insertado");
			
		}
		
		LOG.endTrace("insertJsonBk", "Finalizado", Constantes.VACIO);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TraceImpresionDTO> getTraceImpresionByOC(Long correlativoVenta) throws DataAccessException {
		LOG.initTrace("getTraceImpresionByOC", "Long correlativoVenta", 
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
		
		Map<String, Object> out = getTraceImpresionByOC.execute(correlativoVenta);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getTraceImpresionByOC", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getTraceImpresionByOC", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getTraceImpresionByOC", "Codigo [" + codigo + "]");
		
		List<TraceImpresionDTO> traceImpresion = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			traceImpresion = (List<TraceImpresionDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getTraceImpresionByOC", "Finalizado", "List<TraceImpresionDTO> traceImpresion = " + String.valueOf(traceImpresion));
		
		return traceImpresion;
	}

	@Override
	public void upsertTraceImpresion(TraceImpresionDTO dto) throws DataAccessException {
		LOG.initTrace("upsertTraceImpresion", "TraceImpresionDTO dto", 
				new KeyLog("TraceImpresionDTO", String.valueOf(dto)));
		
		Map<String, Object> out = upsertTraceImpresion.execute(	dto.getCorrelativoVenta(),
																dto.getPpl(),
																dto.getBo(),
																dto.getBt(),
																dto.getBcv(),
																dto.getMkp());
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("upsertTraceImpresion", "Query ejecutada [" + query + "]");
		LOG.traceInfo("upsertTraceImpresion", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("upsertTraceImpresion", "Codigo [" + codigo + "]");
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			LOG.traceInfo("upsertTraceImpresion", "Insertado OK");
			
		} else {
			
			LOG.traceInfo("upsertTraceImpresion", "NO Insertado");
			
		}
		
		LOG.endTrace("upsertTraceImpresion", "Finalizado", Constantes.VACIO);
		
	}

	@Override
	public String getIdProveedor(Long correlativoVenta) throws DataAccessException {
		LOG.initTrace("getIdProveedor", "getIdProveedor", new KeyLog("correlativoVenta", correlativoVenta+""));
		String codProveedor = Constantes.NRO_CERO + "";
		BigDecimal outCodProveedor;
		Map<String, Object> out = getIdProveedor.execute(correlativoVenta);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.OUT_COD_PARAM);
		String mensaje = (String) out.get(Constantes.OUT_MJE_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		LOG.traceInfo("getIdProveedor", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getIdProveedor", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getIdProveedor", "Codigo [" + codigo + "]");
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			outCodProveedor = (BigDecimal) out.get(Constantes.OUT_RUT_COMERCIAL_PARAM);
			codProveedor = outCodProveedor.toString();
			
			
		} 
		LOG.traceInfo("getIdProveedor", "codProveedor [" + codProveedor + "]");
		LOG.endTrace("upsertTraceImpresion", "Finalizado", Constantes.VACIO);
		return codProveedor;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TraceRposDTO> getTraceRPOS(Long correlativoVenta) throws DataAccessException {
		LOG.initTrace("getTraceRPOS", "Long correlativoVenta", 
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
		
		Map<String, Object> out = getTraceRpos.execute(correlativoVenta);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getTraceRPOS", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getTraceRPOS", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getTraceRPOS", "Codigo [" + codigo + "]");
		
		List<TraceRposDTO> traceRpos = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			traceRpos = (List<TraceRposDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getTraceRPOS", "Finalizado", "List<TraceRposDTO> traceRpos = " + String.valueOf(traceRpos));
		
		return traceRpos;
	}

	@Override
	public void upsertTraceRPOS(TraceRposDTO dto) throws DataAccessException {
		LOG.initTrace("upsertTraceRPOS", "TraceRposDTO dto", 
				new KeyLog("TraceRposDTO", String.valueOf(dto)));
		
		Map<String, Object> out = upsertTraceRpos.execute(	dto.getCorrelativoVenta(),
																dto.getFechaInicio(),
																dto.getGetOcTv(),
																dto.getTiempoGetOcTv(),
																dto.getInsertaOc(),
																dto.getTiempoInsertaOc(),
																dto.getValidaOc(),
																dto.getTiempoValidaOc(),
																dto.getImprimirOc(),
																dto.getTiempoImprimirOc(),
																dto.getGeneraJson(),
																dto.getTiempoGeneraJson(),
																dto.getFechaFin());
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("upsertTraceRPOS", "Query ejecutada [" + query + "]");
		LOG.traceInfo("upsertTraceRPOS", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("upsertTraceRPOS", "Codigo [" + codigo + "]");
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			LOG.traceInfo("upsertTraceRPOS", "Insertado OK");
		} else {
			LOG.traceInfo("upsertTraceRPOS", "NO Insertado");
		}
		LOG.endTrace("upsertTraceRPOS", "Finalizado", Constantes.VACIO);
	}
	
	
	
	@Override
	public void reprocTraceRPOS(TraceRposDTO dto) throws DataAccessException {
		LOG.initTrace("reprocTraceRPOS", "TraceRposDTO dto", 
				new KeyLog("TraceRposDTO", String.valueOf(dto)));
		
		Map<String, Object> out = reprocTraceRPOS.execute(	dto.getCorrelativoVenta(),
																dto.getValidaOc(),
																dto.getImprimirOc(),
																dto.getGeneraJson());
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("reprocTraceRPOS", "Query ejecutada [" + query + "]");
		LOG.traceInfo("reprocTraceRPOS", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("reprocTraceRPOS", "Codigo [" + codigo + "]");
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			LOG.traceInfo("reprocTraceRPOS", "Insertado OK");
		} else {
			LOG.traceInfo("reprocTraceRPOS", "NO Insertado");
		}
		LOG.endTrace("reprocTraceRPOS", "Finalizado", Constantes.VACIO);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OcTraceRposDTO> getOrdenesByEstadoAndCajaRpos(Integer estado, Integer caja, Integer cantidad, Integer sucursal)
			throws DataAccessException {
		
		LOG.initTrace("getOrdenesByEstadoAndCajaRpos", "Integer estado, Integer caja, Integer cantidad", 
				new KeyLog("Estado", String.valueOf(estado)),
				new KeyLog("Caja", String.valueOf(caja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)),
				new KeyLog("Cantidad Registros", String.valueOf(cantidad))
				);
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.CAJA_PARAM, caja);
		in.put(Constantes.SUCURSAL_PARAM, sucursal);
		in.put(Constantes.ESTADO_PARAM, estado);
		in.put(Constantes.CANTIDAD_PARAM, cantidad);
		
		Map<String, Object> out = getOcByEstadoAndCajaRposSP.execute(in);
		
		BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);
		String query = (String) out.get(Constantes.QUERY_OUT_PARAM);
		
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Query ejecutada [" + query + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Mensaje [" + mensaje + "]");
		LOG.traceInfo("getOrdenesByEstadoAndCaja", "Codigo [" + codigo + "]");
		
		List<OcTraceRposDTO> ocs = new ArrayList<>(Constantes.NRO_UNO);
		
		if(codigo.intValue() == Constantes.NRO_CERO) {
			
			ocs = (List<OcTraceRposDTO>) out.get(Constantes.CURSOR_OUT_PARAM);
			
		}
		
		LOG.endTrace("getOrdenesByEstadoAndCajaRpos", "Finalizado", "List<Long> ocs = " + String.valueOf(ocs));
		
		return ocs;
	}

}
