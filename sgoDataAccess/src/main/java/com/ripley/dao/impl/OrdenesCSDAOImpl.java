package com.ripley.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.OrdenesCSDAO;
import com.ripley.dao.dto.OrdenesCSDTO;
import com.ripley.dao.util.Constantes;
import com.ripley.dao.procedure.GenericProcedure;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Repository
public class OrdenesCSDAOImpl implements OrdenesCSDAO {
    private static final AriLog LOG = new AriLog(OrdenesCSDAOImpl.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
//    @Autowired
//    private PoolBD poolBD;
    
    @Autowired
    @Qualifier("actualizaEstadoCS")
    private StoredProcedure actualizaEstadoCS;
    
//    @Autowired
//    @Qualifier("insertaPagoEnTiendaSP")
//    private StoredProcedure insertaPagoEnTiendaSP;

    /*
    @Override
    public OrdenesCSDTO actualizaEstadoCS(String xml, Long oc) throws Exception{
        LOG.initTrace("actualizaEstadoCS", "String xml", 
                        new KeyLog("xml", xml));
      //  LOG.initTrace("actualizaEstadoCS", "Ingreso a metodo; correlativoVenta=" + correlativoVenta);
        Connection conn = null;
	CallableStatement cs = null;        
        OrdenesCSDTO dto = new OrdenesCSDTO();
        //InsertaModelExtendDTO dto = new InsertaModelExtendDTO();
        
        try {
            conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
            //conn = poolBD.getConnection();
            String procSQL = "{CALL CORP_RDSUSERSGO.CSME_ACTUALIZA_ESTADO_OC_CS(?,?,?,?)}";
            cs = conn.prepareCall(procSQL);
            cs.setLong(1, oc);
            cs.setString(2, xml);
            cs.registerOutParameter(3, OracleTypes.NUMBER);
            cs.registerOutParameter(4, OracleTypes.VARCHAR);
            cs.execute();

            BigDecimal cod = cs.getBigDecimal(3);
            String msg = cs.getString(4);
        
            dto.setCodigo(cod.intValue());
            dto.setMensaje(msg);

            //logger.traceInfo("esRechazo", "cod: " + cod);
            //logger.traceInfo("esRechazo", "mensaje: " + mensaje);
        //        LOG.traceInfo("actualizaEstadoCS", "Procedure " + ((GenericProcedure) actualizaEstadoCS).getProcedure() + " ejecutado", 
        //                new KeyLog("Codigo Salida", String.valueOf(cod)),
        //                new KeyLog("Mensaje Salida", String.valueOf(msg)));
        

            if(cod.intValue() != 1) {
                //throw new AligareException(mensaje);
            }

        } catch (Exception e) {
        //  logger.traceError("esRechazo", "Error en esRechazo: ", e);
            LOG.traceError("actualizaEstadoCS", "Error al actualizar Estado CS: ", e);
        //  throw new AligareException(e);
        } finally {
            PoolBDs.closeConnection(conn);
            //PoolBDs.closeConnection(conn, null, cs);
        }
        
        LOG.endTrace("actualizaEstadoCS", "Finalizado", "OrdenesCSDTO = " + String.valueOf(dto));
        return dto;        
    }
    */
    
    private static class Holder {
            public static final OrdenesCSDAOImpl INSTANCE = new OrdenesCSDAOImpl();
    }
    
    public static OrdenesCSDAOImpl getInstance() {
            return OrdenesCSDAOImpl.Holder.INSTANCE;
    }
    
    
    @Override
    public OrdenesCSDTO actualizaEstadoCS(String xml, Long oc) throws Exception{
        LOG.initTrace("actualizaEstadoCS", "String xml", new KeyLog("xml", xml));

        System.out.println("actualizaEstadoCS(SP): "+oc); 
        
        
        OrdenesCSDTO dto = new OrdenesCSDTO();
        //InsertaModelExtendDTO dto = new InsertaModelExtendDTO();
        try {
            Map<String, Object> in = new HashMap<>();
            
            in.put(Constantes.IN_nCORRELATIVO_VENTA, oc);
            in.put(Constantes.IN_cMsgXml, xml);
            //System.out.println("actualizaEstadoCS(SP - 01): "+oc); 
            Map<String, Object> out = actualizaEstadoCS.execute(in);
            BigDecimal cod = (BigDecimal) out.get(Constantes.OUT_nCodEstProc);
            String msg = (String) out.get(Constantes.OUT_vMsgProc);
            
            System.out.println("actualizaEstadoCS(SP - cod): "+cod); 
            System.out.println("actualizaEstadoCS(SP - msg): "+msg); 
            
            LOG.traceInfo("actualizaEstadoCS", "Procedure " + ((GenericProcedure) actualizaEstadoCS).getProcedure() + " ejecutado", 
                            new KeyLog("Código Salida", String.valueOf(cod)),
                            new KeyLog("Mensaje Salida", String.valueOf(msg)));

            dto.setCodigo(cod.intValue());
            dto.setMensaje(msg);

         } catch (Exception e) {
        //  logger.traceError("esRechazo", "Error en esRechazo: ", e);
         //   msg = 
            dto.setCodigo(-Constantes.NRO_UNO);
            dto.setMensaje(e.getMessage());
            LOG.traceError("actualizaEstadoCS", "Error al ejecutar Procedimiento CS: ", e);
        //  throw new AligareException(e);
        }
        
        LOG.endTrace("actualizaEstadoCS", "Finalizado", "OrdenesCSDTO = " + String.valueOf(dto));
        return dto;
    }
}
