package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.PagoEfectivoDAO;
import com.ripley.dao.dto.InsertaPagoEfectivoDTO;
import com.ripley.dao.procedure.GenericProcedure;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

@Repository
public class PagoEfectivoDAOImpl implements PagoEfectivoDAO {
	private static final AriLog LOG = new AriLog(PagoEfectivoDAOImpl.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	@Qualifier("registraPagoEfectivo")
	private StoredProcedure registraPagoEfectivo;
	
	@Override
	public InsertaPagoEfectivoDTO registraPagoEfectivo (String cip, String moneda, Float monto_venta, String estado_cip, String codigo_evento, String fecha_pago, String codigo_transaccion) 	throws Exception {
		LOG.initTrace("registraPagoEfectivo", "cip, moneda, monto_venta, estado_cip, codigo_evento, fecha_pago, codigo_transaccion",
				new KeyLog("cip", String.valueOf(cip)),
				new KeyLog("moneda", String.valueOf(moneda)),
				new KeyLog("monto_venta", String.valueOf(monto_venta)),
				new KeyLog("estado_cip", String.valueOf(estado_cip)),
				new KeyLog("codigo_evento", String.valueOf(codigo_evento))
				//new KeyLog("fecha_pago", String.valueOf(fecha_pago)),
				//new KeyLog("codigo_transaccion", String.valueOf(codigo_transaccion))
				);		
		System.out.print("Nuevos campos de la trama fecha_pago: "+ fecha_pago + " codigo_transaccion: " +codigo_transaccion);
		
		InsertaPagoEfectivoDTO resp = new InsertaPagoEfectivoDTO();
		
		Map<String, Object> in = new HashMap<>();
		in.put(Constantes.IN_CIP, cip);
		in.put(Constantes.IN_MONEDA, moneda);
		in.put(Constantes.IN_MONTO_VENTA, monto_venta);
		in.put(Constantes.IN_ESTADO_CIP, estado_cip);
		in.put(Constantes.IN_CODIGO_EVENTO, codigo_evento);
		in.put(Constantes.IN_FECHA_PAGO, fecha_pago);
		in.put(Constantes.IN_CODIGO_TRANSACCION, codigo_transaccion);

		Map<String, Object> out = registraPagoEfectivo.execute(in);

		BigDecimal cod = (BigDecimal) out.get(Constantes.COD_ERROR);
		String msg = (String) out.get(Constantes.MSG_ERROR);
		
		LOG.traceInfo("registraPagoEfectivo", "Procedure " + ((GenericProcedure) registraPagoEfectivo).getProcedure() + " ejecutado", 
				new KeyLog("Codigo Salida", String.valueOf(cod)),
				new KeyLog("Mensaje Salida", String.valueOf(msg)));
		
		resp.setCodigo(cod.intValue());
		resp.setMensaje(msg);
		LOG.endTrace("registraPagoEfectivo", "Finalizado", "InsertaPagoEfectivoDTO = " + String.valueOf(resp));
		return resp;
	}
}
