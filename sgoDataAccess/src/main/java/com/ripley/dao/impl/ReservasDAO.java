package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.IReservasDAO;
import com.ripley.dao.dto.DatosReservaResponse;
import com.ripley.dao.dto.EstadoReservaResponse;
import com.ripley.dao.dto.FechaVigenciaResponse;
import com.ripley.dao.dto.Reserva;
import com.ripley.dao.dto.TopeProductoResponse;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

@Repository
public class ReservasDAO implements IReservasDAO{

	private static final AriLog logger = new AriLog(ReservasDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

	@Autowired
	@Qualifier("obtenerFechaVigencia")
	private StoredProcedure obtenerFechaVigencia;

	@Autowired
	@Qualifier("obtenerTopeProductos")
	private StoredProcedure obtenerTopeProductos;

	@Autowired
	@Qualifier("obtenerReservaOC")
	private StoredProcedure obtenerReservaOC;

	@Autowired
	@Qualifier("obtenerReservaRut")
	private StoredProcedure obtenerReservaRut;

	@Autowired
	@Qualifier("obtenerEstadoOC")
	private StoredProcedure obtenerEstadoOC;

	@Autowired
	@Qualifier("updateReservasVencidasSP")
	private StoredProcedure updateReservasVencidasSP;

	@Override
	public FechaVigenciaResponse fechaVigencia(String numeroReserva) {

		logger.initTrace("fechaVigencia", "String numeroReserva:"+ numeroReserva);
		FechaVigenciaResponse retorno = new FechaVigenciaResponse();
		Map<String, Object> initParam = new HashMap<String, Object>();
		initParam.put(Constantes.NUM_RESERVA, Long.parseLong(numeroReserva));

		logger.traceInfo("fechaVigencia", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParam = obtenerFechaVigencia.execute(initParam);

		try{
			logger.traceInfo("fechaVigencia", "Llamado a base de datos correcto");
			retorno.setCod(((BigDecimal) outParam.get(Constantes.COD_ERROR)).intValue());
			retorno.setMsg((String) outParam.get(Constantes.MSG_ERROR));
			retorno.setFechaVigencia((String) outParam.get(Constantes.FEC_VIGENCIA));

		}catch(Exception e){
			retorno.setCod(-1);
			retorno.setMsg(Constantes.ERROR_PARSEO);
			retorno.setFechaVigencia(null);
			logger.traceError("fechaVigencia", e);
		}

		logger.endTrace("fechaVigencia", "Finalizado", retorno.toString());
		return retorno;
	}

	@Override
	public TopeProductoResponse topeProductos() {

		logger.initTrace("topeProductos", null);
		TopeProductoResponse retorno = new TopeProductoResponse();

		logger.traceInfo("topeProductos", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParam = obtenerTopeProductos.execute();

		try{
			logger.traceInfo("topeProductos", "Llamado a base de datos correcto");
			retorno.setCod(((BigDecimal) outParam.get(Constantes.COD_ERROR)).intValue());
			retorno.setMsg((String)  outParam.get(Constantes.MSG_ERROR));
			retorno.setTopeProductos(Integer.parseInt((String) outParam.get(Constantes.TOPE_PRODUCTOS)));

		}catch(Exception e){
			retorno.setCod(-1);
			retorno.setMsg(Constantes.ERROR_PARSEO);
			retorno.setTopeProductos(0);
			logger.traceError("topeProductos", e);
		}

		logger.endTrace("topeProductos", "Finalizado", retorno.toString());
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DatosReservaResponse getReservaOC(String numeroReserva) {
		logger.initTrace("getReservaOC", "String numeroReserva:"+ numeroReserva);

		DatosReservaResponse retorno = new DatosReservaResponse();
		Map<String, Object> initParams = new HashMap<String, Object>();
		initParams.put(Constantes.NUM_RESERVA, Long.parseLong(numeroReserva));

		logger.traceInfo("getReservaOC", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParams = obtenerReservaOC.execute(initParams);

		retorno.setCod(((BigDecimal) outParams.get(Constantes.COD_ERROR)).intValue());
		retorno.setMsg((String)  outParams.get(Constantes.MSG_ERROR));
		logger.traceInfo("getReservaOC", "Respuesta procedimiento SQL codigo: ["+retorno.getCod()+ "] Mensaje: ["+retorno.getMsg()+"]");

		if(retorno.getCod()==0){
			retorno.setReserva((List<Reserva>) outParams.get(Constantes.RESERVA));
			logger.traceInfo("getReservaOC", "Largo reservas: ["+retorno.getReserva().size()+"]");

		}

		logger.endTrace("getReservaOC", "Finalizado", retorno.toString());
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DatosReservaResponse getReservaRut(String rutCliente) {

		logger.initTrace("getReservaRut", "String rutCliente:"+rutCliente);

		DatosReservaResponse retorno = new DatosReservaResponse();
		Map<String, Object> initParams = new HashMap<String, Object>(); 
		initParams.put(Constantes.RUT_CLIENTE, Long.parseLong(rutCliente));

		logger.traceInfo("getReservaRut", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParams = obtenerReservaRut.execute(initParams);

		retorno.setCod(((BigDecimal) outParams.get(Constantes.COD_ERROR)).intValue());
		retorno.setMsg((String)  outParams.get(Constantes.MSG_ERROR));
		logger.traceInfo("getReservaRut", "Respuesta procedimiento SQL codigo: ["+retorno.getCod()+ "] Mensaje: ["+retorno.getMsg()+"]");

		if(retorno.getCod()==0){
			retorno.setReserva((List<Reserva>) outParams.get(Constantes.RESERVA));
			logger.traceInfo("getReservaRut", "Largo reservas: ["+retorno.getReserva().size()+"]");
		}

		logger.endTrace("getReservaRut", "Finalizado", retorno.toString());
		return retorno;
	}

	@Override
	public EstadoReservaResponse getEstadoOC(String numeroReserva) {

		logger.initTrace("getEstadoOC", "String numeroReserva:"+numeroReserva);


		EstadoReservaResponse retorno = new EstadoReservaResponse();
		Map<String, Object> initParams = new HashMap<String, Object>(); 
		Long reserva = Long.parseLong(numeroReserva);
		initParams.put(Constantes.NUM_RESERVA, reserva);

		logger.traceInfo("getEstadoOC", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParams = obtenerEstadoOC.execute(initParams);

		retorno.setCod(((BigDecimal) outParams.get(Constantes.COD_ERROR)).intValue());
		retorno.setMsg((String)  outParams.get(Constantes.MSG_ERROR));
		retorno.setNumeroReserva(reserva);
		retorno.setAnulable(Constantes.FALSE);


		if(retorno.getCod()==100){
			retorno.setDescripcion(Constantes.NO_EXISTE);
		}else{
			int estado = ((BigDecimal) outParams.get(Constantes.ESTADO_OUT)).intValue();
			retorno.setEstado(estado);
			if(estado == 2){
				retorno.setDescripcion(Constantes.BOLETA_GENERADA);
			}else{
				retorno.setDescripcion(Constantes.PENDIENTE_BOLETA);
			}
		}
		logger.traceInfo("getEstadoOC", "Respuesta procedimiento SQL codigo: ["+retorno.getCod()+ "] Mensaje: ["+retorno.getMsg()+"]");

		logger.endTrace("getEstadoOC", "Finalizado", retorno.toString());
		return retorno;
	}

	@Override
	public Map<String, Object> updateReservasVencidas() throws DataAccessException {
		logger.initTrace("updateReservasVencidas", "Inicio");


		EstadoReservaResponse retorno = new EstadoReservaResponse();
		Map<String, Object> initParams = new HashMap<String, Object>(1); 

		logger.traceInfo("updateReservasVencidas", "Se realiza llamado a procedimiento que extrae informacion");
		Map<String, Object> outParams = obtenerEstadoOC.execute(initParams);

		logger.traceInfo("updateReservasVencidas", "Respuesta procedimiento SQL codigo: ["+retorno.getCod()+ "] Mensaje: ["+retorno.getMsg()+"]");

		logger.endTrace("updateReservasVencidas", "Finalizado", retorno.toString());
		return outParams;
	}

}
