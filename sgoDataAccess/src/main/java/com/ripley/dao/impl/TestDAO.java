package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.ITestDAO;
import com.ripley.dao.dto.Parametros;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

/**Implementacion de {@linkplain ITestDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 07-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class TestDAO implements ITestDAO {

	private static final AriLog LOG = new AriLog(TestDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);
	
	@Autowired
	private StoredProcedure buscarParametros;
	
	@Override
	public Parametros getParametros() {
		LOG.initTrace("getParametros", "Inicio");
		Parametros parametros = new Parametros();
		
		Map<String, Object> initParam = new HashMap<String, Object>();
		Map<String, Object> outParam = buscarParametros.execute(initParam);
				
		try{
			parametros.setOrdenCompra(((BigDecimal) outParam.get(Constantes.CORRELATIVO_VENTA_OUT)).longValue());
			parametros.setIdDespacho(((BigDecimal) outParam.get(Constantes.ID_DESPACHO_OUT)).longValue());
			parametros.setFechaActual((String) outParam.get(Constantes.FECHA_ACTUAL_OUT));
			parametros.setFechaDespacho((String) outParam.get(Constantes.FECHA_DESPACHO_OUT));
			
		}catch(Exception e){
			LOG.traceError("getParametros", e);
		}
		
		LOG.endTrace("getParametros", "Finalizado", "Parametros = " + String.valueOf(parametros));
		return parametros;
	}

}
