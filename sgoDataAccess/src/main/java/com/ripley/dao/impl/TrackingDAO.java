package com.ripley.dao.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

import com.ripley.dao.ITrackingDAO;
import com.ripley.dao.dto.ArticuloDTO;
import com.ripley.dao.dto.TrackingLoadResponse;
import com.ripley.dao.util.Constantes;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

/**
 * Implementación de {@linkplain ITrackingDAO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 * <br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
// Se extrae funcionalidad "tracking" de SGO Rest Services y se define en trackingJob como parte de mejoras al proceso de sincronización de estados de OCs entre BT y ME.
@Deprecated
@Repository
public class TrackingDAO implements ITrackingDAO {

    private static final AriLog LOG = new AriLog(TrackingDAO.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

    @Autowired
    private StoredProcedure trackingLoad;

    @Autowired
    private StoredProcedure trackingLoadNuevo;

    @Deprecated
    @Override
    public TrackingLoadResponse trackingLoad(String load) {
        LOG.initTrace("trackingLoad", "String load", new KeyLog("Load", String.valueOf(load)));

        Map<String, Object> out = trackingLoad.execute(load);

        BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
        String mensaje = (String) out.get(Constantes.MENSAJE_OUT_PARAM);

        LOG.traceInfo("trackingLoad", "Salida Procedure", new KeyLog("Codigo", String.valueOf(codigo)), new KeyLog("Mensaje", mensaje));

        TrackingLoadResponse resp = new TrackingLoadResponse();
        resp.setCodigo(codigo.intValue());
        resp.setMensaje(mensaje);

        LOG.endTrace("trackingLoad", "Finalizado", "TrackingLoadResponse = " + String.valueOf(resp));
        return resp;
    }

    @Deprecated
    @Override
    public int registrarTracking(ArticuloDTO articulo) throws SQLException {
        LOG.initTrace("trackingLoadNuevo", "TrackingDAO");
        TrackingLoadResponse resp = new TrackingLoadResponse();
        LOG.initTrace("trackingLoadNuevo", "TrackingDAO :" + articulo.getCud() + "|" + articulo.getFechaCambio() + "|" + articulo.getCodEstado() + "|" + articulo.getDescripcionEstado() + "|" + articulo.getOrdCompra());
        Map<String, Object> out = trackingLoadNuevo.execute(articulo.getCud(), articulo.getFechaCambio(), articulo.getCodEstado(), articulo.getDescripcionEstado(), articulo.getOrdCompra());
        BigDecimal codigo = (BigDecimal) out.get(Constantes.COD_OUT_PARAM);
        String mensaje = (String) out.get(Constantes.P_MENSAJE_OUT);
        LOG.traceInfo("trackingLoad", "Salida Procedure", new KeyLog("Codigo", String.valueOf(codigo)), new KeyLog("Mensaje", mensaje));
        resp.setCodigo(codigo.intValue());
        resp.setMensaje(mensaje);
        System.out.println(mensaje);
        LOG.endTrace("trackingLoadNuevo", "Finalizado", "TrackingLoadResponse = " + String.valueOf(resp));

        return resp.getCodigo();
    }

}
