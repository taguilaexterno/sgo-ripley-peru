package com.ripley.dao.procedure;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.Assert;

import com.ripley.dao.util.Constantes;

/**Procedimiento almacenado genérico para instanciar procedimientos almacenados.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class GenericProcedure extends StoredProcedure {
	
	private String procedure;
	
	public GenericProcedure(String schema, String packName, String procedureName, JdbcTemplate jdbcTemplate) {
		Assert.notNull(procedureName, "Nombre de procedimiento no debe ser null.");
		Assert.hasText(procedureName, "Nombre de procedimiento no debe ser vacío.");
		
		String name = schema == null || schema.isEmpty() ? Constantes.VACIO : schema;
		name += packName == null || packName.isEmpty() ? Constantes.VACIO : schema != null && !schema.isEmpty() ? "." + packName : packName;
		name +=  (packName != null && !packName.isEmpty()) || (schema != null && !schema.isEmpty()) ? "." + procedureName : procedureName;
		
		procedure = name;
		
		this.setJdbcTemplate(jdbcTemplate);
		this.setSql(procedure);
		
	}

	public String getProcedure() {
		return procedure;
	}

	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}
	
	
	
}
