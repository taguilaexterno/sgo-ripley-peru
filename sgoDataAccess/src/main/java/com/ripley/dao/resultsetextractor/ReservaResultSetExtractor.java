package com.ripley.dao.resultsetextractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.Productos;
import com.ripley.dao.dto.Reserva;

/**Extrae estructura compleja de reserva de la base de datos
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component("reservaResultSetExtractor")
public class ReservaResultSetExtractor implements ResultSetExtractor<List<Reserva>> {

	@Override
	public List<Reserva> extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		List<Reserva> list = new ArrayList<>();
		
		Map<String, Reserva> mapReservas = new LinkedHashMap<>(); 
		
		while(rs != null && rs.next()) {
			
			Long correlativo = rs.getLong("CORRELATIVO_VENTA");
			
			Reserva r = mapReservas.get(String.valueOf(correlativo));
			
			if(r == null) {
				
				r = new Reserva();
				
				r.setOrdenCompra(correlativo);
				r.setCodEstado(rs.getInt("COD_ESTADO"));
				r.setDescEstado(rs.getString("DESC_ESTADO"));
				r.setRutCliente(rs.getString("RUT_CLIENTE"));
				r.setFechaReserva(rs.getString("FECHA_RESERVA"));
				r.setFechaVigencia(rs.getString("FECHA_VIGENCIA"));
				r.setMonto(rs.getBigDecimal("MONTO_VENTA"));
				r.setMontoTarjetaRipley(rs.getBigDecimal("MONTO_RIPLEY"));
				r.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
				r.setDireccionCorreo(rs.getString("CORREO_DESPACHO"));
				
				mapReservas.put(String.valueOf(correlativo), r);
				
			}
			
			if(r.getProductos() == null) {
				
				r.setProductos(new ArrayList<>());
				
			}
			
			Productos prod = new Productos();
			prod.setCorrelativoVenta(correlativo);
			prod.setDescripcionProducto(rs.getString("DESCRIPCION"));
			prod.setTipoEntrega(rs.getString("TIPO_DESPACHO"));
			prod.setDireccionEntrega(rs.getString("DIRECCION_DESPACHO"));
			prod.setSku(rs.getString("SKU"));
			
			r.getProductos().add(prod);
			
			r.setCantidadProductos(r.getProductos().size());
			
		}
		
		list.addAll(mapReservas.values());
		
		return list;
	}

}
