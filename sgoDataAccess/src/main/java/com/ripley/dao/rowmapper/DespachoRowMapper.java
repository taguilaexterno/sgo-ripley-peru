package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.DespachoDTO;


/**Extrae estructura compleja de reserva de la base de datos
 * @author Boris Parra Luman [Aligare]
 * @since 25 may. 2018
 */
@Component("despachoRowMapper")
public class DespachoRowMapper implements RowMapper <DespachoDTO> {


	@Override
	public DespachoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {	
	
			
			DespachoDTO d = new DespachoDTO();
			d.setpComercio(rs.getString("PCOMERCIO"));
			d.setpTipoVta(rs.getString("PTIPOVTA"));
			d.setpPgmOrigen(rs.getString("PPGMORIGEN"));
			d.setpPgmVersion(rs.getString("PPGMVERSION"));
			d.setpCodTrx(rs.getString("PCODTRX"));
			d.setpCocAcc(rs.getString("PCOCACC"));
			d.setpSucVta(rs.getString("PSUCVTA"));
			d.setpCaja(rs.getString("PCAJA"));
			d.setpTipDocto(rs.getString("PTIPDOCTO"));
			d.setpNumDocto(rs.getString("PNUMDOCTO"));
			d.setpFecVta    (rs.getString("PFECVTA"));
			d.setpRutVendedor(rs.getString("PRUTVENDEDOR"));
			d.setpEstIni(rs.getString("PESTINI"));
			d.setpCodRegalo(rs.getString("PCODREGALO"));
			d.setpTipCli(rs.getString("PTIPCLI"));
			d.setpIdeCli(rs.getString("PIDECLI"));
			d.setpNomCli(rs.getString("PNOMCLI"));
			d.setpDirCli(rs.getString("PDIRCLI"));
			d.setpFonCli(rs.getString("PFONCLI"));
			d.setpEmailCliente(rs.getString("PEMAILCLIENTE"));
			d.setpFormaPago(rs.getString("PFORMAPAGO"));
			d.setpMedioPago(rs.getString("PMEDIOPAGO"));
			d.setpTipCliDesp(rs.getString("PTIPCLIDESP"));
			d.setpRutDesp(rs.getString("PRUTDESP"));
			d.setpNombreDesp(rs.getString("PNOMBREDESP"));
			d.setpEmail(rs.getString("PEMAIL"));
			d.setpCodRegion(rs.getString("PCODREGION"));
			d.setpCodComuna(rs.getString("PCODCOMUNA"));
			d.setpDireccionDesp(rs.getString("PDIRECCIONDESP"));
			d.setpCodRefUno(rs.getString("PCODREFUNO"));
			d.setpCodRefDos(rs.getString("PCODREFDOS"));
			d.setpObservaciones(rs.getString("POBSERVACIONES"));
			d.setpObservaciones2(rs.getString("POBSERVACIONES2"));
			d.setpObservaciones3(rs.getString("POBSERVACIONES3"));
			d.setpObservaciones4(rs.getString("POBSERVACIONES4"));
			d.setpObservaciones5(rs.getString("POBSERVACIONES5"));
			d.setpObservaciones6(rs.getString("POBSERVACIONES6"));
			d.setpObservaciones7(rs.getString("POBSERVACIONES7"));
			d.setpNumOcu(rs.getString("PNUMOCU"));
			d.setpDespacho(rs.getString("PDESPACHO"));
	
		return d;
	}

}
