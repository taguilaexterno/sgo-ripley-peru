package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.DetalleTrxBoDTO;


/**Extrae estructura compleja de reserva de la base de datos
 * @author Boris Parra Luman [Aligare]
 * @since 25 may. 2018
 */
@Component
public class DetalleTrxBoRowMapper implements RowMapper<DetalleTrxBoDTO> {


	@Override
	public DetalleTrxBoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {	
	
		DetalleTrxBoDTO d = new DetalleTrxBoDTO();
		d.setCorrelativoVenta(rs.getLong("CORRELATIVO_VENTA"));
		d.setNroTrx(rs.getLong("NUMERO_TRX"));
		d.setFechaGeneracion(rs.getDate("FECHA_GENERACION"));
		d.setEstadoIniOC(rs.getInt("ESTADO_INI_OC"));
		d.setEstadoGenOC(rs.getString("ESTADO_GEN_OC"));
		d.setObservacionGenOC(rs.getString("OBSERVACION_GEN_OC"));
		d.setCodigoPanCte(rs.getString("CODIGO_PAN_CTE"));
		d.setTipoPgoBol(rs.getString("TIPO_PGO_BOL"));
		d.setMontoPago(rs.getBigDecimal("MONTO_PAGO"));
		d.setIdAperturaCierre(rs.getLong("ID_APERTURA_CIERRE"));
		d.setIgv(rs.getLong("IGV"));
		d.setMontoNeto(rs.getBigDecimal("MONTO_NETO"));
		d.setSerieCorr(rs.getString("SERIE_CORR"));
		d.setNroBolNC(rs.getLong("NUMERO_BOL_NC"));
	
		return d;
	}

}
