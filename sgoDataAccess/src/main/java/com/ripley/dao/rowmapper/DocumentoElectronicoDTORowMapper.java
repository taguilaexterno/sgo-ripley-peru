package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.DocumentoElectronicoDTO;

/**Row mapper para llenar una lista de objetos equivalente a la tabla documento_electronico del Modelo Extendido (BD).
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component
public class DocumentoElectronicoDTORowMapper implements RowMapper<DocumentoElectronicoDTO> {

	@Override
	public DocumentoElectronicoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		DocumentoElectronicoDTO d = new DocumentoElectronicoDTO();
		
		d.setCorrelativoVenta(rs.getLong("CORRELATIVO_VENTA"));
		d.setSucursal(rs.getInt("NUMERO_SUCURSAL"));
		d.setNroCaja(rs.getInt("NUMERO_CAJA"));
		d.setTramaDTE(rs.getString("TRAMA_DTE"));
		d.setXmlDTE(rs.getString("XML_TDE"));
		d.setXmlMail(rs.getString("XML_MAIL"));
		d.setPdf417(rs.getString("PDF417"));
		
		return d;
	}

}
