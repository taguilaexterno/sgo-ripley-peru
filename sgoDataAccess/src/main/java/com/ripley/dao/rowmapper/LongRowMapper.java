package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.util.Constantes;

@Component("longRowMapper")
public class LongRowMapper implements RowMapper<Long> {

	@Override
	public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return rs.getLong(Constantes.NRO_UNO);
	}

}
