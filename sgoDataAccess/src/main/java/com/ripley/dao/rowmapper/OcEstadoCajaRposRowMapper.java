package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.dao.util.Constantes;

/**Extrae estructura compleja de OCs Rpos de la base de datos para ImprimirOffLine y reprocesos de OCs
 * @author Pablo Salazar Osorio (Aligare)
 * @since 17-12-2018
 */
@Component
public class OcEstadoCajaRposRowMapper implements RowMapper<OcTraceRposDTO> {

	@Override
	public OcTraceRposDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		OcTraceRposDTO ocTRPOS = new OcTraceRposDTO();
		ocTRPOS.setOc(rs.getLong(Constantes.CORRELATIVO_VENTA));
		ocTRPOS.setOcTV(rs.getInt(Constantes.NAME_GET_OC_TV));
		ocTRPOS.setInsertaOC(rs.getInt(Constantes.NAME_INSERTA_OC));
		ocTRPOS.setValidaOC(rs.getInt(Constantes.NAME_VALIDA_OC));
		ocTRPOS.setImprimeOC(rs.getInt(Constantes.NAME_IMPR_OC));
		ocTRPOS.setGeneraJson(rs.getInt(Constantes.NAME_GENERA_JSON));
		ocTRPOS.setPpl(rs.getInt(Constantes.NAME_PPL));
		return ocTRPOS;
	}
	


}
