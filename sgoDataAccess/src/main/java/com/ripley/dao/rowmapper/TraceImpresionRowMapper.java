package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.TraceImpresionDTO;


/**Extrae estructura compleja de traza de impresión de la base de datos
 * @author Boris Parra Luman [Aligare]
 * @since 25 may. 2018
 */
@Component
public class TraceImpresionRowMapper implements RowMapper<TraceImpresionDTO> {


	@Override
	public TraceImpresionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {	
	
		TraceImpresionDTO t = new TraceImpresionDTO();
		t.setCorrelativoVenta(rs.getLong("CORRELATIVO_VENTA"));
		t.setPpl(rs.getInt("PPL"));
		t.setBo(rs.getInt("BO"));
		t.setBt(rs.getInt("BT"));
		t.setBcv(rs.getInt("BCV"));
		t.setMkp(rs.getInt("MKP"));
	
		return t;
	}

}
