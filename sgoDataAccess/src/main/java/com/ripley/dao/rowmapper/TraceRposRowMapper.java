package com.ripley.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.dao.dto.TraceRposDTO;

/**Extrae estructura compleja de traza Rpos de la base de datos
 * @author Pablo Salazar Osorio (Aligare)
 * @since 08-10-2018
 */
@Component
public class TraceRposRowMapper implements RowMapper<TraceRposDTO> {

	@Override
	public TraceRposDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		TraceRposDTO t = new TraceRposDTO();
		t.setCorrelativoVenta(rs.getLong("CORRELATIVO_VENTA"));
		t.setFechaInicio(rs.getDate("FECHA_INI"));
		t.setGetOcTv(rs.getInt("GET_OC_TV"));
		t.setTiempoGetOcTv(rs.getString("T_GET_OC_TV"));
		t.setInsertaOc(rs.getInt("INSERTA_OC"));
		t.setTiempoInsertaOc(rs.getString("T_INSERTA_OC"));
		t.setValidaOc(rs.getInt("VALIDA_OC"));
		t.setTiempoValidaOc(rs.getString("T_VALIDA_OC"));
		t.setImprimirOc(rs.getInt("IMPR_OC"));
		t.setTiempoImprimirOc(rs.getString("T_IMPR_OC"));
		t.setGeneraJson(rs.getInt("GENERA_JSON"));
		t.setTiempoGeneraJson(rs.getString("T_GENERA_JSON"));
		t.setFechaFin(rs.getDate("FECHA_FIN"));
		return t;
	}
	
	

}
