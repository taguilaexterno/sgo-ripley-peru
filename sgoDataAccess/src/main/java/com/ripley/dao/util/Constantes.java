package com.ripley.dao.util;

/**Constantes DAO
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class Constantes {

	public static final String VACIO = "";
	public static final String CODIGO_APP = "SGO CORP DAO";
	public static final String ORDER_ID_PARAM = "ORDER_ID";
	public static final String ORDER_XML_PARAM = "ORDER_XML";
	public static final String ORDER_ESTADO_PARAM = "ORDER_ESTADO";
	public static final String ORDER_MSG_PARAM = "ORDER_MSG";
	public static final String ORDER_EST_OUT_PARAM = "ORDER_EST_OUT";
	public static final String ORDER_MSG_OUT_PARAM = "ORDER_MSG_OUT";
	public static final String PROCEDURE_INSERTA_ME = "TVME_INSERTA_OC_MODEL_EXTENDED";
	public static final Long UNO_LONG = 1L;
	public static final String MSG_OK = "OK";
	public static final int COD_OK = 0;
	public static final int NRO_CERO = 0;
	public static final int NRO_UNO = 1;
	
	
	/*
	 * Procedure pago en tienda
	 */
	public static final String PROCEDURE_INSERTA_PAGO_TIENDA = "PROC_INSERTA_PAGO_TIENDA";
	public static final String PKG_CAVIRA_PAGO_TIENDA = "CAVIRA_PAGO_TIENDA";
	
	public static final String INSERT_PAGO_TIENDA_STRUC_PARAM = "INSERT_PAGO_TIENDA_STRUC";
	public static final String INSERT_PAGO_TIENDA_STRUC_TYPE_NAME = "PAGO_EN_TIENDA_TYPE";
	
	public static final String MENSAJE_OUT_PARAM = "MENSAJE";
	public static final String COD_OUT_PARAM = "CODIGO";
	public static final String PKG_SGO_RESERVAS = "SGO_RESERVAS";
	public static final String PROCEDURE_PROC_ACTUALIZA_ESTADO_VIGENCIA = "PROC_ACTUALIZA_ESTADO_VIGENCIA";
	public static final String PROCEDURE_PROC_GET_FECHA_VIG = "PROC_GET_FECHA_VIG";
	public static final String PROCEDURE_PROC_GET_TOPE_PRODUCTOS = "PROC_GET_TOPE_PRODUCTOS";
	public static final String PROCEDURE_PROC_GET_RESERVA_OC = "PROC_GET_RESERVA_OC";
	public static final String PROCEDURE_PROC_GET_RESERVA_RUT = "PROC_GET_RESERVA_RUT";
	public static final String PROCEDURE_PROC_GET_ESTADO = "PROC_GET_ESTADO";
	
	public static final String PKG_CAVIRA_MANEJO_BOLETAS = "CAVIRA_MANEJO_BOLETAS";
	public static final String PROCEDURE_PROC_GET_OC_BY_CAJA_ESTADO = "PROC_GET_OC_BY_CAJA_ESTADO";
	public static final String PROCEDURE_PROC_GET_OC_URL_DOCE = "PROC_GET_OC_URL_DOCE";
	public static final String PROCEDURE_PROC_GET_OC_BY_CAJA_ESTADO_RPOS = "PROC_GET_OC_BY_CAJAESTADO_RPOS";
	public static final String CAJA_PARAM = "IN_CAJA";
	public static final String SUCURSAL_PARAM = "IN_NRO_SUCURSAL";
	public static final String ESTADO_PARAM = "IN_ESTADO";
	public static final String CANTIDAD_PARAM = "IN_CANTIDAD";
	public static final String MAIL_ACTIVE_PARAM = "IN_MAIL_ACTIVE";
	public static final String QUERY_OUT_PARAM = "OUT_QUERY";
	public static final String CURSOR_OUT_PARAM = "OUT_CURSOR";
	
	public static final String NUM_RESERVA 			= "NUM_RESERVA";
	public static final String FEC_VIGENCIA 		= "FEC_VIGENCIA";
	public static final String COD_ERROR 			= "COD_ERROR";
	public static final String MSG_ERROR 			= "MSG_ERROR";
	public static final String TOPE_PRODUCTOS 		= "TOPE_PRODUCTOS";
	public static final String ERROR_PARSEO 		= "ERROR: NO SE PUEDE PARSEAR INFORMACION DESDE BASE DE DATOS";
	public static final String RUT_CLIENTE 			= "RUT_CLIENTE";
	public static final String CONSULTA_RES 		= "CONSULTA_RES";
	public static final String CONSULTA_ART 		= "CONSULTA_ART";
	public static final String RESERVA 				= "RESERVA";
	public static final String ARTICULOS 			= "ARTICULOS";
	public static final String CORRELATIVO_VENTA	= "CORRELATIVO_VENTA";
	public static final String FECHA_RESERVA 		= "FECHA_RESERVA";
	public static final String FECHA_VIGENCIA 		= "FECHA_VIGENCIA";
	public static final String MONTO_VENTA 			= "MONTO_VENTA";
	public static final String COD_ESTADO 			= "COD_ESTADO";
	public static final String DESC_ESTADO 			= "DESC_ESTADO";
	public static final String ESTADO_OUT 			= "ESTADO_OUT";
	public static final String ERROR_GENERAL 		= "ERROR: ERROR EN SERVICIO WEB, VERIFIQUE LA INFORMACIÓN INGRESADA";
	public static final String SKU 					= "SKU";
	public static final String DESCRIPCION 			= "DESCRIPCION";
	public static final String TIPO_DESPACHO 		= "TIPO_DESPACHO";
	public static final String DIRECCION_DESPACHO 	= "DIRECCION_DESPACHO";
	public static final String NOMBRE_CLIENTE 		= "NOMBRE_CLIENTE";
	public static final String CORREO_DESPACHO 		= "CORREO_DESPACHO";
	public static final String MONTO_RIPLEY 		= "MONTO_RIPLEY";
	public static final String PROYECTO 			= "Pago en Tienda";
	public static final Boolean FALSE 				= false;
	public static final String NO_EXISTE 			= "ORDEN DE COMPRA CONSULTADA NO EXISTE EN MODELO EXTENDIDO.";
	public static final String BOLETA_GENERADA 		= "BOLETA GENERADA, SE DEBE GENERAR NOTA DE CREDITO.";
	public static final String PENDIENTE_BOLETA 	= "EN PROCESO SGO, GENERANDO BOLETA";
	public static final String PKG_CAVIRA_MANEJO_CAJA = "CAVIRA_MANEJO_CAJA";
	public static final String PROCEDURE_OBT_TRX_CAJA = "OBT_TRX_CAJA";
	public static final String FECHA_PARAM = "IN_FECHA";
	public static final String TRX_OUT_PARAM = "TRX_OUT";
	public static final String PROCEDURE_UPD_TRX_CAJA = "UPD_TRX_CAJA";
	public static final String TRX_IN_PARAM = "TRX_IN";
	public static final String PKG_TEST = "SGO_TEST_BO";
	public static final String GET_INI_INSERT_OC = "GET_INI_INSERT_OC";
	public static final String CORRELATIVO_VENTA_OUT = "CORRELATIVO_VENTA_OUT";
	public static final String ID_DESPACHO_OUT = "ID_DESPACHO_OUT";
	public static final String FECHA_ACTUAL_OUT = "FECHA_ACTUAL_OUT";
	public static final String FECHA_DESPACHO_OUT = "FECHA_DESPACHO_OUT";
	public static final String PKG_SGO_PARAMETROS = "SGO_PARAMETROS";
	public static final String PROCEDURE_GET_PARAM = "GET_PARAM";
	public static final String PARAM_NAME_PARAM = "PARAM_NAME";
	public static final String PARAM_VALUE_PARAM = "PARAM_VALUE";
	public static final String PROCEDURE_UPD_PARAM = "UPD_PARAM";
	public static final String LAST_ID_TV = "LAST_ID_TV";
	public static final String PKG_TV_TRACKING = "PKG_TRACKING";
	public static final String PROCEDURE_PRC_TRACKING_LOAD = "PRC_TRACKING_LOAD";
	public static final String PROCEDURE_PRC_TRACKING_LOAD_BT = "PRC_TRACKING_LOAD_BT";
	
	
	public static final String PKG_TV_DESPACHO		= "PKG_DESPACHO";
	public static final String PRC_OCS_PENDIENTES	= "PRC_OCS_PENDIENTES";
	public static final String PRC_OCS_PROCESAR 	= "PRC_OCS_PROCESAR";
	public static final String P_XML 				= "P_XML";
	public static final String P_ERROR_ID			= "P_ERROR_ID";
	public static final String P_MENSAJE			= "P_MENSAJE";
	public static final String LAST_DATETIME_TV = "LAST_DATETIME_TV";
	public static final String CAVIRA_MANEJO_NC = "CAVIRA_MANEJO_NC";
	public static final String OBT_TRX_XDETALLE_BO = "OBT_TRX_XDETALLE_BO";
	public static final String IN_CORRELATIVO_VENTA = "IN_CORRELATIVO_VENTA";
	public static final String PROC_GET_DOC_ELEC_BY_CORR_VEN = "PROC_GET_DOC_ELEC_BY_CORR_VEN";
	public static final String PROC_GET_DET_TRX_BO_BY_OC = "PROC_GET_DET_TRX_BO_BY_OC";
	public static final String PROC_INSERT_JSON_BK = "PROC_INSERT_JSON_BK";
	public static final String IN_TIPO_LLAMDA = "IN_TIPO_LLAMDA";
	public static final String IN_JSON = "IN_JSON";
	public static final String PROC_GET_TRACE_IMPR_BY_OC = "PROC_GET_TRACE_IMPR_BY_OC";
	public static final String PROC_UPSERT_TRACE_IMPR = "PROC_UPSERT_TRACE_IMPR";
	public static final String IN_ESTADO_PPL = "IN_ESTADO_PPL";
	public static final String IN_ESTADO_BO = "IN_ESTADO_BO";
	public static final String IN_ESTADO_BT = "IN_ESTADO_BT";
	public static final String IN_ESTADO_BCV = "IN_ESTADO_BCV";
	public static final String IN_ESTADO_MKP = "IN_ESTADO_MKP";
	public static final String PROC_GET_ID_PROVEEDOR = "PROC_GET_ID_PROVEEDOR";
	public static final String OUT_RUT_COMERCIAL_PARAM = "OUT_RUT_COMERCIAL";
	public static final String OUT_COD_PARAM = "OUT_COD";
	public static final String OUT_MJE_PARAM = "OUT_MJE";
	public static final String PROC_GET_TRACE_RPOS = "PROC_GET_TRACE_RPOS";
	public static final String PROC_UPSERT_TRACE_RPOS = "PROC_UPSERT_TRACE_RPOS";
	public static final String I_FECHA_INI = "I_FECHA_INI";
	public static final String I_GET_OC_TV = "I_GET_OC_TV";
	public static final String I_T_GET_OC_TV = "I_T_GET_OC_TV";
	public static final String I_INSERTA_OC = "I_INSERTA_OC";
	public static final String I_T_INSERTA_OC = "I_T_INSERTA_OC";
	public static final String I_VALIDA_OC = "I_VALIDA_OC";
	public static final String I_T_VALIDA_OC = "I_T_VALIDA_OC";
	public static final String I_IMPR_OC = "I_IMPR_OC";
	public static final String I_T_IMPR_OC = "I_T_IMPR_OC";
	public static final String I_GENERA_JSON = "I_GENERA_JSON";
	public static final String I_T_GENERA_JSON = "I_T_GENERA_JSON";
	public static final String I_FECHA_FIN = "I_FECHA_FIN";

	public static final String PROC_REPROC_TRACE_RPOS 		= "PROC_REPROC_TRACE_RPOS";
	public final static String NAME_GET_OC_TV				= "GET_OC_TV";
	public final static String NAME_INSERTA_OC				= "INSERTA_OC";
	public final static String NAME_VALIDA_OC				= "VALIDA_OC";
	public final static String NAME_IMPR_OC					= "IMPR_OC";
	public final static String NAME_GENERA_JSON				= "GENERA_JSON";
	public final static String NAME_PPL						= "PPL";
	public final static String P_CUD						= "p_cud";
	public final static String P_FECHA_CAMBIO				= "p_fecha_cambio";
	public final static String P_CODIGO_ESTADO				= "p_codigo_estado";
	public final static String P_DSC_ESTADO					= "p_dsc_estado";
	public final static String P_ORDEN_COMPRA				= "p_orden_compra";
	public final static String P_MENSAJE_OUT				= "p_mensaje";

	/*Se Agrega para Cybersource*/
	public static final String PROCEDURE_ACTUALIZA_OC_CS = "CSME_ACTUALIZA_ESTADO_OC_CS";
    public static final Long ORDER_ID_PARAM_LONG = 0L;
    public static final String PKG_CORP_RDSUSERSGO = "CORP_RDSUSERSGO";
	public final static String IN_nCORRELATIVO_VENTA = "ORDER_ID";
	public final static String IN_cMsgXml = "ORDER_XML";
	public final static String OUT_nCodEstProc = "ORDER_EST";
	public final static String OUT_vMsgProc = "OK";
	
	/*Se Agrega para PagoEfectivo*/
	public static final String PROCEDURE_REGISTRA_PAGO_EFECTIVO = "PROC_REGISTRA_PAGO_EFECTIVO";
	public final static String IN_CIP = "CIP";
	public final static String IN_MONEDA = "MONEDA";
	public final static String IN_MONTO_VENTA = "MONTO_VENTA";
	public final static String IN_ESTADO_CIP = "ESTADO_CIP";
	public final static String IN_CODIGO_EVENTO = "CODIGO_EVENTO";
	public final static String IN_FECHA_PAGO = "FECHA_PAGO";
	public final static String IN_CODIGO_TRANSACCION = "CODIGO_TRANSACCION";
	public final static int NRO_MENOSUNO = -1;
	
	private Constantes() {
		
	}
	
}
