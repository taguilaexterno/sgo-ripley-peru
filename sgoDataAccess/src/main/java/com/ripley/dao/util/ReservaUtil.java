package com.ripley.dao.util;

import java.util.ArrayList;
import java.util.List;

import com.ripley.dao.dto.Productos;
import com.ripley.dao.dto.Reserva;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;

public class ReservaUtil {
	
	private static final AriLog logger = new AriLog(ReservaUtil.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

	public static void organizarProductos(List<Reserva> reservas, List<Productos> productos) {
		
		logger.initTrace("organizarProductos", "List: reservas["+reservas.size()+"], List productos["+productos.size()+"]");
		for(int i=0; i<reservas.size(); i++){
			List<Productos> productosAux = new ArrayList<Productos>();
			for(int j=0; j<productos.size(); j++){
				if(reservas.get(i).getOrdenCompra().equals(productos.get(j).getCorrelativoVenta())){
					productosAux.add(productos.get(j));
				}
			}
			reservas.get(i).setProductos(productosAux);
			reservas.get(i).setCantidadProductos(reservas.get(i).getProductos().size());
			logger.traceInfo("organizarProductos", "generacion de reserva", new KeyLog("reserva["+i+"]", reservas.get(i).toString()));
		}
		logger.endTrace("organizarProductos", "Finalizado", "");
	}

}
