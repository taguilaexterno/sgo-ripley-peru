package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class ArticuloVenta implements Serializable{
	private Long		correlativoVenta;
	private Integer		correlativoItem;
	private String		codArticulo;
	private String		descRipley;
	private BigDecimal		precio;
	private Integer		unidades;
	private Integer		color;
	private BigDecimal		montoDescuento;
	private Integer		tipoDescuento;
	private String		codDespacho;
	private Integer		tipoRegalo;
	private String		esregalo;
	private String		mensaje;
	private Integer		numNotaCredito;
	private Timestamp	fecNotaCredito;
	private Timestamp	horaNotaCredito;
	private Integer		numCajaNotaCredito;
	private Long		correlativoNotaCredito;
	private Integer		rutNotaCredito;
	private String		dvNotaCredito;
	private Integer		indicadorEg;
	private String		tipoDespacho;
	private Date        fechaDespacho;
	private String		codBodega;
	private String		tipoPapelRegalo;
	private Long		despachoId;
	private String		estadoVenta;
	private String		ordenMkp;
	private String		depto;
	private String		deptoGlosa;
	private Integer     costo;
	private Integer		esNC;
	private String		codArticuloMkp;
	private String		bodegaStock;
	private Integer 	esMkp;
	private Integer 	enTransito;
	private BigDecimal  precioPorUnidades;
	private String		codigoSunat;
	private String		direccionEntrega;
	private String		horarioEntrega;
	private String		urlGoogleEntrega;
	
	public Integer getEsMkp() {
		return esMkp;
	}
	public void setEsMkp(Integer esMkp) {
		this.esMkp = esMkp;
	}
	public Integer getEnTransito() {
		return enTransito;
	}
	public void setEnTransito(Integer enTransito) {
		this.enTransito = enTransito;
	}
	public BigDecimal getPrecioPorUnidades() {
		return precioPorUnidades;
	}
	public void setPrecioPorUnidades(BigDecimal precioPorUnidades) {
		this.precioPorUnidades = precioPorUnidades;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getCorrelativoItem() {
		return correlativoItem;
	}
	public void setCorrelativoItem(Integer correlativoItem) {
		this.correlativoItem = correlativoItem;
	}
	public String getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}
	public String getDescRipley() {
		return descRipley;
	}
	public void setDescRipley(String descRipley) {
		this.descRipley = descRipley;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	public Integer getUnidades() {
		return unidades;
	}
	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}
	public BigDecimal getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(BigDecimal montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public Integer getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(Integer tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	public String getCodDespacho() {
		return codDespacho;
	}
	public void setCodDespacho(String codDespacho) {
		this.codDespacho = codDespacho;
	}
	public Integer getTipoRegalo() {
		return tipoRegalo;
	}
	public void setTipoRegalo(Integer tipoRegalo) {
		this.tipoRegalo = tipoRegalo;
	}
	public String getEsregalo() {
		return esregalo;
	}
	public void setEsregalo(String esregalo) {
		this.esregalo = esregalo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(Integer numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public Timestamp getFecNotaCredito() {
		return fecNotaCredito;
	}
	public void setFecNotaCredito(Timestamp fecNotaCredito) {
		this.fecNotaCredito = fecNotaCredito;
	}
	public Timestamp getHoraNotaCredito() {
		return horaNotaCredito;
	}
	public void setHoraNotaCredito(Timestamp horaNotaCredito) {
		this.horaNotaCredito = horaNotaCredito;
	}
	public Integer getNumCajaNotaCredito() {
		return numCajaNotaCredito;
	}
	public void setNumCajaNotaCredito(Integer numCajaNotaCredito) {
		this.numCajaNotaCredito = numCajaNotaCredito;
	}
	public Long getCorrelativoNotaCredito() {
		return correlativoNotaCredito;
	}
	public void setCorrelativoNotaCredito(Long correlativoNotaCredito) {
		this.correlativoNotaCredito = correlativoNotaCredito;
	}
	public Integer getRutNotaCredito() {
		return rutNotaCredito;
	}
	public void setRutNotaCredito(Integer rutNotaCredito) {
		this.rutNotaCredito = rutNotaCredito;
	}
	public String getDvNotaCredito() {
		return dvNotaCredito;
	}
	public void setDvNotaCredito(String dvNotaCredito) {
		this.dvNotaCredito = dvNotaCredito;
	}
	public Integer getIndicadorEg() {
		return indicadorEg;
	}
	public void setIndicadorEg(Integer indicadorEg) {
		this.indicadorEg = indicadorEg;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public Date getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	public String getCodBodega() {
		return codBodega;
	}
	public void setCodBodega(String codBodega) {
		this.codBodega = codBodega;
	}
	public String getTipoPapelRegalo() {
		return tipoPapelRegalo;
	}
	public void setTipoPapelRegalo(String tipoPapelRegalo) {
		this.tipoPapelRegalo = tipoPapelRegalo;
	}
	public Long getDespachoId() {
		return despachoId;
	}
	public void setDespachoId(Long despachoId) {
		this.despachoId = despachoId;
	}
	public String getEstadoVenta() {
		return estadoVenta;
	}
	public void setEstadoVenta(String estadoVenta) {
		this.estadoVenta = estadoVenta;
	}
	public String getOrdenMkp() {
		return ordenMkp;
	}
	public void setOrdenMkp(String ordenMkp) {
		this.ordenMkp = ordenMkp;
	}
	public String getDepto() {
		return depto;
	}
	public void setDepto(String depto) {
		this.depto = depto;
	}
	public String getDeptoGlosa() {
		return deptoGlosa;
	}
	public void setDeptoGlosa(String deptoGlosa) {
		this.deptoGlosa = deptoGlosa;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Integer getEsNC() {
		return esNC;
	}
	public void setEsNC(Integer esNC) {
		this.esNC = esNC;
	}
	public String getCodArticuloMkp() {
		return codArticuloMkp;
	}
	public void setCodArticuloMkp(String codArticuloMkp) {
		this.codArticuloMkp = codArticuloMkp;
	}
	public String getBodegaStock() {
		return bodegaStock;
	}
	public void setBodegaStock(String bodegaStock) {
		this.bodegaStock = bodegaStock;
	}
	public String getCodigoSunat() {
		return codigoSunat;
	}
	public void setCodigoSunat(String codigoSunat) {
		this.codigoSunat = codigoSunat;
	}
	
	public String getDireccionEntrega() {
		return direccionEntrega;
	}
	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}
	public String getHorarioEntrega() {
		return horarioEntrega;
	}
	public void setHorarioEntrega(String horarioEntrega) {
		this.horarioEntrega = horarioEntrega;
	}
	public String getUrlGoogleEntrega() {
		return urlGoogleEntrega;
	}
	public void setUrlGoogleEntrega(String urlGoogleEntrega) {
		this.urlGoogleEntrega = urlGoogleEntrega;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArticuloVenta [correlativoVenta=");
		builder.append(correlativoVenta);
		builder.append(", correlativoItem=");
		builder.append(correlativoItem);
		builder.append(", codArticulo=");
		builder.append(codArticulo);
		builder.append(", descRipley=");
		builder.append(descRipley);
		builder.append(", precio=");
		builder.append(precio);
		builder.append(", unidades=");
		builder.append(unidades);
		builder.append(", color=");
		builder.append(color);
		builder.append(", montoDescuento=");
		builder.append(montoDescuento);
		builder.append(", tipoDescuento=");
		builder.append(tipoDescuento);
		builder.append(", codDespacho=");
		builder.append(codDespacho);
		builder.append(", tipoRegalo=");
		builder.append(tipoRegalo);
		builder.append(", esregalo=");
		builder.append(esregalo);
		builder.append(", mensaje=");
		builder.append(mensaje);
		builder.append(", numNotaCredito=");
		builder.append(numNotaCredito);
		builder.append(", fecNotaCredito=");
		builder.append(fecNotaCredito);
		builder.append(", horaNotaCredito=");
		builder.append(horaNotaCredito);
		builder.append(", numCajaNotaCredito=");
		builder.append(numCajaNotaCredito);
		builder.append(", correlativoNotaCredito=");
		builder.append(correlativoNotaCredito);
		builder.append(", rutNotaCredito=");
		builder.append(rutNotaCredito);
		builder.append(", dvNotaCredito=");
		builder.append(dvNotaCredito);
		builder.append(", indicadorEg=");
		builder.append(indicadorEg);
		builder.append(", tipoDespacho=");
		builder.append(tipoDespacho);
		builder.append(", fechaDespacho=");
		builder.append(fechaDespacho);
		builder.append(", codBodega=");
		builder.append(codBodega);
		builder.append(", tipoPapelRegalo=");
		builder.append(tipoPapelRegalo);
		builder.append(", despachoId=");
		builder.append(despachoId);
		builder.append(", estadoVenta=");
		builder.append(estadoVenta);
		builder.append(", ordenMkp=");
		builder.append(ordenMkp);
		builder.append(", depto=");
		builder.append(depto);
		builder.append(", deptoGlosa=");
		builder.append(deptoGlosa);
		builder.append(", costo=");
		builder.append(costo);
		builder.append(", esNC=");
		builder.append(esNC);
		builder.append(", codArticuloMkp=");
		builder.append(codArticuloMkp);
		builder.append(", bodegaStock=");
		builder.append(bodegaStock);
		builder.append(", esMkp=");
		builder.append(esMkp);
		builder.append(", enTransito=");
		builder.append(enTransito);
		builder.append(", precioPorUnidades=");
		builder.append(precioPorUnidades);
		builder.append(", codigoSunat=");
		builder.append(codigoSunat);
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
