package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;

public class ArticuloVentaTramaDTE implements Serializable{

	private ArticuloVenta articuloVenta;
	private IdentificadorMarketplace identificadorMarketplace;
	private Despacho despacho;
	private DatoFacturacion datoFacturacion;
	
	public ArticuloVenta getArticuloVenta() {
		return articuloVenta;
	}
	public void setArticuloVenta(ArticuloVenta articuloVenta) {
		this.articuloVenta = articuloVenta;
	}
	public IdentificadorMarketplace getIdentificadorMarketplace() {
		return identificadorMarketplace;
	}
	public void setIdentificadorMarketplace(
			IdentificadorMarketplace identificadorMarketplace) {
		this.identificadorMarketplace = identificadorMarketplace;
	}
	public Despacho getDespacho() {
		return despacho;
	}
	public void setDespacho(Despacho despacho) {
		this.despacho = despacho;
	}
	public DatoFacturacion getDatoFacturacion() {
		return datoFacturacion;
	}
	public void setDatoFacturacion(DatoFacturacion datoFacturacion) {
		this.datoFacturacion = datoFacturacion;
	}

}
