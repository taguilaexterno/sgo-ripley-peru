package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;

public class DatoFacturacion implements Serializable{

	private Long correlativoVenta;
	private Integer addressId;
	private String	direccionFacturaDes;
	private String	comunaFacturaDes;
	private String	ciudadFacturaDes;
	private String	rutFactura;
	private String	razonSocialFactura;
	private String	telefonoFactura;
	private Integer giroFacturaId;
	private String	giroFacturaDes;
	private Integer regionFacturaId;
	private String	regionFacturaDes;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public String getDireccionFacturaDes() {
		return direccionFacturaDes;
	}
	public void setDireccionFacturaDes(String direccionFacturaDes) {
		this.direccionFacturaDes = direccionFacturaDes;
	}
	public String getComunaFacturaDes() {
		return comunaFacturaDes;
	}
	public void setComunaFacturaDes(String comunaFacturaDes) {
		this.comunaFacturaDes = comunaFacturaDes;
	}
	public String getCiudadFacturaDes() {
		return ciudadFacturaDes;
	}
	public void setCiudadFacturaDes(String ciudadFacturaDes) {
		this.ciudadFacturaDes = ciudadFacturaDes;
	}
	public String getRutFactura() {
		return rutFactura;
	}
	public void setRutFactura(String rutFactura) {
		this.rutFactura = rutFactura;
	}
	public String getRazonSocialFactura() {
		return razonSocialFactura;
	}
	public void setRazonSocialFactura(String razonSocialFactura) {
		this.razonSocialFactura = razonSocialFactura;
	}
	public String getTelefonoFactura() {
		return telefonoFactura;
	}
	public void setTelefonoFactura(String telefonoFactura) {
		this.telefonoFactura = telefonoFactura;
	}
	public Integer getGiroFacturaId() {
		return giroFacturaId;
	}
	public void setGiroFacturaId(Integer giroFacturaId) {
		this.giroFacturaId = giroFacturaId;
	}
	public String getGiroFacturaDes() {
		return giroFacturaDes;
	}
	public void setGiroFacturaDes(String giroFacturaDes) {
		this.giroFacturaDes = giroFacturaDes;
	}
	public Integer getRegionFacturaId() {
		return regionFacturaId;
	}
	public void setRegionFacturaId(Integer regionFacturaId) {
		this.regionFacturaId = regionFacturaId;
	}
	public String getRegionFacturaDes() {
		return regionFacturaDes;
	}
	public void setRegionFacturaDes(String regionFacturaDes) {
		this.regionFacturaDes = regionFacturaDes;
	}
	
}
