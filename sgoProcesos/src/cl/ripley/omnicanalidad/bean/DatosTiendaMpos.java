package cl.ripley.omnicanalidad.bean;

public class DatosTiendaMpos {

	private String urlImagen;
	private String horario;
	private String direccion;
	
	
	public String getUrlImagen() {
		return urlImagen;
	}
	public String getHorario() {
		return horario;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
	
	
	
	
}
