package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class Despacho implements Serializable{

	private Long		despachoId;
	private Long		correlativoVenta;
	private Integer		rutDespacho;
	private String		nombreDespacho;
	private Timestamp	fechaDespacho;
	private Integer		sucursalDespacho;
	private Integer		comunaDespacho;
	private Integer		regionDespacho;
	private String		direccionDespacho;
	private String		telefonoDespacho;
	private String		jornadaDespacho;
	private String		observacion;
	private Integer		rutCliente;
	private String		direccionCliente;
	private String		telefonoCliente;
	private Integer		tipoCliente;
	private String		nombreCliente;
	private String		apellidoPatCliente;
	private String		apellidoMatCliente;
	private Integer		tipoDespacho;
	private String		emailCliente;
	private Integer		codigoRegalo;
	private String		mensajeTarjeta;
	private Integer		optcounter;
	private String		aceptaCorreo;
	private Integer		regionFacturacion;
	private Integer		comunaFacturacion;
	private Integer		envioDte;
	private Integer		addressId;
	private String		comunaDespachoDes;
	private String		regionDespachoDes;
//	private String		emailIdPaypal;
	
	public Long getDespachoId() {
		return despachoId;
	}
	public void setDespachoId(Long despachoId) {
		this.despachoId = despachoId;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getRutDespacho() {
		return rutDespacho;
	}
	public void setRutDespacho(Integer rutDespacho) {
		this.rutDespacho = rutDespacho;
	}
	public String getNombreDespacho() {
		return nombreDespacho;
	}
	public void setNombreDespacho(String nombreDespacho) {
		this.nombreDespacho = nombreDespacho;
	}
	public Timestamp getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(Timestamp fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	public Integer getSucursalDespacho() {
		return sucursalDespacho;
	}
	public void setSucursalDespacho(Integer sucursalDespacho) {
		this.sucursalDespacho = sucursalDespacho;
	}
	public Integer getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(Integer comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public Integer getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(Integer regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getDireccionDespacho() {
		return direccionDespacho;
	}
	public void setDireccionDespacho(String direccionDespacho) {
		this.direccionDespacho = direccionDespacho;
	}
	public String getTelefonoDespacho() {
		return telefonoDespacho;
	}
	public void setTelefonoDespacho(String telefonoDespacho) {
		this.telefonoDespacho = telefonoDespacho;
	}
	public String getJornadaDespacho() {
		return jornadaDespacho;
	}
	public void setJornadaDespacho(String jornadaDespacho) {
		this.jornadaDespacho = jornadaDespacho;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public Integer getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Integer rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getDireccionCliente() {
		return direccionCliente;
	}
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	public String getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
	public Integer getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getApellidoPatCliente() {
		return apellidoPatCliente;
	}
	public void setApellidoPatCliente(String apellidoPatCliente) {
		this.apellidoPatCliente = apellidoPatCliente;
	}
	public String getApellidoMatCliente() {
		return apellidoMatCliente;
	}
	public void setApellidoMatCliente(String apellidoMatCliente) {
		this.apellidoMatCliente = apellidoMatCliente;
	}
	public Integer getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(Integer tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public Integer getCodigoRegalo() {
		return codigoRegalo;
	}
	public void setCodigoRegalo(Integer codigoRegalo) {
		this.codigoRegalo = codigoRegalo;
	}
	public String getMensajeTarjeta() {
		return mensajeTarjeta;
	}
	public void setMensajeTarjeta(String mensajeTarjeta) {
		this.mensajeTarjeta = mensajeTarjeta;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}
	public String getAceptaCorreo() {
		return aceptaCorreo;
	}
	public void setAceptaCorreo(String aceptaCorreo) {
		this.aceptaCorreo = aceptaCorreo;
	}
	public Integer getRegionFacturacion() {
		return regionFacturacion;
	}
	public void setRegionFacturacion(Integer regionFacturacion) {
		this.regionFacturacion = regionFacturacion;
	}
	public Integer getComunaFacturacion() {
		return comunaFacturacion;
	}
	public void setComunaFacturacion(Integer comunaFacturacion) {
		this.comunaFacturacion = comunaFacturacion;
	}
	public Integer getEnvioDte() {
		return envioDte;
	}
	public void setEnvioDte(Integer envioDte) {
		this.envioDte = envioDte;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public String getComunaDespachoDes() {
		return comunaDespachoDes;
	}
	public void setComunaDespachoDes(String comunaDespachoDes) {
		this.comunaDespachoDes = comunaDespachoDes;
	}
	public String getRegionDespachoDes() {
		return regionDespachoDes;
	}
	public void setRegionDespachoDes(String regionDespachoDes) {
		this.regionDespachoDes = regionDespachoDes;
	}
@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Despacho [despachoId=").append(despachoId).append(", correlativoVenta=")
				.append(correlativoVenta).append(", rutDespacho=").append(rutDespacho).append(", nombreDespacho=")
				.append(nombreDespacho).append(", fechaDespacho=").append(fechaDespacho).append(", sucursalDespacho=")
				.append(sucursalDespacho).append(", comunaDespacho=").append(comunaDespacho).append(", regionDespacho=")
				.append(regionDespacho).append(", direccionDespacho=").append(direccionDespacho)
				.append(", telefonoDespacho=").append(telefonoDespacho).append(", jornadaDespacho=")
				.append(jornadaDespacho).append(", observacion=").append(observacion).append(", rutCliente=")
				.append(rutCliente).append(", direccionCliente=").append(direccionCliente).append(", telefonoCliente=")
				.append(telefonoCliente).append(", tipoCliente=").append(tipoCliente).append(", nombreCliente=")
				.append(nombreCliente).append(", apellidoPatCliente=").append(apellidoPatCliente)
				.append(", apellidoMatCliente=").append(apellidoMatCliente).append(", tipoDespacho=")
				.append(tipoDespacho).append(", emailCliente=").append(emailCliente).append(", codigoRegalo=")
				.append(codigoRegalo).append(", mensajeTarjeta=").append(mensajeTarjeta).append(", optcounter=")
				.append(optcounter).append(", aceptaCorreo=").append(aceptaCorreo).append(", regionFacturacion=")
				.append(regionFacturacion).append(", comunaFacturacion=").append(comunaFacturacion)
				.append(", envioDte=").append(envioDte).append(", addressId=").append(addressId)
				.append(", comunaDespachoDes=").append(comunaDespachoDes).append(", regionDespachoDes=")
				.append(regionDespachoDes).append("]");
		return builder.toString();
	}

}
