package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class EstadoMirakl {
	private BigDecimal amount = null;
	private String api = null;
	private Long correlativoVenta = null;
	private String customerId = null;
	private Integer estado = null;
	private String fechaEstado = null;
	private Integer idEnvio = null;
	private Integer intentos = null;
	private String ordenMkp = null;
	private Integer refundId = null;
	private String respuesta = null;
	private String usuarioEstado = null;
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public String getFechaEstado() {
		return fechaEstado;
	}
	public void setFechaEstado(String fechaEstado) {
		this.fechaEstado = fechaEstado;
	}
	public Integer getIdEnvio() {
		return idEnvio;
	}
	public void setIdEnvio(Integer idEnvio) {
		this.idEnvio = idEnvio;
	}
	public Integer getIntentos() {
		return intentos;
	}
	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}
	public String getOrdenMkp() {
		return ordenMkp;
	}
	public void setOrdenMkp(String ordenMkp) {
		this.ordenMkp = ordenMkp;
	}
	public Integer getRefundId() {
		return refundId;
	}
	public void setRefundId(Integer refundId) {
		this.refundId = refundId;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getUsuarioEstado() {
		return usuarioEstado;
	}
	public void setUsuarioEstado(String usuarioEstado) {
		this.usuarioEstado = usuarioEstado;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EstadoMirakl [amount=").append(amount).append(", api=").append(api)
				.append(", correlativoVenta=").append(correlativoVenta).append(", customerId=").append(customerId)
				.append(", estado=").append(estado).append(", fechaEstado=").append(fechaEstado).append(", idEnvio=")
				.append(idEnvio).append(", intentos=").append(intentos).append(", ordenMkp=").append(ordenMkp)
				.append(", refundId=").append(refundId).append(", respuesta=").append(respuesta)
				.append(", usuarioEstado=").append(usuarioEstado).append("]");
		return builder.toString();
	}
	
}
