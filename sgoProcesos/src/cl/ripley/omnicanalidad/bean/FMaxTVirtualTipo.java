package cl.ripley.omnicanalidad.bean;

public class FMaxTVirtualTipo {
	private String maxFecha;
	private int maxNroTrx;
	
	public String getMaxFecha() {
		return maxFecha;
	}
	public void setMaxFecha(String maxFecha) {
		this.maxFecha = maxFecha;
	}
	public int getMaxNroTrx() {
		return maxNroTrx;
	}
	public void setMaxNroTrx(int maxNroTrx) {
		this.maxNroTrx = maxNroTrx;
	}
	
	@Override
	public String toString(){
		String salida = "";
		salida = salida + "Max Fecha ["+this.getMaxFecha()+"]";
		salida = salida + "Max Numero Transaccion ["+this.getMaxNroTrx()+"]";
		return salida;
	}	
	
	

}
