package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class IdentificadorMarketplace implements Serializable{

	private Long		correlativoVenta;
	private String		ordenMkp;
	private String		rutProveedor;
	private Integer		estado;
	private Integer		esMkp;
	private BigDecimal		montoVenta;
	private Long		correlativoBoleta;
	private Integer		nroBoleta;
	private Timestamp	fechaBoleta;
	private Integer		numeroCaja;
	private Integer		numNotaCredito;
	private Integer		numCajaNotaCredito;
	private Timestamp	fecNotaCredito;
	private Timestamp	fechaCreacion;
	private Integer		numeroSucursal;
	private String		razonSocial;
	private BigDecimal     amount;
	private Integer     idRefund;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getOrdenMkp() {
		return ordenMkp;
	}
	public void setOrdenMkp(String ordenMkp) {
		this.ordenMkp = ordenMkp;
	}
	public String getRutProveedor() {
		return rutProveedor;
	}
	public void setRutProveedor(String rutProveedor) {
		this.rutProveedor = rutProveedor;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Integer getEsMkp() {
		return esMkp;
	}
	public void setEsMkp(Integer esMkp) {
		this.esMkp = esMkp;
	}
	public BigDecimal getMontoVenta() {
		return montoVenta;
	}
	public void setMontoVenta(BigDecimal montoVenta) {
		this.montoVenta = montoVenta;
	}
	public Long getCorrelativoBoleta() {
		return correlativoBoleta;
	}
	public void setCorrelativoBoleta(Long correlativoBoleta) {
		this.correlativoBoleta = correlativoBoleta;
	}
	public Integer getNroBoleta() {
		return nroBoleta;
	}
	public void setNroBoleta(Integer nroBoleta) {
		this.nroBoleta = nroBoleta;
	}
	public Timestamp getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(Timestamp fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	public Integer getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(Integer numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public Integer getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(Integer numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public Integer getNumCajaNotaCredito() {
		return numCajaNotaCredito;
	}
	public void setNumCajaNotaCredito(Integer numCajaNotaCredito) {
		this.numCajaNotaCredito = numCajaNotaCredito;
	}
	public Timestamp getFecNotaCredito() {
		return fecNotaCredito;
	}
	public void setFecNotaCredito(Timestamp fecNotaCredito) {
		this.fecNotaCredito = fecNotaCredito;
	}
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getNumeroSucursal() {
		return numeroSucursal;
	}
	public void setNumeroSucursal(Integer numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getIdRefund() {
		return idRefund;
	}
	public void setIdRefund(Integer idRefund) {
		this.idRefund = idRefund;
	}
	
}
