package cl.ripley.omnicanalidad.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Linea {
	
	private String imagen;
	private String text;
	private String text1;
	private String text2;
	private String text3;
	private String align;
	private String size;
	private String value;
	private String bold;
	private String textLeft;
	private String textRight;
	private String sizeColumn1;
	private String sizeColumn3;
	private String tipo;
	private List<String> texts;
	
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public String getText3() {
		return text3;
	}
	public void setText3(String text3) {
		this.text3 = text3;
	}
	public String getAlign() {
		return align;
	}
	public void setAlign(String align) {
		this.align = align;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getBold() {
		return bold;
	}
	public void setBold(String bold) {
		this.bold = bold;
	}
	public String getTextLeft() {
		return textLeft;
	}
	public void setTextLeft(String textLeft) {
		this.textLeft = textLeft;
	}
	public String getTextRight() {
		return textRight;
	}
	public void setTextRight(String textRight) {
		this.textRight = textRight;
	}
	public String getSizeColumn1() {
		return sizeColumn1;
	}
	public void setSizeColumn1(String sizeColumn1) {
		this.sizeColumn1 = sizeColumn1;
	}
	public String getSizeColumn3() {
		return sizeColumn3;
	}
	public void setSizeColumn3(String sizeColumn3) {
		this.sizeColumn3 = sizeColumn3;
	}
	public List<String> getTexts() {
		return texts;
	}
	public void setTexts(List<String> texts) {
		this.texts = texts;
	}
	
	

}
