package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class MkpMkl {
	
	private int estadoMkpMkl;
	private BigDecimal totalVentaMkpSubOrden;
	private BigDecimal totalVentaMkpRutPrv;
	
	
	public int getEstadoMkpMkl() {
		return estadoMkpMkl;
	}
	public void setEstadoMkpMkl(int estadoMkpMkl) {
		this.estadoMkpMkl = estadoMkpMkl;
	}
	public BigDecimal getTotalVentaMkpSubOrden() {
		return totalVentaMkpSubOrden;
	}
	public void setTotalVentaMkpSubOrden(BigDecimal totalVentaMkpSubOrden) {
		this.totalVentaMkpSubOrden = totalVentaMkpSubOrden;
	}
	public BigDecimal getTotalVentaMkpRutPrv() {
		return totalVentaMkpRutPrv;
	}
	public void setTotalVentaMkpRutPrv(BigDecimal totalVentaMkpRutPrv) {
		this.totalVentaMkpRutPrv = totalVentaMkpRutPrv;
	}
	
	

}
