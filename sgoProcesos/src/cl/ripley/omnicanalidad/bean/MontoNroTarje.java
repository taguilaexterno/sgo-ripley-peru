package cl.ripley.omnicanalidad.bean;

public class MontoNroTarje {
	private int nroTarjeta;
	private int monto;
	
	public int getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(int nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
	
	

}
