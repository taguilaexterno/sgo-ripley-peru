package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class NotaVenta implements Serializable {

	private Long correlativoVenta;
	private Timestamp fechaCreacion;
	private Timestamp fechaHoraCreacion;
	private Integer estado;
	private BigDecimal montoVenta;
	private BigDecimal montoDescuento;
	private Integer tipoDescuento;
	private Long rutComprador;
	private String dvComprador;
	private Integer numeroSucursal;
	private Long codigoRegalo;
	private Integer tipoRegalo;
	private Long numeroBoleta;
	private Timestamp fechaBoleta;
	private Timestamp horaBoleta;
	private Integer numeroCaja;
	private Long correlativoBoleta;
	private Integer rutBoleta;
	private String dvBoleta;
	private Long numNotaCredito;
	private Timestamp fecNotaCredito;
	private Timestamp horaNotaCredito;
	private Integer numCajaNotaCredito;
	private Long correlativoNotaCredito;
	private Integer rutNotaCredito;
	private String dvNotaCredito;
	private String tvnveGlsOreExo;
	private String tipoPromocion;
//	private Integer optcounter;
	private String origenVta;
	private String ejecutivoVta;
	private Integer tipoDoc;
	private String folioSii;
	private Long folioNcSii;
	private String urlDoce;
	private String urlNotaCredito;
	private String usuario;
	private String indicadorMkp;
	private String tipoDespacho;
	private Long rutCliente;
	private String telefonoCliente;
	private String emailCliente;
	private Integer comunaDespacho;
	private Integer regionDespacho;
	private String direccionDespacho;
	private Integer minutosCompra;
//	private String ruc;
//	private String razonSocial;
	private String nombreApellido;
//	private BigDecimal paypalTipoCambio;
	private Integer enviaDoceFisico;
	private String pcMac;
	private Integer sucursalMac;
	private String tipoPago;
	private String estadoDes;
	private String ncuotas;
	private String bloqueado;
	private String glosaBloqueo;
	private Long nroTrx;
	private Long nroTrxRpos;
	private Integer horasAdministrativas;
	private String kioskoVendedor;
	private String kioskoVendedorMac;

	public Long getNroTrxRpos() {
		return nroTrxRpos;
	}
	public void setNroTrxRpos(Long nroTrxRpos) {
		this.nroTrxRpos = nroTrxRpos;
	}
	public Integer getHorasAdministrativas() {
		return horasAdministrativas;
	}
	public void setHorasAdministrativas(Integer horasAdministrativas) {
		this.horasAdministrativas = horasAdministrativas;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getEstadoDes() {
		return estadoDes;
	}
	public void setEstadoDes(String estadoDes) {
		this.estadoDes = estadoDes;
	}
	public String getNcuotas() {
		return ncuotas;
	}
	public void setNcuotas(String ncuotas) {
		this.ncuotas = ncuotas;
	}
	public String getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}
	public String getGlosaBloqueo() {
		return glosaBloqueo;
	}
	public void setGlosaBloqueo(String glosaBloqueo) {
		this.glosaBloqueo = glosaBloqueo;
	}
	public Long getNroTrx() {
		return nroTrx;
	}
	public void setNroTrx(Long nroTrx) {
		this.nroTrx = nroTrx;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Timestamp getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}
	public void setFechaHoraCreacion(Timestamp fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public BigDecimal getMontoVenta() {
		return montoVenta;
	}
	public void setMontoVenta(BigDecimal montoVenta) {
		this.montoVenta = montoVenta;
	}
	public BigDecimal getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(BigDecimal montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public Integer getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(Integer tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	
	public Long getRutComprador() {
		return rutComprador;
	}
	public void setRutComprador(Long rutComprador) {
		this.rutComprador = rutComprador;
	}
	public String getDvComprador() {
		return dvComprador;
	}
	public void setDvComprador(String dvComprador) {
		this.dvComprador = dvComprador;
	}
	public Integer getNumeroSucursal() {
		return numeroSucursal;
	}
	public void setNumeroSucursal(Integer numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}
	public Long getCodigoRegalo() {
		return codigoRegalo;
	}
	public void setCodigoRegalo(Long codigoRegalo) {
		this.codigoRegalo = codigoRegalo;
	}
	public Integer getTipoRegalo() {
		return tipoRegalo;
	}
	public void setTipoRegalo(Integer tipoRegalo) {
		this.tipoRegalo = tipoRegalo;
	}
	public Long getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(Long numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public Timestamp getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(Timestamp fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	public Timestamp getHoraBoleta() {
		return horaBoleta;
	}
	public void setHoraBoleta(Timestamp horaBoleta) {
		this.horaBoleta = horaBoleta;
	}
	public Integer getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(Integer numeroCaja) {
		this.numeroCaja = numeroCaja;
	}
	public Long getCorrelativoBoleta() {
		return correlativoBoleta;
	}
	public void setCorrelativoBoleta(Long correlativoBoleta) {
		this.correlativoBoleta = correlativoBoleta;
	}
	public Integer getRutBoleta() {
		return rutBoleta;
	}
	public void setRutBoleta(Integer rutBoleta) {
		this.rutBoleta = rutBoleta;
	}
	public String getDvBoleta() {
		return dvBoleta;
	}
	public void setDvBoleta(String dvBoleta) {
		this.dvBoleta = dvBoleta;
	}
	public Long getNumNotaCredito() {
		return numNotaCredito;
	}
	public void setNumNotaCredito(Long numNotaCredito) {
		this.numNotaCredito = numNotaCredito;
	}
	public Timestamp getFecNotaCredito() {
		return fecNotaCredito;
	}
	public void setFecNotaCredito(Timestamp fecNotaCredito) {
		this.fecNotaCredito = fecNotaCredito;
	}
	public Timestamp getHoraNotaCredito() {
		return horaNotaCredito;
	}
	public void setHoraNotaCredito(Timestamp horaNotaCredito) {
		this.horaNotaCredito = horaNotaCredito;
	}
	public Integer getNumCajaNotaCredito() {
		return numCajaNotaCredito;
	}
	public void setNumCajaNotaCredito(Integer numCajaNotaCredito) {
		this.numCajaNotaCredito = numCajaNotaCredito;
	}
	public Long getCorrelativoNotaCredito() {
		return correlativoNotaCredito;
	}
	public void setCorrelativoNotaCredito(Long correlativoNotaCredito) {
		this.correlativoNotaCredito = correlativoNotaCredito;
	}
	public Integer getRutNotaCredito() {
		return rutNotaCredito;
	}
	public void setRutNotaCredito(Integer rutNotaCredito) {
		this.rutNotaCredito = rutNotaCredito;
	}
	public String getDvNotaCredito() {
		return dvNotaCredito;
	}
	public void setDvNotaCredito(String dvNotaCredito) {
		this.dvNotaCredito = dvNotaCredito;
	}
	public String getTvnveGlsOreExo() {
		return tvnveGlsOreExo;
	}
	public void setTvnveGlsOreExo(String tvnveGlsOreExo) {
		this.tvnveGlsOreExo = tvnveGlsOreExo;
	}
	public String getTipoPromocion() {
		return tipoPromocion;
	}
	public void setTipoPromocion(String tipoPromocion) {
		this.tipoPromocion = tipoPromocion;
	}
//	public Integer getOptcounter() {
//		return optcounter;
//	}
//	public void setOptcounter(Integer optcounter) {
//		this.optcounter = optcounter;
//	}
	public String getOrigenVta() {
		return origenVta;
	}
	public void setOrigenVta(String origenVta) {
		this.origenVta = origenVta;
	}
	public String getEjecutivoVta() {
		return ejecutivoVta;
	}
	public void setEjecutivoVta(String ejecutivoVta) {
		this.ejecutivoVta = ejecutivoVta;
	}
	public Integer getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Integer tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getFolioSii() {
		return folioSii;
	}
	public void setFolioSii(String folioSii) {
		this.folioSii = folioSii;
	}
	public Long getFolioNcSii() {
		return folioNcSii;
	}
	public void setFolioNcSii(Long folioNcSii) {
		this.folioNcSii = folioNcSii;
	}
	public String getUrlDoce() {
		return urlDoce;
	}
	public void setUrlDoce(String urlDoce) {
		this.urlDoce = urlDoce;
	}
	public String getUrlNotaCredito() {
		return urlNotaCredito;
	}
	public void setUrlNotaCredito(String urlNotaCredito) {
		this.urlNotaCredito = urlNotaCredito;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIndicadorMkp() {
		return indicadorMkp;
	}
	public void setIndicadorMkp(String indicadorMkp) {
		this.indicadorMkp = indicadorMkp;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public Long getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Long rutCliente) {
		this.rutCliente = rutCliente;
	}
/*
	public Integer getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(Integer telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
*/
	
	public String getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
	
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public Integer getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(Integer comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public Integer getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(Integer regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public Integer getMinutosCompra() {
		return minutosCompra;
	}
	public void setMinutosCompra(Integer minutosCompra) {
		this.minutosCompra = minutosCompra;
	}
	public String getDireccionDespacho() {
		return direccionDespacho;
	}
	public void setDireccionDespacho(String direccionDespacho) {
		this.direccionDespacho = direccionDespacho;
	}
//	public String getRuc() {
//		return ruc;
//	}
//	public void setRuc(String ruc) {
//		this.ruc = ruc;
//	}
//	public String getRazonSocial() {
//		return razonSocial;
//	}
//	public void setRazonSocial(String razonSocial) {
//		this.razonSocial = razonSocial;
//	}
	public String getNombreApellido() {
		return nombreApellido;
	}
	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}
//	public BigDecimal getPaypalTipoCambio() {
//		return paypalTipoCambio;
//	}
//	public void setPaypalTipoCambio(BigDecimal paypalTipoCambio) {
//		this.paypalTipoCambio = paypalTipoCambio;
//	}
	public Integer getEnviaDoceFisico() {
		return enviaDoceFisico;
	}
	public void setEnviaDoceFisico(Integer enviaDoceFisico) {
		this.enviaDoceFisico = enviaDoceFisico;
	}
	public String getPcMac() {
		return pcMac;
	}
	public void setPcMac(String pcMac) {
		this.pcMac = pcMac;
	}
	public Integer getSucursalMac() {
		return sucursalMac;
	}
	public void setSucursalMac(Integer sucursalMac) {
		this.sucursalMac = sucursalMac;
	}
	public String getKioskoVendedor() {
		return kioskoVendedor;
	}
	public void setKioskoVendedor(String kioskoVendedor) {
		this.kioskoVendedor = kioskoVendedor;
	}
	public String getKioskoVendedorMac() {
		return kioskoVendedorMac;
	}
	public void setKioskoVendedorMac(String kioskoVendedorMac) {
		this.kioskoVendedorMac = kioskoVendedorMac;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"correlativoVenta\":\"").append(correlativoVenta).append("\",\"fechaCreacion\":\"")
				.append(fechaCreacion).append("\",\"fechaHoraCreacion\":\"").append(fechaHoraCreacion).append("\",\"estado\":\"")
				.append(estado).append("\",\"montoVenta\":\"").append(montoVenta).append("\",\"montoDescuento\":\"")
				.append(montoDescuento).append("\",\"tipoDescuento\":\"").append(tipoDescuento).append("\",\"rutComprador\":\"")
				.append(rutComprador).append("\",\"dvComprador\":\"").append(dvComprador).append("\",\"numeroSucursal\":\"")
				.append(numeroSucursal).append("\",\"codigoRegalo\":\"").append(codigoRegalo).append("\",\"tipoRegalo\":\"")
				.append(tipoRegalo).append("\",\"numeroBoleta\":\"").append(numeroBoleta).append("\",\"fechaBoleta\":\"")
				.append(fechaBoleta).append("\",\"horaBoleta\":\"").append(horaBoleta).append("\",\"numeroCaja\":\"")
				.append(numeroCaja).append("\",\"correlativoBoleta\":\"").append(correlativoBoleta).append("\",\"rutBoleta\":\"")
				.append(rutBoleta).append("\",\"dvBoleta\":\"").append(dvBoleta).append("\",\"numNotaCredito\":\"")
				.append(numNotaCredito).append("\",\"fecNotaCredito\":\"").append(fecNotaCredito).append("\",\"horaNotaCredito\":\"")
				.append(horaNotaCredito).append("\",\"numCajaNotaCredito\":\"").append(numCajaNotaCredito)
				.append("\",\"correlativoNotaCredito\":\"").append(correlativoNotaCredito).append("\",\"rutNotaCredito\":\"")
				.append(rutNotaCredito).append("\",\"dvNotaCredito\":\"").append(dvNotaCredito).append("\",\"tvnveGlsOreExo\":\"")
				.append(tvnveGlsOreExo).append("\",\"tipoPromocion\":\"").append(tipoPromocion).append("\",\"origenVta\":\"")
				.append(origenVta).append("\",\"ejecutivoVta\":\"").append(ejecutivoVta).append("\",\"tipoDoc\":\"").append(tipoDoc)
				.append("\",\"folioSii\":\"").append(folioSii).append("\",\"folioNcSii\":\"").append(folioNcSii).append("\",\"urlDoce\":\"")
				.append(urlDoce).append("\",\"urlNotaCredito\":\"").append(urlNotaCredito).append("\",\"usuario\":\"").append(usuario)
				.append("\",\"indicadorMkp\":\"").append(indicadorMkp).append("\",\"tipoDespacho\":\"").append(tipoDespacho)
				.append("\",\"rutCliente\":\"").append(rutCliente).append("\",\"telefonoCliente\":\"").append(telefonoCliente)
				.append("\",\"emailCliente\":\"").append(emailCliente).append("\",\"comunaDespacho\":\"").append(comunaDespacho)
				.append("\",\"regionDespacho\":\"").append(regionDespacho).append("\",\"direccionDespacho\":\"")
				.append(direccionDespacho).append("\",\"minutosCompra\":\"").append(minutosCompra).append("\",\"nombreApellido\":\"")
				.append(nombreApellido).append("\",\"enviaDoceFisico\":\"").append(enviaDoceFisico).append("\",\"pcMac\":\"")
				.append(pcMac).append("\",\"sucursalMac\":\"").append(sucursalMac).append("\",\"tipoPago\":\"").append(tipoPago)
				.append("\",\"estadoDes\":\"").append(estadoDes).append("\",\"ncuotas\":\"").append(ncuotas).append("\",\"bloqueado\":\"")
				.append(bloqueado).append("\",\"glosaBloqueo\":\"").append(glosaBloqueo).append("\",\"nroTrx\":\"").append(nroTrx)
				.append("\",\"nroTrxRpos\":\"").append(nroTrxRpos).append("\",\"horasAdministrativas\":\"").append(horasAdministrativas)
				.append("\",\"kioskoVendedor\":\"").append(kioskoVendedor).append("\",\"kioskoVendedorMac\":\"").append(kioskoVendedorMac).append("]");
		return builder.toString();
	}

}
