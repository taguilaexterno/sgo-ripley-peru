package cl.ripley.omnicanalidad.bean;

import java.util.List;

public class NotaVentaMail {

	private NotaVenta notaVenta;
	private DatoFacturacion datoFacturacion;
	private Despacho despacho;
	private TipoDoc tipoDoc;
	private Vendedor vendedor;
	private TarjetaBancaria tarjetaBancaria;
	private List<ArticuloVentaTramaDTE> articuloVentas;
	
	public NotaVenta getNotaVenta() {
		return notaVenta;
	}
	public void setNotaVenta(NotaVenta notaVenta) {
		this.notaVenta = notaVenta;
	}
	public DatoFacturacion getDatoFacturacion() {
		return datoFacturacion;
	}
	public void setDatoFacturacion(DatoFacturacion datoFacturacion) {
		this.datoFacturacion = datoFacturacion;
	}
	public Despacho getDespacho() {
		return despacho;
	}
	public void setDespacho(Despacho despacho) {
		this.despacho = despacho;
	}
	public TipoDoc getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(TipoDoc tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public Vendedor getVendedor() {
		return vendedor;
	}
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	public List<ArticuloVentaTramaDTE> getArticuloVentas() {
		return articuloVentas;
	}
	public void setArticuloVentas(List<ArticuloVentaTramaDTE> articuloVentas) {
		this.articuloVentas = articuloVentas;
	}
	public TarjetaBancaria getTarjetaBancaria() {
		return tarjetaBancaria;
	}
	public void setTarjetaBancaria(TarjetaBancaria tarjetaBancaria) {
		this.tarjetaBancaria = tarjetaBancaria;
	}

}
