package cl.ripley.omnicanalidad.bean;

import java.sql.Timestamp;

public class NotaVentaRechazo {
	private Long correlativoVenta;
	private Integer codMotivo;
	private Timestamp fechaBoleta;
	private Integer idValidacion;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getCodMotivo() {
		return codMotivo;
	}
	public void setCodMotivo(Integer codMotivo) {
		this.codMotivo = codMotivo;
	}
	public Timestamp getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(Timestamp fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	public Integer getIdValidacion() {
		return idValidacion;
	}
	public void setIdValidacion(Integer idValidacion) {
		this.idValidacion = idValidacion;
	}

}
