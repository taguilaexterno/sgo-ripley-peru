package cl.ripley.omnicanalidad.bean;

import java.util.List;

public class OrdenesDeCompra {
	private NotaVenta notaVenta;
	private TarjetaRipley tarjetaRipley;
	private TarjetaBancaria tarjetaBancaria;
	private TarjetaRegaloEmpresa tarjetaRegaloEmpresa;
	private DatoFacturacion datoFacturacion;
	private Despacho despacho;
	private TipoDespacho tipoDespacho;
	private TipoDoc tipoDoc;
	private Vendedor vendedor;
	private NotaVentaRechazo notaVentaRechazo;
	private MotivoRechazo motivoRechazo;
	private Validacion validacion;
	private XValidacionOc xValidacionOc;
	private List<ArticulosVentaOC> articulosVenta;
	private List<IdentificadorMarketplace> identificadorMarketplace;
	private DocumentoElectronico documentoElectronico;
	
	public NotaVenta getNotaVenta() {
		return notaVenta;
	}
	public void setNotaVenta(NotaVenta notaVenta) {
		this.notaVenta = notaVenta;
	}
	public TarjetaRipley getTarjetaRipley() {
		return tarjetaRipley;
	}
	public void setTarjetaRipley(TarjetaRipley tarjetaRipley) {
		this.tarjetaRipley = tarjetaRipley;
	}
	public TarjetaBancaria getTarjetaBancaria() {
		return tarjetaBancaria;
	}
	public void setTarjetaBancaria(TarjetaBancaria tarjetaBancaria) {
		this.tarjetaBancaria = tarjetaBancaria;
	}
	public TarjetaRegaloEmpresa getTarjetaRegaloEmpresa() {
		return tarjetaRegaloEmpresa;
	}
	public void setTarjetaRegaloEmpresa(TarjetaRegaloEmpresa tarjetaRegaloEmpresa) {
		this.tarjetaRegaloEmpresa = tarjetaRegaloEmpresa;
	}
	public DatoFacturacion getDatoFacturacion() {
		return datoFacturacion;
	}
	public void setDatoFacturacion(DatoFacturacion datoFacturacion) {
		this.datoFacturacion = datoFacturacion;
	}
	public Despacho getDespacho() {
		return despacho;
	}
	public void setDespacho(Despacho despacho) {
		this.despacho = despacho;
	}
	public TipoDoc getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(TipoDoc tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public Vendedor getVendedor() {
		return vendedor;
	}
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	public NotaVentaRechazo getNotaVentaRechazo() {
		return notaVentaRechazo;
	}
	public void setNotaVentaRechazo(NotaVentaRechazo notaVentaRechazo) {
		this.notaVentaRechazo = notaVentaRechazo;
	}
	public Validacion getValidacion() {
		return validacion;
	}
	public void setValidacion(Validacion validacion) {
		this.validacion = validacion;
	}
	public XValidacionOc getxValidacionOc() {
		return xValidacionOc;
	}
	public void setxValidacionOc(XValidacionOc xValidacionOc) {
		this.xValidacionOc = xValidacionOc;
	}
	public List<ArticulosVentaOC> getArticulosVenta() {
		return articulosVenta;
	}
	public void setArticulosVenta(List<ArticulosVentaOC> articulosVenta) {
		this.articulosVenta = articulosVenta;
	}
	public TipoDespacho getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(TipoDespacho tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public MotivoRechazo getMotivoRechazo() {
		return motivoRechazo;
	}
	public void setMotivoRechazo(MotivoRechazo motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	public List<IdentificadorMarketplace> getIdentificadorMarketplace() {
		return identificadorMarketplace;
	}
	public void setIdentificadorMarketplace(
			List<IdentificadorMarketplace> identificadorMarketplace) {
		this.identificadorMarketplace = identificadorMarketplace;
	}
	public DocumentoElectronico getDocumentoElectronico() {
		return documentoElectronico;
	}
	public void setDocumentoElectronico(DocumentoElectronico documentoElectronico) {
		this.documentoElectronico = documentoElectronico;
	}
	
}
