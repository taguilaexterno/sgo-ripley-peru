package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class OrdenesImpresas {
	
	private BigDecimal montoEfectivo;
	private BigDecimal  cantidadEfectivo;
	
	public BigDecimal getMontoEfectivo() {
		return montoEfectivo;
	}
	public void setMontoEfectivo(BigDecimal montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}
	public BigDecimal getCantidadEfectivo() {
		return cantidadEfectivo;
	}
	public void setCantidadEfectivo(BigDecimal cantidadEfectivo) {
		this.cantidadEfectivo = cantidadEfectivo;
	}
	
	@Override
	public String toString(){
		String salida="";
		salida = salida + "MONTO_EFECTIVO:["+this.getMontoEfectivo()+"]";
		salida = salida + "CANTIDAD_EFECTIVO:["+this.getCantidadEfectivo()+"]";
		return salida;
	}
}
