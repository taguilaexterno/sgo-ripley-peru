package cl.ripley.omnicanalidad.bean;

import java.util.ArrayList;


/**Clase que contiene la lista de parámetros de la aplicación.
 * Estos parámetros se pueden traer desde la base de datos.
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class Parametros {
	
	private ArrayList<Parametro> parametros;	

	public ArrayList<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(ArrayList<Parametro> parametros) {
		this.parametros = parametros;
	}
	public boolean reemplazaValorLista(String nombre, String valor){
		boolean encontrado=false;
		int c=0;		
		while (!encontrado && c < this.parametros.size()){
			if (this.parametros.get(c).getNombre().equalsIgnoreCase(nombre)){
				encontrado=true;
				this.parametros.get(c).setValor(valor);
			}	
			c++;				
		}	
		return encontrado;
	}
	
	
	
	/**Obtiene un valor de parámetro de acuerdo al nombre de este.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param nombre
	 * @return
	 */
	public String buscaValorPorNombre(String nombre){
		
		boolean encontrado=false;
		String resultado="NoEncontrado: "+ nombre;		
		int c=0;			
		
			while (!encontrado && c < this.parametros.size()){
				if (this.parametros.get(c).getNombre().equalsIgnoreCase(nombre)){
					encontrado=true;
					resultado=this.parametros.get(c).getValor();
				}	
				c++;				
			}
		return resultado;
	}
	
	public Parametro buscaParametroPorNombre(String nombre){
		
		boolean encontrado=false;
		Parametro resultado = new Parametro();
		int c=0;			
			while (!encontrado && c < this.parametros.size()){
				if (this.parametros.get(c).getNombre().equalsIgnoreCase(nombre)){
					encontrado=true;
					resultado.setValor(this.parametros.get(c).getValor());
					resultado.setNombre(nombre);
				}	
				c++;				
			}	
		
		return resultado;
		
	}
	
	@Override
	public String toString() {
		String salida = "";
		for (Parametro parametro : parametros) {
			salida = salida + "[Nombre:";
			salida = salida + parametro.getNombre();
			salida = salida + "][Valor:";
			salida = salida + parametro.getValor();
			salida = salida + "]";
		}
		return salida;
	}
	
	

}
