package cl.ripley.omnicanalidad.bean;

public class Personalization {

	private String TAG_NAME;
	private String VALUE;
	
	
	public Personalization(String a, String b){
		this.TAG_NAME = a;
		this.VALUE = b;
		
	}
	
	public String getTagName() {
		return TAG_NAME;
	}
	public String getValue() {
		return VALUE;
	}
	public void setTagName(String tagName) {
		this.TAG_NAME = tagName;
	}
	public void setValue(String value) {
		this.VALUE = value;
	}
	
	
	
	
}
