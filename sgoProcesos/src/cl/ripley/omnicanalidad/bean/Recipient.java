package cl.ripley.omnicanalidad.bean;

import java.util.List;

public class Recipient {

	private String EMAIL;
	private String BODY_TYPE;
	private Personalization PERSONALIZATION;
	private List<Personalization> PERSONALIZATIONS;
	
	public List<Personalization> getPERSONALIZATIONS() {
		return PERSONALIZATIONS;
	}
	public void setPERSONALIZATIONS(List<Personalization> pERSONALIZATIONS) {
		PERSONALIZATIONS = pERSONALIZATIONS;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public String getBODY_TYPE() {
		return BODY_TYPE;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public void setBODY_TYPE(String bODY_TYPE) {
		BODY_TYPE = bODY_TYPE;
	}
	public Personalization getPERSONALIZATION() {
		return PERSONALIZATION;
	}
	public void setPERSONALIZATION(Personalization pERSONALIZATION) {
		PERSONALIZATION = pERSONALIZATION;
	}


	
	
	
	
	
	
	
}
