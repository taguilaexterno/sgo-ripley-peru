package cl.ripley.omnicanalidad.bean;

public class RespuestaSilverpop {
	
	private String codigoError;
	private String descripcionError;
	
	public String getCodigoError() {
		return codigoError;
	}
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	


}
