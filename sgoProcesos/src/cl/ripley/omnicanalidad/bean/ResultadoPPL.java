package cl.ripley.omnicanalidad.bean;

public class ResultadoPPL {

	private int codigo;
	private String mensaje;
	private String ted;
	
	public ResultadoPPL(){}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getTed() {
		return ted;
	}
	public void setTed(String ted) {
		this.ted = ted;
	}
	@Override
	public String toString() {
		return "[Codigo:"+this.codigo+"][Mensaje:"+((this.mensaje != null)?this.mensaje:"null")+"]";
	}
}
