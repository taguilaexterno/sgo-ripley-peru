package cl.ripley.omnicanalidad.bean;

public class RetornoNotaCredito {

	private Integer codigo;
	private String mensaje;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[Codigo][" + this.getCodigo() +"]";
		salida = salida + "[Mensaje][" + this.getMensaje()+"]";

		return salida;
	}
	
	
}
