package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class Saldos {
	
	private BigDecimal saldoTRE;
	private BigDecimal saldoCAR;
	private BigDecimal saldoTotal;
	private BigDecimal montoTarRipley;
	
	public BigDecimal getMontoTarRipley() {
		return montoTarRipley;
	}
	public void setMontoTarRipley(BigDecimal montoTarRipley) {
		this.montoTarRipley = montoTarRipley;
	}
	public BigDecimal getSaldoTRE() {
		return saldoTRE;
	}
	public void setSaldoTRE(BigDecimal saldoTRE) {
		this.saldoTRE = saldoTRE;
	}
	public BigDecimal getSaldoCAR() {
		return saldoCAR;
	}
	public void setSaldoCAR(BigDecimal saldoCAR) {
		this.saldoCAR = saldoCAR;
	}
	public BigDecimal getSaldoTotal() {
		return saldoTotal;
	}
	public void setSaldoTotal(BigDecimal saldoTotal) {
		this.saldoTotal = saldoTotal;
	}
	
	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[saldoTRE][" + this.getSaldoTRE() +"]";
		salida = salida + "[saldoCAR][" + this.getSaldoCAR()+"]";
		salida = salida + "[saldoTotal][" + this.getSaldoTotal()+"]";
		salida = salida + "[montoTarRipley][" + this.getMontoTarRipley()+"]";
		return salida;
	}
	

}
