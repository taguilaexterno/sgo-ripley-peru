package cl.ripley.omnicanalidad.bean;

public class SaveColumns {

	private String COLUMN_NAME;
	
	public SaveColumns(String data){
		this.COLUMN_NAME = data;
	}

	public String getCOLUMN_NAME() {
		return COLUMN_NAME;
	}

	public void setCOLUMN_NAME(String cOLUMN_NAME) {
		COLUMN_NAME = cOLUMN_NAME;
	}
	
}
