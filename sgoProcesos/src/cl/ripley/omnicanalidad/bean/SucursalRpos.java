package cl.ripley.omnicanalidad.bean;

public class SucursalRpos {
	private Integer caja;
	private Integer sucursal;
	private String direccion;
	private String localTienda;
	private String descripcion;
	private String codSunat;
	private String serieSunat;
	private String distrito;
	private String direccionCorta;
	private Integer sucursalPpl;
	private Integer sucursalRpos;
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalTienda() {
		return localTienda;
	}
	public void setLocalTienda(String localTienda) {
		this.localTienda = localTienda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodSunat() {
		return codSunat;
	}
	public void setCodSunat(String codSunat) {
		this.codSunat = codSunat;
	}
	public String getSerieSunat() {
		return serieSunat;
	}
	public void setSerieSunat(String serieSunat) {
		this.serieSunat = serieSunat;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getDireccionCorta() {
		return direccionCorta;
	}
	public void setDireccionCorta(String direccionCorta) {
		this.direccionCorta = direccionCorta;
	}
	public Integer getSucursalPpl() {
		return sucursalPpl;
	}
	public void setSucursalPpl(Integer sucursalPpl) {
		this.sucursalPpl = sucursalPpl;
	}
	public Integer getSucursalRpos() {
		return sucursalRpos;
	}
	public void setSucursalRpos(Integer sucursalRpos) {
		this.sucursalRpos = sucursalRpos;
	}
	public Integer getCaja() {
		return caja;
	}
	public void setCaja(Integer caja) {
		this.caja = caja;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SucursalRpos [caja=").append(caja).append(", sucursal=").append(sucursal).append(", direccion=")
				.append(direccion).append(", localTienda=").append(localTienda).append(", descripcion=")
				.append(descripcion).append(", codSunat=").append(codSunat).append(", serieSunat=").append(serieSunat)
				.append(", distrito=").append(distrito).append(", direccionCorta=").append(direccionCorta)
				.append(", sucursalPpl=").append(sucursalPpl).append(", sucursalRpos=").append(sucursalRpos)
				.append("]");
		return builder.toString();
	}
	
	
}
