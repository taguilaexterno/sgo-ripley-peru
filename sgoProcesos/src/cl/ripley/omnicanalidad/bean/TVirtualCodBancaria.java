package cl.ripley.omnicanalidad.bean;

public class TVirtualCodBancaria {

	private Integer codInter;
	private Integer codBoleta;
	private Integer codBack;
	private String descripcion;
	
	public Integer getCodInter() {
		return codInter;
	}
	public void setCodInter(Integer codInter) {
		this.codInter = codInter;
	}
	public Integer getCodBoleta() {
		return codBoleta;
	}
	public void setCodBoleta(Integer codBoleta) {
		this.codBoleta = codBoleta;
	}
	public Integer getCodBack() {
		return codBack;
	}
	public void setCodBack(Integer codBack) {
		this.codBack = codBack;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		return "[codInter: "+codInter+"][codBoleta: "+codBoleta+"][codBack: "+codBack+"][descripcion: "+descripcion+"]";
	}

}
