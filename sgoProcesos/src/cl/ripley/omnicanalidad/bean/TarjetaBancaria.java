package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TarjetaBancaria implements Serializable{
	private Long	correlativoVenta;
	private Integer	tipoTarjeta;
	private String	tipoTarjetaStr;
	private String	codigoAutorizador;
	private BigDecimal	montoTarjeta;
	private Integer	optcounter;
	private String	vd;
	private String	medioAcceso;
	private String	oneclickBuyorder;
	private Integer codigoConvenio;
	private Long idCanal;
	private String descripcionCanal;
	private String binNumber;
	private String ultimosDigitosTarjeta;
	private String diferido;
	private Long idFormaPago;
	private String descripcionFormaPago;
	private Integer plazo;
	private String glosa;
	private Long identificadorTransaccion;
	
	public String getTipoTarjetaStr() {
		return tipoTarjetaStr;
	}
	public void setTipoTarjetaStr(String tipoTarjetaStr) {
		this.tipoTarjetaStr = tipoTarjetaStr;
	}
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(Integer tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getCodigoAutorizador() {
		return codigoAutorizador;
	}
	public void setCodigoAutorizador(String codigoAutorizador) {
		this.codigoAutorizador = codigoAutorizador;
	}
	public BigDecimal getMontoTarjeta() {
		return montoTarjeta;
	}
	public void setMontoTarjeta(BigDecimal montoTarjeta) {
		this.montoTarjeta = montoTarjeta;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}
	public String getVd() {
		return vd;
	}
	public void setVd(String vd) {
		this.vd = vd;
	}
	public String getMedioAcceso() {
		return medioAcceso;
	}
	public void setMedioAcceso(String medioAcceso) {
		this.medioAcceso = medioAcceso;
	}
	public String getOneclickBuyorder() {
		return oneclickBuyorder;
	}
	public void setOneclickBuyorder(String oneclickBuyorder) {
		this.oneclickBuyorder = oneclickBuyorder;
	}
	public Integer getCodigoConvenio() {
		return codigoConvenio;
	}
	public void setCodigoConvenio(Integer codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}
	public Long getIdCanal() {
		return idCanal;
	}
	public void setIdCanal(Long idCanal) {
		this.idCanal = idCanal;
	}
	public String getDescripcionCanal() {
		return descripcionCanal;
	}
	public void setDescripcionCanal(String descripcionCanal) {
		this.descripcionCanal = descripcionCanal;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public String getUltimosDigitosTarjeta() {
		return ultimosDigitosTarjeta;
	}
	public void setUltimosDigitosTarjeta(String ultimosDigitosTarjeta) {
		this.ultimosDigitosTarjeta = ultimosDigitosTarjeta;
	}
	public String getDiferido() {
		return diferido;
	}
	public void setDiferido(String diferido) {
		this.diferido = diferido;
	}
	public Long getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}
	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public Long getIdentificadorTransaccion() {
		return identificadorTransaccion;
	}
	public void setIdentificadorTransaccion(Long identificadorTransaccion) {
		this.identificadorTransaccion = identificadorTransaccion;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TarjetaBancaria [correlativoVenta=");
		builder.append(correlativoVenta);
		builder.append(", tipoTarjeta=");
		builder.append(tipoTarjeta);
		builder.append(", tipoTarjetaStr=");
		builder.append(tipoTarjetaStr);
		builder.append(", codigoAutorizador=");
		builder.append(codigoAutorizador);
		builder.append(", montoTarjeta=");
		builder.append(montoTarjeta);
		builder.append(", optcounter=");
		builder.append(optcounter);
		builder.append(", vd=");
		builder.append(vd);
		builder.append(", medioAcceso=");
		builder.append(medioAcceso);
		builder.append(", oneclickBuyorder=");
		builder.append(oneclickBuyorder);
		builder.append(", codigoConvenio=");
		builder.append(codigoConvenio);
		builder.append(", idCanal=");
		builder.append(idCanal);
		builder.append(", descripcionCanal=");
		builder.append(descripcionCanal);
		builder.append(", binNumber=");
		builder.append(binNumber);
		builder.append(", ultimosDigitosTarjeta=");
		builder.append(ultimosDigitosTarjeta);
		builder.append(", diferido=");
		builder.append(diferido);
		builder.append(", idFormaPago=");
		builder.append(idFormaPago);
		builder.append(", descripcionFormaPago=");
		builder.append(descripcionFormaPago);
		builder.append(", plazo=");
		builder.append(plazo);
		builder.append(", glosa=");
		builder.append(glosa);
		builder.append(", identificadorTransaccion=");
		builder.append(identificadorTransaccion);
		builder.append("]");
		return builder.toString();
	}
}
