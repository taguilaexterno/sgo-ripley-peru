package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TarjetaRegaloEmpresa implements Serializable{
	private Long	correlativoVenta;
	private Integer	administradora;
	private Integer	emisor;
	private Integer	tarjeta;
	private Integer	codigoAutorizador;
	private Integer	rutCliente;
	private String	dvCliente;
	private BigDecimal	monto;
	private Integer	nroTarjeta;
	private Integer	flag;
	private Integer	optcounter;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getAdministradora() {
		return administradora;
	}
	public void setAdministradora(Integer administradora) {
		this.administradora = administradora;
	}
	public Integer getEmisor() {
		return emisor;
	}
	public void setEmisor(Integer emisor) {
		this.emisor = emisor;
	}
	public Integer getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(Integer tarjeta) {
		this.tarjeta = tarjeta;
	}
	public Integer getCodigoAutorizador() {
		return codigoAutorizador;
	}
	public void setCodigoAutorizador(Integer codigoAutorizador) {
		this.codigoAutorizador = codigoAutorizador;
	}
	public Integer getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Integer rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getDvCliente() {
		return dvCliente;
	}
	public void setDvCliente(String dvCliente) {
		this.dvCliente = dvCliente;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Integer getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(Integer nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}

}
