package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

public class TarjetaRipley implements Serializable {
	private Long		correlativoVenta;
	private Integer		administradora;
	private Integer		emisor;
	private Integer		tarjeta;
	private Integer		rutTitular;
	private Integer		rutPoder;
	private Integer		plazo;
	private Integer		diferido;
	private BigDecimal		montoCapital;
	private BigDecimal		montoPie;
	private BigDecimal		descuentoCar;
	private Integer		codigoDescuento;
	private Integer		prefijoTitular;
	private String		dvTitular;
	private Integer		tipoCliente;
	private Integer		tipoCredito;
	private Integer		prefijoPoder;
	private String		dvPoder;
	private BigDecimal		valorCuota;
	private Timestamp	fechaPrimerVencto;
	private Integer		sernacFinanCae;
	private Integer		sernacFinanCt;
	private Integer		tvtriMntMntFnd;
	private String		tvtriGlsPrjTsaInt;
	private String		tvtriGlsPrjTsaEfe;
	private Integer		tvtriCodImpTsaEfe;
	private Integer		tvtriMntMntEva;
	private String		glosaFinancieraGsic;
	private BigInteger	pan;
	private Integer		optcounter;
	private Integer		codigoCore;
	private BigInteger	codigoAutorizacion;
	private Integer		cantidadCuotas;
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public Integer getAdministradora() {
		return administradora;
	}
	public void setAdministradora(Integer administradora) {
		this.administradora = administradora;
	}
	public Integer getEmisor() {
		return emisor;
	}
	public void setEmisor(Integer emisor) {
		this.emisor = emisor;
	}
	public Integer getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(Integer tarjeta) {
		this.tarjeta = tarjeta;
	}
	public Integer getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(Integer rutTitular) {
		this.rutTitular = rutTitular;
	}
	public Integer getRutPoder() {
		return rutPoder;
	}
	public void setRutPoder(Integer rutPoder) {
		this.rutPoder = rutPoder;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public Integer getDiferido() {
		return diferido;
	}
	public void setDiferido(Integer diferido) {
		this.diferido = diferido;
	}
	public BigDecimal getMontoCapital() {
		return montoCapital;
	}
	public void setMontoCapital(BigDecimal montoCapital) {
		this.montoCapital = montoCapital;
	}
	public BigDecimal getMontoPie() {
		return montoPie;
	}
	public void setMontoPie(BigDecimal montoPie) {
		this.montoPie = montoPie;
	}
	public BigDecimal getDescuentoCar() {
		return descuentoCar;
	}
	public void setDescuentoCar(BigDecimal descuentoCar) {
		this.descuentoCar = descuentoCar;
	}
	public Integer getCodigoDescuento() {
		return codigoDescuento;
	}
	public void setCodigoDescuento(Integer codigoDescuento) {
		this.codigoDescuento = codigoDescuento;
	}
	public Integer getPrefijoTitular() {
		return prefijoTitular;
	}
	public void setPrefijoTitular(Integer prefijoTitular) {
		this.prefijoTitular = prefijoTitular;
	}
	public String getDvTitular() {
		return dvTitular;
	}
	public void setDvTitular(String dvTitular) {
		this.dvTitular = dvTitular;
	}
	public Integer getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public Integer getTipoCredito() {
		return tipoCredito;
	}
	public void setTipoCredito(Integer tipoCredito) {
		this.tipoCredito = tipoCredito;
	}
	public Integer getPrefijoPoder() {
		return prefijoPoder;
	}
	public void setPrefijoPoder(Integer prefijoPoder) {
		this.prefijoPoder = prefijoPoder;
	}
	public String getDvPoder() {
		return dvPoder;
	}
	public void setDvPoder(String dvPoder) {
		this.dvPoder = dvPoder;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public Timestamp getFechaPrimerVencto() {
		return fechaPrimerVencto;
	}
	public void setFechaPrimerVencto(Timestamp fechaPrimerVencto) {
		this.fechaPrimerVencto = fechaPrimerVencto;
	}
	public Integer getSernacFinanCae() {
		return sernacFinanCae;
	}
	public void setSernacFinanCae(Integer sernacFinanCae) {
		this.sernacFinanCae = sernacFinanCae;
	}
	public Integer getSernacFinanCt() {
		return sernacFinanCt;
	}
	public void setSernacFinanCt(Integer sernacFinanCt) {
		this.sernacFinanCt = sernacFinanCt;
	}
	public Integer getTvtriMntMntFnd() {
		return tvtriMntMntFnd;
	}
	public void setTvtriMntMntFnd(Integer tvtriMntMntFnd) {
		this.tvtriMntMntFnd = tvtriMntMntFnd;
	}
	public String getTvtriGlsPrjTsaInt() {
		return tvtriGlsPrjTsaInt;
	}
	public void setTvtriGlsPrjTsaInt(String tvtriGlsPrjTsaInt) {
		this.tvtriGlsPrjTsaInt = tvtriGlsPrjTsaInt;
	}
	public String getTvtriGlsPrjTsaEfe() {
		return tvtriGlsPrjTsaEfe;
	}
	public void setTvtriGlsPrjTsaEfe(String tvtriGlsPrjTsaEfe) {
		this.tvtriGlsPrjTsaEfe = tvtriGlsPrjTsaEfe;
	}
	public Integer getTvtriCodImpTsaEfe() {
		return tvtriCodImpTsaEfe;
	}
	public void setTvtriCodImpTsaEfe(Integer tvtriCodImpTsaEfe) {
		this.tvtriCodImpTsaEfe = tvtriCodImpTsaEfe;
	}
	public Integer getTvtriMntMntEva() {
		return tvtriMntMntEva;
	}
	public void setTvtriMntMntEva(Integer tvtriMntMntEva) {
		this.tvtriMntMntEva = tvtriMntMntEva;
	}
	public String getGlosaFinancieraGsic() {
		return glosaFinancieraGsic;
	}
	public void setGlosaFinancieraGsic(String glosaFinancieraGsic) {
		this.glosaFinancieraGsic = glosaFinancieraGsic;
	}
	public BigInteger getPan() {
		return pan;
	}
	public void setPan(BigInteger pan) {
		this.pan = pan;
	}
	public Integer getOptcounter() {
		return optcounter;
	}
	public void setOptcounter(Integer optcounter) {
		this.optcounter = optcounter;
	}
	public Integer getCodigoCore() {
		return codigoCore;
	}
	public void setCodigoCore(Integer codigoCore) {
		this.codigoCore = codigoCore;
	}
	public BigInteger getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(BigInteger codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public Integer getCantidadCuotas() {
		return cantidadCuotas;
	}
	public void setCantidadCuotas(Integer cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TarjetaRipley [correlativoVenta=").append(correlativoVenta).append(", administradora=")
				.append(administradora).append(", emisor=").append(emisor).append(", tarjeta=").append(tarjeta)
				.append(", rutTitular=").append(rutTitular).append(", rutPoder=").append(rutPoder).append(", plazo=")
				.append(plazo).append(", diferido=").append(diferido).append(", montoCapital=").append(montoCapital)
				.append(", montoPie=").append(montoPie).append(", descuentoCar=").append(descuentoCar)
				.append(", codigoDescuento=").append(codigoDescuento).append(", prefijoTitular=").append(prefijoTitular)
				.append(", dvTitular=").append(dvTitular).append(", tipoCliente=").append(tipoCliente)
				.append(", tipoCredito=").append(tipoCredito).append(", prefijoPoder=").append(prefijoPoder)
				.append(", dvPoder=").append(dvPoder).append(", valorCuota=").append(valorCuota)
				.append(", fechaPrimerVencto=").append(fechaPrimerVencto).append(", sernacFinanCae=")
				.append(sernacFinanCae).append(", sernacFinanCt=").append(sernacFinanCt).append(", tvtriMntMntFnd=")
				.append(tvtriMntMntFnd).append(", tvtriGlsPrjTsaInt=").append(tvtriGlsPrjTsaInt)
				.append(", tvtriGlsPrjTsaEfe=").append(tvtriGlsPrjTsaEfe).append(", tvtriCodImpTsaEfe=")
				.append(tvtriCodImpTsaEfe).append(", tvtriMntMntEva=").append(tvtriMntMntEva)
				.append(", glosaFinancieraGsic=").append(glosaFinancieraGsic).append(", pan=").append(pan)
				.append(", optcounter=").append(optcounter).append(", codigoCore=").append(codigoCore)
				.append(", codigoAutorizacion=").append(codigoAutorizacion).append(", cantidadCuotas=")
				.append(cantidadCuotas).append("]");
		return builder.toString();
	}

}
