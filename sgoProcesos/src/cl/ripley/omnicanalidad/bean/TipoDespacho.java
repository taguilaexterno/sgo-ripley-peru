package cl.ripley.omnicanalidad.bean;

public class TipoDespacho {

	private Integer	tipDespachoId;
	private String	descTipDespacho;
	private String	nomTipoDespacho;
	private String	glosaDespacho;
	
	public Integer getTipDespachoId() {
		return tipDespachoId;
	}
	public void setTipDespachoId(Integer tipDespachoId) {
		this.tipDespachoId = tipDespachoId;
	}
	public String getDescTipDespacho() {
		return descTipDespacho;
	}
	public void setDescTipDespacho(String descTipDespacho) {
		this.descTipDespacho = descTipDespacho;
	}
	public String getNomTipoDespacho() {
		return nomTipoDespacho;
	}
	public void setNomTipoDespacho(String nomTipoDespacho) {
		this.nomTipoDespacho = nomTipoDespacho;
	}
	public String getGlosaDespacho() {
		return glosaDespacho;
	}
	public void setGlosaDespacho(String glosaDespacho) {
		this.glosaDespacho = glosaDespacho;
	}
}
