package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;

public class TipoDoc implements Serializable{
	private Integer	tipoDoc;
	private String	descTipodoc;
	private Integer	docOrigen;
	private Integer	codBo;
	private Integer	codPpl;
	private String	subTipoDoc;
	
	public Integer getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Integer tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getDescTipodoc() {
		return descTipodoc;
	}
	public void setDescTipodoc(String descTipodoc) {
		this.descTipodoc = descTipodoc;
	}
	public Integer getDocOrigen() {
		return docOrigen;
	}
	public void setDocOrigen(Integer docOrigen) {
		this.docOrigen = docOrigen;
	}
	public Integer getCodBo() {
		return codBo;
	}
	public void setCodBo(Integer codBo) {
		this.codBo = codBo;
	}
	public Integer getCodPpl() {
		return codPpl;
	}
	public void setCodPpl(Integer codPpl) {
		this.codPpl = codPpl;
	}
	public String getSubTipoDoc() {
		return subTipoDoc;
	}
	public void setSubTipoDoc(String subTipoDoc) {
		this.subTipoDoc = subTipoDoc;
	}

}
