package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class TotalCuadratura {
	
	public BigDecimal unidadesImp;
	public BigDecimal recaudaciones;
	public BigDecimal boletasFacturas;
	public BigDecimal pagoEnTienda;
	
	
	
	public BigDecimal getUnidadesImp() {
		return unidadesImp;
	}



	public void setUnidadesImp(BigDecimal unidadesImp) {
		this.unidadesImp = unidadesImp;
	}



	public BigDecimal getRecaudaciones() {
		return recaudaciones;
	}



	public void setRecaudaciones(BigDecimal recaudaciones) {
		this.recaudaciones = recaudaciones;
	}



	public BigDecimal getBoletasFacturas() {
		return boletasFacturas;
	}



	public void setBoletasFacturas(BigDecimal boletasFacturas) {
		this.boletasFacturas = boletasFacturas;
	}



	public BigDecimal getPagoEnTienda() {
		return pagoEnTienda;
	}



	public void setPagoEnTienda(BigDecimal pagoEnTienda) {
		this.pagoEnTienda = pagoEnTienda;
	}



	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[Total unidadesImp][" + this.getUnidadesImp() +"]";
		salida = salida + "[Total recaudaciones][" + this.getRecaudaciones()+"]";
		salida = salida + "[Total boletasFacturas][" + this.getBoletasFacturas()+"]";	
		salida = salida + "[Total pagoEnTienda][" + this.getBoletasFacturas()+"]";	
		return salida;
	}
	
	

}
