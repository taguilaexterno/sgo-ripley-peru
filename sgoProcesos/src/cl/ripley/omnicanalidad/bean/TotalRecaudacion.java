package cl.ripley.omnicanalidad.bean;


public class TotalRecaudacion {
	private long totalRecaudacion;
	private int cantidadRecaudacion;
	private long totalDTE;
	
	public TotalRecaudacion(){}
	
	public long getTotalDTE() {
		return totalDTE;
	}
	public void setTotalDTE(long totalDTE) {
		this.totalDTE = totalDTE;
	}
	public long getTotalRecaudacion() {
		return totalRecaudacion;
	}
	public void setTotalRecaudacion(long totalRecaudacion) {
		this.totalRecaudacion = totalRecaudacion;
	}
	public int getCantidadRecaudacion() {
		return cantidadRecaudacion;
	}
	public void setCantidadRecaudacion(int cantidadRecaudacion) {
		this.cantidadRecaudacion = cantidadRecaudacion;
	}
	
	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[Total Recaudacion][" + this.getTotalRecaudacion() +"]";
		salida = salida + "[Total Cantidad Recaudacion][" + this.getCantidadRecaudacion() +"]";
		salida = salida + "[Total DTE][" + this.getTotalDTE() +"]";		
		return salida;
	}
	
	

}
