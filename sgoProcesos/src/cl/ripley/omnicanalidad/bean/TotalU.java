package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class TotalU {
	
	private BigDecimal totalUCar;
	private BigDecimal totalUTre;
	private BigDecimal totalUBanc;
	
	public BigDecimal getTotalUCar() {
		return totalUCar;
	}
	public void setTotalUCar(BigDecimal totalUCar) {
		this.totalUCar = totalUCar;
	}
	public BigDecimal getTotalUTre() {
		return totalUTre;
	}
	public void setTotalUTre(BigDecimal totalUTre) {
		this.totalUTre = totalUTre;
	}
	public BigDecimal getTotalUBanc() {
		return totalUBanc;
	}
	public void setTotalUBanc(BigDecimal totalUBanc) {
		this.totalUBanc = totalUBanc;
	}
	
	@Override
	public String toString(){
		String salida="";
		salida = salida + "VENTACARU:["+this.getTotalUCar()+"]";
		salida = salida + "VENTABANU:["+this.getTotalUBanc()+"]";
		salida = salida + "VENTATREU:["+this.getTotalUTre()+"]";
		return salida;
	}
	

}
