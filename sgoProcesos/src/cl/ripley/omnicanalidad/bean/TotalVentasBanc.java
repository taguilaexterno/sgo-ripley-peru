package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class TotalVentasBanc {
	
	private BigDecimal cantidadVentasBanc;
	private BigDecimal totalVentasBanc;
		
	public BigDecimal getCantidadVentasBanc() {
		return cantidadVentasBanc;
	}
	public void setCantidadVentasBanc(BigDecimal cantidadVentasBanc) {
		this.cantidadVentasBanc = cantidadVentasBanc;
	}
	public BigDecimal getTotalVentasBanc() {
		return totalVentasBanc;
	}
	public void setTotalVentasBanc(BigDecimal totalVentasBanc) {
		this.totalVentasBanc = totalVentasBanc;
	}
	
	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[Total Ventas Bancarias][" + this.getTotalVentasBanc() +"]";
		salida = salida + "[Total Cantidad Ventas Bancarias][" + this.getCantidadVentasBanc()+"]";		
		return salida;
	}

}
