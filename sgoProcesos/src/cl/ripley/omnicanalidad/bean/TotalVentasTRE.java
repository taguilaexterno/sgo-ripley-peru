package cl.ripley.omnicanalidad.bean;


public class TotalVentasTRE {
	
	private long wTotalVentasTRE;
	private long wTotalVentasTRECOM;
	private int cantidadVentasTRE;
	
	public long getwTotalVentasTRE() {
		return wTotalVentasTRE;
	}
	public void setwTotalVentasTRE(long wTotalVentasTRE) {
		this.wTotalVentasTRE = wTotalVentasTRE;
	}
	public long getwTotalVentasTRECOM() {
		return wTotalVentasTRECOM;
	}
	public void setwTotalVentasTRECOM(long wTotalVentasTRECOM) {
		this.wTotalVentasTRECOM = wTotalVentasTRECOM;
	}
	public int getCantidadVentasTRE() {
		return cantidadVentasTRE;
	}
	public void setCantidadVentasTRE(int cantidadVentasTRE) {
		this.cantidadVentasTRE = cantidadVentasTRE;
	}

	@Override
	public String toString() {
		String salida = "";
		salida = salida + "[Cantidad Ventas TRE][" + this.getCantidadVentasTRE() +"]";
		salida = salida + "[Total Ventas TRE][" + this.getwTotalVentasTRE()+"]";
		salida = salida + "[Total Ventas TRECOM][" + this.getwTotalVentasTRECOM()+"]";
		return salida;
	}
	
	
	

	

}
