package cl.ripley.omnicanalidad.bean;

import java.math.BigDecimal;

public class TotalesCierre {
	
	private BigDecimal totalCantVentas;
	private BigDecimal totalDTE;
	private BigDecimal totalVenta;
	private BigDecimal wUnidades;
	private BigDecimal montoEfectivo;
	private BigDecimal cantidadEfectivo;
	private BigDecimal totalMontoReca;
	private BigDecimal ventasCar;
	private BigDecimal totalU;
	
	
	//Informe Cierre Boletas
	private BigDecimal totalCantDTE;
	private BigDecimal totalMontoDTE;
	private BigDecimal totalCantCAR;
	private BigDecimal totalMontoCAR;
	private BigDecimal totalCantTRE;
	private BigDecimal totalMontoTRE;
	
	//Informe Cierre NC
	private BigDecimal ncAnuladas;
	
	

	
	public BigDecimal getTotalCantVentas() {
		return totalCantVentas;
	}




	public void setTotalCantVentas(BigDecimal totalCantVentas) {
		this.totalCantVentas = totalCantVentas;
	}




	public BigDecimal getTotalDTE() {
		return totalDTE;
	}




	public void setTotalDTE(BigDecimal totalDTE) {
		this.totalDTE = totalDTE;
	}




	public BigDecimal getTotalVenta() {
		return totalVenta;
	}




	public void setTotalVenta(BigDecimal totalVenta) {
		this.totalVenta = totalVenta;
	}




	public BigDecimal getwUnidades() {
		return wUnidades;
	}




	public void setwUnidades(BigDecimal wUnidades) {
		this.wUnidades = wUnidades;
	}




	public BigDecimal getMontoEfectivo() {
		return montoEfectivo;
	}




	public void setMontoEfectivo(BigDecimal montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}




	public BigDecimal getCantidadEfectivo() {
		return cantidadEfectivo;
	}




	public void setCantidadEfectivo(BigDecimal cantidadEfectivo) {
		this.cantidadEfectivo = cantidadEfectivo;
	}




	public BigDecimal getTotalMontoReca() {
		return totalMontoReca;
	}




	public void setTotalMontoReca(BigDecimal totalMontoReca) {
		this.totalMontoReca = totalMontoReca;
	}




	public BigDecimal getVentasCar() {
		return ventasCar;
	}




	public void setVentasCar(BigDecimal ventasCar) {
		this.ventasCar = ventasCar;
	}




	public BigDecimal getTotalU() {
		return totalU;
	}




	public void setTotalU(BigDecimal totalU) {
		this.totalU = totalU;
	}




	public BigDecimal getTotalCantDTE() {
		return totalCantDTE;
	}




	public void setTotalCantDTE(BigDecimal totalCantDTE) {
		this.totalCantDTE = totalCantDTE;
	}




	public BigDecimal getTotalMontoDTE() {
		return totalMontoDTE;
	}




	public void setTotalMontoDTE(BigDecimal totalMontoDTE) {
		this.totalMontoDTE = totalMontoDTE;
	}




	public BigDecimal getTotalCantCAR() {
		return totalCantCAR;
	}




	public void setTotalCantCAR(BigDecimal totalCantCAR) {
		this.totalCantCAR = totalCantCAR;
	}




	public BigDecimal getTotalMontoCAR() {
		return totalMontoCAR;
	}




	public void setTotalMontoCAR(BigDecimal totalMontoCAR) {
		this.totalMontoCAR = totalMontoCAR;
	}




	public BigDecimal getTotalCantTRE() {
		return totalCantTRE;
	}




	public void setTotalCantTRE(BigDecimal totalCantTRE) {
		this.totalCantTRE = totalCantTRE;
	}




	public BigDecimal getTotalMontoTRE() {
		return totalMontoTRE;
	}




	public void setTotalMontoTRE(BigDecimal totalMontoTRE) {
		this.totalMontoTRE = totalMontoTRE;
	}




	public BigDecimal getNcAnuladas() {
		return ncAnuladas;
	}




	public void setNcAnuladas(BigDecimal ncAnuladas) {
		this.ncAnuladas = ncAnuladas;
	}




	@Override
	public String toString(){
		String salida="";
		salida = salida + "TOTAL CANT VENTAS:["+this.getTotalCantVentas()+"]";
		salida = salida + "TOTAL DTE["+this.getTotalDTE()+"]";
		salida = salida + "TOTAL VENTA["+this.getTotalVenta()+"]";
		salida = salida + "TOTAL WUNIDADES["+this.getwUnidades()+"]";
		salida = salida + "MONTO EFECTIVO["+this.getMontoEfectivo()+"]";
		salida = salida + "TOTAL MONTO RECA ["+this.getTotalMontoReca()+"]";
		return salida;
	}
	
	
}
