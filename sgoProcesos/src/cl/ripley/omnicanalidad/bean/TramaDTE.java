package cl.ripley.omnicanalidad.bean;

import com.ripley.restservices.model.pagoTiendaRest.PagoEnTienda;
import java.io.Serializable;
import java.util.List;

public class TramaDTE implements Serializable {


	private NotaVenta notaVenta;
	private TarjetaRipley tarjetaRipley;
	private TarjetaBancaria tarjetaBancaria;
	private TarjetaRegaloEmpresa tarjetaRegaloEmpresa;
	private DatoFacturacion datoFacturacion;
	private Despacho despacho;
	private TipoDoc tipoDoc;
	private Vendedor vendedor;
	private List<ArticuloVentaTramaDTE> articuloVentas;
	private List<IdentificadorMarketplace> identificadoresMarketplace;
	private PagoEnTienda pagoEnTienda;
	
	public NotaVenta getNotaVenta() {
		return notaVenta;
	}
	public void setNotaVenta(NotaVenta notaVenta) {
		this.notaVenta = notaVenta;
	}
	public TarjetaRipley getTarjetaRipley() {
		return tarjetaRipley;
	}
	public void setTarjetaRipley(TarjetaRipley tarjetaRipley) {
		this.tarjetaRipley = tarjetaRipley;
	}
	public TarjetaBancaria getTarjetaBancaria() {
		return tarjetaBancaria;
	}
	public void setTarjetaBancaria(TarjetaBancaria tarjetaBancaria) {
		this.tarjetaBancaria = tarjetaBancaria;
	}
	public TarjetaRegaloEmpresa getTarjetaRegaloEmpresa() {
		return tarjetaRegaloEmpresa;
	}
	public void setTarjetaRegaloEmpresa(TarjetaRegaloEmpresa tarjetaRegaloEmpresa) {
		this.tarjetaRegaloEmpresa = tarjetaRegaloEmpresa;
	}
	public DatoFacturacion getDatoFacturacion() {
		return datoFacturacion;
	}
	public void setDatoFacturacion(DatoFacturacion datoFacturacion) {
		this.datoFacturacion = datoFacturacion;
	}
	public Despacho getDespacho() {
		return despacho;
	}
	public void setDespacho(Despacho despacho) {
		this.despacho = despacho;
	}
	public TipoDoc getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(TipoDoc tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public List<ArticuloVentaTramaDTE> getArticuloVentas() {
		return articuloVentas;
	}
	public void setArticuloVentas(List<ArticuloVentaTramaDTE> articuloVentas) {
		this.articuloVentas = articuloVentas;
	}
	public Vendedor getVendedor() {
		return vendedor;
	}
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	public List<IdentificadorMarketplace> getIdentificadoresMarketplace() {
		return identificadoresMarketplace;
	}
	public void setIdentificadoresMarketplace(
			List<IdentificadorMarketplace> identificadoresMarketplace) {
		this.identificadoresMarketplace = identificadoresMarketplace;
	}
	public PagoEnTienda getPagoEnTienda() {
		return pagoEnTienda;
	}
	public void setPagoEnTienda(PagoEnTienda pagoEnTienda) {
		this.pagoEnTienda = pagoEnTienda;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TramaDTE [notaVenta=").append(notaVenta).append(", tarjetaRipley=").append(tarjetaRipley)
				.append(", tarjetaBancaria=").append(tarjetaBancaria).append(", tarjetaRegaloEmpresa=")
				.append(tarjetaRegaloEmpresa).append(", datoFacturacion=").append(datoFacturacion).append(", despacho=")
				.append(despacho).append(", tipoDoc=").append(tipoDoc).append(", vendedor=").append(vendedor)
				.append(", articuloVentas=").append(articuloVentas).append(", identificadoresMarketplace=")
				.append(identificadoresMarketplace).append(", pagoEnTienda=").append(pagoEnTienda).append("]");
		return builder.toString();
	}

}
