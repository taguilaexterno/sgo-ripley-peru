package cl.ripley.omnicanalidad.bean;

import java.io.Serializable;

public class Vendedor implements Serializable{
	private String rutVendedor;
	private String nombreVendedor;
	
	public String getRutVendedor() {
		return rutVendedor;
	}
	public void setRutVendedor(String rutVendedor) {
		this.rutVendedor = rutVendedor;
	}
	public String getNombreVendedor() {
		return nombreVendedor;
	}
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

}
