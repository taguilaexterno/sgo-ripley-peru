package cl.ripley.omnicanalidad.bean;

public class XValidacionOc {
	private Integer idValidacion;
	private String mensajeError;
	private String queryValidacion;
	private String valorOkErr;
	private Integer queryActivo;
	private Integer estadoFinalOc;
	
	public Integer getIdValidacion() {
		return idValidacion;
	}
	public void setIdValidacion(Integer idValidacion) {
		this.idValidacion = idValidacion;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getQueryValidacion() {
		return queryValidacion;
	}
	public void setQueryValidacion(String queryValidacion) {
		this.queryValidacion = queryValidacion;
	}
	public String getValorOkErr() {
		return valorOkErr;
	}
	public void setValorOkErr(String valorOkErr) {
		this.valorOkErr = valorOkErr;
	}
	public Integer getQueryActivo() {
		return queryActivo;
	}
	public void setQueryActivo(Integer queryActivo) {
		this.queryActivo = queryActivo;
	}
	public Integer getEstadoFinalOc() {
		return estadoFinalOc;
	}
	public void setEstadoFinalOc(Integer estadoFinalOc) {
		this.estadoFinalOc = estadoFinalOc;
	}
}
