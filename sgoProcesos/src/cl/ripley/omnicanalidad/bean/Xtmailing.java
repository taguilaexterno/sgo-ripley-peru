package cl.ripley.omnicanalidad.bean;

import java.util.List;

public class Xtmailing {
	
	private String CAMPAIGN_ID;
	private String TRANSACTION_ID;
	private String SHOW_ALL_SEND_DETAIL;
	private String SEND_AS_BATCH;
	private String NO_RETRY_ON_FAILURE;
	private List<SaveColumns> SAVE_COLUMN;
	private List<String> SAVE_COLUMNS;
	private Recipient RECIPIENT;
	
		

	public List<SaveColumns> getSAVE_COLUMN() {
		return SAVE_COLUMN;
	}

	public void setSAVE_COLUMN(List<SaveColumns> sAVE_COLUMN) {
		SAVE_COLUMN = sAVE_COLUMN;
	}
	public String getCAMPAIGN_ID() {
		return CAMPAIGN_ID;
	}
	public String getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}
	public String getSHOW_ALL_SEND_DETAIL() {
		return SHOW_ALL_SEND_DETAIL;
	}
	public String getSEND_AS_BATCH() {
		return SEND_AS_BATCH;
	}
	public String getNO_RETRY_ON_FAILURE() {
		return NO_RETRY_ON_FAILURE;
	}
	public Recipient getRecipient() {
		return RECIPIENT;
	}
	public void setCAMPAIGN_ID(String cAMPAIGN_ID) {
		CAMPAIGN_ID = cAMPAIGN_ID;
	}
	public void setTRANSACTION_ID(String tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}
	public void setSHOW_ALL_SEND_DETAIL(String sHOW_ALL_SEND_DETAIL) {
		SHOW_ALL_SEND_DETAIL = sHOW_ALL_SEND_DETAIL;
	}
	public void setSEND_AS_BATCH(String sEND_AS_BATCH) {
		SEND_AS_BATCH = sEND_AS_BATCH;
	}
	public void setNO_RETRY_ON_FAILURE(String nO_RETRY_ON_FAILURE) {
		NO_RETRY_ON_FAILURE = nO_RETRY_ON_FAILURE;
	}
	public void setRecipient(Recipient recipient) {
		this.RECIPIENT = recipient;
	}
	public List<String> getSAVE_COLUMNS() {
		return SAVE_COLUMNS;
	}
	public void setSAVE_COLUMNS(List<String> sAVE_COLUMNS) {
		SAVE_COLUMNS = sAVE_COLUMNS;
	}
	
	
}
