package cl.ripley.omnicanalidad.dao;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public interface AnulacionRecaudacionDAO {
	
	public boolean AnulacionMKP(TramaDTE trama, Parametros pcv, String fecha, String fechaHora) throws AligareException;
	public boolean anulacionMKP2(TramaDTE trama, Parametros pcv, String fecha, String fechaHora, Integer transaccion) throws AligareException;
	public boolean AnulacionCDF(TramaDTE trama, Parametros pcv, String fecha, String fechaHora) throws AligareException;
	public boolean updateIdentificadorMkpReca(TramaDTE trama, Parametros pcv, int transaccion, String fecha) throws AligareException;

}
