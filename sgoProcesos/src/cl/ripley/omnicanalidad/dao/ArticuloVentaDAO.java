package cl.ripley.omnicanalidad.dao;

import java.sql.Timestamp;

public interface ArticuloVentaDAO {

	public Integer actualizar(	Integer correlativoVenta,
								Integer numeroNotaCredito,
								Timestamp fechaNotaCredito,
								Timestamp horaNotaCredito,
								Integer numCajaNotaCredito,
								Integer correlativoNotaCredito,
								Integer rutNotaCredito,
								Integer codigoDespacho,
								Integer correlativoItem);
	
}

