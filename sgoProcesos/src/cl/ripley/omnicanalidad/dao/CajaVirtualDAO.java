
package cl.ripley.omnicanalidad.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.ripley.dao.dto.OcTraceRposDTO;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.FMaxTVirtualTipo;
import cl.ripley.omnicanalidad.bean.OrdenesImpresas;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.TotalCuadratura;
import cl.ripley.omnicanalidad.bean.TotalRecaudacion;
import cl.ripley.omnicanalidad.bean.TotalU;
import cl.ripley.omnicanalidad.bean.TotalVentasBanc;
import cl.ripley.omnicanalidad.bean.TotalVentasTRE;
import cl.ripley.omnicanalidad.bean.TotalesCierre;
/**Interface DAO para operaciones de caja.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface CajaVirtualDAO {
	
	public Integer resNroTransaccion(Parametros pcv, String fecha) throws AligareException;
	public boolean existeTrx(Parametros pcv, int nroTrx, String fecha) throws AligareException;
	public boolean creaCajaTvirtualBoletas (Parametros pcv) throws AligareException;
	public boolean aperturaCaja(Parametros pcv, int nroTrx, String fecha, String fechaHora)throws AligareException;
	public boolean backOffice (int sucursal, String fecha) throws AligareException;
	public boolean actualizaTransaccion (int nroCaja, int nroTrx) throws AligareException;
	public boolean cambioEstadoCaja(Parametros pcv, int estado, int nroTrx,String fecha) throws AligareException;
	public boolean actualizaBoleta(Parametros pcv, int nroBoleta) throws AligareException;
	public String sysdateFromDual(String formato, Parametros pcv)throws AligareException;
	public void cambiaFechaBoletasSinProcesar(String fecha, Parametros pcv) throws AligareException;
	public Long obtieneMayorIdAperturaCierre() throws AligareException; 
	public Long obtieneIdAperturaCierre(Parametros pcv) throws AligareException;
	public RetornoEjecucion registraAperturaCierreCaja(Parametros pcv, int nroTrxApe, int nroTrxCie, String fechaApertura, String fechaCierre, int tipoLlamada, Long nroIdApertura) throws AligareException;
	public int maximaTrxApertura(Parametros pcv, String fecha, Long nroIdApertura)throws AligareException;
	public boolean cierreCajaIntegrada(Parametros pcv, String fecha, int nroTrx,BigDecimal totalVenta, BigDecimal wsUnidades, BigDecimal totalMontoReca,BigDecimal montoEfectivo, String fechaHora) throws AligareException;
	public BigDecimal fcuentaPorTipoTVirtual(Parametros pcv, String fecha, int tipoFiltro,int maxTrxApertura, Long idAperturaCierre)throws AligareException;
	public TotalRecaudacion totalRecaudacionCierre(Parametros pcv, String fecha, int maxNroTrx) throws AligareException;
	public TotalVentasTRE totalVentasTRECierre(Parametros pcv, String fecha, int maxNroTrx, int nroTrx)throws AligareException;
	public TotalVentasBanc totalVentasBancCierre(Parametros pcv, String fecha, int maxNroTrx,int nroTrx, Long idAperturaCierre) throws AligareException;
	public FMaxTVirtualTipo fmaxVirtualTipo(Parametros pcv, String fecha)throws AligareException;
	public TotalU totalUCierre(Parametros pcv, String fecha, int maxNroTrx)throws AligareException;
	public TotalesCierre totales(Parametros pcv, String fecha, int nroTrx, Long idAperturaCierre)throws AligareException;
	public OrdenesImpresas obtieneOrdentesImpresas(Parametros pcv, String fecha, int maxNroTrx, int nroTrx) throws AligareException;;
	public boolean existeCaja(Parametros pcv, String fecha, int nroTrx) throws AligareException;
	public boolean actualizaSaldoVolumen (Parametros pcv, String fecha, boolean esOC) throws AligareException;
	public boolean aperturaCierreAutomatico (Parametros pcv, int tipoCierre) throws AligareException;
	public void generaArchivoDeCierre(Parametros pcv,Long nroIdApertura, String ruta) throws AligareException;
	public boolean existeCajaCerrada(Parametros pcv, String fechaCierre) throws AligareException;
	public TotalCuadratura datosMailCuadratura(Parametros pcv, Long nroIdApertura)throws AligareException;
	
	public boolean saltarFolio(Parametros pcv, int nroTrx) throws AligareException;
	
	/**Realiza el cirre de la caja invocando al servicio del BackOffice.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trx
	 * @param pcv
	 * @param fecha
	 * @param fechaHora
	 * @param totalesCierre
	 * @return
	 * @throws AligareException
	 */
	boolean cierreCajaIntegradaNew(Long trx, Parametros pcv, String fecha, String fechaHora, TotalesCierre totalesCierre) throws AligareException;
	/**Realiza la apertura de la caja invocando al servicio del BackOffice.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trx
	 * @param pcv
	 * @param fecha
	 * @param fechaHora
	 * @return
	 * @throws AligareException
	 */
	boolean aperturaCajaNew(Long trx, Parametros pcv, String fecha, String fechaHora) throws AligareException;
	/**Actualiza el número de transacción que necesita enviarse al BackOffice.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param nroTrx
	 * @param pcv
	 * @return
	 * @throws AligareException
	 */
	boolean updNroTransaccion(Integer nroTrx, Parametros pcv) throws AligareException;
	public List<SucursalRpos> cajaSucursalRpos(int jobJenkins) throws AligareException;
	public List<SucursalRpos> cajaSucUrlDoce(int jobUrlDoce) throws AligareException;
	public SucursalRpos getDatosSuc(Integer sucursal, Integer caja) throws AligareException;
	public Map<String, Integer> getCajaSucRpos(Long oc) throws AligareException;
	
}
