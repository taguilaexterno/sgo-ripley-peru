package cl.ripley.omnicanalidad.dao;

import java.sql.Timestamp;

import cl.ripley.omnicanalidad.bean.ClienteClub;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;

public interface ClubDAO {
	public ClienteClub getClienteClub(Integer numeroEvento, int indicadorTitular);
	public RetornoEjecucion lglreg_prc_mov_lis_vlr(	String codigoEvento,
													long codigoProducto,
													Integer codSucursal,
													Timestamp fechaCompra,
													Integer numeroTransaccion,
													Integer codigoTransaccion,
													Integer codEntraSale,
													Integer tipoDocumento,
													Integer cantidadProducto,
													long precioCompra,
													Integer numeroBol,
													Integer numeroCaja,
													Integer formaPago,
													Integer secuencial,
													String dirDespacho,
													String marcaRegalo,
													Integer fechaDespacho,
													String email,
													String nombreInvitado,
													String apellidoPaternoInvit,
													String apellidoMaternoInvit,
													String direcInvitado,
													Integer telefonoInvitado,
													String mensajeInvitado,
													Integer fchOrigen,
													Integer boletaOrigen,
													Integer sucursalOrigen,
													Integer cjaOrigen);

	public RetornoEjecucion lglreg_prc_mov_lis_vlr_mkp(	String codigoEvento,
														long codigoProducto,
														Integer codSucursal,
														Timestamp fechaCompra,
														Integer numeroTransaccion,
														Integer codigoTransaccion,
														Integer codEntraSale,
														Integer tipoDocumento,
														Integer cantidadProducto,
														long precioCompra,
														Integer numeroBol,
														Integer numeroCaja,
														Integer formaPago,
														Integer secuencial,
														String dirDespacho,
														String marcaRegalo,
														Integer fechaDespacho,
														String email,
														String nombreInvitado,
														String apellidoPaternoInvit,
														String apellidoMaternoInvit,
														String direcInvitado,
														Integer telefonoInvitado,
														String mensajeInvitado,
														Integer fchOrigen,
														Integer boletaOrigen,
														Integer sucursalOrigen,
														Integer cjaOrigen,
														String descProducto);


}
