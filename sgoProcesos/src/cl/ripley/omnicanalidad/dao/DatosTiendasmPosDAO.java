package cl.ripley.omnicanalidad.dao;

import cl.ripley.omnicanalidad.bean.DatosTiendaMpos;

public interface DatosTiendasmPosDAO {

	public DatosTiendaMpos getDataTiendaByLlave(String llave);
}
