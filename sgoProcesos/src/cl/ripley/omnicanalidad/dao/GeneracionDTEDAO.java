package cl.ripley.omnicanalidad.dao;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TVirtualCodBancaria;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public interface GeneracionDTEDAO {

	public ConcurrentLinkedQueue<TramaDTE> obtenerOrdenesParaGeneracion(Integer nroCaja, Integer nroSucusal, Integer nroEstado);
	public ConcurrentLinkedQueue<TramaDTE> obtenerOrdenesParaGeneracion(Integer nroSucusal, Integer nroEstado);
	public List<ArticuloVentaTramaDTE> obtenerArticuloVentas(Long correlativoVenta);
	public TVirtualCodBancaria getCodigoConvenioXTipo(int codigoTipoInterno);
	public boolean creacionTransaccionesDte(Parametros parametros, TramaDTE trama, Integer nroTransaccion, Integer nroTransaccionAnterior, String codUnico, String boleta,
			boolean isRipleyProcessed, boolean isMkpProcessed) throws AligareException;
	public boolean buscaFormaDePago(Parametros parametros, TramaDTE trama);
	public boolean actualizarNotaVenta(Long correlativoVenta, Integer estado, Integer nroCaja);
	public boolean actualizarNotaVenta(Long correlativoVenta, Integer estado, Integer nroCaja, Integer sucursal);
	public boolean creacionRecaudacionesCDF(Parametros parametros, TramaDTE trama, Integer nroTransaccion, String codUnico, Integer boleta) throws AligareException;
	public boolean creacionRecaudacionesMKP(Parametros parametros, TramaDTE trama, Integer nroTransaccion, String codUnico, Integer boleta, Integer boletaOrigen) throws AligareException;

	public List<IdentificadorMarketplace> obtenerIdentificadorMarketplace(Long correlativoVenta);
	public boolean registraTransaccion(Parametros pcv, Integer nroTransaccion, String boleta, String fechaGen, String estGeneraOC,String observaciones, TramaDTE trama, int flag, int esNC) throws AligareException;

	public boolean actualizarNotaVentaEstadoFolio(Long correlativoVenta, Integer estado, String folio, int nroCaja, int flag);
	
	public boolean actualizarNotaVentaCnFlag(Integer estado, Integer boleta, String folioSII, String fechaBoleta, String formatoFechaBoleta, String horaBoleta, String FormatoHoraBoleta, Integer numCaja, Integer correlativoBoleta, Integer rutBoleta, Integer sucursal, Long correlativoVenta, int flag);
	
	public boolean updateIdentificadorMkpBoleta(TramaDTE trama, Integer estado, Parametros pcv, int nroTransaccion, String fecha, int nroTransaccionOrigen) throws AligareException;

	public boolean updateConvenioBancaria(TramaDTE trama) throws AligareException;
	
	public boolean updateDireccionDespachoHistorico(TramaDTE trama) throws AligareException;

	public boolean ingresarDocumentoElectronico(Long correlativoVenta,Integer numCaja, Integer sucursal, String tramaDTE, String xmlTDE, String xmlMail, String pdf417) throws AligareException;
	
	List<TramaDTE> obtenerUnaOrdenParaGeneracion(Long correlativoVenta, Integer nroCaja, Integer nroSucusal,
			Integer nroEstado);
	
	/**Obtiene números de OCs listas para impresión
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 20-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param cantidadOCs
	 * @return
	 */
	List<Long> obtenerNumerosOCsImpresion(Integer caja, Integer cantidadOCs);
	
	/**Obtiene número de OCs según el estado entregado.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 20-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param cantidadOCs
	 * @param estado
	 * @return
	 */
	List<Long> obtenerNumerosOCsByEstado(Integer caja, Integer cantidadOCs, Integer estado, Integer funcion);
	
	/**Envia TRX a BO
	 *
	 * @param inOrdenCompra
	 * @param parametros
	 * @param nroTransaccion
	 * @param esMkp
	 * @param boleta
	 * @throws AligareException
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	void envioTrxBo(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, Long nroTrxAnterior, boolean esMkp, String boleta,
			boolean isRipleyProcessed, boolean isMkpProcessed) throws AligareException;

}
