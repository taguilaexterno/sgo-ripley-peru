package cl.ripley.omnicanalidad.dao;

import cl.ripley.omnicanalidad.bean.EstadoMirakl;

public interface MiraklDAO {
	boolean insertarEstadoMKL(EstadoMirakl estadoMirakl);
}
