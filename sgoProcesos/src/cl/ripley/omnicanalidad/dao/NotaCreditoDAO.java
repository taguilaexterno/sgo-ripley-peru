package cl.ripley.omnicanalidad.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.MontoNroTarje;
import cl.ripley.omnicanalidad.bean.RetornoNotaCredito;
import cl.ripley.omnicanalidad.bean.Saldos;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public interface NotaCreditoDAO {
	

	public RetornoNotaCredito actualizaEfectivoCV(	Timestamp fechaTransaccion,
													Integer sucursal,
													Integer numeroCaja,
													Integer numeroTransaccion,
													long montoEfectivo,
													Integer porcentajeParticipacion);

	public RetornoNotaCredito insertTransbankCv(Timestamp fechaTransaccion, 
												Integer sucursal, 
												Integer numeroCaja,
												Integer numeroTransaccion, 
												String marcaTarjeta, 
												String tipoTarjeta, 
												Integer rutCliente,
												Integer numeroTarjeta, 
												long montoEfectivo, 
												Integer tipoCuota, 
												Integer numeroCuotas, 
												Integer fechaVence,
												String codigoAutorizador, 
												String codigoUnico);

	public RetornoNotaCredito actualizaTarRegaloEmpresa(	Timestamp fechaTransaccion, 
															Integer sucursal, 
															Integer numeroCaja,
															Integer numeroTransaccion, 
															Integer administradora, 
															Integer emisor, 
															Integer tarjeta,
															Integer codigoAutorizador, 
															long rutCliente, 
															long montoEfectivo, 
															Integer numeroTarjeta, 
															Integer flag);
	
	
	public RetornoNotaCredito insertaBancariaBack(	Timestamp fechaTransaccion,
													Integer sucursal,
													Integer numeroCaja,
													Integer numeroTransaccion,
													Integer tipoTarjeta,
													long monto,
													String correlativoVenta);
	
	public RetornoNotaCredito insertaRipleyBack(	Timestamp fechaTransaccion,
													Integer sucursal,
													Integer numeroCaja,
													Integer numeroTransaccion,
													Integer administradora,
													Integer emisor,
													Integer tarjeta,
													Integer rutTitular,
													Integer rutPoder,
													Integer plazo,
													Integer diferido,
													long montoTarjetaRipley,
													Integer montoPie,
													Integer descuentoCar,
													Integer codigoDescuento,
													BigDecimal codigoAutorizador,
													Integer porcentajePar,
													BigDecimal numeroPan); 

	//backoffice
	public RetornoNotaCredito insertaTransaccionBack4(	Timestamp fechaTransaccion, 
														Integer sucursal, 
														Integer numeroCaja,
														Integer numeroTransaccion, 
														Integer numeroDocumento, 
														Integer tipoTransaccion, 
														long monto, 
														Integer sucursalOriginal,
														Timestamp fechaBoleta, 
														Integer numeroCajaOriginal, 
														Integer numeroBoleta, 
														Integer numeroDoctoOriginal, 
														Integer supervisor,
														Integer vendedor, 
														Integer origenTransaccion, 
														Integer rutComprador, 
														Integer montoDescuento, 
														Integer descuentoCar, 
														Integer montoPie,
														Timestamp fechaHoraGl, 
														Integer estado, 
														Integer tipoOrigen, 
														Long correlativoVenta, 
														Integer nroTranOriginal,
														Timestamp fechaCreacionNotaVenta, 
														Integer folioSII, 
														Integer codigoBO, 
														String servicioNCA,
														String detalle);
	//bigticket BIGT_ADM
	public RetornoNotaCredito fNotaCredito(	String codigoDespacho,
											String mot,//motivo?
											Integer sucursal,
											Integer numeroCaja,
											Integer transaccion,
											String vendedor,
											Timestamp fechaNotaCredito,
											Integer origen);
	//tarjeta club regalos
	public RetornoNotaCredito lglreg_prc_mov_lis_vlr(	String codigoEvento,
														long codigoProducto,
														Integer codSucursal,
														Timestamp fechaCompra,
														Integer numeroTransaccion,
														Integer codigoTransaccion,
														Integer codEntraSale,
														Integer tipoDocumento,
														Integer cantidadProducto,
														long precioCompra,
														Integer numeroBol,
														Integer numeroCaja,
														Integer formaPago,
														Integer secuencial,
														String dirDespacho,
														String marcaRegalo,
														Integer fechaDespacho,
														String email,
														String nombreInvitado,
														String apellidoPaternoInvit,
														String apellidoMaternoInvit,
														String direcInvitado,
														Integer telefonoInvitado,
														String mensajeInvitado,
														Integer fchOrigen,
														Integer boletaOrigen,
														Integer sucursalOrigen,
														Integer cjaOrigen);
	// backoffice function
	public RetornoNotaCredito insertaDespachoBack(	Timestamp fecha,
						 							Integer sucursal,
						 							Integer numeroCaja,
						 							Integer numeroTransaccion,
						 							String codigoUnidadDespacho,
						 							Integer color,
						 							Integer	cubicaje,
						 							long rutDespacho,
						 							String nombreDespacho,
						 							Timestamp fechaDespacho,
						 							Integer sucursalDespacho,
						 							Integer comunaDespacho,
						 							Integer regionDespacho,
						 							String direccionDespacho,
						 							String telefonoDespacho,
						 							Integer sectorDespacho,
						 							String jornadaDespacho,
						 							String observacion,
						 							Integer paginaCtc,
													String cordCtc,
													long rutCliente,
													String direccionCliente,
													String telefonoCliente,
													Integer tipoCliente,
													String nombreCliente);
	
	// backoffice function
	public RetornoNotaCredito insertaArticulosBack(	Timestamp fecha,
													Integer sucursal,
													Integer numeroCaja,
													Integer numeroTransaccion,
													Integer numeroItem,
													long codigoArticulo,
													Integer vendedor,
													Integer codigoNovios,
													Integer saleArticulo,
													Integer precioArticulo,
													Integer unidades,
													Integer descuento,
													Integer tipoDescuento,
													String 	codUnidadDespacho,
													Integer porcentajeParticip,
													Integer despachoDomicilio );
			

	
	public RetornoNotaCredito updateNotaVenta( Integer estado,
											   Integer numeroNotaCredito,
											   Integer numeroBoleta,
											   Timestamp fechaNotaCredito,
											   Timestamp horaNotaCredito,
											   Integer numeroCaja,
											   Integer numeroCorrelativoNotaCredito,
											   Integer rutVendedor,
											   Long correlativoVenta);
	
	public RetornoNotaCredito updateMarketPlace(Integer estado,
												Integer transaccion,
												Integer numeroCaja,
												Timestamp	fecha,
												Long correlativoVenta,
												String 	ordenMkp);
	
	public RetornoNotaCredito updateArticuloVenta(Integer nroBoleta,
												  Timestamp fecha,
												  Timestamp fechaHora,
												  Integer nroCaja,
												  Integer transaccion,
												  Integer vendedor,
												  Long correlativoVenta,
												  String  cud,
												  Integer correlativoItem);
	
	public int obtieneMaxCorrNC();
	
	public MontoNroTarje obtieneMontoNroTarje (Long correlativoVenta);
	public RetornoNotaCredito insertIntoCredito(Integer idCorrNc,
			Integer nroBoleta, Timestamp fechaNc, Integer correlativoVenta,
			String tipoPago, Integer montoCompra, long glTotalNc,
			long montoCar, long saldoCar, long montoTre,
			long saldoTre, long saldoTotal);
	

	
	public RetornoNotaCredito anulacionFolio(Timestamp fecha, int funcion,int sucursal, int nroCaja, int nroTrx, int nroBoleta, Long correlativoVenta, int tipoDePago, String supervisor, String vendedor) throws AligareException;
	public RetornoNotaCredito fegife_prc_act_sld_nc(int nroTarjeta, long montoTarjeta, int numeroCaja, Long correlativoVenta, Timestamp fechaBoleta) throws AligareException;
	public BigDecimal getTotalNc(TramaDTE trama) throws AligareException;
	public boolean actualizarEsNCArticuloVenta(Long correlativoVenta, Integer esNC);
	public Saldos getSaldos(Long correlativoVenta) throws AligareException;
	public Integer tieneCud(String articulo);
}