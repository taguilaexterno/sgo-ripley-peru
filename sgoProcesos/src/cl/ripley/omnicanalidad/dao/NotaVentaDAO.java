package cl.ripley.omnicanalidad.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;

public interface NotaVentaDAO {
	
	public List<NotaVenta> cargaOrdenesCompraCajaTransito(String nroCajaTransito, String volumen, String estadosIniciales);
	public NotaVenta cargaOrdenesCompraCajaTransitoPorOC(String nroCajaTransito, Long oc, String estadosIniciales);
	public NotaVenta cargarOrdenOC(Long oc);

	
	public List<NotaVenta> cargaOrdenesCompraCajaTransitoSeguras(String nroCajaTransito, String volumen);
	
	public Integer actualizarPorNotaCredito(	Integer correlativoVenta,
								Integer numeroNotaCredito,
								Integer folioNcSII,
								Timestamp fechaNotaCredito,
								Timestamp horaNotaCredito,
								Integer numCajaNotaCredito,
								Integer correlativoNotaCredito,
								Integer rutNotaCredito,
								Integer estado);
	
	public ConcurrentLinkedQueue<NotaVentaMail> obtenerOrdenesParaEnvioMail(Integer nroCaja, Integer nroSucusal, Integer nroEstado);
	public ConcurrentLinkedQueue<NotaVentaMail> obtenerOrdenesParaEnvioMailTransac(Integer nroCaja, Integer nroSucusal, Integer nroEstado);
	
	public boolean actualizarNotaVentaCnUrlPPL(Long correlativoVenta, String urlPPL, String nombreColumna);

	public NotaVentaMail obtenerOrdenesParaEnvioMailTransac(Long correlativoVenta);
	
	/**Obtiene número de OC por estado y número de caja
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 20-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param cantidadOCs
	 * @param estado
	 * @return
	 */
	List<Long> obtenerNumerosOCsByEstado(Integer caja, Integer cantidadOCs, Integer estado);
	
	/**Asigna número de caja a la OC indicada en el parámetro.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 20-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param caja
	 * @param oc
	 * @return
	 */
	boolean asignarCaja(Integer caja, Long oc);
}

