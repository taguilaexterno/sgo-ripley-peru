package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;

public interface OrdenCompraDAO {
	
	public List<NotaVenta> cargaOrdenesCompraCajaTransito(String nroCajaTransito, String volumen, String estadosIniciales) throws AligareException;
	
	public List<NotaVenta> cargaOrdenesCompraCajaTransitoSeguras(	String nroCajaTransito, String volumen);
	
	/**
	 * Método para obtener datos para pintar boleta html. De uso exclusivo.
	 * 
	 * @param nroCaja
	 * @param nroSucusal
	 * @param oc
	 * 
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 12-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	List<OrdenesDeCompra> obtenerDatosBoleta(Integer nroCaja, Integer nroSucusal, Long oc);
	
	/**Obtiene articulos por orden de compra (correlativo de venta)
	 *
	 * @param correlativoOC
	 * 
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 12-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 */
	List<ArticulosVentaOC> getArticulosByOC(Long correlativoOC);
	
}
