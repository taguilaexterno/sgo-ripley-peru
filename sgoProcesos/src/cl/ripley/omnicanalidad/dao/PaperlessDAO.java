package cl.ripley.omnicanalidad.dao;

import java.util.List;

import cl.ripley.omnicanalidad.bean.ResultadoPPL;

public interface PaperlessDAO {
	
	public ResultadoPPL generacionDte(int rut, String user, String pass, String trama, int tipoGeneracion, int tipoRetorno, int segundosTimeOut);
	public ResultadoPPL generacionBoleta(int rut, String user, String pass, String trama, int tipoGeneracion, int tipoRetorno, int segundosTimeOut);
	public ResultadoPPL generacionBoleta2(String rut, String user, String pass, String trama, int tipoGeneracion, int tipoRetorno);
	public ResultadoPPL recuperacionOnline(String rut, String user, String pass, String tipoDoc, String nroFolio, int tipoRetorno, int segundosTimeOut);
	
	public List<ResultadoPPL> getListadoErroresPPL(int codigoMsg);
}