package cl.ripley.omnicanalidad.dao;

import java.util.ArrayList;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametro;

public interface ParametrosDAO {
	
	public void getParametrosCajaVirtual(int funcion, ArrayList<Parametro> p, String balance_jar) throws AligareException;
	public void getParametrosOmnicanalidad(ArrayList<Parametro> pList, String balance_jar) throws AligareException;
	
	public boolean updateParametro(Parametro parametro);

}
