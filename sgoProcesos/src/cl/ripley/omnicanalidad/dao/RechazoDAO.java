package cl.ripley.omnicanalidad.dao;

import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public interface RechazoDAO {
	
	public RetornoEjecucion fegifePrcInsTreint(TramaDTE trama) throws AligareException;
	public RetornoEjecucion fegifePrcActSldConf(TramaDTE trama) throws AligareException;

}
