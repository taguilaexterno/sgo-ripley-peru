
package cl.ripley.omnicanalidad.dao;

import java.util.HashMap;

import cl.aligare.ara.util.AligareException;
/**Interface DAO operaciones de codigos sunat.
 *
 * @author Martin Corrales (Aligare).
 * @since 08-01-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface SunatDAO {
	
	public boolean insertCodigosSunat (HashMap<String,String> codigosSunat) throws AligareException;

}
