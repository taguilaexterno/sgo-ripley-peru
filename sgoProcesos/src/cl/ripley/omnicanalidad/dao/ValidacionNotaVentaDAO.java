package cl.ripley.omnicanalidad.dao;

import cl.aligare.ara.util.AligareException;

public interface ValidacionNotaVentaDAO {
	
	public Integer aplicaXValidacionesOrdenCompraTransito(Long valido, boolean flagRpos) throws AligareException;
	
	public Integer aplicaValidacionesMedioPago(	Long correlativoVenta,
												Long rutComprador,
												String emailCliente,
												String telefonoCliente,
												String tipoDespacho,
												Integer comunaDespacho,
												Integer regionDespacho,
												String direccionDespacho,
												Integer tipoDoc);
	
	public Integer aplicaValidacionesMedioPagoSeguro(	Long correlativoVenta,
														Long rutComprador,
														String emailCliente,
														String telefonoCliente,
														String tipoDespacho,
														String indicadorMkp,
														Integer comunaDespacho,
														Integer regionDespacho,
														String direccionDespacho,
														Integer tipoDoc);
}
