package cl.ripley.omnicanalidad.dao;

import cl.ripley.omnicanalidad.bean.TemplateVoucher;

public interface VoucherTemplateDAO {

	public TemplateVoucher getTemplateByLlave(String llave);
}
