package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ripley.restservices.model.backOfficeRest.BackOfficeResponse;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallAnulacionMkp;
import com.ripley.restservices.model.backOfficeRest.RequestBO;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.Saldos;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.AnulacionRecaudacionDAO;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Validaciones;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;
import oracle.jdbc.OracleTypes;

@Repository
public class AnulacionRecaudacionDAOImpl implements AnulacionRecaudacionDAO {

	private static final AriLog logger = new AriLog(
			AnulacionRecaudacionDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private CajaVirtualDAO caja;
	
	@Autowired
	private RestTemplate rt;
	
	@Autowired
	private ConversionService conversionService;

	@Override
	public boolean updateIdentificadorMkpReca(TramaDTE trama, Parametros pcv, int transaccion, String fecha) throws AligareException {

		boolean retorno = true;
		CallableStatement cs = null;
		Connection conn = null;
		Integer codSalida = null;
		String mjeSalida = null;
		String sql = "";
		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
		Long correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		List<IdentificadorMarketplace> listaIdentMkp = trama
				.getIdentificadoresMarketplace();
		List<ArticuloVentaTramaDTE> listaArtVenta = trama.getArticuloVentas();
		conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {

			try {
				if (!listaIdentMkp.isEmpty() && !listaArtVenta.isEmpty()) {
					for (IdentificadorMarketplace identificadorMarketplace : listaIdentMkp) {
						String ordenMkp = identificadorMarketplace.getOrdenMkp();
						if (trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL) {
							if ((indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS) || 
									indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_MKP)) && 
									identificadorMarketplace.getEsMkp().intValue() == Constantes.ES_MKP_MKP) {
								String procSQL = "{CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_IDENT_MKP(?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?)}";
								cs = conn.prepareCall(procSQL);
								cs.setInt(1, Constantes.NRO_TRES);
								cs.setInt(2, transaccion);
								cs.setString(3, fecha);
								cs.setInt(4, nroCaja);
								cs.setLong(5, correlativoVenta);
								cs.setString(6, ordenMkp);
								cs.registerOutParameter(7, OracleTypes.NUMBER);
								cs.registerOutParameter(8, OracleTypes.VARCHAR);
								sql = "{CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_IDENT_MKP(3,"
										+ transaccion
										+ ",TO_DATE("
										+ fecha
										+ ",'DD/MM/YYYY')}"
										+ ","
										+ nroCaja
										+ ","
										+ correlativoVenta
										+ ","
										+ ordenMkp + ",out,out)";
								logger.traceInfo("updateIdentificadorMkpReca", sql);
								cs.execute();
								codSalida = cs.getInt(7);
								mjeSalida = cs.getString(8);
								cs.close();
								break;
							}
						}// fin nota_credito_total
						if (trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_PARCIAL) {
							for (ArticuloVentaTramaDTE obj : listaArtVenta) {
								// busco orden en Articulo de ventas
								if (!ordenMkp.equalsIgnoreCase(obj.getArticuloVenta().getOrdenMkp())) {
									continue;
								}
								// pregunto si articuloVenta es NC
								if (obj.getArticuloVenta().getEsNC().intValue() != Constantes.esNC) {
									continue;
								}
								// Si es el mismo proveedor se hace la Update
								// para ese proveedor
								if ((indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS) || 
										indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_MKP)) && 
										identificadorMarketplace.getEsMkp().intValue() == Constantes.ES_MKP_MKP) {
									String procSQL = "{CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_IDENT_MKP(?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?)}";
									cs = conn.prepareCall(procSQL);
									cs.setInt(1, Constantes.NRO_CUATRO);
									cs.setInt(2, transaccion);
									cs.setString(3, fecha);
									cs.setInt(4, nroCaja);
									cs.setLong(5, correlativoVenta);
									cs.setString(6, ordenMkp);
									cs.registerOutParameter(7, OracleTypes.NUMBER);
									cs.registerOutParameter(8, OracleTypes.VARCHAR);
									sql = "{CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_IDENT_MKP(4,"
											+ transaccion
											+ ",TO_DATE("
											+ fecha
											+ ",'DD/MM/YYYY')}"
											+ ","
											+ nroCaja
											+ ","
											+ correlativoVenta
											+ ","
											+ ordenMkp + ",out,out)";
									logger.traceInfo("updateIdentificadorMkpReca", sql);
									cs.execute();
									codSalida = cs.getInt(7);
									mjeSalida = cs.getString(8);
									cs.close();
								}

							}// fin for listaArtVenta

						}// fin nota_credito_parcial

					}// fin for listaIdentMkp

					if (Validaciones.validaInteger(codSalida) && codSalida.intValue() != Constantes.NRO_CERO) {
						logger.traceError(
								"updateIdentificadorMkpReca",
								"--> Imposible hacer Update - " + sql,
								new Exception("CodError: "
										+ codSalida.toString() + " MjeError"
										+ mjeSalida));
						logger.endTrace("updateIdentificadorMkpReca", "Finalizado", "Null");
						retorno = false;
					}

				}
			} catch (Exception e) {
				logger.traceError(
						"updateIdentificadorMkpReca", "Error durante la ejecuci�n de updateIdentificadorMkpReca", e);
				return retorno;
			} finally {

				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("updateIdentificadorMkpReca", "Problemas de conexion a la base de datos");
		}

		return retorno;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean AnulacionMKP(TramaDTE trama, Parametros pcv, String fecha, String fechaHora) throws AligareException {
		logger.initTrace("AnulacionMKP", "Parametros: parametros");

		//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		//		ClubDAO clubDAO = new ClubDAOImpl();
		//		
		//		Connection conn = null;
		//		Connection connTRE = null;
		//		Connection connClaseS = null;
		//		Connection connExtend = null;
		boolean AnulacionMKP = false;
		//		RetornoEjecucion retornoEjecucion = null;
		//		int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		//		int sucursal = Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"));
		//		int transaccion = caja.resNroTransaccion(pcv, null);
		//		long glTotalNc = getTotalNcMKP(trama);
		//		int wsFlagNcTre = 0;
		//		String vendedor = pcv.buscaValorPorNombre("RUT_VENDEDOR");
		//		String rutEjecutivoVta = "";
		//		String sql = "";
		// Nuevo 8.0.12
		// String rutTransbank =
		// (trama.getNotaVenta().getRutComprador().toString()!=null)?trama.getNotaVenta().getRutComprador().toString():Constantes.STRING_CERO;
		// String dvCompradorMirakl =
		// (trama.getNotaVenta().getDvComprador()!=null)?trama.getNotaVenta().getRutComprador().toString():Constantes.STRING_CERO
		// ;
		// String rutCompradorMirakl =
		// (trama.getNotaVenta().getRutComprador().toString()!=null)?trama.getNotaVenta().getRutComprador().toString()+dvCompradorMirakl:Constantes.STRING_CERO;

		//		int tipo = 37;
		//		String codAut = "";
		//		CallableStatement cs = null;
		//		String procSQL = "";
		//		Integer wsTipo = new Integer(Constantes.NRO_CERO);
		//		Integer sal = null;
		//		String sal2 = Constantes.VACIO;
		//		try {
		//			// Conexiones a BACKOFFICE
		//			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//			if (conn != null) {
		//				if (trama.getTarjetaBancaria().getCorrelativoVenta() != null) {
		//					logger.traceInfo("AnulacionMKP", "Entra a trama.getTarjetaBancaria().getCorrelativoVenta()");
		//					wsTipo = (Validaciones.validaInteger(trama.getTarjetaBancaria().getTipoTarjeta())) ? trama.getTarjetaBancaria().getTipoTarjeta() : Constantes.NRO_CERO;
		//					codAut = (trama.getTarjetaBancaria().getCodigoAutorizador() != null) ? trama.getTarjetaBancaria().getCodigoAutorizador() : Constantes.VACIO;
		//					rutEjecutivoVta = ((trama.getNotaVenta().getEjecutivoVta() != null && 
		//										Constantes.VACIO.equals(trama.getNotaVenta().getEjecutivoVta().trim())) ? trama.getNotaVenta().getEjecutivoVta() : Constantes.STRING_CERO);
		//					// Creaci�n Tarjeta Bancaria
		//					sql = "";
		//
		//					if (Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(trama.getTarjetaBancaria().getVd())
		//							&& !(Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso()))) {
		//						procSQL = "{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						cs.setInt(2, sucursal);
		//						cs.setInt(3, nroCaja);
		//						cs.setInt(4, transaccion);
		//						cs.setLong(5, glTotalNc);
		//						cs.setInt(6, Constantes.NRO_CERO);
		//						cs.registerOutParameter(7, OracleTypes.NUMBER);
		//						cs.registerOutParameter(8, OracleTypes.VARCHAR);
		//						logger.traceInfo("AnulacionMKP",
		//								"{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE('" + fecha
		//										+ "','DD/MM/YYYY')," + sucursal + ","
		//										+ nroCaja + "," + transaccion + ","
		//										+ glTotalNc + ",0,outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(7));
		//						sal2 = cs.getString(8);
		//						cs.close();
		//
		//					} else if (Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())) {
		//						procSQL = "{CALL inserta_transbank_cv(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						cs.setInt(2, sucursal);
		//						cs.setInt(3, nroCaja);
		//						cs.setInt(4, transaccion);
		//						cs.setString(5, Constantes.TARJETA_MERCADO_PAGO);
		//						cs.setString(6, "C");
		//						cs.setInt(7, trama.getNotaVenta().getRutComprador().intValue());
		//						cs.setInt(8, Constantes.NRO_CERO);
		//						cs.setLong(9, glTotalNc);
		//						cs.setInt(10, Constantes.NRO_CERO);
		//						cs.setInt(11, Constantes.NRO_CERO);
		//						cs.setInt(12, Constantes.NRO_CERO);
		//						cs.setString(13, codAut);
		//						cs.setString(14, "NU003900");
		//						cs.registerOutParameter(15, OracleTypes.NUMBER);
		//						cs.registerOutParameter(16, OracleTypes.VARCHAR);
		//						logger.traceInfo("AnulacionMKP",
		//								"{CALL inserta_transbank_cv(TO_DATE('"
		//										+ fecha
		//										+ "','DD/MM/YYYY'),"
		//										+ sucursal
		//										+ ","
		//										+ nroCaja
		//										+ ","
		//										+ transaccion
		//										+ ",'"
		//										+ Constantes.TARJETA_MERCADO_PAGO
		//										+ "','C',"
		//										+ trama.getNotaVenta()
		//												.getRutComprador().intValue()
		//										+ "," + Constantes.NRO_CERO + ","
		//										+ glTotalNc + "," + Constantes.NRO_CERO
		//										+ "," + Constantes.NRO_CERO + ","
		//										+ Constantes.NRO_CERO + ",'" + codAut
		//										+ "','NU003900',outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(15));
		//						cs.close();
		//
		//					} else {
		//						procSQL = "{CALL ? := inserta_bancaria_back(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.registerOutParameter(1, OracleTypes.NUMBER);
		//						cs.setString(2, fecha);
		//						cs.setInt(3, sucursal);
		//						cs.setInt(4, nroCaja);
		//						cs.setInt(5, transaccion);
		//						cs.setInt(6, wsTipo);
		//						cs.setLong(7, glTotalNc);
		//						cs.setString(8, codAut);
		//						logger.traceInfo("AnulacionMKP",
		//								"{CALL ? := inserta_bancaria_back(TO_DATE('"
		//										+ fecha + "','DD/MM/YYYY')," + sucursal
		//										+ "," + nroCaja + "," + transaccion
		//										+ "," + wsTipo + "," + glTotalNc + ",'"
		//										+ codAut + "')}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(1));
		//						cs.close();
		//
		//					}
		//
		//					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//						logger.traceError(
		//								"Creacion NC T.Bancaria BackOffice",
		//								"--> Imposible crear forma de Pago Bancaria Error - "
		//										+ sql,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//						
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//
		//				} // fin:trama.getTarjetaBancaria().getCorrelativoVenta()
		//				wsFlagNcTre = 1;
		//				// Creaci�n Tarjeta Ripley query se obtiene de la Trama
		//				if (trama.getTarjetaRipley().getCorrelativoVenta() != null) {
		//					BigInteger pan = trama.getTarjetaRipley().getPan();
		//					BigInteger codAutor = (trama.getTarjetaRipley().getCodigoAutorizacion() != null) ? trama.getTarjetaRipley().getCodigoAutorizacion() : new BigInteger(Constantes.STRING_CERO);
		//					logger.traceInfo("AnulacionMKP", "INSERTA_RIPLEY_BACK");
		//					// BigInteger tipoCodAuto= BigInteger.valueOf(0);
		//					// BigInteger tipoCodAuto = new BigInteger("0");
		//					tipo = 2;// en duro, copiado del c�digo fuente
		//					procSQL = "{CALL ? := INSERTA_RIPLEY_BACK(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//					cs = conn.prepareCall(procSQL);
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					cs.setString(2, fecha);
		//					cs.setInt(3, sucursal);
		//					cs.setInt(4, nroCaja);
		//					cs.setInt(5, transaccion);
		//					cs.setInt(6, trama.getTarjetaRipley().getAdministradora());
		//					cs.setInt(7, trama.getTarjetaRipley().getEmisor());
		//					cs.setInt(8, trama.getTarjetaRipley().getTarjeta());
		//					cs.setInt(9, trama.getTarjetaRipley().getRutTitular());
		//					cs.setInt(10, trama.getTarjetaRipley().getRutPoder());
		//					cs.setInt(11, trama.getTarjetaRipley().getPlazo());
		//					cs.setInt(12, trama.getTarjetaRipley().getDiferido());
		//					// cs.setInt(13,
		//					// trama.getTarjetaRipley().getMontoCapital()); Nuevo 8.0.12
		//					cs.setLong(13, glTotalNc);
		//					cs.setInt(14, trama.getTarjetaRipley().getMontoPie());
		//
		//					// if
		//					// (Integer.parseInt(pcv.buscaValorPorNombre("PARAM_CFT"))
		//					// == 0) {
		//					// tipoCodAuto = trama.getTarjetaRipley().getPan();
		//					// }
		//
		//					if (trama.getTarjetaRipley().getCodigoCore().intValue() == 1) {
		//						if (trama.getTarjetaRipley().getPan().intValue() > 1) {
		//							if (Integer.parseInt(trama.getTarjetaRipley().getTvtriGlsPrjTsaInt()) > 0) {
		//
		//								cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//								cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//								cs.setInt(17, 1);
		//								cs.setInt(18, 0);
		//								cs.setBigDecimal(19,
		//										((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) 
		//												&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//												Util.rellenarString(
		//														pan.toString(), 16,
		//														Constantes.FALSO,
		//														Constantes.CHAR_SPACE)
		//														.substring(0, 15))
		//												: null
		//												: (pan != null) ? new BigDecimal(
		//														pan.toString()) : null));
		//							} else {
		//								cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//								cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//								cs.setInt(17, 0);
		//								cs.setInt(18, 0);
		//								cs.setBigDecimal(19,
		//										((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) 
		//												&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//												Util.rellenarString(
		//														pan.toString(), 16,
		//														Constantes.FALSO,
		//														Constantes.CHAR_SPACE)
		//														.substring(0, 15))
		//												: null
		//												: (pan != null) ? new BigDecimal(pan.toString()) : null));
		//							}
		//						} else {
		//							cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//							cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//							cs.setInt(17, trama.getTarjetaRipley().getPan().intValue());
		//							cs.setInt(18, 0);
		//							cs.setInt(19, trama.getTarjetaRipley().getCodigoDescuento());
		//						}
		//					} else {
		//						cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//						cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//						cs.setBigDecimal(
		//								17,
		//								((Validaciones.validaInteger(trama
		//										.getTarjetaRipley().getCodigoCore()) && trama
		//										.getTarjetaRipley().getCodigoCore()
		//										.intValue() == Constantes.NRO_UNO) ? new BigDecimal(
		//										codAutor)
		//										: new BigDecimal(
		//												(((Validaciones
		//														.validaBigInteger(codAutor)) ? codAutor
		//														.toString()
		//														: Constantes.VACIO) + Constantes.STRING_CEROX2))));
		//						cs.setInt(18, 0);
		//						cs.setBigDecimal(19,
		//								((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && 
		//										trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//										Util.rellenarString(pan.toString(), 16,
		//												Constantes.FALSO,
		//												Constantes.CHAR_SPACE)
		//												.substring(0, 15)) : null
		//										: (pan != null) ? new BigDecimal(pan
		//												.toString()) : null));
		//					}
		//
		//					logger.traceInfo("AnulacionMKP",
		//							"{CALL ? := INSERTA_RIPLEY_BACK(to_date('"
		//									+ fecha
		//									+ "','DD/MM/YYYY'),"
		//									+ sucursal
		//									+ ","
		//									+ nroCaja
		//									+ ","
		//									+ transaccion
		//									+ ","
		//									+ trama.getTarjetaRipley().getAdministradora()
		//									+ ","
		//									+ trama.getTarjetaRipley().getEmisor()
		//									+ ","
		//									+ trama.getTarjetaRipley().getTarjeta()
		//									+ ","
		//									+ trama.getTarjetaRipley().getRutTitular()
		//									+ ","
		//									+ trama.getTarjetaRipley().getRutPoder()
		//									+ ","
		//									+ trama.getTarjetaRipley().getPlazo()
		//									+ ","
		//									+ trama.getTarjetaRipley().getDiferido()
		//									+ ","
		//									+ trama.getTarjetaRipley().getMontoCapital()
		//									+ ","
		//									+ trama.getTarjetaRipley().getMontoPie()
		//									+ ","
		//									+ trama.getTarjetaRipley().getDescuentoCar()
		//									+ ","
		//									+ trama.getTarjetaRipley().getCodigoDescuento() + ",?,0,?)}");
		//
		//					cs.execute();
		//					sal = new Integer(cs.getInt(1));
		//					cs.close();
		//					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//						tipo = 37;
		//						logger.traceError(
		//								"Creacion T.Ripley BackOffice - ",
		//								"--> Imposible crear forma de pago Ripley Error -"
		//										+ procSQL,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//				}
		//				// '------------------Creacion
		//				// TRE---------------------------------
		//				if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null) {
		//					logger.traceInfo("AnulacionMKP", "Creaci�n TRE");
		//					tipo = 37;// en duro, desde c�digo fuente
		//					wsTipo = trama.getTarjetaRegaloEmpresa().getTarjeta();
		//					Integer nroTarjeta = trama.getTarjetaRegaloEmpresa().getNroTarjeta();
		//					Integer codAdmin = trama.getTarjetaRegaloEmpresa().getAdministradora();
		//					Integer codAu = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();
		//					Integer emisor = trama.getTarjetaRegaloEmpresa().getEmisor();
		//					Integer flag = trama.getTarjetaRegaloEmpresa().getFlag();
		//					Saldos saldos = new Saldos();
		//					saldos = getSaldos(trama);
		//					sql = "";
		//					String rutCliente = trama.getTarjetaRegaloEmpresa().getNroTarjeta().toString();
		//					long montoTarRegalo = 0l, wMontoTre = 0l; // wMontoTotal =
		//																// 0l,
		//				
		//					if (wsFlagNcTre == 1) {
		//						if (tipo == 37) {
		//							montoTarRegalo = saldos.getSaldoTRE();
		//							// wMontoTotal = saldos.getMontoTarRipley() +
		//							// saldos.getSaldoTRE();
		//							if (saldos.getMontoTarRipley() < glTotalNc) {
		//								wMontoTre = glTotalNc - saldos.getMontoTarRipley();
		//							}
		//						} else {
		//							montoTarRegalo = glTotalNc;
		//						}
		//
		//						procSQL = "{CALL actualiza_tar_regalo_empresa(to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						sql = "CALL actualiza_tar_regalo_empresa(to_date('"
		//								+ fecha;
		//						cs.setInt(2, sucursal);
		//						sql = sql + "','dd/mm/yyyy')," + sucursal;
		//						cs.setInt(3, nroCaja);
		//						sql = sql + "," + nroCaja;
		//						cs.setInt(4, transaccion);
		//						sql = sql + "," + transaccion;
		//						cs.setInt(5, codAdmin);
		//						sql = sql + "," + codAdmin;
		//						cs.setInt(6, emisor);
		//						sql = sql + "," + emisor;
		//						cs.setInt(7, wsTipo);
		//						sql = sql + "," + wsTipo;
		//						cs.setInt(8, codAu);
		//						sql = sql + "," + codAu;
		//
		//						if (wMontoTre > 0) {
		//							cs.setString(9, rutCliente);
		//							sql = sql + ",'" + rutCliente;
		//							cs.setLong(10, wMontoTre);
		//							sql = sql + "'," + wMontoTre;
		//							cs.setInt(11, nroTarjeta);
		//							sql = sql + "," + nroTarjeta;
		//							cs.setInt(12, flag);
		//							sql = sql + "," + flag;
		//
		//						} else {
		//							cs.setString(9, rutCliente);
		//							sql = sql + "'," + rutCliente;
		//							cs.setLong(10, montoTarRegalo);
		//							sql = sql + "," + montoTarRegalo;
		//							cs.setInt(11, nroTarjeta);
		//							sql = sql + "," + nroTarjeta;
		//							cs.setInt(12, flag);
		//							sql = sql + "," + flag;
		//						}
		//						cs.registerOutParameter(13, OracleTypes.NUMBER);
		//						cs.registerOutParameter(14, OracleTypes.VARCHAR);
		//
		//						logger.traceInfo("AnulacionMKP", sql + ",outn,outv)}");
		//
		//						cs.execute();
		//						sal = new Integer(cs.getInt(13));
		//						sal2 = cs.getString(14);
		//						cs.close();
		//						if (Validaciones.validaInteger(sal)
		//								&& sal.intValue() != Constantes.NRO_CERO) {
		//							logger.traceError(
		//									" Creacion TRE BackOffice - ",
		//									"--> Imposible crear forma de Pago Tarjeta Regalos Empresa Error - "
		//											+ sql,
		//									new Exception("Resultado obtenido: "
		//											+ sal.toString()));
		//
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (conn != null) {
		//								PoolBDs.closeConnection(conn);
		//							}
		//							return false;
		//						}
		//
		//						procSQL = "{CALL FEGIFE_PRC_ACT_SLD_NC(?,?,?,?,to_date(?,'DD/MM/YYYY'),?,?)}";
		//						connTRE = PoolBDs.getConnection(BDType.TRE);
		//						sql = Constantes.VACIO;
		//						cs = conn.prepareCall(procSQL);
		//						cs.setInt(1, nroTarjeta);
		//						sql = "{CALL FEGIFE_PRC_ACT_SLD_NC(" + nroTarjeta;
		//						if (wMontoTre > 0) {
		//							cs.setLong(2, wMontoTre);
		//							sql = sql + "," + wMontoTre;
		//						} else {
		//							cs.setLong(2, montoTarRegalo);
		//							sql = sql + "," + montoTarRegalo;
		//						}
		//						cs.setLong(3, nroCaja);
		//						sql = sql + "," + nroCaja;
		//						cs.setInt(4, trama.getNotaVenta().getCorrelativoVenta());
		//						sql = sql + ","
		//								+ trama.getNotaVenta().getCorrelativoVenta();
		//						cs.setString(5, fecha);
		//						sql = sql + ",to_date(" + fecha + ",'DD/MM/YYYY')";
		//						cs.registerOutParameter(6, OracleTypes.NUMBER);
		//						cs.registerOutParameter(7, OracleTypes.VARCHAR);
		//						logger.traceInfo("AnulacionMKP", sql + ",outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(13));
		//						sal2 = cs.getString(14);
		//						cs.close();
		//						if (Validaciones.validaInteger(sal)
		//								&& sal.intValue() != Constantes.NRO_CERO) {
		//							logger.traceError(
		//									" Creacion TRE BackOffice -  ",
		//									"--> Imposible Actualizar Saldo Tarjeta Regalos Empresa Error - "
		//											+ sql,
		//									new Exception("Resultado obtenido: "
		//											+ sal.toString()));
		//
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (conn != null) {
		//								PoolBDs.closeConnection(conn);
		//							}
		//							return false;
		//						}
		//					}
		//				}
		//
		//				List<ArticuloVentaTramaDTE> articuloVentas = trama
		//						.getArticuloVentas();
		//
		//				if (articuloVentas != null && articuloVentas.size() > 0) {
		//					for (int i = 0; i < articuloVentas.size(); i++) {
		//						ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
		//						if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO) {
		//							continue;
		//						}
		//
		//						// creando variables de la trama para uso en package
		//						Integer correlativoVenta = trama.getDespacho().getCorrelativoVenta();
		//
		//						String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
		//						Integer rutClienteDos = trama.getDespacho().getRutCliente();
		//						String rutCliente = rutClienteDos.toString() + Util.getDigitoValidadorRut2(rutClienteDos.toString());
		//						String emailCliete = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
		//						String nombreInvitado = Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
		//						String apellidoPaterno = null;
		//						String apellidoMaterno = null;
		//
		//						if (trama.getDespacho().getApellidoPatCliente() != null) {
		//							apellidoPaterno = trama.getDespacho().getApellidoPatCliente();
		//						} else {
		//							apellidoPaterno = Util.cambiaCaracteres("Apellido no descrito");
		//						}
		//
		//						if (trama.getDespacho().getApellidoPatCliente() != null) {
		//							apellidoMaterno = trama.getDespacho().getApellidoMatCliente();
		//						} else {
		//							apellidoMaterno = Util.cambiaCaracteres("Apellido no descrito");
		//						}
		//						// desde Nota de ventas
		//						Integer codigoRegalo = 0;
		//						if (trama.getNotaVenta().getCodigoRegalo() != null && trama.getNotaVenta().getCodigoRegalo() > 0) {
		//							codigoRegalo = trama.getNotaVenta().getCodigoRegalo();
		//						}
		//						String mensajeTarjeta = trama.getDespacho().getMensajeTarjeta() != null ? trama.getDespacho().getMensajeTarjeta() : Constantes.VACIO;
		//						// String venedorDos = vendedor.toString() +
		//						// Util.getDigitoValidadorRut2(rutClienteDos.toString());
		//						// Articulo de ventas
		//						String costoEnvio = articuloVentaTrama
		//								.getArticuloVenta().getDescRipley() != null ? articuloVentaTrama
		//								.getArticuloVenta().getDescRipley()
		//								: Constantes.VACIO;
		//						// String jornadaDespacho =
		//						// articuloVentaTrama.getArticuloVenta().getCodDespacho();
		//						// String jornadaDespachoBo = null;
		//						// if
		//						// (jornadaDespacho.equalsIgnoreCase(Constantes.BOL_JORNADA_AM))
		//						// {
		//						// jornadaDespachoBo = Constantes.STRING_UNO;
		//						// } else if
		//						// (jornadaDespacho.equalsIgnoreCase(Constantes.BOL_JORNADA_PM))
		//						// {
		//						// jornadaDespachoBo = Constantes.STRING_DOS;
		//						// }
		//						int tipoRegalo = 0;
		//						if (tieneCud(
		//								articuloVentaTrama.getArticuloVenta().getCodArticulo()).intValue() == 0) {
		//							if (articuloVentaTrama.getArticuloVenta().getEsregalo() != null) {
		//								if ("S".equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getEsregalo())
		//										&& !mensajeTarjeta.equalsIgnoreCase(Constantes.VACIO)
		//										&& mensajeTarjeta.indexOf("99") != 0) {
		//									tipoRegalo = 99;
		//								}
		//							}
		//						}
		//						// Creaci�n de Despacho para Big Ticket
		//						String cud = null;
		//						String codDespacho = articuloVentaTrama.getArticuloVenta().getCodDespacho().trim();
		//						if (!Constantes.VACIO.equalsIgnoreCase(costoEnvio)	&& (costoEnvio.contains(Constantes.TRAMA_COSTO_ENVIO) || costoEnvio.contains(Constantes.TRAMA_EG_EXT))) {
		//							cud = "";
		//						} else {
		//							if (((codDespacho.trim().length()) < 22) && codDespacho.length() > 0 ) {
		//								cud = codDespacho.substring(0, 14) + Constantes.CHAR_CERO + codDespacho.substring(17, 4);
		//							} else {
		//								cud = codDespacho.trim();
		//							}
		//						}
		//
		//						Integer vCantPrd = articuloVentaTrama.getArticuloVenta().getUnidades();
		//						Integer vPrcCmp = null;
		//						Integer precio = null;
		//						// Se calcula precio
		//						if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() < 0) {
		//							precio = articuloVentaTrama.getArticuloVenta().getPrecio() - articuloVentaTrama.getArticuloVenta().getMontoDescuento();
		//						} else {
		//							//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
		//							precio = (int) (articuloVentaTrama.getArticuloVenta().getPrecio() - Math.floor(articuloVentaTrama.getArticuloVenta().getMontoDescuento() / vCantPrd));
		//						}
		//
		//						vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); //merge .22
		//						Integer correlativoItem = articuloVentaTrama.getArticuloVenta().getCorrelativoItem();
		//						
		//						if ((codigoRegalo.intValue() > Constantes.NRO_CERO) && (cud.length() > 0)){
		//							// CLUB CONEXION
		//						//	connClaseS = PoolBDs.getConnection(BDType.CLUB);
		//							RetornoEjecucion retorno = new RetornoEjecucion();
		//							
		//							
		//							procSQL = "{CALL lglreg_prc_mov_lis_vlr(?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//							connClaseS = PoolBDs.getConnection(BDType.CLUB);
		//							cs = connClaseS.prepareCall(procSQL);
		//							cs.setInt(1, codigoRegalo);
		//							cs.setString(2, articuloVentaTrama.getArticuloVenta().getCodArticulo());
		//							cs.setInt(3, sucursal);
		//							cs.setString(4, fecha);
		//							cs.setInt(5, transaccion);
		//							cs.setInt(6, 37);// Info en duro
		//							cs.setInt(7, 1); // Info en duro
		//							cs.setInt(8, 0); // Info en duro
		//							cs.setInt(9, vCantPrd);// REVISAR v_cant_prd
		//							cs.setInt(10, vPrcCmp);// v_prc_cmp
		//							cs.setInt(11, transaccion);// v_boleta
		//							cs.setInt(12, nroCaja);
		//							cs.setInt(13, tipo);// revisar que tipo
		//							cs.setInt(14, correlativoItem);
		//							cs.setString(15, direccionDespacho);
		//							cs.setString(16, "< SR >");
		//							cs.setString(17, Constantes.VACIO);
		//							cs.setString(18, emailCliete);
		//							cs.setString(19, nombreInvitado);
		//							cs.setString(20, apellidoPaterno);
		//							cs.setString(21, apellidoMaterno);
		//							cs.setString(22, Constantes.VACIO);
		//							cs.setInt(23, Constantes.NRO_CERO);
		//							if (tipoRegalo == 99)
		//								cs.setString(24, Constantes.STRING_CERO);
		//							else
		//								cs.setString(24, mensajeTarjeta);
		//							cs.setInt(25, Constantes.NRO_CERO);
		//							cs.setInt(26, Constantes.NRO_CERO);
		//							cs.setInt(27, Constantes.NRO_CERO);
		//							cs.setInt(28, Constantes.NRO_CERO);
		//							cs.registerOutParameter(29, OracleTypes.NUMBER);
		//							cs.registerOutParameter(30, OracleTypes.VARCHAR);
		//
		//							sql = "{CALL lglreg_prc_mov_lis_vlr("
		//									+ codigoRegalo
		//									+ ",'"
		//									+ articuloVentaTrama.getArticuloVenta().getCodArticulo()
		//									+ "',"
		//									+ sucursal
		//									+ ",to_date('"
		//									+ fecha
		//									+ "','DD/MM/YYYY'),"
		//									+ transaccion
		//									+ ",37,1,0,"
		//									+ vCantPrd
		//									+ ","
		//									+ vPrcCmp
		//									+ ","
		//									+ transaccion
		//									+ ","
		//									+ nroCaja
		//									+ ","
		//									+ tipo
		//									+ ","
		//									+ correlativoItem
		//									+ ",'"
		//									+ direccionDespacho
		//									+ "','< SR >','','"
		//									+ emailCliete
		//									+ "','"
		//									+ nombreInvitado
		//									+ "','"
		//									+ apellidoPaterno
		//									+ "','"
		//									+ apellidoMaterno
		//									+ "','',0,"
		//									+ ((tipoRegalo == 99) ? '0'
		//											: mensajeTarjeta)
		//									+ ",0,0,0,0,outn,outv)}";
		//							logger.traceInfo("AnulacionMKP", sql);
		//
		//							cs.execute();
		//							logger.traceInfo("AnulacionMKP", "Creacion de REGISTRO NOVIOS ");
		//							sal = new Integer(cs.getInt(29));
		//							sal2 = cs.getString(30);
		//							cs.close();
		//							if (Validaciones.validaInteger(sal)
		//									&& sal.intValue() != Constantes.NRO_UNO) {
		//								logger.traceError(
		//										"Creacion de REGISTRO NOVIOS",
		//										"--> Imposible de grabar en Novios Error - "
		//												+ sql,
		//										new Exception("Resultado obtenido: "
		//												+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								logger.endTrace("AnulacionMKP", "Finalizado",
		//										"boolean: false");
		//								return false;
		//							}
		//							
		//						
		//						}else{
		//							
		//							boolean esCosto = false;
		//							
		//							System.out.println(articuloVentaTrama.getArticuloVenta().getDescRipley());
		//							
		//							if (!Constantes.VACIO.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getDescRipley())	
		//									&& ((articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_COSTO_ENVIO) || 
		//									    articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_EXT)
		//									|| articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_REEM)))) {
		//								    esCosto = true;
		//							}
		//							
		//							
		//							
		//							if ((articuloVentaTrama.getArticuloVenta().getCodDespacho() == null || articuloVentaTrama.getArticuloVenta().getCodDespacho().trim().equalsIgnoreCase("")) && !esCosto){ //JPRM
		//							
		//							RetornoEjecucion retorno = new RetornoEjecucion();
		//							retorno = clubDAO.lglreg_prc_mov_lis_vlr_mkp(	codigoRegalo.toString(),
		//									Long.parseLong((articuloVentaTrama.getArticuloVenta().getCodArticuloMkp() != null)?articuloVentaTrama.getArticuloVenta().getCodArticuloMkp():articuloVentaTrama.getArticuloVenta().getCodArticulo()),
		//									sucursal, 
		//									FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3), 
		//									transaccion, 
		//									Constantes.NRO_TREINTASIETE,
		//									Constantes.NRO_UNO,
		//									Constantes.NRO_CERO,
		//									vCantPrd, 
		//									vPrcCmp, 
		//									transaccion, 
		//									nroCaja, 
		//									tipo, 
		//									correlativoItem, 
		//									direccionDespacho, 
		//									Constantes.MSJ_REGALO_NR, 
		//									Constantes.NRO_CERO, 
		//									emailCliete, 
		//									nombreInvitado, 
		//									apellidoPaterno, 
		//									apellidoMaterno, 
		//									Constantes.VACIO, 
		//									Constantes.NRO_CERO, 
		//									mensajeTarjeta, 
		//									Constantes.NRO_CERO, 
		//									Constantes.NRO_CERO, 
		//									Constantes.NRO_CERO, 
		//									Constantes.NRO_CERO, 
		//									articuloVentaTrama.getArticuloVenta().getCodArticulo()+"-"+articuloVentaTrama.getArticuloVenta().getDescRipley()
		//								);
		//
		//							if(retorno.getCodigo() != Constantes.EJEC_SIN_ERRORES){
		//							logger.traceInfo("AnulacionMKP", "Imposible de grabar en Novios Error - Mensaje: " + retorno.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())));
		//							} else {
		//							logger.traceInfo("AnulacionMKP", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())), new KeyLog("Correlativo item", String.valueOf(articuloVentaTrama.getArticuloVenta().getCorrelativoItem())));
		//							}
		//
		//						}
		//
		//						String codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
		//						
		//						if (!Validaciones.validarVacio(codArticulo)) { // Nuevo
		//																		// 8.0.4
		//							// if (codArticulo != Constantes.VACIO) {
		//							connExtend = PoolBDs
		//									.getConnection(BDType.MODEL_EXTEND);
		//							procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA(?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY hh24:mi:ss'),?,?,?,?,?,?,?,?)}";
		//							cs = connExtend.prepareCall(procSQL);
		//							cs.setInt(1, transaccion);
		//							cs.setString(2, fecha);
		//							cs.setString(3, fechaHora);
		//							cs.setInt(4, nroCaja);
		//							cs.setInt(5, transaccion);
		//							cs.setString(6, vendedor);
		//							cs.setInt(7, correlativoVenta);
		//							cs.setString(8, (cud.length() > 0) ? cud : Constantes.SPACE);//codArticulo);
		//							cs.setInt(9, correlativoItem);
		//							cs.registerOutParameter(10, OracleTypes.NUMBER);
		//							cs.registerOutParameter(11, OracleTypes.VARCHAR);
		//
		//							sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA("
		//									+ transaccion
		//									+ ",TO_DATE('"
		//									+ fecha
		//									+ "','DD/MM/YYYY'),TO_DATE('"
		//									+ fechaHora
		//									+ "','DD/MM/YYYY hh24:mi:ss'),"
		//									+ nroCaja
		//									+ ","
		//									+ transaccion
		//									+ ",'"
		//									+ vendedor
		//									+ "',"
		//									+ correlativoVenta
		//									+ ",'"
		//									+ ((cud.length() > 0) ? cud : Constantes.SPACE)
		//									+ "',"
		//									+ correlativoItem
		//									+ ",outn,outv)}";
		//							logger.traceInfo("AnulacionMKP", sql);
		//
		//							cs.execute();
		//
		//							sal = new Integer(cs.getInt(10));
		//							sal2 = cs.getString(11);
		//							cs.close();
		//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//								logger.traceError(
		//										"Creacion NC T.Bancaria BackOffice",
		//										"--> Imposible crear forma de Pago Bancaria Error - "
		//												+ sql,
		//										new Exception("Resultado obtenido: "
		//												+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								return false;
		//							}
		//							logger.traceInfo("AnulacionMKP", "UPDATE_ARTICULO_VENTA: "+sal);
		//
		//
		//						}
		//					}//JPRM
		//
		//					} // fin del for
		//				} // fin If articulos
		//
		//				// ---------creaci�n transaccion
		//				if (trama.getNotaVenta().getCorrelativoVenta() != null) {
		//					int correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		//					tipo = 37;
		//					String fechaBoleta = FechaUtil.formatTimestamp(
		//							Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaBoleta());
		//
		//					if (!(Constantes.STRING_CERO).equalsIgnoreCase(rutEjecutivoVta)) {
		//						if (rutEjecutivoVta != null && rutEjecutivoVta.length() > 1){
		//							vendedor = rutEjecutivoVta.substring(0, rutEjecutivoVta.length() - Constantes.NRO_UNO);
		//						}
		//					}
		//					// Nuevo 8.0.12
		//
		//					String rutDespacho = (trama.getDespacho().getRutDespacho() != null)
		//							? trama.getDespacho().getRutDespacho().toString()
		//							: Constantes.STRING_CERO;
		//					conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//					logger.traceInfo("AnulacionMKP", "inserta_recaudacion_bo");
		//					procSQL = "{CALL ? := INSERTA_RECAUDACION_BO(TO_DATE(?,'DD/MM/YYYY'),?,?,?,32000,9001,0,?,?,?,1)}";
		//					cs = conn.prepareCall(procSQL);
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					cs.setString(2, fecha);
		//					cs.setInt(3, sucursal);
		//					cs.setInt(4, nroCaja);
		//					cs.setInt(5, transaccion);
		//					cs.setString(6, "37-" + correlativoVenta);
		//					cs.setLong(7, glTotalNc);
		//					cs.setString(8, rutDespacho);
		//					sql = "CALL outCod := INSERTA_RECAUDACION_BO(TO_DATE("
		//							+ fecha + ",'DD/MM/YYYY')" + "," + sucursal + ","
		//							+ nroCaja + "," + transaccion + ",32000,9001,0"
		//							+ ",37-" + correlativoVenta + "," + glTotalNc + ","
		//							+ rutDespacho + ",1)}";
		//					logger.traceInfo("AnulacionMKP", sql);
		//					cs.execute();
		//
		//					sal = new Integer(cs.getInt(1));
		//					if (Validaciones.validaInteger(sal)
		//							&& sal.intValue() == Constantes.NRO_UNO) {
		//						logger.traceError(
		//								" Inserta Transaccion BackOffice - " + procSQL,
		//								"--> Imposible crear recaudacion en monedero  Error - "
		//										+ sql,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//
		//					// Nuevo 8.0.12
		//					// 'Consulta para insertar a la tabla recaudacion_prv
		//					List<IdentificadorMarketplace> tramaIdentificadorMkp = trama.getIdentificadoresMarketplace();
		//
		//					if (tramaIdentificadorMkp.size() > 0) {
		//						for (IdentificadorMarketplace objLista : tramaIdentificadorMkp) {
		//							Integer glTotalNCxPrvMkp = getTotalNcMKPxPrv(trama, objLista.getOrdenMkp());
		//							if (glTotalNCxPrvMkp != null && objLista.getEsMkp().intValue() == Constantes.NRO_UNO) {
		//								String rutProveedorMkp = objLista.getRutProveedor() != null ? objLista.getRutProveedor() : Constantes.STRING_CERO;
		//								conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//								logger.traceInfo("AnulacionMKP", "inserta_recaudacion_prv_bo");
		//								procSQL = "{CALL ? := inserta_recaudacion_prv_bo(TO_DATE(?,'DD/MM/YYYY'),?,?,?,32000,?,?,0)}";
		//								cs = conn.prepareCall(procSQL);
		//								cs.registerOutParameter(1, OracleTypes.NUMBER);
		//								cs.setString(2, fecha);
		//								cs.setInt(3, sucursal);
		//								cs.setInt(4, nroCaja);
		//								cs.setInt(5, transaccion);
		//								cs.setString(6, rutProveedorMkp);
		//								cs.setInt(7, glTotalNCxPrvMkp.intValue());
		//								sql = "outNcod := inserta_recaudacion_prv_bo(TO_DATE("
		//										+ fecha
		//										+ ",'DD/MM/YYYY')"
		//										+ ","
		//										+ sucursal
		//										+ ","
		//										+ nroCaja
		//										+ ","
		//										+ transaccion
		//										+ ",32000"
		//										+ ","
		//										+ rutProveedorMkp
		//										+ ","
		//										+ glTotalNCxPrvMkp.intValue() + ",0)}";
		//								logger.traceInfo("AnulacionMKP", sql);
		//								cs.execute();
		//
		//								sal = new Integer(cs.getInt(1));
		//
		//								if (Validaciones.validaInteger(sal)
		//										&& sal.intValue() == Constantes.NRO_UNO) {
		//									logger.traceError(
		//											"Inserta Transaccion BackOffice - "
		//													+ procSQL,
		//											"--> Imposible crear recaudacion en monedero  Error - "
		//													+ sql, new Exception(
		//													"Resultado obtenido: "
		//															+ sal.toString()));
		//
		//									// se obtiene medio de pago
		//									int tipoDePago = 37;
		//									int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//									int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//									// Se ejecuta anulaci�n de folio, si no
		//									// logra anular, concatena mensaje.
		///*
		//									retornoEjecucion = anulacionFolio(
		//											FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4),
		//											Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")),
		//											sucursal, nroCaja, transaccion,
		//											transaccion, trama.getNotaVenta().getCorrelativoVenta(),
		//											tipoDePago, rutSupervisor,
		//											rutVendedor);
		//
		//									logger.endTrace("AnulacionMKP", "Finalizado", "boolean: false");
		//*/
		//									if (conn != null) {
		//										PoolBDs.closeConnection(conn);
		//									}
		//									return false;
		//								}
		//							}
		//						}
		//					}
		//
		//					conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//					logger.traceInfo("AnulacionMKP",
		//							"inserta_transaccion_back3");
		//					procSQL = "{CALL ? := inserta_transaccion_back3(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY hh24:mi:ss'),?,?,?,?,to_date(?,'DD/MM/YYYY'))}";
		//					cs = conn.prepareCall(procSQL);
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					cs.setString(2, fecha);
		//					cs.setInt(3, sucursal);
		//					cs.setInt(4, nroCaja);
		//					cs.setInt(5, transaccion);
		//					cs.setInt(6, transaccion);
		//					cs.setInt(7, tipo);
		//					cs.setLong(8, glTotalNc);
		//					cs.setInt(9, sucursal);
		//					cs.setString(10, fechaBoleta);// FIXME merge .20
		//					cs.setInt(11, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA_BOLETAS")));// trama.getNotaVenta().getNumeroCaja());
		//					cs.setInt(12, trama.getNotaVenta().getNumeroBoleta());// FIXME merge .20
		//					cs.setInt(13, Constantes.NRO_CERO);
		//					cs.setInt(14, Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR")));
		//					if (vendedor != null && !vendedor.isEmpty()) { //TODO revisar en cuadratura
		//						vendedor = (vendedor.indexOf('-') > Constantes.NRO_CERO) ? vendedor.substring(0, vendedor.indexOf('-')) : vendedor;
		//					} else {
		//						vendedor = Constantes.STRING_CERO;
		//					}
		//					cs.setInt(15, Integer.parseInt(vendedor));
		//					cs.setInt(16, Constantes.NRO_CERO);
		//					cs.setInt(17, trama.getNotaVenta().getRutComprador());
		//					cs.setInt(18, trama.getNotaVenta().getMontoDescuento());
		//					cs.setInt(19, Constantes.NRO_CERO);
		//					cs.setInt(20, Constantes.NRO_CERO);
		//					cs.setString(21, fechaHora);
		//					cs.setInt(22, Constantes.NRO_CERO);
		//					cs.setInt(23, Constantes.NRO_CERO);
		//					cs.setInt(24, correlativoVenta);
		//					cs.setInt(25, trama.getNotaVenta().getCorrelativoBoleta());
		//					cs.setString(26, FechaUtil.formatTimestamp(
		//							Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaCreacion()));
		//
		//					sql = "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_TRANSACCION_BACK3(TO_DATE("
		//							+ fecha
		//							+ ",'DD/MM/YYYY'),"
		//							+ sucursal
		//							+ ","
		//							+ nroCaja
		//							+ ","
		//							+ transaccion
		//							+ ","
		//							+ transaccion
		//							+ ","
		//							+ tipo
		//							+ ","
		//							+ glTotalNc
		//							+ ","
		//							+ sucursal
		//							+ ",TO_DATE('"
		//							+ fechaBoleta
		//							+ "','dd/MM/yyyy'),"
		//							+ Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA_BOLETAS"))
		//							+ ","
		//							+ trama.getNotaVenta().getNumeroBoleta()
		//							+ ",0,"
		//							+ Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"))
		//							+ ","
		//							+ vendedor
		//							+ ",0,"
		//							+ trama.getNotaVenta().getRutComprador()
		//							+ ","
		//							+ trama.getNotaVenta().getMontoDescuento()
		//							+ ","
		//							+ "0,0,to_date("
		//							+ fechaHora
		//							+ ",'DD/MM/YYYY hh24:mi:ss'),"
		//							+ "0,0,"
		//							+ correlativoVenta
		//							+ ","
		//							+ trama.getNotaVenta().getCorrelativoBoleta()
		//							+ ","
		//							+ FechaUtil.formatTimestamp(
		//									Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaCreacion())
		//							+ ")";
		//
		//					logger.traceInfo("AnulacionMKP", sql);
		//					cs.execute();
		//					sal = new Integer(cs.getInt(1));
		//					if (Validaciones.validaInteger(sal)
		//							&& sal.intValue() != Constantes.NRO_CERO) {
		//						logger.traceError(
		//								"Inserta NC Transaccion BackOffice - "
		//										+ procSQL,
		//								"--> Imposible crear Encabezado Transaccion Error - "
		//										+ sql,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						caja.saltarFolio(pcv, transaccion);
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					} else {
		//
		//						// String tipoIdentNc =
		//						// trama.getNotaVenta().getIndicadorMkp();
		//
		//						List<IdentificadorMarketplace> listaMkp = trama.getIdentificadoresMarketplace();
		//
		//						for (IdentificadorMarketplace identificadorMarketplace : listaMkp) {
		//
		//							// Nuevo 7.5.36
		//							// Nuevo 8.0.12
		//							if (trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL) {
		//								procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA(?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)}";
		//								if (connExtend == null) {
		//									connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//								}
		//								cs = connExtend.prepareCall(procSQL);
		//								cs.setInt(1, Constantes.NRO_TRES);
		//								cs.setInt(2, transaccion);
		//								cs.setNull(3, OracleTypes.NUMBER);
		//								//cs.setInt(3, transaccion);
		//								cs.setString(4, fecha);
		//								cs.setString(5, fechaHora);
		//								cs.setInt(6, nroCaja);
		//								cs.setInt(7, transaccion);
		//								cs.setInt(8, Integer.parseInt(vendedor));
		//								cs.setInt(9, correlativoVenta);
		//								cs.registerOutParameter(10, OracleTypes.NUMBER);
		//								cs.registerOutParameter(11, OracleTypes.VARCHAR);
		//								// Log
		//								sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA"
		//										+ "("
		//										+ Constantes.NRO_TRES
		//										+ ","
		//										+ transaccion
		//										+ ",null"
		//										//+ transaccion
		//										+ ",to_date("
		//										+ fecha
		//										+ ",'DD/MM/YYYY'),"
		//										+ "to_date("
		//										+ fechaHora
		//										+ ",'DD/MM/YYYY HH24:MI:SS')"
		//										+ ","
		//										+ nroCaja
		//										+ ","
		//										+ transaccion
		//										+ ","
		//										+ Integer.parseInt(vendedor)
		//										+ ","
		//										+ correlativoVenta
		//										+ ",outN"
		//										+ ",outV)}";
		//								logger.traceInfo("AnulacionMKP", sql);
		//								cs.execute();
		//								sal = new Integer(cs.getInt(10));
		//								cs.close();
		//
		//							} else if (trama.getNotaVenta().getEstado()
		//									.intValue() == Constantes.NOTA_CREDITO_PARCIAL) {
		//								procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA(?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)}";
		//								if (connExtend == null) {
		//									connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//								}
		//								cs = connExtend.prepareCall(procSQL);
		//								cs.setInt(1, Constantes.NRO_CUATRO);
		//								cs.setInt(2, transaccion);
		//								cs.setNull(3, OracleTypes.NUMBER);
		//								//cs.setInt(3, transaccion);
		//								cs.setString(4, fecha);
		//								cs.setString(5, fechaHora);
		//								cs.setInt(6, nroCaja);
		//								cs.setInt(7, transaccion);
		//								cs.setInt(8, Integer.parseInt(vendedor));
		//								cs.setInt(9, correlativoVenta);
		//								cs.registerOutParameter(10, OracleTypes.NUMBER);
		//								cs.registerOutParameter(11, OracleTypes.VARCHAR);
		//								// Log
		//								sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA"
		//										+ "("
		//										+ Constantes.NRO_CUATRO
		//										+ ","
		//										+ transaccion
		//										+ ",null"
		//										//+ transaccion
		//										+ ",to_date("
		//										+ fecha
		//										+ ",'DD/MM/YYYY'),"
		//										+ "to_date("
		//										+ fechaHora
		//										+ ",'DD/MM/YYYY HH24:MI:SS')"
		//										+ ","
		//										+ nroCaja
		//										+ ","
		//										+ transaccion
		//										+ ","
		//										+ Integer.parseInt(vendedor)
		//										+ ","
		//										+ correlativoVenta
		//										+ ",outN"
		//										+ ",outV)}";
		//								logger.traceInfo("AnulacionMKP", sql);
		//								cs.execute();
		//								sal = new Integer(cs.getInt(10));
		//								cs.close();
		//
		//							}
		//
		//							if (Validaciones.validaInteger(sal)
		//									&& sal.intValue() != Constantes.NRO_CERO) {
		//								logger.traceError(
		//										"UPDATE NOTA_VENTA ",
		//										"--> CALL "
		//												+ procSQL
		//												+ "Actualizacion NOTA_VENTA termino de la transacciones: - "
		//												+ sql,
		//										new Exception("Resultado obtenido: "
		//												+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								return false;
		//							}
		//
		//							String ordenMkp = (Constantes.VACIO
		//									.equalsIgnoreCase(identificadorMarketplace
		//											.getOrdenMkp())) ? identificadorMarketplace
		//									.getOrdenMkp() : Constantes.VACIO;
		//
		//							if (!esMkpSinStock(trama, correlativoVenta,
		//									ordenMkp)) {// nuevo 8.0.4
		//								// Nuevo insert MIRAKL 7.5.36
		//								String rutCompradorMkp = (trama.getNotaVenta().getRutComprador()!=null)?trama.getNotaVenta().getRutComprador().toString():Constantes.STRING_CERO;
		//								rutCompradorMkp = rutCompradorMkp + ((trama.getNotaVenta().getDvComprador()!=null)?trama.getNotaVenta().getDvComprador().toString():Constantes.STRING_CERO);
		//
		//								int rutComprador = (trama.getNotaVenta().getRutComprador() != null) ? trama.getNotaVenta().getRutComprador().intValue() : Constantes.NRO_CERO;
		//								procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL(?,?,?,?,TO_TIMESTAMP(?,'"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),?,?,?,?,?,?,?)}";
		//								if (connExtend == null) {
		//									connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//								}
		//								cs = connExtend.prepareCall(procSQL);
		//								cs.setInt(1, correlativoVenta);
		//								cs.setString(2, Constantes.PA_DOS);
		//								cs.setString(3, identificadorMarketplace.getOrdenMkp());
		//								cs.setInt(4, Constantes.MKP_PENDIENTE_PETICION);//identificadorMarketplace.getEstado());
		//								cs.setString(5, fechaHora);
		//								cs.setString(6, pcv.buscaValorPorNombre("USUARIO"));
		//								cs.setInt(7, identificadorMarketplace.getAmount().intValue());
		//								//cs.setInt(8, rutComprador);
		//								cs.setString(8, rutCompradorMkp);
		//								cs.setInt(9, identificadorMarketplace.getIdRefund().intValue());
		//								cs.registerOutParameter(10, OracleTypes.NUMBER);
		//								cs.registerOutParameter(11, OracleTypes.VARCHAR);
		//								cs.registerOutParameter(12, OracleTypes.VARCHAR);
		//								sql = "{CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL("
		//										+ correlativoVenta
		//										+ ","
		//										+ Constantes.PA_DOS
		//										+ ","
		//										+ identificadorMarketplace.getOrdenMkp()
		//										+ ","
		//										+ identificadorMarketplace.getEstado()
		//										+ ",TO_TIMESTAMP('"
		//										+ fechaHora
		//										+ "','"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),"
		//										+ pcv.buscaValorPorNombre("USUARIO")
		//										+ ", "
		//										+ identificadorMarketplace.getAmount().intValue()
		//										+ ", '"
		//										+ rutCompradorMkp
		//										+ "', "
		//										+ identificadorMarketplace.getIdRefund().intValue()
		//										+ ")}";
		//								logger.traceInfo("AnulacionMKP", sql);
		//								cs.execute();
		//								sal = new Integer(cs.getInt(10));
		//								procSQL = sql;
		//								sql = cs.getString(12);
		//
		//								if (Validaciones.validaInteger(sal)
		//										&& sal.intValue() != Constantes.NRO_CERO) {
		//									logger.traceError(
		//											"INSERT ENVIO_ESTADO_OC_MIRAKL ",
		//											"--> CALL: "
		//													+ procSQL
		//													+ " Actualizacion ENVIO_ESTADO_OC_MIRAKL Insert realizado: - "
		//													+ sql, new Exception(
		//													"Resultado obtenido: "
		//															+ sal.toString()));
		//
		//									// se obtiene medio de pago
		//									int tipoDePago = 37;
		//									int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//									int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//									if (conn != null) {
		//										PoolBDs.closeConnection(conn);
		//									}
		//									return false;
		//								}
		//							} // If esMkpSinStock
		//						} // Fin for articulo de ventas
		//					}
		//				}
		//				AnulacionMKP = true;
		//			} else {
		//
		//				logger.traceInfo("AnulacionMKP",
		//						"Problemas de conexion a la base de datos");
		//			}
		//
		//		} catch (Exception e) {
		//			logger.traceError("AnulacionMKP",
		//					"Error durante la ejecuci�n de AnulacionMKP", e);
		//			// se obtiene medio de pago
		//			int tipoDePago = 37;
		//			int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//			int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//			return false;
		//		} finally {
		//			if (conn != null)
		//				PoolBDs.closeConnection(conn);
		//			if (connClaseS != null)
		//				PoolBDs.closeConnection(connClaseS);
		//			if (connExtend != null)
		//				PoolBDs.closeConnection(connExtend);
		//			if (connTRE != null)
		//				PoolBDs.closeConnection(connTRE);
		//
		//		}
		//
		logger.endTrace("AnulacionMKP", "Finalizado", "boolean: "
				+ AnulacionMKP);
		return AnulacionMKP;
	}

	public boolean anulacionMKP2(TramaDTE trama, Parametros pcv, String fecha, String fechaHora, Integer transaccion) throws AligareException {
		logger.initTrace("AnulacionMKP", "Parametros: parametros");

		boolean anulacionMkp = Boolean.TRUE;

		int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		int sucursal = Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"));
		String vendedor = pcv.buscaValorPorNombre("RUT_VENDEDOR");
		Long correlativoVenta = trama.getDespacho().getCorrelativoVenta();

		String sql = Constantes.VACIO;

		CallableStatement cs = null;

		String procSQL = "";
		Integer sal = null;
		String sal2 = Constantes.VACIO;

		try(Connection connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND)) {

			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();

			if (articuloVentas != null && articuloVentas.size() > 0) {
				for (int i = 0; i < articuloVentas.size(); i++) {
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO) {
						continue;
					}

					// creando variables de la trama para uso en package
					String codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();

					if (!Validaciones.validarVacio(codArticulo)) { // Nuevo
						// 8.0.4
						procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA(?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY hh24:mi:ss'),?,?,?,?,?,?,?,?)}";
						cs = connExtend.prepareCall(procSQL);
						cs.setInt(1, transaccion);
						cs.setString(2, fecha);
						cs.setString(3, fechaHora);
						cs.setInt(4, nroCaja);
						cs.setInt(5, transaccion);
						cs.setString(6, vendedor);
						cs.setLong(7, correlativoVenta);

						Optional<String> optCud = Optional.ofNullable(articuloVentaTrama.getArticuloVenta().getCodDespacho());

						cs.setString(8, (optCud.isPresent() && optCud.get().length() > 0) ? optCud.get() : Constantes.SPACE);//codArticulo);
						cs.setInt(9, articuloVentaTrama.getArticuloVenta().getCorrelativoItem());
						cs.registerOutParameter(10, OracleTypes.NUMBER);
						cs.registerOutParameter(11, OracleTypes.VARCHAR);

						sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA("
								+ transaccion
								+ ",TO_DATE('"
								+ fecha
								+ "','DD/MM/YYYY'),TO_DATE('"
								+ fechaHora
								+ "','DD/MM/YYYY hh24:mi:ss'),"
								+ nroCaja
								+ ","
								+ transaccion
								+ ",'"
								+ vendedor
								+ "',"
								+ correlativoVenta
								+ ",'"
								+ ((optCud.isPresent() && optCud.get().length() > 0) ? optCud.get() : Constantes.SPACE)
								+ "',"
								+ articuloVentaTrama.getArticuloVenta().getCorrelativoItem()
								+ ",outn,outv)}";
						logger.traceInfo("AnulacionMKP", sql);

						cs.execute();

						sal = new Integer(cs.getInt(10));
						sal2 = cs.getString(11);
						cs.close();
						if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
							logger.traceError(
									"Creacion NC T.Bancaria BackOffice",
									"--> Imposible crear forma de Pago Bancaria Error - "
											+ sql,
											new Exception("Resultado obtenido: "
													+ sal.toString()));


							if(connExtend != null) {
								PoolBDs.closeConnection(connExtend);
							}

							return false;
						}
						logger.traceInfo("AnulacionMKP", "UPDATE_ARTICULO_VENTA: "+sal);


					}
				}


			}


			List<IdentificadorMarketplace> listaMkp = trama.getIdentificadoresMarketplace();

			for (IdentificadorMarketplace identificadorMarketplace : listaMkp) {
				// Nuevo 7.5.36
				// Nuevo 8.0.12
				if (trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL) {
					procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA(?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)}";

					cs = connExtend.prepareCall(procSQL);
					cs.setInt(1, Constantes.NRO_TRES);
					cs.setInt(2, transaccion);
					cs.setNull(3, OracleTypes.NUMBER);
					//cs.setInt(3, transaccion);
					cs.setString(4, fecha);
					cs.setString(5, fechaHora);
					cs.setInt(6, nroCaja);
					cs.setInt(7, transaccion);
					cs.setInt(8, Integer.parseInt(vendedor));
					cs.setLong(9, correlativoVenta);
					cs.registerOutParameter(10, OracleTypes.NUMBER);
					cs.registerOutParameter(11, OracleTypes.VARCHAR);
					// Log
					sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA"
							+ "("
							+ Constantes.NRO_TRES
							+ ","
							+ transaccion
							+ ",null"
							//+ transaccion
							+ ",to_date("
							+ fecha
							+ ",'DD/MM/YYYY'),"
							+ "to_date("
							+ fechaHora
							+ ",'DD/MM/YYYY HH24:MI:SS')"
							+ ","
							+ nroCaja
							+ ","
							+ transaccion
							+ ","
							+ Integer.parseInt(vendedor)
							+ ","
							+ correlativoVenta
							+ ",outN"
							+ ",outV)}";
					logger.traceInfo("AnulacionMKP", sql);
					cs.execute();
					sal = new Integer(cs.getInt(10));
					cs.close();

				} else if (trama.getNotaVenta().getEstado()
						.intValue() == Constantes.NOTA_CREDITO_PARCIAL) {
					procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA(?,?,?,to_date(?,'DD/MM/YYYY'),to_date(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?)}";
					cs = connExtend.prepareCall(procSQL);
					cs.setInt(1, Constantes.NRO_CUATRO);
					cs.setInt(2, transaccion);
					cs.setNull(3, OracleTypes.NUMBER);
					//cs.setInt(3, transaccion);
					cs.setString(4, fecha);
					cs.setString(5, fechaHora);
					cs.setInt(6, nroCaja);
					cs.setInt(7, transaccion);
					cs.setInt(8, Integer.parseInt(vendedor));
					cs.setLong(9, correlativoVenta);
					cs.registerOutParameter(10, OracleTypes.NUMBER);
					cs.registerOutParameter(11, OracleTypes.VARCHAR);
					// Log
					sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA"
							+ "("
							+ Constantes.NRO_CUATRO
							+ ","
							+ transaccion
							+ ",null"
							//+ transaccion
							+ ",to_date("
							+ fecha
							+ ",'DD/MM/YYYY'),"
							+ "to_date("
							+ fechaHora
							+ ",'DD/MM/YYYY HH24:MI:SS')"
							+ ","
							+ nroCaja
							+ ","
							+ transaccion
							+ ","
							+ Integer.parseInt(vendedor)
							+ ","
							+ correlativoVenta
							+ ",outN"
							+ ",outV)}";
					logger.traceInfo("AnulacionMKP", sql);
					cs.execute();
					sal = new Integer(cs.getInt(10));
					cs.close();

				}

				if (Validaciones.validaInteger(sal)
						&& sal.intValue() != Constantes.NRO_CERO) {
					logger.traceError(
							"UPDATE NOTA_VENTA ",
							"--> CALL "
									+ procSQL
									+ "Actualizacion NOTA_VENTA termino de la transacciones: - "
									+ sql,
									new Exception("Resultado obtenido: "
											+ sal.toString()));


					if (connExtend != null) {
						PoolBDs.closeConnection(connExtend);
					}
					return false;
				}

				if(identificadorMarketplace.getAmount() != null && !BigDecimal.ZERO.equals(identificadorMarketplace.getAmount())
						&& identificadorMarketplace.getIdRefund() != null && identificadorMarketplace.getIdRefund().intValue() > 0 ) {
						String ordenMkp = (Constantes.VACIO
								.equalsIgnoreCase(identificadorMarketplace
								.getOrdenMkp())) ? identificadorMarketplace
										.getOrdenMkp() : Constantes.VACIO;

										//if (!esMkpSinStock(trama, correlativoVenta,
											//	ordenMkp)) {// nuevo 8.0.4
											// Nuevo insert MIRAKL 7.5.36
											String rutCompradorMkp = String.format(Constantes.FORMATO_8_CEROS_IZQ, trama.getNotaVenta().getRutComprador() != null ? trama.getNotaVenta().getRutComprador() : Constantes.NRO_CERO);

											int rutComprador = (trama.getNotaVenta().getRutComprador() != null) ? trama.getNotaVenta().getRutComprador().intValue() : Constantes.NRO_CERO;
											procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL(?,?,?,?,TO_TIMESTAMP(?,'"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),?,?,?,?,?,?,?)}";
											cs = connExtend.prepareCall(procSQL);
											cs.setLong(1, correlativoVenta);
											cs.setString(2, Constantes.PA_DOS);
											cs.setString(3, identificadorMarketplace.getOrdenMkp());
											cs.setInt(4, Constantes.MKP_PENDIENTE_PETICION);//identificadorMarketplace.getEstado());
											cs.setString(5, fechaHora);
											cs.setString(6, pcv.buscaValorPorNombre("USUARIO"));
											cs.setBigDecimal(7, identificadorMarketplace.getAmount());
											//cs.setInt(8, rutComprador);
											cs.setString(8, rutCompradorMkp);
											cs.setInt(9, identificadorMarketplace.getIdRefund().intValue());
											cs.registerOutParameter(10, OracleTypes.NUMBER);
											cs.registerOutParameter(11, OracleTypes.VARCHAR);
											cs.registerOutParameter(12, OracleTypes.VARCHAR);
											sql = "{CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL("
													+ correlativoVenta
													+ ","
													+ Constantes.PA_DOS
													+ ","
													+ identificadorMarketplace.getOrdenMkp()
													+ ","
													+ identificadorMarketplace.getEstado()
													+ ",TO_TIMESTAMP('"
													+ fechaHora
													+ "','"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),"
													+ pcv.buscaValorPorNombre("USUARIO")
													+ ", "
													+ identificadorMarketplace.getAmount()
													+ ", '"
													+ rutCompradorMkp
													+ "', "
													+ identificadorMarketplace.getIdRefund().intValue()
													+ ")}";
											logger.traceInfo("AnulacionMKP", sql);
											cs.execute();
											sal = new Integer(cs.getInt(10));
											procSQL = sql;
											sql = cs.getString(12);

											if (Validaciones.validaInteger(sal)
													&& sal.intValue() != Constantes.NRO_CERO) {
												logger.traceError(
														"INSERT ENVIO_ESTADO_OC_MIRAKL ",
														"--> CALL: "
																+ procSQL
																+ " Actualizacion ENVIO_ESTADO_OC_MIRAKL Insert realizado: - "
																+ sql, new Exception(
																		"Resultado obtenido: "
																				+ sal.toString()));


												if (connExtend != null) {
													PoolBDs.closeConnection(connExtend);
												}
												return false;
											}
										//} // If esMkpSinStock
				}
			} // Fin for articulo de ventas


			//Envio trx al BO
			envioTrxBo(trama, pcv, Long.valueOf(transaccion), Boolean.TRUE);
			
			

		} catch (Exception e) {
			logger.traceError("AnulacionMKP",
					"Error durante la ejecución de AnulacionMKP", e);

			return false;
		}

		return anulacionMkp;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean AnulacionCDF(TramaDTE trama, Parametros pcv, String fecha,
			String fechaHora) throws AligareException {
		logger.initTrace("anulacionCDF", "Parametros: parametros");
		//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		//		ClubDAO clubDAO = new ClubDAOImpl();
		//
		//		Connection conn = null;
		//		Connection connTRE = null;
		//		Connection connClaseS = null;
		//		Connection connExtend = null;
		//		Connection connCAR = null;
		boolean anulacionCDF = false;
		//		int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		//		int sucursal = Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"));
		//		int transaccion = caja.resNroTransaccion(pcv, null);
		//		long glTotalNc = getTotalNcCDF(trama);
		//		int wsFlagNcTre = 0;
		//		String vendedor = pcv.buscaValorPorNombre("RUT_VENDEDOR");
		//		String rutEjecutivoVta = "";
		//		String sql = "";
		//		int tipo = 37;
		//		String codAut = "";
		//		CallableStatement cs = null;
		//		String procSQL = "";
		//		Integer wsTipo = new Integer(Constantes.NRO_CERO);
		//		Integer sal = null;
		//		String sal2 = Constantes.VACIO;
		//		RetornoEjecucion retornoEjecucion = null;
		//
		//		try {
		//			// Conexiones a BACKOFFICE
		//			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//			if (conn != null) {
		//				if (trama.getTarjetaBancaria().getCorrelativoVenta() != null) {
		//					logger.traceInfo("anulacionCDF",
		//							"Entra a trama.getTarjetaBancaria().getCorrelativoVenta()");
		//					wsTipo = (Validaciones.validaInteger(trama
		//							.getTarjetaBancaria().getTipoTarjeta())) ? trama
		//							.getTarjetaBancaria().getTipoTarjeta()
		//							: Constantes.NRO_CERO;
		//					codAut = (trama.getTarjetaBancaria().getCodigoAutorizador() != null) ? trama
		//							.getTarjetaBancaria().getCodigoAutorizador()
		//							: Constantes.VACIO;
		//					rutEjecutivoVta = ((trama.getNotaVenta().getEjecutivoVta() != null && Constantes.VACIO
		//							.equals(trama.getNotaVenta().getEjecutivoVta().trim())) ? trama.getNotaVenta()
		//							.getEjecutivoVta() : Constantes.STRING_CERO);
		//
		//					// Creaci�n Tarjeta Bancaria
		//					if (Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(trama
		//							.getTarjetaBancaria().getVd())
		//							&& !(Constantes.TARJETA_MERCADO_PAGO
		//									.equalsIgnoreCase(trama
		//											.getTarjetaBancaria()
		//											.getMedioAcceso()))) {
		//						procSQL = "{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						cs.setInt(2, sucursal);
		//						cs.setInt(3, nroCaja);
		//						cs.setInt(4, transaccion);
		//						cs.setLong(5, glTotalNc);
		//						cs.setInt(6, Constantes.NRO_CERO);
		//						cs.registerOutParameter(7, OracleTypes.NUMBER);
		//						cs.registerOutParameter(8, OracleTypes.VARCHAR);
		//						logger.traceInfo("anulacionCDF",
		//								"{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE('" + fecha
		//										+ "','DD/MM/YYYY')," + sucursal + ","
		//										+ nroCaja + "," + transaccion + ","
		//										+ glTotalNc + ",0,outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(7));
		//						sal2 = cs.getString(8);
		//						cs.close();
		//
		//					} else if (Constantes.TARJETA_MERCADO_PAGO
		//							.equalsIgnoreCase(trama.getTarjetaBancaria()
		//									.getMedioAcceso())) {
		//						procSQL = "{CALL inserta_transbank_cv(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						cs.setInt(2, sucursal);
		//						cs.setInt(3, nroCaja);
		//						cs.setInt(4, transaccion);
		//						cs.setString(5, Constantes.TARJETA_MERCADO_PAGO);
		//						cs.setString(6, "C");
		//						cs.setInt(7, trama.getNotaVenta().getRutComprador().intValue());
		//						cs.setInt(8, Constantes.NRO_CERO);
		//						cs.setLong(9, glTotalNc);
		//						cs.setInt(10, Constantes.NRO_CERO);
		//						cs.setInt(11, Constantes.NRO_CERO);
		//						cs.setInt(12, Constantes.NRO_CERO);
		//						cs.setString(13, codAut);
		//						cs.setString(14, "NU003900");
		//						cs.registerOutParameter(15, OracleTypes.NUMBER);
		//						cs.registerOutParameter(16, OracleTypes.VARCHAR);
		//						sql = "Llamada a pl: {declare sal number;sal2 varchar(100);begin inserta_transbank_cv(TO_DATE('"
		//								+ fecha
		//								+ "','DD/MM/YYYY')"
		//								+ ","
		//								+ sucursal
		//								+ ","
		//								+ nroCaja
		//								+ ","
		//								+ transaccion
		//								+ ","
		//								+ Constantes.TARJETA_MERCADO_PAGO
		//								+ ","
		//								+ "C"
		//								+ ","
		//								+ trama.getNotaVenta().getRutComprador().intValue()
		//								+ ","
		//								+ Constantes.NRO_CERO
		//								+ ","
		//								+ glTotalNc
		//								+ ","
		//								+ Constantes.NRO_CERO
		//								+ ","
		//								+ Constantes.NRO_CERO
		//								+ ","
		//								+ Constantes.NRO_CERO
		//								+ ","
		//								+ codAut
		//								+ ",'NU003900'" + ",out1,out2)}";
		//						logger.traceInfo("anulacionCDF", sql);
		//						cs.execute();
		//						sal = new Integer(cs.getInt(15));
		//						cs.close();
		//
		//					} else {
		//						procSQL = "{CALL ? := inserta_bancaria_back(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.registerOutParameter(1, OracleTypes.NUMBER);
		//						cs.setString(2, fecha);
		//						cs.setInt(3, sucursal);
		//						cs.setInt(4, nroCaja);
		//						cs.setInt(5, transaccion);
		//						cs.setInt(6, wsTipo);
		//						cs.setLong(7, glTotalNc);
		//						cs.setString(8, codAut);
		//						sql = "Llamada a pl: declare sal number; begin sal:=inserta_bancaria_back(to_date('"
		//								+ fecha
		//								+ "','dd-mm-yyyy'),"
		//								+ sucursal
		//								+ ","
		//								+ nroCaja
		//								+ ","
		//								+ transaccion
		//								+ ","
		//								+ wsTipo
		//								+ "," + glTotalNc + ",'" + codAut + "'); end;";
		//						logger.traceInfo("anulacionCDF", sql);
		//						cs.execute();
		//						sal = new Integer(cs.getInt(1));
		//						cs.close();
		//
		//					}
		//
		//					if (Validaciones.validaInteger(sal)
		//							&& sal.intValue() != Constantes.NRO_CERO) {
		//						logger.traceError(
		//								"Creacion NC T.Bancaria BackOffice",
		//								"--> Imposible crear forma de Pago Bancaria Error - "
		//										+ sql,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//
		//				} // fin:trama.getTarjetaBancaria().getCorrelativoVenta()
		//				wsFlagNcTre = 1;
		//				// Creaci�n Tarjeta Ripley query se obtiene de la Trama
		//				if (trama.getTarjetaRipley().getCorrelativoVenta() != null) {
		//					BigInteger pan = trama.getTarjetaRipley().getPan();
		//					BigInteger codAutor = (trama.getTarjetaRipley()
		//							.getCodigoAutorizacion() != null) ? trama
		//							.getTarjetaRipley().getCodigoAutorizacion()
		//							: new BigInteger(Constantes.STRING_CERO);
		//					logger.traceInfo("anulacionCDF",
		//							"Entra a trama.getTarjetaRipley().getCorrelativoVenta()");
		//					// BigInteger tipoCodAuto= BigInteger.valueOf(0);
		//					// BigInteger tipoCodAuto = new BigInteger("0");
		//					tipo = 2;// en duro, copiado del c�digo fuente
		//					procSQL = "{CALL ? := INSERTA_RIPLEY_BACK(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//					cs = conn.prepareCall(procSQL);
		//					sql = "{CALL ? := INSERTA_RIPLEY_BACK(to_date(";
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					sql = sql +"'"+fecha+"'"+",'DD/MM/YYYY')";
		//					cs.setString(2, fecha);
		//					sql = sql + ","+sucursal;
		//					cs.setInt(3, sucursal);
		//					sql = sql + ","+nroCaja;
		//					cs.setInt(4, nroCaja);
		//					sql = sql + ","+transaccion;
		//					cs.setInt(5, transaccion);
		//					sql = sql + ","+trama.getTarjetaRipley().getAdministradora();
		//					cs.setInt(6, trama.getTarjetaRipley().getAdministradora());
		//					sql = sql + ","+trama.getTarjetaRipley().getEmisor();
		//					cs.setInt(7, trama.getTarjetaRipley().getEmisor());
		//					sql = sql + ","+trama.getTarjetaRipley().getTarjeta();
		//					cs.setInt(8, trama.getTarjetaRipley().getTarjeta());
		//					sql = sql + ","+trama.getTarjetaRipley().getRutTitular();
		//					cs.setInt(9, trama.getTarjetaRipley().getRutTitular());
		//					sql = sql + ","+trama.getTarjetaRipley().getRutPoder();
		//					cs.setInt(10, trama.getTarjetaRipley().getRutPoder());
		//					sql = sql + ","+trama.getTarjetaRipley().getPlazo();
		//					cs.setInt(11, trama.getTarjetaRipley().getPlazo());
		//					sql = sql + ","+trama.getTarjetaRipley().getDiferido();
		//					cs.setInt(12, trama.getTarjetaRipley().getDiferido());
		//					sql = sql + ","+trama.getTarjetaRipley().getMontoCapital();
		//					cs.setInt(13, trama.getTarjetaRipley().getMontoCapital());
		//					sql = sql + ","+trama.getTarjetaRipley().getMontoPie();
		//					cs.setInt(14, trama.getTarjetaRipley().getMontoPie());
		//
		//				
		//
		//					if (trama.getTarjetaRipley().getCodigoCore().intValue() == 1) {
		//						if (trama.getTarjetaRipley().getPan().intValue() > 1) {
		//							if (Integer.parseInt(trama.getTarjetaRipley()
		//									.getTvtriGlsPrjTsaInt()) > 0) {
		//								sql = sql + ","+trama.getTarjetaRipley().getDescuentoCar();
		//								cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//								sql = sql + ","+trama.getTarjetaRipley().getCodigoDescuento();
		//								cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//								sql = sql + ",1,0";
		//								cs.setInt(17, 1);
		//								cs.setInt(18, 0);
		//								sql = sql + ","+((Validaciones.validaInteger(trama
		//										.getTarjetaRipley()
		//										.getCodigoCore()) && trama
		//										.getTarjetaRipley()
		//										.getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//										Util.rellenarString(
		//												pan.toString(), 16,
		//												Constantes.FALSO,
		//												Constantes.CHAR_SPACE)
		//												.substring(0, 15))
		//										: null
		//										: (pan != null) ? new BigDecimal(
		//												pan.toString()) : null);
		//								cs.setBigDecimal(
		//										19,
		//										((Validaciones.validaInteger(trama
		//												.getTarjetaRipley()
		//												.getCodigoCore()) && trama
		//												.getTarjetaRipley()
		//												.getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//												Util.rellenarString(
		//														pan.toString(), 16,
		//														Constantes.FALSO,
		//														Constantes.CHAR_SPACE)
		//														.substring(0, 15))
		//												: null
		//												: (pan != null) ? new BigDecimal(
		//														pan.toString()) : null));
		//							} else {
		//								sql = sql + ","+trama.getTarjetaRipley().getDescuentoCar();
		//								cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//								sql = sql + ","+trama.getTarjetaRipley().getCodigoDescuento();
		//								cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//								sql = sql + ",0,0";
		//								cs.setInt(17, 0);
		//								cs.setInt(18, 0);
		//								sql = sql + ","+((Validaciones.validaInteger(trama
		//										.getTarjetaRipley()
		//										.getCodigoCore()) && trama
		//										.getTarjetaRipley()
		//										.getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//										Util.rellenarString(
		//												pan.toString(), 16,
		//												Constantes.FALSO,
		//												Constantes.CHAR_SPACE)
		//												.substring(0, 15))
		//										: null
		//										: (pan != null) ? new BigDecimal(
		//												pan.toString()) : null);
		//								cs.setBigDecimal(
		//										19,
		//										((Validaciones.validaInteger(trama
		//												.getTarjetaRipley()
		//												.getCodigoCore()) && trama
		//												.getTarjetaRipley()
		//												.getCodigoCore().intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//												Util.rellenarString(
		//														pan.toString(), 16,
		//														Constantes.FALSO,
		//														Constantes.CHAR_SPACE)
		//														.substring(0, 15))
		//												: null
		//												: (pan != null) ? new BigDecimal(
		//														pan.toString()) : null));
		//							}
		//						} else {
		//							sql = sql + ","+trama.getTarjetaRipley().getDescuentoCar();
		//							cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//							sql = sql + ","+trama.getTarjetaRipley().getCodigoDescuento();
		//							cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//							sql = sql + ","+trama.getTarjetaRipley().getPan().intValue();
		//							cs.setInt(17, trama.getTarjetaRipley().getPan().intValue());
		//							sql = sql + ",0";
		//							cs.setInt(18, 0);
		//							sql = sql + ","+trama.getTarjetaRipley().getCodigoDescuento();
		//							cs.setInt(19, trama.getTarjetaRipley().getCodigoDescuento());
		//						}
		//					} else {
		//						sql = sql + ","+trama.getTarjetaRipley().getDescuentoCar();
		//						cs.setInt(15, trama.getTarjetaRipley().getDescuentoCar());
		//						sql = sql + ","+trama.getTarjetaRipley().getCodigoDescuento();
		//						cs.setInt(16, trama.getTarjetaRipley().getCodigoDescuento());
		//						sql = sql + ","+((Validaciones.validaInteger(trama
		//								.getTarjetaRipley().getCodigoCore()) && trama
		//								.getTarjetaRipley().getCodigoCore()
		//								.intValue() == Constantes.NRO_UNO) ? new BigDecimal(
		//								codAutor)
		//								: new BigDecimal(
		//										(((Validaciones
		//												.validaBigInteger(codAutor)) ? codAutor
		//												.toString()
		//												: Constantes.VACIO) + Constantes.STRING_CEROX2)));
		//						cs.setBigDecimal(
		//								17,
		//								((Validaciones.validaInteger(trama
		//										.getTarjetaRipley().getCodigoCore()) && trama
		//										.getTarjetaRipley().getCodigoCore()
		//										.intValue() == Constantes.NRO_UNO) ? new BigDecimal(
		//										codAutor)
		//										: new BigDecimal(
		//												(((Validaciones
		//														.validaBigInteger(codAutor)) ? codAutor
		//														.toString()
		//														: Constantes.VACIO) + Constantes.STRING_CEROX2))));
		//						sql = sql + ",0";
		//						cs.setInt(18, 0);
		//						sql = sql + ","+((Validaciones.validaInteger(trama
		//								.getTarjetaRipley().getCodigoCore()) && trama
		//								.getTarjetaRipley().getCodigoCore()
		//								.intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//								Util.rellenarString(pan.toString(), 16,
		//										Constantes.FALSO,
		//										Constantes.CHAR_SPACE)
		//										.substring(0, 15)) : null
		//								: (pan != null) ? new BigDecimal(pan
		//										.toString()) : null);
		//						cs.setBigDecimal(
		//								19,
		//								((Validaciones.validaInteger(trama
		//										.getTarjetaRipley().getCodigoCore()) && trama
		//										.getTarjetaRipley().getCodigoCore()
		//										.intValue() == Constantes.NRO_UNO) ? (pan != null) ? new BigDecimal(
		//										Util.rellenarString(pan.toString(), 16,
		//												Constantes.FALSO,
		//												Constantes.CHAR_SPACE)
		//												.substring(0, 15)) : null
		//										: (pan != null) ? new BigDecimal(pan
		//												.toString()) : null));
		//					}
		//					sql = sql + "}";
		//					logger.traceInfo("anulacionCDF",sql);
		//
		//					cs.execute();
		//					sal = new Integer(cs.getInt(1));
		//					cs.close();
		//					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//						tipo = 37;
		//						logger.traceError("Creacion T.Ripley BackOffice - ","--> Imposible crear forma de pago Ripley Error -"
		//										+ procSQL, new Exception("Resultado obtenido: "+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;// codigo de anulacione para
		//											// anulaci�n de recaudaci�n
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//				}
		//				// '------------------Creacion
		//				// TRE---------------------------------
		//				if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null) {
		//					logger.traceInfo("anulacionCDF",
		//							"entra a trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()");
		//					tipo = 37;// en duro, desde c�digo fuente
		//					wsTipo = trama.getTarjetaRegaloEmpresa().getTarjeta();
		//					Integer nroTarjeta = trama.getTarjetaRegaloEmpresa().getNroTarjeta();
		//					Integer codAdmin = trama.getTarjetaRegaloEmpresa().getAdministradora();
		//					Integer codAu = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();
		//					Integer emisor = trama.getTarjetaRegaloEmpresa().getEmisor();
		//					Integer flag = trama.getTarjetaRegaloEmpresa().getFlag();
		//					Saldos saldos = new Saldos();
		//					saldos = getSaldos(trama);
		//					String rutCliente = trama.getTarjetaRegaloEmpresa().getNroTarjeta().toString();
		//					long montoTarRegalo = 0l, wMontoTre = 0l; // wMontoTotal =
		//																// 0l,
		//				
		//					if (wsFlagNcTre == 1) {
		//						if (tipo == 37) {
		//							montoTarRegalo = saldos.getSaldoTRE();
		//							// wMontoTotal = saldos.getMontoTarRipley() +
		//							// saldos.getSaldoTRE();
		//							if (saldos.getMontoTarRipley() < glTotalNc) {
		//								wMontoTre = glTotalNc - saldos.getMontoTarRipley();
		//							}
		//						} else {
		//							montoTarRegalo = glTotalNc;
		//						}
		//
		//						procSQL = "{CALL actualiza_tar_regalo_empresa(to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//						cs = conn.prepareCall(procSQL);
		//						cs.setString(1, fecha);
		//						sql = "CALL actualiza_tar_regalo_empresa(to_date("+ fecha;
		//						cs.setInt(2, sucursal);
		//						sql = sql + ",'dd/mm/yyyy')," + sucursal;
		//						cs.setInt(3, nroCaja);
		//						sql = sql + "," + nroCaja;
		//						cs.setInt(4, transaccion);
		//						sql = sql + "," + transaccion;
		//						cs.setInt(5, codAdmin);
		//						sql = sql + "," + codAdmin;
		//						cs.setInt(6, emisor);
		//						sql = sql + "," + emisor;
		//						cs.setInt(7, wsTipo);
		//						sql = sql + "," + wsTipo;
		//						cs.setInt(8, codAu);
		//						sql = sql + "," + codAu;
		//
		//						if (wMontoTre > 0) {
		//							cs.setString(9, rutCliente);
		//							sql = sql + "," + rutCliente;
		//							cs.setLong(10, wMontoTre);
		//							sql = sql + "," + wMontoTre;
		//							cs.setInt(11, nroTarjeta);
		//							sql = sql + "," + nroTarjeta;
		//							cs.setInt(12, flag);
		//							sql = sql + "," + flag;
		//
		//						} else {
		//							cs.setString(9, rutCliente);
		//							sql = sql + "," + rutCliente;
		//							cs.setLong(10, montoTarRegalo);
		//							sql = sql + "," + montoTarRegalo;
		//							cs.setInt(11, nroTarjeta);
		//							sql = sql + "," + nroTarjeta;
		//							cs.setInt(12, flag);
		//							sql = sql + "," + flag;
		//						}
		//						cs.registerOutParameter(13, OracleTypes.NUMBER);
		//						cs.registerOutParameter(14, OracleTypes.VARCHAR);
		//						logger.traceInfo("anulacionCDF", sql + ",outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(13));
		//						sal2 = cs.getString(14);
		//						cs.close();
		//						if (Validaciones.validaInteger(sal)
		//								&& sal.intValue() != Constantes.NRO_CERO) {
		//							logger.traceError(
		//									" Creacion TRE BackOffice - ",
		//									"--> Imposible crear forma de Pago Tarjeta Regalos Empresa Error - "
		//											+ sql,
		//									new Exception("Resultado obtenido: "
		//											+ sal.toString()));
		//
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (conn != null) {
		//								PoolBDs.closeConnection(conn);
		//							}
		//							return false;
		//						}
		//
		//						// CONEXION TRE
		//						procSQL = "{CALL FEGIFE_PRC_ACT_SLD_NC(?,?,?,?,to_date(?,'DD/MM/YYYY'),?,?)}";
		//						connTRE = PoolBDs.getConnection(BDType.TRE);
		//						sql = Constantes.VACIO;
		//						cs = conn.prepareCall(procSQL);
		//						cs.setInt(1, nroTarjeta);
		//						sql = "{CALL FEGIFE_PRC_ACT_SLD_NC(" + nroTarjeta;
		//						if (wMontoTre > 0) {
		//							cs.setLong(2, wMontoTre);
		//							sql = sql + "," + wMontoTre;
		//						} else {
		//							cs.setLong(2, montoTarRegalo);
		//							sql = sql + "," + montoTarRegalo;
		//						}
		//						cs.setLong(3, nroCaja);
		//						sql = sql + "," + nroCaja;
		//						cs.setInt(4, trama.getNotaVenta().getCorrelativoVenta());
		//						sql = sql + ","
		//								+ trama.getNotaVenta().getCorrelativoVenta();
		//						cs.setString(5, fecha);
		//						sql = sql + ",to_date(" + fecha + ",'DD/MM/YYYY')";
		//						cs.registerOutParameter(6, OracleTypes.NUMBER);
		//						cs.registerOutParameter(7, OracleTypes.VARCHAR);
		//						logger.traceInfo("anulacionCDF", sql + ",outn,outv)}");
		//						cs.execute();
		//						sal = new Integer(cs.getInt(13));
		//						sal2 = cs.getString(14);
		//						cs.close();
		//						if (Validaciones.validaInteger(sal)
		//								&& sal.intValue() != Constantes.NRO_CERO) {
		//							logger.traceError(
		//									" Creacion TRE BackOffice -  ",
		//									"--> Imposible Actualizar Saldo Tarjeta Regalos Empresa Error - "
		//											+ sql,
		//									new Exception("Resultado obtenido: "
		//											+ sal.toString()));
		//
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (connTRE != null) {
		//								PoolBDs.closeConnection(connTRE);
		//							}
		//							return false;
		//						}
		//					}
		//				}
		//				List<ArticuloVentaTramaDTE> articuloVentas = trama
		//						.getArticuloVentas();
		//
		//				if (articuloVentas != null && articuloVentas.size() > 0) {
		//					logger.traceInfo("anulacionCDF",
		//							"articuloVentas distinto a null");
		//					for (int i = 0; i < articuloVentas.size(); i++) {
		//						ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas
		//								.get(i);
		//						if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO) {
		//							continue;
		//						}
		//
		//						// creando variables de la trama para uso en package
		//						Integer correlativoVenta = trama.getDespacho().getCorrelativoVenta();
		//						Integer rutDespachoDos = trama.getDespacho().getRutDespacho();
		//						// String rutDespacho = rutDespachoDos.toString() +
		//						// Util.getDigitoValidadorRut2(rutDespachoDos.toString());
		//						String nombreDespacho = Util.cambiaCaracteres(trama.getDespacho().getNombreDespacho());
		//						Timestamp fechaDespacho = (trama.getDespacho()
		//								.getFechaDespacho() != null) ? trama
		//								.getDespacho().getFechaDespacho() : FechaUtil
		//								.stringToTimestamp("01-01-1900",
		//										Constantes.FECHA_DDMMYYYY3);
		//						Integer sucursalDespacho = trama.getDespacho().getSucursalDespacho();
		//						Integer comunaDespacho = trama.getDespacho().getComunaDespacho();
		//						Integer regionDespacho = trama.getDespacho().getRegionDespacho();
		//						String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
		//						String telefonoDespacho = trama.getDespacho().getTelefonoDespacho();
		//						String observacion = null;
		//						if (trama.getDespacho().getObservacion() != null) {
		//							observacion = Util.cambiaCaracteres(trama.getDespacho().getObservacion());
		//						} else {
		//							observacion = Util.cambiaCaracteres("sin observacion");
		//						}
		//						// datos cliente
		//						Integer rutClienteDos = trama.getDespacho()
		//								.getRutCliente();
		//						// String rutCliente = rutClienteDos.toString()
		//						// +
		//						// Util.getDigitoValidadorRut2(rutClienteDos.toString());
		//						String direccionCliente = trama.getDespacho().getDireccionCliente();
		//						String telefonoCliente = trama.getDespacho().getTelefonoCliente();
		//						// Integer tipoCliente =
		//						// trama.getDespacho().getTipoCliente();
		//						String nombreCliente = Util.cambiaCaracteres(trama
		//								.getDespacho().getNombreCliente()
		//								+ " "
		//								+ trama.getDespacho().getApellidoPatCliente()
		//								+ " "
		//								+ trama.getDespacho().getApellidoMatCliente());
		//						String emailCliete = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
		//						String nombreInvitado = Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
		//						String apellidoPaterno = null;
		//						String apellidoMaterno = null;
		//
		//						if (trama.getDespacho().getApellidoPatCliente() != null) {
		//							apellidoPaterno = trama.getDespacho().getApellidoPatCliente();
		//						} else {
		//							apellidoPaterno = Util.cambiaCaracteres("Apellido no descrito");
		//						}
		//
		//						if (trama.getDespacho().getApellidoPatCliente() != null) {
		//							apellidoMaterno = trama.getDespacho().getApellidoMatCliente();
		//						} else {
		//							apellidoMaterno = Util.cambiaCaracteres("Apellido no descrito");
		//						}
		//						// desde Nota de ventas
		//						Integer codigoRegalo = 0;
		//						if (trama.getNotaVenta().getCodigoRegalo() > 0) {
		//							codigoRegalo = trama.getNotaVenta().getCodigoRegalo();
		//						}
		//						String mensajeTarjeta = trama.getDespacho()
		//								.getMensajeTarjeta() != null ? trama
		//								.getDespacho().getMensajeTarjeta()
		//								: Constantes.VACIO;
		//						// String venedorDos = vendedor.toString() +
		//						// Util.getDigitoValidadorRut2(rutClienteDos.toString());
		//						// Articulo de ventas
		//						String costoEnvio = articuloVentaTrama
		//								.getArticuloVenta().getDescRipley() != null ? articuloVentaTrama
		//								.getArticuloVenta().getDescRipley()
		//								: Constantes.VACIO;
		//						String jornadaDespacho = articuloVentaTrama.getArticuloVenta().getCodDespacho();
		//						String jornadaDespachoBo = null;
		//						if (jornadaDespacho.equalsIgnoreCase(Constantes.BOL_JORNADA_AM)) {
		//							jornadaDespachoBo = Constantes.STRING_UNO;
		//						} else if (jornadaDespacho.equalsIgnoreCase(Constantes.BOL_JORNADA_PM)) {
		//							jornadaDespachoBo = Constantes.STRING_DOS;
		//						}
		//						int tipoRegalo = 0;
		//						if (tieneCud(
		//								articuloVentaTrama.getArticuloVenta().getCodArticulo()).intValue() == 0) {
		//							if (articuloVentaTrama.getArticuloVenta().getEsregalo() != null) {
		//								if ("S".equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getEsregalo())
		//										&& !mensajeTarjeta.equalsIgnoreCase(Constantes.VACIO)
		//										&& mensajeTarjeta.indexOf("99") != 0) {
		//									tipoRegalo = 99;
		//								}
		//							}
		//						}
		//
		//						// Creaci�n de Despacho para Big Ticket
		//													
		//								String cud = null;
		//								String codDespacho = articuloVentaTrama.getArticuloVenta().getCodDespacho().trim();
		//								if (!Constantes.VACIO.equalsIgnoreCase(costoEnvio)	&& (costoEnvio.contains(Constantes.TRAMA_COSTO_ENVIO) || costoEnvio.contains(Constantes.TRAMA_EG_EXT))) {
		//									cud = "";
		//								} else {
		//									if (((codDespacho.trim().length()) < 22) && codDespacho.length() > 0 ) {
		//										cud = codDespacho.substring(0, 14) + Constantes.CHAR_CERO + codDespacho.substring(17, 4);
		//									} else {
		//										cud = codDespacho.trim();
		//									}
		//								}								
		//								
		//								
		//								
		//						Integer vCantPrd = articuloVentaTrama
		//								.getArticuloVenta().getUnidades();
		//						Integer vPrcCmp = null;
		//						Integer precio = null;
		//						// Se calcula precio
		//						if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() < 0) {
		//							precio = articuloVentaTrama.getArticuloVenta().getPrecio() - articuloVentaTrama.getArticuloVenta().getMontoDescuento();
		//						} else {
		//							//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
		//							precio = (int) (articuloVentaTrama.getArticuloVenta().getPrecio() - Math.floor(articuloVentaTrama.getArticuloVenta().getMontoDescuento() / vCantPrd));
		//						}
		//						vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); // merge .22
		//						Integer correlativoItem = articuloVentaTrama.getArticuloVenta().getCorrelativoItem();
		//						// CONEXION CLUB
		//						if ((codigoRegalo.intValue() > Constantes.NRO_CERO) && (cud.length() > 0)){
		//
		//						//if (codigoRegalo.intValue() > Constantes.NRO_CERO) {
		//							if (articuloVentaTrama.getArticuloVenta().getCodArticuloMkp() == null){
		//								connClaseS = PoolBDs.getConnection(BDType.CLUB);
		//								procSQL = "{CALL lglreg_prc_mov_lis_vlr(?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//								cs = connClaseS.prepareCall(procSQL);
		//								cs.setInt(1, codigoRegalo);
		//								cs.setString(2, articuloVentaTrama.getArticuloVenta().getCodArticulo());
		//								cs.setInt(3, sucursal);
		//								cs.setString(4, fecha);
		//								cs.setInt(5, transaccion);
		//								cs.setInt(6, 37);// Info en duro
		//								cs.setInt(7, 1); // Info en duro
		//								cs.setInt(8, 0); // Info en duro
		//								cs.setInt(9, vCantPrd);// REVISAR v_cant_prd
		//								cs.setInt(10, vPrcCmp);// v_prc_cmp
		//								cs.setInt(11, transaccion);// v_boleta
		//								cs.setInt(12, nroCaja);
		//								cs.setInt(13, tipo);// revisar que tipo
		//								cs.setInt(14, correlativoItem);
		//								cs.setString(15, direccionDespacho);
		//								cs.setString(16, "< SR >");
		//								cs.setString(17, Constantes.VACIO);
		//								cs.setString(18, emailCliete);
		//								cs.setString(19, nombreInvitado);
		//								cs.setString(20, apellidoPaterno);
		//								cs.setString(21, apellidoMaterno);
		//								cs.setString(22, Constantes.VACIO);
		//								cs.setInt(23, Constantes.NRO_CERO);
		//								if (tipoRegalo == 99)
		//									cs.setString(24, Constantes.STRING_CERO);
		//								else
		//									cs.setString(24, mensajeTarjeta);
		//								cs.setInt(25, Constantes.NRO_CERO);
		//								cs.setInt(26, Constantes.NRO_CERO);
		//								cs.setInt(27, Constantes.NRO_CERO);
		//								cs.setInt(28, Constantes.NRO_CERO);
		//								cs.registerOutParameter(29, OracleTypes.NUMBER);
		//								cs.registerOutParameter(30, OracleTypes.VARCHAR);
		//								sql = "{CALL lglreg_prc_mov_lis_vlr("
		//										+ codigoRegalo
		//										+ ",'"
		//										+ articuloVentaTrama.getArticuloVenta().getCodArticulo()
		//										+ "',"
		//										+ sucursal
		//										+ ",to_date('"
		//										+ fecha
		//										+ "','DD/MM/YYYY'),"
		//										+ transaccion
		//										+ ",37,1,0,"
		//										+ vCantPrd
		//										+ ","
		//										+ vPrcCmp
		//										+ ","
		//										+ transaccion
		//										+ ","
		//										+ nroCaja
		//										+ ","
		//										+ tipo
		//										+ ","
		//										+ correlativoItem
		//										+ ",'"
		//										+ direccionDespacho
		//										+ "','< SR >','','"
		//										+ emailCliete
		//										+ "','"
		//										+ nombreInvitado
		//										+ "','"
		//										+ apellidoPaterno
		//										+ "','"
		//										+ apellidoMaterno
		//										+ "','',0,"
		//										+ ((tipoRegalo == 99) ? '0' : mensajeTarjeta)
		//										+ ",0,0,0,0,outn,outv)}";
		//								logger.traceInfo("AnulacionMKP", sql);
		//								cs.execute();
		//								logger.traceInfo("anulacionCDF", "Creacion de REGISTRO NOVIOS ");
		//								sal = new Integer(cs.getInt(29));
		//								sal2 = cs.getString(30);
		//								cs.close();
		//								if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO) {
		//									logger.traceError( "Creacion de REGISTRO NOVIOS",
		//											"--> Imposible de grabar en Novios Error - " + sql + sal2,
		//											new Exception("Resultado obtenido: " + sal.toString()));
		//
		//									// se obtiene medio de pago
		//									int tipoDePago = 37;
		//									int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//									int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//									if (connClaseS != null) {
		//										PoolBDs.closeConnection(connClaseS);
		//									}
		//									return false;
		//								}
		//							} else {
		//								
		//								boolean esCosto = false;
		//								if (!Constantes.VACIO.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getDescRipley())	
		//										&& ((articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_COSTO_ENVIO) || 
		//										    articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_EXT)
		//										|| articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_REEM)))) {
		//									    esCosto = true;
		//								}
		//								
		//								if ((articuloVentaTrama.getArticuloVenta().getCodDespacho() == null || articuloVentaTrama.getArticuloVenta().getCodDespacho().trim().equalsIgnoreCase("")) && !esCosto){ //JPRM
		//							
		//								RetornoEjecucion retorno = new RetornoEjecucion();
		//								retorno = clubDAO.lglreg_prc_mov_lis_vlr_mkp(	codigoRegalo.toString(),
		//																				Long.parseLong(articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()),
		//																				sucursal, 
		//																				FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3), 
		//																				transaccion, 
		//																				Constantes.NRO_TREINTASIETE,
		//																				Constantes.NRO_UNO,
		//																				Constantes.NRO_CERO,
		//																				vCantPrd, 
		//																				vPrcCmp, 
		//																				transaccion, 
		//																				nroCaja, 
		//																				tipo, 
		//																				correlativoItem, 
		//																				direccionDespacho, 
		//																				Constantes.MSJ_REGALO_SR, 
		//																				Constantes.NRO_CERO, 
		//																				emailCliete, 
		//																				nombreInvitado, 
		//																				apellidoPaterno, 
		//																				apellidoMaterno, 
		//																				Constantes.VACIO, 
		//																				Constantes.NRO_CERO, 
		//																				mensajeTarjeta, 
		//																				Constantes.NRO_CERO, 
		//																				Constantes.NRO_CERO, 
		//																				Constantes.NRO_CERO, 
		//																				Constantes.NRO_CERO, 
		//																				articuloVentaTrama.getArticuloVenta().getCodArticulo()+"-"+articuloVentaTrama.getArticuloVenta().getDescRipley()
		//																			);
		//								
		//								if(retorno.getCodigo() != Constantes.EJEC_SIN_ERRORES){
		//									logger.traceInfo("anulacionCDF", "Imposible de grabar en Novios Error - Mensaje: " + retorno.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())));
		//								} else {
		//									logger.traceInfo("anulacionCDF", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())), new KeyLog("Correlativo item", String.valueOf(articuloVentaTrama.getArticuloVenta().getCorrelativoItem())));
		//								}
		//							}
		//							}//JPRM
		//						}else{
		//							
		//
		//
		//							//if (codigoRegalo.intValue() > Constantes.NRO_CERO) {
		//								if (articuloVentaTrama.getArticuloVenta().getCodArticuloMkp() == null){
		//									connClaseS = PoolBDs.getConnection(BDType.CLUB);
		//									procSQL = "{CALL lglreg_prc_mov_lis_vlr(?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		//									cs = connClaseS.prepareCall(procSQL);
		//									cs.setInt(1, codigoRegalo);
		//									cs.setString(2, articuloVentaTrama.getArticuloVenta().getCodArticulo());
		//									cs.setInt(3, sucursal);
		//									cs.setString(4, fecha);
		//									cs.setInt(5, transaccion);
		//									cs.setInt(6, 37);// Info en duro
		//									cs.setInt(7, 1); // Info en duro
		//									cs.setInt(8, 0); // Info en duro
		//									cs.setInt(9, vCantPrd);// REVISAR v_cant_prd
		//									cs.setInt(10, vPrcCmp);// v_prc_cmp
		//									cs.setInt(11, transaccion);// v_boleta
		//									cs.setInt(12, nroCaja);
		//									cs.setInt(13, tipo);// revisar que tipo
		//									cs.setInt(14, correlativoItem);
		//									cs.setString(15, direccionDespacho);
		//									cs.setString(16, "< SR >");
		//									cs.setString(17, Constantes.VACIO);
		//									cs.setString(18, emailCliete);
		//									cs.setString(19, nombreInvitado);
		//									cs.setString(20, apellidoPaterno);
		//									cs.setString(21, apellidoMaterno);
		//									cs.setString(22, Constantes.VACIO);
		//									cs.setInt(23, Constantes.NRO_CERO);
		//									if (tipoRegalo == 99)
		//										cs.setString(24, Constantes.STRING_CERO);
		//									else
		//										cs.setString(24, mensajeTarjeta);
		//									cs.setInt(25, Constantes.NRO_CERO);
		//									cs.setInt(26, Constantes.NRO_CERO);
		//									cs.setInt(27, Constantes.NRO_CERO);
		//									cs.setInt(28, Constantes.NRO_CERO);
		//									cs.registerOutParameter(29, OracleTypes.NUMBER);
		//									cs.registerOutParameter(30, OracleTypes.VARCHAR);
		//									sql = "{CALL lglreg_prc_mov_lis_vlr("
		//											+ codigoRegalo
		//											+ ",'"
		//											+ articuloVentaTrama.getArticuloVenta().getCodArticulo()
		//											+ "',"
		//											+ sucursal
		//											+ ",to_date('"
		//											+ fecha
		//											+ "','DD/MM/YYYY'),"
		//											+ transaccion
		//											+ ",37,1,0,"
		//											+ vCantPrd
		//											+ ","
		//											+ vPrcCmp
		//											+ ","
		//											+ transaccion
		//											+ ","
		//											+ nroCaja
		//											+ ","
		//											+ tipo
		//											+ ","
		//											+ correlativoItem
		//											+ ",'"
		//											+ direccionDespacho
		//											+ "','< SR >','','"
		//											+ emailCliete
		//											+ "','"
		//											+ nombreInvitado
		//											+ "','"
		//											+ apellidoPaterno
		//											+ "','"
		//											+ apellidoMaterno
		//											+ "','',0,"
		//											+ ((tipoRegalo == 99) ? '0' : mensajeTarjeta)
		//											+ ",0,0,0,0,outn,outv)}";
		//									logger.traceInfo("AnulacionMKP", sql);
		//									cs.execute();
		//									logger.traceInfo("anulacionCDF", "Creacion de REGISTRO NOVIOS ");
		//									sal = new Integer(cs.getInt(29));
		//									sal2 = cs.getString(30);
		//									cs.close();
		//									if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO) {
		//										logger.traceError( "Creacion de REGISTRO NOVIOS",
		//												"--> Imposible de grabar en Novios Error - " + sql + sal2,
		//												new Exception("Resultado obtenido: " + sal.toString()));
		//
		//										// se obtiene medio de pago
		//										int tipoDePago = 37;
		//										int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//										int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//	
		//										if (connClaseS != null) {
		//											PoolBDs.closeConnection(connClaseS);
		//										}
		//										return false;
		//									}
		//								} else {
		//									
		//									boolean esCosto = false;
		//									if (!Constantes.VACIO.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getDescRipley())	
		//											&& ((articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_COSTO_ENVIO) ||
		//											    articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_EXT) ||
		//											 articuloVentaTrama.getArticuloVenta().getDescRipley().contains(Constantes.TRAMA_EG_REEM) ))) {
		//										    esCosto = true;
		//									}
		//									
		//															
		//								
		//									if ((articuloVentaTrama.getArticuloVenta().getCodDespacho() == null || articuloVentaTrama.getArticuloVenta().getCodDespacho().trim().equalsIgnoreCase("")) && !esCosto){ //JPRM
		//									
		//									RetornoEjecucion retorno = new RetornoEjecucion();
		//									retorno = clubDAO.lglreg_prc_mov_lis_vlr_mkp(	codigoRegalo.toString(),
		//																					Long.parseLong(articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()),
		//																					sucursal, 
		//																					FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3), 
		//																					transaccion, 
		//																					Constantes.NRO_TREINTASIETE,
		//																					Constantes.NRO_UNO,
		//																					Constantes.NRO_CERO,
		//																					vCantPrd, 
		//																					vPrcCmp, 
		//																					transaccion, 
		//																					nroCaja, 
		//																					tipo, 
		//																					correlativoItem, 
		//																					direccionDespacho, 
		//																					Constantes.MSJ_REGALO_SR, 
		//																					Constantes.NRO_CERO, 
		//																					emailCliete, 
		//																					nombreInvitado, 
		//																					apellidoPaterno, 
		//																					apellidoMaterno, 
		//																					Constantes.VACIO, 
		//																					Constantes.NRO_CERO, 
		//																					mensajeTarjeta, 
		//																					Constantes.NRO_CERO, 
		//																					Constantes.NRO_CERO, 
		//																					Constantes.NRO_CERO, 
		//																					Constantes.NRO_CERO, 
		//																					articuloVentaTrama.getArticuloVenta().getCodArticulo()+"-"+articuloVentaTrama.getArticuloVenta().getDescRipley()
		//																				);
		//									
		//									if(retorno.getCodigo() != Constantes.EJEC_SIN_ERRORES){
		//										logger.traceInfo("anulacionCDF", "Imposible de grabar en Novios Error - Mensaje: " + retorno.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())));
		//									} else {
		//										logger.traceInfo("anulacionCDF", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(trama.getNotaVenta().getCorrelativoVenta())), new KeyLog("Correlativo item", String.valueOf(articuloVentaTrama.getArticuloVenta().getCorrelativoItem())));
		//									}
		//								}
		//
		//							
		//								}//JPRM
		//							
		//							
		//						}
		//						// Creacion de Despacho para Backoffice
		//						Integer color = articuloVentaTrama.getArticuloVenta().getColor();
		//						if (costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != 0
		//								&& costoEnvio.indexOf(Constantes.TRAMA_EG) != 0
		//								&& regionDespacho != Constantes.NRO_TRECE
		//								&& regionDespacho != Constantes.NRO_NOVENTANUEVE) {
		//							
		//							
		//							procSQL = "{CALL ? := inserta_despacho_back(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"; // TODO
		//																																											// Revisar
		//																																											// pkg...
		//																																											// cantidad
		//																																											// parametros
		//																																											// entrada
		//							cs = conn.prepareCall(procSQL);
		//							cs.registerOutParameter(1, OracleTypes.NUMBER);
		//							cs.setString(2, fecha);
		//							cs.setInt(3, sucursal);
		//							cs.setInt(4, nroCaja);
		//							cs.setInt(5, transaccion);
		//							//cs.setString(6, cud);
		//							cs.setString(6, (articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()==null)?cud:articuloVentaTrama.getArticuloVenta().getCodArticuloMkp());
		//							cs.setInt(7, color);
		//							cs.setInt(8, Constantes.NRO_UNO);
		//							cs.setInt(9, rutDespachoDos);
		//							cs.setString(10, nombreDespacho);
		//							cs.setString(11, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaDespacho));// to_date(despacho)
		//							cs.setInt(12, sucursalDespacho);
		//							cs.setInt(13, comunaDespacho);
		//							cs.setInt(14, Constantes.NRO_CERO);
		//							cs.setInt(15, regionDespacho);
		//							cs.setString(16, direccionDespacho);
		//							cs.setString(17, telefonoDespacho);
		//							cs.setInt(18, Constantes.NRO_CERO);
		//							cs.setString(19, jornadaDespachoBo);
		//							cs.setString(20, observacion);
		//							cs.setInt(21, Constantes.NRO_UNO);
		//							cs.setString(22, "A1");
		//							cs.setInt(23, rutClienteDos);
		//							cs.setString(24, direccionCliente);
		//							cs.setString(25, telefonoCliente);
		//							cs.setInt(26, Constantes.NRO_UNO);
		//							cs.setString(27, nombreCliente);
		//							sql = "{CALL ? := inserta_despacho_back(to_date("
		//									+ fecha
		//									+ ",'DD/MM/YYYY'), "
		//									+ sucursal
		//									+ ","
		//									+ nroCaja
		//									+ ","
		//									+ transaccion
		//									+ ","
		//									//+ cud
		//									+ ((articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()==null)?cud:articuloVentaTrama.getArticuloVenta().getCodArticuloMkp())
		//									+ ","
		//									+ color
		//									+ ",1"
		//									+ ","
		//									+ rutDespachoDos
		//									+ ","
		//									+ nombreDespacho
		//									+ ",to_date("
		//									+ FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaDespacho) + ",'DD/MM/YYYY')"
		//									+ "," + sucursalDespacho + ","
		//									+ comunaDespacho + ",0" + ","
		//									+ regionDespacho + "," + direccionDespacho
		//									+ "," + telefonoDespacho + ",0" + ","
		//									+ jornadaDespachoBo + "," + observacion
		//									+ ",1" + ",'A1'" + "," + rutClienteDos
		//									+ "," + direccionCliente + ","
		//									+ telefonoCliente + ",1" + ","
		//									+ nombreCliente + ")}";
		//							logger.traceInfo("anulacionCDF", sql);
		//							cs.execute();
		//							logger.traceInfo("anulacionCDF", " Creacion Despacho BackOffice");
		//							sal = new Integer(cs.getInt(1));
		//							cs.close();
		//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//								logger.traceError(
		//										"Creacion Despacho BackOffice - ",
		//										"--> Imposible crear registro Despacho en Backoffice Error -" + sql,
		//										new Exception("Resultado obtenido: "+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								return false;
		//							}
		//
		//						}
		//						// Creacion de Articulo en BackOffice
		//						String codArticulo = articuloVentaTrama
		//								.getArticuloVenta().getCodArticulo();
		//						// if ((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO)
		//						// != 0)
		//						// && costoEnvio.indexOf(Constantes.TRAMA_EG) != 0) {
		//						String rutAnfitrion = Constantes.STRING_CERO;
		//						procSQL = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,0,?,?,?,?,?,0,5)}";
		//						// conn.setAutoCommit(Constantes.FALSO);
		//						cs = conn.prepareCall(procSQL);
		//						cs.registerOutParameter(1, OracleTypes.NUMBER);
		//						cs.setString(2, fecha);
		//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
		//						cs.setInt(4, sucursal);
		//						cs.setInt(5, nroCaja);
		//						cs.setInt(6, transaccion);
		//						cs.setInt(7, articuloVentaTrama.getArticuloVenta().getCorrelativoItem());
		//						//cs.setLong(8, Long.parseLong(articuloVentaTrama.getArticuloVenta().getCodArticulo()));
		//						cs.setLong(8, Long.parseLong((articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()==null)?articuloVentaTrama.getArticuloVenta().getCodArticulo():articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()));
		//						cs.setInt(9, Integer.parseInt(rutAnfitrion));
		//						cs.setInt(10, codigoRegalo);
		//						cs.setInt(11, precio);
		//						cs.setInt(12, vCantPrd);
		//						if (!Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getMontoDescuento())) {
		//							cs.setNull(13, OracleTypes.NUMBER);
		//						} else {
		//							cs.setInt(13, articuloVentaTrama.getArticuloVenta().getMontoDescuento());
		//						}
		//						cs.setInt(14, articuloVentaTrama.getArticuloVenta().getTipoDescuento());
		//						cs.setString(15, cud);
		//						sql = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE('"
		//								+ fecha
		//								+ "','"
		//								+ Constantes.FECHA_DDMMYYYY
		//								+ "'),"
		//								+ sucursal
		//								+ ","
		//								+ nroCaja
		//								+ ","
		//								+ transaccion
		//								+ ","
		//								+ articuloVentaTrama.getArticuloVenta().getCorrelativoItem()
		//								+ ","
		//								+ Long.parseLong((articuloVentaTrama.getArticuloVenta().getCodArticuloMkp()==null)?articuloVentaTrama.getArticuloVenta().getCodArticulo():articuloVentaTrama.getArticuloVenta().getCodArticuloMkp())
		//								+ ","
		//								+ Integer.parseInt(rutAnfitrion)
		//								+ ","
		//								+ codigoRegalo
		//								+ ",0,"
		//								+ precio
		//								+ ","
		//								+ vCantPrd
		//								+ ","
		//								+ ((!Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))
		//										? null
		//										: articuloVentaTrama.getArticuloVenta().getMontoDescuento())
		//								+ ","
		//								+ articuloVentaTrama.getArticuloVenta().getTipoDescuento() + ",'" + cud
		//								+ "',0,5)}";
		//						logger.traceInfo("anulacionCDF", sql);
		//						cs.executeUpdate();
		//						sal = new Integer(cs.getInt(1));
		//						cs.close();
		//						logger.traceInfo("AnulacionRecaudacionDAOImpl", "Creacion de Articulo en BackOffice");
		//						if (Validaciones.validaInteger(sal)
		//								&& sal.intValue() != Constantes.NRO_UNO) {// Constantes.NRO_CERO)
		//																			// {
		//							logger.traceError(
		//									"Creacion de Articulo en BackOffice -",
		//									"--> Imposible crear registro Articulos en Backoffice Error - " + procSQL,
		//									new Exception("Resultado obtenido: " + sal.toString()));
		//
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (conn != null) {
		//								PoolBDs.closeConnection(conn);
		//							}
		//							return false;
		//						}
		//						// }
		//						if (!Validaciones.validarVacio(cud)
		//								|| codigoRegalo.intValue() > Constantes.NRO_CERO) { // Nuevo
		//																					// 8.0.4
		//							// if (cud != Constantes.VACIO) {
		//							connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//							procSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA(?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?)}";
		//							cs = connExtend.prepareCall(procSQL);
		//							cs.setInt(1, transaccion);
		//							cs.setString(2, fecha);
		//							cs.setString(3, fechaHora);
		//							cs.setInt(4, nroCaja);
		//							cs.setInt(5, transaccion);
		//							cs.setString(6, vendedor);
		//							cs.setInt(7, correlativoVenta);
		//							cs.setString(8, (cud.length() > 0)
		//									? cud
		//									: Constantes.SPACE);
		//							cs.setInt(9, correlativoItem);
		//							cs.registerOutParameter(10, OracleTypes.NUMBER);
		//							cs.registerOutParameter(11, OracleTypes.VARCHAR);
		//
		//							sql = "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA("
		//									+ transaccion
		//									+ ",TO_DATE('"
		//									+ fecha
		//									+ "','DD/MM/YYYY'),TO_DATE('"
		//									+ fechaHora
		//									+ "','DD/MM/YYYY hh24:mi:ss'),"
		//									+ nroCaja
		//									+ ","
		//									+ transaccion
		//									+ ",'"
		//									+ vendedor
		//									+ "',"
		//									+ correlativoVenta
		//									+ ",'"
		//									+ ((cud.length() > 0) ? cud
		//											: Constantes.SPACE)
		//									+ "',"
		//									+ correlativoItem + ",outn,outv)}";
		//							logger.traceInfo("anulacionCDF", sql);
		//							cs.execute();
		//
		//							sal = new Integer(cs.getInt(10));
		//							sal2 = cs.getString(11);
		//							cs.close();
		//							if (Validaciones.validaInteger(sal)
		//									&& sal.intValue() != Constantes.NRO_CERO) {
		//								logger.traceError(
		//										"Creacion NC T.Bancaria BackOffice",
		//										"--> Imposible crear forma de Pago Bancaria Error - "
		//												+ sql,
		//										new Exception("Resultado obtenido: "
		//												+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								return false;
		//							}
		//
		//						}
		//					}// Fin For
		//				}// articulos ventas
		//				if (trama.getNotaVenta().getCorrelativoVenta() != null) {
		//					// ---------creaci�n transaccion
		//					tipo = 37;
		//					int correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		//					String fechaBoleta = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaBoleta());
		//					if (!(Constantes.STRING_CERO).equalsIgnoreCase(rutEjecutivoVta)) {
		//						if (rutEjecutivoVta != null && rutEjecutivoVta.length() > 1){
		//							vendedor = rutEjecutivoVta.substring(0, rutEjecutivoVta.length() - Constantes.NRO_UNO);
		//						}
		//
		//					}
		//
		//					// Nuevo 8.0.12
		//					String rutDespacho = (trama.getDespacho().getRutDespacho() != null) 
		//							? trama.getDespacho().getRutDespacho().toString()
		//							: Constantes.STRING_CERO;
		//							
		//					//+TC_CARGO_MONEDERO
		//					if (trama.getDespacho().getCorrelativoVenta() != null && trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
		//						connCAR = PoolBDs.getConnection(BDType.CAR);
		//					
		//						procSQL = "{CALL TC_CARGO_MONEDERO(?,201,?,4,1,50,null,null,null,1,?,TO_DATE(?,'"+Constantes.FECHA_DDMMYYYY+"'),?,0,null,null,'TCVIR',0,?,?,1,?,?)}";
		//					
		//						cs = connCAR.prepareCall(procSQL);
		//					
		//						cs.setLong(1, Long.parseLong(rutDespacho));
		//						cs.setLong(2, glTotalNc);
		//						cs.setInt(3, sucursal);
		//						cs.setString(4, fecha);
		//						cs.setInt(5, nroCaja);
		//					
		//						cs.registerOutParameter(6, OracleTypes.NUMBER);
		//						cs.registerOutParameter(7, OracleTypes.NUMBER);
		//						cs.registerOutParameter(8, OracleTypes.VARCHAR);
		//						cs.registerOutParameter(9, OracleTypes.NUMBER);
		//
		//						cs.setInt(7, Constantes.NRO_CERO);
		//					
		//						sql = "{CALL TC_CARGO_MONEDERO("+rutDespacho+",201,"+glTotalNc+",4,1,50,null,null,null,1,"+sucursal+",TO_DATE('"+fecha+"','"+Constantes.FECHA_DDMMYYYY+"'),"+nroCaja+",0,null,null,'TCVIR',0,outn,0-outn,1,outv,outn)}";
		//						logger.traceInfo("anulacionCDF", sql);
		//						cs.execute();
		//					
		//						sal = new Integer(cs.getInt(9));
		//						if (Validaciones.validaInteger(sal) && sal.intValue() == Constantes.NRO_UNO) {
		//							logger.traceError(
		//									" Inserta Transaccion BackOffice - " + procSQL,
		//									"--> Imposible crear recaudacion en monedero  Error - " + sql,
		//									new Exception("Resultado obtenido: " + sal.toString()));
		//					
		//							// se obtiene medio de pago
		//							int tipoDePago = 37;
		//							int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//							int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//							if (connCAR != null) {
		//								PoolBDs.closeConnection(connCAR);
		//							}
		//							return false;
		//						}
		//					}
		//					//-TC_CARGO_MONEDERO
		//							
		//					conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//
		//					procSQL = "{CALL ? := INSERTA_RECAUDACION_BO(TO_DATE(?,'DD/MM/YYYY'),?,?,?,9000,9001,0,?,?,?,1)}";
		//					cs = conn.prepareCall(procSQL);
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					cs.setString(2, fecha);
		//					cs.setInt(3, sucursal);
		//					cs.setInt(4, nroCaja);
		//					cs.setInt(5, transaccion);
		//					cs.setString(6, "37-" + correlativoVenta);
		//					cs.setLong(7, glTotalNc);
		//					cs.setString(8, rutDespacho);
		//					sql = "CALL outCod := INSERTA_RECAUDACION_BO(TO_DATE("
		//							+ fecha + ",'DD/MM/YYYY')" + "," + sucursal + ","
		//							+ nroCaja + "," + transaccion + ",32000,9001,0"
		//							+ ",37-" + correlativoVenta + "," + glTotalNc + ","
		//							+ rutDespacho + ",1)}";
		//					logger.traceInfo("anulacionCDF", sql);
		//					cs.execute();
		//
		//					sal = new Integer(cs.getInt(1));
		//					if (Validaciones.validaInteger(sal) && sal.intValue() == Constantes.NRO_UNO) {
		//						logger.traceError(
		//								" Inserta Transaccion BackOffice - " + procSQL,
		//								"--> Imposible crear recaudacion en monedero  Error - "
		//										+ sql,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					}
		//
		//					conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		//					logger.traceInfo("anulacionCDF", "inserta_transaccion_back3");
		//					procSQL = "{CALL ? := inserta_transaccion_back3(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY hh24:mi:ss'),?,?,?,?,to_date(?,'DD/MM/YYYY'))}";
		//					sql = "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_TRANSACCION_BACK3(TO_DATE("
		//							+ fecha
		//							+ ",'DD/MM/YYYY'),"
		//							+ sucursal
		//							+ ","
		//							+ nroCaja
		//							+ ","
		//							+ transaccion
		//							+ ","
		//							+ transaccion
		//							+ ","
		//							+ tipo
		//							+ ","
		//							+ glTotalNc
		//							+ ","
		//							+ sucursal
		//							+ ",TO_DATE('"
		//							+ fechaBoleta
		//							+ "','dd/MM/yyyy'),"
		//							+ Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA_BOLETAS"))
		//							+ ","
		//							+ trama.getNotaVenta().getNumeroBoleta()
		//							+ ",0,"
		//							+ Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"))
		//							+ ","
		//							+ vendedor
		//							+ ",0,"
		//							+ trama.getNotaVenta().getRutComprador()
		//							+ ","
		//							+ trama.getNotaVenta().getMontoDescuento()
		//							+ ","
		//							+ "0,0,to_date("
		//							+ fechaHora
		//							+ ",'DD/MM/YYYY hh24:mi:ss'),"
		//							+ "0,0,"
		//							+ correlativoVenta
		//							+ ","
		//							+ trama.getNotaVenta().getCorrelativoBoleta()
		//							+ ","
		//							+ FechaUtil.formatTimestamp(
		//									Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaCreacion())
		//							+ ")";
		//					logger.traceInfo("anulacionCDF", sql);
		//					cs = conn.prepareCall(procSQL);
		//					cs.registerOutParameter(1, OracleTypes.NUMBER);
		//					cs.setString(2, fecha);
		//					cs.setInt(3, sucursal);
		//					cs.setInt(4, nroCaja);
		//					cs.setInt(5, transaccion);
		//					cs.setInt(6, transaccion);
		//					cs.setInt(7, tipo);
		//					cs.setLong(8, glTotalNc);
		//					cs.setInt(9, sucursal);
		//					cs.setString(10, fechaBoleta);
		//					cs.setInt(11, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA_BOLETAS")));// trama.getNotaVenta().getNumeroCaja());
		//					cs.setInt(12, trama.getNotaVenta().getNumeroBoleta());
		//					cs.setInt(13, Constantes.NRO_CERO);
		//					cs.setInt(14, Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR")));// TODO
		//																		// Revisar
		//																		// rut
		//																		// es
		//																		// entero
		//					if (vendedor != null) {
		//						vendedor = (vendedor.indexOf('-') > Constantes.NRO_CERO) ? vendedor.substring(0, vendedor.indexOf('-')) : vendedor;
		//					} else {
		//						vendedor = Constantes.STRING_CERO;
		//					}
		//					cs.setInt(15, Integer.parseInt(vendedor));
		//					cs.setInt(16, Constantes.NRO_CERO);
		//					cs.setInt(17, trama.getNotaVenta().getRutComprador());
		//					cs.setInt(18, trama.getNotaVenta().getMontoDescuento());
		//					cs.setInt(19, Constantes.NRO_CERO);
		//					cs.setInt(20, Constantes.NRO_CERO);
		//					cs.setString(21, fechaHora);
		//					cs.setInt(22, Constantes.NRO_CERO);
		//					cs.setInt(23, Constantes.NRO_CERO);
		//					cs.setInt(24, correlativoVenta);
		//					cs.setInt(25, trama.getNotaVenta().getCorrelativoBoleta());
		//					cs.setString(26, FechaUtil.formatTimestamp(
		//							Constantes.FECHA_DDMMYYYY4, trama.getNotaVenta().getFechaCreacion()));
		//
		//					cs.execute();
		//					sal = new Integer(cs.getInt(1));
		//					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
		//						logger.traceError(
		//								"Inserta NC Transaccion BackOffice - "
		//										+ procSQL,
		//								"--> Imposible crear Encabezado Transaccion Error - "
		//										+ procSQL,
		//								new Exception("Resultado obtenido: "
		//										+ sal.toString()));
		//
		//						// se obtiene medio de pago
		//						int tipoDePago = 37;
		//						int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//						int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//						// Se ejecuta anulaci�n de folio, si no logra anular,
		//						// concatena mensaje.
		//						caja.saltarFolio(pcv, transaccion);
		//						if (conn != null) {
		//							PoolBDs.closeConnection(conn);
		//						}
		//						return false;
		//					} else {
		//						if (trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL) {
		//							procSQL = "{CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_NOTAVENTA(?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?)}";
		//							if (connExtend == null) {
		//								connExtend = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//							}
		//							cs = connExtend.prepareCall(procSQL);
		//							cs.setInt(1, Constantes.NRO_TRES);
		//							cs.setInt(2, transaccion);
		//							cs.setString(3, fecha);
		//							cs.setString(4, fechaHora);
		//							cs.setInt(5, nroCaja);
		//							cs.setInt(6, correlativoVenta);
		//							cs.setString(7, vendedor);
		//
		//							cs.registerOutParameter(8, OracleTypes.NUMBER);
		//							cs.registerOutParameter(9, OracleTypes.VARCHAR);
		//
		//							// Log
		//							sql = "CALL CAVIRA_MANEJO_RECA.PROC_UPDATE_NOTAVENTA("
		//									+ Constantes.NRO_TRES
		//									+ ","
		//									+ transaccion
		//									+ ","
		//									+ Constantes.NRO_CERO
		//									+ ","
		//									+ fecha
		//									+ ","
		//									+ Constantes.FECHA_DDMMYYYY
		//									+ ","
		//									+ fechaHora
		//									+ ","
		//									+ Constantes.FECHA_DDMMYYYY_HHMMSS
		//									+ ","
		//									+ nroCaja
		//									+ ","
		//									+ correlativoVenta
		//									+ ","
		//									+ Integer.parseInt(vendedor)
		//									+ ","
		//									+ sucursal
		//									+ ","
		//									+ correlativoVenta
		//									+ ","
		//									+ Constantes.NRO_UNO + ")";
		//							logger.traceInfo("anulacionCDF", sql);
		//							cs.execute();
		//							sal = new Integer(cs.getInt(8));
		//							sal2 = cs.getString(9);
		//							if (Validaciones.validaInteger(sal)
		//									&& sal.intValue() != Constantes.NRO_CERO) {
		//								logger.traceError(
		//										"Creacion NC T.Bancaria BackOffice",
		//										"--> Imposible crear forma de Pago Bancaria Error - "
		//												+ sql,
		//										new Exception("Resultado obtenido: "
		//												+ sal.toString()));
		//
		//								// se obtiene medio de pago
		//								int tipoDePago = 37;
		//								int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//								int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//								
		//								if (conn != null) {
		//									PoolBDs.closeConnection(conn);
		//								}
		//								return false;
		//							}
		//						}
		//					}
		//
		//				}// fin codigo
		//
		//				anulacionCDF = true;
		//			} else {
		//				logger.traceInfo("anulacionCDF",
		//						"Problemas de conexion a la base de datos");
		//			}
		//
		//		} catch (Exception e) {
		//			// se obtiene medio de pago
		//			int tipoDePago = 37;
		//			int rutSupervisor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
		//			int rutVendedor = Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"));
		//			logger.traceError("anulacionCDF",
		//					"Error durante la ejecucion de anulacionCDF", e);
		//
		//			return false;
		//		} finally {
		//			if (conn != null)
		//				PoolBDs.closeConnection(conn);
		//			if (connClaseS != null)
		//				PoolBDs.closeConnection(connClaseS);
		//			if (connExtend != null)
		//				PoolBDs.closeConnection(connExtend);
		//			if (connTRE != null)
		//				PoolBDs.closeConnection(connTRE);
		//			if (connCAR != null)
		//				PoolBDs.closeConnection(connCAR);
		//
		//		}

		logger.endTrace("anulacionCDF", "Finalizado", "Boolean: " + false);
		return anulacionCDF;
	}

	private long getTotalNcMKP(TramaDTE trama) throws AligareException {
		logger.initTrace("getTotalNcMKP", "TramaDTE: trama");
		long totalNC = 0;
		//		try {
		//			String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
		//			Integer vTotal = Constantes.NRO_CERO;
		//			Integer montoTotal = Constantes.NRO_CERO;
		//			Integer vDesc = null;
		//			List<ArticuloVentaTramaDTE> articuloVentas = trama
		//					.getArticuloVentas();
		//
		//			if (articuloVentas.size() > 0) {
		//				for (int i = 0; i < articuloVentas.size(); i++) {
		//					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
		//					if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO // Si NO es NotaCredito lo Salta
		//							|| indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_REGALO)) { // Si es Novios lo Salta
		//						continue;
		//					}
		//
		//					if ((indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS))
		//							&& (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() != Constantes.ES_MKP_MKP)) { // Si es ambas, pregunto en Identificador_marketplace si es es_mkp
		//						continue;
		//					}
		//
		//					Integer precio = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getPrecio())) ? articuloVentaTrama
		//							.getArticuloVenta().getPrecio() : new Integer(
		//							Constantes.NRO_CERO);
		//					Integer descuento = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getMontoDescuento())) ? articuloVentaTrama
		//							.getArticuloVenta().getMontoDescuento()
		//							: new Integer(Constantes.NRO_CERO);
		//					Integer unidades = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getUnidades())) ? articuloVentaTrama
		//							.getArticuloVenta().getUnidades() : new Integer(
		//							Constantes.NRO_CERO);
		//					Integer tipoDesc = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getTipoDescuento())) ? articuloVentaTrama
		//							.getArticuloVenta().getTipoDescuento()
		//							: new Integer(Constantes.NRO_CERO);
		//
		//					vTotal = (descuento.intValue() < Constantes.NRO_CERO) ? new Integer(
		//							(precio.intValue() - descuento.intValue()) * unidades.intValue())
		//							: (precio.intValue() * unidades.intValue());
		//					vDesc = (descuento.intValue() < Constantes.NRO_CERO) ? Constantes.NRO_CERO
		//							: descuento.intValue();
		//
		//					montoTotal = montoTotal.intValue() + vTotal;
		//					if (tipoDesc.intValue() > Constantes.NRO_CERO) {
		//						if (vDesc.intValue() == Constantes.NRO_CERO) {
		//							montoTotal = montoTotal.intValue()
		//									- (vDesc.intValue() * unidades.intValue());
		//						} else {
		//							montoTotal = montoTotal.intValue()
		//									- vDesc.intValue();
		//						}
		//					}
		//					totalNC = montoTotal;
		//
		//				}
		//			}
		//		} catch (Exception e) {
		//			logger.traceError("getTotalNcMKP",
		//					"Error al obtener Total Nota venta ", e);
		//		}

		logger.endTrace("getTotalNcMKP", "Finalizado", "long: " + totalNC);
		return totalNC;
	}

	private Integer getTotalNcMKPxPrv(TramaDTE trama, String ordenMkp)
			throws AligareException {

		logger.initTrace("getTotalNcMKPxPrv", "ordenMkp: "+ordenMkp);
		Integer totalNC = null;
		//		int montoDocto = 0;
		//		try {
		//
		//			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		//
		//			if (articuloVentas.size() > 0) {
		//				for (int i = 0; i < articuloVentas.size(); i++) {
		//					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
		//					logger.traceInfo("getTotalNcMKPxPrv", "\narticuloVentaTrama CorrelativoItem: "+articuloVentaTrama.getArticuloVenta().getCorrelativoItem());
		//					logger.traceInfo("getTotalNcMKPxPrv", "\narticuloVentaTrama EsNC: "+articuloVentaTrama.getArticuloVenta().getEsNC());
		//					if (!ordenMkp.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getOrdenMkp())
		//							|| articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.esNC) {
		//						continue;
		//					} else {
		//
		//						int precio = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getPrecio()))
		//								? articuloVentaTrama.getArticuloVenta().getPrecio()
		//								: Constantes.NRO_CERO;
		//						int descuento = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))
		//								? articuloVentaTrama.getArticuloVenta().getMontoDescuento()
		//								: Constantes.NRO_CERO;
		//						int unidades = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getUnidades()))
		//								? articuloVentaTrama.getArticuloVenta().getUnidades()
		//								: Constantes.NRO_CERO;
		//						int tipoDesc = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getTipoDescuento()))
		//								? articuloVentaTrama.getArticuloVenta().getTipoDescuento()
		//								: Constantes.NRO_CERO;
		//
		//						logger.traceInfo("getTotalNcMKPxPrv", "Precio: "+precio);
		//						logger.traceInfo("getTotalNcMKPxPrv", "Unidades: "+unidades);
		//						logger.traceInfo("getTotalNcMKPxPrv", "MontoDescuento: "+descuento);
		//						logger.traceInfo("getTotalNcMKPxPrv", "tipoDesc: "+tipoDesc);
		//
		//								
		//								
		//						if (descuento > Constantes.NRO_CERO) {
		//							montoDocto = (tipoDesc == Constantes.NRO_CERO)
		//											? montoDocto - (descuento * unidades)
		//											: montoDocto - (descuento);
		//						} else {
		//							montoDocto = montoDocto - (precio * unidades);
		//						}
		//
		//						logger.traceInfo("getTotalNcMKPxPrv", "montoDocto: "+montoDocto);
		//						
		//					}
		//
		//				}
		//			}
		//			if (montoDocto < 0){
		//				montoDocto = montoDocto * -1;
		//			}
		//			
		//			if (montoDocto > 0){
		//				totalNC = new Integer(montoDocto);
		//			}
		//		} catch (Exception e) {
		//			logger.traceError("getTotalNcMKPxPrv", "Error al obtener total NC MKP por Proveedor ", e);
		//		}

		logger.endTrace("getTotalNcMKPxPrv", "Finalizado", "Integer: " + totalNC);
		return totalNC;
	}

	private long getTotalNcCDF(TramaDTE trama) throws AligareException {

		logger.initTrace("getTotalNcCDF", "Trama: trama");
		long totalNC = 0;
		//		try {
		//
		//			String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
		//			Integer vTotal = Constantes.NRO_CERO;
		//			Integer montoTotal = Constantes.NRO_CERO;
		//			Integer vDesc = null;
		//			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		//
		//			if (articuloVentas.size() > 0) {
		//				for (int i = 0; i < articuloVentas.size(); i++) {
		//					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
		//					if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO // Si NO es
		//																// NotaCredito
		//																// lo Salta
		//							|| indicadorMkp
		//									.equalsIgnoreCase(Constantes.IND_MKP_MKP)) { // Si
		//																					// es
		//																					// MarketPlace
		//																					// lo
		//																					// salta
		//						continue;
		//					}
		//
		//					if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { // Si
		//																					// es
		//																					// ambas
		//																					// lo
		//																					// salta
		//						continue;
		//					}
		//
		//					// procesa solo los codigos 3
		//
		//					Integer precio = (Validaciones
		//							.validaInteger(articuloVentaTrama
		//									.getArticuloVenta().getPrecio())) ? articuloVentaTrama
		//							.getArticuloVenta().getPrecio() : new Integer(
		//							Constantes.NRO_CERO);
		//					Integer descuento = (Validaciones
		//							.validaInteger(articuloVentaTrama
		//									.getArticuloVenta().getMontoDescuento())) ? articuloVentaTrama
		//							.getArticuloVenta().getMontoDescuento()
		//							: new Integer(Constantes.NRO_CERO);
		//					Integer unidades = (Validaciones
		//							.validaInteger(articuloVentaTrama
		//									.getArticuloVenta().getUnidades())) ? articuloVentaTrama
		//							.getArticuloVenta().getUnidades() : new Integer(
		//							Constantes.NRO_CERO);
		//					Integer tipoDesc = (Validaciones
		//							.validaInteger(articuloVentaTrama
		//									.getArticuloVenta().getTipoDescuento())) ? articuloVentaTrama
		//							.getArticuloVenta().getTipoDescuento()
		//							: new Integer(Constantes.NRO_CERO);
		//
		//					vTotal = (descuento.intValue() < Constantes.NRO_CERO) ? new Integer(
		//							(precio.intValue() - descuento.intValue())
		//									* unidades.intValue())
		//							: (precio.intValue() * unidades.intValue());
		//
		//					vDesc = (descuento.intValue() < Constantes.NRO_CERO) ? Constantes.NRO_CERO
		//							: descuento.intValue();
		//
		//					montoTotal = montoTotal.intValue() + vTotal;
		//					if (tipoDesc.intValue() > Constantes.NRO_CERO) {
		//						if (vDesc.intValue() == Constantes.NRO_CERO) {
		//							montoTotal = montoTotal.intValue()
		//									- (vDesc.intValue() * unidades.intValue());
		//						} else {
		//							montoTotal = montoTotal.intValue()
		//									- vDesc.intValue();
		//						}
		//					}
		//
		//					totalNC = montoTotal;
		//
		//				}
		//			}
		//		} catch (Exception e) {
		//			logger.traceError("getTotalNcCDF",
		//					"Error al obtener Total Nota venta ", e);
		//		}

		logger.endTrace("getTotalNcCDF", "Finalizado", "long: " + totalNC);
		return totalNC;
	}

	private Saldos getSaldos(TramaDTE trama) {

		logger.initTrace(
				"getSaldos",
				"Trama: trama",
				new KeyLog("Correlativo venta: ", ""
						+ (trama.getNotaVenta().getCorrelativoVenta())));
		Saldos saldos = new Saldos();
		//		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		//		if (conn != null) {
		//			String selectSQL = "";
		//			try {
		//				selectSQL = "SELECT saldo_tre, saldo_car,saldo_total FROM nota_credito WHERE corr_nc in(select max(corr_nc) from nota_credito where correlativo_venta = ?)";
		//				ResultSet rs = null;
		//				PreparedStatement ps = conn.prepareStatement(selectSQL);
		//				if (ps != null) {
		//					ps.setInt(1, trama.getNotaVenta().getCorrelativoVenta());
		//					rs = ps.executeQuery();
		//
		//					logger.traceInfo("getSaldos",
		//							"Rescata saldo de tabla nota_credito: Ejecuci�n de query OK!");
		//					if (rs != null && rs.next()) {
		//						if (rs.getInt(1) > 0) {
		//							saldos.setSaldoTRE(rs.getLong(1));
		//							saldos.setSaldoCAR(rs.getLong(2));
		//							saldos.setSaldoTotal(rs.getLong(3));
		//						}
		//						rs.close();
		//					}
		//					ps.close();
		//				}
		//
		//			} catch (Exception e) {
		//				logger.traceError("getSaldos",
		//						"Error durante la ejecuci�n de getSaldos", e);
		//				logger.endTrace("getSaldos", "Finalizado", "Saldos: null");
		//				return null;
		//			} finally {
		//				PoolBDs.closeConnection(conn);
		//			}
		//		} else {
		//			logger.traceInfo("getSaldos",
		//					"Problemas de conexion a la base de datos");
		//		}
		logger.endTrace("getSaldos", "Finalizado",
				"Saldos: " + saldos.toString());

		return saldos;
	}

	private Integer tieneCud(String articulo) {
		logger.initTrace("tieneCud", "String: " + articulo);
		Integer sal = Constantes.NRO_CERO;
		String query = "select count(*) as CANTIDAD from tvirtual_articuloS_sin_cud where cod_art_ean13 = ?";
		String q = "select count(*) as CANTIDAD from tvirtual_articuloS_sin_cud where cod_art_ean13 = "
				+ articulo;
		logger.traceInfo("tieneCud", "Consulta a Back Office: " + q);
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn != null) {
			try {
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(query);
				if (ps != null) {
					ps.setString(1, articulo);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						sal = rs.getInt("CANTIDAD");
						rs.close();
					}
					ps.close();
				}
			} catch (Exception e) {
				logger.traceError(
						"tieneCud",
						"Se produjo un error en la ejecucion de la Query - Error: ",
						e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("tieneCud",
					"Problemas de conexion a la base de datos BACK OFFICE");
		}
		logger.endTrace("tieneCud", "Finalizado", "Integer: " + sal.intValue());
		return sal;
	}

	private boolean esMkpSinStock(TramaDTE trama, Long correlativoVenta,
			String ordenMkp) {
		logger.initTrace("esMkpSinStock", "TramaDTE : trama", new KeyLog(
				"correlativoVenta: ", "" + correlativoVenta), new KeyLog(
						"ordenMkp", ordenMkp));
		boolean sinStock = false;
		List<ArticuloVentaTramaDTE> listArticuloVenta = trama.getArticuloVentas();
		for (ArticuloVentaTramaDTE artVenta : listArticuloVenta) {

			// Se busca el articulo en la lista por correlativo y orden mkp.
			if (correlativoVenta != artVenta.getArticuloVenta().getCorrelativoVenta()
					|| !ordenMkp.equalsIgnoreCase(artVenta.getArticuloVenta().getOrdenMkp())) {
				continue;
			}
			// Si se encuentra se pregunta por Stock
			else if (artVenta.getArticuloVenta().getEstadoVenta() != null && Constantes.SIN_STOCK.equalsIgnoreCase(artVenta.getArticuloVenta().getEstadoVenta().trim())) {
				sinStock = true;
			}

		}

		logger.endTrace("esMkpSinStock", "�Es MKP sin Stock?", "" + sinStock);

		return sinStock;

	}

	private void envioTrxBo(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) throws RestServiceTransactionException {

		logger.initTrace("envioTrxBo", "TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp", 
				new KeyLog("inOrdenCompra", String.valueOf(inOrdenCompra)),
				new KeyLog("nroTransaccion", String.valueOf(nroTransaccion)),
				new KeyLog("esMkp", String.valueOf(esMkp)));
		
		
		PrepareBoCallAnulacionMkp source = new PrepareBoCallAnulacionMkp();
		source.setEsMkp(esMkp);
		source.setInOrdenCompra(inOrdenCompra);
		source.setNroTransaccion(nroTransaccion);
		source.setParametros(parametros);
		
		RequestBO request = conversionService.convert(source, RequestBO.class);
		
		logger.traceInfo("envioTrxBo", "Objeto request generado", 
				new KeyLog("Objeto Request RequestBO", String.valueOf(request)));
		
		String url = parametros.buscaValorPorNombre("URL_BACKOFFICE");
		
		logger.traceInfo("envioTrxBo", "URL para servicio REST", 
				new KeyLog("URL", url));
		
		logger.traceInfo("envioTrxBo", "Invocando servicio REST");
		
		BackOfficeResponse resp = rt.postForObject(url, request, BackOfficeResponse.class);
		
		logger.traceInfo("envioTrxBo", "Respuesta servicio REST", 
				new KeyLog("Objeto respuesta BackOfficeResponse", String.valueOf(resp)));
		
		if(resp.getCodigo().intValue() != Constantes.NRO_UNO) {
			
			logger.traceInfo("envioTrxBo", "Resultado no exitoso", 
					new KeyLog("Codigo respuesta", String.valueOf(resp.getCodigo())),
					new KeyLog("Mensaje respuesta", String.valueOf(resp.getMensaje())));
			
			logger.endTrace("envioTrxBo", "Servicio REST invocado con error", "resultado = " + resp);
			throw new RestServiceTransactionException(resp.getMensaje(), resp.getCodigo());
			
		}
		
		logger.endTrace("envioTrxBo", "Servicio REST invocado exitosamente", "resultado = " + resp);

	}

}
