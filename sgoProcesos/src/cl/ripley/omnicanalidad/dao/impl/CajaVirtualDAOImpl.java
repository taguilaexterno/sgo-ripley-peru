package cl.ripley.omnicanalidad.dao.impl;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.restservices.model.backOfficeRest.BackOfficeResponse;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallApertura;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallCierre;
import com.ripley.restservices.model.backOfficeRest.RequestBO;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.FMaxTVirtualTipo;
import cl.ripley.omnicanalidad.bean.OrdenesImpresas;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.TotalCuadratura;
import cl.ripley.omnicanalidad.bean.TotalRecaudacion;
import cl.ripley.omnicanalidad.bean.TotalU;
import cl.ripley.omnicanalidad.bean.TotalVentasBanc;
import cl.ripley.omnicanalidad.bean.TotalVentasTRE;
import cl.ripley.omnicanalidad.bean.TotalesCierre;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.CodErrorSystemExit;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;

/**Clase DAO para operaciones de caja.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class CajaVirtualDAOImpl implements CajaVirtualDAO {

	private static final AriLog logger = new AriLog(CajaVirtualDAOImpl.class,
			Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ConversionService conversionService;

	@Override
	public boolean existeTrx(Parametros pcv, int nroTrx, String fecha)
		throws AligareException {
		logger.initTrace("existeTrx", "Parametros: Parametros, int nroTrx: "+nroTrx, new KeyLog("nroTrx: ", ""+nroTrx));
		boolean existeTrx = false;
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn!=null){
		String selectSQL = "";
		
		try {
			 if (fecha == null){
			
					selectSQL = "SELECT COUNT(*) FROM trx_transaccion WHERE SUCURSAL=? AND NRO_CAJA=? AND NRO_TRANSACCION=?";
					ResultSet rs = null;
					PreparedStatement ps = conn.prepareStatement(selectSQL);
					if (ps != null) {
						ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(3, nroTrx);
						rs = ps.executeQuery();
						
						if (rs != null && rs.next()) {
							if (rs.getInt(1) > 0) {
								logger.traceInfo("existeTrx", "existeTrx = true");
								existeTrx = true;
							}
							rs.close();
						}
						ps.close();
					}
					selectSQL = "SELECT COUNT(*) FROM trx_transaccion WHERE SUCURSAL="+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+
							" AND NRO_CAJA="+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+
							" AND NRO_TRANSACCION="+nroTrx;
			 }else{
				 
				 
				 selectSQL = "SELECT COUNT(*) FROM trx_transaccion WHERE FECHA_TRX = TO_DATE(?,'DD/MM/YYYY') AND SUCURSAL=? AND NRO_CAJA=? AND NRO_TRANSACCION=?";
					ResultSet rs = null;
					PreparedStatement ps = conn.prepareStatement(selectSQL);
					if (ps != null) {
						ps.setString(1, fecha);
						ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(4, nroTrx);
						rs = ps.executeQuery();
						
						if (rs != null && rs.next()) {
							if (rs.getInt(1) > 0) {
								logger.traceInfo("existeTrx", "existeTrx = true");
								existeTrx = true;
							}
							rs.close();
						}
						ps.close();
					}
				 
					selectSQL = "SELECT COUNT(*) FROM trx_transaccion WHERE FECHA_TRX = TO_DATE("+ fecha +",'DD/MM/YYYY') and  SUCURSAL="+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+
							" AND NRO_CAJA="+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+
							" AND NRO_TRANSACCION="+nroTrx;
					
			 }

			
			logger.traceInfo("existeTrx", "SQL: "+selectSQL);

		} catch (Exception e) {
			logger.traceError("existeTrx", "Error durante la ejecuci�n de existeTrx", e);
			logger.endTrace("existeTrx", "Finalizado", "boolean: " + existeTrx);
			return existeTrx;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("existeTrx", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("existeTrx", "Finalizado", "boolean: " + existeTrx);
		return existeTrx;
	}
	@Override
	public boolean creaCajaTvirtualBoletas(Parametros pcv) throws AligareException {
		logger.initTrace("creaCajaTvirtualBoletas", "Parametros: Parametros",
				new KeyLog("NRO_CAJA: ", pcv.buscaValorPorNombre("nro_caja")));
		Connection conn = null;
		boolean seCreaCaja = false;
		try {
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String selectSQL = "";
				selectSQL = "SELECT NVL(NRO_TRANSACCION,0) NRO_TRANSACCION  FROM TVIRTUAL_BOLETAS WHERE NRO_CAJA =?";
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"));
				if (ps != null) {
					ps.setInt(1, nroCaja);
					rs = ps.executeQuery();
					if (rs != null) {
						if (!rs.next()) {	
							seCreaCaja = true;
							String insertSQL = "INSERT INTO TVIRTUAL_BOLETAS VALUES (?,0,1,0)";
							ps = conn.prepareStatement(insertSQL);
							ps.setInt(1, nroCaja);
							ps.executeUpdate();
							insertSQL = "INSERT INTO TVIRTUAL_BOLETAS VALUES ("+nroCaja+",0,1,0)";
							logger.traceInfo("creaCajaTvirtualBoletas", "Insert: "+ insertSQL);
						}else{
							selectSQL = "SELECT NVL(NRO_TRANSACCION,0) NRO_TRANSACCION  FROM TVIRTUAL_BOLETAS WHERE NRO_CAJA ="+nroCaja;
							logger.traceInfo("creaCajaTvirtualBoletas", "La caja ya existe en Tvirtual. SQL: "+selectSQL);
						}
						
					ps.close();
				}

			}
			}
		catch (Exception e) {
			logger.traceError("creaCajaTvirtualBoletas", e);
			throw new AligareException(
					"Imposible realizar conexion a 'BACK_OFFICE' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("creaCajaTvirtualBoletas", "Finalizado", "�Se crea caja?: "+seCreaCaja);

		}
		return seCreaCaja;
	}
	@Override
	public Integer resNroTransaccion(Parametros pcv, String fecha) throws AligareException {
		logger.initTrace("resNroTransaccion", "Parametros: Parametros",
				new KeyLog("NRO_CAJA: ", pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME))
				,new KeyLog("NRO_SUCURSAL: ", pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
		Connection conn = null;
		int nroTrx = 0;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL = "";
			selectSQL =	"SELECT NRO_TRX + 1 " + 
						"FROM CV_CONTROL_TRX " + 
						"WHERE TO_CHAR(FECHA, ?) = ? " + 
						"AND NRO_CAJA = ? AND NRO_SUCURSAL = ?";
			ResultSet rs = null;
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			
			ps.setString(1, Constantes.FECHA_YYYY_MM_DD);
			ps.setString(2, fecha);
			ps.setInt(3, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
			ps.setInt(4, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));

			logger.traceInfo("resNroTransaccion", "se obtiene resultado de consulta (Nro Transaccion)");
			if (ps != null) {
				rs = ps.executeQuery();
				if (rs != null && rs.next()) {
					nroTrx = rs.getInt(1);					
					rs.close();
				} else {
					
					rs.close();
					ps.close();
					
					String selectSQL2 = "UPDATE	CV_CONTROL_TRX " + 
								"SET	NRO_TRX = 0, " + 
								"		FECHA = TO_DATE(?, ?) " + 
								"WHERE	NRO_CAJA = ? AND NRO_SUCURSAL = ?";
					
					ps = conn.prepareStatement(selectSQL2);
					
					ps.setString(1, fecha);
					ps.setString(2, Constantes.FECHA_YYYY_MM_DD);
					ps.setInt(3, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
					ps.setInt(4, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
					
					if(ps.executeUpdate() != Constantes.NRO_CERO) {
						
						ps.close();
						
						ps = conn.prepareStatement(selectSQL);
						ps.setString(1, Constantes.FECHA_YYYY_MM_DD);
						ps.setString(2, fecha);
						ps.setInt(3, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
						
						rs = ps.executeQuery();
						
						if(rs != null && rs.next()) {
							
							nroTrx = rs.getInt(1);					
							rs.close();
							
						}
						
					}
					
				}
				
				if(ps != null) {
					ps.close();
				}
				logger.traceInfo("resNroTransaccion", "Se consulta existencia de la nroTransaccion");

			}
			
		} catch (Exception e) {
			logger.traceError("resNroTransaccion", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);

			if (nroTrx == Constantes.NRO_CERO){
				logger.traceInfo("resNroTransaccion", "SYSTEM EXIT ERROR: La caja no está creada en tabla CV_CONTROL_TRX");
				logger.endTrace("resNroTransaccion", "Finalizado", "Nro Trx disponible: "+ nroTrx);
//				System.exit(CodErrorSystemExit.CAJA_NO_DISPONIBLE);
			}else{
				logger.endTrace("resNroTransaccion", "Finalizado", "Nro Trx disponible: "+ nroTrx);
			}

		}

		return nroTrx;

	}

	@Override
	public boolean aperturaCaja(Parametros pcv, int nroTrx, String fecha, String fechaHora) throws AligareException {
		logger.initTrace("aperturaCaja", "Parametros: Parametros, int nroTrx: "+nroTrx+", String fecha: "+fecha+", String fechaHora: "+fechaHora, new KeyLog("nro Trx: ", ""+nroTrx));
		// variables
		Connection conn = null;
		CallableStatement cs = null;
		PreparedStatement ps = null;
		boolean cajaAbierta = false;
				
		try {
			// Pido conecci�n
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String selectSQL = "";
			String procSQL = "{call apertura_caja2(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY HH24:MI:SS'))}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, fecha);
			cs.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
			cs.setInt(3,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
			cs.setInt(4, nroTrx);
			cs.setInt(5, Integer.parseInt(pcv.buscaValorPorNombre("nro_boleta")));
			cs.setInt(6, Integer.parseInt(pcv.buscaValorPorNombre("cod_apertura")));
			cs.setInt(7, Integer.parseInt(pcv.buscaValorPorNombre("rut_vendedor")));
			cs.setInt(8, Integer.parseInt(pcv.buscaValorPorNombre("rut_supervisor")));
			cs.setInt(9, Integer.parseInt(pcv.buscaValorPorNombre("origen")));
			cs.setString(10, fechaHora);
			// ejecuta sp			
			cs.execute();
				procSQL = "{call apertura_caja2(to_date("+fecha+",'DD/MM/YYYY'),"
						+ Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+","
						+ nroTrx+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("nro_boleta"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("cod_apertura"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("rut_vendedor"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("rut_supervisor"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("origen"))+","
						+ "to_date("+fechaHora+",'DD/MM/YYYY HH24:MI:SS'))}";
				
				logger.traceInfo("aperturaCaja", "Estado de ejecuci�n apertura_caja2: true");
				logger.traceInfo("aperturaCaja", "PL/SQL: "+ procSQL);
				
				
				
				//Update tabla tvirtual_boletas en  funci�n nota de credito
				if (Integer.parseInt(pcv.buscaValorPorNombre("funcion")) == Constantes.FUNCION_NC){
					selectSQL = "update tvirtual_boletas set nro_nc = ? where nro_caja = ?";
					ps = conn.prepareStatement(selectSQL);				
				}
				//Update tabla tvirtual_boletas en  funci�n boleta
				else if (Integer.parseInt(pcv.buscaValorPorNombre("funcion")) == Constantes.FUNCION_BOLETA){
					selectSQL = "update tvirtual_boletas set nro_boleta = ? where nro_caja = ?";
					ps = conn.prepareStatement(selectSQL);
				}
				ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("nro_boleta")));
				ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				if (ps.executeUpdate()>0){				
					cajaAbierta = true;
				}						
			cs.close();

		} catch (Exception e) {
			logger.traceError("aperturaCaja", e);
			logger.traceInfo("aperturaCaja", "SYSTEM EXIT ERROR: No se realiza APERTURA de caja correctamente");
			logger.endTrace("aperturaCaja", "Finalizado", "aperturaCaja: " + cajaAbierta);
			System.exit(CodErrorSystemExit.APERTURA_CIERRE);
		} finally {
			PoolBDs.closeConnection(conn);
		}
		
		logger.endTrace("aperturaCaja", "Finalizado", "aperturaCaja: " + cajaAbierta);
		return cajaAbierta;
	}

	@Override
	public boolean backOffice(int sucursal, String fecha) throws AligareException {
		
		logger.initTrace("backOffice", "int sucursal: " + sucursal+", String fecha: "+fecha, new KeyLog("sucursal", ""+sucursal));
		//variables
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		boolean sucursalAbierta=false;
		try {
			
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String selectSQL = "";
			
				selectSQL = "SELECT NVL(ESTADO,0) ESTADO FROM TRX_ESTADO_BASE WHERE SUCURSAL=? "
						+ "AND TRUNC(FECHA_TRX) = TO_DATE(?,'dd/mm/yyyy')";
				ps = conn.prepareStatement(selectSQL);
				logger.traceInfo("backoffice", "Se ejecuta query:");
				if (ps != null) {
					ps.setInt(1, sucursal);
					ps.setString(2, fecha);
					
					rs = ps.executeQuery();
					
					if (rs != null && rs.next() && rs.getInt(1)>0) {
						sucursalAbierta=true;
						rs.close();
					}
					ps.close();
				}
				selectSQL = "SELECT NVL(ESTADO,0) ESTADO FROM TRX_ESTADO_BASE WHERE SUCURSAL="+ sucursal
						+ " AND TRUNC(FECHA_TRX) = TO_DATE('"+fecha+"','dd/mm/yyyy')";
				
				logger.traceInfo("backOffice", "SQL: "+selectSQL);

		} catch (Exception e) {
			logger.traceError("backOffice", e);
			throw new AligareException("Error en obtenci�n de estado en backOffice: "+ e.getMessage());
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("backOffice", "Finalizado", "Sucursal est� Abierta?: "+sucursalAbierta);
		}
		
		return sucursalAbierta;
	}

	@Override
	public boolean actualizaTransaccion(int nroCaja, int nroTrx) throws AligareException {
		
		logger.initTrace("actualizaTransaccion", "int nroCaja: " + nroCaja+", int nroTrx: "+nroTrx, 
				new KeyLog("nroTrx", ""+nroTrx));
		//variables
		Connection conn = null;
		boolean resultado=false;
		try {
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String updateSQL = "";
			
			updateSQL = "UPDATE TVIRTUAL_BOLETAS SET NRO_TRANSACCION =? WHERE NRO_CAJA =?";
			PreparedStatement ps = conn.prepareStatement(updateSQL);

			if (ps != null) {
				ps.setInt(1, nroTrx);
				ps.setInt(2, nroCaja);
				if (ps.executeUpdate() > 0) {
					resultado = true;
					logger.traceInfo("actualizaTransaccion", "Se actualiza transacci�n");
				}
				ps.close();
			}
			updateSQL = "UPDATE TVIRTUAL_BOLETAS SET NRO_TRANSACCION ="+nroTrx+" WHERE NRO_CAJA ="+nroCaja;
			logger.traceInfo("actualizaTransaccion", "SQL:"+updateSQL);
		} catch (Exception e) {
			logger.traceError("actualizaTransaccion", e);
			throw new AligareException("Ha ocurrido un error al momento de actualizar en ActualizaTransaccion: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("actualizaTransaccion", "Finalizado", "Se ejecuto Update Correctamente: "
					+ resultado);
		}
		return resultado;
	}
				
	public String sysdateFromDual(String formato, Parametros pcv)throws AligareException{
		
		logger.initTrace("sysdateFromDual", "String formato: "+formato);
		//variables
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		CallableStatement cs = null;
		String strDate = "";
	
		if (conn != null) {
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBTIENE_FECHA(?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, formato);
				cs.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				cs.registerOutParameter(4, OracleTypes.VARCHAR);//2
				cs.registerOutParameter(5, OracleTypes.VARCHAR);//3
				cs.registerOutParameter(6, OracleTypes.NUMBER);
				cs.registerOutParameter(7, OracleTypes.VARCHAR);
				
				procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBTIENE_FECHA("+formato+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+","
						+ "?,?,?,?)}";
				
				logger.traceInfo("sysdateFromDual", procSQL);
				cs.executeQuery();
				strDate=cs.getString(4);
			
				cs.close();
				
				
			} catch (Exception e) {
				logger.traceError("sysdateFromDual", "Error durante la ejecuci�n de sysdateFromDual",e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("sysdateFromDual", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("sysdateFromDual", "Finalizado", "Fecha:" + strDate);
		return strDate;
	}

	@Override
	public boolean cambioEstadoCaja(Parametros pcv, int estado, int nroTrx, String fecha)
			throws AligareException {
		logger.initTrace("cambioEstadoCaja", "Parametros: Parametros, int estado: "+estado+", int nroTrx:"+nroTrx+", String fecha: "+fecha, 
				new KeyLog("Estado", ""+estado), new KeyLog("nroTrx", ""+nroTrx));
		//variables
		Connection conn = null;
		boolean updateExitoso=false;
		try {
			
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String updateSQL = "";
			
				updateSQL = "update tvirtual_caja set estado =?,nro_transaccion =?,supervisor =?,vendedor =? "
						  + "where nro_caja =? and sucursal =? and comercio =? "
						  + "and TRUNC(fecha) = (SELECT MAX(FECHA) FROM TVIRTUAL_CAJA " 
						  + "where nro_caja =? and sucursal =? and comercio =? )";
				PreparedStatement ps = conn.prepareStatement(updateSQL);

				if (ps != null) {
					ps.setInt(1,estado);
					ps.setInt(2, nroTrx);
					ps.setString(3, pcv.buscaValorPorNombre("RUT_SUPERVISOR"));
					ps.setString(4, pcv.buscaValorPorNombre("RUT_VENDEDOR"));
					ps.setInt(5, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
					ps.setInt(6, Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL")));
					ps.setInt(7, Integer.parseInt(pcv.buscaValorPorNombre("COMERCIO")));
					ps.setInt(8, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
					ps.setInt(9, Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL")));
					ps.setInt(10, Integer.parseInt(pcv.buscaValorPorNombre("COMERCIO")));
					logger.traceInfo("test", updateSQL);
					if (ps.executeUpdate() > 0) {
						updateExitoso = true;
						logger.traceInfo("cambioEstadoCaja", "Update exitoso!!!");
					}
					
					updateSQL = "update tvirtual_caja set estado ="+estado
								+ ",nro_transaccion ="+nroTrx
								+ ",supervisor ="+pcv.buscaValorPorNombre("RUT_SUPERVISOR")
								+ ",vendedor ="+pcv.buscaValorPorNombre("RUT_VENDEDOR")
								+ " where nro_caja ="+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
								+ " and sucursal ="+Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"))
								+ " and comercio ="+Integer.parseInt(pcv.buscaValorPorNombre("COMERCIO"))
								+ " and TRUNC(fecha) = (SELECT MAX(FECHA) FROM TVIRTUAL_CAJA" 
								+ " where nro_caja = "+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
								+ " and sucursal = "+Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"))
								+ " and comercio = "+Integer.parseInt(pcv.buscaValorPorNombre("COMERCIO"))+")";
		
					ps.close();
					logger.traceInfo("cambioEstadoCaja", "SQL: "+updateSQL);
				}
			
		} catch (Exception e) {
			logger.traceError("cambioEstadoCaja", e);
			throw new AligareException("Error en cambioEstadoCaja: ");
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("cambioEstadoCaja", "Finalizado", "Se cambia estado?: "
					+ updateExitoso);
		}
		
		return updateExitoso;
	}

	@Override
	public boolean actualizaBoleta(Parametros pcv, int nroBoleta)throws AligareException {
		
		logger.initTrace("actualizaBoleta", "Parametros: Parametros, int nroBoleta: "+nroBoleta, 
				new KeyLog("NroBoleta", ""+nroBoleta));
		//variables
		Connection conn = null;
		boolean updateExitoso=false;
		try {
			conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
			String updateSQL = "";
			
			//Update tabla tvirtual_boletas en funci�n boleta
			if (Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")) == Constantes.FUNCION_BOLETA){
				updateSQL = "update tvirtual_boletas set nro_boleta = ? where nro_caja = ?";		
			}
			//Update tabla tvirtual_boletas en funci�n  nota de credito
			else if (Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")) == Constantes.FUNCION_NC){
				updateSQL = "update tvirtual_boletas set nro_NC = ? where nro_caja = ?";
			}		
			
			PreparedStatement ps = conn.prepareStatement(updateSQL);
			if (ps != null) {
				ps.setInt(1,nroBoleta);
				ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
				if (ps.executeUpdate() > 0) {
					updateExitoso = true;
					logger.traceInfo("actualizaBoleta", "Update exitoso!!!");
				}
				ps.close();
			}

		} catch (Exception e) {
			logger.traceError("actualizaBoleta", e);
			throw new AligareException("Error en actualizaBoleta: ");
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("actualizaBoleta", "Finalizado", "updateExitoso: "
					+ updateExitoso);
		}
		
		return updateExitoso;
	}

	@Override
	public void cambiaFechaBoletasSinProcesar(String fecha, Parametros pcv) throws AligareException {
		
		logger.initTrace("cambiaFechaBoletasSinProcesar", "String Fecha: " + fecha);
		//variables
		Connection conn = null;
		CallableStatement cs = null;
		int cod=0;
		String mje="";
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_CAMB_FEC_BOL_SIN_PROCESAR(TO_DATE(?,'" + Constantes.FECHA_YYYY_MM_DD + "'),?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setString(1, fecha);
			cs.setInt(2, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);		
			cs.execute();   
			cod = cs.getInt(3);
			mje = cs.getString(4);
            cs.close();
            
            logger.traceInfo("cambiaFechaBoletasSinProcesar","Ejecuci�n de Procedure PROC_CAMB_FEC_BOL_SIN_PROCESAR: "+mje);
            procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_CAMB_FEC_BOL_SIN_PROCESAR(TO_DATE('"+fecha+"','" + Constantes.FECHA_YYYY_MM_DD + "'),"+
            		pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME) +","+cod+","+mje+")}";
            logger.traceInfo("cambiaFechaBoletasSinProcesar","SQL:"+procSQL);
			
		} catch (Exception e) {
			logger.traceError("cambiaFechaBoletasSinProcesar", e);
			throw new AligareException("Error en cambiaFechaBoletasSinProcesar: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("cambiaFechaBoletasSinProcesar", "Finalizado", "Tipo Void: "+mje+" cod: "+cod);
		}
		
	}

	@Override
	public Long obtieneMayorIdAperturaCierre() throws AligareException {
		logger.initTrace("obtieneMayorIdAperturaCierre", null);
		//variables
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		CallableStatement cs = null;
		Long nroMayId=0L;
		
		if (conn != null) {
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_MAYOR_ID_APERTURA_CIERRE(?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.registerOutParameter(2, OracleTypes.NUMBER);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				logger.traceInfo("obtieneMayorIdAperturaCierre", "{CALL CAVIRA_MANEJO_CAJA.PROC_MAYOR_ID_APERTURA_CIERRE(outn,outn,outv,outv)}");
				cs.execute();
				if (cs.getInt(1) > 0) {
					nroMayId = cs.getLong(1);
				}
				
				cs.close();
				
				//logger.traceInfo("obtieneMayorIdAperturaCierre", "Qry CLOB: "+qry);

			} catch (Exception e) {
				logger.traceError(
						"obtieneMayorIdAperturaCierre",
						"Error durante la ejecuci�n de obtieneMayorIdAperturaCierre",
						e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtieneMayorIdAperturaCierre",
					"Problemas de conexion a la base de datos");
		}
		logger.endTrace("obtieneMayorIdAperturaCierre", "Finalizado",
				"Numero Mayor Id: " + nroMayId);
		return nroMayId;
	}
	
	@Override
	public Long obtieneIdAperturaCierre(Parametros pcv) throws AligareException {
		logger.initTrace("obtieneIdAperturaCierre", "Parametros: Parametros");
		//variables
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		CallableStatement cs = null;
		Long nroMayId=0L;
		
		if (conn != null) {
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBT_ID_APERTURA_CIERRE(?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
				cs.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
				cs.registerOutParameter(3, OracleTypes.NUMBER);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				logger.traceInfo("obtieneIdAperturaCierre", "{CALL CAVIRA_MANEJO_CAJA.PROC_OBT_ID_APERTURA_CIERRE("+pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)+
						", "+pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)+",outn,outn,outv,outv)}");
				cs.execute();
				if (cs.getInt(3) > 0) {
					nroMayId = cs.getLong(3);
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError(
						"obtieneIdAperturaCierre",
						"Error durante la ejecuci�n de obtieneIdAperturaCierre",
						e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtieneIdAperturaCierre",
					"Problemas de conexion a la base de datos");
		}
		logger.endTrace("obtieneIdAperturaCierre", "Finalizado","Numero Mayor Id: " + nroMayId);
		return nroMayId;
	}


	@Override
	public RetornoEjecucion registraAperturaCierreCaja(Parametros pcv, int nroTrxApe, int nroTrxCie, String fechaApertura, String fechaCierre, int tipoLlamada, Long nroIdApertura) throws AligareException {
		logger.initTrace("registraAperturaCierreCaja", "Parametros: Parametros, int nroTrxApe: "+nroTrxApe+", int nroTrxCie: "+nroTrxCie+", String fechaApertura: "+fechaApertura+", String fechaCierre: "+fechaCierre+", int tipoLlamada: "+tipoLlamada+", int nroIdApertura: "+nroIdApertura, 
				new KeyLog("nroTrxApe", ""+nroTrxApe),new KeyLog("nroTrxCie", ""+nroTrxCie),new KeyLog("tipoLlamada", ""+tipoLlamada),new KeyLog("nroIdApertura", ""+nroIdApertura));
		//variables
		RetornoEjecucion retorno = new RetornoEjecucion();
		Connection conn = null;
		CallableStatement cs = null;
		try {
			//obtengo numero de apertura para enviarlo por parametro.
			if(nroTrxApe==0){
				nroTrxApe=maximaTrxApertura(pcv, fechaCierre, nroIdApertura);
			}
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_REG_APERTURA_CIERRE_CAJA(?,?,?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
			cs.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
			cs.setInt(3, nroTrxApe);
			cs.setInt(4,nroTrxCie); 
			cs.setString(5, fechaApertura);
			cs.setString(6, fechaCierre);//en duro
			cs.setInt(7, tipoLlamada);//en duro
			cs.setLong(8, nroIdApertura);
			cs.registerOutParameter(9, OracleTypes.NUMBER);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);		
			cs.registerOutParameter(11, OracleTypes.VARCHAR);
			logger.traceInfo("registraAperturaCierreCaja", "{CALL CAVIRA_MANEJO_CAJA.PROC_REG_APERTURA_CIERRE_CAJA("+pcv.buscaValorPorNombre(
					Constantes.NRO_CAJA_NAME)+","+pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)+", "+nroTrxApe+","+nroTrxCie+",'"+
					fechaApertura+"','"+fechaCierre+"',"+tipoLlamada+","+nroIdApertura+",outn,outv,outv)}");
			cs.execute();   			
			retorno.setCodigo(cs.getInt(9));
			retorno.setMensaje(cs.getString(10));
			if (retorno.getCodigo().intValue()==Constantes.EJEC_SIN_ERRORES){
				logger.traceInfo("registraAperturaCierreCaja", "Registro exitoso!");
			}
            cs.close();			
		} catch (Exception e) {
			logger.traceError("registraAperturaCierreCaja", e);
			throw new AligareException("Error en registraAperturaCierreCaja: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("registraAperturaCierreCaja", "Finalizado", "Intento registro. Codigo Retorno igual a 0 (exitoso) Distinto a 0 (con error). Codigo retornado es: " + retorno.getCodigo().intValue());
		}
		return retorno;
	}

	@Override
	public int maximaTrxApertura(Parametros pcv, String fecha, Long nroIdApertura)throws AligareException {
		logger.initTrace("maximaTrxApertura", "Parametros: Parametros, String fecha: "+fecha, 
				new KeyLog("Fecha", fecha));
		//variables
		Connection conn = null;
		int maxTrxApertura=0;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL = "";
			
//				selectSQL = "SELECT MAX(NRO_TRANSACCION) "
//						  + "FROM TRX_TRANSACCION "
//						  + "WHERE SUCURSAL =? "
//						  + "AND TRUNC(FECHA_TRX) >= TO_DATE(?,'DD/MM/YYYY')"
//						  + "AND NRO_CAJA = ? AND TIPO_TRX = ?";
			
			selectSQL = "SELECT x.NRO_TRX_APERTURA " + 
					"FROM XAPERTURA_CIERRE_TRX_BO x " + 
					"WHERE x.ID_APERTURA_CIERRE=? " +
					"AND TO_CHAR(x.FEC_HOR_APERTURA, '" + Constantes.FECHA_YYYY_MM_DD + "') = ?";
			
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);

				if (ps != null) {
//					ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL")));
//					ps.setString(2, fecha);
//					ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
//					ps.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("COD_TRX_APERTURA")));
					
					ps.setLong(1, nroIdApertura);
					ps.setString(2, fecha);
					
					selectSQL = "SELECT x.NRO_TRX_APERTURA " + 
							"FROM XAPERTURA_CIERRE_TRX_BO x " + 
							"WHERE x.ID_APERTURA_CIERRE=" + nroIdApertura +
							"AND TO_CHAR(x.FEC_HOR_APERTURA, '" + Constantes.FECHA_YYYY_MM_DD + "') = '" + fecha + "'";
							
					
					logger.traceInfo("maximaTrxApertura", "Query: "+selectSQL);
					
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						maxTrxApertura = rs.getInt(1);
						rs.close();
					}
					ps.close();
					
				}

		} catch (Exception e) {
			logger.traceError("maximaTrxApertura", e);
			throw new AligareException("Error en maximaTrxApertura: ");
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("maximaTrxApertura", "Finalizado", "maxTrxApertura: "
					+ maxTrxApertura);
		}
		return maxTrxApertura;
	}

	@Override
	public boolean cierreCajaIntegrada(Parametros pcv, String fecha, int nroTrx,
			BigDecimal totalVenta, BigDecimal totalUventas, BigDecimal totalMontoReca,
			BigDecimal montoEfectivo, String fechaHora) throws AligareException {
		
		logger.initTrace("cierreCajaIntegrada", "Parametros: Parametros, String Fecha: "+ fecha + ", int nroTrx: "+nroTrx+ ", long totalVenta: "+totalVenta+", long totalUventas: "+totalUventas+
		", long totalMontoReca: "+totalMontoReca+", long montoEfectivo: "+montoEfectivo+", String fechaHora: "+fechaHora);
		
		boolean cajaCerrada = false;
		int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn != null) {
			String procSQL = "";
			try {
				if (funcion == Constantes.FUNCION_BOLETA){
//					procSQL = "{call cierre_caja_tot(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY HH24:MI:SS'))}";
					procSQL = "{call cierre_caja_tot_v2(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY HH24:MI:SS'))}";
					CallableStatement cs  = conn.prepareCall(procSQL);
					cs.setString(1, fecha);
					cs.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL")));
					cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
					cs.setInt(4, nroTrx);
					cs.setInt(5, nroTrx);
					cs.setInt(6, Integer.parseInt(pcv.buscaValorPorNombre("COD_TRX_CIERRE")));
					cs.setInt(7, Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR")));
					cs.setInt(8, Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR")));
					cs.setInt(9, Integer.parseInt(pcv.buscaValorPorNombre("ORIGEN")));
					cs.setBigDecimal(10, totalVenta);
					cs.setBigDecimal(11, totalUventas);
					cs.setBigDecimal(12, totalMontoReca);
					cs.setInt(13, Constantes.NRO_CERO);
					cs.setInt(14, Constantes.NRO_CERO);
					cs.setString(15, fechaHora);
					
					String sql = "{call cierre_caja_tot_v2(to_date('"+fecha+"','DD/MM/YYYY')"
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
		            		+ ","+nroTrx
		            		+ ","+nroTrx
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("COD_TRX_CIERRE"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("ORIGEN"))
		            		+ ","+totalVenta
		            		+ ","+totalUventas
		            		+ ","+totalMontoReca
		            		+ ","+Constantes.NRO_CERO
		            		+ ","+Constantes.NRO_CERO
		            		+ ",to_date("+fechaHora+",'DD/MM/YYYY HH24:MI:SS'))}";
					
		            logger.traceInfo("cierreCajaIntegrada", "--> Cierre caja cierre_caja_tot"+sql);
		            cs.execute();
					cajaCerrada=true;
					cs.close();
				/*	
				public boolean cierreCajaIntegrada(Parametros pcv, String fecha, int nroTrx, long totalVenta, long totalUventas, 
					long totalMontoReca,long montoEfectivo, String fechaHora)
				caja.cierreCajaIntegrada(pcv, fecha, nroTrx, tcierre.getTotalVenta(), tcierre.getwUnidades(), 
					tcierre.getTotalMontoReca(), tcierre.getMontoEfectivo()*Constantes.NRO_MENOSUNO, fechaHora);	
				*/
					
	        	//nuevo
				} else if (funcion == Constantes.FUNCION_NC) {
					totalMontoReca = totalMontoReca.multiply(Constantes.NRO_MENOSUNO_B);
					totalVenta = totalVenta.multiply(Constantes.NRO_MENOSUNO_B);
					BigDecimal totIngNetoCierre = (montoEfectivo.subtract(totalMontoReca));
//					procSQL = "{call cierre_caja_nc(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY HH24:MI:SS'))}";
					procSQL = "{call cierre_caja_tot_v2(to_date(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,to_date(?,'DD/MM/YYYY HH24:MI:SS'))}";
					CallableStatement cs  = conn.prepareCall(procSQL);
					
					cs.setString(1, fecha);
					cs.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL")));
					cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
					cs.setInt(4, nroTrx);
					cs.setInt(5, nroTrx);
					cs.setInt(6, Integer.parseInt(pcv.buscaValorPorNombre("COD_TRX_CIERRE")));
					cs.setInt(7, Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR")));
					cs.setInt(8, Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR")));
					cs.setInt(9, Integer.parseInt(pcv.buscaValorPorNombre("ORIGEN")));
					cs.setBigDecimal(10, totalVenta);
					cs.setBigDecimal(11, totalUventas);
					cs.setBigDecimal(12, totalMontoReca);
					cs.setBigDecimal(13, totalMontoReca);
					cs.setBigDecimal(14, totIngNetoCierre);
					cs.setString(15, fechaHora);
					
					String sql = "{call cierre_caja_tot_v2(to_date('"+fecha+"','DD/MM/YYYY')"
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("SUCURSAL"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
		            		+ ","+nroTrx
		            		+ ","+nroTrx
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("COD_TRX_CIERRE"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("RUT_VENDEDOR"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("RUT_SUPERVISOR"))
		            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("ORIGEN"))
		            		+ ","+totalVenta
		            		+ ","+totalUventas
		            		+ ","+totalMontoReca
		            		+ ","+totalMontoReca
		            		+ ","+totIngNetoCierre
		            		+ ",to_date("+fechaHora+",'DD/MM/YYYY HH24:MI:SS'))}";
					
					cs.execute();
		            logger.traceInfo("cierreCajaIntegrada", "--> Cierre caja cierre_caja_tot_v2"+sql);		            
		            cajaCerrada=true;
		            cs.close();
					
				}
			} catch (Exception e) {
				logger.traceError("cierreCajaIntegrada",
						"Error durante la ejecuci�n de cierreCajaIntegrada", e);
				logger.traceInfo("cierreCajaIntegrada", "SYSTEM EXIT ERROR: No se realiza cierre de caja correctamente");
				logger.endTrace("cierreCajaIntegrada", "Finalizado", "Boolean: " + cajaCerrada);
				System.exit(CodErrorSystemExit.APERTURA_CIERRE);
				return cajaCerrada;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("cierreCajaIntegrada",
					"Problemas de conexion a la base de datos");
		}
		logger.endTrace("cierreCajaIntegrada", "Finalizado", "Boolean: " + cajaCerrada);

		return cajaCerrada;
	}

	@Override
	public BigDecimal fcuentaPorTipoTVirtual(Parametros pcv, String fecha, int tipoFiltro, int maxTrxApertura, Long idAperturaCierre)throws AligareException {
		logger.initTrace("fcuentaPorTipoTVirtual", "Parametros: Parametros, String fecha: "+fecha+", int tipoFiltro: "+tipoFiltro+", int maxTrxApertura: "+maxTrxApertura, new KeyLog("Cuenta por tipo: ", ""+tipoFiltro));
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		BigDecimal totalContados = Constantes.NRO_CERO_B;
		if (conn != null) {
			String selectSQL = "";
			try {
				
				selectSQL = "select fcuenta_portipo_tvirtual(to_date(?,'YYYY-MM-DD'),?,?,?) from dual";
				if (tipoFiltro==3){
					selectSQL = "SELECT NVL(SUM(MONTO_PAGO),0) " + 
							"FROM NOTA_VENTA NV " + 
							"INNER JOIN TARJETA_RIPLEY TR ON(NV.CORRELATIVO_VENTA = TR.CORRELATIVO_VENTA) " + 
							"INNER JOIN XDETALLE_TRX_BO DET ON(TR.CORRELATIVO_VENTA = DET.CORRELATIVO_VENTA) " + 
							"WHERE NV.ESTADO= 2 " +
							"AND NV.NUMERO_CAJA = ? " + 
							"AND NV.NUMERO_SUCURSAL = ? " + 
							"AND NV.INDICADOR_MKP IN (0,2) " + 
							"AND DET.ID_APERTURA_CIERRE = ? " + 
							"AND TRIM(det.ESTADO_GEN_OC) NOT IN('" + Constantes.RM + "', '" + Constantes.ERROR + "', '" + Constantes.ERROR_MKP + "') ";
					
//					selectSQL = "SELECT COALESCE(SUM((art.PRECIO * art.UNIDADES) - art.MONTO_DESCUENTO), 0)   " + 
//							"FROM TARJETA_RIPLEY car   " + 
//							"JOIN NOTA_VENTA nv ON nv.CORRELATIVO_VENTA = car.CORRELATIVO_VENTA  " + 
//							"JOIN ARTICULO_VENTA art ON art.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
//							"JOIN IDENTIFICADOR_MARKETPLACE imkp ON imkp.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
//							"WHERE nv.ESTADO = 2   " + 
//							"AND nv.NUMERO_CAJA = ? " + 
//							"AND nv.NUMERO_SUCURSAL = ? " + 
//							"AND EXISTS( " + 
//							"	SELECT * " + 
//							"	FROM XDETALLE_TRX_BO " + 
//							"	WHERE CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
//							"	AND ID_APERTURA_CIERRE = ? " + 
//							") " + 
//							"AND imkp.ES_MKP = 0 ";
							
				}
				
				if (tipoFiltro==8){
					selectSQL = "SELECT COUNT(*)  " + 
							"FROM TARJETA_RIPLEY car  " + 
							"JOIN NOTA_VENTA nv ON nv.CORRELATIVO_VENTA = car.CORRELATIVO_VENTA " + 
							"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"WHERE nv.ESTADO = 2  " + 
							"AND nv.NUMERO_CAJA = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND det.ID_APERTURA_CIERRE = ? " +
							"AND TRIM(det.ESTADO_GEN_OC) NOT IN('" + Constantes.RM + "', '" + Constantes.ERROR + "', '" + Constantes.ERROR_MKP + "') ";
				}
				
				if (tipoFiltro==11){
					
					  selectSQL = "SELECT COALESCE(SUM(art.UNIDADES), 0)  " + 
					  		"FROM ARTICULO_VENTA art  " + 
					  		"JOIN NOTA_VENTA nv ON nv.CORRELATIVO_VENTA = art.CORRELATIVO_VENTA " + 
					  		"JOIN IDENTIFICADOR_MARKETPLACE imkp ON imkp.CORRELATIVO_VENTA = art.CORRELATIVO_VENTA " +
					  		"WHERE nv.ESTADO = 2 " + 
					  		"AND nv.NUMERO_CAJA = ? " + 
					  		"AND nv.NUMERO_SUCURSAL = ? " + 
					  		"AND EXISTS( " + 
							"	SELECT * " + 
							"	FROM XDETALLE_TRX_BO " + 
							"	WHERE CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"	AND ID_APERTURA_CIERRE = ? " + 
							") " + 
							"AND imkp.ES_MKP = 0 ";

					
					
					
			/*		selectSQL ="SELECT SUM (unidades) FROM trx_articulos "
							+ "WHERE nro_transaccion IN (SELECT nro_transaccion FROM trx_transaccion "
							+ "WHERE sucursal = ? "
							+ "AND nro_caja = ? "
							+ "AND nro_transaccion > ? "
							+ "AND tipo_trx IN (1, 2) "
							+ "AND estado = 0  AND FECHA_TRX = TO_DATE('" + fecha + "','DD/MM/YYYY')) " // Optimizacion JP 08-09-2017
							+ "AND sucursal = ? "
							+ "AND nro_caja = ? AND nro_transaccion > ? "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','DD/MM/YYYY')"; // Optimizacion JP 08-09-2017;*/
				}
				
				if (tipoFiltro==Constantes.NRO_QUINCE){
							
					selectSQL = "SELECT COALESCE(SUM(det.MONTO_PAGO), 0) " + 
							"FROM NOTA_VENTA nv " + 
							"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"WHERE nv.ESTADO = 2  "+
							"AND nv.NUMERO_CAJA = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND det.ID_APERTURA_CIERRE = ? " + 
							"AND TRIM(det.ESTADO_GEN_OC) NOT IN('" + Constantes.RM + "', '" + Constantes.ERROR + "', '" + Constantes.ERROR_MKP + "') ";
				}
				
				if (tipoFiltro==Constantes.NRO_DIECISEIS){
					
					selectSQL = "SELECT COALESCE(SUM(det.MONTO_PAGO), 0) " + 
							"FROM NOTA_VENTA nv " + 
							"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"WHERE nv.ESTADO in (3, 4) "+
							"AND nv.NUM_CAJA_NOTA_CREDITO = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND det.ID_APERTURA_CIERRE = ? " + 
							"AND TRIM(det.ESTADO_GEN_OC) = '" + Constantes.ANRM + "' ";
				}
				
				if (tipoFiltro==Constantes.NRO_DIECISIETE){
					
					selectSQL = "SELECT COUNT(*) " + 
							"FROM NOTA_VENTA nv " + 
							"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"WHERE nv.ESTADO in (3, 4) "+
							"AND nv.NUM_CAJA_NOTA_CREDITO = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND det.ID_APERTURA_CIERRE = ? " + 
							"AND TRIM(det.ESTADO_GEN_OC) = '" + Constantes.ANRM + "' ";
				}
				
				if (tipoFiltro==10){
					selectSQL = "SELECT COUNT(*)  " + 
							"FROM TARJETA_RIPLEY car  " + 
							"JOIN NOTA_VENTA nv ON nv.CORRELATIVO_VENTA = car.CORRELATIVO_VENTA " + 
							"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
							"WHERE nv.ESTADO in(3, 4) " + 
							"AND nv.NUMERO_CAJA = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND det.ID_APERTURA_CIERRE = ? " +
							"AND TRIM(det.ESTADO_GEN_OC) ? '" + Constantes.RM + "' ";
				}
				
				if (tipoFiltro==9){
					selectSQL ="select count(*) from trx_transaccion "
							+ "where  sucursal = ? "
							+ "and nro_caja = ? "
							+ "and nro_transaccion > ? "
							+ "and tipo_trx = 11 and estado = 0 "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','YYYY-MM-DD')"; // Optimizacion JP 08-09-2017;
				}
				
				if (tipoFiltro==4){
					selectSQL ="select sum(monto_trx) from trx_transaccion "
							+ "where  sucursal = ? "
							+ "and nro_caja = ? "
							+ "and nro_transaccion > ? "
							+ "and tipo_trx = 11 and estado = 0 "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','YYYY-MM-DD')"; // Optimizacion JP 08-09-2017;
					
				}
				if (tipoFiltro==6){
					selectSQL="select count(*) from trx_transaccion, trx_cod_tran "
							+ "where sucursal = ? "
							+ "and nro_caja = ? "
							+ "and nro_transaccion > ? "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','YYYY-MM-DD') " // Optimizacion JP 08-09-2017
							+ "and tipo_trx = cod_tran_ori "
							+ "and grupo_tran = 2 and estado = 1";
				}
				
				if (tipoFiltro==12){
					
					 selectSQL = "SELECT SUM (art.unidades) FROM trx_transaccion trx, trx_articulos art "
								+ "WHERE trx.fecha_trx = art.fecha_trx "
								+ "AND trx.sucursal = art.sucursal "
								+ "and trx.nro_caja = art.nro_caja "
								+ "and trx.nro_transaccion = art.nro_transaccion "
								+ "AND trx.sucursal = ? "
								+ "AND trx.nro_caja = ? "
								+ "AND trx.nro_transaccion > ? "
								+ "AND trx.tipo_trx IN (11, 15) "
								+ "AND trx.estado = 0 "
								+ "AND trx.FECHA_TRX = TO_DATE('" + fecha + "','YYYY-MM-DD') ";
					
					
					/*selectSQL="SELECT SUM (unidades) FROM trx_articulos WHERE nro_transaccion IN "
							+ "(SELECT nro_transaccion FROM trx_transaccion WHERE sucursal = ? "
							+ "AND nro_caja = ? "
							+ "AND nro_transaccion > ? "
							+ "AND tipo_trx IN (11, 15) AND estado = 0 AND FECHA_TRX = TO_DATE('" + fecha + "','DD/MM/YYYY')) "
							+ "AND sucursal = ? "
							+ "AND nro_caja = ? AND nro_transaccion > ? "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','DD/MM/YYYY')"; // Optimizacion JP 08-09-2017;*/
				}
				
				if (tipoFiltro==5){
					selectSQL="select sum(monto_trx) from trx_transaccion "
							+ "where sucursal = ? "
							+ "and nro_caja = ? "
							+ "and nro_transaccion > ? "
							+ "and tipo_trx = 15 and estado = 0 "
							+ " AND FECHA_TRX = TO_DATE('" + fecha + "','YYYY-MM-DD')"; // Optimizacion JP 08-09-2017;
				}
				
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				if (ps != null) {
					if (tipoFiltro==10 || tipoFiltro==12 || tipoFiltro==9 || tipoFiltro==4 || tipoFiltro==6 || tipoFiltro==5){
						ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(3, maxTrxApertura);
					}

					
					if (tipoFiltro==3 || tipoFiltro==8 ||  tipoFiltro==11 || tipoFiltro==Constantes.NRO_QUINCE
							|| tipoFiltro == Constantes.NRO_DIECISEIS || tipoFiltro == Constantes.NRO_DIECISIETE){
						ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setLong(3, idAperturaCierre);
					}

				/*
					if (tipoFiltro==11 || tipoFiltro==12){
						ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(3, maxTrxApertura);
						ps.setInt(4, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						ps.setInt(5, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						ps.setInt(6, maxTrxApertura);
					}					
					*/
					
					logger.traceInfo("fcuentaPorTipoTVirtual", "SQL:"+ selectSQL);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						if (rs.getInt(1) > 0) {
							totalContados= rs.getBigDecimal(1);
						}
						rs.close();
					}
					ps.close();
				}
				
			}catch(SQLException sqlEx){
				
				logger.traceError("fcuentaPorTipoTVirtual","Error SQL durante la ejecuci�n de fcuentaPorTipoTVirtual", sqlEx);
				logger.endTrace("fcuentaPorTipoTVirtual", "Finalizado", "Total: -1");
				return Constantes.NRO_MENOSUNO_B;
				
			}
				

			catch (Exception e) {
				logger.traceError("fcuentaPorTipoTVirtual","Error durante la ejecuci�n de fcuentaPorTipoTVirtual", e);
				logger.endTrace("fcuentaPorTipoTVirtual", "Finalizado", "Total: -1");
				return Constantes.NRO_MENOSUNO_B;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("fcuentaPorTipoTVirtual",
					"Problemas de conexion a la base de datos");
		}
		logger.endTrace("fcuentaPorTipoTVirtual", "Finalizado", "Total: " +totalContados);

		return totalContados;
	}

	@Override
	public TotalRecaudacion totalRecaudacionCierre(Parametros pcv, String fecha, int maxNroTrx)
			throws AligareException {
		logger.initTrace("totalRecaudacionCierre", "Parametros: Parametros, String Fecha: " + fecha+", int maxNroTrx: "+maxNroTrx);
		
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		TotalRecaudacion totalReca = new TotalRecaudacion();		
		int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));
		ResultSet rs = null;
		PreparedStatement ps;
		if (conn != null) {
			String selectSQL= "",selectDosSQL= "",selectTresSQL = "";			
			try {
				//selectSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION WHERE NRO_CAJA = 2 AND TRUNC(FECHA_TRX)= TO_DATE('20/07/2016','DD/MM/YYYY') AND SUCURSAL = 39 AND NRO_TRANSACCION > 12909 --AND TIPO_TRX = 27";
				selectSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
						+ "WHERE NRO_CAJA = ? "
						+ "AND SUCURSAL = ? AND NRO_TRANSACCION > ? "
						+ "AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
						+ "AND TIPO_TRX = 27"; //Recuadacion Seguros
				
				selectDosSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
						+ "WHERE NRO_CAJA = ? "
						+ "AND SUCURSAL = ? AND NRO_TRANSACCION > ? "
						+ "AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
						+ "AND TIPO_TRX = 37"; // NC Recuadacion Seguros

				
				selectTresSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
						+ "WHERE NRO_CAJA = ? "
						+ "AND SUCURSAL = ? AND NRO_TRANSACCION > ? "
						+ "AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
						+ "AND TIPO_TRX IN (15,11)"; //NC Credi - NC  Sin Financiamiento

				
				if (funcion==Constantes.FUNCION_BOLETA){
					ps = conn.prepareStatement(selectSQL);
					if (ps != null) {					
						 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
//						 ps.setString(2,fecha);
						 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						 ps.setInt(3,maxNroTrx);
						 
						rs = ps.executeQuery();
						
						selectSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
								+ "WHERE NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
								+ " AND SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
								+ " AND NRO_TRANSACCION > "+maxNroTrx + " "
								+ " AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
								+ " AND TIPO_TRX = 27";
						
						logger.traceInfo("totalRecaudacionCierre", "SQL Bol Recaudaci�n Seguros: "+selectSQL);
			

						if (rs != null && rs.next()) {
							totalReca.setCantidadRecaudacion(rs.getInt("CANTIDAD"));
							totalReca.setTotalRecaudacion(rs.getLong("MONTO_TRX"));
							rs.close();
						}
						ps.close();
					}
				}
				//MONTO DE LAS TRANSACCIONES DE ANULACI�N
				if (funcion==Constantes.FUNCION_NC){
					ps = conn.prepareStatement(selectDosSQL);
					if (ps != null) {					
						 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
//						 ps.setString(2,fecha);
						 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						 ps.setInt(3,maxNroTrx);
						 
						rs = ps.executeQuery();
						selectDosSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
								+ "WHERE NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
								+ " AND SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
								+ " AND NRO_TRANSACCION > "+ maxNroTrx + " "
								+ " AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
								+ " AND TIPO_TRX = 37";
						logger.traceInfo("totalRecaudacionCierre", "SQL NC Recuadacion Seguros: "+selectDosSQL);
						if (rs != null && rs.next()) {
							if (rs.getInt(1) > 0) {
								totalReca.setCantidadRecaudacion(rs.getInt("CANTIDAD"));
								totalReca.setTotalRecaudacion(rs.getLong("MONTO_TRX"));
							}
							rs.close(); 
						}
						ps.close();
					}
				
					//MONTO DE LAS TRANSACCIONES DE NOTA DE CREDITO
					ps = conn.prepareStatement(selectTresSQL);
					if (ps != null) {					
						 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
	//					 ps.setString(2,fecha);
						 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						 ps.setInt(3,maxNroTrx);					 
						rs = ps.executeQuery();
						selectTresSQL = "SELECT NVL(SUM(MONTO_TRX),0) MONTO_TRX, COUNT (*) CANTIDAD FROM TRX_TRANSACCION "
								+ "WHERE NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
								+ " AND SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
								+ " AND NRO_TRANSACCION > "+maxNroTrx + " "
								+ " AND FECHA_TRX = TO_DATE('"+ fecha +"','DD/MM/YYYY') "
								+ " AND TIPO_TRX IN (15,11)";
						
						logger.traceInfo("totalRecaudacionCierre", "SQL NC Credi - NC  Sin Financiamiento: "+selectTresSQL);
						if (rs != null && rs.next()) {
							if (rs.getInt(1) > 0) {
								totalReca.setTotalDTE(rs.getLong("MONTO_TRX"));
							}
							rs.close();
						}
						ps.close();
					}
				}
			} catch (Exception e) {
				logger.traceError("totalRecaudacionCierre",
						"Error durante la ejecuci�n de TotalRecaudacion", e);
				logger.endTrace("totalRecaudacionCierre", "Finalizado", "Resultado: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("totalRecaudacionCierre",
					"Problemas de conexion a la base de datos");
		}
		logger.endTrace("totalRecaudacionCierre", "Finalizado", "Resultado: " + totalReca.toString());
		return totalReca;
	}

	@Override
	public TotalVentasTRE totalVentasTRECierre(Parametros pcv, String fecha,
			int maxNroTrx, int nroTrx) throws AligareException {
		logger.initTrace("totalVentasTRECierre", "Parametros: Parammetros, String fecha: "+fecha+", int maxNroTrx: "+maxNroTrx+", int nroTrx: "+nroTrx,new KeyLog("fecha: ", fecha)
		,new KeyLog("maxNroTrx: ", ""+maxNroTrx));
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		TotalVentasTRE totalVentasTRE = null;
		if (conn!=null){
			String selectCountSQL = "", selectTRESQL= "" , selectTRECOMSQL = "";
		try {
			//Cuenta ventas TRE para BOLETAS y NC es la misma query		 
			selectCountSQL = "SELECT COUNT(*) CANTIDAD FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
					+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
					+ "AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
					+ "AND TRE.NRO_CAJA= ? AND TRE.SUCURSAL= ? AND TRX.TIPO_TRX=1 "
					+ "AND TRX.NRO_TRANSACCION > ? "
					+ "AND TRX.NRO_TRANSACCION < ? "
					+ "AND  TRX.ESTADO=0";
			
			//+ "AND TRUNC(TRE.FECHA_TRX) = TO_DATE(?,'DD/MM/YYYY') "
			ResultSet rs = null;
			PreparedStatement ps = conn.prepareStatement(selectCountSQL);
			totalVentasTRE = new TotalVentasTRE();
			if (ps != null) {
//				 ps.setString(1,fecha);
				 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				 ps.setInt(3,maxNroTrx);
				 ps.setInt(4, nroTrx);
				 				
				selectCountSQL = "SELECT COUNT(*) CANTIDAD FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
						+ " AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ " AND TRE.NRO_CAJA= "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
						+ " AND TRE.SUCURSAL= "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
						+ " AND TRX.TIPO_TRX=1"
						+ " AND TRX.NRO_TRANSACCION > "+maxNroTrx
						+ " AND TRX.NRO_TRANSACCION < "+nroTrx
						+ " AND TRX.ESTADO=0";
				
				logger.traceInfo("totalVentasTRECierre", "SQL Cantidad: "+selectCountSQL);
				rs = ps.executeQuery();
				if (rs != null && rs.next()) {
					if (rs.getInt(1) > 0) {
						totalVentasTRE.setCantidadVentasTRE(rs.getInt("CANTIDAD"));
						
					
					}
					rs.close();
				}
				ps.close();
			}
			//Total ventas TRE Y TRECOM para boletas y NC depende la funci�n
			int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));
			
			if (funcion==Constantes.FUNCION_BOLETA){
				
				selectTRESQL = "SELECT NVL(SUM(TRE.TXMTE_MNT_TAR),0) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION AND TRE.NRO_CAJA=TRX.NRO_CAJA "
						+ "AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= ? AND TRE.SUCURSAL= ? "
						+ "AND (TRX.TIPO_TRX=1 OR TRX.TIPO_TRX=2) "
						+ "AND TRX.NRO_TRANSACCION > ? "
						+ "AND TRX.NRO_TRANSACCION < ? "
						+ "AND  TRX.ESTADO=0";
				
				
				selectTRECOMSQL = "SELECT NVL(SUM(TRE.TXMTE_MNT_TAR),0) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
						+ "AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= ? AND TRE.SUCURSAL= ? AND (TRX.TIPO_TRX=2) "
						+ "AND TRX.NRO_TRANSACCION > ? "
						+ "AND TRX.NRO_TRANSACCION < ? "
						+ "AND  TRX.ESTADO=0";
				
				
			}else if(funcion==Constantes.FUNCION_NC){
				selectTRESQL = "SELECT NVL(SUM(TRE.TXMTE_MNT_TAR),0) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION AND TRE.NRO_CAJA=TRX.NRO_CAJA "
						+ "AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= ? AND TRE.SUCURSAL= ? "
						+ "AND (TRX.TIPO_TRX=11 OR TRX.TIPO_TRX=15) "
						+ "AND TRX.NRO_TRANSACCION > ? "
						+ "AND TRX.NRO_TRANSACCION < ? "
						+ "AND  TRX.ESTADO=0"; 
				
				selectTRECOMSQL = "SELECT NVL(SUM(TRE.TXMTE_MNT_TAR),0) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
						+ "AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= ? AND TRE.SUCURSAL= ? AND (TRX.TIPO_TRX=15) "
						+ "AND TRX.NRO_TRANSACCION > ? "
						+ "AND TRX.NRO_TRANSACCION < ? "
						+ "AND  TRX.ESTADO=0";				
			}
			ps = conn.prepareStatement(selectTRESQL);
			
			if (ps != null) {
//				 ps.setString(1,fecha);
				 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				 ps.setInt(3,maxNroTrx);
				 ps.setInt(4,nroTrx);
				 rs = ps.executeQuery();				
				logger.traceInfo("totalVentasTRECierre", "Ejecuci�n de query que TRE-OK!");
				if (rs != null && rs.next()) {
					if (rs.getInt(1) > 0) {
						totalVentasTRE.setwTotalVentasTRE(rs.getLong("SUMA"));
						
					}
					rs.close();
				}
				ps.close();
			}
			ps = conn.prepareStatement(selectTRECOMSQL);
			if (ps != null) {
//				 ps.setString(1,fecha);
				 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				 ps.setInt(3,maxNroTrx);
				 ps.setInt(4,nroTrx);
				 rs = ps.executeQuery();				
				logger.traceInfo("totalVentasTRECierre", "Ejecuci�n de query que TRECOM-OK!");
				if (rs != null && rs.next()) {
						totalVentasTRE.setwTotalVentasTRECOM(rs.getLong("SUMA"));
					rs.close();
				}
				ps.close();
			}
			//log
			if(funcion == Constantes.FUNCION_BOLETA){
				selectTRESQL = "SELECT SUM(TRE.TXMTE_MNT_TAR) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION AND TRE.NRO_CAJA=TRX.NRO_CAJA "
						+ "AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TRE.SUCURSAL= "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
						+ "AND (TRX.TIPO_TRX=1 OR TRX.TIPO_TRX=2) "
						+ "AND TRX.NRO_TRANSACCION > "+maxNroTrx+" "
						+ "AND TRX.NRO_TRANSACCION < "+nroTrx+" "
						+ "AND  TRX.ESTADO=0";
				logger.traceInfo("totalVentasTRECierre", "SQLTRE: "+selectTRESQL);
				
				
				selectTRECOMSQL = "SELECT SUM(TRE.TXMTE_MNT_TAR) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
						+ "AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TRE.SUCURSAL= "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+" AND (TRX.TIPO_TRX=2) "
						+ "AND TRX.NRO_TRANSACCION > "+maxNroTrx+" "
						+ "AND TRX.NRO_TRANSACCION < "+nroTrx+" "
						+ "AND  TRX.ESTADO=0";
				logger.traceInfo("totalVentasTRECierre", "SQLTRE: "+selectTRECOMSQL);
			}
			else{
				selectTRESQL = "SELECT SUM(TRE.TXMTE_MNT_TAR) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION AND TRE.NRO_CAJA=TRX.NRO_CAJA "
						+ "AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TRE.SUCURSAL= "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
						+ "AND (TRX.TIPO_TRX=11 OR TRX.TIPO_TRX=15) "
						+ "AND TRX.NRO_TRANSACCION > "+maxNroTrx+" "
						+ "AND TRX.NRO_TRANSACCION < "+nroTrx+" "
						+ "AND TRX.ESTADO=0";
				logger.traceInfo("totalVentasTRECierre", "SQLTRE: "+selectTRESQL);
				
				selectTRECOMSQL = "SELECT SUM(TRE.TXMTE_MNT_TAR) SUMA FROM BOBDTX_MOV_TAR_RGL_EMR TRE, TRX_TRANSACCION TRX "
						+ "WHERE TRE.FECHA_TRX = TRX.FECHA_TRX AND TRE.NRO_TRANSACCION = TRX.NRO_TRANSACCION "
						+ "AND TRE.NRO_CAJA=TRX.NRO_CAJA AND TRE.SUCURSAL=TRX.SUCURSAL "
						+ "AND TRE.NRO_CAJA= "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TRE.SUCURSAL= "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+" AND (TRX.TIPO_TRX=15) "
						+ "AND TRX.NRO_TRANSACCION > "+maxNroTrx+" "
						+ "AND TRX.NRO_TRANSACCION < "+nroTrx+" "
						+ "AND TRX.ESTADO=0";
				logger.traceInfo("totalVentasTRECierre", "SQLTRE: "+selectTRECOMSQL);
			}

		} catch (Exception e) {
			logger.traceError("totalVentasTRECierre", "Error durante la ejecuci�n de totalVentasTRECierre", e);
			logger.endTrace("totalVentasTRECierre", "Finalizado", "totalVentasTRE: null");
			return null;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("totalVentasTRECierre", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("totalVentasTRECierre", "Finalizado", "totalVentasTRE: " + totalVentasTRE.toString());
		return totalVentasTRE;
	}

	@Override
	//Cuenta y Suma Boletas sin TRE LblVentasBancarias/w_ventasbanc(suma), LblBancarias(count)
	public TotalVentasBanc totalVentasBancCierre(Parametros pcv, String fecha,int maxNroTrx, int nroTrx, Long idAperturaCierre) {
		logger.initTrace("totalVentasBancCierre", "Parametros: Parametros, String fecha: "+fecha+", int maxNroTrx: "+maxNroTrx+", int nroTrx: "+nroTrx, new KeyLog("fecha: ", fecha)
		,new KeyLog("maxNroTrx: ", ""+maxNroTrx),new KeyLog("nroTrx: ", ""+nroTrx));
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		TotalVentasBanc totalVentasBanc = null;
		int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));	
		if (conn!=null){
		String selectSQL = "";
		try {
			totalVentasBanc = new TotalVentasBanc();
			if (funcion==Constantes.FUNCION_BOLETA){
				selectSQL = "SELECT COALESCE(SUM(MONTO_PAGO), 0) MONTO_TRX, COUNT(*) CANTIDAD " + 
							"FROM MEDIO_PAGO MP " + 
							"JOIN NOTA_VENTA NV ON (NV.CORRELATIVO_VENTA = MP.CORRELATIVO_VENTA) " + 
							"JOIN XDETALLE_TRX_BO DET ON (DET.CORRELATIVO_VENTA = NV.CORRELATIVO_VENTA) " + 
							"WHERE NV.ESTADO = 2 " + 
							"AND nv.NUMERO_CAJA = ? " + 
							"AND nv.NUMERO_SUCURSAL = ? " + 
							"AND MP.ID_FORMA_PAGO NOT IN (2,5) " + 
							"AND det.ID_APERTURA_CIERRE = ? " + 
							"AND TRIM(det.ESTADO_GEN_OC) NOT IN('" + Constantes.RM + "', '" + Constantes.ERROR + "', '" + Constantes.ERROR_MKP + "') ";
				
//				selectSQL = "SELECT COALESCE(SUM(MONTO), 0) MONTO_TRX, COUNT(*) CANTIDAD " + 
//						"FROM MEDIO_PAGO mp " + 
//						"JOIN NOTA_VENTA nv ON nv.CORRELATIVO_VENTA = mp.CORRELATIVO_VENTA " + 
//						"JOIN XDETALLE_TRX_BO det ON det.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
//						"WHERE nv.ESTADO = 2 " + 
//						"AND mp.TIPO_TARJETA IN('C', 'D') " + 
//						"AND nv.NUMERO_CAJA = ? " + 
//						"AND nv.NUMERO_SUCURSAL = ? " + 
//						"AND det.ID_APERTURA_CIERRE = ? " +
//						"AND TRIM(det.ESTADO_GEN_OC) <> 'RM' ";
				//se quita filtro
				//TRUNC(TRX.FECHA_TRX)= TO_DATE(?,'DD/MM/YYYY') "
				//+ "AND  
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				if (ps != null) {
					 ps.setInt(1,Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
					 ps.setInt(2,Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
					 ps.setLong(3, idAperturaCierre);
					 
					rs = ps.executeQuery();
					logger.traceInfo("totalVentasBancCierre", "Ejecución de query OK!");
					
					if (rs != null && rs.next()) {
							totalVentasBanc.setCantidadVentasBanc(rs.getBigDecimal("CANTIDAD"));
							totalVentasBanc.setTotalVentasBanc(rs.getBigDecimal("MONTO_TRX"));
						rs.close();
					}
					ps.close();
				}
				
//				selectSQL = "SELECT COALESCE(SUM(MONTO), 0) MONTO_TRX, COUNT(*) CANTIDAD " + 
//						"FROM MEDIO_PAGO mp " + 
//						"JOIN NOTA_VENTA nv ON mp.CORRELATIVO_VENTA = nv.CORRELATIVO_VENTA " + 
//						"WHERE nv.ESTADO = 2 " + 
//						"AND mp.TIPO_TARJETA IN('C', 'D') " +
//						"AND nv.NUMERO_CAJA = " +  pcv.buscaValorPorNombre("nro_caja") +
//						"AND nv.NUMERO_SUCURSAL = " + pcv.buscaValorPorNombre("sucursal") +
//						"AND TO_CHAR(nv.FECHA_CREACION, 'YYYY-MM-DD') = '" + fecha + "'";
				
				logger.traceInfo("totalVentasBancCierre", "SQL: "+selectSQL);
				
				
			} 
		} catch (Exception e) {
			logger.traceError("totalVentasBancCierre", "Error durante la ejecuci�n de totalVentasBancCierre", e);
			logger.endTrace("totalVentasBancCierre", "Finalizado", "totalVentasBanc: null");
			return null;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("totalVentasBancCierre", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("totalVentasBancCierre", "Finalizado", "totalVentasBanc: "+ totalVentasBanc.toString());
		return totalVentasBanc;
	}

	@Override
	public FMaxTVirtualTipo fmaxVirtualTipo(Parametros pcv, String fecha)
			throws AligareException {
		logger.initTrace("fmaxVirtualTipo", "Parametros: Parametros, String fecha: "+fecha, new KeyLog("fecha: ", fecha));
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		FMaxTVirtualTipo fmaxTVirtual = null;
		if (conn!=null){
		String selectMaxFechaSQL = "", selectMaxTrxSQL="";
		try {
				selectMaxFechaSQL = "SELECT FMAX_FECHA_TVIRTUAL_TIPO(?,TO_DATE(?,'DD/MM/YYYY'),?,42) FROM DUAL";
				selectMaxTrxSQL   = "SELECT FMAX_TRX_TVIRTUAL_TIPO(?,TO_DATE(?,'DD/MM/YYYY'),?,42) FROM DUAL";
			
			ResultSet rs = null;
			PreparedStatement ps = conn.prepareStatement(selectMaxFechaSQL);
			fmaxTVirtual = new FMaxTVirtualTipo();
			if (ps != null) {
				ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				ps.setString(2, fecha);
				ps.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				rs = ps.executeQuery();
				logger.traceInfo("fmaxVirtualTipo", "Ejecuci�n de FMAX_FECHA_TVIRTUAL_TIPO OK!");
				if (rs != null && rs.next()) {
					if (rs.getDate(1)!=null)
						fmaxTVirtual.setMaxFecha(rs.getDate(1).toString());
					else
						fmaxTVirtual.setMaxFecha("01/01/1900");
					rs.close();
				}
				ps.close();
			}
			selectMaxFechaSQL = "SELECT FMAX_FECHA_TVIRTUAL_TIPO("+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
					+",TO_DATE('"+fecha+"','DD/MM/YYYY'),"+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
					+ ",42) FROM DUAL";
			logger.traceInfo("fmaxVirtualTipo", "FECHA MAX SQL:"+selectMaxFechaSQL);

			ps = conn.prepareStatement(selectMaxTrxSQL);
			if (ps != null) {
				ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				ps.setString(2, fecha);
				ps.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				rs = ps.executeQuery();
				logger.traceInfo("fmaxVirtualTipo", "Ejecuci�n de FMAX_TRX_TVIRTUAL_TIPO OK!");
				if (rs != null && rs.next()) {
					if(rs.getInt(1)>0)
						fmaxTVirtual.setMaxNroTrx(rs.getInt(1));
					rs.close();
				}
				ps.close();
			}
			selectMaxTrxSQL   = "SELECT FMAX_TRX_TVIRTUAL_TIPO("+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
					+ ",TO_DATE('"+fecha+"','DD/MM/YYYY'),"+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
					+ ",42) FROM DUAL";

		} catch (Exception e) {
			logger.traceError("fmaxVirtualTipo", "Error durante la ejecuci�n de fmaxVirtualTipo", e);
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("fmaxVirtualTipo", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("fmaxVirtualTipo", "Finalizado", "fmaxTVirtual: " + fmaxTVirtual.toString());
		return fmaxTVirtual;
	}

	@Override
	public TotalU totalUCierre(Parametros pcv, String fecha, int maxNroTrx)
			throws AligareException {
		logger.initTrace("totalUCierre", "Parametros: Parametros, String fecha: "+fecha+", int maxNroTrx: "+maxNroTrx, new KeyLog("fecha: ", fecha), new KeyLog("maxNroTrx: ",""+maxNroTrx));
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		TotalU totalU = null;
		if (conn!=null){
		try {
			
			totalU = new TotalU();
			
			String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBTIENE_TOTALES_UCIERRE (?,?,?,?,?,?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(procSQL);
			cs.setString(1, fecha);
			cs.setInt(2, maxNroTrx);
			cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.execute();
			totalU.setTotalUCar(cs.getBigDecimal(4));
			totalU.setTotalUTre(cs.getBigDecimal(5));
			totalU.setTotalUBanc(cs.getBigDecimal(6));
            cs.close();
            procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBTIENE_TOTALES_UCIERRE "
            		+ "('"+fecha+"'"
            		+ ","+maxNroTrx
            		+ ","+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
            		+ ")}";
            logger.traceInfo("totalUCierre", "PkgEjecutado: " + procSQL);
		} catch (Exception e) {
			logger.traceError("totalUCierre", "Error durante la ejecuci�n de totalUCierre", e);
			logger.endTrace("totalUCierre", "Finalizado", "TotalU: null");
			return null;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("totalUCierre", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("totalUCierre", "Finalizado", "TotalU: " + totalU.toString());
		return totalU;
	}
	
	@Override
	public OrdenesImpresas obtieneOrdentesImpresas(Parametros pcv, String fecha, int maxNroTrx, int nroTrx)
			throws AligareException {
		logger.initTrace("obtieneOrdentesImpresas", "Parametros: Parametros, String fecha: "+fecha+", int maxNroTrx: "+maxNroTrx+", int nroTrx: "+nroTrx, new KeyLog("fecha: ", fecha), new KeyLog("maxNroTrx: ", ""+maxNroTrx)
		, new KeyLog("nroTrx: ", ""+nroTrx));
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		OrdenesImpresas oi = null;
		if (conn!=null){
		try {
			
			oi = new OrdenesImpresas();
			
			String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_OBTIENE_ORDENES_IMPRESAS (?,?,?,?,?,?,?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(procSQL);
			cs.setInt(1, maxNroTrx);
			cs.setString(2, fecha);
			cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
			cs.setInt(4, nroTrx);
			cs.setInt(5, Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")));
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			cs.registerOutParameter(10, OracleTypes.VARCHAR);
			cs.execute();
			oi.setMontoEfectivo(cs.getBigDecimal(6));
			oi.setCantidadEfectivo(cs.getBigDecimal(7));
			logger.traceInfo("obtieneOrdentesImpresas", "\nSQL: "+cs.getString(10)+";");
			
            cs.close();

		} catch (Exception e) {
			logger.traceError("obtieneOrdentesImpresas", "Error durante la ejecuci�n de obtieneOrdentesImpresas", e);
			logger.endTrace("obtieneOrdentesImpresas", "Finalizado", "OrdenesImpresas: null");
			return null;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("obtieneOrdentesImpresas", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("obtieneOrdentesImpresas", "Finalizado", "OrdenesImpresas: " + oi.toString());
		return oi;
	}
	
	@Override
	public TotalesCierre totales(Parametros pcv, String fecha, int nroTrx, Long idAperturaCierre) throws AligareException {
		
		logger.initTrace("totales", "Parametros: Parametros, String fecha: "+fecha+", int nroTrx: "+nroTrx, new KeyLog("fecha: ", fecha), new KeyLog("nroTrx: ", ""+nroTrx));
		TotalesCierre tcierre = null;
		try {
			int maxNroTrx = maximaTrxApertura(pcv, fecha, idAperturaCierre);
			
//			FMaxTVirtualTipo fmax  = fmaxVirtualTipo(pcv, fecha);
			OrdenesImpresas oImpresas = obtieneOrdentesImpresas(pcv, fecha, maxNroTrx, nroTrx);//listo
			TotalVentasBanc tVentasBanc = totalVentasBancCierre(pcv, fecha, maxNroTrx, nroTrx, idAperturaCierre);//listo
//			TotalRecaudacion tReca = totalRecaudacionCierre(pcv, fecha, maxNroTrx);//listo
//			TotalVentasTRE tVentasTRE = totalVentasTRECierre(pcv, fecha, maxNroTrx,nroTrx);//listo
		
			TotalU totalU = null;
			int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));
			BigDecimal totalVentas = Constantes.NRO_CERO_B, totalCantVentas = Constantes.NRO_CERO_B, totalUVentas = Constantes.NRO_CERO_B;
			BigDecimal wUnidades = Constantes.NRO_CERO_B;		
				
			BigDecimal wVentasCar = Constantes.NRO_CERO_B;

			if (funcion==Constantes.FUNCION_BOLETA){

				
				
				BigDecimal montoEfectivo = oImpresas.getMontoEfectivo();
				BigDecimal cantidadEfectivo = oImpresas.getCantidadEfectivo();
				BigDecimal totalMontoReca = fcuentaPorTipoTVirtual(pcv, fecha, Constantes.NRO_QUINCE,maxNroTrx, idAperturaCierre); //tReca.getTotalRecaudacion();
				
				BigDecimal cantidadCar = fcuentaPorTipoTVirtual(pcv, fecha, 8,maxNroTrx, idAperturaCierre); //tipo 8 = venta car cantidad de trx
				BigDecimal cantidadBanc = tVentasBanc.getCantidadVentasBanc(); 
				BigDecimal cantidadTre = Constantes.NRO_CERO_B; //tVentasTRE.getCantidadVentasTRE();
				
				wUnidades = fcuentaPorTipoTVirtual(pcv, fecha, 11, maxNroTrx, idAperturaCierre); //tipo 11 = Unidades trx boletas count(1)
				wVentasCar = fcuentaPorTipoTVirtual(pcv, fecha, 3, maxNroTrx, idAperturaCierre); // - tVentasTRE.getwTotalVentasTRECOM();//tipo = 3 Venta car sum(monto_trx) (tipotrx=2)
				totalVentas = wVentasCar.add(tVentasBanc.getTotalVentasBanc()).add(montoEfectivo); // + tVentasTRE.getwTotalVentasTRE();
				totalCantVentas = cantidadCar.add(cantidadBanc).add(cantidadEfectivo); // + cantidadTre;
				
				
				tcierre = new TotalesCierre();
				tcierre.setTotalMontoCAR(wVentasCar);
				tcierre.setTotalMontoDTE(tVentasBanc.getTotalVentasBanc());
//				tcierre.setTotalMontoTRE(tVentasTRE.getwTotalVentasTRE());
				tcierre.setTotalMontoTRE(Constantes.NRO_CERO_B);
				tcierre.setTotalCantCAR(cantidadCar);
				tcierre.setTotalCantDTE(cantidadBanc);
				tcierre.setTotalCantTRE(cantidadTre);
				
				tcierre.setTotalCantVentas(totalCantVentas);
				tcierre.setTotalDTE(totalVentas); //se repite en total venta... 
				tcierre.setTotalVenta(totalVentas);
				tcierre.setwUnidades(wUnidades);
				tcierre.setMontoEfectivo(montoEfectivo);
				tcierre.setCantidadEfectivo(cantidadEfectivo);
				tcierre.setTotalMontoReca(totalMontoReca);
				tcierre.setTotalU(totalUVentas);
				tcierre.setVentasCar(wVentasCar);
				
			} else if (funcion==Constantes.FUNCION_NC){
				
				BigDecimal montoEfectivo = oImpresas.getMontoEfectivo();
				BigDecimal cantidadEfectivo = oImpresas.getCantidadEfectivo();
				BigDecimal totalMontoReca = fcuentaPorTipoTVirtual(pcv, fecha, Constantes.NRO_DIECISEIS,maxNroTrx, idAperturaCierre);; //tReca.getTotalRecaudacion();
				
				
								
//				int maxNroTrxME = fmax.getMaxNroTrx();//en pruebas devuelve los mismos valores que maximaTrxApertura..se cambia para usar filtro de fechas
//				BigDecimal cantidadCar = BigDecimal.ZERO;//fcuentaPorTipoTVirtual(pcv, fecha, 10,maxNroTrx, idAperturaCierre); //tipo 10 = NC credito cantidad de trx
//				BigDecimal cantidadBanc = BigDecimal.ZERO;//fcuentaPorTipoTVirtual(pcv, fecha, 9, maxNroTrx, idAperturaCierre); //tiipo 9 = NC contado cantidad de trx
//				BigDecimal cantidadTre = Constantes.NRO_CERO_B; //tVentasTRE.getCantidadVentasTRE();
//				BigDecimal wVentasBanc = BigDecimal.ZERO;//fcuentaPorTipoTVirtual(pcv, fecha, 4, maxNroTrx, idAperturaCierre);//tipo = 4 NC contado
				BigDecimal nulas = fcuentaPorTipoTVirtual(pcv, fecha, Constantes.NRO_DIECISIETE, maxNroTrx, idAperturaCierre);//tipo = 6 NC anuladas
				
				totalU = totalUCierre(pcv, fecha, maxNroTrx);
				wUnidades = nulas;//fcuentaPorTipoTVirtual(pcv, fecha, 12,maxNroTrx, idAperturaCierre);//tipo = 12 Unidades trx N/C
				wUnidades =wUnidades.multiply(Constantes.NRO_MENOSUNO_B);
//				wVentasCar = (fcuentaPorTipoTVirtual(pcv, fecha, 5, maxNroTrx)- tVentasTRE.getwTotalVentasTRECOM());//tipo = 5 NC credito
				wVentasCar = BigDecimal.ZERO;//(fcuentaPorTipoTVirtual(pcv, fecha, 5, maxNroTrx, idAperturaCierre));
				totalVentas = BigDecimal.ZERO;//wVentasCar.add(wVentasBanc).add(montoEfectivo);
				totalCantVentas = BigDecimal.ZERO;//cantidadBanc.add(cantidadCar).add(cantidadTre).add(cantidadEfectivo);
				totalUVentas = BigDecimal.ZERO;//totalU.getTotalUBanc().add(totalU.getTotalUCar()).add(totalU.getTotalUTre()).multiply(Constantes.NRO_MENOSUNO_B);
				
				tcierre = new TotalesCierre();
				tcierre.setNcAnuladas(nulas);
				tcierre.setTotalCantVentas(totalCantVentas);
				tcierre.setTotalDTE(totalVentas); //se repite en total venta... 
				tcierre.setTotalVenta(totalVentas);
				tcierre.setwUnidades(wUnidades);
				tcierre.setMontoEfectivo(montoEfectivo);
				tcierre.setCantidadEfectivo(cantidadEfectivo);
				tcierre.setTotalMontoReca(totalMontoReca);
				tcierre.setTotalU(totalUVentas);
				tcierre.setVentasCar(wVentasCar);
				
			}
			
			
			
			
		} catch (Exception e) {
			logger.traceError("totales", "Error durante la ejecuci�n de totales", e);
			logger.endTrace("Totatales", "Finalizado", "totales cierre: null");
			return null;
		}
		logger.endTrace("Totatales", "Finalizado", "totales cierre: " + tcierre.toString());
		return tcierre;
	}

	@Override
	public boolean existeCaja(Parametros pcv, String fecha, int nroTrx)
			throws AligareException {
		logger.initTrace("existeCaja", "Parametros: Parametros, String fecha: "+fecha+", int nroTrx: "+nroTrx, new KeyLog("fecha: ", ""+fecha), new KeyLog("nroTrx: ", ""+nroTrx));
		boolean existeCaja = false;
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn!=null){
		String selectSQL = "";
		try {
			selectSQL = "SELECT COUNT(1) FROM TRX_CAJAS WHERE COD_SUCURSAL=? AND NRO_CAJA=? AND ESTADO = 1";
			ResultSet rs = null;
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			if (ps != null) {
				ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				rs = ps.executeQuery();
				logger.traceInfo("existeCaja", "Ejecuci�n de query OK!");
				if (rs != null && rs.next()) {
					if (rs.getInt(1) > 0) {
						existeCaja = true;
					}
					else{
						existeCaja = false;
						rs.close();
						ps.close();
						logger.traceInfo("existeCaja", "ERROR: La caja no est� creada en tabla TRX_CAJAS o se encuentra Inactiva");
						PoolBDs.closeConnection(conn);	
						System.exit(CodErrorSystemExit.CAJA_NO_DISPONIBLE);
					}
					rs.close();
				}
				ps.close();
				selectSQL = "SELECT COUNT(1) FROM TRX_CAJAS WHERE COD_SUCURSAL="+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
						+ " AND NRO_CAJA="+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
						+ " AND ESTADO = 1";
			}
			selectSQL = "SELECT COUNT(1) FROM TVIRTUAL_CAJA WHERE COMERCIO = ? AND SUCURSAL = ? AND FECHA = TO_DATE(?,'DD/MM/YYYY') AND NRO_CAJA = ?";
			rs = null;
			ps = conn.prepareStatement(selectSQL);
			if (ps!=null && existeCaja){
				ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("comercio")));
				ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				ps.setString(3, fecha);
				ps.setInt(4, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				rs = ps.executeQuery();
				logger.traceInfo("existeCaja", "Ejecuci�n de query OK!");
				selectSQL = "SELECT COUNT(1) FROM TVIRTUAL_CAJA WHERE COMERCIO ="+Integer.parseInt(pcv.buscaValorPorNombre("comercio"))
						+ "AND SUCURSAL ="+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
						+ " AND FECHA = TO_DATE('"+fecha+"','DD/MM/YYYY') "
						+ " AND NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"));
				
				logger.traceInfo("existeCaja", "SQL:"+selectSQL);
				if (rs != null && rs.next()) {
					if (rs.getInt(1) > 0) {
						existeCaja = true;						
					}
					else{
						existeCaja = false;
						//"INSERT INTO TVIRTUAL_CAJA VALUES (COMERCIO,SUCURSAL,FECHA,NRO_CAJA,1,RUT_OPER,RUT_SUP,TXT_NRO_BOLETA.TEXT,LB_NRO_TRANSACCION.CAPTION)"
						String insertSQL ="INSERT INTO TVIRTUAL_CAJA VALUES (?,?,TO_DATE(?,'DD/MM/YYYY'),?,1,?,?,?,?)";
						PreparedStatement psi = conn.prepareStatement(insertSQL);						
						psi.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("comercio")));
						psi.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
						psi.setString(3, fecha);
						psi.setInt(4, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
						psi.setString(5, pcv.buscaValorPorNombre("rut_vendedor"));
						psi.setString(6, pcv.buscaValorPorNombre("rut_supervisor"));
						psi.setInt(7, Constantes.NRO_CERO);
						psi.setInt(8,nroTrx);
						insertSQL ="INSERT INTO TVIRTUAL_CAJA VALUES ("+Integer.parseInt(pcv.buscaValorPorNombre("comercio"))
								+ ","+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))
								+ ",TO_DATE('"+fecha+"','DD/MM/YYYY')"
								+ ","+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))
								+ ",1"
								+ ",'"+pcv.buscaValorPorNombre("rut_vendedor")+"'"
								+ ",'"+pcv.buscaValorPorNombre("rut_supervisor")+"'"
								+ ","+Constantes.NRO_CERO
								+ ","+nroTrx
								+ ")";
						
						logger.traceInfo("existeCaja", "SQL:"+insertSQL);
						if (psi.executeUpdate()>0) {
							existeCaja=true;
						}
						creaCajaTvirtualBoletas(pcv);
						
						
					}
					rs.close();		
					
				}
				ps.close();
			}

		} catch (Exception e) {
			logger.traceError("existeCaja", "Error durante la ejecuci�n de existeCaja", e);
			logger.endTrace("existeCaja", "Finalizado", "Existe caja Nro: false");
			return false;
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("existeCaja", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("existeCaja", "Finalizado", "Existe caja Nro: "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+"?"+ existeCaja);
		
		return existeCaja;
	}

	@Override
	public boolean actualizaSaldoVolumen(Parametros pcv, String fechaHora,boolean esOC) throws AligareException {
		logger.initTrace("actualizaSaldoVolumen","Parametros: Parametros, String fechaHora: "+fechaHora+", boolean esOC:"+esOC, new KeyLog("fechaHora: ",fechaHora));
		boolean seActualizaCAJA = false;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		int saldo=0, flag=0;
		String sql;
		flag = esOC?1:0; //si se debe actualizar el saldo. flag==1
		
		if (conn!=null){
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_ACTUALIZA_VOLUMEN (?,?,?,?,?,?,?,?,?,?)}";
				CallableStatement cs = conn.prepareCall(procSQL);
				
				cs.setString(1, fechaHora);
				cs.setInt(2,flag);
				cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
				cs.setInt(4, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
				cs.registerOutParameter(5, OracleTypes.NUMBER); //3
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.registerOutParameter(7, OracleTypes.VARCHAR);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				cs.registerOutParameter(9, OracleTypes.NUMBER);
				cs.registerOutParameter(10, OracleTypes.VARCHAR);
				sql = "{CALL CAVIRA_MANEJO_CAJA.PROC_ACTUALIZA_VOLUMEN ('"+fechaHora+"',"
						+ flag +","
						+ Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+","
						+ Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+","
						+ "OUT_nSALDO,"
						+ "OUT_vAPERTURA,"
						+ "OUT_vCIERRE,"
						+ "OUT_vQRY,"
						+ "OUT_nCOD,"
						+ "OUT_vMJE)}";
				
				logger.traceInfo("actualizaSaldoVolumen", "PL/SQL: " +sql);
				cs.execute();
				saldo = cs.getInt(5);
				if (saldo >= Constantes.NRO_CERO){
					seActualizaCAJA = true;
					logger.traceInfo("actualizaSaldoVolumen", "Saldo Volumen: "+ saldo);
				}else if(saldo == Constantes.NRO_MENOSDOS || saldo == Constantes.NRO_MENOSTRES ){
					logger.traceInfo("actualizaSaldoVolumen", "Cierre/Apertura por hora");
					seActualizaCAJA = aperturaCierreAutomatico(pcv, Constantes.CIERRE_POR_HORARIO);
				}else if(saldo == Constantes.NRO_MENOSUNO){
					logger.traceInfo("actualizaSaldoVolumen", "Cierre/Apertura por volumen");
					seActualizaCAJA = aperturaCierreAutomatico(pcv, Constantes.CIERRE_POR_VOLUMEN);
				}			
	            cs.close();	
			} catch (Exception e) {
				logger.traceError("actualizaSaldoVolumen", "Error durante la ejecuci�n de actualizaSaldoVolumen", e);
				seActualizaCAJA = true;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		} else{
			logger.traceInfo("actualizaSaldoVolumen", "Problemas de conexion a la base de datos");
			seActualizaCAJA = true;
		}
		logger.endTrace("actualizaSaldoVolumen", "Finalizado", "[True: Cuando se actualiza y/o informa Saldo][False:Cuando se solicita un cierre y apertura] " + seActualizaCAJA);
		return seActualizaCAJA;
	}

	@Override
	public boolean aperturaCierreAutomatico(Parametros pcv, int tipoCierre)
			throws AligareException {
		logger.initTrace("aperturaCierreAutomatico", "Parametros: Parametros, int tipoCierre: "+tipoCierre, new KeyLog("tipoCierre: ",""+tipoCierre));
		boolean resultado = true;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
		String procSQL = "";
		try {
			CallableStatement cs = null;
			procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_PROGRAMA_DE_CIERRE(?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, tipoCierre);
			
			Integer numCaja = Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"));
			Integer numSuc = Integer.parseInt(pcv.buscaValorPorNombre("sucursal"));
			
			cs.setInt(2, numCaja);
			cs.setInt(3, numSuc);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);	
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.NUMBER);//4
			cs.registerOutParameter(7, OracleTypes.VARCHAR);//5
			cs.registerOutParameter(8, OracleTypes.VARCHAR);
			
			procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_PROGRAMA_DE_CIERRE("+tipoCierre+", " + numCaja + ", " + numSuc + ",outV,outN,outN,outV,outV)}";
			logger.traceInfo("aperturaCierreAutomatico", "PL/SQL: "+procSQL);
			cs.execute();  
			logger.traceInfo("aperturaCierreAutomatico", "Estado de ejecución CAVIRA_MANEJO_CAJA.PROC_PROGRAMA_DE_CIERRE\n");
			logger.traceInfo("aperturaCierreAutomatico", "Respuesta de PKG Valor: "+cs.getInt(6));
			logger.traceInfo("aperturaCierreAutomatico", "Respuesta de PKG dice: "+cs.getString(7));
			
			
			
			if(cs.getInt(6) == 0 && !cs.getString(7).equalsIgnoreCase(Constantes.SIN_ERRORES)){
				resultado = false;
			}
			cs.close();
		} catch (Exception e) {
			logger.traceError("aperturaCierreAutomatico", "Error durante la ejecuci�n de aperturaCierreAutomatico", e);
			
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("aperturaCierreAutomatico", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("aperturaCierreAutomatico", "Finalizado", null);
		return resultado;
	}
	
public void generaArchivoDeCierre(Parametros pcv,Long nroIdApertura, String ruta) throws AligareException {
	
		logger.initTrace("generaArchivoDeCierre", "Parametros: Parametros, int nroIdApertura: "+nroIdApertura, new KeyLog("nroIdApertura: ", ""+nroIdApertura));
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet("Resumen");
        HSSFRow fila = hoja.createRow(0);
//        HSSFCell celda = fila.createCell(0);
//        HSSFRichTextString texto = new HSSFRichTextString("Prueba de archivo");
//        celda.setCellValue(texto);
        FileOutputStream elFichero =null;
        String titulo = "";
        BigDecimal sumaError=Constantes.NRO_CERO_B,sumaMail=Constantes.NRO_CERO_B,sumaImp=Constantes.NRO_CERO_B,sumaMonto=Constantes.NRO_CERO_B,sumaOC=Constantes.NRO_CERO_B;
        BigDecimal sumaBol=Constantes.NRO_CERO_B, sumaRR=Constantes.NRO_CERO_B, sumaRM=Constantes.NRO_CERO_B, sumaFac=Constantes.NRO_CERO_B;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
			int funcion = Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"));
		if (funcion==Constantes.FUNCION_BOLETA)	{
			
			   titulo= "Ventas y Recaudaciones";
			   logger.traceInfo("generaArchivoDeCierre", "Boleta: Generaci�n documento de boleta");
		}
		else{
		       titulo = "Notas de Credito y Anulaciones";
		       logger.traceInfo("generaArchivoDeCierre", "NC: Generaci�n documento de boleta");
			
		}
		
		String procSQL ="{CALL CAVIRA_MANEJO_CAJA.PROC_INFORME_DE_CIERRE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		String sql = "";
		CallableStatement cs = null;
		try {
			ResultSet rs = null;
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, nroIdApertura);
			cs.setInt(2, funcion);
			cs.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA")));
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.NUMBER);
			cs.registerOutParameter(8, OracleTypes.CURSOR);
			cs.registerOutParameter(9, OracleTypes.NUMBER);
			cs.registerOutParameter(10, OracleTypes.NUMBER);
			cs.registerOutParameter(11, OracleTypes.NUMBER);
			cs.registerOutParameter(12, OracleTypes.NUMBER);
			cs.registerOutParameter(13, OracleTypes.NUMBER);
			cs.registerOutParameter(14, OracleTypes.VARCHAR);
			cs.registerOutParameter(15, OracleTypes.VARCHAR);
			
			sql = "{CALL CAVIRA_MANEJO_CAJA.PROC_INFORME_DE_CIERRE("+nroIdApertura
					+ ","+funcion
					+ ","+Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"))
					+ ",outV,outV,outN,outN,outC,outN,outN,outN,outN,outN,outV,outV)}";
			logger.traceInfo("generaArchivoDeCierre", sql);
			cs.execute();
			rs = (ResultSet) cs.getObject(8);

			elFichero = new FileOutputStream(ruta);
			HSSFCell celda = null;
			int cont =8;
			if (rs!=null) {
				ResultSetMetaData rsmd = rs.getMetaData();	
				int ancho = rsmd.getColumnCount();
				
				celda = fila.createCell(1);
					
				//Defino estilos
                HSSFCellStyle miEstilo = libro.createCellStyle();
                HSSFFont miFuente=libro.createFont();
                miFuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                miFuente.setFontHeight((short)400);
                //miFuente.setUnderline((byte)1);Underline
                miEstilo.setFont(miFuente);
                hoja.addMergedRegion(new CellRangeAddress(0,0,1,10));
                fila = hoja.createRow(0);
                celda = fila.createCell(1);
                celda.setCellValue(titulo);                                
                celda.setCellStyle(miEstilo);
                
				//Resumen
                
                fila = hoja.createRow(2);
                celda = fila.createCell(0);
                celda.setCellValue("Caja N�: ");
                celda = fila.createCell(1);
                celda.setCellValue(pcv.buscaValorPorNombre("NRO_CAJA"));
                
                fila = hoja.createRow(3);
                celda = fila.createCell(0);
                celda.setCellValue("Fec.Proceso: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getString(4));
                
                fila = hoja.createRow(4);
                celda = fila.createCell(0);
                celda.setCellValue("N�Trx Apertura: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getInt(6));
                celda = fila.createCell(3);
                celda.setCellValue("Fec. Apertura: ");
                celda = fila.createCell(4);
                celda.setCellValue(cs.getString(4));
                
                fila = hoja.createRow(5);
                celda = fila.createCell(0);
                celda.setCellValue("N�Trx Cierre: ");
                celda = fila.createCell(1);
                celda.setCellValue(cs.getInt(7));
                celda = fila.createCell(3);
                celda.setCellValue("Fec. Cierre: ");
                celda = fila.createCell(4);
                celda.setCellValue(cs.getString(5));
                
				miFuente = null;
				miEstilo = null;
				
				miEstilo = libro.createCellStyle();
                miFuente=libro.createFont();
				miFuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				miEstilo.setFont(miFuente);
                
				//coloca los titulos a las cabeceras
				fila = hoja.createRow(cont);
				for (int cc=0;cc<ancho-1;cc++){
					celda = fila.createCell(cc);
					String res = rsmd.getColumnName(cc + 1);
					celda.setCellValue(res);
					celda.setCellStyle(miEstilo);
				
				}
				
				//llena la informaci�n
				cont++;
				while(rs.next()){
					int i=0; 				
					
					fila = hoja.createRow(cont);
					for (i = 0; i < ancho; i++) {
//						hoja.autoSizeColumn((short)i);
						celda = fila.createCell(i);
						String res = rs.getString(i + 1);
						if ((i+1)==6 && res.equalsIgnoreCase("ERROR"))		sumaError = sumaError.add(BigDecimal.ONE);
						if ((i+1)==6 && res.equalsIgnoreCase("MAIL"))		sumaMail = sumaMail.add(BigDecimal.ONE);
						if ((i+1)==6 && res.equalsIgnoreCase("IMPRESION"))	sumaImp = sumaImp.add(BigDecimal.ONE);
						if ((i+1)==11 && ((res.equalsIgnoreCase("B")) || (res.equalsIgnoreCase("BM"))))		sumaBol = sumaBol.add(BigDecimal.ONE);
						if ((i+1)==11 && ((res.equalsIgnoreCase("F")) || (res.equalsIgnoreCase("FM"))))		sumaFac = sumaFac.add(BigDecimal.ONE);
						if ((i+1)==6 && ((res.equalsIgnoreCase("RR")) || (res.equalsIgnoreCase("ANRR"))))	sumaRR = sumaRR.add(BigDecimal.ONE);
						if ((i+1)==6 && ((res.equalsIgnoreCase("RM")) || (res.equalsIgnoreCase("ANRM"))))	sumaRM = sumaRM.add(BigDecimal.ONE);
						if ((i+1)==7) {sumaMonto = sumaMonto.add(new BigDecimal(res)); sumaOC = sumaOC.add(BigDecimal.ONE);}
						if ((i+1)<=12)celda.setCellValue(res);						
					}
					cont++;
				}			
				//totales
				cont=cont+2;
				fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Monto");
                celda = fila.createCell(10);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaMonto));               
                
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Resumen");
                celda.setCellStyle(miEstilo);
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total imp"); 
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaImp));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Mail");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaMail));

                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (funcion==Constantes.FUNCION_NC)	{
					celda.setCellValue("NC/Boletas");
				}
				else{
					celda.setCellValue("Boletas");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaBol));//cs.getInt(9));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (funcion==Constantes.FUNCION_NC)	{
					celda.setCellValue("NC/Facturas");
				}
				else{
					celda.setCellValue("Facturas");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaFac));//cs.getInt(10));
                
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (funcion==Constantes.FUNCION_NC)	{
					celda.setCellValue("Anulaci�n RR");
				}
				else{
					celda.setCellValue("Total RR");
				}
				celda.setCellStyle(miEstilo);
				
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaRR));//cs.getInt(11));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
				if (funcion==Constantes.FUNCION_NC)	{
					celda.setCellValue("Anulaci�n RM");
				}
				else{
					celda.setCellValue("Total RM");
				}
				celda.setCellStyle(miEstilo);
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaRM));//cs.getInt(12));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total OC");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaOC));
                
                fila = hoja.createRow(cont++);
                celda = fila.createCell(0);
                celda.setCellValue("Total Err");
                celda.setCellStyle(miEstilo);
                
                celda = fila.createCell(1);
                celda.setCellValue(String.format(Locale.US, "%.2f", sumaError));

                //cierra libro
				libro.write(elFichero);
				rs.close();
				cs.close();
			}	
			
			
			elFichero.close();
			libro.close();
		} catch (Exception e) {
			logger.traceError("generaArchivo", "Error durante la ejecuci�n de generaArchivo", e);
		} finally {
			PoolBDs.closeConnection(conn);			
		}
		}else
		{
			logger.traceInfo("generaArchivo", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("generaArchivo", "Finalizado", null);
        
        
    }
@Override
public boolean existeCajaCerrada(Parametros pcv, String fechaCierre) throws AligareException {
	logger.initTrace("existeCajaCerrada", "Parametros: Parametros");
	boolean cajaCerrada = false;
	Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
	if (conn!=null){
	String selectSQL = ""; 
	try {
		/*
		selectSQL = "SELECT COUNT(*) FROM TRX_TRANSACCION "
				+ "WHERE SUCURSAL = ? AND NRO_CAJA = ? AND TIPO_TRX = 44 "
				+ "AND NRO_TRANSACCION >= (SELECT MAX(NRO_TRANSACCION) "
				+ "FROM TRX_TRANSACCION WHERE SUCURSAL =? AND NRO_CAJA = ? AND TIPO_TRX = 42)";
		*/
		
		selectSQL = "SELECT COUNT(*) FROM TRX_TRANSACCION T1 "
				+ "WHERE T1.SUCURSAL = ? AND T1.NRO_CAJA = ? AND T1.TIPO_TRX = 44 "
				+ "AND T1.NRO_TRANSACCION >= (SELECT MAX(T2.NRO_TRANSACCION) "
				+ "FROM TRX_TRANSACCION T2 WHERE T2.SUCURSAL =? AND T2.NRO_CAJA = ? AND T2.TIPO_TRX = 42 "
				+ ") "
				+ " AND T1.FECHA_TRX >= TO_DATE(?,'DD/MM/YYYY') ";		
		
		ResultSet rs = null;
		PreparedStatement ps = conn.prepareStatement(selectSQL);
		if (ps != null) {
			ps.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
			ps.setInt(2, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
			ps.setInt(3, Integer.parseInt(pcv.buscaValorPorNombre("sucursal")));
			ps.setInt(4, Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")));
			ps.setString(5, fechaCierre);

			rs = ps.executeQuery();
			
			/*selectSQL = "\nSELECT COUNT(*) FROM TRX_TRANSACCION "
					+ "WHERE SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+" AND NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TIPO_TRX = 44 "
					+ "AND NRO_TRANSACCION >= (SELECT MAX(NRO_TRANSACCION) "
					+ "FROM TRX_TRANSACCION WHERE SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+"  AND NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND TIPO_TRX = 42)";
					*/
			selectSQL = "\nSELECT COUNT(*) FROM TRX_TRANSACCION T1 "
					+ "WHERE T1.SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+" AND T1.NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND T1.TIPO_TRX = 44 "
					+ "AND T1.NRO_TRANSACCION >= (SELECT MAX(T2.NRO_TRANSACCION) "
					+ "FROM TRX_TRANSACCION T2 WHERE T2.SUCURSAL = "+Integer.parseInt(pcv.buscaValorPorNombre("sucursal"))+"  AND T2.NRO_CAJA = "+Integer.parseInt(pcv.buscaValorPorNombre("nro_caja"))+" AND T2.TIPO_TRX = 42 "
			        + " ) "
					+ " AND T1.FECHA_TRX >= TO_DATE('"+fechaCierre+"','DD/MM/YYYY') ";			
					
			logger.traceInfo("existeCajaCerrada", "Ejecuci�n de query OK!" + selectSQL);
			if (rs != null && rs.next()) {
				if (rs.getInt(1) > 0) {
					cajaCerrada = true;
				}
				rs.close();			
				
			}
			ps.close();
		}

	} catch (Exception e) {
		logger.traceError("existeCajaCerrada", "Error durante la ejecuci�n de existeCajaCerrada", e);
	} finally {
		PoolBDs.closeConnection(conn);			
	}
	}else
	{
		logger.traceInfo("existeCajaCerrada", "Problemas de conexion a la base de datos");
	}
	logger.endTrace("existeCajaCerrada", "Finalizado", "La caja se encuentra cerrada?: " + cajaCerrada);
	return cajaCerrada;
}
@Override
public TotalCuadratura datosMailCuadratura(Parametros pcv, Long nroIdApertura) throws AligareException {
	logger.initTrace("datosMailCuadratura", "Parametros: ", new KeyLog("nroIdApertura: ",""+nroIdApertura));
	TotalCuadratura totalCuadratura = null;
	Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
	if (conn!=null){
	try {
		String procSQL ="{CALL CAVIRA_MANEJO_CAJA.PROC_MAIL_CUADRATURA(?,?,?,?,?,?,?,?,?)}";
		String sql = "";
		CallableStatement cs = null;
		
			cs = conn.prepareCall(procSQL);			
			cs.setInt(1, Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")));
			cs.setLong(2, nroIdApertura);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.registerOutParameter(8, OracleTypes.NUMBER);
			
			cs.registerOutParameter(9, OracleTypes.VARCHAR);
			
			sql = "{CALL CAVIRA_MANEJO_CAJA.PROC_MAIL_CUADRATURA("
					+ ""+Integer.parseInt(pcv.buscaValorPorNombre("FUNCION"))
					+ ","+nroIdApertura
					+ ",outN,outN,outN,outN,outV,outN,outV)}";
			
			logger.traceInfo("generaArchivoDeCierre", sql);
			cs.execute();
			logger.traceInfo("generaArchivoDeCierre", "mensaje de salida", new KeyLog("mensaje salida", cs.getString(8)));
			totalCuadratura = new TotalCuadratura();
			totalCuadratura.setBoletasFacturas(cs.getBigDecimal(3));
			totalCuadratura.setRecaudaciones(cs.getBigDecimal(4));
			totalCuadratura.setUnidadesImp(cs.getBigDecimal(5));
			totalCuadratura.setPagoEnTienda(cs.getBigDecimal(6));

	} catch (Exception e) {
		logger.traceError("datosMailCuadratura", "Error durante la ejecución de datosMailCuadratura", e);
		return null;
	} finally {
		PoolBDs.closeConnection(conn);			
	}
	}else
	{
		logger.traceInfo("datosMailCuadratura", "Problemas de conexion a la base de datos");
	}
	logger.endTrace("datosMailCuadratura", "Finalizado", "totalCuadratura: " + totalCuadratura.toString());
	return totalCuadratura;
}
	@Override
	//Ariel de las mercedes pidio esta custion 
	public boolean saltarFolio(Parametros pcv, int nroTrx) throws AligareException {
		logger.initTrace("saltarFolio", "Parametros: ", new KeyLog("nroTrx: ",""+nroTrx));
		boolean retorno = true;
		boolean act = true;
		if (!existeTrx(pcv, nroTrx, null)){
			logger.traceInfo("saltarFolio", "No existe numero de trx "+nroTrx+" se salta al "+(nroTrx+1));
			act = actualizaTransaccion(Integer.parseInt(pcv.buscaValorPorNombre("nro_caja")), nroTrx+1);
			logger.traceInfo("saltarFolio", "Problemas en actualizaTransaccion a trx "+(nroTrx+1)+" - "+act);
		}
		logger.endTrace("saltarFolio", "Finalizado", "retorno: " + retorno);
		return retorno;
	}
	@Override
	public boolean cierreCajaIntegradaNew(Long trx, Parametros pcv, String fecha, String fechaHora, TotalesCierre totalesCierre)
			throws AligareException {
		logger.initTrace("cierreCajaIntegradaNew", "Long trx, Parametros pcv, String fecha, String fechaHora, TotalesCierre totalesCierre", 
				new KeyLog("Trx", String.valueOf(trx)),
				new KeyLog("Fecha", fecha),
				new KeyLog("Fecha Hora", fechaHora),
				new KeyLog("Totales Cierre", totalesCierre.toString()));
		
		boolean resultado = Boolean.TRUE;
		
		Integer nroCaja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		logger.traceInfo("cierreCajaIntegradaNew", "Generando request para servicio REST", 
				new KeyLog("Nro Caja", String.valueOf(nroCaja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)));
		
		
		PrepareBoCallCierre s = new PrepareBoCallCierre();
		s.setFecha(fecha);
		s.setFechaHora(fechaHora);
		s.setPcv(pcv);
		s.setTotalesCierre(totalesCierre);
		s.setTrx(trx);
		
		RequestBO request = conversionService.convert(s, RequestBO.class);
		
		logger.traceInfo("cierreCajaIntegradaNew", "Objeto request generado", 
				new KeyLog("Objeto Request RequestBO", String.valueOf(request)));
		
		String url = pcv.buscaValorPorNombre("URL_BACKOFFICE");
		
		logger.traceInfo("cierreCajaIntegradaNew", "URL para servicio REST", 
				new KeyLog("URL", url));
		
		logger.traceInfo("cierreCajaIntegradaNew", "Invocando servicio REST");
		
		BackOfficeResponse resp = restTemplate.postForObject(url, request, BackOfficeResponse.class);
		
		logger.traceInfo("cierreCajaIntegradaNew", "Respuesta servicio REST", 
				new KeyLog("Objeto respuesta BackOfficeResponse", String.valueOf(resp)));
		
		if(resp.getCodigo().intValue() != Constantes.NRO_UNO) {
			
			resultado = Boolean.FALSE;
			logger.traceInfo("cierreCajaIntegradaNew", "Resultado no exitoso", 
					new KeyLog("Codigo respuesta", String.valueOf(resp.getCodigo())),
					new KeyLog("Mensaje respuesta", String.valueOf(resp.getMensaje())));
			
		}
		
		logger.endTrace("cierreCajaIntegradaNew", "Servicio REST invocado exitosamente", "resultado = " + resultado);
		return resultado;
	}
	@Override
	public boolean aperturaCajaNew(Long trx, Parametros pcv, String fecha, String fechaHora) throws AligareException {
		logger.initTrace("aperturaCajaNew", "Long trx, Parametros pcv, String fecha, String fechaHora, TotalesCierre totalesCierre", 
				new KeyLog("Trx", String.valueOf(trx)),
				new KeyLog("Fecha", fecha),
				new KeyLog("Fecha Hora", fechaHora));
		
		boolean resultado = Boolean.TRUE;
		
		Integer nroCaja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		logger.traceInfo("aperturaCajaNew", "Generando request para servicio REST", 
				new KeyLog("Nro Caja", String.valueOf(nroCaja)),
				new KeyLog("Sucursal", String.valueOf(sucursal)));
		
		PrepareBoCallApertura s = new PrepareBoCallApertura();
		s.setFecha(fecha);
		s.setFechaHora(fechaHora);
		s.setPcv(pcv);
		s.setTrx(trx);
		
		RequestBO request = conversionService.convert(s, RequestBO.class);
		
		
		logger.traceInfo("aperturaCajaNew", "Objeto request generado", 
				new KeyLog("Objeto Request RequestBO", String.valueOf(request)));
		
		String url = pcv.buscaValorPorNombre("URL_BACKOFFICE");
		
		logger.traceInfo("aperturaCajaNew", "URL para servicio REST", 
				new KeyLog("URL", url));
		
		logger.traceInfo("aperturaCajaNew", "Invocando servicio REST");
		
		BackOfficeResponse resp = restTemplate.postForObject(url, request, BackOfficeResponse.class);
		
		logger.traceInfo("aperturaCajaNew", "Respuesta servicio REST", 
				new KeyLog("Objeto respuesta BackOfficeResponse", String.valueOf(resp)));
		
		if(resp.getCodigo().intValue() != Constantes.NRO_UNO) {
			
			resultado = Boolean.FALSE;
			logger.traceInfo("aperturaCajaNew", "Resultado no exitoso", 
					new KeyLog("Codigo respuesta", String.valueOf(resp.getCodigo())),
					new KeyLog("Mensaje respuesta", String.valueOf(resp.getMensaje())));
			
		}
		
		logger.endTrace("aperturaCajaNew", "Servicio REST invocado exitosamente", "resultado = " + resultado);
		return resultado;
	}
	@Override
	public boolean updNroTransaccion(Integer nroTrx, Parametros pcv) throws AligareException {
		logger.initTrace("updNroTransaccion", "Parametros: Parametros",
				new KeyLog("NRO_CAJA: ", pcv.buscaValorPorNombre("nro_caja")),
				new KeyLog("Nro Trx: ", String.valueOf(nroTrx)),
				new KeyLog("NRO_SUCURSAL: ", pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));
		Connection conn = null;
		PreparedStatement ps = null;
		boolean resultado = Boolean.TRUE;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);


			String selectSQL2 = "UPDATE	CV_CONTROL_TRX " + 
					"SET	NRO_TRX = ? " + 
					"WHERE	NRO_CAJA = ? AND NRO_SUCURSAL = ?";

			ps = conn.prepareStatement(selectSQL2);

			ps.setInt(1, nroTrx);
			ps.setInt(2, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
			ps.setInt(3, Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));

			int cont = ps.executeUpdate();
			
			logger.traceInfo("updNroTransaccion", "Cantidad registros actualizados = " + cont);
			
			if(cont != Constantes.NRO_CERO) {

				resultado = Boolean.FALSE;

			}

			if(ps != null) {
				ps.close();
			}
			logger.traceInfo("updNroTransaccion", "Se actualiza el nro de trx");

			
		} catch (Exception e) {
			logger.traceError("updNroTransaccion", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			
			PoolBDs.closeConnection(conn);

			if (!resultado){
				logger.traceInfo("updNroTransaccion", "SYSTEM EXIT ERROR: La caja no está creada en tabla CV_CONTROL_TRX o no se encuentra en la fecha correcta");
				logger.endTrace("updNroTransaccion", "Finalizado", "Resultado del update: "+ resultado);
//				System.exit(CodErrorSystemExit.CAJA_NO_DISPONIBLE);
			}else{
				logger.endTrace("resNroTransaccion", "Finalizado", "Resultado del update: "+ resultado);
			}

		}

		return resultado;
	}
	
	@Override
	public List<SucursalRpos> cajaSucursalRpos(int jobJenkins) throws AligareException {
		logger.initTrace("cajaSucursalRpos", "jobJenkins: "+String.valueOf(jobJenkins),
				new KeyLog("jobJenkins: ", String.valueOf(jobJenkins)));
		List<SucursalRpos> list = new ArrayList<SucursalRpos>();
		Connection conn=null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL ="SELECT NUMERO_SUCURSAL, NUMERO_CAJA FROM CV_SUCURSAL_RPOS "
					+ "WHERE JOB_JENKINS = ? AND ACTIVO = ?";
			
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			ps.setString(1, String.valueOf(jobJenkins));
			ps.setString(2, Constantes.STRING_UNO);
			logger.traceInfo("cajaSucursalRpos", "se obtiene resultados de consulta");
			if (ps != null) {
				ResultSet rs = ps.executeQuery();
				if (rs != null) {
					while (rs.next()) {
						SucursalRpos cajaSuc= new SucursalRpos();
						cajaSuc.setCaja(rs.getInt(Constantes.PARAM_NUMERO_CAJA));
						cajaSuc.setSucursal(rs.getInt(Constantes.PARAM_NUMERO_SUCURSAL));
						list.add(cajaSuc);
					}
					rs.close();
					ps.close();
				}
			}
		} catch (Exception e) {
			logger.traceError("cajaSucursalRpos", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
		}
		logger.endTrace("cajaSucursalRpos", "Finalizado", "Resultado: "+ list);
		return list;
	}
	
	@Override
	public SucursalRpos getDatosSuc(Integer sucursal, Integer caja) throws AligareException {
		logger.initTrace("getDatosSuc", "sucursal: "+String.valueOf(sucursal)+", caja: "+String.valueOf(caja),
				new KeyLog("sucursal: ", String.valueOf(sucursal))
				,new KeyLog("caja: ", String.valueOf(caja)));
		SucursalRpos sucursalRpos = new SucursalRpos();
		Connection conn=null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL ="SELECT * FROM CV_SUCURSAL_RPOS "
					+ "WHERE NUMERO_SUCURSAL = ? AND NUMERO_CAJA = ? AND ACTIVO = ?";
			
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			ps.setString(1, String.valueOf(sucursal));
			ps.setString(2, String.valueOf(caja));
			ps.setString(3, Constantes.STRING_UNO);
			logger.traceInfo("getDatosSuc", "se obtiene resultados de consulta");
			if (ps != null) {
				ResultSet rs = ps.executeQuery();
				if (rs != null && rs.next()) {
					sucursalRpos.setSucursal(rs.getInt(Constantes.PARAM_NUMERO_SUCURSAL));
					sucursalRpos.setCaja(rs.getInt(Constantes.PARAM_NUMERO_CAJA));
					sucursalRpos.setDireccion(rs.getString(Constantes.PARAM_DIRECCION));
					sucursalRpos.setLocalTienda(rs.getString(Constantes.PARAM_LOCAL_TIENDA));
					sucursalRpos.setDescripcion(rs.getString(Constantes.PARAM_DESCRIPCION));
					sucursalRpos.setCodSunat(rs.getString(Constantes.PARAM_COD_SUNAT));
					sucursalRpos.setSerieSunat(rs.getString(Constantes.PARAM_SERIE_SUNAT));
					sucursalRpos.setDistrito(rs.getString(Constantes.PARAM_DISTRITO));
					sucursalRpos.setDireccionCorta(rs.getString(Constantes.PARAM_DIRECCION_CORTA));
					sucursalRpos.setSucursalPpl(rs.getInt(Constantes.PARAM_SUCURSAL_PPL));
					sucursalRpos.setSucursalRpos(rs.getInt(Constantes.PARAM_SUCURSAL_RPOS));
					rs.close();
					ps.close();
				}
			}
		} catch (Exception e) {
			logger.traceError("getDatosSuc", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
		}
		logger.endTrace("getDatosSuc", "Finalizado", "Resultado: "+ sucursalRpos);
		return sucursalRpos;
	}
	@Override
	public Map<String, Integer> getCajaSucRpos(Long oc) throws AligareException {
		logger.initTrace("getCajaSucRpos", "oc: "+String.valueOf(oc),
				new KeyLog("oc: ", String.valueOf(oc)));
		Map<String,Integer> resp = new HashMap<>();
		Connection conn=null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL ="SELECT NUMERO_CAJA, NUMERO_SUCURSAL FROM NOTA_VENTA "
					+ "WHERE CORRELATIVO_VENTA = ?";
			
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			ps.setString(1, String.valueOf(oc));
			logger.traceInfo("getCajaSucRpos", "se obtiene resultados de consulta");
			if (ps != null) {
				ResultSet rs = ps.executeQuery();
				if (rs != null && rs.next()) {
					resp.put(Constantes.PARAM_NUMERO_CAJA, rs.getInt(Constantes.PARAM_NUMERO_CAJA));
					resp.put(Constantes.PARAM_NUMERO_SUCURSAL, rs.getInt(Constantes.PARAM_NUMERO_SUCURSAL));
					rs.close();
					ps.close();
				}
			}
		} catch (Exception e) {
			logger.traceError("getCajaSucRpos", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
		}
		logger.endTrace("getCajaSucRpos", "Finalizado", "Resultado: "+ resp);
		return resp;
	}
	@Override
	public List<SucursalRpos> cajaSucUrlDoce(int jobUrlDoce) throws AligareException {
		logger.initTrace("cajaSucUrlDoce", "jobUrlDoce: "+String.valueOf(jobUrlDoce),
				new KeyLog("jobJenkins: ", String.valueOf(jobUrlDoce)));
		List<SucursalRpos> list = new ArrayList<SucursalRpos>();
		Connection conn=null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String selectSQL ="SELECT NUMERO_SUCURSAL, NUMERO_CAJA FROM CV_SUCURSAL_RPOS "
					+ "WHERE JOB_URL_DOCE = ? AND ACTIVO = ?";
			
			PreparedStatement ps = conn.prepareStatement(selectSQL);
			ps.setString(1, String.valueOf(jobUrlDoce));
			ps.setString(2, Constantes.STRING_UNO);
			logger.traceInfo("cajaSucUrlDoce", "se obtiene resultados de consulta");
			if (ps != null) {
				ResultSet rs = ps.executeQuery();
				if (rs != null) {
					while (rs.next()) {
						SucursalRpos cajaSuc= new SucursalRpos();
						cajaSuc.setCaja(rs.getInt(Constantes.PARAM_NUMERO_CAJA));
						cajaSuc.setSucursal(rs.getInt(Constantes.PARAM_NUMERO_SUCURSAL));
						list.add(cajaSuc);
					}
					rs.close();
					ps.close();
				}
			}
		} catch (Exception e) {
			logger.traceError("cajaSucUrlDoce", e);
			throw new AligareException(
					"Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
		}
		logger.endTrace("cajaSucUrlDoce", "Finalizado", "Resultado: "+ list);
		return list;
	}
}
