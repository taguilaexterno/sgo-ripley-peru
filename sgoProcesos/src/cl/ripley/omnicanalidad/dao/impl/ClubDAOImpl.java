package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.ClienteClub;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.dao.ClubDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

public class ClubDAOImpl implements ClubDAO {

	private static final AriLog logger = new AriLog(ClubDAOImpl.class, "RIP16-004", PlataformaType.JAVA);
	
	@Override
	public ClienteClub getClienteClub(Integer numeroEvento, int indicadorTitular) {
		logger.initTrace("getClienteClub", "Integer numeroEvento: "+numeroEvento+" - int indicadorTitular: "+indicadorTitular);
		
		ClienteClub clienteClub = null;
		
		Connection conn = PoolBDs.getConnection(BDType.CLUB);
		if (conn!=null){
			String selectSQL = "";
			try {
				selectSQL = "SELECT RGMCL_COC_IDE AS RUT, RGMCL_NOM_CLI AS NOMBRE, RGMCL_GLS_APL_PAT AS APELLIDOP, RGMCL_GLS_APL_MAT AS APELLIDOM " + 
							"FROM LGLREG_MAE_CLI_EVT " + 
							"WHERE RGMCL_COD_EVT = ? " + 
							"AND RGMCL_NRO_IDC_MON = ?";
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				if (ps != null) {
					ps.setInt(1, numeroEvento);
					ps.setInt(2, indicadorTitular);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						clienteClub = new ClienteClub();
						clienteClub.setRut(rs.getString("RUT"));
						clienteClub.setNombre(rs.getString("NOMBRE"));
						clienteClub.setApellidoPaterno(rs.getString("APELLIDOP"));
						clienteClub.setApellidoMaterno(rs.getString("APELLIDOM"));
						rs.close();
					}
					ps.close();
				}
			} catch (Exception e) {
				logger.traceError("getClienteClub", "Error durante la ejecución de getClienteClub", e);
				logger.endTrace("getClienteClub", "Finalizado", "ERROR");
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		} else {
			logger.traceInfo("getClienteClub", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("getClienteClub", "Finalizado", "clienteClub: " + ((clienteClub!=null)?clienteClub.getRut():"null"));
		return clienteClub;
	}

	@Override
	public RetornoEjecucion lglreg_prc_mov_lis_vlr(	String codigoEvento,
													long codigoProducto,
													Integer codSucursal,
													Timestamp fechaCompra,
													Integer numeroTransaccion,
													Integer codigoTransaccion,
													Integer codEntraSale,
													Integer tipoDocumento,
													Integer cantidadProducto,
													long precioCompra,
													Integer numeroBol,
													Integer numeroCaja,
													Integer formaPago,
													Integer secuencial,
													String dirDespacho,
													String marcaRegalo,
													Integer fechaDespacho,
													String email,
													String nombreInvitado,
													String apellidoPaternoInvit,
													String apellidoMaternoInvit,
													String direcInvitado,
													Integer telefonoInvitado,
													String mensajeInvitado,
													Integer fchOrigen,
													Integer boletaOrigen,
													Integer sucursalOrigen,
													Integer cjaOrigen) {
		logger.initTrace("lglreg_prc_mov_lis_vlr",	    "(String codigoEvento: "+codigoEvento+"," 
													+ "\nlong codigoProducto: "+codigoProducto+"," 
													+ "\nInteger codSucursal: "+codSucursal+","
													+ "\nTimestamp fechaCompra: "+fechaCompra+", "
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+"," 
													+ "\nInteger codigoTransaccion: "+codigoTransaccion+", "
													+ "\nInteger codEntraSale: "+codEntraSale+","
													+ "\nInteger tipoDocumento: "+tipoDocumento+"," 
													+ "\nInteger cantidadProducto: "+cantidadProducto+"," 
													+ "\nlong precioCompra: "+precioCompra+", "
													+ "\nInteger numeroBol: "+numeroBol+","
													+ "\nInteger numeroCaja: "+numeroCaja+"," 
													+ "\nInteger formaPago: "+formaPago+", "
													+ "\nInteger secuencial: "+secuencial+", "
													+ "\nString dirDespacho: "+dirDespacho+", "
													+ "\nString marcaRegalo: "+marcaRegalo+","
													+ "\nInteger fechaDespacho: "+fechaDespacho+"," 
													+ "\nString email: "+email+", "
													+ "\nString nombreInvitado: "+nombreInvitado+"," 
													+ "\nString apellidoPaternoInvit: "+apellidoPaternoInvit+","
													+ "\nString apellidoMaternoInvit: "+apellidoMaternoInvit+", "
													+ "\nString direcInvitado: "+direcInvitado+"," 
													+ "\nInteger telefonoInvitado: "+telefonoInvitado+"," 
													+ "\nString mensajeInvitado: "+mensajeInvitado+","
													+ "\nInteger fchOrigen: "+fchOrigen+"," 
													+ "\nInteger boletaOrigen: "+boletaOrigen+"," 
													+ "\nInteger sucursalOrigen: "+sucursalOrigen+"," 
													+ "\nInteger cjaOrigen: "+cjaOrigen+")");
		Connection conn = PoolBDs.getConnection(BDType.CLUB);
		CallableStatement cs;
		RetornoEjecucion retorno = new RetornoEjecucion();

		if (conn != null) {
			try {

				String procSQL = "{CALL LGLREG_PRC_MOV_LIS_VLR(?,?,?,TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, codigoEvento);
				cs.setLong(2, codigoProducto);
				cs.setInt(3, (codSucursal.intValue() + 10000));
				cs.setString(4, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra));
				cs.setString(5, Constantes.FECHA_DDMMYYYY);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, codigoTransaccion);
				cs.setInt(8, codEntraSale);
				cs.setInt(9, tipoDocumento);
				cs.setInt(10, cantidadProducto);
				cs.setLong(11, precioCompra);
				cs.setInt(12, numeroBol);
				cs.setInt(13, numeroCaja);
				cs.setInt(14, formaPago);
				cs.setInt(15, secuencial);
				cs.setString(16, dirDespacho);
				cs.setString(17, marcaRegalo);
				if (!Validaciones.validaInteger(fechaDespacho)) {
					cs.setNull(18, OracleTypes.NUMBER);
				} else {
					cs.setInt(18, fechaDespacho);
				}
				cs.setString(19, email);
				cs.setString(20, nombreInvitado);
				cs.setString(21, apellidoPaternoInvit);
				cs.setString(22, apellidoMaternoInvit);
				cs.setString(23, direcInvitado);
				cs.setInt(24, telefonoInvitado);
				cs.setString(25, mensajeInvitado);
				cs.setInt(26, fchOrigen);
				cs.setInt(27, boletaOrigen);
				cs.setInt(28, sucursalOrigen);
				cs.setInt(29, cjaOrigen);
				cs.registerOutParameter(30, OracleTypes.NUMBER);
				cs.registerOutParameter(31, OracleTypes.VARCHAR);

				logger.traceInfo("lglreg_prc_mov_lis_vlr", "{CALL LGLREG_PRC_MOV_LIS_VLR('"+codigoEvento+"',"+codigoProducto+","+(codSucursal.intValue() + 10000)+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+numeroTransaccion+","+codigoTransaccion+","+codEntraSale+","+tipoDocumento+","+cantidadProducto+","+precioCompra+","+numeroBol+","+numeroCaja+","+formaPago+","+secuencial+",'"+dirDespacho+"','"+marcaRegalo+"',"+fechaDespacho+",'"+email+"','"+nombreInvitado+"','"+apellidoPaternoInvit+"','"+apellidoMaternoInvit+"','"+direcInvitado+"',"+telefonoInvitado+",'"+mensajeInvitado+"',"+fchOrigen+","+boletaOrigen+","+sucursalOrigen+","+cjaOrigen+",out,out)}");

				cs.execute();

				retorno.setCodigo(cs.getInt(30));
				retorno.setMensaje(cs.getString(31));

				cs.close();
			} catch (Exception e) {
				retorno.setCodigo(Constantes.EJEC_CON_ERRORES);
				retorno.setMensaje("Mensaje de error lglreg_prc_mov_lis_vlr: " + e.getMessage());
				logger.traceError("lglreg_prc_mov_lis_vlr", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("lglreg_prc_mov_lis_vlr", "Problemas de conexion a la base de datos Club");
			retorno.setCodigo(Constantes.EJEC_CON_ERRORES);
			retorno.setMensaje("Conexion nula");
		}
		logger.endTrace("lglreg_prc_mov_lis_vlr", "Finalizado", "RetornoEjecucion: " + retorno.toString());
		return retorno;
	}

	@Override
	public RetornoEjecucion lglreg_prc_mov_lis_vlr_mkp(	String codigoEvento,
														long codigoProducto,
														Integer codSucursal,
														Timestamp fechaCompra,
														Integer numeroTransaccion,
														Integer codigoTransaccion,
														Integer codEntraSale,
														Integer tipoDocumento,
														Integer cantidadProducto,
														long precioCompra,
														Integer numeroBol,
														Integer numeroCaja,
														Integer formaPago,
														Integer secuencial,
														String dirDespacho,
														String marcaRegalo,
														Integer fechaDespacho,
														String email,
														String nombreInvitado,
														String apellidoPaternoInvit,
														String apellidoMaternoInvit,
														String direcInvitado,
														Integer telefonoInvitado,
														String mensajeInvitado,
														Integer fchOrigen,
														Integer boletaOrigen,
														Integer sucursalOrigen,
														Integer cjaOrigen, 
														String descProducto) {
		logger.initTrace("lglreg_prc_mov_lis_vlr_mkp", "(String codigoEvento: "+codigoEvento+"," 
													+ "\nlong codigoProducto: "+codigoProducto+"," 
													+ "\nInteger codSucursal: "+codSucursal+","
													+ "\nTimestamp fechaCompra: "+fechaCompra+", "
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+"," 
													+ "\nInteger codigoTransaccion: "+codigoTransaccion+", "
													+ "\nInteger codEntraSale: "+codEntraSale+","
													+ "\nInteger tipoDocumento: "+tipoDocumento+"," 
													+ "\nInteger cantidadProducto: "+cantidadProducto+"," 
													+ "\nlong precioCompra: "+precioCompra+", "
													+ "\nInteger numeroBol: "+numeroBol+","
													+ "\nInteger numeroCaja: "+numeroCaja+"," 
													+ "\nInteger formaPago: "+formaPago+", "
													+ "\nInteger secuencial: "+secuencial+", "
													+ "\nString dirDespacho: "+dirDespacho+", "
													+ "\nString marcaRegalo: "+marcaRegalo+","
													+ "\nInteger fechaDespacho: "+fechaDespacho+"," 
													+ "\nString email: "+email+", "
													+ "\nString nombreInvitado: "+nombreInvitado+"," 
													+ "\nString apellidoPaternoInvit: "+apellidoPaternoInvit+","
													+ "\nString apellidoMaternoInvit: "+apellidoMaternoInvit+", "
													+ "\nString direcInvitado: "+direcInvitado+"," 
													+ "\nInteger telefonoInvitado: "+telefonoInvitado+"," 
													+ "\nString mensajeInvitado: "+mensajeInvitado+","
													+ "\nInteger fchOrigen: "+fchOrigen+"," 
													+ "\nInteger boletaOrigen: "+boletaOrigen+"," 
													+ "\nInteger sucursalOrigen: "+sucursalOrigen+"," 
													+ "\nInteger cjaOrigen: "+cjaOrigen+","
													+ "\nString descProducto: "+descProducto+")");
		Connection conn = PoolBDs.getConnection(BDType.CLUB);
		CallableStatement cs;
		RetornoEjecucion retorno = new RetornoEjecucion();
		
		if (conn != null) {
			try {
				String procSQL = "{CALL LGLREG_PRC_MOV_LIS_VLR_MKP(?,?,?,TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, codigoEvento);
				cs.setLong(2, codigoProducto);
				cs.setInt(3, (codSucursal.intValue() + 10000));
				cs.setString(4, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra));
				cs.setString(5, Constantes.FECHA_DDMMYYYY);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, codigoTransaccion);
				cs.setInt(8, codEntraSale);
				cs.setInt(9, tipoDocumento);
				cs.setInt(10, cantidadProducto);
				cs.setLong(11, precioCompra);
				cs.setInt(12, numeroBol);
				cs.setInt(13, numeroCaja);
				cs.setInt(14, formaPago);
				cs.setInt(15, secuencial);
				cs.setString(16, dirDespacho);
				cs.setString(17, marcaRegalo);
				if (!Validaciones.validaInteger(fechaDespacho)) {
					cs.setNull(18, OracleTypes.NUMBER);
				} else {
					cs.setInt(18, fechaDespacho);
				}
				cs.setString(19, email);
				cs.setString(20, nombreInvitado);
				cs.setString(21, apellidoPaternoInvit);
				cs.setString(22, apellidoMaternoInvit);
				cs.setString(23, direcInvitado);
				cs.setInt(24, telefonoInvitado);
				cs.setString(25, mensajeInvitado);
				cs.setInt(26, fchOrigen);
				cs.setInt(27, boletaOrigen);
				cs.setInt(28, sucursalOrigen);
				cs.setInt(29, cjaOrigen);
				cs.setString(30, descProducto);
				cs.registerOutParameter(31, OracleTypes.NUMBER);
				cs.registerOutParameter(32, OracleTypes.VARCHAR);
				
				logger.traceInfo("lglreg_prc_mov_lis_vlr_mkp", "{CALL LGLREG_PRC_MOV_LIS_VLR_MKP('"+codigoEvento+"',"+codigoProducto+","+(codSucursal.intValue() + 10000)+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+numeroTransaccion+","+codigoTransaccion+","+codEntraSale+","+tipoDocumento+","+cantidadProducto+","+precioCompra+","+numeroBol+","+numeroCaja+","+formaPago+","+secuencial+",'"+dirDespacho+"','"+marcaRegalo+"',"+fechaDespacho+",'"+email+"','"+nombreInvitado+"','"+apellidoPaternoInvit+"','"+apellidoMaternoInvit+"','"+direcInvitado+"',"+telefonoInvitado+",'"+mensajeInvitado+"',"+fchOrigen+","+boletaOrigen+","+sucursalOrigen+","+cjaOrigen+",'"+descProducto+"',out,out)}");
				
				cs.execute();
				
				retorno.setCodigo(cs.getInt(31));
				retorno.setMensaje(cs.getString(32));
				
				cs.close();
			} catch (Exception e) {
				retorno.setCodigo(Constantes.EJEC_CON_ERRORES);
				retorno.setMensaje("Mensaje de error lglreg_prc_mov_lis_vlr_mkp: " + e.getMessage());
				logger.traceError("lglreg_prc_mov_lis_vlr_mkp", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("lglreg_prc_mov_lis_vlr_mkp", "Problemas de conexion a la base de datos Club");
			retorno.setCodigo(Constantes.EJEC_CON_ERRORES);
			retorno.setMensaje("Conexion nula");
		}
		logger.endTrace("lglreg_prc_mov_lis_vlr_mkp", "Finalizado", "RetornoEjecucion: " + retorno.toString());
		return retorno;
	}

}
