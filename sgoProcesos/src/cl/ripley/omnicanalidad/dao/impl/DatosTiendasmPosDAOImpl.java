package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.DatosTiendaMpos;
import cl.ripley.omnicanalidad.dao.DatosTiendasmPosDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;

public class DatosTiendasmPosDAOImpl implements DatosTiendasmPosDAO{
	private static final AriLog logger = new AriLog(DatosTiendasmPosDAO.class,"RIP16-004", PlataformaType.JAVA);
	
	@Override
	public DatosTiendaMpos getDataTiendaByLlave(String llave) {
		logger.initTrace("getTemplateByLlave", "String : Cod Bodega "+llave);
		DatosTiendaMpos datosTienda = null; 
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if(conn != null){
			CallableStatement cs = null;
			
			try {
				int codBodega = Integer.parseInt(llave);
				
				String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_DATOS_TIENDA(?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, codBodega);
				cs.registerOutParameter(2, OracleTypes.CURSOR);
				cs.registerOutParameter(3, OracleTypes.NUMBER);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				logger.traceInfo("getTemplateByLlave", "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_DATOS_TIENDA('"+llave+"',outc,outn,outv)}");
				cs.execute();    
				ResultSet rs = (ResultSet) cs.getObject(2);
				//logger.traceInfo("getTemplateByLlave", "Query: "+((String)cs.getObject(2)));
				if (rs != null && rs.next()) {
					datosTienda = new DatosTiendaMpos();
					datosTienda.setUrlImagen(rs.getString("URL_MAPA"));
					datosTienda.setHorario(rs.getString("HORARIO"));
					datosTienda.setDireccion(rs.getString("DIRECCION"));
				} else {
					datosTienda = new DatosTiendaMpos();
					datosTienda.setUrlImagen("ERROR");
				}
				rs.close();
				cs.close();
			} catch (Exception e) {
				logger.traceError("getDataTiendaByLlave", "Error durante la ejecución de getDataTiendaByLlave", e);
				logger.endTrace("getDataTiendaByLlave", "Finalizado", null);
				return null;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("getDataTiendaByLlave", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("getDataTiendaByLlave", "Finalizado", null);

		return datosTienda;
	}

	
	
}
