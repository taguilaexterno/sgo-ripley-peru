package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ripley.dao.IOrdenCompraDAO;
import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.restservices.model.backOfficeRest.BackOfficeResponse;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallBoleta;
import com.ripley.restservices.model.backOfficeRest.RequestBO;
import com.ripley.restservices.model.pagoTiendaRest.PagoEnTienda;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVenta;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.DatoFacturacion;
import cl.ripley.omnicanalidad.bean.Despacho;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.MkpMkl;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.TVirtualCodBancaria;
import cl.ripley.omnicanalidad.bean.TarjetaBancaria;
import cl.ripley.omnicanalidad.bean.TarjetaRegaloEmpresa;
import cl.ripley.omnicanalidad.bean.TarjetaRipley;
import cl.ripley.omnicanalidad.bean.TipoDoc;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.bean.Vendedor;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.ClubDAO;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

@Repository
public class GeneracionDTEDAOImpl implements GeneracionDTEDAO {

	private static final AriLog logger = new AriLog(GeneracionDTEDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private CajaVirtualDAO caja;
	
	@Autowired
	private ConversionService conversionService;
	
	@Autowired
	private IOrdenCompraDAO ordenCompraDAO;
	
	@Autowired
	private RestTemplate rt;
	
	public GeneracionDTEDAOImpl() {
		
	}
	
	@Override
	public ConcurrentLinkedQueue<TramaDTE> obtenerOrdenesParaGeneracion(Integer nroCaja, Integer nroSucusal,
			Integer nroEstado) {

		logger.initTrace("obtenerOrdenesParaGeneracion", "Integer nroCaja: " + nroCaja + " - Integer nroSucusal: "
				+ nroSucusal + " - Integer nroEstado: " + nroEstado, new KeyLog("Numero Caja", nroCaja+Constantes.VACIO), new KeyLog("Numero sucursal", nroSucusal+Constantes.VACIO), new KeyLog("Estado", nroEstado+Constantes.VACIO));

		ConcurrentLinkedQueue<TramaDTE> listado = new ConcurrentLinkedQueue<TramaDTE>();
//
//		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
//		if (conn != null) {
//			CallableStatement cs = null;
//
//			try {
//
//				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_BOLETAS(?,?,?,?,?,?,?)}";
//				cs = conn.prepareCall(procSQL);
//				cs.setInt(1, nroCaja);
//				cs.setInt(2, nroSucusal);
//				cs.setInt(3, nroEstado);
//				cs.registerOutParameter(4, OracleTypes.CLOB);
//				cs.registerOutParameter(5, OracleTypes.CURSOR);
//				cs.registerOutParameter(6, OracleTypes.NUMBER);
//				cs.registerOutParameter(7, OracleTypes.VARCHAR);
//				logger.traceInfo("obtenerOrdenesParaGeneracion","{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_BOLETAS("+nroCaja+","+nroSucusal+","+nroEstado+",outc,outc,outn,outv)}");
//				cs.execute();
//				ResultSet rs = (ResultSet) cs.getObject(5);
//
//				if (rs != null) {
//					//logger.traceInfo("obtenerOrdenesParaGeneracion", "Query: " + cs.getString(4));
//					while (rs.next()) {
//						NotaVenta notaVenta = new NotaVenta();
//						notaVenta.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_VENTA")));
//						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
//						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
//						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
//						notaVenta.setMontoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("MONTO_VENTA")));
//						notaVenta.setMontoDescuento(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("MONTO_DESCUENTO")));
//						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
//						notaVenta.setRutComprador(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_COMPRADOR")));
//						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
//						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
//						notaVenta.setCodigoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_REGALO")));
//						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
//						notaVenta.setNumeroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_BOLETA")));
//						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
//						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
//						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
//						notaVenta.setCorrelativoBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_BOLETA")));
//						notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
//						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
//						notaVenta.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_NOTA_CREDITO")));
//						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
//						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
//						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
//						notaVenta.setCorrelativoNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_NOTA_CREDITO")));
//						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
//						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
//						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
//						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
//						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
//						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
//						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
//						notaVenta.setFolioNcSii(Validaciones.validaIntegerCeroXNull(rs.getInt("FOLIO_NC_SII")));
//						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
//						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
//						notaVenta.setUsuario(rs.getString("USUARIO"));
//						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//
//						TarjetaRipley tarjetaRipley = new TarjetaRipley();
//						tarjetaRipley.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("TR_CORRELATIVO_VENTA")));
//						tarjetaRipley.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("ADMINISTRADORA")));
//						tarjetaRipley.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("EMISOR")));
//						tarjetaRipley.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TARJETA")));
//						tarjetaRipley.setRutTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_TITULAR")));
//						tarjetaRipley.setRutPoder(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_PODER")));
//						tarjetaRipley.setPlazo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("PLAZO")));
//						tarjetaRipley.setDiferido(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DIFERIDO")));
//						tarjetaRipley.setMontoCapital(Validaciones.validaIntegerCeroXNull(rs.getInt("MONTO_CAPITAL")));
//						tarjetaRipley.setMontoPie(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("MONTO_PIE")));
//						tarjetaRipley.setDescuentoCar(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESCUENTO_CAR")));
//						tarjetaRipley.setCodigoDescuento(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("CODIGO_DESCUENTO")));
//						tarjetaRipley.setPrefijoTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_TITULAR")));
//						tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
//						tarjetaRipley.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CLIENTE")));
//						tarjetaRipley.setTipoCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CREDITO")));
//						tarjetaRipley.setPrefijoPoder(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_PODER")));
//						tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
//						tarjetaRipley.setValorCuota(Validaciones.validaIntegerCeroXNull(rs.getInt("VALOR_CUOTA")));
//						tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
//						tarjetaRipley.setSernacFinanCae(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CAE")));
//						tarjetaRipley.setSernacFinanCt(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CT")));
//						tarjetaRipley.setTvtriMntMntFnd(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_FND")));
//						tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
//						tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
//						tarjetaRipley.setTvtriCodImpTsaEfe(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_COD_IMP_TSA_EFE")));
//						tarjetaRipley.setTvtriMntMntEva(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_EVA")));
//						tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
//						tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
//						tarjetaRipley.setCodigoCore(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CORE")));
//						tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));
//
//						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
//						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("TB_CORRELATIVO_VENTA")));
//						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TIPO_TARJETA")));
//						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
//						tarjetaBancaria.setMontoTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("MONTO_TARJETA")));
//						tarjetaBancaria.setVd(rs.getString("VD"));
//						tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
//						tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
//						tarjetaBancaria.setCodigoConvenio(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));
//
//						TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
//						tarjetaRegaloEmpresa.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_CORRELATIVO_VENTA")));
//						tarjetaRegaloEmpresa.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_ADMINISTRADORA")));
//						tarjetaRegaloEmpresa.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_EMISOR")));
//						tarjetaRegaloEmpresa.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_TARJETA")));
//						tarjetaRegaloEmpresa.setCodigoAutorizador(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TRE_COD_AUTORIZADOR")));
//						tarjetaRegaloEmpresa.setRutCliente(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_CLIENTE")));
//						tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
//						tarjetaRegaloEmpresa.setMonto(Validaciones.validaIntegerCeroXNull(rs.getInt("MONTO")));
//						tarjetaRegaloEmpresa.setNroTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_TARJETA")));
//						tarjetaRegaloEmpresa.setFlag(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("FLAG")));
//
//						DatoFacturacion datoFacturacion = new DatoFacturacion();
//						datoFacturacion.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("DF_CORRELATIVO_VENTA")));
//						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
//						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
//						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
//						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
//						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
//						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
//						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
//						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
//						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
//						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
//						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));
//
//						Despacho despacho = new Despacho();
//						despacho.setCorrelativoVenta(Validaciones.validaIntegerCeroXNull(rs.getInt("D_CORRELATIVO_VENTA")));
//						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
//						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
//						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
//						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
//						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
//						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
//						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
//						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
//						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
//						despacho.setObservacion(rs.getString("OBSERVACION"));
//						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
//						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
//						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
//						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
//						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
//						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
//						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
//						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
//						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
//						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
//						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
//						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
//						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
//						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
//						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
//						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
//						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
//						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
//
//						TipoDoc tipoDoc = new TipoDoc();
//						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
//						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
//						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
//						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
//						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
//						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));
//
//						Vendedor vendedor = new Vendedor();
//						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
//						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));
//
//						List<ArticuloVentaTramaDTE> articuloVentas = obtenerArticuloVentas(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_VENTA")));
//						
//						List<IdentificadorMarketplace> identificadoresMarketplace = obtenerIdentificadorMarketplace(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_VENTA")));
//
//						TramaDTE tramaDTE = new TramaDTE();
//						tramaDTE.setNotaVenta(notaVenta);
//						tramaDTE.setTarjetaRipley(tarjetaRipley);
//						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
//						tramaDTE.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
//						tramaDTE.setDatoFacturacion(datoFacturacion);
//						tramaDTE.setDespacho(despacho);
//						tramaDTE.setTipoDoc(tipoDoc);
//						tramaDTE.setArticuloVentas(articuloVentas);
//						tramaDTE.setIdentificadoresMarketplace(identificadoresMarketplace);
//						listado.add(tramaDTE);
//					}
//				}
//				cs.close();
//			} catch (Exception e) {
//				logger.traceError("obtenerOrdenesParaGeneracion", "Error en obtenerOrdenesParaGeneracion: ", e);
//				logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: null");
//				return null;
//			} finally {
//				PoolBDs.closeConnection(conn);
//			}
//		} else {
//			logger.traceInfo("obtenerOrdenesParaGeneracion", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
//			logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: AligareException");
//			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
//		}
		logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado",
				"ConcurrentLinkedQueue<TramaDTE>.size: " + ((listado != null) ? listado.size() : 0));
		return listado;
	}

	@Override
	public List<ArticuloVentaTramaDTE> obtenerArticuloVentas(Long correlativoVenta) {

		logger.initTrace("obtenerArticuloVentas", "Integer correlativoVenta: " + correlativoVenta, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));

		ArrayList<ArticuloVentaTramaDTE> listado = new ArrayList<ArticuloVentaTramaDTE>();

		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);

		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_ARTICULOS(?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.CLOB);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerArticuloVentas", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_ARTICULOS("+correlativoVenta+",outc,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(3);
				
				if (rs != null) {
					//logger.traceInfo("obtenerArticuloVentas", "Query: " +  cs.getString(2));
					while (rs.next()) {
						ArticuloVenta articuloVenta = new ArticuloVenta();
						articuloVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						articuloVenta.setCorrelativoItem(Validaciones.validaIntegerCeroXNull(rs.getInt("CORRELATIVO_ITEM")));
						articuloVenta.setCodArticulo(rs.getString("COD_ARTICULO"));
						articuloVenta.setDescRipley(rs.getString("DESC_RIPLEY"));
						articuloVenta.setPrecio(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("PRECIO")));
						articuloVenta.setUnidades(Validaciones.validaIntegerCeroXNull(rs.getInt("UNIDADES")));
						articuloVenta.setColor(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("COLOR")));
						articuloVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						articuloVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						articuloVenta.setCodDespacho(rs.getString("COD_DESPACHO"));
						articuloVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						articuloVenta.setEsregalo(rs.getString("ESREGALO"));
						articuloVenta.setMensaje(rs.getString("MENSAJE"));
						articuloVenta.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_NOTA_CREDITO")));
						articuloVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						articuloVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						articuloVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						articuloVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						articuloVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						articuloVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						articuloVenta.setIndicadorEg(Validaciones.validaIntegerCeroXNull(rs.getInt("INDICADOR_EG")));
						articuloVenta.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
						articuloVenta.setFechaDespacho(rs.getDate("FECHA_DESPACHO"));
						articuloVenta.setCodBodega(rs.getString("COD_BODEGA"));
						articuloVenta.setTipoPapelRegalo(rs.getString("TIPO_PAPEL_REGALO"));
						articuloVenta.setDespachoId(Validaciones.validaLongCeroXNull(rs.getLong("DESPACHO_ID")));
						articuloVenta.setEstadoVenta(rs.getString("ESTADO_VENTA"));
						articuloVenta.setOrdenMkp(rs.getString("ORDEN_MKP"));
						articuloVenta.setDepto(rs.getString("DEPTO"));
						articuloVenta.setDeptoGlosa(rs.getString("DEPTO_GLOSA"));
						articuloVenta.setCosto(Validaciones.validaIntegerCeroXNull(rs.getInt("COSTO")));
						articuloVenta.setEsNC(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ES_NC")));
						articuloVenta.setCodArticuloMkp(rs.getString("COD_ART_MKP"));
						articuloVenta.setBodegaStock(rs.getString("BODEGA_STOCK"));
						articuloVenta.setCodigoSunat(rs.getString("CODIGO_SUNAT"));
						articuloVenta.setDireccionEntrega(rs.getString("DIRECCION_ENTREGA"));
						articuloVenta.setHorarioEntrega(rs.getString("HORARIO_ENTREGA"));
						articuloVenta.setUrlGoogleEntrega(rs.getString("URLGOOGLE_ENTREGA"));

						IdentificadorMarketplace identificadorMarketplace = new IdentificadorMarketplace();
						identificadorMarketplace.setOrdenMkp(rs.getString("MKP_ORDEN_MKP"));
						identificadorMarketplace.setRutProveedor(rs.getString("RUT_PROVEEDOR"));
						identificadorMarketplace.setEstado(Validaciones.validaIntegerCeroXNull(rs.getInt("ESTADO")));
						identificadorMarketplace.setEsMkp(Validaciones.validaIntegerMenosUnoXNullaCERO(rs.getInt("ES_MKP")));
						identificadorMarketplace.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						identificadorMarketplace.setNroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_BOLETA")));
						identificadorMarketplace.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						identificadorMarketplace.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						identificadorMarketplace.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NOTA_CREDITO")));
						identificadorMarketplace.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NUM_CAJA_NOTA_CREDITO")));
						identificadorMarketplace.setFecNotaCredito(rs.getTimestamp("MKP_FEC_NOTA_CREDITO"));
						identificadorMarketplace.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						identificadorMarketplace.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						identificadorMarketplace.setRazonSocial(rs.getString("RAZON_SOCIAL"));
						
						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(rs.getString("COMUNA_FACTURA_DES"));
						datoFacturacion.setCiudadFacturaDes(rs.getString("CIUDAD_FACTURA_DES"));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));
						
						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("D_FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("DES_TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
						despacho.setDespachoId(Validaciones.validaLongCeroXNull(rs.getLong("D_DESPACHO_ID")));

						ArticuloVentaTramaDTE articuloVentaTramaDTE = new ArticuloVentaTramaDTE();
						articuloVentaTramaDTE.setArticuloVenta(articuloVenta);
						articuloVentaTramaDTE.setIdentificadorMarketplace(identificadorMarketplace);
						articuloVentaTramaDTE.setDespacho(despacho);
						articuloVentaTramaDTE.setDatoFacturacion(datoFacturacion);

						listado.add(articuloVentaTramaDTE);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerArticuloVentas", "Error en obtenerArticuloVentas: ", e);
				logger.endTrace("obtenerArticuloVentas", "Finalizado", "ArrayList<ArticuloVentaTramaDTE>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerArticuloVentas", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerArticuloVentas", "Finalizado", "ArrayList<ArticuloVentaTramaDTE>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerArticuloVentas", "Finalizado",
				"ArrayList<ArticuloVentaTramaDTE>.size: " + ((listado != null) ? listado.size() : 0));

		return listado;
	}

	@Override
	public TVirtualCodBancaria getCodigoConvenioXTipo(int codigoTipoInterno) {
		logger.initTrace("getCodigoConvenioXTipo", "int codigoTipoInterno: " + codigoTipoInterno);
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		TVirtualCodBancaria codigoConvenioXTipo = new TVirtualCodBancaria();
		if (conn != null) {
			String selectSQL = "";
			try {
				selectSQL = "SELECT " + "  COD_BOLETA, COD_INTER, COD_BACK, DESCRIPCION " + "FROM "
						+ "  TVIRTUAL_COD_BANCARIA " + "WHERE " + "  COD_INTER = ?";
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(selectSQL);
				if (ps != null) {
					ps.setInt(1, codigoTipoInterno);

					rs = ps.executeQuery();
					logger.traceInfo("getCodigoConvenioXTipo", "Ejecuci�n de query OK!");

					if (rs != null && rs.next()) {
						codigoConvenioXTipo.setCodBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BOLETA")));
						codigoConvenioXTipo.setCodInter(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_INTER")));
						codigoConvenioXTipo.setCodBack(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BACK")));
						codigoConvenioXTipo.setDescripcion(rs.getString("DESCRIPCION"));
						rs.close();
					}
					ps.close();
				}

			} catch (Exception e) {
				logger.traceError("getCodigoConvenioXTipo", "Error durante la ejecuci�n de totalVentasBancCierre: ", e);
				logger.endTrace("getCodigoConvenioXTipo", "Finalizado", "TVirtualCodBancaria: Null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("getCodigoConvenioXTipo", "Problemas de conexion a la base de datos BACK OFFICE");
			logger.endTrace("getCodigoConvenioXTipo", "Finalizado", "TVirtualCodBancaria: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("getCodigoConvenioXTipo", "Finalizado", "TVirtualCodBancaria: "+codigoConvenioXTipo.toString());
		return codigoConvenioXTipo;
	}

	@Override
	public boolean creacionTransaccionesDte(Parametros parametros, TramaDTE trama, Integer nroTransaccion, Integer nroTransaccionAnterior,
			String codUnico, String boleta,
			boolean isRipleyProcessed, boolean isMkpProcessed) throws AligareException{
		logger.initTrace("creacionTransaccionesDteB",
				"Parametros parametros: " + parametros.toString() + " - TramaDTE: trama - Integer nroTransaccion: " + nroTransaccion
						+ " - Integer nroTransaccionAnterior: " + nroTransaccionAnterior + 
						" - String codUnico: " + codUnico + " - Integer boleta: " + boleta,
				new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO),
				new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO),
				new KeyLog("codUnico", codUnico));
//		int tipoDePago = obtieneTipoDePago(trama);
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		String formaPago = Constantes.VACIO;
		String fechaDesp = Constantes.VACIO;
		String fecha = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros)/*FechaUtil.getCurrentDateString()*/;
		String hms = caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros)/*FechaUtil.getCurrentHoursString()*/;
		String vendedor = parametros.buscaValorPorNombre("RUT_VENDEDOR");
		String supervisor = parametros.buscaValorPorNombre("RUT_SUPERVISOR");
		String plTipoPago = parametros.buscaValorPorNombre("PL_TIPO_PAGO");
		//String fechaDespachoPreVenta = parametros.buscaValorPorNombre("FECHA_DESPACHO_PREVENTA");
		//boolean actFechPreVenta = Boolean.parseBoolean(parametros.buscaValorPorNombre("ACT_FECH_PREVENTA"));
		boolean paramFechaDespacho = Boolean.parseBoolean(parametros.buscaValorPorNombre("PARAM_FECHA_DESPACHO"));
		int sucursal = Integer.parseInt(parametros.buscaValorPorNombre("SUCURSAL"));
		int nroCaja = Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA"));
//		int funcion = Integer.parseInt(parametros.buscaValorPorNombre("FUNCION"));
		Long correlativo = trama.getNotaVenta().getCorrelativoVenta();
		//String corrVenta = trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO;
		String wsCodAuto = Constantes.VACIO;
		//Integer paramCFTValor = new Integer(parametros.buscaValorPorNombre("PARAM_CFT"));
		int tipo = Constantes.NRO_UNO;
		int tipoReg = Constantes.NRO_CERO;
		String servicioNCA = Constantes.VACIO;
		boolean resultado = Constantes.VERDADERO;
		String query1 = Constantes.VACIO;
		String query2 = Constantes.VACIO;
		String query3 = Constantes.VACIO;
		String query4 = Constantes.VACIO;
		String query5 = Constantes.VACIO;
		CallableStatement cs = null;
		Connection conn1 = null;
		Connection conn2 = null;
		Connection conn3 = null;
		Connection conn4 = null;
		Connection conn5 = null;
		Integer sal = null;
		String sal2 = Constantes.VACIO;
		String rutFacturaBo = Constantes.VACIO;
		String giroFacturaBoDes = Constantes.VACIO;
		String razonSocialFacturaBoDes = Constantes.VACIO;
		String rutFacturaBoDes = Constantes.VACIO;
		BigDecimal totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
		try {
			if (Validaciones.validaInteger(trama.getTipoDoc().getCodPpl()) && trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TREINTANUEVE) {
				String rutClienteBO = ((Validaciones.validaInteger(trama.getDespacho().getRutCliente()))
						? trama.getDespacho().getRutCliente().toString() : Constantes.VACIO);
				String nomClienteBO = (((trama.getDespacho().getNombreCliente() != null)
						? trama.getDespacho().getNombreCliente() : Constantes.VACIO)
						+ Constantes.SPACE
						+ ((trama.getDespacho().getApellidoPatCliente() != null)
								? trama.getDespacho().getApellidoPatCliente() : Constantes.VACIO)
						+ Constantes.SPACE + ((trama.getDespacho().getApellidoMatCliente() != null)
								? trama.getDespacho().getApellidoMatCliente() : Constantes.VACIO));
				nomClienteBO = Util.rellenarString(nomClienteBO, 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49);
				String dirClienteBO = ((trama.getDespacho().getDireccionCliente() != null)
						? trama.getDespacho().getDireccionCliente() : Constantes.VACIO);
				String comunaBO = ((trama.getDespacho().getComunaDespachoDes() != null)
						? trama.getDespacho().getComunaDespachoDes() : Constantes.VACIO);
				String rutClienteBOFin = Util.rellenarString(rutClienteBO, Constantes.NRO_DIEZ, Constantes.VERDADERO,
						Constantes.CHAR_CERO);
				String nomClienteBOFin = Util.rellenarString(Util.cambiaCaracteres(nomClienteBO), 51, Constantes.FALSO,
						Constantes.CHAR_SPACE).substring(0, 50);
				String dirClienteBOFin = Util.rellenarString(Util.cambiaCaracteres(dirClienteBO), 90, Constantes.FALSO,
						Constantes.CHAR_SPACE).substring(0, 89);
				String comunaBOFin = Util
						.rellenarString(Util.cambiaCaracteres(comunaBO), 30, Constantes.FALSO, Constantes.CHAR_SPACE)
						.substring(0, 29);

				servicioNCA = rutClienteBOFin + nomClienteBOFin + dirClienteBOFin + comunaBOFin;
			} else {
				/*BPL Se cambia index de los String => rutFacturaBo + razonSocialFacturaBo + direccionFacturaBo + comunaFacturaBo
				 + ciudadFacturaBo + telefonoFacturaBo + giroFacturaBo - Para poder formar los 252 caracteres que requiere las NC en POS*/
				servicioNCA = Util.rellenarString(Constantes.VACIO, 252, Constantes.FALSO, Constantes.CHAR_SPACE);

				rutFacturaBoDes = (trama.getDatoFacturacion().getRutFactura() != null)?trama.getDatoFacturacion().getRutFactura().trim():Constantes.VACIO;
				
				rutFacturaBo = ((trama.getDatoFacturacion()
						.getRutFactura() != null)
								? Util.rellenarString(
										trama.getDatoFacturacion().getRutFactura().substring(0,
												(trama.getDatoFacturacion().getRutFactura().length()
														- Constantes.NRO_UNO)),
										Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO)
								: Constantes.VACIO);
				String razonSocialFacturaBo = Util.rellenarString(
						Util.cambiaCaracteres((trama.getDatoFacturacion().getRazonSocialFactura() != null)
								? trama.getDatoFacturacion().getRazonSocialFactura() : Constantes.VACIO),
						51, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 51);
				String direccionFacturaBo = Util.rellenarString(((trama.getDatoFacturacion().getDireccionFacturaDes() != null)
						? trama.getDatoFacturacion().getDireccionFacturaDes() : Constantes.VACIO),
                        90, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 90);
				String comunaFacturaBo = Util.rellenarString(
						Util.cambiaCaracteres((trama.getDatoFacturacion().getComunaFacturaDes() != null)
								? trama.getDatoFacturacion().getComunaFacturaDes() : Constantes.VACIO),
						30, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 30);
				String ciudadFacturaBo = Util.rellenarString(
						((trama.getDatoFacturacion().getCiudadFacturaDes() != null)
								? trama.getDatoFacturacion().getCiudadFacturaDes() : Constantes.VACIO),
						20, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 20);
				String telefonoFacturaBo = Util.rellenarString(
						Util.cambiaCaracteres(((trama.getDatoFacturacion().getTelefonoFactura() != null)
								? trama.getDatoFacturacion().getTelefonoFactura() : Constantes.VACIO)),
						16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 16);
				String giroFacturaBo = Util.rellenarString(
						((trama.getDatoFacturacion().getGiroFacturaDes() != null)
								? trama.getDatoFacturacion().getGiroFacturaDes() : Constantes.VACIO),
						35, Constantes.VERDADERO, Constantes.CHAR_SPACE).substring(0, 35);

				giroFacturaBoDes = ((trama.getDatoFacturacion().getGiroFacturaDes() != null)?trama.getDatoFacturacion().getGiroFacturaDes():Constantes.VACIO);
				razonSocialFacturaBoDes = ((trama.getDatoFacturacion().getRazonSocialFactura() != null)?trama.getDatoFacturacion().getRazonSocialFactura():Constantes.VACIO);
				
				servicioNCA = rutFacturaBo + razonSocialFacturaBo + direccionFacturaBo + comunaFacturaBo
						+ ciudadFacturaBo + telefonoFacturaBo + giroFacturaBo;
			}

			Long rutTransbank = trama.getNotaVenta().getRutComprador();
			String rutEjecutivoVta = ((trama.getNotaVenta().getEjecutivoVta() != null
					&& !Constantes.VACIO.equals(trama.getNotaVenta().getEjecutivoVta().trim()))
							? trama.getNotaVenta().getEjecutivoVta() : Constantes.STRING_CERO);
			
			
			String tipoIdent = trama.getNotaVenta().getIndicadorMkp();
			if(Constantes.STRING_DOS.equals(tipoIdent)){
				if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
					totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
					for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
						totalVentaMkp = totalVentaMkp.add(((Validaciones.validaBigDecimal(obj.getMontoVenta())&&obj.getEsMkp()==0)?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO)));
					}
				}
				logger.traceInfo("creacionTransaccionesDteB", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
			}
			
//			if (Validaciones.validaLong(trama.getTarjetaBancaria().getCorrelativoVenta())) {
//				formaPago = Constantes.BOL_FORMA_PAGO_TB;
//				tipoReg = Constantes.NRO_CUATRO;
//				Integer WsTipo = trama.getTarjetaBancaria().getTipoTarjeta();
//				BigDecimal total = trama.getTarjetaBancaria().getMontoTarjeta();
//				wsCodAuto = trama.getTarjetaBancaria().getCodigoAutorizador();
//				String vd = ((trama.getTarjetaBancaria().getVd() != null) ? trama.getTarjetaBancaria().getVd()
//						: Constantes.BOL_VALOR_VD_N);
//				if ((!Constantes.STRING_UNO.equals(plTipoPago) || Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)) 
//						&& (!Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso()))){ // TARJETA BANCARIA DEBITO
//					query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'',?,?,0,?,0,0,0,?,?,?,?)}";
//					logger.traceInfo("creacionTransaccionesDteB", "TARJETA BANCARIA DEBITO["+formaPago+"] SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'',"+((Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd))?Constantes.BOL_VALOR_VD_D:Constantes.BOL_VALOR_VD_C)+","+rutTransbank+",0,"+((Constantes.STRING_DOS.equals(tipoIdent))?totalVentaMkp:total)+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.setString(1, fecha);
//							cs.setInt(2, sucursal);
//							cs.setInt(3, nroCaja);
//							cs.setInt(4, nroTransaccion);
//							cs.setString(5, (Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)) ? Constantes.BOL_VALOR_VD_D
//									: Constantes.BOL_VALOR_VD_C);
//							cs.setInt(6, rutTransbank);
//							
//							if (Constantes.STRING_DOS.equals(tipoIdent)) {
//								cs.setBigDecimal(7, totalVentaMkp);
//							} else {
//								cs.setBigDecimal(7, total);
//							}
//							
//							cs.setString(8, wsCodAuto);
//							cs.setString(9, codUnico);
//							cs.registerOutParameter(10, OracleTypes.NUMBER);
//							cs.registerOutParameter(11, OracleTypes.VARCHAR);
//							cs.execute();
//							sal = new Integer(cs.getInt(10));
//							sal2 = cs.getString(11);
//							cs.close();
//						} catch (Exception e) {
//							logger.traceError("creacionTransaccionesDteB",
//									"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
//							
//							resultado = Constantes.FALSO; 
//						}
//					} else {
//						logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//				} else if (Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())) { // TARJETA MERCADO PAGO
//					query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'MP',?,?,0,?,0,0,0,?,?,?,?)}";
//					logger.traceInfo("creacionTransaccionesDteB", "TARJETA MERCADO PAGO["+formaPago+"] SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'MP',"+Constantes.BOL_VALOR_VD_C+","+rutTransbank+",0,"+total+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.setString(1, fecha);
//							cs.setInt(2, sucursal);
//							cs.setInt(3, nroCaja);
//							cs.setInt(4, nroTransaccion);
//							cs.setString(5, Constantes.BOL_VALOR_VD_C);
//							cs.setInt(6, rutTransbank);
//							if (Constantes.STRING_DOS.equals(tipoIdent)) {
//								cs.setBigDecimal(7, totalVentaMkp);
//							} else {
//								cs.setBigDecimal(7, total);
//							}
//							cs.setString(8, wsCodAuto);
//							cs.setString(9, codUnico);
//							cs.registerOutParameter(10, OracleTypes.NUMBER);
//							cs.registerOutParameter(11, OracleTypes.VARCHAR);
//							cs.execute();
//							sal = new Integer(cs.getInt(10));
//							sal2 = cs.getString(11);
//							cs.close();
//						} catch (Exception e) {
//							logger.traceError("creacionTransaccionesDteB",
//									"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
//							
//							resultado = Constantes.FALSO; 
//						}
//					} else {
//						logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//				} else { // TARJETA BANCARIA CREDITO
//					query1 = "{CALL ? := INSERTA_BANCARIA_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?)}";
//					logger.traceInfo("creacionTransaccionesDteB", "TARJETA BANCARIA CREDITO["+formaPago+"] FN[BACK_OFFICE]: outCode = INSERTA_BANCARIA_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+WsTipo+","+total+","+wsCodAuto+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.registerOutParameter(1, OracleTypes.NUMBER);
//							cs.setString(2, fecha);
//							cs.setInt(3, sucursal);
//							cs.setInt(4, nroCaja);
//							cs.setInt(5, nroTransaccion);
//							cs.setInt(6, WsTipo);
//							if (Constantes.STRING_DOS.equals(tipoIdent)) {
//								cs.setBigDecimal(7, totalVentaMkp);
//							} else {
//								cs.setBigDecimal(7, total);
//							}
//							cs.setString(8, wsCodAuto);
//							cs.executeUpdate();
//							sal = new Integer(cs.getInt(1));
//							logger.traceInfo("creacionTransaccionesDteB", "TARJETA BANCARIA CREDITO FN INSERTA_BANCARIA_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//							cs.close();
//						} catch (Exception e) {
//							logger.traceError("creacionTransaccionesDteB",
//									"Se produjo un error en la ejecucion de la FN INSERTA_BANCARIA_BACK: ", e);
//							
//							resultado = Constantes.FALSO; 
//						}
//					} else {
//						logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//				} 
//
//				if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//					// tipo = Constantes.NRO_CERO;
//					// tipoReg = Constantes.NRO_CERO;
//					logger.traceError("creacionTransaccionesDteB",
//							"Imposible crear forma de Pago Bancaria Error - ", new AligareException("outCode: "+sal+" outMsj: "+sal2));
//					
//					resultado = Constantes.FALSO; 
//				}
//			}
			
//			totalVentaMkp = 0l; //Nuevo 8.0.12
					

//			if (Validaciones.validaLong(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())){
//				formaPago = Constantes.BOL_FORMA_PAGO_TRE;
//				Integer emisorTRE = trama.getTarjetaRegaloEmpresa().getEmisor();
//				Integer admTRE = trama.getTarjetaRegaloEmpresa().getAdministradora();
//				Integer tarjetaTRE = trama.getTarjetaRegaloEmpresa().getTarjeta();
//				Integer codAutorizadorTRE = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();
//				//Integer rutClienteTRE = trama.getTarjetaRegaloEmpresa().getRutCliente();
//				//String dvClienteTRE = trama.getTarjetaRegaloEmpresa().getDvCliente();
//				Long correVenta = trama.getTarjetaRegaloEmpresa().getCorrelativoVenta();
//				Long montoTRE = 0l;
//				if (Constantes.STRING_DOS.equals(tipoIdent)) {//Nuevo 8.0.12
//					 montoTRE = totalVentaMkp;
//				}else{
//					 montoTRE = trama.getTarjetaRegaloEmpresa().getMonto().longValue();
//				}
//				
//				Integer nroTarjetaTRE = trama.getTarjetaRegaloEmpresa().getNroTarjeta();
//				long rutCliente = (Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getRutCliente()))?trama.getTarjetaRegaloEmpresa().getRutCliente().longValue():Constantes.NRO_CERO;
//				rutCliente = Long.parseLong((rutCliente + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente))));
//				Integer flagTRE = trama.getTarjetaRegaloEmpresa().getFlag();
//				tipo = Constantes.NRO_UNO;
//				tipoReg = Constantes.NRO_OCHO;
//				String WFECHA = fecha.replaceAll(Constantes.SLASH, Constantes.VACIO);
//				query2 = "{CALL FEGIFE_PRC_INS_TREINT(?,'IB',?,?,?,?,?,?)}";
//				logger.traceInfo("creacionTransaccionesDteB", "TARJETA REGALO EMPRESA["+formaPago+"] SP[TRE]: FEGIFE_PRC_INS_TREINT("+nroTarjetaTRE+",'IB',"+(montoTRE.intValue() * Constantes.NRO_MENOSUNO)+","+correVenta+","+nroCaja+","+WFECHA+",outMsj,outCode)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				conn2 = PoolBDs.getConnection(BDType.TRE);
//				if (conn2 != null) {
//					try {
//						cs = conn2.prepareCall(query2);
//						cs.setInt(1, nroTarjetaTRE);
//						cs.setLong(2, (montoTRE.longValue()) * Constantes.NRO_MENOSUNO); 
//						cs.setInt(3, correVenta);
//						cs.setInt(4, nroCaja);
//						cs.setString(5, WFECHA);
//						cs.registerOutParameter(6, OracleTypes.VARCHAR);
//						cs.registerOutParameter(7, OracleTypes.NUMBER);
//						cs.execute();
//						sal = new Integer(cs.getInt(7));
//						sal2 = cs.getString(6);
//						cs.close();
//						
//						if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//							logger.traceError("creacionTransaccionesDteB",
//									"Imposible Registrar forma de Pago TARJETA REGALO EMPRESA Error - ", new AligareException("outCode: "+sal+" outMsj: "+sal2));
//							
//							resultado = Constantes.FALSO; 
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionTransaccionesDteB",
//								"Se produjo un error en la ejecucion del SP FEGIFE_PRC_INS_TREINT: ", e);
//						
//						resultado = Constantes.FALSO; 
//					}
//				} else {
//					logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos TRE");
//					throw new AligareException("Error: Problemas de conexion a la base de datos TRE");
//				}
//
//				query1 = "{CALL ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
//				logger.traceInfo("creacionTransaccionesDteB", "TARJETA REGALO EMPRESA BO["+formaPago+"] SP[BACK_OFFICE]: ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+admTRE+","+emisorTRE+","+tarjetaTRE+","+codAutorizadorTRE+","+rutCliente+","+montoTRE+","+nroTarjetaTRE+","+flagTRE+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if (conn1 == null) {
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//				}
//				if (conn1 != null) {
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.setString(1, fecha);
//						cs.setInt(2, sucursal);
//						cs.setInt(3, nroCaja);
//						cs.setInt(4, nroTransaccion);
//						cs.setInt(5, admTRE);
//						cs.setInt(6, emisorTRE);
//						cs.setInt(7, tarjetaTRE);
//						cs.setInt(8, codAutorizadorTRE);
//						cs.setLong(9, rutCliente);
//						cs.setLong(10, montoTRE);
//						cs.setInt(11, nroTarjetaTRE);
//						cs.setInt(12, flagTRE);
//						cs.registerOutParameter(13, OracleTypes.NUMBER);
//						cs.registerOutParameter(14, OracleTypes.VARCHAR);
//						cs.execute();
//						sal = new Integer(cs.getInt(13));
//						sal2 = cs.getString(14);
//						cs.close();
//						
//						if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//							// tipo = Constantes.NRO_CERO;
//							// tipoReg = Constantes.NRO_CERO;
//							logger.traceError("creacionTransaccionesDteB",
//									"Imposible crear forma de Pago TARJETA REGALO EMPRESA BO Error - ", new AligareException("outCode: "+sal+" outMsj: "+sal2));
//							
//							resultado = Constantes.FALSO; 
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionTransaccionesDteB",
//								"Se produjo un error en la ejecucion del SP ACTUALIZA_TAR_REGALO_EMPRESA: ", e);
//						
//						resultado = Constantes.FALSO; 
//					}
//				}else{
//					logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//					throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//				}
//				
//			}
            
			// Creacion Tarjeta Ripley
			BigInteger codAutor = null;
			//BigInteger tipoCodAuto = null;
//			if (Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())	) {
////					&& !Constantes.STRING_DOS.equals(trama.getNotaVenta().getIndicadorMkp())) {
//				BigInteger pan = trama.getTarjetaRipley().getPan();
//				if (Constantes.VACIO.equals(formaPago)) {
//					formaPago = Constantes.BOL_FORMA_PAGO_CAR;
//				} else {
//					formaPago = formaPago + Constantes.GUION + Constantes.BOL_FORMA_PAGO_CAR;
//				}
//	
//				tipo = Constantes.NRO_DOS;
//				tipoReg = Constantes.NRO_TRES;
//				BigDecimal total= new BigDecimal(Constantes.NRO_CERO);
//				if (Constantes.STRING_DOS.equals(tipoIdent)) {//Nuevo 8.0.12
//					total = totalVentaMkp;
//				}else{
//					total = trama.getTarjetaRipley().getMontoCapital();//Nuevo 8.0.12
//				}
//				
//				
//				codAutor = (trama.getTarjetaRipley().getCodigoAutorizacion() != null)?trama.getTarjetaRipley().getCodigoAutorizacion():new BigInteger(Constantes.STRING_CERO);
//				query1 = "{CALL ? := INSERTA_RIPLEY_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,?)}";
//				logger.traceInfo("creacionTransaccionesDteB", "Creacion T.Ripley BackOffice["+formaPago+"] FN[BACK_OFFICE]: outCode = INSERTA_RIPLEY_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+trama.getTarjetaRipley().getAdministradora()+","+trama.getTarjetaRipley().getEmisor()+","+trama.getTarjetaRipley().getTarjeta()+","+trama.getTarjetaRipley().getRutTitular()+","+((trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO)+","+trama.getTarjetaRipley().getPlazo()+","+((trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO)+","+trama.getTarjetaRipley().getMontoCapital()+","+((trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie():Constantes.NRO_CERO)+","+((trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar():Constantes.NRO_CERO)+
//						","+((trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO)+","+((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)?new BigDecimal(codAutor):new BigDecimal((((Validaciones.validaBigInteger(codAutor))?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2)))+",0,"+((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)?(pan != null)?new BigDecimal(Util.rellenarString(pan.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null:(pan != null)?new BigDecimal(pan.toString()):null)+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if (conn1 == null) {
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//				}
//				if (conn1 != null) {
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setInt(3, sucursal);
//						cs.setInt(4, nroCaja);
//						cs.setInt(5, nroTransaccion);
//						cs.setInt(6, trama.getTarjetaRipley().getAdministradora());
//						cs.setInt(7, trama.getTarjetaRipley().getEmisor());
//						cs.setInt(8, trama.getTarjetaRipley().getTarjeta());
//						cs.setInt(9, trama.getTarjetaRipley().getRutTitular());
//						cs.setInt(10, (trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO);
//						cs.setInt(11, trama.getTarjetaRipley().getPlazo());
//						cs.setInt(12, (trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO);
//						cs.setBigDecimal(13, total);//Nuevo 8.0.12
//						cs.setBigDecimal(14, (trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie(): new BigDecimal(Constantes.NRO_CERO));
//	
//						//tipoCodAuto = (paramCFTValor.intValue() == Constantes.NRO_CERO)?pan:new BigInteger(Constantes.STRING_CERO);
//	
//						cs.setBigDecimal(15, (trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar(): new BigDecimal(Constantes.NRO_CERO));
//						cs.setInt(16, (trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO);
//						cs.setBigDecimal(17,
//								((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
//										&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
//												? new BigDecimal(codAutor)
//												: new BigDecimal((((Validaciones.validaBigInteger(codAutor))?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2))));
//						cs.setBigDecimal(18,
//								((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
//										&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
//												? (pan != null)?new BigDecimal(Util.rellenarString(pan.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null
//												: (pan != null)?new BigDecimal(pan.toString()):null));
//						
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionTransaccionesDteB", "TARJETA RIPLEY FN INSERTA_RIPLEY_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//							// tipo = Constantes.NRO_CERO;
//							// tipoReg = Constantes.NRO_CERO;
//							logger.traceError("creacionTransaccionesDteB",
//									"Imposible crear forma de pago Ripley BO Error - ", new AligareException("outCode: "+sal));
//							
//							resultado = Constantes.FALSO; 
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionTransaccionesDteB",
//								"Se produjo un error en la ejecucion del SP INSERTA_RIPLEY_BACK: ", e);
//						
//						resultado = Constantes.FALSO; 
//					}
//				}else{
//					logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//					throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//				}				
//			}
			
			Integer rutDespachoCambio = null;
			long vendedor2 = 0;
			if (!Validaciones.validaLong(trama.getDespacho().getCorrelativoVenta())) { 
				logger.traceError("creacionTransaccionesDteB",
						"No existe Despacho en forma de pago Ripley BO - Error", new AligareException("Correlativo Venta NULO en Despacho"));
				
				resultado = Constantes.FALSO; 
			} else {
				if (!Validaciones.validaInteger(trama.getDespacho().getRutDespacho())) {
					rutDespachoCambio = trama.getDespacho().getRutCliente();
				} else {
					rutDespachoCambio = ((Constantes.NRO_CERO == trama.getDespacho().getRutDespacho().intValue())
									? trama.getDespacho().getRutCliente() : trama.getDespacho().getRutDespacho());
				}

				//Integer correlativoVenta = trama.getDespacho().getCorrelativoVenta();
				Integer rutDespacho2 = rutDespachoCambio;
				String aux = Util.getDigitoValidadorRut2((Validaciones.validaInteger(rutDespacho2))?rutDespacho2.toString():Constantes.VACIO);
				long rutDespacho = Long.parseLong(
						rutDespacho2 + Constantes.VACIO + ((!Validaciones.validarVacio(aux))?aux:Constantes.STRING_CERO));
				String nombreDespacho = Util.cambiaCaracteres(trama.getDespacho().getNombreDespacho());
				String fechaDespacho = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, trama.getDespacho().getFechaDespacho());
				Integer sucursalDespacho = trama.getDespacho().getSucursalDespacho();
				Integer comunaDespacho = trama.getDespacho().getComunaDespacho();
				Integer regionDespacho = trama.getDespacho().getRegionDespacho();
				String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
				String telefonoDespacho = Util.cambiaCaracteres(trama.getDespacho().getTelefonoDespacho());
				//String observacion = Util.cambiaCaracteres(trama.getDespacho().getObservacion());
				Integer rutCliente2 = trama.getDespacho().getRutCliente();
				aux = Util.getDigitoValidadorRut2((Validaciones.validaInteger(rutCliente2))?rutCliente2.toString():Constantes.VACIO);
				long rutCliente = Long.parseLong(
						rutCliente2 + Constantes.VACIO + ((!Validaciones.validarVacio(aux))?aux:Constantes.STRING_CERO));
				String direccionCliente = Util.cambiaCaracteres(trama.getDespacho().getDireccionCliente());
				String telefonoCliente = trama.getDespacho().getTelefonoCliente();
				//Integer tipoCliente = trama.getDespacho().getTipoCliente();
				String nombreCliente = (((trama.getDespacho().getNombreCliente() != null)
						? trama.getDespacho().getNombreCliente() : Constantes.VACIO)
						+ Constantes.SPACE
						+ ((trama.getDespacho().getApellidoPatCliente() != null)
								? trama.getDespacho().getApellidoPatCliente() : Constantes.VACIO)
						+ Constantes.SPACE
						+ ((trama.getDespacho().getApellidoMatCliente() != null)
								? trama.getDespacho().getApellidoMatCliente() : Constantes.VACIO));
				nombreCliente = Util.cambiaCaracteres(Util.rellenarString(nombreCliente, 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0,49));
				//Integer tipoDespacho = trama.getDespacho().getTipoDespacho();
				String emailCliente = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
				String nombreInvitado = (Validaciones
						.validarVacio(Util.cambiaCaracteres(trama.getDespacho().getNombreCliente())))
								? Constantes.VACIO : Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
				String apellidoPaterno = (Validaciones
						.validarVacio(Util.cambiaCaracteres(trama.getDespacho().getApellidoPatCliente())))
								? Constantes.VACIO
								: Util.cambiaCaracteres(trama.getDespacho().getApellidoPatCliente());
				String apellidoMaterno = (Validaciones
						.validarVacio(Util.cambiaCaracteres(trama.getDespacho().getApellidoMatCliente())))
								? Constantes.VACIO
								: Util.cambiaCaracteres(trama.getDespacho().getApellidoMatCliente());

				Long codigoRegalo = new Long(Constantes.NRO_CERO);
				Integer tipoRegalo = null;
				if (Validaciones.validaInteger(trama.getNotaVenta().getTipoRegalo()) && trama.getNotaVenta().getTipoRegalo().intValue() > Constantes.NRO_CERO) {
					codigoRegalo = (Validaciones.validaLong(trama.getNotaVenta().getCodigoRegalo()) && trama.getNotaVenta().getCodigoRegalo().longValue() < Constantes.NRO_CERO)
							? new Integer((trama.getNotaVenta().getCodigoRegalo().intValue() * Constantes.NRO_MENOSUNO))
							: trama.getNotaVenta().getCodigoRegalo();
					tipoRegalo = trama.getNotaVenta().getTipoRegalo();
				} else {
					codigoRegalo = new Long(Constantes.NRO_CERO);
					tipoRegalo = new Integer(Constantes.NRO_CERO);
				}
				String fechaVenta = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4,
						((trama.getNotaVenta().getFechaCreacion() != null)?trama.getNotaVenta().getFechaCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4)));
				String mensajeTarjeta = trama.getDespacho().getMensajeTarjeta();
				vendedor2 = Long
						.parseLong(vendedor + Constantes.VACIO + Util.getDigitoValidadorRut2(vendedor));
				int contadorNoEsMKP = Constantes.NRO_CERO;
				boolean indMKPAmbas = (!Validaciones.validarVacio(trama.getNotaVenta().getIndicadorMkp()) && Constantes.IND_MKP_AMBAS.equals(trama.getNotaVenta().getIndicadorMkp()));
				if (trama.getArticuloVentas() != null && !trama.getArticuloVentas().isEmpty()) {
					for (ArticuloVentaTramaDTE obj : trama.getArticuloVentas()) {
						if(indMKPAmbas){
							if (Validaciones.validaInteger(obj.getIdentificadorMarketplace().getEsMkp())
									&& obj.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.NRO_CERO) {
								contadorNoEsMKP++;
							} else {
								continue;
							}
						}else{
							contadorNoEsMKP++;
						}
						
						String tipoDespachoComision = (Validaciones
								.validarVacio(obj.getArticuloVenta().getTipoDespacho())) ? Constantes.SI
										: obj.getArticuloVenta().getTipoDespacho();
						//Integer NroDespachoID = (Validaciones.validaInteger(obj.getArticuloVenta().getDespachoId()))?obj.getArticuloVenta().getDespachoId():new Integer(Constantes.NRO_CERO);
						String vArticulo = obj.getArticuloVenta().getCodArticulo();
						String costoEnvio = (obj.getArticuloVenta().getDescRipley() != null)?obj.getArticuloVenta().getDescRipley():Constantes.VACIO;
						String jornadaDespachoBo = null;
						if (Constantes.BOL_JORNADA_AM.equalsIgnoreCase(tipoDespachoComision)) {
							jornadaDespachoBo = Constantes.STRING_UNO;
						} else if (Constantes.BOL_JORNADA_PM.equalsIgnoreCase(tipoDespachoComision)) {
							jornadaDespachoBo = Constantes.STRING_DOS;
						} else {
							jornadaDespachoBo = tipoDespachoComision;
						}

						boolean esExtgarantia = (Validaciones.validaInteger(obj.getArticuloVenta().getIndicadorEg())
								//&& obj.getArticuloVenta().getIndicadorEg().intValue() == Constantes.NRO_TRES);
								&& obj.getArticuloVenta().getIndicadorEg().intValue() > Constantes.NRO_CERO);
						Integer strFechaConfirma = new Integer(Constantes.NRO_UNO);
						if (obj.getArticuloVenta().getFechaDespacho() != null
								&& costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) == Constantes.NRO_CERO) {
							strFechaConfirma = (Validaciones.validarVacio(
									FechaUtil.formatDate(Constantes.FECHA_DDMMYYYY3, obj.getArticuloVenta().getFechaDespacho())))
											? new Integer(Constantes.NRO_CERO) : new Integer(Constantes.NRO_UNO);
						}

						if (strFechaConfirma.intValue() != Constantes.NRO_UNO) {
							Integer dias = new Integer(Constantes.NRO_CERO);
							//String fechaDespachoAux = fecha;
							
//							if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
//								&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
////							if((!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
////									&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2)))){ // TODO VALIDAR CONDICION
//								if(!esExtgarantia){
//									String vFechaBT = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, obj.getArticuloVenta().getFecNotaCredito());
//									String vArticuloBT = vArticulo;
//									Date salFechaBT = null;
//									query4 = "{CALL ? := LOBTCK_FUN_FCH_DBO(?,?,?)}";
//									logger.traceInfo("creacionTransaccionesDteB","Llamada a Big Ticket FN[BIG_TICKET]: outDate = LOBTCK_FUN_FCH_DBO("+vArticuloBT+","+regionDespacho+","+comunaDespacho+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticuloBT));
//									conn4 = PoolBDs.getConnection(BDType.BIG_TICKET);
//									if (conn4 != null) {
//										try {
//											cs = conn4.prepareCall(query4);
//											cs.registerOutParameter(1, OracleTypes.DATE);
//											cs.setLong(2, Long.parseLong(vArticuloBT));
//											cs.setInt(3, regionDespacho);
//											cs.setInt(4, comunaDespacho);
//											cs.executeUpdate();
//											
//											salFechaBT = cs.getDate(1);
//											cs.close();
//											logger.traceInfo("creacionTransaccionesDteB", "Big Ticket FN LOBTCK_FUN_FCH_DBO[outDate]: " + salFechaBT, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticuloBT));
//											fechaDesp = FechaUtil.formatDate(Constantes.FECHA_DDMMYYYY3, salFechaBT);
//											if(paramFechaDespacho && salFechaBT.getTime() <= 
//													FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4).getTime()){
//												fechaDesp = FechaUtil.formatDate(Constantes.FECHA_DDMMYYYY3, FechaUtil.sumarRestarDiasFecha(salFechaBT, Constantes.NRO_UNO));
//											}
//											/* ESTAN CADUCOS VALIDARLOS
//											if (actFechPreVenta && (vArticulo.equals("2000316773122")
//													|| vArticulo.equals("2000316773177") || vArticulo.equals("2000317721399")
//													|| vArticulo.equals("2000317719471"))) { 
//												FECHADESP = fechaDespachoPreVenta;
//											}else
//			 							    */
//											if(Validaciones.validarVacio(fechaDespacho)){
//												fechaDespacho = fechaDesp;
//											}
//				                            
//											dias = FechaUtil.diferenciaDias(fechaDespacho, Constantes.FECHA_DDMMYYYY3, fechaDesp, Constantes.FECHA_DDMMYYYY3);
//											fechaDespacho = (Validaciones.validaInteger(dias) && dias.intValue() > Constantes.NRO_CERO)?vFechaBT:fechaDesp;
//										} catch (Exception e) {
//											fechaDespacho = vFechaBT;
//											logger.traceInfo("creacionTransaccionesDteB", "Se produjo un error en la ejecucion de la FN LOBTCK_FUN_FCH_DBO: "+e.getMessage());
//										}
//									} else {
//										logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BIG TICKET");
//										throw new AligareException("Error: Problemas de conexion a la base de datos BIG TICKET");
//									}
//								}
//							}
						} else {
						/* ESTAN CADUCOS VALIDARLOS
							if (actFechPreVenta && (vArticulo.equals("2000316773122")
									|| vArticulo.equals("2000316773177") || vArticulo.equals("2000317721399")
									|| vArticulo.equals("2000317719471"))) {
								fechaDespacho = fechaDespachoPreVenta;
							}
						*/
							//Integer diferencia = FechaUtil.diferenciaDias(fechaVenta, Constantes.FECHA_DDMMYYYY4, fechaDespacho,Constantes.FECHA_DDMMYYYY3);
							Integer diferencia = new Integer(Constantes.NRO_CERO);
							if (Constantes.VACIO.equalsIgnoreCase(fechaDespacho))
							 diferencia = FechaUtil.diferenciaDias(fechaVenta, Constantes.FECHA_DDMMYYYY4, fechaDespacho,Constantes.FECHA_DDMMYYYY3);
							if (Validaciones.validaInteger(diferencia) && diferencia.intValue() < Constantes.NRO_CERO) {
								query3 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA(?,?,?,?,?)}";
								logger.traceInfo("creacionTransaccionesDteB","ACTUALIZACION FECHA CREACION NOTA VENTA SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+correlativo+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
								conn3 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								if (conn3 != null) {
									try {
										cs = conn3.prepareCall(query3);
										cs.setString(1, (fecha + Constantes.SPACE + hms));
										cs.setString(2, Constantes.FECHA_DDMMYYYY_HHMMSS2);
										cs.setLong(3, correlativo);
										cs.registerOutParameter(4, OracleTypes.NUMBER);
										cs.registerOutParameter(5, OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(4));
										sal2 = cs.getString(5);
										cs.close();
										if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
											throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
										}
									} catch (Exception e) {
										logger.traceError("creacionTransaccionesDteB",
												"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA: ", e);
										
										resultado = Constantes.FALSO; 
									}
								} else {
									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
							}
						}

						if(Validaciones.validaLong(codigoRegalo) && codigoRegalo.intValue() != Constantes.NRO_CERO && Validaciones.validaInteger(rutDespacho2)
								&& rutDespacho2.toString().length() <= Constantes.NRO_UNO){
								rutDespacho = rutDespacho2;
						}
						
						if(Validaciones.validaInteger(rutDespacho2) && rutDespacho2.intValue() == Constantes.NRO_CERO){
							rutDespacho = rutCliente;
						}
		                
						String cud = Constantes.VACIO;
						if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) == Constantes.NRO_CERO))
								|| ((costoEnvio.indexOf(Constantes.TRAMA_EG) == Constantes.NRO_CERO))){
//						if(Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
//								|| Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){
							cud = Constantes.SPACE;
						}else{

							if(obj.getArticuloVenta().getCodDespacho() != null && obj.getArticuloVenta().getCodDespacho().trim().length() > Constantes.NRO_VEINTE && obj.getArticuloVenta().getCodDespacho().length() < Constantes.NRO_VEINTIDOS){
								cud = obj.getArticuloVenta().getCodDespacho().substring(0, 16) + Constantes.STRING_CERO 
										+ obj.getArticuloVenta().getCodDespacho().substring(16, 20);
							}else{
								cud = obj.getArticuloVenta().getCodDespacho();
							}
						}
						
						boolean vSwitch = Constantes.VERDADERO;
						long rutDespachoV2 = Constantes.NRO_CERO;
						long rutClienteV2 = Constantes.NRO_CERO;
						String observacionDespacho = Constantes.VACIO;
						
						if(tieneCud(vArticulo).intValue() == Constantes.NRO_CERO){
							String esRegalo = (obj.getArticuloVenta().getEsregalo() != null)?obj.getArticuloVenta().getEsregalo():Constantes.VACIO;
							
							if(Validaciones.validaInteger(tipoRegalo) && tipoRegalo.intValue() == Constantes.NRO_CERO && Constantes.SI.equalsIgnoreCase(esRegalo)){
								tipoRegalo = new Integer(Constantes.NRO_NOVENTANUEVE);
							}else if(Validaciones.validaInteger(tipoRegalo) && tipoRegalo.intValue() != Constantes.NRO_CERO && Constantes.SI.equalsIgnoreCase(esRegalo)){
								tipoRegalo = obj.getArticuloVenta().getTipoRegalo();
							}else{
								tipoRegalo = new Integer(Constantes.NRO_CERO);
							}
							
							String emailDes = Constantes.VACIO;
							String comuReg = Constantes.VACIO;
							
							try{
								
							    if (Constantes.NRO_TREINTATRES == trama.getTipoDoc().getCodPpl()){
							    	comuReg = comunaRegion(trama.getDespacho(), trama.getDatoFacturacion());
							    }
							    
							}catch(AligareException e){ // TODO VALIDAR SI SE DEBE RETURNAR
								logger.traceError("creacionTransaccionesDteB", "Se produjo un error en metodo comunaRegion: ", e);
								
								resultado = Constantes.FALSO; 
							}
							long rutCliFac = Constantes.NRO_CERO;
							long rutCliFacV2 = Constantes.NRO_CERO;
							String telefonoCliente2 = Constantes.VACIO;
							vSwitch = Constantes.FALSO;
					        rutDespachoCambio = null;
							if(Validaciones.validaLong(trama.getDespacho().getCorrelativoVenta())){
								vSwitch = Constantes.VERDADERO;
					            rutCliente = Constantes.NRO_CERO;
					            rutDespacho = Constantes.NRO_CERO;
					            nombreDespacho = Constantes.VACIO;
					            comunaDespacho = null;
					            regionDespacho = null;
					            direccionDespacho = Constantes.VACIO;
					            telefonoDespacho = Constantes.VACIO;
					            nombreCliente = Constantes.VACIO;
					            direccionCliente = Constantes.VACIO;
					            emailCliente = Constantes.VACIO;
					            
					            if(Validaciones.validaInteger(trama.getDespacho().getRutDespacho()) && trama.getDespacho().getRutDespacho().intValue() == Constantes.NRO_CERO){
					            	rutDespachoCambio = trama.getDespacho().getRutCliente();
					            }else{
					            	rutDespachoCambio = trama.getDespacho().getRutDespacho();
					            }
					            rutCliente = (Validaciones.validaInteger(trama.getDespacho().getRutCliente()))?trama.getDespacho().getRutCliente().longValue():Constantes.NRO_CERO;
					            rutClienteV2 = Long.parseLong(rutCliente + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente)));
					            if (rutFacturaBoDes.length() > 2){
						            rutCliFacV2 = Long.parseLong(rutFacturaBoDes.substring(0, rutFacturaBoDes.length()-1));
						            rutCliFacV2 = Long.parseLong(rutCliFacV2 + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliFacV2)));
					            }
					            rutCliFac = (Validaciones.validaInteger(trama.getTipoDoc().getCodPpl()) && trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TREINTANUEVE)?rutClienteV2:Long.parseLong(String.valueOf(rutCliFacV2)); //TODO revisar por merge .20
					            rutDespacho = (Validaciones.validaInteger(rutDespachoCambio))?rutDespachoCambio.longValue():Constantes.NRO_CERO;
					            rutDespachoV2 = Long.parseLong(rutDespacho + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutDespacho)));
					            nombreDespacho = Util.cambiaCaracteres(trama.getDespacho().getNombreDespacho());
					            comunaDespacho = trama.getDespacho().getComunaDespacho();
					            regionDespacho = trama.getDespacho().getRegionDespacho();
					            direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
					            observacionDespacho = Util.cambiaCaracteres(trama.getDespacho().getObservacion());
					            telefonoDespacho = trama.getDespacho().getTelefonoDespacho();
					            aux = Util.cambiaCaracteres((((trama.getDespacho().getNombreCliente() != null)?trama.getDespacho().getNombreCliente():Constantes.VACIO)
					            		+ Constantes.SPACE + ((trama.getDespacho().getApellidoPatCliente() != null)?trama.getDespacho().getApellidoPatCliente():Constantes.VACIO) 
					            		+ Constantes.SPACE + ((trama.getDespacho().getApellidoMatCliente() != null)?trama.getDespacho().getApellidoMatCliente():Constantes.VACIO)));
					            nombreCliente = Util.rellenarString(aux, 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49);
					            direccionCliente = Util.cambiaCaracteres(trama.getDespacho().getDireccionCliente());
					            telefonoCliente2 = trama.getDespacho().getTelefonoCliente();
					            emailCliente = trama.getDespacho().getEmailCliente();
					            if(revisarEmail((trama.getDespacho().getJornadaDespacho() != null)?trama.getDespacho().getJornadaDespacho():Constantes.VACIO, parametros)){
					            	emailDes = observacionDespacho;
					            }else{
					            	emailDes = emailCliente;
					            }                       						                    
							}
							
							int ent01 = (costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO));
							int ent02 = (costoEnvio.indexOf(Constantes.TRAMA_EG));
							
							logger.traceInfo("TRAMA_COSTO_ENVIO", ""+costoEnvio+" v/s "+ent01);
							logger.traceInfo("TRAMA_EG", ""+costoEnvio+" v/s "+ent02);
							
//							if ((ent01 != Constantes.NRO_CERO) 
//									&& (ent02 != Constantes.NRO_CERO)){
////						    if(((!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) && !esExtgarantia) 
////						    		&& vSwitch) || !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){
//						    	BigDecimal vPrecio = (Validaciones.validaBigDecimal(obj.getArticuloVenta().getPrecio()))?obj.getArticuloVenta().getPrecio():new BigDecimal(Constantes.NRO_CERO);
//						    	Integer vUnidades = (Validaciones.validaInteger(obj.getArticuloVenta().getUnidades()))?obj.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
//						    	BigDecimal vtotal = vPrecio.multiply(new BigDecimal(vUnidades));
//						    	query4 = "{CALL ? := LGBTCK_FUN_INA_DBO(?,?,TO_DATE(?,'dd/MM/yyyy'),?,'0',?,?,?,TO_DATE(?,'dd-MM-yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?,1,1,'A1',?,?,?,?,?,1,3,?,?,?,?,1,'TVIRTUAL',?,'',null,null,null,?,?,?,?,?,'')}";
//								logger.traceInfo("creacionTransaccionesDteB", "Creacion de Despacho en Big Ticket FN[BIG_TICKET]: outCode = LGBTCK_FUN_INA_DBO("+tipo+","+nroTransaccion+",TO_DATE("+fecha+",'dd/MM/yyyy'),"+sucursal+",'0',"+rutCliFac+","+rutDespachoV2+","+nombreDespacho+",TO_DATE("+fechaDespacho+",'dd-MM-yyyy'),"+(sucursal + 10000)+","+cud+","+vendedor2+","+obj.getArticuloVenta().getCodArticulo()+","+obj.getArticuloVenta().getColor()+","+comunaDespacho+","+regionDespacho+","+codigoRegalo+","+direccionDespacho+","+tipoDespachoComision+","+observacionDespacho+","+obj.getArticuloVenta().getPrecio()+
//										","+obj.getArticuloVenta().getUnidades()+",1,1,'A1',"+telefonoDespacho+","+vtotal+","+nombreCliente+","+(comuReg + "Dir: "+direccionCliente)+","+((Constantes.STRING_UNO.equals(wsCodAuto))?telefonoCliente+",1,3,"+Constantes.NRO_CUATRO:telefonoCliente2+",1,3,"+Constantes.NRO_TRES)+","+emailDes+","+tipoRegalo+","+nroCaja+",1,'TVIRTUAL',"+correlativo+",'',null,null,null,"+((((Validaciones.validaInteger(trama.getTipoDoc().getTipoDoc()))?trama.getTipoDoc().getTipoDoc().intValue():Constantes.NRO_CERO) == Constantes.NRO_CUATRO)?Constantes.NRO_UNO:Constantes.NRO_CERO)+
//										","+emailCliente+","+razonSocialFacturaBoDes+","+giroFacturaBoDes+","+rutFacturaBoDes+",'')", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//								if(conn4 == null){
//									conn4 = PoolBDs.getConnection(BDType.BIG_TICKET);
//								}
//								if (conn4 != null) {
//									try {
//										
//										cs = conn4.prepareCall(query4);
//										cs.registerOutParameter(1, OracleTypes.NUMBER);
//										cs.setInt(2, tipo);
//										cs.setInt(3, nroTransaccion);
//										cs.setString(4, fecha);
//										cs.setInt(5, sucursal);
//										cs.setLong(6, rutCliFac);
//										cs.setLong(7, rutDespachoV2);
//										cs.setString(8, nombreDespacho);
//										cs.setString(9, FechaUtil.formatDate(Constantes.FECHA_DDMMYYYY3, obj.getArticuloVenta().getFechaDespacho()));// FIXME fechaDespacho);
//										cs.setInt(10, Integer.parseInt(obj.getArticuloVenta().getCodBodega()));//(sucursal + 10000)); merge .20
//										cs.setString(11, cud);
//										cs.setLong(12, vendedor2);
//										cs.setLong(13, Long.parseLong(obj.getArticuloVenta().getCodArticulo()));
//										if (!Validaciones.validaInteger(obj.getArticuloVenta().getColor())){
//										    cs.setNull(14, OracleTypes.NUMBER);
//									    } else{
//									    	cs.setInt(14, obj.getArticuloVenta().getColor());
//									    }
//										cs.setInt(15, comunaDespacho);
//										cs.setInt(16, regionDespacho);
//										cs.setLong(17, codigoRegalo);
//										cs.setString(18, direccionDespacho);
//										cs.setString(19, tipoDespachoComision);
//										cs.setString(20, observacionDespacho);
//										cs.setBigDecimal(21, obj.getArticuloVenta().getPrecio());
//										cs.setLong(22, obj.getArticuloVenta().getUnidades());
//										cs.setString(23, telefonoDespacho);
//										cs.setBigDecimal(24, vtotal);
//										cs.setString(25, nombreCliente);
//										cs.setString(26, comuReg + " Dir: "+direccionCliente);
//										
//										if(Constantes.STRING_UNO.equals(wsCodAuto)){
//											cs.setString(27, telefonoCliente);
//											cs.setInt(28, Constantes.NRO_TRES);// Constantes.NRO_CUATRO); //cambio merge .20
//										}else{
//											cs.setString(27, telefonoCliente2);
//											cs.setInt(28, Constantes.NRO_TRES);
//										}
//										
//										cs.setString(29, emailDes);
//										cs.setInt(30, tipoRegalo);
//										cs.setInt(31, nroCaja);
//										cs.setLong(32, correlativo);
//										cs.setInt(33, ((((Validaciones.validaInteger(trama.getTipoDoc().getTipoDoc()))?trama.getTipoDoc().getTipoDoc().intValue():Constantes.NRO_CERO) == Constantes.NRO_CUATRO)?Constantes.NRO_UNO:Constantes.NRO_CERO));
//										cs.setString(34, emailCliente);
//										cs.setString(35, razonSocialFacturaBoDes); 
//										cs.setString(36, giroFacturaBoDes);
//										cs.setString(37, rutFacturaBoDes); 
//										cs.executeUpdate();
//										sal = new Integer(cs.getInt(1));
//										logger.traceInfo("creacionTransaccionesDteB", "Big Ticket FN LGBTCK_FUN_INA_DBO[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//										cs.close();
//									} catch (Exception e) {
//										logger.traceError("creacionTransaccionesDteB",
//												"Imposible de grabar en Big Ticket error en LGBTCK_FUN_INA_DBO: ", e);
//										
//										resultado = Constantes.FALSO; 
//									}
//								} else {
//									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BIG TICKET");
//									throw new AligareException("Error: Problemas de conexion a la base de datos BIG TICKET");
//								}
//						    }
						}
						BigDecimal vPrecio = ((Validaciones.validaBigDecimal(obj.getArticuloVenta().getPrecio()))?obj.getArticuloVenta().getPrecio():new BigDecimal(Constantes.NRO_CERO));
						Integer vCantPrd = (Validaciones.validaInteger(obj.getArticuloVenta().getUnidades()))?obj.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
						// Creacion REGISTRO PARA NOVIOS
						if(Validaciones.validaLong(codigoRegalo) && codigoRegalo.intValue() > Constantes.NRO_CERO){
							vendedor2 = Long.parseLong(vendedor + Constantes.VACIO + Util.getDigitoValidadorRut2(vendedor));
							String vCodigoReg = (Validaciones.validaLong(codigoRegalo))?codigoRegalo.toString():Constantes.VACIO;
							long vArticulo2 = Long.parseLong(obj.getArticuloVenta().getCodArticulo());
							BigDecimal vMontoDesc = (Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento()))?obj.getArticuloVenta().getMontoDescuento():new BigDecimal(Constantes.NRO_CERO);
							BigDecimal vTotal = vPrecio.multiply(new BigDecimal(vCantPrd));
							BigDecimal vPrcCmp = vTotal.subtract(vMontoDesc);
							
					//merge .22
							BigDecimal precio = null;
							// Se calcula precio
							if (obj.getArticuloVenta().getMontoDescuento().compareTo(new BigDecimal(Constantes.NRO_CERO)) < 0) {
								precio = obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento());
							} else {
								//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
								precio = (obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento().divide(new BigDecimal(vCantPrd))));
							}
							vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); // merge .22
					//merge .22
							
							Integer vAux = obj.getArticuloVenta().getCorrelativoItem();
							//String vMrc = (Validaciones.validaInteger(tipoRegalo) && tipoRegalo.intValue() == Constantes.NRO_NOVENTANUEVE)?"< SR >":"< NR >";//BPL CAMBIAR AL REVES (SI ES 99 ES SR Y SI NO NR
							//String vMensajeTarjeta = (Validaciones.validaInteger(tipoRegalo) && tipoRegalo.intValue() == Constantes.NRO_NOVENTANUEVE)?Constantes.STRING_CERO:mensajeTarjeta;

							logger.traceInfo("Valores de variables seteo mensaje regalos - "," Tipo Regalo : " + tipoRegalo );
							
							String vMrc = "< NR >";
							String vMensajeTarjeta = Constantes.STRING_CERO;
							if (Validaciones.validaInteger(tipoRegalo)){
								if (tipoRegalo.intValue() == Constantes.NRO_NOVENTANUEVE){ 
									vMrc = "< NR >";
									vMensajeTarjeta = Constantes.STRING_CERO;
								} else{
								
								if(tipoRegalo.intValue() == Constantes.NRO_CERO){
									vMrc = "< NR >";
									vMensajeTarjeta = Constantes.VACIO;
								}
								
								else {
									vMrc = "< SR >";
									vMensajeTarjeta = mensajeTarjeta;
									}
								}
							}

//							if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
//									&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
////							if(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
////									&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){ // TODO VALIDAR CONDICION
//								query5 = "{CALL LGLREG_PRC_MOV_LIS_VLR(?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,1,0,0,?,?,?,?,?,?,?,?,0,?,?,?,?,'',0,?,0,0,0,0,?,?)}";
//								logger.traceInfo("creacionTransaccionesDteB", "Creacion de REGISTRO NOVIOS SP[CLUB]: LGLREG_PRC_MOV_LIS_VLR("+vCodigoReg+","+vArticulo2+","+(sucursal + 10000)+",TO_DATE("+fecha+",'DD/MM/YYYY'),"+nroTransaccion+",1,0,0,"+vCantPrd+","+vPrcCmp+","+nroTransaccion+","+nroCaja+","+tipoReg+","+vAux+","+direccionDespacho+","+vMrc+",0,"+emailCliente+","+nombreInvitado+","+apellidoPaterno+","+apellidoMaterno+",'',0,"+vMensajeTarjeta+",0,0,0,0,outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticulo2+Constantes.VACIO));
//								conn5 = PoolBDs.getConnection(BDType.CLUB);
//								if (conn5 != null) {
//									try {
//										cs = conn5.prepareCall(query5);
//										cs.setString(1, vCodigoReg);
//										cs.setLong(2, vArticulo2);
//										cs.setInt(3, (sucursal + 10000));
//										cs.setString(4, fecha);
//										cs.setInt(5, nroTransaccion);
//										cs.setInt(6, vCantPrd);
//										cs.setBigDecimal(7, vPrcCmp);
//										cs.setInt(8, nroTransaccion);
//										cs.setInt(9, nroCaja);
//										cs.setInt(10, tipoReg);
//										cs.setInt(11, vAux);
//										cs.setString(12, direccionDespacho);
//										cs.setString(13, vMrc);
//										cs.setString(14, emailCliente);
//										cs.setString(15, nombreInvitado);
//										cs.setString(16, apellidoPaterno);
//										cs.setString(17, apellidoMaterno);
//										cs.setString(18, vMensajeTarjeta);
//										cs.registerOutParameter(19, OracleTypes.NUMBER);
//										cs.registerOutParameter(20, OracleTypes.VARCHAR);
//										cs.execute();
//										sal = new Integer(cs.getInt(19));
//										sal2 = cs.getString(20);
//										cs.close();
//										logger.traceInfo("creacionTransaccionesDteB", "NOVIOS SP LGLREG_PRC_MOV_LIS_VLR: outCode: " + sal + " outMsj: " + sal2, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticulo2+Constantes.VACIO));
//									} catch (Exception e) {
//										logger.traceError("creacionTransaccionesDteB",
//												"Imposible de grabar registro de NOVIOS error en SP LGLREG_PRC_MOV_LIS_VLR: ", e);
//										
//										resultado = Constantes.FALSO; 
//									}
//								} else {
//									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos CLUBES");
//									throw new AligareException("Error: Problemas de conexion a la base de datos CLUBES");
//								}
//							}
						}
						
						// Creacion de Despacho para Backoffice
//						if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
//								&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))
//								&& !esExtgarantia
//								&& vSwitch){
////						if(!(((Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) || 
////								Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))) && !esExtgarantia) 
////								&& vSwitch)){ // TODO REVISAR CONDICION
//							query1 = "{CALL ? := INSERTA_DESPACHO_BACK(TO_DATE(?,'DD-MM-YYYY'),?,?,?,?,?,1,?,?,TO_DATE(?,'DD-MM-YYYY'),?,?,0,?,?,?,0,?,?,1,'A1',?,?,?,1,?)}";
//							logger.traceInfo("creacionTransaccionesDteB", "Creacion de Despacho para Backoffice FN[BACK_OFFICE]: outCode = INSERTA_DESPACHO_BACK(TO_DATE('"+fecha+"','DD-MM-YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'"+cud+"',"+((!Validaciones.validaInteger(obj.getArticuloVenta().getColor()))?null:obj.getArticuloVenta().getColor())+",1,"+rutDespachoV2+",'"+nombreDespacho+"',TO_DATE('"+fechaDespacho+"','DD-MM-YYYY'),"+sucursalDespacho+","+comunaDespacho+",0,"+regionDespacho+",'"+direccionDespacho+"','"+telefonoDespacho+"',0,'"+jornadaDespachoBo+"','"+observacionDespacho+"',1,'A1',"+rutClienteV2+",'"+direccionCliente+"','"+telefonoCliente+"',1,'"+nombreCliente+"')", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()), new KeyLog("CUD", cud));
//							if (conn1 == null) {
//								conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//							}
//							if (conn1 != null) {
//								try {
//	
//									cs = conn1.prepareCall(query1);
//									cs.registerOutParameter(1, OracleTypes.NUMBER);
//									cs.setString(2, fecha);
//									cs.setInt(3, sucursal);
//									cs.setInt(4, nroCaja);
//									cs.setInt(5, nroTransaccion);
//									cs.setString(6, cud);
//									if (!Validaciones.validaInteger(obj.getArticuloVenta().getColor())){
//									    cs.setNull(7, OracleTypes.NUMBER);
//								    } else{
//								    	cs.setInt(7, obj.getArticuloVenta().getColor());
//								    }
//									cs.setLong(8, rutDespachoV2);
//									cs.setString(9, nombreDespacho);
//									cs.setString(10, fechaDespacho);
//									cs.setInt(11, sucursalDespacho);
//									cs.setInt(12, comunaDespacho);
//									cs.setInt(13, regionDespacho);
//									cs.setString(14, direccionDespacho);
//									cs.setString(15, telefonoDespacho);
//									cs.setString(16, jornadaDespachoBo);
//									cs.setString(17, observacionDespacho);
//									cs.setLong(18, rutClienteV2);
//									cs.setString(19, direccionCliente);
//									cs.setString(20, telefonoCliente);
//									cs.setString(21, nombreCliente);
//									cs.executeUpdate();
//									sal = new Integer(cs.getInt(1));
//									logger.traceInfo("creacionTransaccionesDteB", "Registro Despacho BO FN INSERTA_DESPACHO_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()), new KeyLog("CUD", cud));
//									cs.close();
//									
//									if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//										logger.traceError("creacionTransaccionesDteB",
//												"Imposible crear registro Despacho en Backoffice Error - ", new AligareException("outCode: "+sal));
//										
//										resultado = Constantes.FALSO; 
//									}
//								} catch (Exception e) {
//									logger.traceError("creacionTransaccionesDteB",
//											"Se produjo un error en la ejecucion de la FN INSERTA_DESPACHO_BACK: ", e);
//									
//									resultado = Constantes.FALSO; 
//								}
//							}else{
//								logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//								throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//							}				
//						}
						boolean gPagoComision = false;
						if (Constantes.SI.equalsIgnoreCase(parametros.buscaValorPorNombre("COMISION_RT"))){
							gPagoComision = true;
						}
						
//						if(//!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) &&
//							// !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2)) &&
//								vPrecio.intValue() != Constantes.NRO_CERO
//								){ // TODO REVISAR CONDICION
//							String rutAnfitrion = Constantes.STRING_CERO;
//							query1 = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,0,?,?,?,?,?,0,5)}";
//							if((Constantes.BOL_TIPO_DES_RT.equalsIgnoreCase(tipoDespachoComision))
//								&& gPagoComision ){
//								query4 = "SELECT NVL(TO_CHAR(RUT_USUARIO),'0') AS RUT_USUARIO FROM BIGT_RESERVAS WHERE CUD = ?";
//								logger.traceInfo("creacionTransaccionesDteB", "Ejecucion de query en BIG TICKET: " + query4.substring(0, query4.length()-1) + cud, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("CUD", cud));
//								if(conn4 == null){
//									conn4 = PoolBDs.getConnection(BDType.BIG_TICKET);
//								}
//								if(conn4 != null){
//									try {
//										ResultSet rs = null;
//										PreparedStatement ps = conn4.prepareStatement(query4);
//										if (ps != null) {
//											ps.setString(1, cud);
//											rs = ps.executeQuery();
//											if (rs != null && rs.next()) {
//												rutAnfitrion = rs.getString("RUT_USUARIO");
//												rs.close();
//												logger.traceInfo("creacionTransaccionesDteB", "se consulta en BIGT_RESERVAS RUT_USUARIO: "+rutAnfitrion, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("CUD", cud));
//											}
//											ps.close();
//										}
//									} catch (Exception e) {
//										logger.traceInfo("creacionTransaccionesDteB", "Se produjo un error en la ejecucion de la query a la tabla BIGT_RESERVAS - " + e.getMessage(), new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("CUD", cud));
//									}
//								}else{
//									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BIG TICKET", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("CUD", cud));
//								}
//					            
//								if(rutAnfitrion.length() == Constantes.NRO_DIEZ || rutAnfitrion.length() == Constantes.NRO_NUEVE){
//									rutAnfitrion = rutAnfitrion.substring(0, rutAnfitrion.length()-2);
//								}
//								logger.traceInfo("creacionTransaccionesDteB", "Creacion de Articulo en BackOffice FN[BACK_OFFICE]: outCode = INSERTA_ARTICULOS_BACK(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+obj.getArticuloVenta().getCorrelativoItem()+","+obj.getArticuloVenta().getCodArticulo()+","+rutAnfitrion+","+codigoRegalo+",0,"+vPrecio+","+vCantPrd+","+((!Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento()))?null:obj.getArticuloVenta().getMontoDescuento())+","+obj.getArticuloVenta().getTipoDescuento()+","+cud+",0,5)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//								if (conn1 == null) {
//									conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//								}
//								if (conn1 != null) {
//									try {
//										cs = conn1.prepareCall(query1);
//										cs.registerOutParameter(1, OracleTypes.NUMBER);
//										cs.setString(2, fecha);
//										cs.setString(3, Constantes.FECHA_DDMMYYYY);
//										cs.setInt(4, sucursal);
//										cs.setInt(5, nroCaja);
//										cs.setInt(6, nroTransaccion);
//										cs.setInt(7, obj.getArticuloVenta().getCorrelativoItem());
//										cs.setLong(8, Long.parseLong(obj.getArticuloVenta().getCodArticulo()));
//										cs.setInt(9, Integer.parseInt(rutAnfitrion));
//										cs.setLong(10, codigoRegalo);
//										cs.setBigDecimal(11, vPrecio);
//										cs.setInt(12, vCantPrd);
//										if (!Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento())){
//										    cs.setNull(13, OracleTypes.NUMBER);
//									    } else{
//									    	cs.setBigDecimal(13, obj.getArticuloVenta().getMontoDescuento());
//									    }
//										cs.setInt(14, obj.getArticuloVenta().getTipoDescuento());
//										cs.setString(15, cud);
//										cs.executeUpdate();
//										sal = new Integer(cs.getInt(1));
//										logger.traceInfo("creacionTransaccionesDteB", "Registro Despacho BO FN INSERTA_ARTICULOS_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//										cs.close();
//									} catch (Exception e) {
//										logger.traceError("creacionTransaccionesDteB",
//												"Se produjo un error en la ejecucion de la FN INSERTA_ARTICULOS_BACK: ", e);
//										
//										resultado = Constantes.FALSO; 
//									}
//								}else{
//									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//									throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//								}				
//							}else{
//								logger.traceInfo("creacionTransaccionesDteB", "Creacion de Articulo en BackOffice FN[BACK_OFFICE]:  outCode = INSERTA_ARTICULOS_BACK(TO_DATE('"+fecha+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+nroCaja+","+nroTransaccion+","+obj.getArticuloVenta().getCorrelativoItem()+","+obj.getArticuloVenta().getCodArticulo()+","+vendedor+","+codigoRegalo+",0,"+vPrecio+","+vCantPrd+","+((!Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento()))?null:obj.getArticuloVenta().getMontoDescuento())+","+obj.getArticuloVenta().getTipoDescuento()+",'"+cud+"',0,5)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//								if (conn1 == null) {
//									conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//								}
//								if (conn1 != null) {
//									try {
//										cs = conn1.prepareCall(query1);
//										cs.registerOutParameter(1, OracleTypes.NUMBER);
//										cs.setString(2, fecha);
//										cs.setString(3, Constantes.FECHA_DDMMYYYY);
//										cs.setInt(4, sucursal);
//										cs.setInt(5, nroCaja);
//										cs.setInt(6, nroTransaccion);
//										cs.setInt(7, obj.getArticuloVenta().getCorrelativoItem());
//										cs.setLong(8, Long.parseLong(obj.getArticuloVenta().getCodArticulo()));
//										cs.setInt(9, Integer.parseInt(vendedor));
//										cs.setLong(10, codigoRegalo);
//										cs.setBigDecimal(11, vPrecio);
//										cs.setInt(12, vCantPrd);
//										if (!Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento())){
//										    cs.setNull(13, OracleTypes.NUMBER);
//									    } else{
//									    	cs.setBigDecimal(13, obj.getArticuloVenta().getMontoDescuento());
//									    }
//										cs.setInt(14, obj.getArticuloVenta().getTipoDescuento());
//										cs.setString(15, cud);
//										cs.executeUpdate();
//										sal = new Integer(cs.getInt(1));
//										logger.traceInfo("creacionTransaccionesDteB", "Registro Despacho BO FN INSERTA_ARTICULOS_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
//										cs.close();
//									} catch (Exception e) {
//										logger.traceError("creacionTransaccionesDteB",
//												"Se produjo un error en la ejecucion de la FN INSERTA_ARTICULOS_BACK: ", e);
//										
//										resultado = Constantes.FALSO; 
//									}
//								}else{
//									logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
//									throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//								}
//							}
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO ){//Constantes.NRO_CERO && sal.intValue() != Constantes.NRO_UNO) {
//								logger.traceError("creacionTransaccionesDteB",
//										"Imposible crear registro Articulos en Backoffice Error - ", new AligareException("outCode: "+sal));
//								
//								resultado = Constantes.FALSO; 
//							}
//						}
						
						if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
								&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))
								&& Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO
								&& !esExtgarantia){
//						if(Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO && 
//								!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) && !esExtgarantia 
//								&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){
							query3 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES(?,?,?,?,?,?)}";
							logger.traceInfo("creacionTransaccionesDteB", "Actualiza Fecha Despacho en Articulo MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES("+correlativo+","+vArticulo+","+fechaDespacho+","+Constantes.FECHA_DDMMYYYY2+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticulo));
							if(conn3 == null){
								conn3 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
							}
							if(conn3 != null){
								try {									
									cs = conn3.prepareCall(query3);
									cs.setLong(1, correlativo);
									cs.setString(2, vArticulo);
									cs.setString(3, fechaDespacho);
									cs.setString(4, Constantes.FECHA_DDMMYYYY2);
									cs.registerOutParameter(5, OracleTypes.NUMBER);
									cs.registerOutParameter(6, OracleTypes.VARCHAR);
									cs.execute();
									sal = new Integer(cs.getInt(5));
									sal2 = cs.getString(6);
									cs.close();
									if(sal.intValue() != Constantes.NRO_CERO){
										throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
									}
								} catch (Exception e) {
									logger.traceError("creacionTransaccionesDteB",
											"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES: ", e);
									
									resultado = Constantes.FALSO; 
								}
							}else{
								logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
								throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
							}
						}
					}
				}

				if (contadorNoEsMKP == Constantes.NRO_CERO) {
					logger.traceError("creacionTransaccionesDteB", "No existen Articulos de Venta - Error", new AligareException("No hay articulos que no sean Marketplace"));
					
					resultado = Constantes.FALSO; 
				}
			}
			
			// Creacion Transaccion
			if(Validaciones.validaLong(trama.getNotaVenta().getCorrelativoVenta())){
				totalVentaMkp = (Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoVenta()))?trama.getNotaVenta().getMontoVenta():new BigDecimal(Constantes.NRO_CERO);
				tipoIdent = trama.getNotaVenta().getIndicadorMkp();
				if(Constantes.STRING_DOS.equals(tipoIdent)){
					if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
						totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
						for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
							if (obj.getEsMkp()==Constantes.NRO_CERO){
							totalVentaMkp = totalVentaMkp.add((Validaciones.validaBigDecimal(obj.getMontoVenta()))?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO));
							}
						}
					}
					logger.traceInfo("creacionTransaccionesDteB", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
				}	else if(Constantes.STRING_UNO.equals(tipoIdent)){
					if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
						totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
						for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
							if (obj.getEsMkp()==Constantes.NRO_UNO){
							totalVentaMkp = totalVentaMkp.add((Validaciones.validaBigDecimal(obj.getMontoVenta()))?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO));
							}
						}
					}
					logger.traceInfo("creacionTransaccionesDteB", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
				}
			
				Timestamp FechaCreacionNv = trama.getNotaVenta().getFechaHoraCreacion();
				
				if(!Validaciones.validarVacio(rutEjecutivoVta) && !Constantes.STRING_CERO.equals(rutEjecutivoVta) && rutEjecutivoVta.length() > Constantes.NRO_UNO){
					vendedor = rutEjecutivoVta.substring(0, (rutEjecutivoVta.length()+Constantes.NRO_MENOSUNO));
				}
				
				Integer codigoBoFac = new Integer(Constantes.NRO_CERO);
				Integer codigoBoBol = new Integer(Constantes.NRO_CERO);
				if(trama.getTipoDoc().getDocOrigen().equals(trama.getTipoDoc().getTipoDoc())){
					codigoBoBol = trama.getTipoDoc().getCodBo();
				}else{
					codigoBoFac = trama.getTipoDoc().getCodBo();
				}

				int sucursalMkp = 0;
				String fechaMkp = "01/01/1901";
				int nroCajaMkp = 0;
				int nroTransaccionMkp = 0;
				if (Constantes.IND_MKP_AMBAS.equalsIgnoreCase(trama.getNotaVenta().getIndicadorMkp())){
					sucursalMkp = sucursal;
					fechaMkp = fecha;
					nroCajaMkp = nroCaja;
					nroTransaccionMkp = nroTransaccion.intValue();
				}

//				Integer codigoBo = (Validaciones.validaInteger(codigoBoFac) && codigoBoFac.intValue() == Constantes.NRO_UNO)?codigoBoFac:codigoBoBol;
//				query1 = "{CALL ? := INSERTA_TRANSACCION_BACK4(TO_DATE(?,?),?,?,?,?,?,?,?,TO_DATE(?,'dd/MM/yyyy'),?,?,0,?,?,0,?,?,0,0,TO_DATE(?,?),0,0,?,0,TO_DATE(?,?),?,?,?,'')}";
//				logger.traceInfo("creacionTransaccionesDteB", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_TRANSACCION_BACK4(TO_DATE('"+fecha+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+nroCaja+","+nroTransaccion+","+nroTransaccion+","+tipo+","+totalVentaMkp+","+sucursalMkp+",TO_DATE('"+fechaMkp+"','dd/MM/yyyy'),"+nroCajaMkp+","+nroTransaccionMkp+",0,"+supervisor+","+vendedor2+",0,"+trama.getNotaVenta().getRutComprador()+","+((!Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoDescuento()))?null:trama.getNotaVenta().getMontoDescuento())+",0,0,TO_DATE("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"),0,0,"+correlativo+",0,TO_DATE('"+(FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, FechaCreacionNv))+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"+boleta+","+codigoBo+",'"+servicioNCA+"','')", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				vendedor2 = Long.parseLong(vendedor + Constantes.VACIO + Util.getDigitoValidadorRut2(vendedor));
//				boolean seguir = Constantes.VERDADERO;
//				if (conn1 == null) {
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//				}
//				if (conn1 != null) {
//					try {									
//
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(4, sucursal);
//						cs.setInt(5, nroCaja);
//						cs.setInt(6, nroTransaccion);
//						cs.setInt(7, nroTransaccion);
//						cs.setInt(8, tipo);
//						cs.setBigDecimal(9, totalVentaMkp);
//						cs.setInt(10, sucursalMkp);
//						cs.setString(11, fechaMkp);
//						cs.setInt(12, nroCajaMkp);
//						cs.setInt(13, nroTransaccionMkp);		
//						cs.setLong(14, Long.parseLong(supervisor));
//						cs.setLong(15, Long.parseLong(vendedor));// FIXME vendedor to vendedor2
//						cs.setInt(16, trama.getNotaVenta().getRutComprador());
//						if (!Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoDescuento())){
//						    cs.setNull(17, OracleTypes.NUMBER);
//					    } else{
//					    	cs.setBigDecimal(17, trama.getNotaVenta().getMontoDescuento());
//					    }
//						cs.setString(18, (fecha + Constantes.SPACE + hms));
//						cs.setString(19, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.setLong(20, correlativo);
//						cs.setString(21, (FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, FechaCreacionNv)));
//						cs.setString(22, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.setInt(23, boleta);
//						cs.setInt(24, codigoBo);
//						cs.setString(25, (servicioNCA.length()<=252)?servicioNCA:servicioNCA.substring(0, 252));
//						//cs.setString(25, (servicioNCA.length()<252)?servicioNCA:servicioNCA.substring(0, 250));
//						//cs.setString(25, servicioNCA);
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionTransaccionesDteB", "Transaccion BO FN INSERTA_TRANSACCION_BACK4[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							// tipo = Constantes.NRO_CERO;
//							caja.saltarFolio(parametros, nroTransaccion);
//							resultado = Constantes.FALSO;
//							seguir = Constantes.FALSO;
//							logger.traceError("creacionTransaccionesDteB", "Imposible crear Encabezado Transaccion Error - ", new AligareException("outCode: " + sal));
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionTransaccionesDteB", "Se produjo un error en la ejecucion de la FN INSERTA_TRANSACCION_BACK4: ", e);
//						resultado = Constantes.FALSO; 
//						seguir = Constantes.FALSO;
//					}
				}else{
					logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos BACK OFFICE");
					throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
				}

			boolean seguir = Boolean.TRUE;
				if(seguir){
					Integer estadoOrigenOC2 = new Integer(Constantes.NRO_DOS);
					/* Integer estadoOrigenOC2 = new Integer(Constantes.NRO_CERO);
					 * if(trama.getNotaVenta().getEstado().intValue() == 14){
					 *    estadoOrigenOC2 = new Integer(24);
					 * }else if(trama.getNotaVenta().getEstado().intValue() == 13){
					 *    estadoOrigenOC2 = new Integer(23);
					 * }else{
					 *    estadoOrigenOC2 = new Integer(2);
					 * } 
					 */
					String fechaBol = fecha + Constantes.SPACE + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2;
//					String horaBol = fecha + Constantes.SPACE + FechaUtil.getCurrentHoursString();
					String horaBol = fecha + Constantes.SPACE + hms;
					query3 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
					logger.traceInfo("creacionTransaccionesDteB", "Actualizacion NOTA VENTA termino de la transacciones MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA("+estadoOrigenOC2+","+nroTransaccion+","+boleta+","+fechaBol+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+horaBol+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+nroCaja+","+nroTransaccion+","+vendedor+","+sucursal+","+correlativo+",outCode,outMsj,"+Constantes.NRO_CERO+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
					if(conn3 == null){
						conn3 = PoolBDs.getConnection(BDType.MODEL_EXTEND); 
					}
					if(conn3 != null){
						try {									
							cs = conn3.prepareCall(query3);
							cs.setInt(1, estadoOrigenOC2);
							cs.setInt(2, nroTransaccion);
							cs.setString(3, boleta);
							cs.setString(4, fechaBol);
							cs.setString(5, Constantes.FECHA_DDMMYYYY_HHMMSS2);
							cs.setString(6, horaBol);
							cs.setString(7, Constantes.FECHA_DDMMYYYY_HHMMSS2);
							cs.setInt(8, nroCaja);
							cs.setInt(9, nroTransaccion);
							cs.setLong(10, Long.parseLong(vendedor)); //FIXME vendedor2 to vendedor
							cs.setInt(11, sucursal);
							cs.setLong(12, correlativo);
							cs.registerOutParameter(13, OracleTypes.NUMBER);
							cs.registerOutParameter(14, OracleTypes.VARCHAR);
							cs.setInt(15, Constantes.NRO_CERO);
							cs.execute();
							sal = new Integer(cs.getInt(13));
							sal2 = cs.getString(14);
							cs.close();
							if(sal.intValue() != Constantes.NRO_CERO){
								throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
							}
						} catch (Exception e) {
							logger.traceError("creacionTransaccionesDteB",
									"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA: ", e);
							
							resultado = Constantes.FALSO;
						}
						trama.getNotaVenta().setNumeroBoleta(nroTransaccion.longValue());
						trama.getNotaVenta().setCorrelativoBoleta(nroTransaccion.longValue());
						//trama.getNotaVenta().setFechaBoleta(Constantes.FECHA_DDMMYYYY_HHMMSS2);
					}else{
							logger.traceInfo("creacionTransaccionesDteB", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
							throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
					}
				}
//			}
			
		} catch (AligareException ex) {
			logger.traceError("creacionTransaccionesDteB", ex);
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
			logger.endTrace("creacionTransaccionesDteB", "Finalizado", "boolean: AligareException");
			throw ex;
		} catch (Exception e) {
			logger.traceError("creacionTransaccionesDteB", "Se produjo un error inesperado: ", e);
			resultado = Constantes.FALSO;
			return resultado;
		} finally {
			if(conn1 != null){
				PoolBDs.closeConnection(conn1);
			}
			if(conn2 != null){
				PoolBDs.closeConnection(conn2);
			}
			if(conn3 != null){
				PoolBDs.closeConnection(conn3);
			}
			if(conn4 != null){
				PoolBDs.closeConnection(conn4);
			}
			if(conn5 != null){
				PoolBDs.closeConnection(conn5);
			}
		}
		
//		if(resultado == Constantes.FALSO){
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
//		}
		
		
		KeyLog keyLog = new KeyLog("Indicador MKP", trama.getNotaVenta().getIndicadorMkp());
		
		logger.traceInfo("creacionTransaccionesDteB", "Indicador MKP Solo Ripley", 
				keyLog);			
			
		logger.endTrace("creacionTransaccionesDteB", "Finalizado", "boolean: " + String.valueOf(resultado));
		
		
		return resultado;
		
		
	}
	
	@Override
	public boolean actualizarNotaVenta(Long correlativoVenta, Integer estado, Integer nrocaja){
		logger.initTrace("actualizarNotaVenta", "Integer correlativoVenta: " + correlativoVenta + " Integer estado: "+estado,
						new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO),
						new KeyLog("Estado", estado+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA(?,?,?,?,?)}";
				logger.traceInfo("actualizarNotaVenta", "Actualiza Nota Venta SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA("+estado+","+correlativoVenta+","+((!Validaciones.validaInteger(nrocaja))?null:nrocaja)+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, estado);
				cs.setLong(2, correlativoVenta);
				if (!Validaciones.validaInteger(nrocaja)){
					cs.setNull(3, OracleTypes.NUMBER);
				} else {
					cs.setInt(3, nrocaja);
				}
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.execute();
				rsOut = cs.getInt(4);
				rsMsjOut = cs.getString(5);
				logger.traceInfo("actualizarNotaVenta", "Resultados de CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA: outCode: "+rsOut+" outMsj: "+rsMsjOut, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				respuesta = (rsOut == Constantes.NRO_CERO);
			} catch (Exception e) {
				logger.traceError("actualizarNotaVenta", "Error al actualizar nota de venta en SP CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA: ", e);
				respuesta = Constantes.FALSO;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("actualizarNotaVenta", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("actualizarNotaVenta", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("actualizarNotaVenta", "Finalizado", "boolean: " + String.valueOf(respuesta));

		return respuesta;
	}
	
	@Override
	public boolean actualizarNotaVenta(Long correlativoVenta, Integer estado, Integer nrocaja, Integer sucursal){
		logger.initTrace("actualizarNotaVenta", "Integer correlativoVenta: " + correlativoVenta + " Integer estado: "+estado+"Integer sucursal: "+sucursal,
						new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO),
						new KeyLog("Estado", estado+Constantes.VACIO),
						new KeyLog("Sucursal", sucursal+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA(?,?,?,?,?,?)}";
				logger.traceInfo("actualizarNotaVenta", "Actualiza Nota Venta SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA("+estado+","+correlativoVenta+","+nrocaja+", "+sucursal+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, estado);
				cs.setLong(2, correlativoVenta);
				cs.setInt(3, nrocaja);
				cs.setInt(4, sucursal);
				cs.registerOutParameter(5, OracleTypes.NUMBER);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.execute();
				rsOut = cs.getInt(5);
				rsMsjOut = cs.getString(6);
				logger.traceInfo("actualizarNotaVenta", "Resultados de CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA: outCode: "+rsOut+" outMsj: "+rsMsjOut, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				respuesta = (rsOut == Constantes.NRO_CERO);
			} catch (Exception e) {
				logger.traceError("actualizarNotaVenta", "Error al actualizar nota de venta en SP CAVIRA_MANEJO_BOLETAS.PROC_ACT_ESTADO_NOTA_VENTA: ", e);
				respuesta = Constantes.FALSO;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("actualizarNotaVenta", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("actualizarNotaVenta", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("actualizarNotaVenta", "Finalizado", "boolean: " + String.valueOf(respuesta));

		return respuesta;
	}
	
	@SuppressWarnings("unused")
	@Override
	public boolean buscaFormaDePago(Parametros parametros, TramaDTE trama){
		boolean resultado = Constantes.VERDADERO;
		logger.initTrace("buscaFormaDePago", "Parametros: parametros - TramaDTE: trama",
				new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO));
		Connection conn = null;
		Connection conn1 = null;
		try{
			if(Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())){
				// Timestamp vFechaPriVcto = (trama.getTarjetaRipley().getFechaPrimerVencto() != null)?trama.getTarjetaRipley().getFechaPrimerVencto():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
				Integer veRutTitular = trama.getTarjetaRipley().getRutTitular();
				int vSnumDiaPago = Constantes.NRO_CERO;
				int vSnumCtlSt = Constantes.NRO_CERO;
				String query = "{CALL CRTASA_PRC_BUS_DIA_PAG(?,?,?)}"; // TODO si se cae poner nombre de esquema (CREDI_ADM)
				logger.traceInfo("buscaFormaDePago", "Busca dia de pago SP[CAR]: CRTASA_PRC_BUS_DIA_PAG("+veRutTitular+",outCode1,outCode2)", new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
				conn = PoolBDs.getConnection(BDType.CAR);
				CallableStatement cs = null;
				if (conn != null) {
					try{
						cs = conn.prepareCall(query);
						cs.setInt(1, veRutTitular);
						cs.registerOutParameter(2, OracleTypes.NUMBER);
						cs.registerOutParameter(3, OracleTypes.NUMBER);
						cs.execute();
						logger.traceInfo("buscaFormaDePago", "Resultados de CRTASA_PRC_BUS_DIA_PAG: outCode1: "+cs.getInt(2)+" outCode2: "+cs.getInt(3), new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
						vSnumCtlSt = cs.getInt(3);
						if(vSnumCtlSt == Constantes.NRO_CERO){
							vSnumDiaPago = cs.getInt(2);
						}
						cs.close();
					} catch (Exception e) {
						logger.traceError("buscaFormaDePago",
								"Se produjo un error en la ejecucion del SP CRTASA_PRC_BUS_DIA_PAG: ", e);
					}
				}else{
					logger.traceInfo("buscaFormaDePago", "Problemas de conexion a la base de datos CAR");
					throw new AligareException("Error: Problemas de conexion a la base de datos CAR");
				}
				
				Timestamp vFecha = (trama.getNotaVenta().getFechaHoraCreacion() != null)?trama.getNotaVenta().getFechaHoraCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
				Integer veMesven = (trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO;
				Integer veProducto = new Integer(Constantes.NRO_CINCUENTA);
				Integer veComercio = new Integer(Constantes.NRO_UNO);
				BigDecimal vMonto = trama.getTarjetaRipley().getMontoCapital();
				// Integer vNroMesesDiferido = new Integer(Constantes.NRO_CINCO);
				Integer mesesDif = new Integer(Constantes.NRO_CERO);
				query = "{CALL ? := F_MESES_DIFERIDO(TO_DATE(?,?),?,?,?)}";
				logger.traceInfo("buscaFormaDePago", "Busca dia de pago FN[CAR]: outCode = F_MESES_DIFERIDO(TO_DATE("+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, vFecha)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"),"+veMesven+",50,"+veRutTitular+")",  new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
				
				if (conn != null) {
					try{
						cs = conn.prepareCall(query);
						cs.registerOutParameter(1, OracleTypes.NUMBER);
						cs.setString(2, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, vFecha));
						cs.setString(3, Constantes.FECHA_DDMMYYYY_HHMMSS2);
						cs.setInt(4, veMesven);
						cs.setInt(5, 50);
						cs.setInt(6, veRutTitular);
						cs.executeUpdate();
						mesesDif = new Integer(cs.getInt(1));
						logger.traceInfo("buscaFormaDePago", "Meses Diferido[outCode]: "+mesesDif, new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
						cs.close();
					} catch (Exception e) {
						logger.traceError("buscaFormaDePago",
								"Se produjo un error en la ejecucion de la FN F_MESES_DIFERIDO: ", e);
					}
				}else{
					logger.traceInfo("buscaFormaDePago", "Problemas de conexion a la base de datos CAR");
					throw new AligareException("Error: Problemas de conexion a la base de datos CAR");
				}
								
				if(Validaciones.validaInteger(trama.getTarjetaRipley().getTvtriMntMntFnd()) && trama.getTarjetaRipley().getTvtriMntMntFnd().intValue() == Constantes.NRO_CERO){
					query = "{CALL FINANCIA_NEW(?,4,1,50,?,?,?,'','',null,?,?,?,?,1,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
					Integer vsStatus = null;
					Integer vsValorFinanciado = null;
					String vsTasaCar = Constantes.VACIO;
					String vsTasaFinal = Constantes.VACIO;
					Integer vsMontoGastos = null;
					Integer vPlazo = trama.getTarjetaRipley().getPlazo();
					// Integer vsFactorDiferido = null;
					// Integer snumVsValorCuota = null;
					// Integer snumVsFechaVencimiento = null;
					
					if (conn != null) {
						try{
							logger.traceInfo("buscaFormaDePago", "Ejecucion de SP[CAR]: FINANCIA_NEW("+veRutTitular+",4,1,50,"+veProducto+","+veComercio+","+trama.getNotaVenta().getNumeroSucursal()+",'','',null,"+FechaUtil.formatTimestamp(Constantes.FECHA_YYYYMMDD3, vFecha)+
									","+vPlazo+","+vMonto+","+veMesven+",1,inOut[0],outCode1,outCode2,outCode3,outCode4,outCode5,outMsj1,outCode6,outCode7,outCode8,outCode9,outCode10,outCode11,"+vSnumDiaPago+",outCode12,outCode13,outCode14,outCode15,outCode16,outCode17)", new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
							         
							cs = conn.prepareCall(query);
							cs.setInt(1, veRutTitular);
							cs.setInt(2, veProducto);
							cs.setInt(3, veComercio);
							cs.setInt(4, trama.getNotaVenta().getNumeroSucursal());
							cs.setInt(5, Integer.parseInt(FechaUtil.formatTimestamp(Constantes.FECHA_YYYYMMDD3, vFecha)));
							cs.setInt(6, vPlazo);
							cs.setBigDecimal(7, vMonto);
							cs.setInt(8, veMesven);
							cs.setInt(9, Constantes.NRO_CERO);
							cs.registerOutParameter(9, OracleTypes.NUMBER);
							cs.registerOutParameter(10, OracleTypes.FLOAT);
							cs.registerOutParameter(11, OracleTypes.FLOAT);
							cs.registerOutParameter(12, OracleTypes.FLOAT);
							cs.registerOutParameter(13, OracleTypes.FLOAT);
							cs.registerOutParameter(14, OracleTypes.FLOAT);
							cs.registerOutParameter(15, OracleTypes.VARCHAR);
							cs.registerOutParameter(16, OracleTypes.NUMBER);
							cs.registerOutParameter(17, OracleTypes.NUMBER);
							cs.registerOutParameter(18, OracleTypes.NUMBER);
							cs.registerOutParameter(19, OracleTypes.NUMBER);
							cs.registerOutParameter(20, OracleTypes.NUMBER);
							cs.registerOutParameter(21, OracleTypes.NUMBER);
							cs.setInt(22, vSnumDiaPago);
							cs.registerOutParameter(23, OracleTypes.NUMBER);
							cs.registerOutParameter(24, OracleTypes.NUMBER);
							cs.registerOutParameter(25, OracleTypes.NUMBER);
							cs.registerOutParameter(26, OracleTypes.NUMBER);
							cs.registerOutParameter(27, OracleTypes.NUMBER);
							cs.registerOutParameter(28, OracleTypes.NUMBER);
							cs.execute();
							vsTasaFinal = String.valueOf(cs.getFloat(13));
							vsTasaCar = String.valueOf(cs.getFloat(14));
							vsStatus = new Integer(cs.getInt(16));
							vsValorFinanciado = new Integer(cs.getInt(19));
							// vsFactorDiferido = new Integer(cs.getInt(17));
							vsMontoGastos = new Integer(cs.getInt(21));
							// snumVsFechaVencimiento = new Integer(cs.getInt(27));
							// snumVsValorCuota = new Integer(cs.getInt(28));
							logger.traceInfo("buscaFormaDePago", "Resultados de FINANCIA_NEW: outCode4: "+vsTasaFinal+" outCode5: "+vsTasaCar+" outCode7: "+vsStatus+" outCode10: "+vsValorFinanciado+" outCode12: "+vsMontoGastos, new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
							cs.close();
						} catch (Exception e) {
							logger.traceError("buscaFormaDePago",
									"Se produjo un error en la ejecucion de la SP FINANCIA_NEW: ", e);
						}
					}else{
						logger.traceInfo("buscaFormaDePago", "Problemas de conexion a la base de datos CAR");
						throw new AligareException("Error: Problemas de conexion a la base de datos CAR");
					}
					
					Integer sal = null;
					String sal2 = Constantes.VACIO;
					Integer impCostoEfectivo = new Integer(parametros.buscaValorPorNombre("IMPRIME_COSTO_EFECTIVO"));
					if(Validaciones.validaInteger(vsStatus) && vsStatus.intValue() == Constantes.NRO_CERO){
						query = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_TAR_RIPLEY(?,?,?,?,?,?,?,?)}";
						logger.traceInfo("buscaFormaDePago", "ACTUALIZA INFO EN TABLA TARJETA_RIPLEY SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_TAR_RIPLEY("+vsValorFinanciado+","+(vsTasaCar + Constantes.PORCENTAJE)+","+(vsTasaFinal + Constantes.PORCENTAJE)+","+vsMontoGastos+","+impCostoEfectivo+","+trama.getNotaVenta().getCorrelativoVenta()+",outCode,outMsj)", new KeyLog("Correlativo venta", trama.getTarjetaRipley().getCorrelativoVenta()+Constantes.VACIO));
						conn1 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
						if (conn1 != null) {
							try{
								cs = conn1.prepareCall(query);
								cs.setInt(1, vsValorFinanciado);
								cs.setString(2, vsTasaCar + Constantes.PORCENTAJE);
								cs.setString(3, vsTasaFinal + Constantes.PORCENTAJE);
								cs.setInt(4, vsMontoGastos);
								cs.setInt(5, impCostoEfectivo);
								cs.setLong(6, trama.getNotaVenta().getCorrelativoVenta());
								cs.registerOutParameter(7, OracleTypes.NUMBER);
								cs.registerOutParameter(8, OracleTypes.VARCHAR);
								cs.execute();
								sal = new Integer(cs.getInt(7));
								sal2 = cs.getString(8);
								cs.close();
								if(sal.intValue() != Constantes.NRO_CERO){
									throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
								}
							} catch (Exception e) {
								logger.traceError("buscaFormaDePago",
										"Se produjo un error en la ejecucion de la SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_TAR_RIPLEY: ", e);
								resultado = Constantes.FALSO;
							}
						}else{
							logger.traceInfo("buscaFormaDePago", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
							throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
						}
					}
					
					/*
					Integer vTotalPagar = null;
					Integer vPrimerMes = null;
					BigDecimal vValorCuota = null;
					Integer vTotalCredito = null;
					Integer vMes2A03 = null;
					Integer vGastos = null;
					BigDecimal vImporteMes = null;
					Integer vTasaCar = null;
					Integer vDiferido = null;
		            Timestamp fechaPrimerVcto = vFechaPriVcto;
		            BigDecimal vTasaMensualDiferido = null;
		            
					if(Validaciones.validaInteger(vPlazo) && vPlazo.intValue() > Constantes.NRO_UNO) {
						vTotalCredito = (Validaciones.validaInteger(vsValorFinanciado))?vsValorFinanciado:new Integer(Constantes.NRO_CERO);
						vGastos = (Validaciones.validaInteger(vsMontoGastos))?vsMontoGastos:new Integer(Constantes.NRO_CERO);
						vValorCuota = new BigDecimal((vTotalCredito.intValue() - vGastos.intValue()) / vPlazo.intValue());
						vTasaCar = (!Validaciones.validarVacio(vsTasaCar))?new Integer(vsTasaCar):null;
				        vDiferido = (Validaciones.validaInteger(veMesven))?veMesven:new Integer(Constantes.NRO_CERO);
				        
				        if(Validaciones.validaInteger(mesesDif) && mesesDif.intValue() == Constantes.NRO_UNO){
				        	vTasaMensualDiferido = new BigDecimal((((Validaciones.validaInteger(vsFactorDiferido))?vsFactorDiferido.intValue():Constante.NRO_UNO) - Constante.NRO_UNO) * Constante.NRO_CIEN);
				        }else{
				        	Integer vX = new Integer(((Validaciones.validaInteger(mesesDif))?mesesDif.intValue():Constantes.NRO_CERO) - Constante.NRO_UNO);
				        	vTasaMensualDiferido = new BigDecimal((vX.intValue() > Constantes.NRO_CERO)?(Math.round(((Math.pow(((Validaciones.validaInteger(vsFactorDiferido))?vsFactorDiferido.intValue():Constante.NRO_UNO), (Constante.NRO_UNO / vX.intValue())) - Constante.NRO_UNO) * Constante.NRO_CIEN) * 100.0) / 100.0):Constantes.NRO_CERO);
				        }        		
	
				        vTotalPagar = (Validaciones.validaInteger(vsValorFinanciado))?vsValorFinanciado:new Integer(Constantes.NRO_CERO);
				                     
				        if((vTotalPagar.intValue() % vPlazo.intValue()) != Constantes.NRO_CERO){
				        	vPrimerMes = new Integer(((Validaciones.validaInteger(snumVsValorCuota))?snumVsValorCuota.intValue():Constantes.NRO_CERO) + (vTotalPagar.intValue() % vPlazo.intValue()));
				        }else{
				        	vPrimerMes = (Validaciones.validaInteger(snumVsValorCuota))?snumVsValorCuota:new Integer(Constantes.NRO_CERO);
				        }
				                      
				        vMes2A03 = (Validaciones.validaInteger(snumVsValorCuota))?snumVsValorCuota:new Integer(Constantes.NRO_CERO);
				        fechaPrimerVcto = (snumVsFechaVencimiento != null)?FechaUtil.stringYYYYMMDDToTimestamp(snumVsFechaVencimiento.toString(), Constantes.FECHA_YYYYMMDD3):vFechaPriVcto;
				        vImporteMes = ((vGastos.intValue() % vPlazo.intValue()) == Constantes.NRO_CERO)?new BigDecimal((vGastos.intValue() / vPlazo.intValue())):new BigDecimal(((vGastos.intValue() / vPlazo.intValue()) + (vGastos.intValue() % vPlazo.intValue())));
					}else if(Validaciones.validaInteger(trama.getTarjetaRipley().getPlazo()) && trama.getTarjetaRipley().getPlazo().intValue() == Constantes.NRO_UNO){
						vTotalPagar = vMonto;
			            vPrimerMes = vMonto;
			            vValorCuota = (Validaciones.validaInteger(vMonto))?new BigDecimal(vMonto.intValue()):null;
			            vTotalCredito = vMonto;
			            vMes2A03 = new Integer(Constantes.NRO_CERO);
			            vGastos = new Integer(Constantes.NRO_CERO);
			            vImporteMes = new BigDecimal(Constantes.NRO_CERO);
			            fechaPrimerVcto = vFechaPriVcto;
					}else{
						vImporteMes = new BigDecimal(Constantes.NRO_CERO);
			            vTotalPagar = new Integer(Constantes.NRO_CERO);
			            vPrimerMes = new Integer(Constantes.NRO_CERO);
			            vMes2A03 = new Integer(Constantes.NRO_CERO);
			            vGastos = new Integer(Constantes.NRO_CERO);
					}
					
					String vFormaPago = Constantes.TRAMA_TARJETA_RIPLEY;
					*/				       
				}
			}
		} catch (AligareException e) {
			logger.traceError("buscaFormaDePago", e);
			logger.endTrace("buscaFormaDePago", "Finalizado", "boolean: AligareException");
			throw e;
		} catch (Exception ex) {
			logger.traceError("buscaFormaDePago", "Se produjo un error inesperado: ", ex);
			resultado = Constantes.FALSO;
		} finally {
			if(conn != null){
				PoolBDs.closeConnection(conn);
			}
			if(conn1 != null){
				PoolBDs.closeConnection(conn1);
			}
		}
	
		logger.endTrace("buscaFormaDePago", "Finalizado", "boolean: " + String.valueOf(resultado));
		
		return resultado;
	}
	 
	@Override
	public boolean creacionRecaudacionesCDF(Parametros parametros, TramaDTE trama, Integer nroTransaccion, String codUnico, Integer boleta) throws AligareException{
		boolean resultado = Constantes.VERDADERO;
		logger.initTrace("creacionRecaudacionesCDF",
				"Parametros parametros: " + parametros.toString() + " - TramaDTE: trama - Integer nroTransaccion: " + nroTransaccion
						+ " String codUnico: " + codUnico + " - Integer boleta: " + boleta,
				new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO),
				new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("codUnico", codUnico)); 
		//String fechaAnterior = Constantes.VACIO;
	    //String fechaCompara = Constantes.VACIO;
		//String vFechaBT = Constantes.VACIO;
//		int tipoDePago = obtieneTipoDePago(trama);
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		ClubDAO clubDAO = new ClubDAOImpl();
		String fecha = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros)/*FechaUtil.getCurrentDateString()*/;
//		String fechaHora = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, parametros);
		String hms = caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros)/*FechaUtil.getCurrentHoursString()*/;
		String vendedor = parametros.buscaValorPorNombre("RUT_VENDEDOR");
		String supervisor = parametros.buscaValorPorNombre("RUT_SUPERVISOR");
		String plTipoPago = parametros.buscaValorPorNombre("PL_TIPO_PAGO");
		int sucursal = Integer.parseInt(parametros.buscaValorPorNombre("SUCURSAL"));
		int nroCaja = Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA"));
		Integer pAdministradora = new Integer(parametros.buscaValorPorNombre("ADMINISTRADORA"));
		Integer pEmisor = new Integer(parametros.buscaValorPorNombre("EMISOR"));
		Integer pTarjeta = new Integer(parametros.buscaValorPorNombre("TARJETA"));
//		int funcion = Integer.parseInt(parametros.buscaValorPorNombre("FUNCION"));
		Long correlativo = trama.getNotaVenta().getCorrelativoVenta();
		//Integer paramCFTValor = new Integer(parametros.buscaValorPorNombre("PARAM_CFT"));
		Integer sal = null;
	    String sal2 = Constantes.VACIO;
	    String query1 = Constantes.VACIO;
		String query2 = Constantes.VACIO;
		String query3 = Constantes.VACIO;
		String query4 = Constantes.VACIO;
		String query5 = Constantes.VACIO;
		CallableStatement cs = null;
		Connection conn1 = null;
		Connection conn2 = null;
		Connection conn3 = null;
		Connection conn4 = null;
		Connection conn5 = null;
		String wsCodAuto = Constantes.VACIO;
		//int tipo = Constantes.NRO_UNO;
		int tipoReg = Constantes.NRO_CERO;
		String formaPago = Constantes.VACIO;
		try{
			if(Validaciones.validaLong(trama.getNotaVenta().getCorrelativoVenta())){
				Long rutTransbank = trama.getNotaVenta().getRutComprador();
				//Integer rutEjecutivoVta = (!Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta()))?new Integer(trama.getNotaVenta().getEjecutivoVta()):new Integer(Constantes.NRO_CERO);
				String rutEjecutivoVta = (!Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta()))?trama.getNotaVenta().getEjecutivoVta().trim():Constantes.STRING_CERO;
				if(Validaciones.validaLong(trama.getTarjetaBancaria().getCorrelativoVenta())){
					formaPago = obtieneFormaPago(trama);
					Integer wsTipo = trama.getTarjetaBancaria().getTipoTarjeta();
					BigDecimal total = trama.getTarjetaBancaria().getMontoTarjeta();
					wsCodAuto = trama.getTarjetaBancaria().getCodigoAutorizador();
					String vd = (!Validaciones.validarVacio(trama.getTarjetaBancaria().getVd()))?trama.getTarjetaBancaria().getVd():Constantes.BOL_VALOR_VD_N;
					
					//if(!Validaciones.validarVacio(plTipoPago) && (!Constantes.STRING_UNO.equals(plTipoPago) || Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd))){
						
					if((!Validaciones.validarVacio(plTipoPago) 
							&& (!Constantes.STRING_UNO.equals(plTipoPago) || Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)))
							&& (!Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso()))){
					
						query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'',?,?,0,?,0,0,0,?,?,?,?)}";
						logger.traceInfo("creacionRecaudacionesCDF", "Creacion T.Bancaria BackOffice["+formaPago+"] - SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'',"+
						((Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd) && !Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso()))?Constantes.BOL_VALOR_VD_D:Constantes.BOL_VALOR_VD_C)+","+rutTransbank+",0,"+total+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
						if (conn1 != null) {
							try {
								cs = conn1.prepareCall(query1);
								cs.setString(1, fecha);
								cs.setInt(2, sucursal);
								cs.setInt(3, nroCaja);
								cs.setInt(4, nroTransaccion);
								cs.setString(5, (Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)
										//&& !Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())) 
										? Constantes.BOL_VALOR_VD_D : Constantes.BOL_VALOR_VD_C));
								cs.setLong(6, rutTransbank);
								cs.setBigDecimal(7, total);
								cs.setString(8, wsCodAuto);
								cs.setString(9, codUnico);
								cs.registerOutParameter(10, OracleTypes.NUMBER);
								cs.registerOutParameter(11, OracleTypes.VARCHAR);
								cs.execute();
								sal = new Integer(cs.getInt(10));
								sal2 = cs.getString(11);
								cs.close();
							} catch (Exception e) {
								logger.traceError("creacionRecaudacionesCDF",
										"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
								resultado = Constantes.FALSO; 
							}
						} else {
							logger.traceInfo("creacionRecaudacionesCDF",
									"Problemas de conexion a la base de datos BACK OFFICE");
							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
						}
					} else if (Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())) { // TARJETA MERCADO PAGO
						query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'MP',?,?,0,?,0,0,0,?,?,?,?)}";
						logger.traceInfo("creacionRecaudacionesCDF", "TARJETA MERCADO PAGO["+formaPago+"] SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'MP',"+Constantes.BOL_VALOR_VD_C+","+rutTransbank+",0,"+total+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
						if (conn1 != null) {
							try {
								cs = conn1.prepareCall(query1);
								cs.setString(1, fecha);
								cs.setInt(2, sucursal);
								cs.setInt(3, nroCaja);
								cs.setInt(4, nroTransaccion);
								cs.setString(5, Constantes.BOL_VALOR_VD_C);
								cs.setLong(6, rutTransbank);
								cs.setBigDecimal(7, total);
								cs.setString(8, wsCodAuto);
								cs.setString(9, codUnico);
								cs.registerOutParameter(10, OracleTypes.NUMBER);
								cs.registerOutParameter(11, OracleTypes.VARCHAR);
								cs.execute();
								sal = new Integer(cs.getInt(10));
								sal2 = cs.getString(11);
								cs.close();
							} catch (Exception e) {
								logger.traceError("creacionRecaudacionesCDF",
										"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
								resultado = Constantes.FALSO;
							}
						} else {
							logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
						}
					
					} else {
						query1 = "{CALL ? := INSERTA_BANCARIA_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?)}";
						logger.traceInfo("creacionRecaudacionesCDF", "TARJETA BANCARIA CREDITO["+formaPago+"] FN[BACK_OFFICE]: outCode = INSERTA_BANCARIA_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+wsTipo+","+total+","+wsCodAuto+")", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
						if (conn1 != null) {
							try {
								cs = conn1.prepareCall(query1);
								cs.registerOutParameter(1, OracleTypes.NUMBER);
								cs.setString(2, fecha);
								cs.setInt(3, sucursal);
								cs.setInt(4, nroCaja);
								cs.setInt(5, nroTransaccion);
								cs.setInt(6, wsTipo);
								cs.setBigDecimal(7, total);
								cs.setString(8, wsCodAuto);
								cs.executeUpdate();
								sal = new Integer(cs.getInt(1));
								logger.traceInfo("creacionRecaudacionesCDF", "FN INSERTA_BANCARIA_BACK[outCode]: "+sal, new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
								cs.close();
							} catch (Exception e) {
								logger.traceError("creacionRecaudacionesCDF",
										"Se produjo un error en la ejecucion de la FN INSERTA_BANCARIA_BACK: ", e);
								resultado = Constantes.FALSO; 
							}
						} else {
							logger.traceInfo("creacionRecaudacionesCDF",
									"Problemas de conexion a la base de datos BACK OFFICE");
							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
						}
					}
					
					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
						// tipo = Constantes.NRO_CERO;
						logger.traceError("creacionRecaudacionesCDF",
								"Imposible crear forma de Pago Bancaria Error: ", new AligareException("outCode: " + sal + " outMsj: "+ sal2));
						resultado = Constantes.FALSO; 
					}
				}
				// Creacion Tarjeta Regalo Empresa
//				if (Validaciones.validaLong(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())) {
//					formaPago = obtieneFormaPago(trama);
//					Integer emisorTRE = trama.getTarjetaRegaloEmpresa().getEmisor();
//					Integer admTRE = trama.getTarjetaRegaloEmpresa().getAdministradora();
//					Integer tarjetaTRE = trama.getTarjetaRegaloEmpresa().getTarjeta();
//					Integer codAutorizadorTRE = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();
//					//Integer rutClienteTRE = trama.getTarjetaRegaloEmpresa().getRutCliente();
//					//String dvClienteTRE = trama.getTarjetaRegaloEmpresa().getDvCliente();
//					Long correVenta = trama.getTarjetaRegaloEmpresa().getCorrelativoVenta();
//					Integer montoTRE = trama.getTarjetaRegaloEmpresa().getMonto();
//					Integer nroTarjetaTRE = trama.getTarjetaRegaloEmpresa().getNroTarjeta();
//					long rutCliente = (Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getRutCliente()))?trama.getTarjetaRegaloEmpresa().getRutCliente().longValue():Constantes.NRO_CERO;
//					rutCliente = Long.parseLong((rutCliente + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente))));
//					Integer flagTRE = trama.getTarjetaRegaloEmpresa().getFlag();
//					//tipo = Constantes.NRO_UNO;
//					String WFECHA = fecha.replaceAll(Constantes.SLASH, Constantes.VACIO);
//					query4 = "{CALL FEGIFE_PRC_INS_TREINT(?,'IB',?,?,?,?,?,?)}";
//					logger.traceInfo("creacionRecaudacionesCDF", "TARJETA REGALO EMPRESA["+formaPago+"] SP[TRE] FEGIFE_PRC_INS_TREINT("+nroTarjetaTRE+",'IB',"+(montoTRE.intValue() * Constantes.NRO_MENOSUNO)+","+correVenta+","+nroCaja+","+WFECHA+",outMsj,outCode)", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					conn4 = PoolBDs.getConnection(BDType.TRE);
//					if (conn4 != null) {
//						try {
//							cs = conn4.prepareCall(query4);
//							cs.setInt(1, nroTarjetaTRE);
//							cs.setInt(2, (montoTRE.intValue() * Constantes.NRO_MENOSUNO)); // TODO DUDA N1
//							cs.setInt(3, correVenta);
//							cs.setInt(4, nroCaja);
//							cs.setString(5, WFECHA);
//							cs.registerOutParameter(6, OracleTypes.VARCHAR);
//							cs.registerOutParameter(7, OracleTypes.NUMBER);
//							cs.execute();
//							sal = new Integer(cs.getInt(7));
//							sal2 = cs.getString(6);
//							cs.close();
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//								logger.traceError("creacionRecaudacionesCDF",
//										"Imposible Registrar forma de Pago TARJETA REGALO EMPRESA Error - ",
//										new AligareException("outCode: "+sal+" outMsj: "+sal2));
//								
//								resultado = Constantes.FALSO;
//							}
//						} catch (Exception e) {
//							logger.traceError("creacionRecaudacionesCDF",
//									"Se produjo un error en la ejecucion del SP FEGIFE_PRC_INS_TREINT: ", e);
//							
//							resultado = Constantes.FALSO;
//						}
//					} else {
//						logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos TRE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos TRE");
//					}
//					
//					query1 = "{CALL ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
//					logger.traceInfo("creacionRecaudacionesCDF", "TARJETA REGALO EMPRESA BO["+formaPago+"] SP[BACK_OFFICE] ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+admTRE+","+emisorTRE+","+tarjetaTRE+","+codAutorizadorTRE+","+rutCliente+","+montoTRE+","+nroTarjetaTRE+","+flagTRE+",outCode,outMsj)", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					if (conn1 == null) {
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					}
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.setString(1, fecha);
//							cs.setInt(2, sucursal);
//							cs.setInt(3, nroCaja);
//							cs.setInt(4, nroTransaccion);
//							cs.setInt(5, admTRE);
//							cs.setInt(6, emisorTRE);
//							cs.setInt(7, tarjetaTRE);
//							cs.setInt(8, codAutorizadorTRE);
//							cs.setLong(9, rutCliente);
//							cs.setInt(10, montoTRE);
//							cs.setInt(11, nroTarjetaTRE);
//							cs.setInt(12, flagTRE);
//							cs.registerOutParameter(13, OracleTypes.NUMBER);
//							cs.registerOutParameter(14, OracleTypes.VARCHAR);
//							cs.execute();
//							sal = new Integer(cs.getInt(13));
//							sal2 = cs.getString(14);
//							cs.close();
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) { // if (Validaciones.validaInteger(sal) && sal.intValue() < Constantes.NRO_UNO)
//								// tipo = Constantes.NRO_CERO;
//								logger.traceError("creacionRecaudacionesCDF",
//										"Imposible crear forma de Pago TARJETA REGALO EMPRESA BO Error - ",
//										new AligareException("outCode: "+sal+" outMsj: "+sal2));
//								
//								resultado = Constantes.FALSO; 
//							}
//						} catch (Exception e) {
//							logger.traceError("creacionRecaudacionesCDF",
//									"Se produjo un error en la ejecucion del SP ACTUALIZA_TAR_REGALO_EMPRESA: ", e);
//							resultado = Constantes.FALSO; 
//						}
//					}else{
//						logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//					
//				}
				// Creacion Tarjeta Ripley
				if(Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())){
					BigInteger codAutor = (trama.getTarjetaRipley().getCodigoAutorizacion() != null)?trama.getTarjetaRipley().getCodigoAutorizacion():new BigInteger(Constantes.STRING_CERO);
					BigInteger codigoPAN = trama.getTarjetaRipley().getPan();
					//BigInteger tipoCodAuto = null;
					formaPago = obtieneFormaPago(trama);
					pAdministradora = trama.getTarjetaRipley().getAdministradora();
					pEmisor = trama.getTarjetaRipley().getEmisor();
					pTarjeta = trama.getTarjetaRipley().getTarjeta();
					//tipo = Constantes.NRO_DOS;
					query1 = "{CALL ? := INSERTA_RIPLEY_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,?)}";
					logger.traceInfo("creacionRecaudacionesCDF", "Creacion T.Ripley["+formaPago+"] BackOffice FN[BACK_OFFICE]: outCode = INSERTA_RIPLEY_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+pAdministradora+","+pEmisor+","+pTarjeta+","+trama.getTarjetaRipley().getRutTitular()+","+((trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO)+","+
					trama.getTarjetaRipley().getPlazo()+","+((trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO)+","+trama.getTarjetaRipley().getMontoCapital()+","+((trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie():Constantes.NRO_CERO)+","+((trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar():Constantes.NRO_CERO)+","+
							((trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO)+","+((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)?new BigDecimal(codAutor):new BigDecimal(((Validaciones.validaBigInteger(codAutor)?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2)))+",0,"+
							((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)? (codigoPAN != null)?new BigDecimal(Util.rellenarString(codigoPAN.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null:(codigoPAN != null)?new BigDecimal(codigoPAN.toString()):null)+")", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
					if (conn1 == null) {
						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
					}
					if (conn1 != null) {
						try {
							cs = conn1.prepareCall(query1);
							cs.registerOutParameter(1, OracleTypes.NUMBER);
							cs.setString(2, fecha);
							cs.setInt(3, sucursal);
							cs.setInt(4, nroCaja);
							cs.setInt(5, nroTransaccion);
							cs.setInt(6, pAdministradora);
							cs.setInt(7, pEmisor);
							cs.setInt(8, pTarjeta);
							cs.setInt(9, trama.getTarjetaRipley().getRutTitular());
							cs.setInt(10, (trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO);
							cs.setInt(11, trama.getTarjetaRipley().getPlazo());
							cs.setInt(12, (trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO);
							cs.setBigDecimal(13, trama.getTarjetaRipley().getMontoCapital());
							cs.setBigDecimal(14, (trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie(): new BigDecimal(Constantes.NRO_CERO));
		
							//tipoCodAuto = (paramCFTValor.intValue() == Constantes.NRO_CERO)?codigoPAN:new BigInteger(Constantes.STRING_CERO);
		                  
							cs.setBigDecimal(15, (trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar(): new BigDecimal(Constantes.NRO_CERO));
							cs.setInt(16, (trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO);
							cs.setBigDecimal(17,
									((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
											&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
													? new BigDecimal(codAutor)
													: new BigDecimal(((Validaciones.validaBigInteger(codAutor)?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2))));
							cs.setBigDecimal(18,
									((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
											&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
													? (codigoPAN != null)?new BigDecimal(Util.rellenarString(codigoPAN.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null
													: (codigoPAN != null)?new BigDecimal(codigoPAN.toString()):null));
							
							cs.executeUpdate();
							sal = new Integer(cs.getInt(1));
							logger.traceInfo("creacionRecaudacionesCDF", "FN INSERTA_RIPLEY_BACK[outCode]: "+sal, new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
							cs.close();
							
							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
								// tipo = Constantes.NRO_CERO;
								logger.traceError("creacionRecaudacionesCDF",
										"Imposible crear forma de pago Ripley BO Error - ",
										new AligareException("outCode: "+sal));
								resultado = Constantes.FALSO; 
							}
						} catch (Exception e) {
							logger.traceError("creacionRecaudacionesCDF",
									"Se produjo un error en la ejecucion de la FN INSERTA_RIPLEY_BACK: ", e);
							resultado = Constantes.FALSO; 
						}
					}else{
						logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
					}

				}
				
				long rutDespachoCambio = Constantes.NRO_CERO;
				if(Validaciones.validaLong(trama.getDespacho().getCorrelativoVenta())){
					if(Validaciones.validaInteger(trama.getDespacho().getRutDespacho()) && trama.getDespacho().getRutDespacho().intValue() > Constantes.NRO_CERO){
						rutDespachoCambio = trama.getDespacho().getRutDespacho().longValue();
					}else{
						rutDespachoCambio = (Validaciones.validaInteger(trama.getDespacho().getRutCliente()))?trama.getDespacho().getRutCliente().longValue():Constantes.NRO_CERO;
					}
					Long correlativoVenta = trama.getDespacho().getCorrelativoVenta();
				    long rutDespacho = Long.parseLong((rutDespachoCambio + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutDespachoCambio))));
				    //String nombreDespacho = Util.cambiaCaracteres(trama.getDespacho().getNombreDespacho());

				    Timestamp fechaDespacho = (trama.getDespacho().getFechaDespacho() != null)?trama.getDespacho().getFechaDespacho():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
				    //Integer sucursalDespacho = trama.getDespacho().getSucursalDespacho();
				    //Integer comunaDespacho = trama.getDespacho().getComunaDespacho();
				    //Integer regionDespacho = trama.getDespacho().getRegionDespacho();
				    String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
				    //String telefonoDespacho = Util.cambiaCaracteres(trama.getDespacho().getTelefonoDespacho());
				    //String observacion = (trama.getDespacho().getObservacion() != null)?Util.cambiaCaracteres(trama.getDespacho().getObservacion()):"Sin observacion";    
				    long rutCliente2 = (Validaciones.validaInteger(trama.getDespacho().getRutCliente()))?trama.getDespacho().getRutCliente().longValue():Constantes.NRO_CERO;
				    long rutCliente = Long.parseLong((rutCliente2 + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente2))));
				    //String direccionCliente = Util.cambiaCaracteres(trama.getDespacho().getDireccionCliente());
				    //String telefonoCliente = Util.cambiaCaracteres(trama.getDespacho().getTelefonoCliente());
				    //Integer tipoCliente = trama.getDespacho().getTipoCliente();
				    String nombreCliente =  (((trama.getDespacho().getNombreCliente() != null)
							? trama.getDespacho().getNombreCliente() : Constantes.VACIO)
							+ Constantes.SPACE
							+ ((trama.getDespacho().getApellidoPatCliente() != null)
									? trama.getDespacho().getApellidoPatCliente() : Constantes.VACIO)
							+ Constantes.SPACE + ((trama.getDespacho().getApellidoMatCliente() != null)
									? trama.getDespacho().getApellidoMatCliente() : Constantes.VACIO));
				    nombreCliente = Util.rellenarString(Util.cambiaCaracteres(nombreCliente), 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49);
				    String nombreInvitado =  Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
				    //Integer tipoDespacho = trama.getDespacho().getTipoDespacho();
				    String emailCliente = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
				    String apellidoPaterno = (Validaciones.validarVacio(trama.getDespacho().getApellidoPatCliente()))?"Apellido paterno no descrito":trama.getDespacho().getApellidoPatCliente();
				    String apellidoMaterno = (Validaciones.validarVacio(trama.getDespacho().getApellidoMatCliente()))?"Apellido materno no descrito":trama.getDespacho().getApellidoMatCliente();
				    Long codigoRegalo = (Validaciones.validaLong(trama.getNotaVenta().getCodigoRegalo()))?trama.getNotaVenta().getCodigoRegalo():new Long(Constantes.NRO_CERO);
				    Integer tipoRegalo = (Validaciones.validaInteger(trama.getNotaVenta().getTipoRegalo()))?trama.getNotaVenta().getTipoRegalo():new Integer(Constantes.NRO_CERO);

				    Timestamp fechaVenta = (trama.getNotaVenta().getFechaCreacion() != null)?trama.getNotaVenta().getFechaCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
			        String mensajeTarjeta = (trama.getDespacho().getMensajeTarjeta() != null)?trama.getDespacho().getMensajeTarjeta():Constantes.VACIO;
			        //long vendedor2 = Long.parseLong((vendedor + Constantes.VACIO + Util.getDigitoValidadorRut2(vendedor)));
					if (trama.getArticuloVentas() != null && !trama.getArticuloVentas().isEmpty()) {
						for (ArticuloVentaTramaDTE obj : trama.getArticuloVentas()) {
							String vArticulo = obj.getArticuloVenta().getCodArticulo();
							String costoEnvio = (obj.getArticuloVenta().getDescRipley() != null)?obj.getArticuloVenta().getDescRipley():Constantes.VACIO;
						    //String jornadaDespacho = (obj.getArticuloVenta().getTipoDespacho() != null)?obj.getArticuloVenta().getTipoDespacho():Constantes.SI;
						    /*String jornadaDespachoBo = Constantes.VACIO;
						    if(Constantes.BOL_JORNADA_AM.equalsIgnoreCase(jornadaDespacho)){
						    	jornadaDespachoBo = Constantes.STRING_UNO;
						    }else if(Constantes.BOL_JORNADA_PM.equalsIgnoreCase(jornadaDespacho)){
						    	jornadaDespachoBo = Constantes.STRING_DOS;
						    }else{
						    	jornadaDespachoBo = jornadaDespacho;
						    }*/
						    boolean esExtgarantia = (Validaciones.validaInteger(obj.getArticuloVenta().getIndicadorEg())
									&& obj.getArticuloVenta().getIndicadorEg().intValue() == Constantes.NRO_TRES);
							Integer strFechaConfirma = new Integer(Constantes.NRO_CERO);  
							Integer dias = new Integer(Constantes.NRO_CERO);
							/* ESTAN CADUCOS VALIDARLOS
							if (actFechPreVenta && (vArticulo.equals("2000316773122")
									|| vArticulo.equals("2000316773177") || vArticulo.equals("2000317721399")
									|| vArticulo.equals("2000317719471"))) { 
								fechaDespacho = fechaDespachoPreVenta;
							}else
							*/
							dias = FechaUtil.diferenciaDias(FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaVenta), Constantes.FECHA_DDMMYYYY3, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho), Constantes.FECHA_DDMMYYYY3);
							if (Validaciones.validaInteger(dias) && dias.intValue() < Constantes.NRO_CERO) {
								query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA(?,?,?,?,?)}";
								logger.traceInfo("creacionRecaudacionesCDF", "ACTUALIZACION FECHA CREACION NOTA VENTA SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+correlativo+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
								conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								if (conn2 != null) {
									try {
										cs = conn2.prepareCall(query2);
										cs.setString(1, (fecha + Constantes.SPACE + hms));
										cs.setString(2, Constantes.FECHA_DDMMYYYY_HHMMSS2);
										cs.setLong(3, correlativo);
										cs.registerOutParameter(4, OracleTypes.NUMBER);
										cs.registerOutParameter(5, OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(4));
										sal2 = cs.getString(5);
										cs.close();
										if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
											throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
										}
									} catch (Exception e) {
										logger.traceError("creacionRecaudacionesCDF",
												"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA: ", e);
										resultado = Constantes.FALSO; 
									}
								} else {
									logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
							}
							
							Integer tipoRegalo2 = new Integer(Constantes.NRO_CERO);
							String esRegalo = (obj.getArticuloVenta().getEsregalo() != null)?obj.getArticuloVenta().getEsregalo():Constantes.VACIO;
							if(Constantes.NO.equalsIgnoreCase(esRegalo)){
								tipoRegalo2 = new Integer(Constantes.NRO_CERO);
								mensajeTarjeta = Constantes.SPACE;
							}else if(Constantes.SI.equalsIgnoreCase(esRegalo)){
								tipoRegalo2 = new Integer(Constantes.NRO_UNO);
								mensajeTarjeta = (obj.getArticuloVenta().getMensaje() != null)?obj.getArticuloVenta().getMensaje():Constantes.VACIO;
							}
							
							tipoRegalo = (Validaciones.validaLong(codigoRegalo) && codigoRegalo == Constantes.NRO_CERO)?tipoRegalo2:tipoRegalo;
							rutDespacho = (rutDespachoCambio == Constantes.NRO_CERO)?rutCliente:rutDespacho;
							String cud = obj.getArticuloVenta().getCodDespacho();//Constantes.SPACE;
							if(tieneCud(vArticulo).intValue() == Constantes.NRO_CERO){
								if(Constantes.SI.equalsIgnoreCase(esRegalo) && String.valueOf(Constantes.NRO_NOVENTANUEVE).equals((obj.getArticuloVenta().getMensaje() != null)?obj.getArticuloVenta().getMensaje():Constantes.VACIO)){
									tipoRegalo = new Integer(Constantes.NRO_NOVENTANUEVE);
								}
								if(Validaciones.validaLong(codigoRegalo) && codigoRegalo != Constantes.NRO_CERO && (Validaciones.validarVacio(esRegalo) || Constantes.NO.equalsIgnoreCase(esRegalo))){
									tipoRegalo = new Integer(Constantes.NRO_CERO);
								}
							}	
							
							Integer vCantPrd = (Validaciones.validaInteger(obj.getArticuloVenta().getUnidades()))?obj.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
							BigDecimal vMontoDesc = (Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento()))?obj.getArticuloVenta().getMontoDescuento():new BigDecimal(Constantes.NRO_CERO);
							BigDecimal vPrecio = (Validaciones.validaBigDecimal(obj.getArticuloVenta().getPrecio()))?obj.getArticuloVenta().getPrecio():new BigDecimal(Constantes.NRO_CERO);
							Integer vtipoDesc = (Validaciones.validaInteger(obj.getArticuloVenta().getTipoDescuento()))?obj.getArticuloVenta().getTipoDescuento():new Integer(Constantes.NRO_CERO);
							BigDecimal newPrecio = (vMontoDesc.compareTo(new BigDecimal(Constantes.NRO_CERO)) < Constantes.NRO_CERO)?vPrecio.subtract(vMontoDesc):vPrecio; // VALIDAR CALCULO
//							long vtotal = (vMontoDesc.intValue() < Constantes.NRO_CERO)?((vPrecio.intValue() - vMontoDesc.intValue()) * vCantPrd.intValue()):vPrecio.intValue() * vCantPrd.intValue();// TODO VALIDAR CALCULO
							BigDecimal vDesc = (vMontoDesc.compareTo(new BigDecimal(Constantes.NRO_CERO)) < Constantes.NRO_CERO) ? 
									new BigDecimal(Constantes.NRO_CERO) : 
								(vtipoDesc.intValue() == Constantes.NRO_CERO) ? vMontoDesc.multiply(new BigDecimal(vCantPrd)) : vMontoDesc; // TODO VALIDAR CALCULO
							
							if(Validaciones.validaLong(trama.getTarjetaBancaria().getCorrelativoVenta())){
								tipoReg = new Integer(Constantes.NRO_CUATRO);
							}else if(Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())){
								tipoReg = new Integer(Constantes.NRO_TRES);
							}else if(Validaciones.validaLong(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())){
								tipoReg = new Integer(Constantes.NRO_OCHO);
							}

							if(Validaciones.validaLong(codigoRegalo) && codigoRegalo.intValue() > Constantes.NRO_CERO){
								logger.traceInfo("creacionRecaudacionesCDF", "OC TIENE CODIGO REGALO: "+codigoRegalo.intValue(), new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo));

								if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
										&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
//								if(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
//					            		&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){ // TODO VALIDAR CONDICION
					                	String vCodigoReg = (Validaciones.validaLong(codigoRegalo))?codigoRegalo.toString():Constantes.VACIO;
										long vArticulo2 = Long.parseLong(vArticulo);
										Integer vSucursal = (sucursal + 10000);
										BigDecimal vPrcCmp = vPrecio.subtract(vDesc);//vtotal - vDesc.intValue(); //FIXME
										
								//merge .22
										BigDecimal precio = null;
										// Se calcula precio
										if (obj.getArticuloVenta().getMontoDescuento().compareTo(new BigDecimal(Constantes.NRO_CERO)) < Constantes.NRO_CERO) {
											precio = obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento());
										} else {
											//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
											precio = obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento().divide(new BigDecimal(vCantPrd)));
										}
										vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); // merge .22
								//merge .22										
										
										String vMrc = (tipoRegalo.intValue() == Constantes.NRO_NOVENTANUEVE || tipoRegalo2.intValue() == Constantes.NRO_CERO)?"< NR >":"< SR >";
										mensajeTarjeta = (tipoRegalo.intValue() == Constantes.NRO_NOVENTANUEVE)?Constantes.STRING_CERO:obj.getArticuloVenta().getMensaje();

										if (obj.getArticuloVenta().getCodArticuloMkp() == null){
						                	query3 = "{CALL LGLREG_PRC_MOV_LIS_VLR(?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,27,0,0,?,?,?,?,?,?,?,?,0,?,?,?,?,'',0,?,0,0,0,0,?,?)}";
											logger.traceInfo("creacionRecaudacionesCDF", "Creacion de REGISTRO NOVIOS SP[CLUB]: LGLREG_PRC_MOV_LIS_VLR("+vCodigoReg+","+vArticulo2+","+vSucursal+",TO_DATE("+fecha+",'DD/MM/YYYY'),"+nroTransaccion+",27,0,0,"+vCantPrd+","+vPrcCmp+","+nroTransaccion+","+nroCaja+","+tipoReg+","+obj.getArticuloVenta().getCorrelativoItem()+","+direccionDespacho+","+vMrc+",0,"+emailCliente+","+nombreInvitado+","+apellidoPaterno+","+apellidoMaterno+",'',0,"+mensajeTarjeta+",0,0,0,0,outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo2+Constantes.VACIO));
											conn3 = PoolBDs.getConnection(BDType.CLUB);
											if (conn3 != null) {
												try {
													cs = conn3.prepareCall(query3);
													cs.setString(1, vCodigoReg);
													cs.setLong(2, vArticulo2);
													cs.setInt(3, vSucursal);
													cs.setString(4, fecha);
													cs.setInt(5, nroTransaccion);
													cs.setInt(6, vCantPrd);
													cs.setBigDecimal(7, vPrcCmp);
													cs.setInt(8, nroTransaccion);
													cs.setInt(9, nroCaja);
													cs.setInt(10, tipoReg);
													cs.setInt(11, obj.getArticuloVenta().getCorrelativoItem());
													cs.setString(12, direccionDespacho);
													cs.setString(13, vMrc);
													cs.setString(14, emailCliente);
													cs.setString(15, nombreInvitado);
													cs.setString(16, apellidoPaterno);
													cs.setString(17, apellidoMaterno);
													cs.setString(18, mensajeTarjeta);
													cs.registerOutParameter(19, OracleTypes.NUMBER);
													cs.registerOutParameter(20, OracleTypes.VARCHAR);
													cs.execute();
													sal = new Integer(cs.getInt(19));
													sal2 = cs.getString(20);
													cs.close();
													//if(!Validaciones.validarVacio(sal2)){
														logger.traceInfo("creacionRecaudacionesCDF", "Resultados de LGLREG_PRC_MOV_LIS_VLR: outCode: "+sal+" outMsj: "+sal2, new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo2+Constantes.VACIO));
													//}
												} catch (Exception e) {
													logger.traceError("creacionRecaudacionesCDF",
															"Imposible de grabar registro de NOVIOS - Error: ", e);
													resultado = Constantes.FALSO; 
												}
											} else {
												logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos CLUBES");
												throw new AligareException("Error: Problemas de conexion a la base de datos CLUBES");
											} 
										} else {
											RetornoEjecucion retorno = new RetornoEjecucion();
//											retorno = clubDAO.lglreg_prc_mov_lis_vlr_mkp(	vCodigoReg,
//																							Long.parseLong(obj.getArticuloVenta().getCodArticuloMkp()),
//																							sucursal, 
//																							FechaUtil.stringToTimestamp(fecha + Constantes.SPACE + hms, Constantes.FECHA_DDMMYYYY_HHMMSS3), 
//																							nroTransaccion, 
//																							Constantes.NRO_VEINTISIETE,
//																							Constantes.NRO_CERO,
//																							Constantes.NRO_CERO,
//																							vCantPrd, 
//																							vPrcCmp, 
//																							nroTransaccion, 
//																							nroCaja, 
//																							tipoReg, 
//																							obj.getArticuloVenta().getCorrelativoItem(), 
//																							direccionDespacho, 
//																							vMrc, 
//																							Constantes.NRO_CERO, 
//																							emailCliente, 
//																							nombreInvitado, 
//																							apellidoPaterno, 
//																							apellidoMaterno, 
//																							Constantes.VACIO, 
//																							Constantes.NRO_CERO, 
//																							mensajeTarjeta, 
//																							Constantes.NRO_CERO, 
//																							Constantes.NRO_CERO, 
//																							Constantes.NRO_CERO, 
//																							Constantes.NRO_CERO, 
//																							vArticulo+"-"+obj.getArticuloVenta().getDescRipley()
//																						);
											
											if(retorno.getCodigo() != Constantes.EJEC_SIN_ERRORES){
												logger.traceInfo("creacionRecaudacionesCDF", "Imposible de grabar en Novios Error - Mensaje: " + retorno.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(correlativo)));
											} else {
												logger.traceInfo("creacionRecaudacionesCDF", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativo)), new KeyLog("Correlativo item", String.valueOf(obj.getArticuloVenta().getCorrelativoItem())));
											}
										}
					            }
							}
							
							if(//(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
								//	&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))) &&
									newPrecio.intValue() != Constantes.NRO_CERO){ // TODO VALIDAR CONDICION

								if (obj.getArticuloVenta().getCodArticuloMkp() == null){
									query1 = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,0,?,?,?,?,?,0,5)}";
									logger.traceInfo("creacionRecaudacionesCDF", "Creacion de Articulo en BackOffice FN[BACK_OFFICE]: outCode = INSERTA_ARTICULOS_BACK(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+obj.getArticuloVenta().getCorrelativoItem()+","+obj.getArticuloVenta().getCodArticulo()+","+vendedor+","+codigoRegalo+",0,"+newPrecio+","+vCantPrd+","+vDesc+","+vtipoDesc+","+cud+",0,5)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",obj.getArticuloVenta().getCodArticulo()));
									if(conn1 == null){
										conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
									}
									if(conn1 != null){
										try {
											cs = conn1.prepareCall(query1);
											cs.registerOutParameter(1, OracleTypes.NUMBER);
											cs.setString(2, fecha);
											cs.setString(3, Constantes.FECHA_DDMMYYYY);
											cs.setInt(4, sucursal);
											cs.setInt(5, nroCaja);
											cs.setInt(6, nroTransaccion);
											cs.setInt(7, obj.getArticuloVenta().getCorrelativoItem());
											cs.setLong(8, Long.parseLong(obj.getArticuloVenta().getCodArticulo()));
											cs.setInt(9, Integer.parseInt(vendedor));
											cs.setLong(10, codigoRegalo);
											cs.setBigDecimal(11, newPrecio);
											cs.setInt(12, vCantPrd);
											cs.setBigDecimal(13, vDesc);
											cs.setInt(14, vtipoDesc);
											cs.setString(15, cud);
											cs.executeUpdate();
											sal = new Integer(cs.getInt(1));
											logger.traceInfo("creacionRecaudacionesCDF", "Registro Articulos BO FN INSERTA_ARTICULOS_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
											cs.close();
											
											if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO){// if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
												logger.traceError("creacionRecaudacionesCDF", "Imposible crear registro Articulos en Backoffice Error - ", new AligareException("outCode: " + sal));
												resultado = Constantes.FALSO; 
											}
										} catch (Exception e) {
											logger.traceError("creacionRecaudacionesCDF", "Se produjo un error en la ejecucion de la FN INSERTA_ARTICULOS_BACK: ", e);
											resultado = Constantes.FALSO; 
										}
									}else{
										logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
										throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
									}
								} else {
									query1 = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,0,?,?,?,?,?,0,5)}";
									logger.traceInfo("creacionRecaudacionesCDF", "Creacion de Articulo en BackOffice FN[BACK_OFFICE]: outCode = INSERTA_ARTICULOS_BACK(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+obj.getArticuloVenta().getCorrelativoItem()+","+obj.getArticuloVenta().getCodArticulo()+","+vendedor+","+codigoRegalo+",0,"+newPrecio+","+vCantPrd+","+vDesc+","+vtipoDesc+","+cud+",0,5)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",obj.getArticuloVenta().getCodArticulo()));
									if(conn1 == null){
										conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
									}
									if(conn1 != null){
										try {
											cs = conn1.prepareCall(query1);
											cs.registerOutParameter(1, OracleTypes.NUMBER);
											cs.setString(2, fecha);
											cs.setString(3, Constantes.FECHA_DDMMYYYY);
											cs.setInt(4, sucursal);
											cs.setInt(5, nroCaja);
											cs.setInt(6, nroTransaccion);
											cs.setInt(7, obj.getArticuloVenta().getCorrelativoItem());
											cs.setLong(8, Long.parseLong(obj.getArticuloVenta().getCodArticuloMkp()));
											cs.setInt(9, Integer.parseInt(vendedor));
											cs.setLong(10, codigoRegalo);
											cs.setBigDecimal(11, newPrecio);
											cs.setInt(12, vCantPrd);
											cs.setBigDecimal(13, vDesc);
											cs.setInt(14, vtipoDesc);
											cs.setString(15, cud);
											cs.executeUpdate();
											sal = new Integer(cs.getInt(1));
											logger.traceInfo("creacionRecaudacionesCDF", "Registro Articulos BO FN INSERTA_ARTICULOS_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", obj.getArticuloVenta().getCodArticulo()));
											cs.close();
											
											if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO){// if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
												logger.traceError("creacionRecaudacionesCDF", "Imposible crear registro Articulos en Backoffice Error - ", new AligareException("outCode: " + sal));
												resultado = Constantes.FALSO; 
											}
										} catch (Exception e) {
											logger.traceError("creacionRecaudacionesCDF", "Se produjo un error en la ejecucion de la FN INSERTA_ARTICULOS_BACK: ", e);
											resultado = Constantes.FALSO; 
										}
									}else{
										logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
										throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
									}
								}
								
							}
							
							if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
									&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))
									&& Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO
									&& !esExtgarantia){
//							if(Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO && 
//									(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) &&
//									 !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))) && !esExtgarantia){
								query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES(?,?,?,?,?,?)}";
								logger.traceInfo("creacionRecaudacionesCDF", "Actualiza Fecha Despacho en Articulo MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES("+correlativo+","+vArticulo+","+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho)+","+Constantes.FECHA_DDMMYYYY2+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo", vArticulo));
								if(conn2 == null){
									conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								}
								if(conn2 != null){
									try {									
										cs = conn2.prepareCall(query2);
										cs.setLong(1, correlativo);
										cs.setString(2, vArticulo);
										cs.setString(3, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho));
										cs.setString(4, Constantes.FECHA_DDMMYYYY2);
										cs.registerOutParameter(5, OracleTypes.NUMBER);
										cs.registerOutParameter(6, OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(5));
										sal2 = cs.getString(6);
										cs.close();
										if(sal.intValue() != Constantes.NRO_CERO){
											throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
										}
									} catch (Exception e) {
										logger.traceError("creacionRecaudacionesCDF",
												"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES: ", e);
										resultado = Constantes.FALSO; 
									}
								}else{
									logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
							}
						}
					} else {
						logger.traceInfo("creacionRecaudacionesCDF", "No existen Articulos de Venta", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
						resultado = Constantes.FALSO; 
					}
				}else{
					logger.traceError("creacionRecaudacionesCDF",
							"No existe Despacho - Error: ", new AligareException("Correlativo Venta NULO en Despacho"));
					resultado = Constantes.FALSO;
				}
				
				// Creacion Transaccion
			    Timestamp fechaCreacionNV = (trama.getNotaVenta().getFechaHoraCreacion() != null)?trama.getNotaVenta().getFechaHoraCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
			    //if(Validaciones.validaInteger(rutEjecutivoVta) && rutEjecutivoVta.intValue() != Constantes.NRO_CERO){
			    if (rutEjecutivoVta != null && rutEjecutivoVta.length() > 1){
			    	vendedor = String.valueOf(rutEjecutivoVta).substring(0, (String.valueOf(rutEjecutivoVta).length()-Constantes.NRO_UNO));
			    }
			    boolean seguir = Constantes.VERDADERO;
//			    Integer total = trama.getNotaVenta().getMontoVenta();
//			    query1 = "{CALL ? := INSERTA_TRANSACCION_BACK3(TO_DATE(?,?),?,?,?,?,27,?,0,TO_DATE('01/01/1901','dd/MM/yyyy'),0,0,0,?,?,0,?,?,0,0,TO_DATE(?,?),0,0,?,0,TO_DATE(?,?))}";
//				logger.traceInfo("creacionRecaudacionesCDF", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_TRANSACCION_BACK3(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+boleta+",27,"+total+",0,TO_DATE('01/01/1901','dd/MM/yyyy'),0,0,0,"+supervisor+","+vendedor+",0,"+trama.getNotaVenta().getRutComprador()+","+((!Validaciones.validaInteger(trama.getNotaVenta().getMontoDescuento()))?null:trama.getNotaVenta().getMontoDescuento())+",0,0,TO_DATE("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"),0,0,"+correlativo+",0,TO_DATE("+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNV)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"))", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if(conn1 == null){
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//				}
//				if(conn1 != null){
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(4, sucursal);
//						cs.setInt(5, nroCaja);
//						cs.setInt(6, nroTransaccion);
//						cs.setInt(7, boleta);
//						cs.setLong(8, total);
//						cs.setInt(9, Integer.parseInt(supervisor));
//						cs.setInt(10, Integer.parseInt(vendedor));
//						cs.setInt(11, trama.getNotaVenta().getRutComprador());
//						if (!Validaciones.validaInteger(trama.getNotaVenta().getMontoDescuento())){
//						    cs.setNull(12, OracleTypes.NUMBER);
//					    } else{
//					    	cs.setInt(12, trama.getNotaVenta().getMontoDescuento());
//					    }
//						cs.setString(13, (fecha + Constantes.SPACE + hms));
//						cs.setString(14, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.setInt(15, correlativo);
//						cs.setString(16, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNV));
//						cs.setString(17, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionRecaudacionesCDF", "Transaccion BO FN INSERTA_TRANSACCION_BACK3[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							//tipo = Constantes.NRO_CERO;
//							logger.traceError("creacionRecaudacionesCDF", "Imposible crear Encabezado Transaccion Error - ", new AligareException("outCode: " + sal));
//							caja.saltarFolio(parametros, nroTransaccion);
//							resultado = Constantes.FALSO; 
//							seguir = Constantes.FALSO;
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionRecaudacionesCDF", "Se produjo un error en la ejecucion de la FN INSERTA_TRANSACCION_BACK3: ", e);
//						resultado = Constantes.FALSO;
//						seguir = Constantes.FALSO;
//					}
				}else{
					logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos BACK OFFICE");
					throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
				}
				
				boolean seguir = Constantes.VERDADERO;
				if(seguir){
					
					String fechaBol = fecha + Constantes.SPACE + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2;
//					String fecha2 = fecha + Constantes.SPACE + FechaUtil.getCurrentHoursString();
					String fecha2 = fecha + Constantes.SPACE + hms;
					query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
					logger.traceInfo("creacionRecaudacionesCDF", "Actualizacion NOTA VENTA termino de la transacciones MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA("+Constantes.NRO_DOS+","+boleta+","+Constantes.NRO_CERO+","+fechaBol+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+fecha2+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+nroCaja+","+nroTransaccion+","+vendedor+","+sucursal+","+correlativo+",outCode,outMsj,"+Constantes.NRO_CERO+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
					if(conn2 == null){
						conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
					}
					if(conn2 != null){
						try {									
							cs = conn2.prepareCall(query2);
							cs.setInt(1, Constantes.NRO_DOS);
							cs.setInt(2, boleta);
							cs.setInt(3, Constantes.NRO_CERO);
							cs.setString(4, fechaBol);
							cs.setString(5, Constantes.FECHA_DDMMYYYY_HHMMSS2);
							cs.setString(6, fecha2);
							cs.setString(7, Constantes.FECHA_DDMMYYYY_HHMMSS2);
							cs.setInt(8, nroCaja);
							cs.setInt(9, nroTransaccion);
							cs.setLong(10, Long.parseLong(vendedor));//FIXME vendedor2 to vendedor
							cs.setInt(11, sucursal);
							cs.setLong(12, correlativo);
							cs.registerOutParameter(13, OracleTypes.NUMBER);
							cs.registerOutParameter(14, OracleTypes.VARCHAR);
							cs.setInt(15, Constantes.NRO_CERO);
							cs.execute();
							sal = new Integer(cs.getInt(13));
							sal2 = cs.getString(14);
							cs.close();
							if(sal.intValue() != Constantes.NRO_CERO){
								throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
							}
						} catch (Exception e) {
							logger.traceError("creacionRecaudacionesCDF",
									"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA: ", e);
							resultado = Constantes.FALSO;
						}
					}else{
						logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
						throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
					}
					trama.getNotaVenta().setNumeroBoleta(nroTransaccion.longValue());
					trama.getNotaVenta().setCorrelativoBoleta(nroTransaccion.longValue());
				} 
				
//				query1 = "{CALL ? := INSERTA_RECAUDACION_BO(TO_DATE(?,?),?,?,?,9000,9001,0,?,?,?,1)}";
//				logger.traceInfo("creacionRecaudacionesCDF", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_RECAUDACION_BO(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+",9000,9001,0,27-"+correlativo+","+total+","+rutDespachoCambio+",1)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if(conn1 != null){
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(4, sucursal);
//						cs.setInt(5, nroCaja);
//						cs.setInt(6, nroTransaccion);
//						cs.setString(7, "27-"+correlativo);
//						cs.setLong(8, total);
//						cs.setLong(9, rutDespachoCambio);
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionRecaudacionesCDF", "Recaudacion en monedero BO FN INSERTA_RECAUDACION_BO[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							//tipo = Constantes.NRO_CERO;
//							logger.traceError("creacionRecaudacionesCDF",
//									"Imposible crear recaudacion en monedero  Error - ", new AligareException("outCode: " + sal));
//							resultado = Constantes.FALSO;
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionRecaudacionesCDF",
//								"Se produjo un error en la ejecucion de la FN INSERTA_RECAUDACION_BO: ", e);
//						resultado = Constantes.FALSO;
//					}
//				}
				
//				pAdministradora = new Integer(parametros.buscaValorPorNombre("ADMINISTRADORA"));
//				pEmisor = new Integer(parametros.buscaValorPorNombre("EMISOR"));
//				pTarjeta = new Integer(parametros.buscaValorPorNombre("TARJETA"));
//				Integer rutDespacho = (Validaciones.validaInteger(trama.getDespacho().getRutDespacho()) && trama.getDespacho().getRutDespacho().intValue() != Constantes.NRO_CERO)?trama.getDespacho().getRutDespacho():trama.getNotaVenta().getRutComprador();
//				long montoDocto = Constantes.NRO_CERO;
//				Integer vArti = null;
//				Integer vDesc = null;
//				long vTotal = Constantes.NRO_CERO;
//				for (ArticuloVentaTramaDTE obj : trama.getArticuloVentas()) { // TODO REVISAR CALCULO
//					String costoEnvio = (obj.getArticuloVenta().getDescRipley() != null)?obj.getArticuloVenta().getDescRipley():Constantes.VACIO;
//
//					if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
//							&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
////					if(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
////							&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){
//						
//						Integer precio = (Validaciones.validaInteger(obj.getArticuloVenta().getPrecio()))?obj.getArticuloVenta().getPrecio():new Integer(Constantes.NRO_CERO);
//						Integer descuento = (Validaciones.validaInteger(obj.getArticuloVenta().getMontoDescuento()))?obj.getArticuloVenta().getMontoDescuento():new Integer(Constantes.NRO_CERO);
//						Integer unidades = (Validaciones.validaInteger(obj.getArticuloVenta().getUnidades()))?obj.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
//						Integer tipoDesc = (Validaciones.validaInteger(obj.getArticuloVenta().getTipoDescuento()))?obj.getArticuloVenta().getTipoDescuento():new Integer(Constantes.NRO_CERO);
//						
//						vArti = (descuento.intValue() < Constantes.NRO_CERO)?new Integer((precio.intValue()-descuento.intValue())):precio;
//						vTotal = vArti.intValue() * unidades.intValue();
//						vDesc = (descuento.intValue() < Constantes.NRO_CERO)?Constantes.NRO_CERO:descuento.intValue();
//						
//						montoDocto = montoDocto + vTotal;
//						
//						if(vDesc.intValue() > Constantes.NRO_CERO){
//							montoDocto = (tipoDesc.intValue() == Constantes.NRO_CERO)?montoDocto - (vDesc.intValue() * unidades.intValue()):montoDocto - (vDesc.intValue());
//						}
//					}
//				}
//				
//				query5 = "{CALL TC_ABONO_MONEDERO(?,217,?,?,?,?,null,null,null,1,?,TO_DATE(?,?),?,?,null,null,1,'TCVIR',?,?,?)}";
//				logger.traceInfo("creacionRecaudacionesCDF", "Abonar al Monedero SP[CAR]: TC_ABONO_MONEDERO("+rutDespacho+",217,"+montoDocto+","+pAdministradora+","+pEmisor+","+pTarjeta+",null,null,null,1,"+sucursal+",TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+nroCaja+","+boleta+",null,null,1,'TCVIR',inout["+nroTransaccion+"],outMsj,outCode)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				conn5 = PoolBDs.getConnection(BDType.CAR);
//				if (conn5 != null) {
//					try {
//						cs = conn5.prepareCall(query5);
//						cs.setLong(1, rutDespacho);
//						cs.setLong(2, montoDocto);
//						cs.setInt(3, pAdministradora);
//						cs.setInt(4, pEmisor);
//						cs.setInt(5, pTarjeta);
//						cs.setInt(6, sucursal);
//						cs.setString(7, fecha);
//						cs.setString(8, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(9, nroCaja);
//						cs.setInt(10, boleta);
//						cs.setInt(11, nroTransaccion);
//						cs.registerOutParameter(11, OracleTypes.NUMBER);
//						cs.registerOutParameter(12, OracleTypes.VARCHAR);
//						cs.registerOutParameter(13, OracleTypes.NUMBER);
//						cs.execute();
//						sal2 = cs.getString(12);
//						sal = new Integer(cs.getInt(13));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							logger.traceError("creacionRecaudacionesCDF",
//									"Imposible Abonar al Monedero - ", new AligareException("outCode: " + sal+ " outMsj: "+sal2));
//							resultado = Constantes.FALSO;
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionRecaudacionesCDF", "Imposible Abonar al Monedero: ", e);
//						resultado = Constantes.FALSO;
//					}
//				} else {
//					logger.traceInfo("creacionRecaudacionesCDF", "Problemas de conexion a la base de datos CAR");
//					throw new AligareException("Error: Problemas de conexion a la base de datos CAR");
//				}
//				/******Juan Patricio Rojas 08-06-2017 *********/
//				
//				
//				/**** Juan Patricio Rojas ********/
//			}
		} catch (AligareException e) {
			logger.traceError("creacionRecaudacionesCDF", e);
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
			logger.endTrace("creacionRecaudacionesCDF", "Finalizado", "boolean: AligareException");
			throw e;
		} catch(Exception ex){
			logger.traceError("creacionRecaudacionesCDF", "Se produjo un error inesperado: ", ex);
			resultado = Constantes.FALSO;
		} finally {
			if(conn1 != null){
				PoolBDs.closeConnection(conn1);
			}
			if(conn2 != null){
				PoolBDs.closeConnection(conn2);
			}
			if(conn3 != null){
				PoolBDs.closeConnection(conn3);
			}
			if(conn4 != null){
				PoolBDs.closeConnection(conn4);
			}
			if(conn5 != null){
				PoolBDs.closeConnection(conn5);
			}
		}		
		
		if(resultado == Constantes.FALSO){
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
		}
		logger.endTrace("creacionRecaudacionesCDF", "Finalizado", "boolean: " + String.valueOf(resultado));
		return resultado;
	}
	
	@Override
	public boolean creacionRecaudacionesMKP(Parametros parametros, TramaDTE trama, Integer nroTransaccion, String codUnico, Integer boleta, Integer boletaOrigen) throws AligareException{
		boolean resultado = Constantes.VERDADERO;
		logger.initTrace("creacionRecaudacionesMKP",
				"Parametros parametros: " + parametros.toString() + " - TramaDTE: trama - Integer nroTransaccion: " + nroTransaccion
						+ " String codUnico: " + codUnico + " - Integer boleta: " + boleta,
				new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO),
				new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("codUnico", codUnico)); 
		//String fechaAnterior = Constantes.VACIO;
	    //String fechaCompara = Constantes.VACIO;
		//String vFechaBT = Constantes.VACIO;
//		int tipoDePago = obtieneTipoDePago(trama);
		
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		ClubDAO clubDAO = new ClubDAOImpl();
		
		String fecha = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros)/*FechaUtil.getCurrentDateString()*/;
		String hms = caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros)/*FechaUtil.getCurrentHoursString()*/;
		String fechaHora = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, parametros);
		String vendedor = parametros.buscaValorPorNombre("RUT_VENDEDOR");
		String supervisor = parametros.buscaValorPorNombre("RUT_SUPERVISOR");
		String plTipoPago = parametros.buscaValorPorNombre("PL_TIPO_PAGO");
		int sucursal = Integer.parseInt(parametros.buscaValorPorNombre("SUCURSAL"));
		int nroCaja = Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA"));
		Integer pAdministradora = new Integer(parametros.buscaValorPorNombre("ADMINISTRADORA"));
		Integer pEmisor = new Integer(parametros.buscaValorPorNombre("EMISOR"));
		Integer pTarjeta = new Integer(parametros.buscaValorPorNombre("TARJETA"));
//		int funcion = Integer.parseInt(parametros.buscaValorPorNombre("FUNCION"));
		Long correlativo = trama.getNotaVenta().getCorrelativoVenta();
		//Integer paramCFTValor = new Integer(parametros.buscaValorPorNombre("PARAM_CFT"));
		Integer sal = null;
	    String sal2 = Constantes.VACIO;
	    String query1 = Constantes.VACIO;
		String query2 = Constantes.VACIO;
//		String query3 = Constantes.VACIO;
		CallableStatement cs = null;
		Connection conn1 = null;
		Connection conn2 = null;
		Connection conn3 = null;
		Connection conn4 = null;
		String wsCodAuto = Constantes.VACIO;
		int tipo = Constantes.NRO_UNO;
		String formaPago = Constantes.VACIO;
		String tipoIdent = trama.getNotaVenta().getIndicadorMkp();
		BigDecimal totalVentaMkp;
		try{
			if(Validaciones.validaLong(trama.getNotaVenta().getCorrelativoVenta())){
				
				// Nuevo 8.0.12
//				String dvCompradorMirakl = (trama.getNotaVenta().getDvComprador()!=null)?trama.getNotaVenta().getRutComprador().toString():Constantes.STRING_CERO	;
//				String rutCompradorMirakl = (trama.getNotaVenta().getRutComprador()!=null)?trama.getNotaVenta().getRutComprador().toString()+dvCompradorMirakl:Constantes.STRING_CERO;
				Long rutTransbank = trama.getNotaVenta().getRutComprador();
				//Integer rutEjecutivoVta = (!Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta()))?new Integer(trama.getNotaVenta().getEjecutivoVta()):new Integer(Constantes.NRO_CERO);
				String rutEjecutivoVta = (!Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta()))?trama.getNotaVenta().getEjecutivoVta().trim():Constantes.STRING_CERO;
				if(Validaciones.validaLong(trama.getTarjetaBancaria().getCorrelativoVenta())){
					formaPago = obtieneFormaPago(trama);
					Integer wsTipo = trama.getTarjetaBancaria().getTipoTarjeta();
//					Integer total = trama.getTarjetaBancaria().getMontoTarjeta();
					wsCodAuto = trama.getTarjetaBancaria().getCodigoAutorizador();
					String vd = (!Validaciones.validarVacio(trama.getTarjetaBancaria().getVd()))?trama.getTarjetaBancaria().getVd():Constantes.BOL_VALOR_VD_N;
					
					//Nuevo
					totalVentaMkp = trama.getTarjetaBancaria().getMontoTarjeta();
					
					if(Constantes.IND_MKP_AMBAS.equals(tipoIdent)){
						if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
							totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
							for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
								if (obj.getEsMkp() == Constantes.ES_MKP_MKP)
									totalVentaMkp = totalVentaMkp.add((Validaciones.validaBigDecimal(obj.getMontoVenta()))?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO));
							}
							
						}
						logger.traceInfo("creacionRecaudacionesMKP", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
					}
					
//					if((!Validaciones.validarVacio(plTipoPago) 
//							&& (!Constantes.STRING_UNO.equals(plTipoPago) || Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)))
//							&& (!Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso()))){
//						query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'',?,?,0,?,0,0,0,?,?,?,?)}";
//						logger.traceInfo("creacionRecaudacionesMKP", "Creacion T.Bancaria BackOffice["+formaPago+"] - SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'',"+((Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd))?Constantes.BOL_VALOR_VD_D:Constantes.BOL_VALOR_VD_C)+","+rutTransbank+",0,"+totalVentaMkp+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//						if (conn1 != null) {
//							try {
//								cs = conn1.prepareCall(query1);
//								cs.setString(1, fecha);
//								cs.setInt(2, sucursal);
//								cs.setInt(3, nroCaja);
//								cs.setInt(4, nroTransaccion);
//								cs.setString(5, (Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)) ? Constantes.BOL_VALOR_VD_D
//										: Constantes.BOL_VALOR_VD_C);
//								cs.setInt(6, rutTransbank);
//								cs.setBigDecimal(7, totalVentaMkp);
//								cs.setString(8, wsCodAuto);
//								cs.setString(9, codUnico);
//								cs.registerOutParameter(10, OracleTypes.NUMBER);
//								cs.registerOutParameter(11, OracleTypes.VARCHAR);
//								cs.execute();
//								sal = new Integer(cs.getInt(10));
//								sal2 = cs.getString(11);
//								cs.close();
//							} catch (Exception e) {
//								logger.traceError("creacionRecaudacionesMKP",
//										"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
//								
//								resultado = Constantes.FALSO; 
//							}
//						} else {
//							logger.traceInfo("creacionRecaudacionesMKP",
//									"Problemas de conexion a la base de datos BACK OFFICE");
//							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//						}
//					} else if (Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())) { // TARJETA MERCADO PAGO
//						query1 = "{CALL INSERTA_TRANSBANK_CV(TO_DATE(?,'dd/MM/YYYY'),?,?,?,'MP',?,?,0,?,0,0,0,?,?,?,?)}";
//						logger.traceInfo("creacionRecaudacionesMKP", "TARJETA MERCADO PAGO["+formaPago+"] SP[BACK_OFFICE]: INSERTA_TRANSBANK_CV(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+",'MP',"+Constantes.BOL_VALOR_VD_C+","+rutTransbank+",0,"+totalVentaMkp+",0,0,0,"+wsCodAuto+","+codUnico+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//						if (conn1 != null) {
//							try {
//								cs = conn1.prepareCall(query1);
//								cs.setString(1, fecha);
//								cs.setInt(2, sucursal);
//								cs.setInt(3, nroCaja);
//								cs.setInt(4, nroTransaccion);
//								cs.setString(5, Constantes.BOL_VALOR_VD_C);
//								cs.setInt(6, rutTransbank);
//								cs.setBigDecimal(7, totalVentaMkp);
//								cs.setString(8, wsCodAuto);
//								cs.setString(9, codUnico);
//								cs.registerOutParameter(10, OracleTypes.NUMBER);
//								cs.registerOutParameter(11, OracleTypes.VARCHAR);
//								cs.execute();
//								sal = new Integer(cs.getInt(10));
//								sal2 = cs.getString(11);
//								cs.close();
//							} catch (Exception e) {
//								logger.traceError("creacionRecaudacionesMKP",
//										"Se produjo un error en la ejecucion del SP INSERTA_TRANSBANK_CV: ", e);
//								
//								resultado = Constantes.FALSO;
//							}
//						} else {
//							logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
//							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//						}
//					
//					}else{
//						query1 = "{CALL ? := INSERTA_BANCARIA_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?)}";
//						logger.traceInfo("creacionRecaudacionesMKP", "TARJETA BANCARIA CREDITO["+formaPago+"] FN[BACK_OFFICE]: outCode = INSERTA_BANCARIA_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+wsTipo+","+totalVentaMkp+","+wsCodAuto+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//						if (conn1 != null) {
//							try {
//								cs = conn1.prepareCall(query1);
//								cs.registerOutParameter(1, OracleTypes.NUMBER);
//								cs.setString(2, fecha);
//								cs.setInt(3, sucursal);
//								cs.setInt(4, nroCaja);
//								cs.setInt(5, nroTransaccion);
//								cs.setInt(6, wsTipo);
//								cs.setBigDecimal(7, totalVentaMkp);
//								cs.setString(8, wsCodAuto);
//								cs.executeUpdate();
//								sal = new Integer(cs.getInt(1));
//								logger.traceInfo("creacionRecaudacionesMKP", "Bancaria BO FN INSERTA_BANCARIA_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//								cs.close();
//							} catch (Exception e) {
//								logger.traceError("creacionRecaudacionesMKP",
//										"Se produjo un error en la ejecucion de la FN INSERTA_BANCARIA_BACK: ", e);
//								
//								resultado = Constantes.FALSO; 
//							}
//						} else {
//							logger.traceInfo("creacionRecaudacionesMKP",
//									"Problemas de conexion a la base de datos BACK OFFICE");
//							throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//						}
//					}
					
					if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
						// tipo = Constantes.NRO_CERO;
						logger.traceError("creacionRecaudacionesMKP",
								"Imposible crear forma de Pago Bancaria Error - ", new AligareException("outCode: " + sal + " outMsj: "+sal2));
						
						resultado = Constantes.FALSO; 
					}
				}
				// Creacion Tarjeta Regalo Empresa
//				if (Validaciones.validaL(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())) {
//					formaPago = obtieneFormaPago(trama);
//					Integer emisorTRE = trama.getTarjetaRegaloEmpresa().getEmisor();
//					Integer admTRE = trama.getTarjetaRegaloEmpresa().getAdministradora();
//					Integer tarjetaTRE = trama.getTarjetaRegaloEmpresa().getTarjeta();
//					Integer codAutorizadorTRE = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();
//					//Integer rutClienteTRE = trama.getTarjetaRegaloEmpresa().getRutCliente();
//					//String dvClienteTRE = trama.getTarjetaRegaloEmpresa().getDvCliente();
//					Integer correVenta = trama.getTarjetaRegaloEmpresa().getCorrelativoVenta();
//					
//					totalVentaMkp = trama.getTarjetaRegaloEmpresa().getMonto();
//					
//					if(Constantes.STRING_DOS.equals(tipoIdent)){
//						if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
//							totalVentaMkp = Constantes.NRO_CERO;
//							for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
//								if (obj.getEsMkp() == Constantes.ES_MKP_MKP)
//									totalVentaMkp = totalVentaMkp + ((Validaciones.validaInteger(obj.getMontoVenta()))?obj.getMontoVenta().longValue():Constantes.NRO_CERO);
//							}
//							
//						}
//						logger.traceInfo("creacionRecaudacionesMKP", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					}
//					
//					Long montoTRE = totalVentaMkp;
//					Integer nroTarjetaTRE = trama.getTarjetaRegaloEmpresa().getNroTarjeta();
//					long rutCliente = (Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getRutCliente()))?trama.getTarjetaRegaloEmpresa().getRutCliente().longValue():Constantes.NRO_CERO;
//					rutCliente = Long.parseLong((rutCliente + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente))));
//					Integer flagTRE = trama.getTarjetaRegaloEmpresa().getFlag();
//					//tipo = Constantes.NRO_UNO;
//					String WFECHA = fecha.replaceAll(Constantes.SLASH, Constantes.VACIO);
//					String query4 = "{CALL FEGIFE_PRC_INS_TREINT(?,'IB',?,?,?,?,?,?)}";
//					logger.traceInfo("creacionRecaudacionesMKP", "TARJETA REGALO EMPRESA["+formaPago+"] SP[TRE] FEGIFE_PRC_INS_TREINT("+nroTarjetaTRE+",'IB',"+(montoTRE.intValue() * Constantes.NRO_MENOSUNO)+","+correVenta+","+nroCaja+","+WFECHA+",outMsj,outCode)", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					conn4 = PoolBDs.getConnection(BDType.TRE);
//					if (conn4 != null) {
//						try {
//							cs = conn4.prepareCall(query4);
//							cs.setInt(1, nroTarjetaTRE);
//							cs.setInt(2, (montoTRE.intValue() * Constantes.NRO_MENOSUNO)); 
//							cs.setInt(3, correVenta);
//							cs.setInt(4, nroCaja);
//							cs.setString(5, WFECHA);
//							cs.registerOutParameter(6, OracleTypes.VARCHAR);
//							cs.registerOutParameter(7, OracleTypes.NUMBER);
//							cs.execute();
//							sal = new Integer(cs.getInt(7));
//							sal2 = cs.getString(6);
//							cs.close();
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//								logger.traceError("creacionRecaudacionesMKP",
//										"Imposible Registrar forma de Pago TARJETA REGALO EMPRESA Error - ",
//										new AligareException("outCode: "+sal+" outMsj: "+sal2));
//					
//								resultado = Constantes.FALSO;
//							}
//						} catch (Exception e) {
//							logger.traceError("creacionRecaudacionesMKP",
//									"Se produjo un error en la ejecucion del SP FEGIFE_PRC_INS_TREINT: ", e);
//			
//							resultado = Constantes.FALSO;
//						}
//					} else {
//						logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos TRE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos TRE");
//					}
//					
//					query1 = "{CALL ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
//					logger.traceInfo("creacionRecaudacionesMKP", "TARJETA REGALO EMPRESA BO["+formaPago+"] SP[BACK_OFFICE] ACTUALIZA_TAR_REGALO_EMPRESA(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+admTRE+","+emisorTRE+","+tarjetaTRE+","+codAutorizadorTRE+","+rutCliente+","+montoTRE+","+nroTarjetaTRE+","+flagTRE+",outCode,outMsj)", new KeyLog("Correlativo_venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					if (conn1 == null) {
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					}
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.setString(1, fecha);
//							cs.setInt(2, sucursal);
//							cs.setInt(3, nroCaja);
//							cs.setInt(4, nroTransaccion);
//							cs.setInt(5, admTRE);
//							cs.setInt(6, emisorTRE);
//							cs.setInt(7, tarjetaTRE);
//							cs.setInt(8, codAutorizadorTRE);
//							cs.setLong(9, rutCliente);
//							cs.setLong(10, montoTRE);
//							cs.setInt(11, nroTarjetaTRE);
//							cs.setInt(12, flagTRE);
//							cs.registerOutParameter(13, OracleTypes.NUMBER);
//							cs.registerOutParameter(14, OracleTypes.VARCHAR);
//							cs.execute();
//							sal = new Integer(cs.getInt(13));
//							sal2 = cs.getString(14);
//							cs.close();
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//								logger.traceError("creacionRecaudacionesMKP",
//										"Imposible crear forma de Pago TARJETA REGALO EMPRESA BO Error - ",
//										new AligareException("outCode: "+sal+" outMsj: "+sal2));
//
//								resultado = Constantes.FALSO; 
//							}
//						} catch (Exception e) {
//							logger.traceError("creacionRecaudacionesMKP",
//									"Se produjo un error en la ejecucion del SP ACTUALIZA_TAR_REGALO_EMPRESA: ", e);
//		
//							resultado = Constantes.FALSO; 
//						}
//					}else{
//						logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//					
//				}
				
				
				
				
				// Creacion Tarjeta Ripley
//				if(Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())){
//					BigInteger codAutor = (trama.getTarjetaRipley().getCodigoAutorizacion() != null)?trama.getTarjetaRipley().getCodigoAutorizacion():new BigInteger(Constantes.STRING_CERO);
//					BigInteger codigoPAN = trama.getTarjetaRipley().getPan();
//					//BigInteger tipoCodAuto = null;
//					formaPago = obtieneFormaPago(trama);
//					pAdministradora = trama.getTarjetaRipley().getAdministradora();
//					pEmisor = trama.getTarjetaRipley().getEmisor();
//					pTarjeta = trama.getTarjetaRipley().getTarjeta();
//					tipo = Constantes.NRO_DOS;
//					
//					totalVentaMkp = trama.getTarjetaRipley().getMontoCapital();
//					if(Constantes.STRING_DOS.equals(tipoIdent)){
//						if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
//							totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
//							for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
//								if (obj.getEsMkp() == Constantes.ES_MKP_MKP)
//									totalVentaMkp = totalVentaMkp.add((Validaciones.validaBigDecimal(obj.getMontoVenta()))?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO));
//							}
//							
//						}
//						logger.traceInfo("creacionRecaudacionesMKP", "Monto parcial de la venta ripley mixta MKP: "+totalVentaMkp, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					}
//					query1 = "{CALL ? := INSERTA_RIPLEY_BACK(TO_DATE(?,'dd/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,?)}";
//					logger.traceInfo("creacionRecaudacionesMKP", "Creacion T.Ripley["+formaPago+"] BackOffice FN[BACK_OFFICE]: outCode = INSERTA_RIPLEY_BACK(TO_DATE("+fecha+",'dd/MM/YYYY'),"+sucursal+","+nroCaja+","+nroTransaccion+","+pAdministradora+","+pEmisor+","+pTarjeta+","+trama.getTarjetaRipley().getRutTitular()+","+((trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO)+","+trama.getTarjetaRipley().getPlazo()+","+((trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO)+","+totalVentaMkp+","+((trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie():Constantes.NRO_CERO)+","+((trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar():Constantes.NRO_CERO)+","+
//					((trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO)+","+((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)?new BigDecimal(codAutor):new BigDecimal(((Validaciones.validaBigInteger(codAutor)?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2)))+",0,"+((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()) && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)?(codigoPAN != null)?new BigDecimal(Util.rellenarString(codigoPAN.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null:(codigoPAN != null)?new BigDecimal(codigoPAN.toString()):null)+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//					if (conn1 == null) {
//						conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//					}
//					if (conn1 != null) {
//						try {
//							cs = conn1.prepareCall(query1);
//							cs.registerOutParameter(1, OracleTypes.NUMBER);
//							cs.setString(2, fecha);
//							cs.setInt(3, sucursal);
//							cs.setInt(4, nroCaja);
//							cs.setInt(5, nroTransaccion);
//							cs.setInt(6, pAdministradora);
//							cs.setInt(7, pEmisor);
//							cs.setInt(8, pTarjeta);
//							cs.setInt(9, trama.getTarjetaRipley().getRutTitular());
//							cs.setInt(10, (trama.getTarjetaRipley().getRutPoder() != null)?trama.getTarjetaRipley().getRutPoder():Constantes.NRO_CERO);
//							cs.setInt(11, trama.getTarjetaRipley().getPlazo());
//							cs.setInt(12, (trama.getTarjetaRipley().getDiferido() != null)?trama.getTarjetaRipley().getDiferido():Constantes.NRO_CERO);
//							cs.setBigDecimal(13, totalVentaMkp);
//							cs.setBigDecimal(14, (trama.getTarjetaRipley().getMontoPie() != null)?trama.getTarjetaRipley().getMontoPie(): new BigDecimal(Constantes.NRO_CERO));
//		
//							//tipoCodAuto = (paramCFTValor.intValue() == Constantes.NRO_CERO)?codigoPAN:new BigInteger(Constantes.STRING_CERO);
//		                  
//							cs.setBigDecimal(15, (trama.getTarjetaRipley().getDescuentoCar() != null)?trama.getTarjetaRipley().getDescuentoCar(): new BigDecimal(Constantes.NRO_CERO));
//							cs.setInt(16, (trama.getTarjetaRipley().getCodigoDescuento() != null)?trama.getTarjetaRipley().getCodigoDescuento():Constantes.NRO_CERO);
//							cs.setBigDecimal(17,
//									((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
//											&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
//													? new BigDecimal(codAutor)
//													: new BigDecimal(((Validaciones.validaBigInteger(codAutor)?codAutor.toString():Constantes.VACIO) + Constantes.STRING_CEROX2))));
//							cs.setBigDecimal(18,
//									((Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore())
//											&& trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO)
//													? (codigoPAN != null)?new BigDecimal(Util.rellenarString(codigoPAN.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15)):null
//													: (codigoPAN != null)?new BigDecimal(codigoPAN.toString()):null));
//							cs.executeUpdate();
//							sal = new Integer(cs.getInt(1));
//							logger.traceInfo("creacionRecaudacionesMKP", "Ripley BO FN INSERTA_RIPLEY_BACK[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//							cs.close();
//							
//							if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
//								// tipo = Constantes.NRO_CERO;
//								logger.traceError("creacionRecaudacionesMKP",
//										"Imposible crear forma de pago Ripley BO Error - ", new AligareException("outCode: " + sal));
//								
//								resultado = Constantes.FALSO; 
//							}
//						} catch (Exception e) {
//							logger.traceError("creacionRecaudacionesMKP",
//									"Se produjo un error en la ejecucion de la FN INSERTA_RIPLEY_BACK: ", e);
//							
//							resultado = Constantes.FALSO; 
//						}
//					}else{
//						logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
//						throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//					}
//				}
//				
				long rutDespachoCambio = Constantes.NRO_CERO;
//				if(Validaciones.validaLong(trama.getDespacho().getCorrelativoVenta())){
//					if(Validaciones.validaInteger(trama.getDespacho().getRutDespacho()) && trama.getDespacho().getRutDespacho().intValue() > Constantes.NRO_CERO){
//						rutDespachoCambio = trama.getDespacho().getRutDespacho().longValue();
//					}else{
//						rutDespachoCambio = (Validaciones.validaInteger(trama.getDespacho().getRutCliente()))?trama.getDespacho().getRutCliente().longValue():Constantes.NRO_CERO;
//					}
					Long correlativoVenta = trama.getDespacho().getCorrelativoVenta();
//				    long rutDespacho = Long.parseLong((rutDespachoCambio + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutDespachoCambio))));
				    long rutDespacho = rutDespachoCambio;
//				    //String nombreDespacho = Util.cambiaCaracteres(trama.getDespacho().getNombreDespacho());
//
				    Timestamp fechaDespacho = (trama.getDespacho().getFechaDespacho() != null)?trama.getDespacho().getFechaDespacho():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
//				    //Integer sucursalDespacho = trama.getDespacho().getSucursalDespacho();
//				    //Integer comunaDespacho = trama.getDespacho().getComunaDespacho();
//				    //Integer regionDespacho = trama.getDespacho().getRegionDespacho();
//				    String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
//				    //String telefonoDespacho = Util.cambiaCaracteres(trama.getDespacho().getTelefonoDespacho());
//				    //String observacion = (trama.getDespacho().getObservacion() != null)?Util.cambiaCaracteres(trama.getDespacho().getObservacion()):"Sin observacion";    
				    long rutCliente2 = (Validaciones.validaInteger(trama.getDespacho().getRutCliente()))?trama.getDespacho().getRutCliente().longValue():Constantes.NRO_CERO;
//				    long rutCliente = Long.parseLong((rutCliente2 + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente2))));
				    long rutCliente = rutCliente2;
//				    //String direccionCliente = Util.cambiaCaracteres(trama.getDespacho().getDireccionCliente());
//				    //String telefonoCliente = Util.cambiaCaracteres(trama.getDespacho().getTelefonoCliente());
//				    //Integer tipoCliente = trama.getDespacho().getTipoCliente();
//				    String nombreCliente =  (((trama.getDespacho().getNombreCliente() != null)
//							? trama.getDespacho().getNombreCliente() : Constantes.VACIO)
//							+ Constantes.SPACE
//							+ ((trama.getDespacho().getApellidoPatCliente() != null)
//									? trama.getDespacho().getApellidoPatCliente() : Constantes.VACIO)
//							+ Constantes.SPACE + ((trama.getDespacho().getApellidoMatCliente() != null)
//									? trama.getDespacho().getApellidoMatCliente() : Constantes.VACIO));
//				    nombreCliente = Util.rellenarString(Util.cambiaCaracteres(nombreCliente), 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49);
//				    String nombreInvitado =  Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
//				    //Integer tipoDespacho = trama.getDespacho().getTipoDespacho();
//				    String emailCliente = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
//				    String apellidoPaterno = (Validaciones.validarVacio(trama.getDespacho().getApellidoPatCliente()))?"Apellido paterno no descrito":trama.getDespacho().getApellidoPatCliente();
//				    String apellidoMaterno = (Validaciones.validarVacio(trama.getDespacho().getApellidoMatCliente()))?"Apellido materno no descrito":trama.getDespacho().getApellidoMatCliente();
				    Long codigoRegalo = (Validaciones.validaLong(trama.getNotaVenta().getCodigoRegalo()))?trama.getNotaVenta().getCodigoRegalo():new Long(Constantes.NRO_CERO);
				    Integer tipoRegalo = (Validaciones.validaInteger(trama.getNotaVenta().getTipoRegalo()))?trama.getNotaVenta().getTipoRegalo():new Integer(Constantes.NRO_CERO);
//
				    Timestamp fechaVenta = (trama.getNotaVenta().getFechaCreacion() != null)?trama.getNotaVenta().getFechaCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
			        String mensajeTarjeta = (trama.getDespacho().getMensajeTarjeta() != null)?trama.getDespacho().getMensajeTarjeta():Constantes.VACIO;
			        //long vendedor2 = Long.parseLong((vendedor + Constantes.VACIO + Util.getDigitoValidadorRut2(vendedor)));
			        int contadorEsMKP = Constantes.NRO_CERO;
					boolean indMKPAmbas = (!Validaciones.validarVacio(trama.getNotaVenta().getIndicadorMkp()) && Constantes.IND_MKP_AMBAS.equals(trama.getNotaVenta().getIndicadorMkp()));
					if (trama.getArticuloVentas() != null && !trama.getArticuloVentas().isEmpty()) {
						for (ArticuloVentaTramaDTE obj : trama.getArticuloVentas()) {
							if(indMKPAmbas){
								if (Validaciones.validaInteger(obj.getIdentificadorMarketplace().getEsMkp())
										&& obj.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.NRO_UNO) {
									contadorEsMKP++;
								} else {
									continue;
								}
							}else{
								contadorEsMKP++;
							}
							
							String vArticulo = obj.getArticuloVenta().getCodArticulo();
							String vArticuloMkp = obj.getArticuloVenta().getCodArticuloMkp();
							String costoEnvio = (obj.getArticuloVenta().getDescRipley() != null)?obj.getArticuloVenta().getDescRipley():Constantes.VACIO;
						    //String jornadaDespacho = (obj.getArticuloVenta().getTipoDespacho() != null)?obj.getArticuloVenta().getTipoDespacho():Constantes.SI;
						    /*String jornadaDespachoBo = Constantes.VACIO;
						    if(Constantes.BOL_JORNADA_AM.equalsIgnoreCase(jornadaDespacho)){
						    	jornadaDespachoBo = Constantes.STRING_UNO;
						    }else if(Constantes.BOL_JORNADA_PM.equalsIgnoreCase(jornadaDespacho)){
						    	jornadaDespachoBo = Constantes.STRING_DOS;
						    }else{
						    	jornadaDespachoBo = jornadaDespacho;
						    }*/
						    boolean esExtgarantia = (Validaciones.validaInteger(obj.getArticuloVenta().getIndicadorEg())
									&& obj.getArticuloVenta().getIndicadorEg().intValue() == Constantes.NRO_TRES);
							Integer strFechaConfirma = new Integer(Constantes.NRO_CERO);  
							Integer dias = new Integer(Constantes.NRO_CERO);
							/* ESTAN CADUCOS VALIDARLOS
							if (actFechPreVenta && (vArticulo.equals("2000316773122")
									|| vArticulo.equals("2000316773177") || vArticulo.equals("2000317721399")
									|| vArticulo.equals("2000317719471"))) { 
								fechaDespacho = fechaDespachoPreVenta;
							}else
							*/

							dias = FechaUtil.diferenciaDias(FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaVenta), Constantes.FECHA_DDMMYYYY3, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho), Constantes.FECHA_DDMMYYYY3);
							
							if (Validaciones.validaInteger(dias) && dias.intValue() < Constantes.NRO_CERO) {
								query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA(?,?,?,?,?)}";
								logger.traceInfo("creacionRecaudacionesMKP", "ACTUALIZACION FECHA CREACION NOTA VENTA SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+correlativo+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
								conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								if (conn2 != null) {
									try {
										cs = conn2.prepareCall(query2);
										cs.setString(1, (fecha + Constantes.SPACE + hms));
										cs.setString(2, Constantes.FECHA_DDMMYYYY_HHMMSS2);
										cs.setLong(3, correlativo);
										cs.registerOutParameter(4, OracleTypes.NUMBER);
										cs.registerOutParameter(5, OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(4));
										sal2 = cs.getString(5);
										cs.close();
										if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
											throw new AligareException("outCode: " + sal.toString() + " outMsj: "+ sal2);
										}
									} catch (Exception e) {
										logger.traceError("creacionRecaudacionesMKP",
												"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_FECH_CREA_NOTA_VENTA: ", e);
										
										resultado = Constantes.FALSO;
									}
								} else {
									logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
							}
							
							Integer tipoRegalo2 = new Integer(Constantes.NRO_CERO);
							String esRegalo = (obj.getArticuloVenta().getEsregalo() != null)?obj.getArticuloVenta().getEsregalo():Constantes.VACIO;
							if(Constantes.NO.equalsIgnoreCase(esRegalo)){
								tipoRegalo2 = new Integer(Constantes.NRO_CERO);
								mensajeTarjeta = Constantes.SPACE;
							}else if(Constantes.SI.equalsIgnoreCase(esRegalo)){
								tipoRegalo2 = new Integer(Constantes.NRO_UNO);
								mensajeTarjeta = (obj.getArticuloVenta().getMensaje() != null)?obj.getArticuloVenta().getMensaje():Constantes.VACIO;
							}
							
							tipoRegalo = (Validaciones.validaLong(codigoRegalo) && codigoRegalo.intValue() == Constantes.NRO_CERO)?tipoRegalo2:tipoRegalo;
							rutDespacho = (rutDespachoCambio == Constantes.NRO_CERO)?rutCliente:rutDespacho;
//							String cud = Constantes.SPACE;
							if(tieneCud(vArticulo).intValue() == Constantes.NRO_CERO){
								if(Constantes.SI.equalsIgnoreCase(esRegalo) && String.valueOf(Constantes.NRO_NOVENTANUEVE).equals((obj.getArticuloVenta().getMensaje() != null)?obj.getArticuloVenta().getMensaje():Constantes.VACIO)){
									tipoRegalo = new Integer(Constantes.NRO_NOVENTANUEVE);
								}
								if(Validaciones.validaLong(codigoRegalo) && codigoRegalo.longValue() != Constantes.NRO_CERO && (Validaciones.validarVacio(esRegalo) || Constantes.NO.equalsIgnoreCase(esRegalo))){
									tipoRegalo = new Integer(Constantes.NRO_CERO);
								}
							}	
							
							Integer vCantPrd = (Validaciones.validaInteger(obj.getArticuloVenta().getUnidades()))?obj.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
							BigDecimal vMontoDesc = (Validaciones.validaBigDecimal(obj.getArticuloVenta().getMontoDescuento()))?obj.getArticuloVenta().getMontoDescuento():new BigDecimal(Constantes.NRO_CERO);
							BigDecimal vPrecio = ((Validaciones.validaBigDecimal(obj.getArticuloVenta().getPrecio()))?obj.getArticuloVenta().getPrecio():new BigDecimal(Constantes.NRO_CERO));
//							Integer newPrecio = (vMontoDesc.intValue() < Constantes.NRO_CERO)?new Integer(vPrecio.intValue()-vMontoDesc.intValue()):vPrecio;// TODO VALIDAR CALCULO
							
							if(Validaciones.validaLong(codigoRegalo) && codigoRegalo.intValue() > Constantes.NRO_CERO) {
								logger.traceInfo("creacionRecaudacionesMKP", "OC TIENE CODIGO REGALO: "+codigoRegalo.intValue(), new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo));
					            mensajeTarjeta = (obj.getArticuloVenta().getMensaje() != null)?obj.getArticuloVenta().getMensaje():Constantes.VACIO;

								if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
										&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
//					            if(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
//					            		&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))){ // TODO VALIDAR CONDICION
										
				                	String vCodigoReg = (Validaciones.validaLong(codigoRegalo))?codigoRegalo.toString():Constantes.VACIO;
									long vArticulo2 = Long.parseLong((vArticuloMkp != null)?vArticuloMkp:vArticulo);
									//Integer vSucursal = (sucursal + 10000);
									BigDecimal vPrcCmp = vPrecio.subtract(vMontoDesc);//newPrecio.intValue() - vMontoDesc.intValue(); //FIXME

							//merge .22
									BigDecimal precio = null;
									// Se calcula precio
									if (obj.getArticuloVenta().getMontoDescuento().compareTo(new BigDecimal(Constantes.NRO_CERO)) < 0) {
										precio = obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento());
									} else {
										//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
										precio = obj.getArticuloVenta().getPrecio().subtract(obj.getArticuloVenta().getMontoDescuento().divide(new BigDecimal(vCantPrd)));
									}
									vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); // merge .22
							//merge .22										

									String vMrc = "< NR >";
									
									RetornoEjecucion retorno = new RetornoEjecucion();
//									retorno = clubDAO.lglreg_prc_mov_lis_vlr_mkp(	vCodigoReg,
//																					vArticulo2,
//																					sucursal, 
//																					FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3), 
//																					nroTransaccion, 
//																					Constantes.NRO_VEINTISIETE,
//																					Constantes.NRO_CERO,
//																					Constantes.NRO_CERO,
//																					vCantPrd, 
//																					vPrcCmp, 
//																					boleta, 
//																					nroCaja, 
//																					tipo, 
//																					obj.getArticuloVenta().getCorrelativoItem(), 
//																					direccionDespacho, 
//																					vMrc, 
//																					Constantes.NRO_CERO, 
//																					emailCliente, 
//																					nombreInvitado, 
//																					apellidoPaterno, 
//																					apellidoMaterno, 
//																					Constantes.VACIO, 
//																					Constantes.NRO_CERO, 
//																					mensajeTarjeta, 
//																					Constantes.NRO_CERO, 
//																					Constantes.NRO_CERO, 
//																					Constantes.NRO_CERO, 
//																					Constantes.NRO_CERO, 
//																					vArticulo+"-"+obj.getArticuloVenta().getDescRipley()
//																				);
//									
//									if(retorno.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//										logger.traceInfo("creacionRecaudacionesMKP", "Imposible de grabar en Novios Error - Mensaje: " + retorno.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(correlativo)));
//									} else {
//										logger.traceInfo("creacionRecaudacionesMKP", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativo)), new KeyLog("Correlativo item", String.valueOf(obj.getArticuloVenta().getCorrelativoItem())));
//									}
/*
				                	query3 = "{CALL LGLREG_PRC_MOV_LIS_VLR(?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,27,0,0,?,?,?,?,?,?,?,?,0,?,?,?,?,'',0,?,0,0,0,0,?,?)}";
									logger.traceInfo("creacionRecaudacionesMKP", "Creacion de REGISTRO NOVIOS SP[CLUB]: LGLREG_PRC_MOV_LIS_VLR("+vCodigoReg+","+vArticulo2+","+vSucursal+",TO_DATE("+fecha+",'DD/MM/YYYY'),"+nroTransaccion+",27,0,0,"+vCantPrd+","+vPrcCmp+","+boleta+","+nroCaja+","+tipo+","+obj.getArticuloVenta().getCorrelativoItem()+","+direccionDespacho+","+vMrc+",0,"+emailCliente+","+nombreInvitado+","+apellidoPaterno+","+apellidoMaterno+",'',0,"+mensajeTarjeta+",0,0,0,0,outCode,outMsj)", new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo2+Constantes.VACIO));
									conn3 = PoolBDs.getConnection(BDType.CLUB);
									if (conn3 != null) {
										try {
											cs = conn3.prepareCall(query3);
											cs.setString(1, vCodigoReg);
											cs.setLong(2, vArticulo2);
											cs.setInt(3, vSucursal);
											cs.setString(4, fecha);
											cs.setInt(5, nroTransaccion);
											cs.setInt(6, vCantPrd);
											cs.setInt(7, vPrcCmp);
											cs.setInt(8, boleta);
											cs.setInt(9, nroCaja);
											cs.setInt(10, tipo);
											cs.setInt(11, obj.getArticuloVenta().getCorrelativoItem());
											cs.setString(12, direccionDespacho);
											cs.setString(13, vMrc);
											cs.setString(14, emailCliente);
											cs.setString(15, nombreInvitado);
											cs.setString(16, apellidoPaterno);
											cs.setString(17, apellidoMaterno);
											cs.setString(18, mensajeTarjeta);
											cs.registerOutParameter(19, OracleTypes.NUMBER);
											cs.registerOutParameter(20, OracleTypes.VARCHAR);
											cs.execute();
											sal = new Integer(cs.getInt(19));
											sal2 = cs.getString(20);
											cs.close();
											//if(!Validaciones.validarVacio(sal2)){
												logger.traceInfo("creacionRecaudacionesMKP", "Resultados de LGLREG_PRC_MOV_LIS_VLR: outCode: "+sal+" outMsj: "+sal2, new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo2+Constantes.VACIO));
											//}
										} catch (Exception e) {
											logger.traceError("creacionRecaudacionesMKP",
													"Imposible de grabar registro de NOVIOS - Error: ", e);
											
											resultado = Constantes.FALSO; 
										}
									} else {
										logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos CLUBES");
										throw new AligareException("Error: Problemas de conexion a la base de datos CLUBES");
									} 
*/
					            }
							}

/*
							if((!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) 
									&& !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2)))
									&& newPrecio.intValue() != Constantes.NRO_CERO) {
						        Integer vTipoDesc = (Validaciones.validaInteger(obj.getArticuloVenta().getTipoDescuento()))?obj.getArticuloVenta().getTipoDescuento():new Integer(Constantes.NRO_CERO);       
						        Integer vMontoDes = (vMontoDesc.intValue() < Constantes.NRO_CERO)?new Integer(Constantes.NRO_CERO):new Integer((vTipoDesc.intValue() == Constantes.NRO_CERO)?(vMontoDesc.intValue() * vCantPrd.intValue()):(vMontoDesc.intValue() * Constantes.NRO_UNO));     
								query1 = "{CALL ? := INSERTA_ARTICULOS_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,0,?,?,?,?,?,0,5)}";
								logger.traceInfo("creacionRecaudacionesMKP", "Creacion de Articulo en BackOffice FN[BACK_OFFICE]: outCode = INSERTA_ARTICULOS_BACK(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+obj.getArticuloVenta().getCorrelativoItem()+","+obj.getArticuloVenta().getCodArticulo()+","+vendedor+","+codigoRegalo+",0,"+newPrecio+","+vCantPrd+","+vMontoDes+","+obj.getArticuloVenta().getTipoDescuento()+","+cud+",0,5)", new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",obj.getArticuloVenta().getCodArticulo()));
								if(conn1 == null){
									conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
								}
								if(conn1 != null){
									try {
										cs = conn1.prepareCall(query1);
										cs.registerOutParameter(1, OracleTypes.NUMBER);
										cs.setString(2, fecha);
										cs.setString(3, Constantes.FECHA_DDMMYYYY);
										cs.setInt(4, sucursal);
										cs.setInt(5, nroCaja);
										cs.setInt(6, nroTransaccion);
										cs.setInt(7, obj.getArticuloVenta().getCorrelativoItem());
										cs.setLong(8, Long.parseLong(obj.getArticuloVenta().getCodArticulo()));
										cs.setInt(9, Integer.parseInt(vendedor));
										cs.setInt(10, codigoRegalo);
										cs.setInt(11, newPrecio);
										cs.setInt(12, vCantPrd);
										cs.setInt(13, vMontoDes);
										cs.setInt(14, obj.getArticuloVenta().getTipoDescuento());
										cs.setString(15, cud);
										cs.executeUpdate();
										sal = new Integer(cs.getInt(1));
										logger.traceInfo("creacionRecaudacionesMKP", "Registro Articulos BO FN INSERTA_ARTICULOS_BACK[outCode]: "+sal, new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",obj.getArticuloVenta().getCodArticulo()));
										cs.close();
										
										if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_UNO) { //TODO REVISAR PROD
											logger.traceError("creacionRecaudacionesMKP",
													"Imposible crear registro Articulos en Backoffice Error - ", new AligareException("outCode: " + sal));

											resultado = Constantes.FALSO; //return false;
										}
									} catch (Exception e) {
										logger.traceError("creacionRecaudacionesMKP",
												"Se produjo un error en la ejecucion de la FN INSERTA_ARTICULOS_BACK: ", e);
										resultado = Constantes.FALSO; //return false;
									}
								}else{
									logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
									throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
								}
							}
*/
							
							if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
									&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))
									&& Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO
									&& !esExtgarantia){
//							if(Validaciones.validaInteger(strFechaConfirma) && strFechaConfirma.intValue() != Constantes.NRO_UNO && 
//									(!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14)) &&
//									 !Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))) && !esExtgarantia) {
								query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES(?,?,?,?,?,?)}";
								logger.traceInfo("creacionRecaudacionesMKP", "Actualiza Fecha Despacho en Articulo MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES("+correlativo+","+vArticulo+","+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho)+","+Constantes.FECHA_DDMMYYYY2+",outCode,outMsj)", new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Codigo articulo",vArticulo));
								if(conn2 == null){
									conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								}
								if(conn2 != null){
									try {									
										cs = conn2.prepareCall(query2);
										cs.setLong(1, correlativo);
										cs.setString(2, vArticulo);

										cs.setString(3, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaDespacho));
										cs.setString(4, Constantes.FECHA_DDMMYYYY2);
										cs.registerOutParameter(5, OracleTypes.NUMBER);
										cs.registerOutParameter(6, OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(5));
										sal2 = cs.getString(6);
										cs.close();
										if(sal.intValue() != Constantes.NRO_CERO){
											throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
										}
									} catch (Exception e) {
										logger.traceError("creacionRecaudacionesMKP",
												"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_ARTICULO_FECHA_DES: ", e);
										resultado = Constantes.FALSO;
									}
								}else{
									logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
							}
						}
					}
					if (contadorEsMKP == Constantes.NRO_CERO) {
						logger.traceInfo("creacionRecaudacionesMKP", "No existen Articulos de Venta", new KeyLog("Correlativo Venta",String.valueOf(correlativoVenta)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
						
						resultado = Constantes.FALSO; 
					}
				} else {
					logger.traceError("creacionRecaudacionesMKP", "No existe Despacho - Error", new AligareException("Correlativo Venta NULO en Despacho"));
					
					resultado = Constantes.FALSO; 
				}
				
				// Creacion Transaccion
//			    Timestamp fechaCreacionNV = (trama.getNotaVenta().getFechaHoraCreacion() != null)?trama.getNotaVenta().getFechaHoraCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
//			    if (rutEjecutivoVta != null && rutEjecutivoVta.length() > 1){
//			    	vendedor = String.valueOf(rutEjecutivoVta).substring(0, (String.valueOf(rutEjecutivoVta).length()-Constantes.NRO_UNO));
//			    }
//			    totalVentaMkp = new BigDecimal(Constantes.NRO_CERO);
//				if(trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()){
//					for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
//						totalVentaMkp = totalVentaMkp.add((Validaciones.validaBigDecimal(obj.getMontoVenta())&& obj.getEsMkp()==Constantes.ES_MKP_MKP)?obj.getMontoVenta(): new BigDecimal(Constantes.NRO_CERO));
//					}
//				}
				boolean seguir = Constantes.VERDADERO;
//			    query1 = "{CALL ? := INSERTA_TRANSACCION_BACK3(TO_DATE(?,?),?,?,?,?,27,?,0,TO_DATE('01/01/1901','dd/MM/yyyy'),0,0,0,?,?,0,?,?,0,0,TO_DATE(?,?),0,0,?,0,TO_DATE(?,?))}";
//				logger.traceInfo("creacionRecaudacionesMKP", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_TRANSACCION_BACK3(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+","+boleta+",27,"+totalVentaMkp+",0,TO_DATE('01/01/1901','dd/MM/yyyy'),0,0,0,"+supervisor+","+vendedor+",0,"+trama.getNotaVenta().getRutComprador()+","+((!Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoDescuento()))?null:trama.getNotaVenta().getMontoDescuento())+",0,0,TO_DATE("+(fecha + Constantes.SPACE + hms)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"),0,0,"+correlativo+",0,TO_DATE("+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNV)+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+"))", new KeyLog("Correlativo Venta",String.valueOf(correlativo)), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if(conn1 == null){
//					conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//				}
//				if(conn1 != null){
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(4, sucursal);
//						cs.setInt(5, nroCaja);
//						cs.setInt(6, nroTransaccion);
//						cs.setInt(7, boleta);
//						cs.setBigDecimal(8, totalVentaMkp);
//						cs.setInt(9, Integer.parseInt(supervisor));
//						cs.setInt(10, Integer.parseInt(vendedor));
//						cs.setInt(11, trama.getNotaVenta().getRutComprador());
//						if (!Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoDescuento())){
//						    cs.setNull(12, OracleTypes.NUMBER);
//					    } else{
//					    	cs.setBigDecimal(12, trama.getNotaVenta().getMontoDescuento());
//					    }
//						cs.setString(13, (fecha + Constantes.SPACE + hms));
//						cs.setString(14, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.setLong(15, correlativo);
//						cs.setString(16, FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNV));
//						cs.setString(17, Constantes.FECHA_DDMMYYYY_HHMMSS2);
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionRecaudacionesMKP", "Transaccion BO FN INSERTA_TRANSACCION_BACK3[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							//tipo = Constantes.NRO_CERO;
//							caja.saltarFolio(parametros, nroTransaccion);
//							resultado = Constantes.FALSO; 
//							seguir = Constantes.FALSO;
//							logger.traceError("creacionRecaudacionesMKP", "Imposible crear Encabezado Transaccion Error - ", new AligareException("outCode: " + sal));
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionRecaudacionesMKP", "Se produjo un error en la ejecucion de la FN INSERTA_TRANSACCION_BACK3: ", e);
//						resultado = Constantes.FALSO; 
//						seguir = Constantes.FALSO;
//					}
//				}else{
//					logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
//					throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//				}
				
				if(seguir){
					String fechaBol = fecha + Constantes.SPACE + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2 + Constantes.DOSPUNTOS + Constantes.STRING_CEROX2;
					if(conn2 == null){
						conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
					}
					if(conn2 != null){
						if(!Validaciones.validarVacio(trama.getNotaVenta().getIndicadorMkp()) && !Constantes.IND_MKP_AMBAS.equals(trama.getNotaVenta().getIndicadorMkp())){
	//						String fecha2 = fecha + Constantes.SPACE + FechaUtil.getCurrentHoursString();
							String fecha2 = fecha + Constantes.SPACE + caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros);
							query2 = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
							logger.traceInfo("creacionRecaudacionesMKP", "Actualizacion NOTA VENTA termino de la transacciones MODELO EXTENDIDO SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA("+Constantes.NRO_DOS+","+boleta+","+Constantes.NRO_CERO+","+fechaBol+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+fecha2+","+Constantes.FECHA_DDMMYYYY_HHMMSS2+","+nroCaja+","+nroTransaccion+","+vendedor+","+sucursal+","+correlativo+",outCode,outMsj,"+Constantes.NRO_CERO+")", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
							try {									
								cs = conn2.prepareCall(query2);
								cs.setInt(1, Constantes.NRO_DOS);
								cs.setInt(2, boletaOrigen);
								cs.setInt(3, Constantes.NRO_CERO);
								cs.setString(4, fechaBol);
								cs.setString(5, Constantes.FECHA_DDMMYYYY_HHMMSS2);
								cs.setString(6, fecha2);
								cs.setString(7, Constantes.FECHA_DDMMYYYY_HHMMSS2);
								cs.setInt(8, nroCaja);
								cs.setInt(9, boletaOrigen);
								cs.setLong(10, Long.parseLong(vendedor));//FIXME vendedor2 to vendedor
								cs.setInt(11, sucursal);
								cs.setLong(12, correlativo);
								cs.registerOutParameter(13, OracleTypes.NUMBER);
								cs.registerOutParameter(14, OracleTypes.VARCHAR);
								cs.setInt(15, Constantes.NRO_CERO);
								cs.execute();
								sal = new Integer(cs.getInt(13));
								sal2 = cs.getString(14);
								cs.close();
								if(sal.intValue() != Constantes.NRO_CERO){
									throw new AligareException("outCode: " + sal + " outMsj: "+ sal2);
								}
							} catch (Exception e) {
								logger.traceError("creacionRecaudacionesMKP",
										"Se produjo un error en la ejecucion del SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA: ", e);
								resultado = Constantes.FALSO; 
							}
							trama.getNotaVenta().setNumeroBoleta(nroTransaccion.longValue());
							trama.getNotaVenta().setCorrelativoBoleta(nroTransaccion.longValue());
						} 
					}else{
						logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
						throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
					}
				}//fin seguir
				
//				query1 = "{CALL ? := INSERTA_RECAUDACION_BO(TO_DATE(?,?),?,?,?,32000,9001,0,?,?,?,1)}";
//				logger.traceInfo("creacionRecaudacionesMKP", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_RECAUDACION_BO(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+",32000,9001,0,27-"+correlativo+","+totalVentaMkp+","+rutDespachoCambio+",1)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//				if(conn1 != null){
//					try {
//						cs = conn1.prepareCall(query1);
//						cs.registerOutParameter(1, OracleTypes.NUMBER);
//						cs.setString(2, fecha);
//						cs.setString(3, Constantes.FECHA_DDMMYYYY);
//						cs.setInt(4, sucursal);
//						cs.setInt(5, nroCaja);
//						cs.setInt(6, nroTransaccion);
//						cs.setString(7, "27-"+correlativo);
//						cs.setBigDecimal(8, totalVentaMkp);
//						cs.setLong(9, rutDespachoCambio);
//						cs.executeUpdate();
//						sal = new Integer(cs.getInt(1));
//						logger.traceInfo("creacionRecaudacionesMKP", "Recaudacion en monedero BO FN INSERTA_RECAUDACION_BO[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//						cs.close();
//						
//						if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//							//tipo = Constantes.NRO_CERO;
//							logger.traceError("creacionRecaudacionesMKP",
//									"Imposible crear recaudacion en monedero  Error - ", new AligareException("outCode: " + sal));
//							
//							resultado = Constantes.FALSO; 
//						}
//					} catch (Exception e) {
//						logger.traceError("creacionRecaudacionesMKP",
//								"Se produjo un error en la ejecucion de la FN INSERTA_RECAUDACION_BO: ", e);
//						
//						resultado = Constantes.FALSO; 
//					}
//				}
								
//				query1 = "{CALL ? := INSERTA_RECAUDACION_PRV_BO(TO_DATE(?,?),?,?,?,32000,?,?,0)}";
				if (trama.getIdentificadoresMarketplace() != null && !trama.getIdentificadoresMarketplace().isEmpty()) {
					for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()){
						if(Validaciones.validaInteger(obj.getEsMkp()) && obj.getEsMkp().intValue() == Constantes.NRO_UNO){
//							
							String ordenMkp = (Constantes.VACIO.equalsIgnoreCase(obj.getOrdenMkp()))?Constantes.VACIO:obj.getOrdenMkp();
							MkpMkl mkpMkl = new MkpMkl();
							mkpMkl = estadoMkpMkl(trama, ordenMkp,obj.getRutProveedor());
//							
//							
//							if(conn1 == null){
//								conn1 = PoolBDs.getConnection(BDType.BACK_OFFICE);
//							}
//							if(conn1 != null){
//								try {
//									cs = conn1.prepareCall(query1);
//									cs.registerOutParameter(1, OracleTypes.NUMBER);
//									cs.setString(2, fecha);
//									cs.setString(3, Constantes.FECHA_DDMMYYYY);
//									cs.setInt(4, sucursal);
//									cs.setInt(5, nroCaja);
//									cs.setInt(6, nroTransaccion);
//									cs.setString(7, obj.getRutProveedor());
//									cs.setInt(8, mkpMkl.getTotalVentaMkpRutPrv());
//
//									logger.traceInfo("creacionRecaudacionesMKP", "Inserta Transaccion BackOffice FN[BACK_OFFICE]: outCode = INSERTA_RECAUDACION_PRV_BO(TO_DATE("+fecha+","+Constantes.FECHA_DDMMYYYY+"),"+sucursal+","+nroCaja+","+nroTransaccion+",32000,"+obj.getRutProveedor()+","+mkpMkl.getTotalVentaMkpRutPrv()+",0)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//									
//									cs.executeUpdate();
//									sal = new Integer(cs.getInt(1));
//									logger.traceInfo("creacionRecaudacionesMKP", "Transaccion BO FN INSERTA_RECAUDACION_PRV_BO[outCode]: " + sal, new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
//									cs.close();
//									
//									if(Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO){
//										//tipo = Constantes.NRO_CERO;
//										logger.traceError("creacionRecaudacionesMKP",
//												"Imposible crear recaudacion en monedero  Error - ", new AligareException("outCode: " + sal));
//										
//										resultado = Constantes.FALSO; 
//									}
//								} catch (Exception e) {
//									logger.traceError("creacionRecaudacionesMKP",
//											"Se produjo un error en la ejecucion de la FN INSERTA_RECAUDACION_PRV_BO: ", e);
//									
//									resultado = Constantes.FALSO; 
//								}
//							}else{
//								logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos BACK OFFICE");
//								throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
//							}
//							
//							
							//JORTUZAR: Se comenta ya que según lo informado desde Perú, por defecto el stock se encuentra confirmado
							//por lo que la integración Mirkl PA01 se debe realizar.
//							if (mkpMkl.getEstadoMkpMkl()>Constantes.NRO_CERO){//nuevo 8.0.4
								String rutCompradorMkp = String.format(Constantes.FORMATO_8_CEROS_IZQ, trama.getNotaVenta().getRutComprador() != null ? trama.getNotaVenta().getRutComprador() : Constantes.NRO_CERO);
//								rutCompradorMkp = rutCompradorMkp + ((trama.getNotaVenta().getDvComprador()!=null)?trama.getNotaVenta().getDvComprador().toString():Constantes.STRING_CERO);
								//Nuevo insert MIRAKL
//								String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL(?,?,?,?,TO_TIMESTAMP(?,'DD/MM/YYYY'),?,?,?,?)}";
								String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL(?,?,?,?,TO_TIMESTAMP(?,'"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),?,?,?,?,?,?,?)}";
								logger.traceInfo("creacionRecaudacionesMKP", "Insert en SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL("+obj.getCorrelativoVenta()+","+Constantes.PA_DOS+","+obj.getOrdenMkp()+","+obj.getEstado()+",TO_TIMESTAMP("+fechaHora+",'"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),"
								+parametros.buscaValorPorNombre("USUARIO")+","+mkpMkl.getTotalVentaMkpSubOrden()+",'"+rutCompradorMkp+"',"+
								null+",outCode,outMsj1,outMsj2)", new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
								if(conn2 == null){
									conn2 = PoolBDs.getConnection(BDType.MODEL_EXTEND);
								}
								if(conn2 != null){
									try{
										cs = conn2.prepareCall(procSQL);
//										cs.setInt(1, obj.getCorrelativoVenta());
//										cs.setString(2, Constantes.PA_DOS);
//										cs.setString(3, obj.getOrdenMkp());
//										cs.setInt(4, obj.getEstado());
//										cs.setString(5, fecha);
//										cs.setString(6, parametros.buscaValorPorNombre("USUARIO"));
//										cs.registerOutParameter(7, OracleTypes.NUMBER);
//										cs.registerOutParameter(8, OracleTypes.VARCHAR);
//										cs.registerOutParameter(9, OracleTypes.VARCHAR);
//										cs.setInt(1, obj.getCorrelativoVenta());
										cs.setLong(1, obj.getCorrelativoVenta());
										cs.setString(2, Constantes.PA_UNO);
										cs.setString(3,obj.getOrdenMkp());
										cs.setInt(4, Constantes.NRO_UNO);//obj.getEstado());
										cs.setString(5, fechaHora);
										cs.setString(6, parametros.buscaValorPorNombre("USUARIO"));
//										cs.setInt(7, mkpMkl.getTotalVentaMkpSubOrden()); //nuevo 8.0.4
										cs.setBigDecimal(7, mkpMkl.getTotalVentaMkpSubOrden()); //nuevo 8.0.4
										//cs.setInt(8, (trama.getNotaVenta().getRutComprador()!=null)?trama.getNotaVenta().getRutComprador():Constantes.NRO_CERO);
										cs.setString(8, rutCompradorMkp);
										cs.setInt(9, OracleTypes.NULL);
										cs.registerOutParameter(10,OracleTypes.NUMBER);
										cs.registerOutParameter(11,OracleTypes.VARCHAR);
										cs.registerOutParameter(12,OracleTypes.VARCHAR);
										cs.execute();
										sal = new Integer(cs.getInt(10));
										logger.traceInfo("creacionRecaudacionesMKP", "Resultados de SP CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL: outCode: " + sal + " outMsj1: " + cs.getString(11) + " outMsj2: "+ cs.getString(12), new KeyLog("Correlativo venta", correlativo+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO));
										String sql = cs.getString(12);
										cs.close();
										
										if (Validaciones.validaInteger(sal) && sal.intValue() != Constantes.NRO_CERO) {
											logger.traceError("creacionRecaudacionesMKP", "Actualizacion PROC_INS_MIRAKL Insert realizado: - " + sql,
													new AligareException("outCode: " + sal));
											
											resultado = Constantes.FALSO;
										}
									} catch(Exception e){
										logger.traceError("creacionRecaudacionesMKP",
												"Se produjo un error en la ejecucion de la PL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL: ", e);
										
										resultado = Constantes.FALSO;
									}
								}else{
									logger.traceInfo("creacionRecaudacionesMKP", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
									throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
								}
//							}
//							
						}else{
							continue;
						}
					}// for
				}else{
					logger.traceError("creacionRecaudacionesMKP",
							"No existen datos en IdentificadorMarketplace: ", new AligareException("No existen Identificadores Marketplace en el listado"));
					resultado = Constantes.FALSO; 
				}
//			}
		} catch (AligareException ex) {
			logger.traceError("creacionRecaudacionesMKP", ex);
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
			logger.endTrace("creacionRecaudacionesMKP", "Finalizado", "boolean: AligareException");
			throw ex;
		} catch(Exception e){
			logger.traceError("creacionRecaudacionesMKP", "Se produjo un error inesperado: ", e);
			resultado = Constantes.FALSO; 
		} finally {
			if(conn1 != null){
				PoolBDs.closeConnection(conn1);
			}
			if(conn2 != null){
				PoolBDs.closeConnection(conn2);
			}
			if(conn3 != null){
				PoolBDs.closeConnection(conn3);
			}
		}
		
		if(resultado == Constantes.FALSO){
			//anulacionFolio(FechaUtil.stringToTimestamp(fecha, Constantes.FECHA_DDMMYYYY4), funcion, sucursal, nroCaja, nroTransaccion, nroTransaccion, correlativo, tipoDePago, supervisor, vendedor);
		}
		logger.endTrace("creacionRecaudacionesMKP", "Finalizado", "boolean: " + String.valueOf(resultado));
		return resultado;
	}
	
	private Integer tieneCud(String articulo){
		logger.initTrace("tieneCud", "String articulo: " + articulo, new KeyLog("Codigo articulo", articulo));
		Integer sal = new Integer(Constantes.NRO_CERO);
		/*se comenta porque en la tabla exinte flete
		 * String query = "select count(*) as CANTIDAD from tvirtual_articuloS_sin_cud where cod_art_ean13 = ?";
		logger.traceInfo("tieneCud", "Consulta a BACK OFFICE: " + query.substring(Constantes.NRO_CERO, query.length()+Constantes.NRO_MENOSUNO) + articulo, new KeyLog("Codigo articulo", articulo));
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn != null) {
			try {
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(query);
				if (ps != null) {
					ps.setString(1, articulo);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						sal = new Integer(rs.getInt("CANTIDAD"));
						logger.traceInfo("tieneCud", "Resultado a consulta: " + sal, new KeyLog("Codigo articulo", articulo));
						rs.close();
					}
					ps.close();
				}
			} catch (Exception e) {
				logger.traceInfo("tieneCud", "Se produjo un error[Se esperaba un valor numerico para codigo de producto]: "+e.getMessage(), new KeyLog("Codigo articulo", articulo));
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("tieneCud", "Problemas de conexion a la base de datos BACK OFFICE");
			logger.endTrace("tieneCud", "Finalizado", "Integer: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos BACK OFFICE");
		}*/
		logger.endTrace("tieneCud", "Finalizado", "Integer: " + sal.intValue());
		return sal;
	}
	
	private String comunaRegion(Despacho des, DatoFacturacion dt){
		String comuReg = null;
		if(Validaciones.validaInteger(des.getComunaFacturacion())&& Validaciones.validaInteger(des.getRegionFacturacion())){
			//comuReg = "Reg: " + des.getRegionDespachoDes() + " Com: " + des.getComunaDespachoDes();
			comuReg = "Reg: " + des.getRegionDespacho().intValue() + " Com: " + des.getComunaDespacho().intValue();
			if(Validaciones.validaLong(dt.getCorrelativoVenta())){
				comuReg = "Reg: " + dt.getRegionFacturaDes() + " Com: " + dt.getComunaFacturaDes();
			}
		}else{
			throw new AligareException("No existen datos de Facturacion");
		}
		
		return comuReg;
	}	
	
	private boolean revisarEmail(String jornada, Parametros parametros){
		//String jornadaAMPM = parametros.buscaValorPorNombre("JORNADA_AMPM");
		String citybox = parametros.buscaValorPorNombre("ENVIO_CITY_BOX");
		String[] jornada2 = citybox.split(",");
		boolean res = Constantes.FALSO;
		for (int i = Constantes.NRO_CERO; i < jornada2.length; i++) {
			if(jornada2[i].equalsIgnoreCase(jornada)){
				res = Constantes.VERDADERO;
				break;
			}
		}
		return res;
	}

	@Override
	public List<IdentificadorMarketplace> obtenerIdentificadorMarketplace(Long correlativoVenta) {

		logger.initTrace("obtenerIdentificadorMarketplace", "Integer correlativoVenta: " + correlativoVenta, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));

		ArrayList<IdentificadorMarketplace> listado = new ArrayList<IdentificadorMarketplace>();

		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);

		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_MARKETPLACE(?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerIdentificadorMarketplace", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_MARKETPLACE("+correlativoVenta+",outv,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(3);
				//logger.traceInfo("obtenerIdentificadorMarketplace", "Query: " + ((String) cs.getObject(2)), new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				if (rs != null) {
					while (rs.next()) {
						IdentificadorMarketplace marketplace = new IdentificadorMarketplace();
						marketplace.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						marketplace.setOrdenMkp(rs.getString("ORDEN_MKP"));
						marketplace.setRutProveedor(rs.getString("RUT_PROVEEDOR"));
						marketplace.setEstado(Validaciones.validaIntegerCeroXNull(rs.getInt("ESTADO")));
						marketplace.setEsMkp(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ES_MKP")));
						marketplace.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						marketplace.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						marketplace.setNroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_BOLETA")));
						marketplace.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						marketplace.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						marketplace.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_NOTA_CREDITO")));
						marketplace.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						marketplace.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						marketplace.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						marketplace.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						marketplace.setRazonSocial(rs.getString("RAZON_SOCIAL"));
						marketplace.setAmount(rs.getBigDecimal("AMOUNT"));
						marketplace.setIdRefund(new Integer(rs.getInt("REFUND_ID")));
						
						listado.add(marketplace);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerIdentificadorMarketplace", "Error en obtenerIdentificadorMarketplace: ", e);
				logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: Null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerIdentificadorMarketplace", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerIdentificadorMarketplace", "Finalizado", "ArrayList<IdentificadorMarketplace>: " + ((listado != null) ? listado.size() : 0));

		return listado;

	}

	@Override
	public boolean registraTransaccion(Parametros pcv, Integer nroTransaccion, String boleta, String fechaGen, String estGeneraOC,
			String observaciones, TramaDTE trama, int flag, int esNC) throws AligareException {
		logger.initTrace("registraTransaccion", "Integer Transacci�n: " + nroTransaccion + " - Integer Boleta: " + boleta + " - String fechaGen: " + fechaGen +
				" - String estGeneraOC: " + estGeneraOC + "String observaciones: " + observaciones + " - TramaDTE trama", new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("boleta", boleta+Constantes.VACIO));
		boolean registraTransaccion = Constantes.FALSO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
			String procSQL = "";
			String sqlLog = "";
			CallableStatement cs = null;
//			CajaVirtualDAO caja = new CajaVirtualDAOImpl();
			
			Long nroIdAperturaCierre = (caja.obtieneIdAperturaCierre(pcv)!=0)?caja.obtieneIdAperturaCierre(pcv):Constantes.NRO_CERO;
			BigInteger pan = null;
			if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
				  pan = trama.getTarjetaRipley().getPan();
			}
		
			try {
				procSQL = "{CALL CAVIRA_MANEJO_CAJA.PROC_REGISTRA_TRANSACCION(?,?,?,to_timestamp(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?)}";
				sqlLog = "{CALL CAVIRA_MANEJO_CAJA.PROC_REGISTRA_TRANSACCION(";
				//String codigoPan, String formaPago, Integer montoCompra
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, trama.getNotaVenta().getCorrelativoVenta());
				cs.setInt(2, nroTransaccion);
				cs.setString(3, boleta);
				cs.setString(4, fechaGen);
				cs.setInt(5, trama.getNotaVenta().getEstado());
				cs.setString(6, estGeneraOC);
				cs.setString(7, (observaciones.length()<250)?observaciones:observaciones.substring(0, 249));
				sqlLog = sqlLog + trama.getNotaVenta().getCorrelativoVenta()+ ","+nroTransaccion+","+boleta+",to_timestamp('"+fechaGen+"','DD/MM/YYYY HH24:MI:SS'),"+trama.getNotaVenta().getEstado()+",'"+estGeneraOC+"','"+((observaciones.length()<250)?observaciones:observaciones.substring(0, 249))+"',";
				if (pan != null){
					cs.setBigDecimal(8,new BigDecimal(pan.toString().trim()));//Nuevo 8.0.12 (trim)
					sqlLog = sqlLog + "'"+pan.toString().trim()+"',";
				}
				else{
					cs.setInt(8, OracleTypes.NULL);
					sqlLog = sqlLog + ",null,";
				}
				
				cs.setString(9, obtieneFormaPago(trama));
				sqlLog = sqlLog + "'"+obtieneFormaPago(trama)+"',";
				BigDecimal totalVentaMkp = (Validaciones.validaBigDecimal(trama.getNotaVenta().getMontoVenta()))?trama.getNotaVenta().getMontoVenta(): BigDecimal.ZERO;
				if (esNC == Constantes.NRO_CERO){
					if(flag != 2){
						totalVentaMkp = BigDecimal.ZERO;
						for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()) {
							if (obj.getEsMkp().intValue() == flag){
								totalVentaMkp = Util.getSubtotal(trama, flag);
							}
						}
					}
				} else {
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP)){
						totalVentaMkp = BigDecimal.ZERO;
						for (ArticuloVentaTramaDTE obj:trama.getArticuloVentas()){
							if (obj.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO || obj.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){
								continue;
							}
							totalVentaMkp = totalVentaMkp.add((obj.getArticuloVenta().getPrecio().multiply(new BigDecimal(obj.getArticuloVenta().getUnidades()))).subtract(obj.getArticuloVenta().getMontoDescuento()));
						}
					} else if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) {
						totalVentaMkp = BigDecimal.ZERO;
						for (ArticuloVentaTramaDTE obj:trama.getArticuloVentas()){
							if (obj.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO || obj.getIdentificadorMarketplace().getEsMkp() != flag){
								continue;
							}
							totalVentaMkp = totalVentaMkp.add((obj.getArticuloVenta().getPrecio().multiply(new BigDecimal(obj.getArticuloVenta().getUnidades()))).subtract(obj.getArticuloVenta().getMontoDescuento()));
						}
					} else {
						totalVentaMkp = BigDecimal.ZERO;
						for (ArticuloVentaTramaDTE obj:trama.getArticuloVentas()){
							if (obj.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){
								continue;
							}
							totalVentaMkp = totalVentaMkp.add((obj.getArticuloVenta().getPrecio().multiply(new BigDecimal(obj.getArticuloVenta().getUnidades())).subtract(obj.getArticuloVenta().getMontoDescuento())));
						}
					}
				}
				cs.setBigDecimal(10, totalVentaMkp);
				cs.setLong(11, nroIdAperturaCierre);
				sqlLog = sqlLog + totalVentaMkp + "," + nroIdAperturaCierre + ",outv, outn, outv)}";
				logger.traceInfo("registraTransaccion", sqlLog);
				cs.registerOutParameter(12, OracleTypes.VARCHAR);
				cs.registerOutParameter(13, OracleTypes.NUMBER);
				cs.registerOutParameter(14, OracleTypes.VARCHAR);
				cs.execute();    
				logger.traceInfo("registraTransaccion", "outv1"+cs.getString(12));
				logger.traceInfo("registraTransaccion", "outn"+cs.getInt(13));
				logger.traceInfo("registraTransaccion", "outv"+cs.getString(14));
				if (cs.getInt(13) == Constantes.EJEC_SIN_ERRORES){
					registraTransaccion = true;
		            logger.traceInfo("registraTransaccion", "Inserta en tabla XDETALLE_TRX_BO - Insert: "+cs.getString(12), new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("boleta", boleta+Constantes.VACIO));
				}
	            
	            cs.close();

			} catch (Exception e) {
				logger.traceError("registraTransaccion", "Error durante la ejecucion de registraTransaccion: ", e);
				logger.endTrace("registraTransaccion", "Finalizado", "boolean: "+registraTransaccion);
				return registraTransaccion;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("registraTransaccion", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("registraTransaccion", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("registraTransaccion", "Finalizado", "boolean: " + registraTransaccion);		
		return registraTransaccion;
	}
	
	private String obtieneFormaPago(TramaDTE trama){

		String formaPago="";
		if(trama.getTarjetaBancaria().getCorrelativoVenta()!=null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				formaPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				formaPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				formaPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				formaPago = formaPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				formaPago = formaPago + Constantes.TRAMA_PLAT_DEBITO;
			}
		}
		
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null){
			formaPago = Constantes.BOL_FORMA_PAGO_TRE;
		}
		
		if(trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			formaPago = Constantes.BOL_FORMA_PAGO_CAR;
		}
		
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			formaPago = Constantes.BOL_FORMA_PAGO_CT;
		}
		return formaPago;
	}
	
	@SuppressWarnings("unused")
	private int obtenerTipo(TramaDTE trama){
		int tipo=0;
		
		
		return tipo;
	}

	@Override
	public boolean actualizarNotaVentaEstadoFolio(Long correlativoVenta, Integer estado, String folio, int nroCaja, int flag) {
		logger.initTrace("actualizarNotaVentaEstadoFolio", "Integer correlativoVenta: " + correlativoVenta + " Integer estado: "+estado+" String folio: " + folio + " int flag: "+flag,
						new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO),
						new KeyLog("Estado", estado+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		
		respuesta = actualizarNotaVentaCnFlag(estado, Constantes.NRO_CERO, folio, Constantes.STRING_CERO, Constantes.STRING_CERO, Constantes.STRING_CERO, Constantes.STRING_CERO, new Integer(nroCaja), Constantes.NRO_CERO, Constantes.NRO_CERO, Constantes.NRO_CERO, correlativoVenta, flag);

		logger.endTrace("actualizarNotaVentaEstadoFolio", "Finalizado", "boolean: " + String.valueOf(respuesta));
		return respuesta;
	}

	@Override
	public boolean actualizarNotaVentaCnFlag(Integer estado, Integer boleta,
			String folioSII, String fechaBoleta, String formatoFechaBoleta,
			String horaBoleta, String formatoHoraBoleta, Integer numCaja,
			Integer correlativoBoleta, Integer rutBoleta, Integer sucursal,
			Long correlativoVenta, int flag) {
		logger.initTrace("actualizarNotaVentaCnFlag", "Integer estado: "+estado+" - Integer boleta: "+boleta+" - Integer folioSII: "+folioSII+" - String fechaBoleta: "+fechaBoleta+" - String formatoFechaBoleta: "+formatoFechaBoleta+
				" - String horaBoleta: "+horaBoleta+" - String formatoHoraBoleta: "+formatoHoraBoleta+" - Integer numCaja: "+numCaja+" - Integer correlativoBoleta: "+correlativoBoleta+" - Integer rutBoleta: "+rutBoleta+" - Integer sucursal: "+sucursal+" - Integer correlativoVenta: "+correlativoVenta+" - int flag: "+flag, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO), new KeyLog("Estado", estado+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				logger.traceInfo("actualizarNotaVentaCnFlag", "Actualiza Nota Venta SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA("+estado+","+boleta+","+folioSII+","+fechaBoleta+","+formatoFechaBoleta+","+horaBoleta+","+formatoHoraBoleta+","+numCaja+","+correlativoBoleta+","+rutBoleta+","+sucursal+","+correlativoVenta+",outCode,outMsj,"+flag+")", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				cs = conn.prepareCall(procSQL);

				cs.setInt(1, estado);
				cs.setInt(2, boleta);
				cs.setString(3, folioSII);
				cs.setString(4, fechaBoleta);
				cs.setString(5, formatoFechaBoleta);
				cs.setString(6, horaBoleta);
				cs.setString(7, formatoHoraBoleta);
				cs.setInt(8, numCaja);
				cs.setInt(9, correlativoBoleta);
				cs.setInt(10, rutBoleta);//FIXME vendedor2 to vendedor
				cs.setInt(11, sucursal);
				cs.setLong(12, correlativoVenta);
				cs.registerOutParameter(13, OracleTypes.NUMBER);
				cs.registerOutParameter(14, OracleTypes.VARCHAR);
				cs.setInt(15, flag);
				
				cs.execute();
				rsOut = cs.getInt(13);
				rsMsjOut = cs.getString(14);
				logger.traceInfo("actualizarNotaVentaCnFlag", "outCode: "+rsOut+" outMsj: "+rsMsjOut, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				respuesta = (rsOut == Constantes.NRO_CERO);
				cs.close();
			} catch (Exception e) {
				logger.traceError("actualizarNotaVentaCnFlag", "Error al actualizar nota de venta en SP CAVIRA_MANEJO_BOLETAS.PROC_UPD_NOTA_VENTA: ", e);
				respuesta = Constantes.FALSO;
			} finally {
				PoolBDs.closeConnection(conn);
			}
			
		} else {
			logger.traceInfo("actualizarNotaVentaCnFlag", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("actualizarNotaVentaCnFlag", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("actualizarNotaVentaCnFlag", "Finalizado", "boolean: " + String.valueOf(respuesta));

		return respuesta;

	}
	
	private MkpMkl estadoMkpMkl(TramaDTE trama, String ordenMkp, String rutProveedor){
		logger.initTrace("estadoMkpMkl", "TramaDTE : trama", new KeyLog("ordenMkp", ordenMkp));
		
		MkpMkl mkpMkl = new MkpMkl();
		mkpMkl.setEstadoMkpMkl(Constantes.NRO_CERO);
		mkpMkl.setTotalVentaMkpSubOrden(new BigDecimal(Constantes.NRO_CERO));
		mkpMkl.setTotalVentaMkpRutPrv(new BigDecimal(Constantes.NRO_CERO));
	
		BigDecimal totalVentaMkpRutPrv = new BigDecimal(Constantes.NRO_CERO);
		List<IdentificadorMarketplace> listIdentificadorMarketplace = trama.getIdentificadoresMarketplace(); 
		logger.traceInfo("estadoMkpMkl", "listIdentificadorMarketplace size: "+listIdentificadorMarketplace.size());
		for (IdentificadorMarketplace idenMkp : listIdentificadorMarketplace) {
			if (idenMkp.getRutProveedor().equals(rutProveedor)) {
				totalVentaMkpRutPrv = totalVentaMkpRutPrv.add(idenMkp.getMontoVenta());  
			}			
		}
		mkpMkl.setTotalVentaMkpRutPrv(totalVentaMkpRutPrv);
		logger.traceInfo("estadoMkpMkl", "totalVentaMkpSubOrden "+totalVentaMkpRutPrv);

		BigDecimal totalVentaMkpSubOrden = new BigDecimal(Constantes.NRO_CERO);
		List<ArticuloVentaTramaDTE> listArticuloVenta = trama.getArticuloVentas(); 
		logger.traceInfo("estadoMkpMkl", "listArticuloVenta size: "+listArticuloVenta.size());
		String estadoVenta = "";
		for (ArticuloVentaTramaDTE artVenta : listArticuloVenta) {
			estadoVenta = artVenta.getArticuloVenta().getEstadoVenta();
			if (estadoVenta != null) {
				estadoVenta = estadoVenta.trim();
			}
			logger.traceInfo("estadoMkpMkl", "\nlistArticuloVenta CorrelativoItem: "+artVenta.getArticuloVenta().getCorrelativoItem());
			logger.traceInfo("estadoMkpMkl", "EstadoVenta: "+estadoVenta);
			logger.traceInfo("estadoMkpMkl", "Precio: "+artVenta.getArticuloVenta().getPrecio().intValue());
			logger.traceInfo("estadoMkpMkl", "Unidades: "+artVenta.getArticuloVenta().getUnidades().intValue());
			logger.traceInfo("estadoMkpMkl", "MontoDescuento: "+artVenta.getArticuloVenta().getMontoDescuento().intValue());
			logger.traceInfo("estadoMkpMkl", "ordenMkp vs getOrdenMkp: "+ordenMkp+" / "+artVenta.getArticuloVenta().getOrdenMkp());
			if(!ordenMkp.equalsIgnoreCase(artVenta.getArticuloVenta().getOrdenMkp()) // ){
					|| (Constantes.SS.equalsIgnoreCase(estadoVenta))){
				continue;
			}		
			
			totalVentaMkpSubOrden = totalVentaMkpSubOrden.add((artVenta.getArticuloVenta().getPrecio().multiply(new BigDecimal(artVenta.getArticuloVenta().getUnidades())).subtract(artVenta.getArticuloVenta().getMontoDescuento())));
/*
			if(!Validaciones.validarVacio(artVenta.getArticuloVenta().getEstadoVenta()) //){
					&& Constantes.SC.equalsIgnoreCase(artVenta.getArticuloVenta().getEstadoVenta().trim())){
				mkpMkl.setEstadoMkpMkl(Constantes.NRO_UNO);
				
				logger.traceInfo("estadoMkpMkl", "mkpMkl EstadoMkpMkl "+mkpMkl.getEstadoMkpMkl());
				logger.traceInfo("estadoMkpMkl", "totalVentaMkpSubOrden "+totalVentaMkpSubOrden);
			}
			else{
				
				mkpMkl.setEstadoMkpMkl(Constantes.NRO_DOCE);
				logger.traceInfo("estadoMkpMkl", "mkpMkl EstadoMkpMkl "+mkpMkl.getEstadoMkpMkl());
				logger.traceInfo("estadoMkpMkl", "totalVentaMkpSubOrden "+totalVentaMkpSubOrden);
				
			}
*/
			if(!Validaciones.validarVacio(artVenta.getArticuloVenta().getEstadoVenta()) ){
				if (Constantes.SC.equalsIgnoreCase(artVenta.getArticuloVenta().getEstadoVenta().trim())){
					mkpMkl.setEstadoMkpMkl(Constantes.NRO_UNO);
				} else {
					mkpMkl.setEstadoMkpMkl(Constantes.NRO_DOCE);
				}
			}
			logger.traceInfo("estadoMkpMkl", "mkpMkl EstadoMkpMkl "+mkpMkl.getEstadoMkpMkl());
			logger.traceInfo("estadoMkpMkl", "totalVentaMkpSubOrden "+totalVentaMkpSubOrden);

		}
		mkpMkl.setTotalVentaMkpSubOrden(totalVentaMkpSubOrden);
		logger.traceInfo("estadoMkpMkl", "mkpMkl EstadoMkpMkl "+mkpMkl.getEstadoMkpMkl());
		logger.traceInfo("estadoMkpMkl", "totalVentaMkpSubOrden "+totalVentaMkpSubOrden);
		logger.traceInfo("estadoMkpMkl", "mkpMkl totalVentaMkpSubOrden "+mkpMkl.getTotalVentaMkpSubOrden());
		
		logger.endTrace("estadoMkpMkl", "Estado Mkp:"+mkpMkl.getEstadoMkpMkl(), "totalVentaMkpSubOrden: "+ mkpMkl.getTotalVentaMkpSubOrden());
		
		return mkpMkl;
	}

	@Override
	public boolean updateIdentificadorMkpBoleta(TramaDTE trama, Integer estado, Parametros pcv, int nroTransaccion, String fecha, int nroTransaccionOrigen) throws AligareException {
		logger.initTrace("updateIdentificadorMkpBoleta", "Trama trama");
		boolean retorno = true;
		CallableStatement cs = null;
		Connection conn = null;
		Integer codSalida = null;
		String mjeSalida = null;
		String sql="";
		Long correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		int nroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		List<IdentificadorMarketplace> listaIdentMkp = trama.getIdentificadoresMarketplace();
		conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			try {
				if (!listaIdentMkp.isEmpty()){
					String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_IDENT_MKP(?,?,?,?,?,?,?,?,?,?,?)}";
					for (IdentificadorMarketplace obj : trama.getIdentificadoresMarketplace()){						
						logger.traceInfo("updateIdentificadorMkpBoleta", "Actualiza el listado de Identificador Marketplace con estado "+estado+" [MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPD_IDENT_MKP("+estado+","+nroTransaccion+","+nroTransaccion+","+nroCaja+","+fecha+","+Constantes.FECHA_DDMMYYYY+","+correlativoVenta+","+obj.getOrdenMkp()+",outCode,outMsj,"+Constantes.NRO_CERO+")", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO), new KeyLog("nro_transaccion", nroTransaccion+Constantes.VACIO), new KeyLog("Correlativo boleta", obj.getCorrelativoBoleta()+Constantes.VACIO));
						cs = conn.prepareCall(procSQL);
						cs.setInt(1, estado);
						if(obj.getEsMkp() == 0) {
							if (nroTransaccionOrigen == 0) {
								cs.setNull(2, OracleTypes.NUMBER);
								cs.setNull(3, OracleTypes.NUMBER);
							} else {
								cs.setInt(2, nroTransaccionOrigen);
								cs.setInt(3, nroTransaccionOrigen);
							}
						}else {
							if (nroTransaccion == 0) {
								cs.setNull(2, OracleTypes.NUMBER);
								cs.setNull(3, OracleTypes.NUMBER);
							} else {
								cs.setInt(2, nroTransaccion);
								cs.setInt(3, nroTransaccion);
							}
						}
						cs.setInt(4, nroCaja);
						cs.setString(5, fecha);
						cs.setString(6, Constantes.FECHA_DDMMYYYY);
						cs.setLong(7, correlativoVenta);
						cs.setString(8, obj.getOrdenMkp());
						cs.registerOutParameter(9, OracleTypes.NUMBER);
						cs.registerOutParameter(10, OracleTypes.VARCHAR);
						cs.setInt(11, Constantes.NRO_CERO);//flag
						cs.execute();
						codSalida = new Integer(cs.getInt(9));
						mjeSalida = cs.getString(10);
						cs.close();						
					}
					
				}
				
				if (Validaciones.validaInteger(codSalida) && codSalida.intValue() != Constantes.NRO_CERO) {
					logger.traceError("updateIdentificadorMkpBoleta",
							"--> Imposible hacer Update - " + sql,
							new Exception("CodError: " + codSalida.toString() + " MjeError"+mjeSalida));
					retorno=false;
				}
				
			} catch (Exception e) {
				logger.traceError("updateIdentificadorMkpBoleta","Error durante la ejecuci�n de updateIdentificadorMkpBoleta", e);
				logger.endTrace("updateIdentificadorMkpBoleta", "Finalizado", "retorno: "+retorno);
				return retorno;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("updateIdentificadorMkpBoleta","Problemas de conexion a la base de datos");
		}		
		logger.endTrace("updateIdentificadorMkpBoleta", "Finalizado", "retorno: "+retorno);
		return retorno;
	}
	
//	private RetornoEjecucion anulacionFolio(Timestamp fecha,
//											 int funcion,
//											 int sucursal,
//											 int nroCaja,
//											 int nroTrx,
//											 int nroBoleta,
//											 int correlativoVenta,
//											 int tipoDePago,
//											 String supervisor,
//											 String vendedor) throws AligareException{
//		
//		logger.initTrace("anulacionFolio", "Timestamp fecha: "+fecha+","
//											+"int funcion: "+funcion+","
//											+"int sucursal: "+sucursal+","
//											+"int nroCaja: "+nroCaja+","
//											+"int nroTrx: "+nroTrx+","
//											+"int nroBoleta: "+nroBoleta+","
//											+"int correlativoVenta: "+correlativoVenta+","
//											+"int tipoDePago: "+tipoDePago+","
//											+"String supervisor: "+supervisor+","
//											+"String vendedor:"+vendedor+"\nint: "+funcion , new KeyLog("nroBoleta: ", ""+nroBoleta));
//		
//		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
//		RetornoEjecucion retornoEjecucion = new RetornoEjecucion();
//		String procSQL;
//		CallableStatement cs = null;
//		if (conn!=null){
//				try {
//					procSQL = "{call anulacion_caja_insert(TO_DATE(?,?),?,?,?,?,?,?,?)}";
//					
//					logger.traceInfo("anulacionFolio", "Procede a anula insert de boleta");// - query: "+procSQL);
//					cs = conn.prepareCall(procSQL);
//					if (cs != null) {
//						cs.setString(1,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha));
//						cs.setString(2,  Constantes.FECHA_DDMMYYYY);
//						cs.setInt(3,sucursal);
//						cs.setInt(4,nroCaja);
//						cs.setInt(5,nroTrx);
//						cs.setInt(6,nroBoleta);
//						cs.setInt(7,tipoDePago);
//						cs.setString(8,supervisor);
//						cs.setString(9,Util.getVacioPorNulo(vendedor).trim());
//						
//						logger.traceInfo("anulacionFolio", "{call anulacion_caja_insert(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+nroCaja+","+nroTrx+","+nroBoleta+","+tipoDePago+",'"+supervisor+"','"+vendedor+"')}");
//						
//						cs.execute();	
//						
//						retornoEjecucion.setCodigo(Constantes.EJEC_SIN_ERRORES);
//						retornoEjecucion.setMensaje("Anulacion exitosa.");
//						
//						cs.close();
//					}else{
//						retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoEjecucion.setMensaje("Error al generar CallableStatement: Null");
//						logger.traceInfo("anulacionFolio", "Error: "+retornoEjecucion.getMensaje());
//					}
//					
//				} catch (Exception e) {
//					logger.traceError("anulacionFolio", e);
//					retornoEjecucion.setMensaje("Mensaje de error anulacionFolio: "+e.getMessage());
//					retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
//				} finally {
//					PoolBDs.closeConnection(conn);
//				}
//		}else{
//			logger.traceInfo("anulacionFolio", "Problemas de conexion a la base de datos Back Office");
//			retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
//			retornoEjecucion.setMensaje("Conexion nula");
//		}
//		logger.endTrace("anulacionFolio", "Finalizado", "retornoEjecucion: " + retornoEjecucion.toString());
//		
//		return retornoEjecucion;
//		
//	}
	
//	private int obtieneTipoDePago(TramaDTE trama){
//		
//		int tipoDePago=Constantes.NRO_ONCE; //Medio de pago es 11 cuando no es tarjeta Ripley
//		if (Validaciones.validaInteger(trama.getTarjetaRipley().getCorrelativoVenta())){
//			tipoDePago = Constantes.NRO_QUINCE;//tarjeta ripley  15
//		}
//		
//		return tipoDePago;
//	}

	@Override
	public boolean updateConvenioBancaria(TramaDTE trama) throws AligareException {
		logger.initTrace("updateConvenioBancaria", "TramaDTE trama", new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO), new KeyLog("Codigo convenio", trama.getTarjetaBancaria().getCodigoConvenio()+Constantes.VACIO));
		boolean retorno = Constantes.VERDADERO;
		CallableStatement cs = null;
		Connection conn = null;
		Integer codSalida = null;
		String mjeSalida = null;
//		String sql="";
		Long correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		int codigoConvenio = trama.getTarjetaBancaria().getCodigoConvenio();

		conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_CONVENIO_BANCARIA(?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.setInt(2, codigoConvenio);
				cs.registerOutParameter(3, OracleTypes.NUMBER);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);

				logger.traceInfo("updateConvenioBancaria", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_CONVENIO_BANCARIA("+correlativoVenta+","+codigoConvenio+",outn,outv)}");
				
				cs.execute();
				codSalida = new Integer(cs.getInt(3));
				mjeSalida = cs.getString(4);
				cs.close();						

				if (Validaciones.validaInteger(codSalida) && codSalida.intValue() != Constantes.NRO_CERO) {
					logger.traceError("updateConvenioBancaria", "Error al actualizar codigo convenio", new Exception("CodError: " + codSalida.toString() + " MjeError"+mjeSalida));
					retorno = Constantes.FALSO;
				}
	
			} catch (Exception e) {
				logger.traceError("updateConvenioBancaria","Error durante la ejecuci�n de updateConvenioBancaria", e);
				logger.endTrace("updateConvenioBancaria", "Finalizado", "retorno: "+retorno);
				return retorno;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("updateConvenioBancaria","Problemas de conexion a la base de datos");
		}		
		logger.endTrace("updateConvenioBancaria", "Finalizado", "retorno: "+retorno);

		return retorno;
	}

	@Override
	public boolean updateDireccionDespachoHistorico(TramaDTE trama) throws AligareException {
		logger.initTrace("updateDireccionDespachoHistorico", "TramaDTE trama", new KeyLog("Correlativo venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO));
		boolean retorno = Constantes.VERDADERO;
		CallableStatement cs = null;
		Connection conn = null;
		Integer codSalida = null;
		String mjeSalida = null;
//		String sql="";
		
		Integer rutCliente			= trama.getDespacho().getRutCliente();
		String direccionDespacho	= trama.getDespacho().getDireccionDespacho();
		Integer regionDespacho		= trama.getDespacho().getRegionDespacho();
		Integer comunaDespacho		= trama.getDespacho().getComunaDespacho();
		
		String tipoDespacho			= "";
		String tiposDespachos[]		= Constantes.DESPACHO_DOMICILIO.split(",");
		
		List<ArticuloVentaTramaDTE> articulos = trama.getArticuloVentas();
		logger.traceInfo("updateDireccionDespachoHistorico","se buscara tipo despacho en "+articulos.size()+ " articulos de venta");
		for (int a = 0; a < articulos.size(); a++){
			if (articulos.get(a).getArticuloVenta().getTipoDespacho() != null && !articulos.get(a).getArticuloVenta().getTipoDespacho().equalsIgnoreCase(Constantes.STRING_CERO)){
				tipoDespacho = articulos.get(a).getArticuloVenta().getTipoDespacho();
				logger.traceInfo("updateDireccionDespachoHistorico","tipoDespacho encontrado: "+tipoDespacho);
				break;
			}
		}
		
		if (tipoDespacho.isEmpty()){
			logger.traceInfo("updateDireccionDespachoHistorico","tipoDespacho no encontrado en listado de articulos de venta");
			return true;
		}
		
		boolean continuar = false;
		for (int t = 0; t < tiposDespachos.length; t++){
			logger.traceInfo("updateDireccionDespachoHistorico","comparacion de tipo despacho: " + tipoDespacho + " v/s " +tiposDespachos[t]);
			if (tipoDespacho.equalsIgnoreCase(tiposDespachos[t])){
				logger.traceInfo("updateDireccionDespachoHistorico","tipoDespacho coinciden");
				continuar = true;
				break;
			}
		}

		if (!continuar){
			logger.traceInfo("updateDireccionDespachoHistorico","tipoDespacho no coincide con alguno de despacho a domicilio");
			return true;
		}
		
		conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_DIRECCION_HIST(?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, rutCliente);
				cs.setString(2, direccionDespacho);
				cs.setInt(3, regionDespacho);
				cs.setInt(4, comunaDespacho);
				cs.registerOutParameter(5, OracleTypes.NUMBER);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);

				logger.traceInfo("updateDireccionDespachoHistorico", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPD_DIRECCION_HIST("+rutCliente+",'"+direccionDespacho+"',"+regionDespacho+","+comunaDespacho+",outn,outv)}");
				
				cs.execute();
				codSalida = new Integer(cs.getInt(5));
				mjeSalida = cs.getString(6);
				cs.close();						

				if (Validaciones.validaInteger(codSalida) && codSalida.intValue() != Constantes.NRO_CERO) {
					logger.traceError("updateDireccionDespachoHistorico", "Error al actualizar direccion despacho historica", new Exception("CodError: " + codSalida.toString() + " MjeError"+mjeSalida));
					retorno = Constantes.FALSO;
				}
	
			} catch (Exception e) {
				logger.traceError("updateDireccionDespachoHistorico","Error durante la ejecuci�n de updateDireccionDespachoHistorico", e);
				logger.endTrace("updateDireccionDespachoHistorico", "Finalizado", "retorno: "+retorno);
				return retorno;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("updateDireccionDespachoHistorico","Problemas de conexion a la base de datos");
		}		
		logger.endTrace("updateDireccionDespachoHistorico", "Finalizado", "retorno: "+retorno);

		return retorno;
	}
	       
	@Override
	public ConcurrentLinkedQueue<TramaDTE> obtenerOrdenesParaGeneracion(Integer nroSucusal,
			Integer nroEstado) {

		logger.initTrace("obtenerOrdenesParaGeneracion", "Integer nroSucusal: "
				+ nroSucusal + " - Integer nroEstado: " + nroEstado, new KeyLog("Numero sucursal", nroSucusal+Constantes.VACIO), new KeyLog("Estado", nroEstado+Constantes.VACIO));

		ConcurrentLinkedQueue<TramaDTE> listado = new ConcurrentLinkedQueue<TramaDTE>();

		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_BOLETAS(?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				//cs.setInt(1, nroCaja);
				cs.setInt(1, nroSucusal);
				cs.setInt(2, nroEstado);
				cs.registerOutParameter(3, OracleTypes.CLOB);
				cs.registerOutParameter(4, OracleTypes.CURSOR);
				cs.registerOutParameter(5, OracleTypes.NUMBER);
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerOrdenesParaGeneracion","{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_BOLETAS_MPOS("+nroSucusal+","+nroEstado+",outc,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(4);

				if (rs != null) {
					//logger.traceInfo("obtenerOrdenesParaGeneracion", "Query: " + cs.getString(4));
					while (rs.next()) {
						NotaVenta notaVenta = new NotaVenta();
						notaVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
						notaVenta.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						notaVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						notaVenta.setRutComprador(Validaciones.validaLongCeroXNull(rs.getLong("RUT_COMPRADOR")));
						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						notaVenta.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						notaVenta.setNumeroBoleta(Validaciones.validaLongCeroXNull(rs.getLong("NUMERO_BOLETA")));
						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						notaVenta.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
						notaVenta.setNumNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("NUM_NOTA_CREDITO")));
						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						notaVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
						notaVenta.setFolioNcSii(Validaciones.validaLongCeroXNull(rs.getLong("FOLIO_NC_SII")));
						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
						notaVenta.setUsuario(rs.getString("USUARIO"));
						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));

						TarjetaRipley tarjetaRipley = new TarjetaRipley();
						tarjetaRipley.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
						tarjetaRipley.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("ADMINISTRADORA")));
						tarjetaRipley.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("EMISOR")));
						tarjetaRipley.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TARJETA")));
						tarjetaRipley.setRutTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_TITULAR")));
						tarjetaRipley.setRutPoder(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_PODER")));
						tarjetaRipley.setPlazo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("PLAZO")));
						tarjetaRipley.setDiferido(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DIFERIDO")));
						tarjetaRipley.setMontoCapital(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_CAPITAL")));
						tarjetaRipley.setMontoPie(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_PIE")));
						tarjetaRipley.setDescuentoCar(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("DESCUENTO_CAR")));
						tarjetaRipley.setCodigoDescuento(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("CODIGO_DESCUENTO")));
						tarjetaRipley.setPrefijoTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_TITULAR")));
						tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
						tarjetaRipley.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CLIENTE")));
						tarjetaRipley.setTipoCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CREDITO")));
						tarjetaRipley.setPrefijoPoder(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_PODER")));
						tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
						tarjetaRipley.setValorCuota(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("VALOR_CUOTA")));
						tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
						tarjetaRipley.setSernacFinanCae(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CAE")));
						tarjetaRipley.setSernacFinanCt(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CT")));
						tarjetaRipley.setTvtriMntMntFnd(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_FND")));
						tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
						tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
						tarjetaRipley.setTvtriCodImpTsaEfe(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_COD_IMP_TSA_EFE")));
						tarjetaRipley.setTvtriMntMntEva(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_EVA")));
						tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
						tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
						tarjetaRipley.setCodigoCore(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CORE")));
						tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));

						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TIPO_TARJETA")));
						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
						tarjetaBancaria.setMontoTarjeta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_TARJETA")));
						tarjetaBancaria.setVd(rs.getString("VD"));
						tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
						tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
						tarjetaBancaria.setCodigoConvenio(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));

						TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
						tarjetaRegaloEmpresa.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
						tarjetaRegaloEmpresa.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_ADMINISTRADORA")));
						tarjetaRegaloEmpresa.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_EMISOR")));
						tarjetaRegaloEmpresa.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_TARJETA")));
						tarjetaRegaloEmpresa.setCodigoAutorizador(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TRE_COD_AUTORIZADOR")));
						tarjetaRegaloEmpresa.setRutCliente(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_CLIENTE")));
						tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
						tarjetaRegaloEmpresa.setMonto(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO")));
						tarjetaRegaloEmpresa.setNroTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_TARJETA")));
						tarjetaRegaloEmpresa.setFlag(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("FLAG")));

						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));

						TipoDoc tipoDoc = new TipoDoc();
						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

						Vendedor vendedor = new Vendedor();
						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));

						List<ArticuloVentaTramaDTE> articuloVentas = obtenerArticuloVentas(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						
						List<IdentificadorMarketplace> identificadoresMarketplace = obtenerIdentificadorMarketplace(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));

						TramaDTE tramaDTE = new TramaDTE();
						tramaDTE.setNotaVenta(notaVenta);
						tramaDTE.setTarjetaRipley(tarjetaRipley);
						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
						tramaDTE.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
						tramaDTE.setDatoFacturacion(datoFacturacion);
						tramaDTE.setDespacho(despacho);
						tramaDTE.setTipoDoc(tipoDoc);
						tramaDTE.setArticuloVentas(articuloVentas);
						tramaDTE.setIdentificadoresMarketplace(identificadoresMarketplace);
						listado.add(tramaDTE);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerOrdenesParaGeneracion", "Error en obtenerOrdenesParaGeneracion: ", e);
				logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerOrdenesParaGeneracion", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerOrdenesParaGeneracion", "Finalizado",
				"ConcurrentLinkedQueue<TramaDTE>.size: " + ((listado != null) ? listado.size() : 0));
		return listado;
	}
	
	@Override
	public boolean ingresarDocumentoElectronico(Long correlativoVenta,
			Integer numCaja, Integer sucursal, String tramaDTE, String xmlTDE, String xmlMail, String pdf417) {
		logger.initTrace("ingresarDocumentoElectronico"
				, "Integer correlativoVenta: " +correlativoVenta+" Integer numCaja: "+numCaja+" Integer sucursal: "+sucursal+" String tramaTDE: "+xmlTDE+" xmlMail: "+xmlMail+" String pdf417: "+pdf417
				, new KeyLog("CorrelativoVenta", correlativoVenta+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;	
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_INS_DOCUMENTO_ELECTRONICO(?,?,?,?,?,?,?,?,?,?)}";
				logger.traceInfo("ingresarDocumentoElectronico", "Ingresa Documento Electronico SP[MODEL_EXTEND]: CAVIRA_ORDEN_COMPRA.PROC_DOCUMENTO_ELECTRONICO("+correlativoVenta+","+numCaja+","+sucursal+","+tramaDTE+","+xmlTDE+","+xmlMail+",outCode,outMsj)", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				cs = conn.prepareCall(procSQL);

				cs.setLong(1, correlativoVenta);
				cs.setInt(2, sucursal);
				cs.setInt(3, numCaja);
				cs.setString(4, tramaDTE);
				cs.setString(5, xmlTDE);
				cs.setString(6, xmlMail);
				cs.setString(7, pdf417);
				cs.registerOutParameter(8, OracleTypes.NUMBER);
				cs.registerOutParameter(9, OracleTypes.VARCHAR);
				cs.registerOutParameter(10, OracleTypes.VARCHAR);
				
				cs.execute();
				rsOut = cs.getInt(8);
				rsMsjOut = cs.getString(9);
				logger.traceInfo("ingresarDocumentoElectronico", "outCode: "+rsOut+" outMsj: "+rsMsjOut, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				respuesta = (rsOut == Constantes.NRO_CERO);
				cs.close();
			} catch (Exception e) {
				logger.traceError("ingresarDocumentoElectronico", "Error al ingresar documento electronico en SP CAVIRA_ORDEN_COMPRA.PROC_DOCUMENTO_ELECTRONICO: ", e);
				respuesta = Constantes.FALSO;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("ingresarDocumentoElectronico", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("ingresarDocumentoElectronico", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("ingresarDocumentoElectronico", "Finalizado", "boolean: " + String.valueOf(respuesta));

		return respuesta;

	}
	
	@Override
	public List<TramaDTE> obtenerUnaOrdenParaGeneracion(Long correlativoVenta, Integer nroCaja, Integer nroSucusal,
			Integer nroEstado) {

		logger.initTrace("obtenerUnaOrdenParaGeneracion", "Long correlativoVenta: " + correlativoVenta + " - Integer nroCaja: " + nroCaja + " - Integer nroSucusal: "
				+ nroSucusal + " - Integer nroEstado: " + nroEstado, new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)), new KeyLog("Numero Caja", nroCaja+Constantes.VACIO), new KeyLog("Numero sucursal", nroSucusal+Constantes.VACIO), new KeyLog("Estado", nroEstado+Constantes.VACIO));

		List<TramaDTE> listado = new ArrayList<TramaDTE>();

		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_UNA_BOLETA(?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.setInt(2, nroCaja);
				cs.setInt(3, nroSucusal);
				cs.setInt(4, nroEstado);
				cs.registerOutParameter(5, OracleTypes.CLOB);
				cs.registerOutParameter(6, OracleTypes.CURSOR);
				cs.registerOutParameter(7, OracleTypes.NUMBER);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerUnaOrdenParaGeneracion","{CALL CAVIRA_MANEJO_BOLETAS.PROC_OBT_UNA_BOLETA(" + correlativoVenta + ","+nroCaja+","+nroSucusal+","+nroEstado+",outc,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(6);

				if (rs != null) {
					//logger.traceInfo("obtenerOrdenesParaGeneracion", "Query: " + cs.getString(4));
					while (rs.next()) {
						NotaVenta notaVenta = new NotaVenta();
						notaVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
						notaVenta.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						notaVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						notaVenta.setRutComprador(Validaciones.validaLongCeroXNull(rs.getLong("RUT_COMPRADOR")));
						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						notaVenta.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						notaVenta.setNumeroBoleta(Validaciones.validaLongCeroXNull(rs.getLong("NUMERO_BOLETA")));
						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						notaVenta.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
						notaVenta.setNumNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("NUM_NOTA_CREDITO")));
						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						notaVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
						notaVenta.setFolioNcSii(Validaciones.validaLongCeroXNull(rs.getLong("FOLIO_NC_SII")));
						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
						notaVenta.setUsuario(rs.getString("USUARIO"));
						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//						notaVenta.setRuc(rs.getString("RUC"));
//						notaVenta.setRazonSocial(rs.getString("NV_RAZON_SOCIAL"));
						notaVenta.setNombreApellido(rs.getString("NOMBRE_APELLIDO"));
//						notaVenta.setPaypalTipoCambio(rs.getBigDecimal("PAYPAL_TIPO_CAMBIO"));
						notaVenta.setEnviaDoceFisico(rs.getInt("ENVIA_DOCE_FISICO"));
						notaVenta.setPcMac(rs.getString("NV_PC_MAC"));
						notaVenta.setSucursalMac(Validaciones.validaIntegerCeroXNull(rs.getInt("NV_SUCURSAL_MAC")));
						notaVenta.setKioskoVendedor(rs.getString("KIOSKO_VENDEDOR"));
						notaVenta.setKioskoVendedorMac(rs.getString("KIOSKO_VENDEDOR_MAC"));

						TarjetaRipley tarjetaRipley = new TarjetaRipley();
						tarjetaRipley.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
						tarjetaRipley.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("ADMINISTRADORA")));
						tarjetaRipley.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("EMISOR")));
						tarjetaRipley.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TARJETA")));
						tarjetaRipley.setRutTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_TITULAR")));
						tarjetaRipley.setRutPoder(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_PODER")));
						tarjetaRipley.setPlazo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("PLAZO")));
						tarjetaRipley.setDiferido(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DIFERIDO")));
						tarjetaRipley.setMontoCapital(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_CAPITAL")));
						tarjetaRipley.setMontoPie(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_PIE")));
						tarjetaRipley.setDescuentoCar(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("DESCUENTO_CAR")));
						tarjetaRipley.setCodigoDescuento(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("CODIGO_DESCUENTO")));
						tarjetaRipley.setPrefijoTitular(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_TITULAR")));
						tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
						tarjetaRipley.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CLIENTE")));
						tarjetaRipley.setTipoCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_CREDITO")));
						tarjetaRipley.setPrefijoPoder(Validaciones.validaIntegerCeroXNull(rs.getInt("PREFIJO_PODER")));
						tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
						tarjetaRipley.setValorCuota(Validaciones.validaBigDecimalNullXCero(rs.getBigDecimal("VALOR_CUOTA")));
						tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
						tarjetaRipley.setSernacFinanCae(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CAE")));
						tarjetaRipley.setSernacFinanCt(Validaciones.validaIntegerCeroXNull(rs.getInt("SERNAC_FINAN_CT")));
						tarjetaRipley.setTvtriMntMntFnd(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_FND")));
						tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
						tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
						tarjetaRipley.setTvtriCodImpTsaEfe(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_COD_IMP_TSA_EFE")));
						tarjetaRipley.setTvtriMntMntEva(Validaciones.validaIntegerCeroXNull(rs.getInt("TVTRI_MNT_MNT_EVA")));
						tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
						tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
						tarjetaRipley.setCodigoCore(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CORE")));
						tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));

						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ID_CANAL")));
						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
						tarjetaBancaria.setMontoTarjeta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_TARJETA")));
						tarjetaBancaria.setVd(rs.getString("TIPO_TARJETA"));
						tarjetaBancaria.setIdCanal(rs.getLong("ID_CANAL"));
						tarjetaBancaria.setDescripcionCanal(rs.getString("DESCRIPCION_CANAL"));
						tarjetaBancaria.setBinNumber(rs.getString("BIN_NUMBER"));
						tarjetaBancaria.setDiferido(rs.getString("DIFERIDO"));
						tarjetaBancaria.setIdFormaPago(rs.getLong("ID_FORMA_PAGO"));
						tarjetaBancaria.setDescripcionFormaPago(rs.getString("DESCRIPCION_PAGO"));
						tarjetaBancaria.setUltimosDigitosTarjeta(rs.getString("ULTIMOS_DIGITOS"));
//						tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
//						tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
//						tarjetaBancaria.setCodigoConvenio(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));
						tarjetaBancaria.setPlazo(rs.getInt("PLAZO_TB"));
						tarjetaBancaria.setGlosa(rs.getString("GLOSA_MP"));
						tarjetaBancaria.setIdentificadorTransaccion(Validaciones.validaLongCeroXNull(rs.getLong("MP_IDENTIFICADOR_TRX")));

						TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
						tarjetaRegaloEmpresa.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
						tarjetaRegaloEmpresa.setAdministradora(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_ADMINISTRADORA")));
						tarjetaRegaloEmpresa.setEmisor(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_EMISOR")));
						tarjetaRegaloEmpresa.setTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("TRE_TARJETA")));
						tarjetaRegaloEmpresa.setCodigoAutorizador(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TRE_COD_AUTORIZADOR")));
						tarjetaRegaloEmpresa.setRutCliente(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("RUT_CLIENTE")));
						tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
						tarjetaRegaloEmpresa.setMonto(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO")));
						tarjetaRegaloEmpresa.setNroTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_TARJETA")));
						tarjetaRegaloEmpresa.setFlag(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("FLAG")));

						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						String comunaDes = rs.getString("COMUNA_DESPACHO_DES");
						despacho.setComunaDespachoDes(Validaciones.validarVacio(comunaDes) ? Constantes.VACIO : comunaDes);
						String regDes = rs.getString("REGION_DESPACHO_DES");
						despacho.setRegionDespachoDes(Validaciones.validarVacio(regDes) ? Constantes.VACIO : regDes);
//						despacho.setEmailIdPaypal(rs.getString("EMAIL_IDPAYPAL"));

						TipoDoc tipoDoc = new TipoDoc();
						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));
						
						logger.traceInfo("obtenerUnaOrdenParaGeneracion","tipoDoc: codPpl=" + tipoDoc.getCodPpl() + "codBo=" + tipoDoc.getCodBo());

						Vendedor vendedor = new Vendedor();
//						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
//						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));
						//vendedor.setRutVendedor(Validaciones.validarVacio(rs.getString("KIOSKO_VENDEDOR")) ? "12497627" : rs.getString("KIOSKO_VENDEDOR"));
						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR2"));
						vendedor.setNombreVendedor(Validaciones.validarVacio(rs.getString("ORIGEN_VTA")) ? "KIOSKO": rs.getString("ORIGEN_VTA"));
						
						PagoEnTienda pet = new PagoEnTienda();
						pet.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("PT_CORRELATIVO_VENTA")));
						pet.setFechaHoraTrx(rs.getString("PT_FECHA_HORA_TRX"));
						pet.setFechaTrx(rs.getString("PT_FECHA_TRX"));
						pet.setSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_SUCURSAL")));
						pet.setNroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_NRO_CAJA")));
						pet.setNroTransaccion(Validaciones.validaLongCeroXNull(rs.getLong("PT_NRO_TRANSACCION")));
						pet.setNroDocumento(Validaciones.validaLongCeroXNull(rs.getLong("PT_NRO_DOCUMENTO")));
						pet.setTipoTrx(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_TIPO_TRX")));
						pet.setRecaudadorNombres(rs.getString("PT_RECAUDADOR_NOMBRES"));
						pet.setRecaudadorDni(rs.getString("PT_RECAUDADOR_DNI"));
						pet.setRecaudadorCodigo(rs.getString("PT_RECAUDADOR_CODIGO"));
						pet.setGlosa(rs.getString("PT_GLOSA"));
						pet.setMontoDestoRipley(rs.getBigDecimal("PT_MONTO_DESCUENTO_RIPLEY"));
						pet.setPlazo(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_PLAZO")));
						pet.setDiferido(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_DIFERIDO")));
						pet.setValorCuota(rs.getBigDecimal("PT_VALOR_CUOTA"));
						pet.setFechaPrimerVencio(rs.getString("PT_FECHA_PRIMER_VENCTO"));
						pet.setPan(Validaciones.validaLongCeroXNull(rs.getLong("PT_PAN")));
						pet.setTipoTarjeta(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_TIPO_TARJETA")));
						pet.setTipoPasarela(rs.getString("PT_TIPO_PASARELA"));
						pet.setBinNumber(rs.getString("PT_BIN_NUMBER"));
						pet.setUltimosDigidos(rs.getString("PT_ULTIMOS_DIGITOS"));
						pet.setIdTrxPago(rs.getString("PT_ID_TRANSACCION_PAGO"));
						pet.setCodigoAutorizacion(rs.getString("PT_CODIGO_AUTORIZACION"));
						pet.setTipoPago(Validaciones.validaIntegerCeroXNull(rs.getInt("PT_TIPO_PAGO")));

						List<ArticuloVentaTramaDTE> articuloVentas = obtenerArticuloVentas(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						
						List<IdentificadorMarketplace> identificadoresMarketplace = obtenerIdentificadorMarketplace(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));

						TramaDTE tramaDTE = new TramaDTE();
						tramaDTE.setNotaVenta(notaVenta);
						tramaDTE.setTarjetaRipley(tarjetaRipley);
						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
						tramaDTE.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
						tramaDTE.setDatoFacturacion(datoFacturacion);
						tramaDTE.setDespacho(despacho);
						tramaDTE.setTipoDoc(tipoDoc);
						tramaDTE.setVendedor(vendedor);
						tramaDTE.setPagoEnTienda(pet);
						tramaDTE.setArticuloVentas(articuloVentas);
						tramaDTE.setIdentificadoresMarketplace(identificadoresMarketplace);
						listado.add(tramaDTE);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerUnaOrdenParaGeneracion", "Error en obtenerUnaOrdenParaGeneracion: ", e);
				logger.endTrace("obtenerUnaOrdenParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerUnaOrdenParaGeneracion", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerUnaOrdenParaGeneracion", "Finalizado", "ConcurrentLinkedQueue<TramaDTE>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerUnaOrdenParaGeneracion", "Finalizado",
				"ConcurrentLinkedQueue<TramaDTE>.size: " + ((listado != null) ? listado.size() : 0));
		return listado;
	}

	public void envioTrxBo(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, Long nroTrxAnterior, boolean esMkp, String boleta,
			boolean isRipleyProcessed, boolean isMkpProcessed) throws AligareException {
		logger.initTrace("envioTrxBo", "TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp", 
				new KeyLog("inOrdenCompra", String.valueOf(inOrdenCompra)),
				new KeyLog("nroTransaccion", String.valueOf(nroTransaccion)),
				new KeyLog("esMkp", String.valueOf(esMkp)));
		
		
		PrepareBoCallBoleta source = new PrepareBoCallBoleta();
		source.setBoleta(boleta);
		source.setEsMkp(esMkp);
		source.setInOrdenCompra(inOrdenCompra);
		source.setNroTransaccion(nroTransaccion);
		source.setNroTrxAnterior(nroTrxAnterior);
		source.setParametros(parametros);
		source.setRipleyProcessed(isRipleyProcessed);
		source.setMkpProcessed(isMkpProcessed);
		
		RequestBO request = conversionService.convert(source, RequestBO.class);
		
		logger.traceInfo("envioTrxBo", "Objeto request generado", 
				new KeyLog("Objeto Request RequestBO", String.valueOf(request)));
		
		String url = parametros.buscaValorPorNombre("URL_BACKOFFICE");
		
		logger.traceInfo("envioTrxBo", "URL para servicio REST", 
				new KeyLog("URL", url));
		
		logger.traceInfo("envioTrxBo", "Invocando servicio REST");
		
		BackOfficeResponse resp=null;
		
		try {
			resp= rt.postForObject(url, request, BackOfficeResponse.class);
		}catch (Exception e) {
			logger.endTrace("envioTrxBo", "Servicio REST invocado de forma no exitosa", "resultado = No exitoso!");
			throw new AligareException("Error: Al llamar a BackOffice: "+ e.getMessage());
		}
		
		
		logger.traceInfo("envioTrxBo", "Respuesta servicio REST", 
				new KeyLog("Objeto respuesta BackOfficeResponse", String.valueOf(resp)));
                // 1 = la transacción fue registrada exitosamente en BO
		// 5 =  la transacción ya está registrada en BO. Si bien es un error, no se considera inhabilitante y se debe continuar coon el flujo.
		if(resp.getCodigo().intValue() == Constantes.NRO_CINCO) {
			TraceImpresionDTO ti =new TraceImpresionDTO();
			ti.setCorrelativoVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
			ti.setPpl(Constantes.NRO_UNO);
			ti.setBo(Constantes.NRO_UNO);
			try {
				ordenCompraDAO.upsertTraceImpresion(ti);
				PoolBDs.commitMESpringTrx();
			} catch (DataAccessException e) {
				logger.traceError("envioTrxBo", "Error al grabar traza BO; " + e.getMessage(), e);
			}
		}else if(resp.getCodigo().intValue() != Constantes.NRO_UNO) {
                    // <> 1 = Error al registrar al transacción en BO
			logger.traceInfo("envioTrxBo", "Resultado no exitoso", 
					new KeyLog("Codigo respuesta", String.valueOf(resp.getCodigo())),
					new KeyLog("Mensaje respuesta", String.valueOf(resp.getMensaje())));
			logger.endTrace("envioTrxBo", "Servicio REST invocado exitosamente con codigo de no éxito", "resultado = " + resp);
			throw new AligareException("Codigo Respuesta = " + String.valueOf(resp.getCodigo()) + "    ;   Mensaje Respuesta = " + String.valueOf(resp.getMensaje()));
		}
		
		logger.endTrace("envioTrxBo", "Servicio REST invocado exitosamente", "resultado = " + resp);
		
	
	}
	
	@Override
	public List<Long> obtenerNumerosOCsImpresion(Integer caja, Integer cantidadOCs) {
		logger.initTrace("obtenerNumerosOCsImpresion", "Integer caja: " + String.valueOf(caja) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs), 
				new KeyLog("Caja", String.valueOf(caja)), new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));

		List<Long> listado = new ArrayList<Long>(cantidadOCs);
		
		try (Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND)) {
			
			if (conn != null) {
				PreparedStatement ps = null;
				
				String sql = "SELECT CORRELATIVO_VENTA FROM NOTA_VENTA WHERE ESTADO = 1 AND NUMERO_CAJA = ? AND ROWNUM <= ? ORDER BY CORRELATIVO_VENTA ASC";
				
				logger.traceInfo("obtenerNumerosOCsImpresion", "SQL Ejecutado: " + sql + "  ------------ Parametros", 
						new KeyLog("NRO_CAJA", String.valueOf(caja)),
						new KeyLog("ROWNUM (cantidadOCs)", String.valueOf(cantidadOCs)));
				
				ps = conn.prepareStatement(sql);
				
				ps.setInt(1, caja);
				ps.setInt(2, cantidadOCs);
				
				ResultSet rs = ps.executeQuery();
				
				//Agrego los numeros de OC al listado
				while(rs.next()) {
					
					listado.add(rs.getLong(1));
					
				}

			}
			
		} catch (SQLException e) {
			logger.traceError("obtenerNumerosOCsImpresion", e);
			logger.endTrace("obtenerNumerosOCsByEstado", "Finalizado OK Lista Nula", null);
			return null;
		}

		
		logger.endTrace("obtenerNumerosOCsImpresion", "Finalizado OK", null);
		
		return listado;
	}

	@Override
	public List<Long> obtenerNumerosOCsByEstado(Integer caja, Integer cantidadOCs, Integer estado, Integer funcion) {
		logger.initTrace("obtenerNumerosOCsByEstado", 
				"Integer caja: " + String.valueOf(caja) 
				+ " - Integer cantidadOCs: " + String.valueOf(cantidadOCs)
				+ " - Integer estado: " + String.valueOf(estado), 
				new KeyLog("Caja", String.valueOf(caja)), 
				new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)),
				new KeyLog("Estado", String.valueOf(estado)));

		List<Long> listado = new ArrayList<Long>(cantidadOCs);
		
		try (Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND)) {
			
			if (conn != null) {
				PreparedStatement ps = null;
				
				StringBuilder sb = new StringBuilder();
				sb.append("SELECT CORRELATIVO_VENTA FROM NOTA_VENTA WHERE ESTADO = ? AND ")
				.append(Constantes.FUNCION_NC == funcion ? "NUM_CAJA_NOTA_CREDITO" : "NUMERO_CAJA")
				.append(" = ? AND ROWNUM <= ? ORDER BY CORRELATIVO_VENTA ASC");
				
				String sql = sb.toString();
				
				logger.traceInfo("obtenerNumerosOCsByEstado", "SQL Ejecutado: " + sql + "  ------------ Parametros", 
						new KeyLog("ESTADO", String.valueOf(estado)),
						new KeyLog("NRO_CAJA", String.valueOf(caja)),
						new KeyLog("ROWNUM (cantidadOCs)", String.valueOf(cantidadOCs)));
				
				ps = conn.prepareStatement(sql);
				
				ps.setInt(1, estado);
				ps.setInt(2, caja);
				ps.setInt(3, cantidadOCs);
				
				ResultSet rs = ps.executeQuery();
				
				//Agrego los numeros de OC al listado
				while(rs.next()) {
					
					listado.add(rs.getLong(1));
					
				}

			}
			
		} catch (SQLException e) {
			logger.traceError("obtenerNumerosOCsByEstado", e);
			logger.endTrace("obtenerNumerosOCsByEstado", "Finalizado OK Lista Nula", null);
			return listado;
		}

		
		logger.endTrace("obtenerNumerosOCsByEstado", "Finalizado OK", null);
		
		return listado;
	}
	
	
}