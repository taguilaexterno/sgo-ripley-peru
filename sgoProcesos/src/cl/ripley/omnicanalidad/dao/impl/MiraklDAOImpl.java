package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.EstadoMirakl;
import cl.ripley.omnicanalidad.dao.MiraklDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;

@Repository
public class MiraklDAOImpl implements MiraklDAO {
	private static final AriLog logger = new AriLog(MiraklDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Override
	public boolean insertarEstadoMKL(EstadoMirakl estadoMirakl) {
		logger.initTrace("insertarEstadoMKL", "estadoMirakl");
		boolean retorno = true;

		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		String procSQL;
		CallableStatement cs = null;
		if (conn!=null){
			try {
				procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL(?,?,?,?,TO_TIMESTAMP(?,'"+Constantes.FECHA_DDMMYYYY_HHMMSS+"'),?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				
				cs.setLong(1, estadoMirakl.getCorrelativoVenta());
				cs.setString(2, estadoMirakl.getApi());
				cs.setString(3, estadoMirakl.getOrdenMkp());
				cs.setInt(4, estadoMirakl.getEstado());
				cs.setString(5, estadoMirakl.getFechaEstado());
				cs.setString(6, estadoMirakl.getUsuarioEstado());
				
				if (estadoMirakl.getAmount() != null){
					cs.setBigDecimal(7, estadoMirakl.getAmount());
				} else{
					cs.setNull(7, OracleTypes.NUMBER);
				}
				if (estadoMirakl.getCustomerId() != null){
					cs.setString(8, estadoMirakl.getCustomerId());
				} else {
					cs.setNull(8, OracleTypes.VARCHAR);
				}
				if (estadoMirakl.getRefundId() != null){
					cs.setInt(9, estadoMirakl.getRefundId());
				} else {
					cs.setNull(9, OracleTypes.NUMBER);
				}
				
				cs.registerOutParameter(10, OracleTypes.NUMBER);
				cs.registerOutParameter(11, OracleTypes.VARCHAR);
				cs.registerOutParameter(12, OracleTypes.VARCHAR);
				
				cs.execute();
				
				if (cs.getInt(10) != 0){
					logger.traceInfo("insertarEstadoMKL", "error ejecucion CAVIRA_MANEJO_BOLETAS.PROC_INS_MIRAKL outCode: "+cs.getInt(10)+" outMsj: "+cs.getString(11));
					retorno = false;
				}
			} catch (Exception e) {
				retorno = false;
				logger.traceError("insertarEstadoMKL", "Error durante la ejecución de insertarEstadoMKL", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			retorno = false;
			logger.traceInfo("insertarEstadoMKL", "Problemas de conexion a la base de datos Modelo Extendido");
		}
		logger.endTrace("insertarEstadoMKL", "Finalizado", "retorno: "+retorno);
		
		return retorno;
	}

}
