package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.MontoNroTarje;
import cl.ripley.omnicanalidad.bean.RetornoNotaCredito;
import cl.ripley.omnicanalidad.bean.Saldos;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.NotaCreditoDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

@Repository
public class NotaCreditoDAOImpl implements NotaCreditoDAO {

	private static final AriLog logger = new AriLog(NotaCreditoDAOImpl.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	/**
	 * actualizaEfectivoCV funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 			fecha transaccion
	 * @param sucursal					sucursal nota de venta
	 * @param numeroCaja				numero caja
	 * @param numeroTransaccion			numero transaccion
	 * @param montoEfectivo 			monto efectivo
	 * @param porcentajeParticipacion 	porcentaje participacion
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */
	@Override
	public RetornoNotaCredito actualizaEfectivoCV(	Timestamp fechaTransaccion, 
													Integer sucursal, 
													Integer numeroCaja,
													Integer numeroTransaccion,
													long montoEfectivo, 
													Integer porcentajeParticipacion) {
		
		logger.initTrace("actualizaEfectivoCV", "(Timestamp fechaTransaccion: "+fechaTransaccion+"," 
												+"\nInteger sucursal: "+sucursal+"," 
												+"\nInteger numeroCaja: "+numeroCaja+","
												+"\nInteger numeroTransaccion: "+numeroTransaccion+","
												+"\nlong montoEfectivo: "+montoEfectivo+", "
												+"\nInteger porcentajeParticipacion: "+porcentajeParticipacion+")");
		
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;

		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		
		if(conn != null){
			try {
				String procSQL = "{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE(?,?),?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(2,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(3,sucursal);
				cs.setInt(4, numeroCaja);
				cs.setInt(5, numeroTransaccion);
				cs.setLong(6, montoEfectivo);
				cs.setInt(7, porcentajeParticipacion);			
				cs.registerOutParameter(8, OracleTypes.NUMBER);
				cs.registerOutParameter(9, OracleTypes.VARCHAR);
				
				logger.traceInfo("actualizaEfectivoCV", "{CALL ACTUALIZA_EFECTIVO_CV(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+montoEfectivo+","+porcentajeParticipacion+",out,out)}");
				
				cs.execute();    
				
				retornoNotaCredito.setCodigo(cs.getInt(8));
				retornoNotaCredito.setMensaje(cs.getString(9));
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error actualizaEfectivoCV: " +e);
				logger.traceError("actualizaEfectivoCV", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("actualizaEfectivoCV", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("actualizaEfectivoCV", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	/**
	 * insertTransbakCv funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param marcaTarjeta 			marca tarjeta
	 * @param tipoTarjeta 			tipo tarjeta
	 * @param rutCliente			rut cliente
	 * @param numeroTarjeta			numero tarjeta
	 * @param montoEfectivo			monto efectivo
	 * @param tipoCuota				tipo cuota
	 * @param numeroCuotas			numero cuotas
	 * @param fechaVence			fecha vence
	 * @param codigoAutorizador		codigo autorizador
	 * @param codigoUnico			codigo unico
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */
	@Override
	public RetornoNotaCredito insertTransbankCv(	Timestamp fechaTransaccion, 
												Integer sucursal, 
												Integer numeroCaja,
												Integer numeroTransaccion,
												String marcaTarjeta,
												String tipoTarjeta,
												Integer rutCliente,
												Integer numeroTarjeta,
												long montoEfectivo,
												Integer tipoCuota,
												Integer numeroCuotas,
												Integer fechaVence,
												String codigoAutorizador,
												String codigoUnico) {
		
		logger.initTrace("insertTransbakCv", "(Timestamp fechaTransaccion: "+fechaTransaccion+"," 
												+"\nInteger sucursal: "+sucursal+"," 
												+"\nInteger numeroCaja: "+numeroCaja+","
												+"\nInteger numeroTransaccion: "+numeroTransaccion+","
												+"\nInteger marcaTarjeta: "+marcaTarjeta+", "
												+"\nInteger tipoTarjeta: "+tipoTarjeta+", "
												+"\nInteger rutCliente: "+rutCliente+", "
												+"\nInteger numeroTarjeta: "+numeroTarjeta+", "
												+"\nlong montoEfectivo: "+montoEfectivo+", "
												+"\nInteger tipoCuota: "+tipoCuota+", "
												+"\nInteger numeroCuotas: "+numeroCuotas+", "
												+"\nInteger fechaVence: "+fechaVence+", "
												+"\nString codigoAutorizador: "+codigoAutorizador+", "
												+"\nString codigoUnico: "+codigoUnico+")");

		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;
		
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		if(conn != null){
			try {
			
				String procSQL = "{CALL inserta_transbank_CV(TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(2,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(3, sucursal);
				cs.setInt(4, numeroCaja);
				cs.setInt(5, numeroTransaccion);
				cs.setString(6, marcaTarjeta);
				cs.setString(7, tipoTarjeta);
				cs.setInt(8, rutCliente);
				cs.setInt(9, numeroTarjeta);
				cs.setLong(10, montoEfectivo);
				cs.setInt(11, tipoCuota);
				cs.setInt(12, numeroCuotas);
				cs.setInt(13, fechaVence);
				cs.setString(14, codigoAutorizador);
				cs.setString(15, codigoUnico);
				cs.registerOutParameter(16, OracleTypes.NUMBER);
				cs.registerOutParameter(17, OracleTypes.VARCHAR);
		
				logger.traceInfo("insertTransbakCv", "{CALL inserta_transbank_CV(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+",'"+marcaTarjeta+"','"+tipoTarjeta+"',"+rutCliente+","+numeroTarjeta+","+montoEfectivo+","+tipoCuota+","+numeroCuotas+","+fechaVence+",'"+codigoAutorizador+"','"+codigoUnico+"',out,out)}");
				
				cs.execute();    
				
				retornoNotaCredito.setCodigo(cs.getInt(16));
				retornoNotaCredito.setMensaje(cs.getString(17));
			    
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error insertTransbakCv: " +e);
				logger.traceError("insertTransbakCv", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertTransbakCv", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("insertTransbakCv", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	/**
	 * actualizaTarRegaloEmpresa funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param administradora 		administradora
	 * @param emisor 				emisor
	 * @param tarjeta				tarjeta
	 * @param codigoAutorizador		codigo autorizador
	 * @param rutCliente			rut cliente
	 * @param montoEfectivo			monto efectivo
	 * @param numeroTarjeta			numero tarjeta
	 * @param flag					flag
	 * @param codigoAutorizador		codigo autorizador
	 * @param codigoUnico			codigo unico
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */
	@Override
	public RetornoNotaCredito actualizaTarRegaloEmpresa(	Timestamp fechaTransaccion, 
															Integer sucursal, 
															Integer numeroCaja,
															Integer numeroTransaccion,
															Integer administradora,
															Integer emisor,
															Integer tarjeta,
															Integer codigoAutorizador,
															long rutCliente,
															long montoEfectivo,
															Integer numeroTarjeta,
															Integer flag) {

		logger.initTrace("actualizaTarRegaloEmpresa", "(Timestamp fechaTransaccion: "+fechaTransaccion+"," 
													+ "\nInteger sucursal: "+sucursal+"," 
													+ "\nInteger numeroCaja: "+numeroCaja+","
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+","
													+ "\nInteger administradora: "+administradora+", "
													+ "\nInteger emisor: "+emisor+", "
													+ "\nInteger tarjeta: "+tarjeta+", "
													+ "\nInteger codigoAutorizador: "+codigoAutorizador+", "
													+ "\nlong rutCliente: "+rutCliente+", "
													+ "\nlong montoEfectivo: "+montoEfectivo+", "
													+ "\nInteger numeroTarjeta: "+numeroTarjeta+", "
													+ "\nInteger flag: "+flag+")");

		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;

		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		if(conn != null){
			try {
				
				String procSQL = "{CALL actualiza_tar_regalo_empresa(TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(2,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(3,sucursal);
				cs.setInt(4, numeroCaja);
				cs.setInt(5, numeroTransaccion);
				cs.setInt(6, administradora);
				cs.setInt(7, emisor);
				cs.setInt(8, tarjeta);
				cs.setInt(9, codigoAutorizador);
				cs.setLong(10, rutCliente);
				cs.setLong(11, montoEfectivo);
				cs.setInt(12, numeroTarjeta);
				cs.setInt(13, flag);
				cs.registerOutParameter(14, OracleTypes.NUMBER);
				cs.registerOutParameter(15, OracleTypes.VARCHAR);
		
				logger.traceInfo("actualizaTarRegaloEmpresa", "{CALL actualiza_tar_regalo_empresa(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+administradora+","+emisor+","+tarjeta+","+codigoAutorizador+","+rutCliente+","+montoEfectivo+","+numeroTarjeta+","+flag+",out,out)}");
				
				cs.execute();    
		
				retornoNotaCredito.setCodigo(cs.getInt(14));
				retornoNotaCredito.setMensaje(cs.getString(15));
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error actualizaTarRegaloEmpresa: " +e);
				logger.traceError("actualizaTarRegaloEmpresa", e);;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("actualizaTarRegaloEmpresa", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("actualizaTarRegaloEmpresa", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	/**
	 * insertaBancariaBack funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param tipoTarjeta 			tipo tarjeta
	 * @param monto 				monto
	 * @param correlativoVenta		correlativo venta
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */	
	@Override
	public RetornoNotaCredito insertaBancariaBack(	Timestamp fechaTransaccion, 
													Integer sucursal, 
													Integer numeroCaja,
													Integer numeroTransaccion, 
													Integer tipoTarjeta, 
													long monto, 
													String correlativoVenta) {

		logger.initTrace("insertaBancariaBack", "Timestamp fechaTransaccion: "+ fechaTransaccion +"," 
				+ "Integer sucursal: "+ sucursal +"," 
				+ "Integer numeroCaja: "+ numeroCaja +","
				+ "Integer numeroTransaccion: "+ numeroTransaccion +","
				+ "Integer tipoTarjeta: "+ tipoTarjeta +", "
				+ "long monto: "+ monto +", "
				+ "String correlativoVenta: "+ correlativoVenta);

		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;
		
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		if(conn != null){
			try {
				
				String procSQL = "{CALL ? := inserta_bancaria_back(TO_DATE(?,?),?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(3,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, tipoTarjeta);
				cs.setLong(8, monto);
				cs.setString(9, correlativoVenta);
				
				logger.traceInfo("insertaBancariaBack", "{CALL ? := inserta_bancaria_back(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+tipoTarjeta+","+monto+",'"+correlativoVenta+"')}");
				
				cs.executeUpdate();    
				
				retornoNotaCredito.setCodigo(cs.getInt(1));
				retornoNotaCredito.setMensaje("");
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error insertaBancariaBack: " +e);
				logger.traceError("insertaBancariaBack", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertaBancariaBack", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("insertaBancariaBack", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());

		return retornoNotaCredito;
	}

	/**
	 * insertaRipleyBack funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param administradora 		administradora
	 * @param emisor 				emisor
	 * @param tarjeta				tarjeta
	 * @param rutTitular			rut titular
	 * @param rutPoder				rut poder
	 * @param plazo					plazo
	 * @param diferido				diferido
	 * @param montoTarjetaRipley 	monto tarjeta ripley
	 * @param montoPie				monto pie
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */		
	@Override
	public RetornoNotaCredito insertaRipleyBack(Timestamp fechaTransaccion, 
												Integer sucursal, 
												Integer numeroCaja,
												Integer numeroTransaccion, 
												Integer administradora, 
												Integer emisor, 
												Integer tarjeta, 
												Integer rutTitular,
												Integer rutPoder, 
												Integer plazo, 
												Integer diferido, 
												long montoTarjetaRipley, 
												Integer montoPie,
												Integer descuentoCar,
												Integer codigoDescuento,
												BigDecimal codigoAutorizador,
												Integer porcentajePar,
												BigDecimal numeroPan) {

		logger.initTrace("insertaRipleyBack", "(Timestamp fechaTransaccion: "+fechaTransaccion+"," 
											+ "\nInteger sucursal: "+sucursal+"," 
											+ "\nInteger numeroCaja: "+numeroCaja+","
											+ "\nInteger numeroTransaccion: "+numeroTransaccion+","
											+ "\nInteger administradora: "+administradora+", "
											+ "\nInteger emisor: "+emisor+", "
											+ "\nInteger tarjeta: "+tarjeta+","
											+ "\nInteger rutTitular: "+rutTitular+", "
											+ "\nInteger rutPoder: "+rutPoder+", "
											+ "\nInteger plazo: "+plazo+", "
											+ "\nInteger diferido: "+diferido+", "
											+ "\nlong montoTarjetaRipley: "+montoTarjetaRipley+", "
											+ "\nInteger montoPie: "+montoPie+","
											+ "\nInteger descuentoCar: "+descuentoCar+", "
											+ "\nInteger codigoDescuento: "+codigoDescuento+", "
											+ "\nInteger codigoAutorizador: "+codigoAutorizador+", "
											+ "\nInteger porcentajePar: "+porcentajePar+", "
											+ "\nInteger numeroPan : "+numeroPan+"");

		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;
		
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		if(conn != null){
			try {
			
				String procSQL = "{CALL ? := INSERTA_RIPLEY_BACK(TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(3,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, administradora);
				cs.setInt(8, emisor);
				cs.setInt(9, tarjeta);
				cs.setInt(10, rutTitular);
				cs.setInt(11, rutPoder);
				cs.setInt(12, plazo);
				cs.setInt(13, diferido);
				cs.setLong(14, montoTarjetaRipley);
				cs.setInt(15, montoPie);
				cs.setInt(16, descuentoCar);
				cs.setInt(17, codigoDescuento);
				cs.setBigDecimal(18, codigoAutorizador);
				cs.setInt(19, porcentajePar);
				cs.setBigDecimal(20, numeroPan);

				logger.traceInfo("insertaRipleyBack", "{CALL ? := INSERTA_RIPLEY_BACK(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+administradora+","+emisor+","+tarjeta+","+rutTitular+","+rutPoder+","+plazo+","+diferido+","+montoTarjetaRipley+","+montoPie+","+descuentoCar+","+codigoDescuento+","+codigoAutorizador+","+porcentajePar+","+numeroPan+")}");
				
				cs.executeUpdate();    
				
				retornoNotaCredito.setCodigo(cs.getInt(1));
				retornoNotaCredito.setMensaje("");
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error insertaRipleyBack: " +e);
				logger.traceError("insertaRipleyBack", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertaRipleyBack", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("insertaRipleyBack", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	/**
	 * insertaTransaccionBack4 funcion para ejecutar procedimiento en BACKOFFICE 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param numeroDocumento		numero documento
	 * @param tipoTransaccion		tipo transaccion
	 * @param monto					monto 
	 * @param sucursalOriginal		sucursal original
	 * @param fechaBoleta			fecha boleta 
	 * @param numeroCajaOriginal 	numero caja original
	 * @param numeroBoleta 			numero boleta
	 * @param numeroDoctoOriginal 	numero documento original
	 * @param supervisor			supervisor
	 * @param vendedor				vendedor
	 * @param origenTransaccion 	origen transaccion
	 * @param rutComprador			rut comprador 
	 * @param montoDescuento		monto descuento 
	 * @param descuentoCar 			descuentoCar
	 * @param montoPie				monto pie
	 * @param fechaHoraGl 			fechaHoraGl
	 * @param estado 				estado
	 * @param tipoOrigen 			tipoOrigen	
	 * @param correlativoVenta 		correlativoVenta
	 * @param nroTranOriginal		nroTranOriginal
	 * @param fechaCreacionNotaVenta fechaCreacionNotaVenta
	 * @param folioSII				folioSII
	 * @param codigoBO 				codigoBO
	 * @param servicioNCA			servicioNCA
	 * @param detalle				detalle
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */		
	@Override
	public RetornoNotaCredito insertaTransaccionBack4(	Timestamp fechaTransaccion, 
														Integer sucursal, 
														Integer numeroCaja,
														Integer numeroTransaccion, 
														Integer numeroDocumento, 
														Integer tipoTransaccion, 
														long monto, 
														Integer sucursalOriginal,
														Timestamp fechaBoleta, 
														Integer numeroCajaOriginal, 
														Integer numeroBoleta, 
														Integer numeroDoctoOriginal, 
														Integer supervisor,
														Integer vendedor, 
														Integer origenTransaccion, 
														Integer rutComprador, 
														Integer montoDescuento, 
														Integer descuentoCar, 
														Integer montoPie,
														Timestamp fechaHoraGl, 
														Integer estado, 
														Integer tipoOrigen, 
														Long correlativoVenta, 
														Integer nroTranOriginal,
														Timestamp fechaCreacionNotaVenta, 
														Integer folioSII, 
														Integer codigoBO, 
														String servicioNCA,
														String detalle) {
		logger.initTrace("insertaTransaccionBack4", "(Timestamp fechaTransaccion: "+fechaTransaccion+"," 
													+ "\nInteger sucursal: "+sucursal+"," 
													+ "\nInteger numeroCaja: "+numeroCaja+","
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+","
													+ "\nInteger numeroDocumento: "+numeroDocumento+"," 
													+ "\nInteger tipoTransaccion: "+tipoTransaccion+", "
													+ "\nlong monto: "+monto+", "
													+ "\nInteger sucursalOriginal: "+sucursalOriginal+","
													+ "\nTimestamp fechaBoleta: "+fechaBoleta+", "
													+ "\nInteger numeroCajaOriginal: "+numeroCajaOriginal+"," 
													+ "\nInteger numeroBoleta: "+numeroBoleta+", "
													+ "\nInteger numeroDoctoOriginal: "+numeroDoctoOriginal+"," 
													+ "\nInteger supervisor: "+supervisor+","
													+ "\nInteger vendedor: "+vendedor+", "
													+ "\nInteger origenTransaccion: "+origenTransaccion+"," 
													+ "\nInteger rutComprador: "+rutComprador+", "
													+ "\nInteger montoDescuento: "+montoDescuento+"," 
													+ "\nInteger descuentoCar: "+descuentoCar+", "
													+ "\nInteger montoPie: "+montoPie+","
													+ "\nTimestamp fechaHoraGl: "+fechaHoraGl+", "
													+ "\nInteger estado: "+estado+", "
													+ "\nInteger tipoOrigen: "+tipoOrigen+"," 
													+ "\nInteger correlativoVenta: "+correlativoVenta+"," 
													+ "\nInteger nroTranOriginal: "+nroTranOriginal+","
													+ "\nTimestamp fechaCreacionNotaVenta: "+fechaCreacionNotaVenta+"," 
													+ "\nInteger folioSII: "+folioSII+", "
													+ "\nInteger codigoBO: "+codigoBO+", "
													+ "\nString servicioNCA: "+servicioNCA+","
													+ "\nString detalle: "+detalle+"");									

		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		CallableStatement cs;
		
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		
		if(conn != null){
			try {
			
				String procSQL = "{CALL ? := INSERTA_TRANSACCION_BACK4(TO_DATE(?,?),?,?,?,?,?,?,?,TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,TO_DATE(?,?),?,?,?,?,TO_DATE(?,?),?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion));
				cs.setString(3,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, numeroDocumento);
				cs.setInt(8, tipoTransaccion);
				cs.setLong(9, monto);
				cs.setInt(10, sucursalOriginal);
				cs.setString(11,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaBoleta));
				cs.setString(12,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(13, numeroCajaOriginal);
				cs.setInt(14, numeroBoleta);
				cs.setInt(15, numeroDoctoOriginal);
				cs.setInt(16, supervisor);
				cs.setInt(17, vendedor);
				cs.setInt(18, origenTransaccion);
				cs.setInt(19, rutComprador);
				cs.setInt(20, montoDescuento);
				cs.setInt(21, descuentoCar);
				cs.setInt(22, montoPie);
				cs.setString(23,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaHoraGl));
				cs.setString(24,  Constantes.FECHA_DDMMYYYY_HHMMSS2);
				cs.setInt(25, estado);
				cs.setInt(26, tipoOrigen);
				cs.setLong(27, correlativoVenta);
				cs.setInt(28, nroTranOriginal);
				cs.setString(29,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNotaVenta));
				cs.setString(30,  Constantes.FECHA_DDMMYYYY_HHMMSS2);
				cs.setInt(31, folioSII);
				cs.setInt(32, codigoBO);
				cs.setString(33, (servicioNCA.length()<252)?servicioNCA:servicioNCA.substring(0, 250));
				cs.setString(34, detalle);
				
				logger.traceInfo("insertaTransaccionBack4", "{CALL ? := "
						+ "INSERTA_TRANSACCION_BACK4(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaTransaccion)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+numeroDocumento+","+tipoTransaccion+","+monto+","+sucursalOriginal+","
						+ "TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaBoleta)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+numeroCajaOriginal+","+numeroBoleta+","+numeroDoctoOriginal+","+supervisor+","+vendedor+","+origenTransaccion+","+rutComprador+","+montoDescuento+","+descuentoCar+","+montoPie+","
						+ "TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaHoraGl)+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"+estado+","+tipoOrigen+","+correlativoVenta+","+nroTranOriginal+","
						+ "TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaCreacionNotaVenta)+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"+folioSII+","+codigoBO+",'"+servicioNCA+"','"+detalle+"')}");
				
				cs.executeUpdate();
				
				retornoNotaCredito.setCodigo(cs.getInt(1));
				retornoNotaCredito.setMensaje("");
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error insertaTransaccionBack4: " +e);
				logger.traceError("insertaTransaccionBack4", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertaTransaccionBack4", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		
		logger.endTrace("insertaTransaccionBack4", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
		
	}
	
	/**
	 * fNotaCredito funcion para ejecutar procedimiento en BIG_TICKET 
	 * durante generacion nota de credito
	 * 
	 * @param fechaTransaccion 		fecha transaccion
	 * @param sucursal				sucursal nota de venta
	 * @param numeroCaja			numero caja
	 * @param numeroTransaccion		numero transaccion
	 * @param numeroDocumento		numero documento
	 * @param tipoTransaccion		tipo transaccion
	 * @param monto					monto 
	 * @param sucursalOriginal		sucursal original
	 * @param fechaBoleta			fecha boleta 
	 * @param numeroCajaOriginal 	numero caja original
	 * @param numeroBoleta 			numero boleta
	 * @param numeroDoctoOriginal 	numero documento original
	 * @param supervisor			supervisor
	 * @param vendedor				vendedor
	 * @param origenTransaccion 	origen transaccion
	 * @param rutComprador			rut comprador 
	 * @param montoDescuento		monto descuento 
	 * @param descuentoCar 			descuentoCar
	 * @param montoPie				monto pie
	 * @param fechaHoraGl 			fechaHoraGl
	 * @param estado 				estado
	 * @param tipoOrigen 			tipoOrigen	
	 * @param correlativoVenta 		correlativoVenta
	 * @param nroTranOriginal		nroTranOriginal
	 * @param fechaCreacionNotaVenta fechaCreacionNotaVenta
	 * @param folioSII				folioSII
	 * @param codigoBO 				codigoBO
	 * @param servicioNCA			servicioNCA
	 * @param detalle				detalle
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */		
	@Override
	public RetornoNotaCredito fNotaCredito(	String codigoDespacho, 
											String mot, 
											Integer sucursal, 
											Integer numeroCaja,
											Integer transaccion, 
											String vendedor, 
											Timestamp fechaNotaCredito,
											Integer origen) {

		logger.initTrace("fNotaCredito",  "(String codigoDespacho: "+codigoDespacho+"," 
										+ "\nString mot: "+mot+"," 
										+ "\nInteger sucursal: "+sucursal+","
										+ "\nInteger numeroCaja: "+numeroCaja+","
										+ "\nInteger transaccion: "+transaccion+", "
										+ "\nInteger vendedor: "+vendedor+", "
										+ "\nTimestamp fechaNotaCredito: "+fechaNotaCredito+","
										+ "\nInteger origen: "+origen+")");

		Connection conn = PoolBDs.getConnection(BDType.BIG_TICKET);
		CallableStatement cs;
		
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		
		if(conn != null){
			try {
				
				String procSQL = "{CALL ? := f_nota_credito(?,?,?,?,?,?,TO_DATE(?,?),?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2, codigoDespacho);
				cs.setString(3, mot);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, transaccion);
				cs.setString(7, vendedor);
				cs.setString(8,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaNotaCredito));
				cs.setString(9,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(10, origen);

				logger.traceInfo("fNotaCredito", "{CALL ? := f_nota_credito('"+codigoDespacho+"','"+mot+"',"+sucursal+","+numeroCaja+","+transaccion+","+vendedor+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaNotaCredito)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+origen+")}");

				cs.executeUpdate();    
			
				retornoNotaCredito.setCodigo((cs.getInt(1)==Constantes.NRO_UNO)?new Integer(Constantes.EJEC_SIN_ERRORES):cs.getInt(1));
				retornoNotaCredito.setMensaje("");
			
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error fNotaCredito: " +e);
				logger.traceError("fNotaCredito", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("fNotaCredito", "Problemas de conexion a la base de datos Big Ticket");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("fNotaCredito", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	/**
	 * lglreg_prc_mov_lis_vlr procedimiento para ejecutar procedimiento en CLUB 
	 * durante generacion nota de credito
	 * 
	 * @param codigoEvento 			codigo evento
	 * @param codigoProducto		codigo producto
	 * @param codSucursal			codigo sucursal
	 * @param fechaCompra			fecha compra
	 * @param numeroTransaccion		numero transaccion
	 * @param codigoTransaccion		codigo transaccion
	 * @param codEntraSale			codEntraSale 
	 * @param tipoDocumento			tipo documento
	 * @param cantidadProducto		cantidad producto
	 * @param precioCompra 			precioCompra
	 * @param numeroBol 			numero boleta
	 * @param numeroCaja 			numero caja
	 * @param formaPago				forma de pago
	 * @param secuencial 			secuencial
	 * @param dirDespacho 			dirDespacho
	 * @param marcaRegalo			marca regalo
	 * @param fechaDespacho 		fecha despacho
	 * @param email					email
	 * @param nombreInvitado		nombre invitado
	 * @param apellidoPaternoInvit	apellido paterno invitado
	 * @param apellidoMaternoInvit	apellido materno invitado 
	 * @param direcInvitado, 		direccion invitado
	 * @param telefonoInvitado 		telefono invitado	
	 * @param mensajeInvitado		mensaje invitado
	 * @param fchOrigen 			fecha origen
	 * @param boletaOrigen 			boleta origen
	 * @param sucursalOrigen		sucursal origen
	 * @param cjaOrigen 			caja origen
	 * 
	 * @return RetornoNotaCredito objeto con codigo y mensaje de operacion 
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */	
	@Override
	public RetornoNotaCredito lglreg_prc_mov_lis_vlr(	String codigoEvento, 
														long codigoProducto, 
														Integer codSucursal,
														Timestamp fechaCompra, 
														Integer numeroTransaccion, 
														Integer codigoTransaccion, 
														Integer codEntraSale,
														Integer tipoDocumento, 
														Integer cantidadProducto, 
														long precioCompra, 
														Integer numeroBol,
														Integer numeroCaja, 
														Integer formaPago, 
														Integer secuencial, 
														String dirDespacho, 
														String marcaRegalo,
														Integer fechaDespacho, 
														String email, 
														String nombreInvitado, 
														String apellidoPaternoInvit,
														String apellidoMaternoInvit, 
														String direcInvitado, 
														Integer telefonoInvitado, 
														String mensajeInvitado,
														Integer fchOrigen, 
														Integer boletaOrigen, 
														Integer sucursalOrigen,
														Integer cjaOrigen) {

		logger.initTrace("lglreg_prc_mov_lis_vlr", 	 "(String codigoEvento: "+codigoEvento+"," 
													+ "\nlong codigoProducto: "+codigoProducto+"," 
													+ "\nInteger codSucursal: "+codSucursal+","
													+ "\nTimestamp fechaCompra: "+fechaCompra+", "
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+"," 
													+ "\nInteger codigoTransaccion: "+codigoTransaccion+", "
													+ "\nInteger codEntraSale: "+codEntraSale+","
													+ "\nInteger tipoDocumento: "+tipoDocumento+"," 
													+ "\nInteger cantidadProducto: "+cantidadProducto+"," 
													+ "\nlong precioCompra: "+precioCompra+", "
													+ "\nInteger numeroBol: "+numeroBol+","
													+ "\nInteger numeroCaja: "+numeroCaja+"," 
													+ "\nInteger formaPago: "+formaPago+", "
													+ "\nInteger secuencial: "+secuencial+", "
													+ "\nString dirDespacho: "+dirDespacho+", "
													+ "\nString marcaRegalo: "+marcaRegalo+","
													+ "\nInteger fechaDespacho: "+fechaDespacho+"," 
													+ "\nString email: "+email+", "
													+ "\nString nombreInvitado: "+nombreInvitado+"," 
													+ "\nString apellidoPaternoInvit: "+apellidoPaternoInvit+","
													+ "\nString apellidoMaternoInvit: "+apellidoMaternoInvit+", "
													+ "\nString direcInvitado: "+direcInvitado+"," 
													+ "\nInteger telefonoInvitado: "+telefonoInvitado+"," 
													+ "\nString mensajeInvitado: "+mensajeInvitado+","
													+ "\nInteger fchOrigen: "+fchOrigen+"," 
													+ "\nInteger boletaOrigen: "+boletaOrigen+"," 
													+ "\nInteger sucursalOrigen: "+sucursalOrigen+"," 
													+ "\nInteger cjaOrigen: "+cjaOrigen+")");
		Connection conn = PoolBDs.getConnection(BDType.CLUB);
		CallableStatement cs;
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		
		if(conn != null){
			try {
				
				String procSQL = "{CALL LGLREG_PRC_MOV_LIS_VLR(?,?,?,TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, codigoEvento);
				cs.setLong(2, codigoProducto);
				cs.setInt(3, (codSucursal.intValue() + 10000));
				cs.setString(4,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra));
				cs.setString(5,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, codigoTransaccion);
				cs.setInt(8, codEntraSale);
				cs.setInt(9, tipoDocumento);
				cs.setInt(10,cantidadProducto);
				cs.setLong(11, precioCompra);
				cs.setInt(12, numeroBol);
				cs.setInt(13, numeroCaja);
				cs.setInt(14, formaPago);
				cs.setInt(15, secuencial);
				cs.setString(16, dirDespacho);
				cs.setString(17, marcaRegalo);
				if(!Validaciones.validaInteger(fechaDespacho)){
					cs.setNull(18, OracleTypes.NUMBER);
				}else{
					cs.setInt(18, fechaDespacho);
				}
				cs.setString(19, email);
				cs.setString(20, nombreInvitado);
				cs.setString(21, apellidoPaternoInvit);
				cs.setString(22, apellidoMaternoInvit);
				cs.setString(23, direcInvitado);
				cs.setInt(24, telefonoInvitado);
				cs.setString(25, mensajeInvitado);
				cs.setInt(26, fchOrigen);
				cs.setInt(27, boletaOrigen);
				cs.setInt(28, sucursalOrigen);
				cs.setInt(29, cjaOrigen);
				cs.registerOutParameter(30, OracleTypes.NUMBER);
				cs.registerOutParameter(31, OracleTypes.VARCHAR);
			
				logger.traceInfo("lglreg_prc_mov_lis_vlr", "{CALL LGLREG_PRC_MOV_LIS_VLR('"+codigoEvento+"',"+codigoProducto+","+(codSucursal.intValue() + 10000)+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaCompra)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+numeroTransaccion+","+codigoTransaccion+","+codEntraSale+","+tipoDocumento+","+cantidadProducto+","+precioCompra+","+numeroBol+","+numeroCaja+","+formaPago+","+secuencial+",'"+dirDespacho+"','"+marcaRegalo+"',"+fechaDespacho+",'"+email+"','"+nombreInvitado+"','"+apellidoPaternoInvit+"','"+apellidoMaternoInvit+"','"+direcInvitado+"',"+telefonoInvitado+",'"+mensajeInvitado+"',"+fchOrigen+","+boletaOrigen+","+sucursalOrigen+","+cjaOrigen+",out,out)}");
				
				cs.execute();    
				
				retornoNotaCredito.setCodigo(cs.getInt(30));
				retornoNotaCredito.setMensaje(cs.getString(31));
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error lglreg_prc_mov_lis_vlr: " +e);
				logger.traceError("lglreg_prc_mov_lis_vlr", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("lglreg_prc_mov_lis_vlr", "Problemas de conexion a la base de datos Club");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("lglreg_prc_mov_lis_vlr", "Finalizado", "RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	@Override
	public RetornoNotaCredito insertaDespachoBack(	Timestamp fecha, 
													Integer sucursal, 
													Integer numeroCaja, 
													Integer numeroTransaccion,
													String codigoUnidadDespacho, 
													Integer color,
													Integer cubicaje, 
													long rutDespacho, 
													String nombreDespacho,
													Timestamp fechaDespacho, 
													Integer sucursalDespacho, 
													Integer comunaDespacho, 
													Integer regionDespacho, 
													String direccionDespacho, 
													String telefonoDespacho, 
													Integer sectorDespacho,
													String jornadaDespacho, 
													String observacion, 
													Integer paginaCtc, 
													String cordCtc, 
													long rutCliente,
													String direccionCliente, 
													String telefonoCliente, 
													Integer tipoCliente, 
													String nombreCliente) {
		
		logger.initTrace("insertaDespachoBack",  	"(Timestamp fecha: "+fecha+"," 
													+ "\nInteger sucursal: "+sucursal+"," 
													+ "\nInteger numeroCaja: "+numeroCaja+","
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+","
													+ "\nString codigoUnidadDespacho: "+codigoUnidadDespacho+"," 
													+ "\nInteger color: "+color+","
													+ "\nInteger cubicaje: "+cubicaje+"," 
													+ "\nlong rutDespacho: "+rutDespacho+"," 
													+ "\nString nombreDespacho: "+nombreDespacho+","
													+ "\nTimestamp fechaDespacho: "+fechaDespacho+"," 
													+ "\nInteger sucursalDespacho: "+sucursalDespacho+"," 
													+ "\nInteger comunaDespacho: "+comunaDespacho+"," 
													+ "\nInteger regionDespacho: "+regionDespacho+","
													+ "\nString direccionDespacho: "+direccionDespacho+"," 
													+ "\nString telefonoDespacho: "+telefonoDespacho+"," 
													+ "\nInteger sectorDespacho: "+sectorDespacho+","
													+ "\nString jornadaDespacho: "+jornadaDespacho+"," 
													+ "\nString observacion: "+observacion+"," 
													+ "\nInteger paginaCtc: "+paginaCtc+"," 
													+ "\nString cordCtc: "+cordCtc+"," 
													+ "\nlong rutCliente: "+rutCliente+","
													+ "\nString direccionCliente: "+direccionCliente+"," 
													+ "\nString telefonoCliente: "+telefonoCliente+"," 
													+ "\nInteger tipoCliente: "+tipoCliente+"," 
													+ "\nString nombreCliente: "+nombreCliente+")");


		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		String procSQL;
		CallableStatement cs = null;
		if (conn!=null){
			try {
				procSQL =  "{CALL ? := inserta_despacho_back(TO_DATE(?,?),?,?,?,?,?,?,?,?,TO_DATE(?,?),?,?,0,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha));
				cs.setString(3,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, numeroTransaccion);
				cs.setString(7, codigoUnidadDespacho);
				cs.setInt(8, color);
				cs.setInt(9, cubicaje);
				cs.setLong(10, rutDespacho);
				cs.setString(11, nombreDespacho);
				cs.setString(12,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaDespacho));
				cs.setString(13,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(14, sucursalDespacho);
				cs.setInt(15, comunaDespacho);
				cs.setInt(16, regionDespacho);
				cs.setString(17, direccionDespacho);
				cs.setString(18, telefonoDespacho);
				cs.setInt(19, sectorDespacho);
				cs.setString(20, jornadaDespacho);
				cs.setString(21, observacion);
				cs.setInt(22, paginaCtc);
				cs.setString(23, cordCtc);
				cs.setLong(24, rutCliente);
				cs.setString(25, direccionCliente);
				cs.setString(26, telefonoCliente);
				cs.setInt(27, tipoCliente);
				cs.setString(28, nombreCliente);
				
				logger.traceInfo("insertaDespachoBack", "{CALL ? := inserta_despacho_back(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+",'"+codigoUnidadDespacho+"',"+color+","+cubicaje+","+rutDespacho+",'"+nombreDespacho+"',TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaDespacho)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursalDespacho+","+comunaDespacho+",0,"+regionDespacho+",'"+direccionDespacho+"','"+telefonoDespacho+"',"+sectorDespacho+",'"+jornadaDespacho+"','"+observacion+"',"+paginaCtc+",'"+cordCtc+"',"+rutCliente+",'"+direccionCliente+"','"+telefonoCliente+"',"+tipoCliente+",'"+nombreCliente+"')}");
				
				cs.executeUpdate();
				
				retornoNotaCredito.setCodigo(cs.getInt(1));
				retornoNotaCredito.setMensaje("");
				
				cs.close();
			} catch (Exception e) {
				logger.traceError("insertaDespachoBack", e);
				retornoNotaCredito.setMensaje("Mensaje de error insertaDespachoBack: "+e.getMessage());
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertaDespachoBack", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("insertaDespachoBack", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
		
		return retornoNotaCredito;
	}

	@Override
	public RetornoNotaCredito insertaArticulosBack(	Timestamp fecha, 
													Integer sucursal, 
													Integer numeroCaja, 
													Integer numeroTransaccion,
													Integer numeroItem, 
													long codigoArticulo, 
													Integer vendedor, 
													Integer codigoNovios, 
													Integer saleArticulo,
													Integer precioArticulo, 
													Integer unidades, 
													Integer descuento, 
													Integer tipoDescuento,
													String codUnidadDespacho, 
													Integer porcentajeParticip, 
													Integer despachoDomicilio) {

		logger.initTrace("insertaArticulosBack",  	"(Timestamp fecha: "+fecha+"," 
													+ "\nInteger sucursal: "+sucursal+"," 
													+ "\nInteger numeroCaja: "+numeroCaja+"," 
													+ "\nInteger numeroTransaccion: "+numeroTransaccion+","
													+ "\nInteger numeroItem: "+numeroItem+"," 
													+ "\nlong codigoArticulo: "+codigoArticulo+"," 
													+ "\nInteger vendedor: "+vendedor+"," 
													+ "\nInteger codigoNovios: "+codigoNovios+"," 
													+ "\nInteger saleArticulo: "+saleArticulo+","
													+ "\nInteger precioArticulo: "+precioArticulo+"," 
													+ "\nInteger unidades: "+unidades+"," 
													+ "\nInteger descuento: "+descuento+"," 
													+ "\nInteger tipoDescuento: "+tipoDescuento+","
													+ "\nString codUnidadDespacho: "+codUnidadDespacho+","
													+ "\nInteger porcentajeParticip: "+porcentajeParticip+"," 
													+ "\nInteger despachoDomicilio: "+despachoDomicilio+")");
		
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		String procSQL;
		CallableStatement cs = null;
		if (conn!=null){
			try {
				procSQL =  "{CALL ? := inserta_articulos_back(TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha));
				cs.setString(3,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(4, sucursal);
				cs.setInt(5, numeroCaja);
				cs.setInt(6, numeroTransaccion);
				cs.setInt(7, numeroItem);
				cs.setLong(8, codigoArticulo);
				cs.setInt(9, vendedor);
				cs.setInt(10, codigoNovios);
				cs.setInt(11, saleArticulo);
				cs.setInt(12, precioArticulo);
				cs.setInt(13, unidades);
				cs.setInt(14, descuento);
				cs.setInt(15, tipoDescuento);
				cs.setString(16, codUnidadDespacho);
				cs.setInt(17, porcentajeParticip);
				cs.setInt(18, despachoDomicilio);
				
				logger.traceInfo("insertaArticulosBack", "{CALL ? := inserta_articulos_back(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+numeroCaja+","+numeroTransaccion+","+numeroItem+","+codigoArticulo+","+vendedor+","+codigoNovios+","+saleArticulo+","+precioArticulo+","+unidades+","+descuento+","+tipoDescuento+",'"+codUnidadDespacho+"',"+porcentajeParticip+","+despachoDomicilio+")}");
				
				cs.executeUpdate();
				
				retornoNotaCredito.setCodigo(new Integer(((cs.getInt(1) == Constantes.NRO_UNO)?Constantes.EJEC_SIN_ERRORES:Constantes.EJEC_CON_ERRORES)));
				retornoNotaCredito.setMensaje("");
				
				cs.close();
			} catch (Exception e) {
				logger.traceError("insertaArticulosBack", e);
				retornoNotaCredito.setMensaje("Mensaje de error insertaArticulosBack: "+e.getMessage());
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("insertaArticulosBack", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("insertaArticulosBack", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
		
		return retornoNotaCredito;
	}

	@Override
	public BigDecimal getTotalNc(TramaDTE trama) throws AligareException{
		
		logger.initTrace("getTotalNc", "TramaDTE: trama", new KeyLog("Correlativo Venta", trama.getNotaVenta().getCorrelativoVenta()+""));
		BigDecimal totalNC = Constantes.NRO_CERO_B;
		try {
			
//			String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
			BigDecimal vTotal = Constantes.NRO_CERO_B;
			BigDecimal montoTotal = Constantes.NRO_CERO_B;
			BigDecimal vArti = null;
			BigDecimal vDesc = null;
			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
			String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
			if (articuloVentas.size() > Constantes.NRO_CERO){
				for (int i = Constantes.NRO_CERO;i < articuloVentas.size(); i++){
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					
					if ((indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS))
						&& (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() != Constantes.ES_MKP_RIPLEY)){ 
						continue;
					}
					
					if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){
						continue;
					}
					BigDecimal precio = (Validaciones.validaBigDecimal(articuloVentaTrama.getArticuloVenta().getPrecio()))?articuloVentaTrama.getArticuloVenta().getPrecio():new BigDecimal(Constantes.NRO_CERO);
					BigDecimal descuento = (Validaciones.validaBigDecimal(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))?articuloVentaTrama.getArticuloVenta().getMontoDescuento():new BigDecimal(Constantes.NRO_CERO);
					Integer unidades = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getUnidades()))?articuloVentaTrama.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
					Integer tipoDesc = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getTipoDescuento()))?articuloVentaTrama.getArticuloVenta().getTipoDescuento():new Integer(Constantes.NRO_CERO);
					
					vArti = (descuento.compareTo(new BigDecimal(Constantes.NRO_CERO)) < Constantes.NRO_CERO)? precio.subtract(descuento) : precio;
					vTotal = vArti.multiply(new BigDecimal(unidades));
					vDesc = (descuento.compareTo(new BigDecimal(Constantes.NRO_CERO)) < Constantes.NRO_CERO)? new BigDecimal(Constantes.NRO_CERO) : descuento;
					
					montoTotal = montoTotal.add(vTotal);
					
					if(vDesc.intValue() > Constantes.NRO_CERO){
						montoTotal = (tipoDesc.intValue() == Constantes.NRO_CERO)?montoTotal.subtract(vDesc.multiply(new BigDecimal(unidades))):montoTotal.subtract(vDesc);
					}
					
				}
			}
			totalNC = montoTotal;
		} catch (Exception e) {
			logger.traceError("getTotalNc", "Error al obtener Total Nota venta ", e);
		}
		logger.endTrace("getTotalNc", "Finalizado", "long: " + totalNC);
		return totalNC;
	}
	
	public Saldos getSaldos(Long correlativoVenta) throws AligareException{
		logger.initTrace("getSaldos", "int: "+correlativoVenta, new KeyLog("Correlativo de Venta: ", ""+correlativoVenta));
		logger.traceInfo("getSaldos", "Obtiene saldos de tabla MODELEXT");
		Saldos saldos = null;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		String procSQL = "{CALL CAVIRA_MANEJO_NC.OBTIENE_SALDOS_NC(?,?,?,?,?)}";
		if (conn!=null){
			
			try {
				ResultSet rs = null;
				CallableStatement cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.CURSOR);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				
				logger.traceInfo("getSaldos", "{CALL CAVIRA_MANEJO_NC.OBTIENE_SALDOS_NC("+correlativoVenta+",out,out,out,out)}");

				cs.execute();    
	            rs = (ResultSet) cs.getObject(2);  
	            
	            if(rs !=null && rs.next()) {
	            	saldos = new Saldos();
	            	saldos.setSaldoTRE(rs.getBigDecimal(1));
	            	saldos.setSaldoCAR(rs.getBigDecimal(2));
	            	saldos.setSaldoTotal(rs.getBigDecimal(3));
	            }    
	            rs.close();
	            cs.close();
	
			} catch (Exception e) {
				logger.traceError("getSaldos", "Error durante la ejecucion de getSaldos", e);
				logger.endTrace("getSaldos", "Finalizado", "Saldos: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("getSaldos", "Problemas de conexion a la base de datos Modelo Extendido");
		}
		
		if (saldos!=null){
			logger.endTrace("getSaldos", "Finalizado existen Saldos!", "Saldos: " + saldos.toString());
		}else{
			logger.endTrace("getSaldos", "Finalizado NO existen saldos","Correlativo venta: "+ correlativoVenta);
		}
		
		return saldos;
	}
	
	@Override
	public RetornoNotaCredito anulacionFolio(Timestamp fecha,
											 int funcion,
											 int sucursal,
											 int nroCaja,
											 int nroTrx,
											 int nroBoleta,
											 Long correlativoVenta,
											 int tipoDePago,
											 String supervisor,
											 String vendedor) throws AligareException{
		
		logger.initTrace("anulacionFolio", "Timestamp fecha: "+fecha+","
											+"int funcion: "+funcion+","
											+"int sucursal: "+sucursal+","
											+"int nroCaja: "+nroCaja+","
											+"int nroTrx: "+nroTrx+","
											+"int nroBoleta: "+nroBoleta+","
											+"int correlativoVenta: "+correlativoVenta+","
											+"int tipoDePago: "+tipoDePago+","
											+"String supervisor: "+supervisor+","
											+"String vendedor:"+vendedor+"\nint: "+funcion , new KeyLog("nroBoleta: ", ""+nroBoleta));
		
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		Connection connExt = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		String procSQL;//,updatetSQL;
		CallableStatement cs = null;
		if (conn!=null){
			if(connExt != null){
				try {
//					if (funcion==Constantes.FUNCION_BOLETA){
//						procSQL = "{call anulacion_caja_insert(TO_DATE(?,?),?,?,?,?,?,?,?)}";
//					}
//					else{
						procSQL = "{call anulacion_caja_insert_NC(TO_DATE(?,?),?,?,?,?,?,?,?)}";
//					}
					logger.traceInfo("anulacionFolio", "Procede a anula insert de boleta");// - query: "+procSQL);
					cs = conn.prepareCall(procSQL);
					if (cs != null) {
						cs.setString(1,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha));
						cs.setString(2,  Constantes.FECHA_DDMMYYYY);
						cs.setInt(3,sucursal);
						cs.setInt(4,nroCaja);
						cs.setInt(5,nroTrx);
						cs.setInt(6,nroBoleta);
						cs.setInt(7,tipoDePago);
						cs.setString(8,supervisor);
						cs.setString(9,Util.getVacioPorNulo(vendedor).trim());
						
						logger.traceInfo("anulacionFolio", "{call anulacion_caja_insert_NC(TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+sucursal+","+nroCaja+","+nroTrx+","+nroBoleta+","+tipoDePago+",'"+supervisor+"','"+vendedor+"')}");
						
						cs.execute();	
						
//						if (funcion==Constantes.FUNCION_BOLETA){
							retornoNotaCredito.setCodigo(Constantes.EJEC_SIN_ERRORES);
							retornoNotaCredito.setMensaje("Anulacion exitosa.");
//						}
						cs.close();
					}
					
//					if (funcion!=Constantes.FUNCION_BOLETA){
						//Llamando a Update
//						updatetSQL = "{CALL CAVIRA_MANEJO_NC.UPDATE_ESTADO_NOTA_VENTA(?,?,?,?)}";
//						logger.traceInfo("anulacionFolio", "Actualiza estado nota venta - query: "+updatetSQL);
//						cs = connExt.prepareCall(updatetSQL);
//						cs.setInt(1,correlativoVenta);
//						cs.setInt(2, funcion);
//						cs.registerOutParameter(3, OracleTypes.NUMBER);
//						cs.registerOutParameter(4, OracleTypes.VARCHAR);
//						
//						logger.traceInfo("anulacionFolio", "{CALL CAVIRA_MANEJO_NC.UPDATE_ESTADO_NOTA_VENTA("+correlativoVenta+","+funcion+",out,out)}");
//						
//						cs.execute();
//						
//						retornoNotaCredito.setCodigo(cs.getInt(3));
//						retornoNotaCredito.setMensaje(cs.getString(4));
//					}
					
//					cs.close();
				} catch (Exception e) {
					logger.traceError("anulacionFolio", e);
					retornoNotaCredito.setMensaje("Mensaje de error anulacionFolio: "+e.getMessage());
					retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				} finally {
					PoolBDs.closeConnection(conn);
					PoolBDs.closeConnection(connExt);	
				}
			}else{
				PoolBDs.closeConnection(conn);
				logger.traceInfo("anulacionFolio", "Problemas de conexion a la base de datos Modelo Extendido");
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Conexion nula");
			}
		}else{
			logger.traceInfo("anulacionFolio", "Problemas de conexion a la base de datos Back Office");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("anulacionFolio", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
		
		return retornoNotaCredito;
		
	}

	@Override
	public RetornoNotaCredito fegife_prc_act_sld_nc(int nroTarjeta,
			long montoTarjeta, int numeroCaja, Long correlativoVenta,
			Timestamp fechaBoleta) throws AligareException {

		logger.initTrace("fegife_prc_act_sld_nc", "(int nroTarjeta: "+nroTarjeta+","
												+ "\nlong montoTarjeta: "+montoTarjeta+","
												+ "\nint numeroCaja:"+numeroCaja+","
												+ "\nint correlativoVenta:"+correlativoVenta+","
												+ "\nTimestamp fechaBoleta:"+fechaBoleta+")");

		Connection conn = PoolBDs.getConnection(BDType.TRE);
		CallableStatement cs;

		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		if (conn != null) {
			try {
				String procSQL = "{CALL FEGIFE_PRC_ACT_SLD_NC(?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, nroTarjeta);
				cs.setLong(2, montoTarjeta);
				cs.setInt(3, numeroCaja);
				cs.setLong(4, correlativoVenta);
				cs.setString(5,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaBoleta).replaceAll(Constantes.GUION, Constantes.VACIO));
				cs.registerOutParameter(6, OracleTypes.VARCHAR);
				cs.registerOutParameter(7, OracleTypes.NUMBER);
				
				logger.traceInfo("fegife_prc_act_sld_nc", "{CALL FEGIFE_PRC_ACT_SLD_NC("+nroTarjeta+","+montoTarjeta+","+numeroCaja+","+correlativoVenta+",'"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY3, fechaBoleta).replaceAll(Constantes.GUION, Constantes.VACIO)+"',out,out)}");
				
				cs.execute();
				
				retornoNotaCredito.setCodigo(cs.getInt(7));
				retornoNotaCredito.setMensaje(cs.getString(6));
				
				cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error fegife_prc_act_sld_nc: " + e.getMessage());
				logger.traceError("fegife_prc_act_sld_nc", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("fegife_prc_act_sld_nc", "Problemas de conexion a la base de datos TRE");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		
		logger.endTrace("fegife_prc_act_sld_nc", "Finalizado", "RetornoNotaCredito: " +retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	@Override
	public RetornoNotaCredito updateNotaVenta(Integer estado,
			Integer numeroNotaCredito, Integer numeroBoleta,
			Timestamp fechaNotaCredito, Timestamp horaNotaCredito, Integer numeroCaja,
			Integer numeroCorrelativoNotaCredito, Integer rutVendedor,
			Long correlativoVenta) {
		logger.initTrace("updateNotaVenta", "(Integer estado: "+estado+", "
											+ "\nInteger numeroNotaCredito: "+numeroNotaCredito+", "
											+ "\nInteger numeroBoleta: "+numeroBoleta+", "
											+ "\nTimestamp fechaNotaCredito: "+fechaNotaCredito+", "
											+ "\nTimestamp horaNotaCredito: "+horaNotaCredito+", "
											+ "\nInteger numeroCaja: "+numeroCaja+", "
											+ "\nInteger numeroCorrelativoNotaCredito: "+numeroCorrelativoNotaCredito+", "
											+ "\nInteger rutVendedor: "+rutVendedor+","
											+ "\nInteger correlativoVenta: "+correlativoVenta+")");
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		String procSQL;
		CallableStatement cs = null;
		if (conn!=null){
			try {
				procSQL = "{call CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA(?,?,?,TO_DATE(?,?),TO_DATE(?,?),?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, estado);
				cs.setInt(2, numeroNotaCredito);
				cs.setInt(3, numeroBoleta);
				cs.setString(4,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaNotaCredito));
				cs.setString(5,  Constantes.FECHA_DDMMYYYY);
				cs.setString(6,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, horaNotaCredito));
				cs.setString(7,  Constantes.FECHA_DDMMYYYY);
				cs.setInt(8, numeroCaja);
				cs.setInt(9, numeroCorrelativoNotaCredito);
				cs.setInt(10, rutVendedor);
				cs.setLong(11, correlativoVenta);
				cs.registerOutParameter(12, OracleTypes.NUMBER);
				cs.registerOutParameter(13, OracleTypes.VARCHAR);
				
				logger.traceInfo("updateNotaVenta", "{call CAVIRA_MANEJO_NC.UPDATE_NOTA_VENTA("+estado+","+numeroNotaCredito+","+numeroBoleta+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fechaNotaCredito)+"','"+Constantes.FECHA_DDMMYYYY+"'),TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, horaNotaCredito)+"','"+Constantes.FECHA_DDMMYYYY+"'),"+numeroCaja+","+numeroCorrelativoNotaCredito+","+rutVendedor+","+correlativoVenta+",out,out)}");
				
				cs.execute();
				
				retornoNotaCredito.setCodigo(cs.getInt(12));
				retornoNotaCredito.setMensaje(cs.getString(13));
				
				cs.close();
			} catch (Exception e) {
				logger.traceError("updateNotaVenta", e);
				retornoNotaCredito.setMensaje("Mensaje de error updateNotaVenta: " + e.getMessage());
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("updateNotaVenta", "Problemas de conexion a la base de datos Modelo Extendido");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("updateNotaVenta", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
		
		return retornoNotaCredito;
	}

	@Override
	public RetornoNotaCredito updateMarketPlace(Integer estado,
			Integer transaccion, Integer numeroCaja, Timestamp fecha,
			Long correlativoVenta, String ordenMkp) {
		logger.initTrace("updateMarketPlace", "Integer: "+estado, new KeyLog("Correlativo Venta: ", ""+correlativoVenta));
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		String procSQL;
		CallableStatement cs = null;
		if (conn!=null){
			try {
				procSQL =  "{CALL CAVIRA_MANEJO_NC.UPDATE_MARKETPLACE(?,?,?,TO_DATE(?,?),?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, estado);
				cs.setInt(2, transaccion);
				cs.setInt(3, numeroCaja);
				cs.setString(4,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fecha));
				cs.setString(5,  Constantes.FECHA_DDMMYYYY_HHMMSS2);
				cs.setLong(6, correlativoVenta);
				cs.setString(7, ordenMkp);
				cs.registerOutParameter(8, OracleTypes.NUMBER);
				cs.registerOutParameter(9, OracleTypes.VARCHAR);
				
				logger.traceInfo("updateMarketPlace", "{CALL CAVIRA_MANEJO_NC.UPDATE_MARKETPLACE("+estado+","+transaccion+","+numeroCaja+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fecha)+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"+correlativoVenta+",'"+ordenMkp+"',out,out)}");
				
				cs.execute();
				
				retornoNotaCredito.setCodigo(cs.getInt(8));
				retornoNotaCredito.setMensaje(cs.getString(9));
				
				cs.close();
			} catch (Exception e) {
				logger.traceError("updateMarketPlace", e);
				retornoNotaCredito.setMensaje("Error en updateMarketPlace: "+e.getMessage());
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}else{
			logger.traceInfo("updateMarketPlace", "Problemas de conexion a la base de datos Modelo Extendido");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		logger.endTrace("updateMarketPlace", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
		
		return retornoNotaCredito;
	}

	@Override
	public RetornoNotaCredito updateArticuloVenta(Integer nroBoleta,
			  Timestamp fecha,
			  Timestamp fechaHora,
			  Integer nroCaja,
			  Integer transaccion,
			  Integer vendedor,
			  Long correlativoVenta,
			  String  cud,
			  Integer correlativoItem) {

			logger.initTrace("updateArticuloVenta", "(Integer nroBoleta: "+nroBoleta+", "
													+ "\nTimestamp fecha: "+fecha+", "
													+ "\nTimestamp fechaHora: "+fechaHora+", "
													+ "\nInteger nroCaja: "+nroCaja+", "
													+ "\nInteger transaccion: "+transaccion+","
													+ "\nInteger vendedor: "+vendedor+", "
													+ "\nInteger correlativoVenta: "+correlativoVenta+", "
													+ "\nString cud: "+cud+","
													+ "\nInteger correlativoItem: "+correlativoItem+")");
			Connection connExt = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
			String procSQL;
			CallableStatement cs = null;
			if (connExt!=null){
				try {
					procSQL =  "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA(?,TO_DATE(?,?),TO_DATE(?,?),?,?,?,?,?,?,?,?)}";
					cs = connExt.prepareCall(procSQL);
					cs.setInt(1, nroBoleta);
					cs.setString(2,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha));
					cs.setString(3,  Constantes.FECHA_DDMMYYYY);
					cs.setString(4,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaHora));
					cs.setString(5,  Constantes.FECHA_DDMMYYYY_HHMMSS2);
					cs.setInt(6, nroCaja);
					cs.setInt(7, transaccion);
					cs.setInt(8, vendedor);
					cs.setLong(9, correlativoVenta);
					cs.setString(10, cud);
					cs.setLong(11, correlativoItem);
					cs.registerOutParameter(12, OracleTypes.NUMBER);
					cs.registerOutParameter(13, OracleTypes.VARCHAR);
					
					logger.traceInfo("updateArticuloVenta", "{CALL CAVIRA_MANEJO_NC.UPDATE_ARTICULO_VENTA("+nroBoleta+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY4, fecha)+"','"+Constantes.FECHA_DDMMYYYY+"'),TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaHora)+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"
							+nroCaja+","+transaccion+","+vendedor+","+correlativoVenta+",'"+cud+"',"+correlativoItem+",out,out)}");
					
					cs.execute();
					
					retornoNotaCredito.setCodigo(cs.getInt(12));
					retornoNotaCredito.setMensaje(cs.getString(13));
						
					cs.close();
				} catch (Exception e) {
					logger.traceError("updateArticuloVenta", e);
					retornoNotaCredito.setMensaje("Error en updateArticuloVenta: "+e.getMessage());
					retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				} finally {
					PoolBDs.closeConnection(connExt);	
				}
			}else{
				logger.traceInfo("updateArticuloVenta", "Problemas de conexion a la base de datos Modelo Extendido");
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Conexion nula");
			}
			logger.endTrace("updateArticuloVenta", "Finalizado", "RetornoNotaCredito: " + retornoNotaCredito.toString());
			
			return retornoNotaCredito;
		}
	

	@Override
	public int obtieneMaxCorrNC() {
		logger.initTrace("obtieneMaxCorrNC", null);
		logger.traceInfo("obtieneMaxCorrNC", "Obtiene numero m�ximo de Correlativo NC de tabla MODELEXT");
		int MaxCorrNC=Constantes.NRO_CERO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
			String procSQL = "{CALL CAVIRA_MANEJO_NC.MAX_CORR_NC(?,?,?)}";
			try {
				CallableStatement cs = conn.prepareCall(procSQL);
				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.registerOutParameter(2, OracleTypes.NUMBER);
				cs.registerOutParameter(3, OracleTypes.VARCHAR);
				
				logger.traceInfo("obtieneMaxCorrNC", "{CALL CAVIRA_MANEJO_NC.MAX_CORR_NC(out,out,out)}");
				
				cs.execute();    
	            
	            MaxCorrNC= (cs.getInt(2)==Constantes.EJEC_SIN_ERRORES)?cs.getInt(1):Constantes.EJEC_CON_ERRORES;   
	            
	            cs.close();
			} catch (Exception e) {
				logger.traceError("obtieneMaxCorrNC", e);
				MaxCorrNC = Constantes.EJEC_CON_ERRORES;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("obtieneMaxCorrNC", "Problemas de conexion a la base de datos Modelo Extendido");
			MaxCorrNC = Constantes.EJEC_CON_ERRORES;
		}
		
		logger.endTrace("obtieneMaxCorrNC", "Finalizado", "Max N�Correlativo NC: "+ MaxCorrNC);
		return MaxCorrNC;
	}
	
	public Integer tieneCud(String articulo) {
		logger.initTrace("tieneCud", "String: " + articulo);
		Integer sal = new Integer(Constantes.NRO_CERO);
		String query = "select count(*) as CANTIDAD from tvirtual_articuloS_sin_cud where cod_art_ean13 = ?";
		String q = "select count(*) as CANTIDAD from tvirtual_articuloS_sin_cud where cod_art_ean13 = "+articulo;
		logger.traceInfo("tieneCud", "Consulta a Back Office: " + q);
		Connection conn = PoolBDs.getConnection(BDType.BACK_OFFICE);
		if (conn != null) {
			try {
				ResultSet rs = null;
				PreparedStatement ps = conn.prepareStatement(query);
				if (ps != null) {
					ps.setString(1, articulo);
					rs = ps.executeQuery();
					if (rs != null && rs.next()) {
						sal = new Integer(rs.getInt("CANTIDAD"));
						rs.close();
					}
					ps.close();
				}
				 
			} catch (Exception e) {
				logger.traceError("tieneCud", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("tieneCud", "Problemas de conexion a la base de datos Back Office");
		}
		logger.endTrace("tieneCud", "Finalizado", "Integer: " + sal.intValue());
		return sal;
	}

	@Override
	public MontoNroTarje obtieneMontoNroTarje(Long correlativoVenta) {
		logger.initTrace("obtieneMontoNroTarje", "int: " + correlativoVenta, new KeyLog("Correlativo Venta", correlativoVenta+""));
		MontoNroTarje montoNroTar = null;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
			String procSQL = "{CALL CAVIRA_MANEJO_NC.OBTIENE_MONTO_NRO_TARJETA(?,?,?,?,?)}";
			try {
				CallableStatement cs = conn.prepareCall(procSQL);
				cs.setLong(1,correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.NUMBER);
				cs.registerOutParameter(3, OracleTypes.NUMBER);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				
				logger.traceInfo("obtieneMontoNroTarje", "{CALL CAVIRA_MANEJO_NC.OBTIENE_MONTO_NRO_TARJETA("+correlativoVenta+",out,out,out,out)}");
				
				cs.execute();    
	            
				if(cs.getInt(4)==Constantes.EJEC_SIN_ERRORES){
					montoNroTar = new MontoNroTarje();
					montoNroTar.setMonto(cs.getInt(2));
					montoNroTar.setNroTarjeta(cs.getInt(3));
	            }
				
	            cs.close();
			} catch (Exception e) {
				logger.traceError("obtieneMontoNroTarje", e);
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("obtieneMontoNroTarje", "Problemas de conexion a la base de datos Modelo Extendido");
		}
		logger.endTrace("obtieneMontoNroTarje", "Se obtiene dato correctamente ","Monto Nro.Tarjeta: "+((montoNroTar != null)?montoNroTar.toString():"null"));
		return montoNroTar;
	}

	@Override
	public RetornoNotaCredito insertIntoCredito(Integer idCorrNc,
			Integer nroBoleta, Timestamp fechaNc, Integer correlativoVenta,
			String tipoPago, Integer montoCompra, long glTotalNc,
			long montoCar, long saldoCar, long montoTre,
			long saldoTre, long saldoTotal) {
		
		logger.initTrace("insertIntoCredito", "Integer: "+idCorrNc+", Integer: "+nroBoleta
				+", Timestamp: "+fechaNc+", Integer: "+correlativoVenta
				+", String: "+tipoPago+", Integer: "+montoCompra
				+", long: "+glTotalNc+", long: "+montoCar+", long: "+saldoCar
				+", long: "+montoTre+", long: "+saldoTre+", long: "+saldoTotal);

		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn!=null){
			String procSQL = "{CALL CAVIRA_MANEJO_NC.INSERT_NOTA_CREDITO(?,?,TO_DATE(?,?),?,?,?,?,?,?,?,?,?,?,?)}";
			try {
				CallableStatement cs = conn.prepareCall(procSQL);
				cs.setInt(1,idCorrNc);
				cs.setInt(2,nroBoleta);
				cs.setString(3,  FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaNc));
				cs.setString(4,  Constantes.FECHA_DDMMYYYY_HHMMSS2);
				cs.setInt(5,correlativoVenta);
				cs.setString(6,tipoPago);
				cs.setInt(7,montoCompra);
				cs.setLong(8,glTotalNc);
				cs.setLong(9,montoCar);
				cs.setLong(10,saldoCar);
				cs.setLong(11,montoTre);
				cs.setLong(12,saldoTre);
				cs.setLong(13,saldoTotal);
				cs.registerOutParameter(14, OracleTypes.NUMBER);
				cs.registerOutParameter(15, OracleTypes.VARCHAR);
				
				logger.traceInfo("insertIntoCredito", "{CALL CAVIRA_MANEJO_NC.INSERT_NOTA_CREDITO("+idCorrNc+","+nroBoleta+",TO_DATE('"+FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY_HHMMSS3, fechaNc)+"','"+Constantes.FECHA_DDMMYYYY_HHMMSS2+"'),"+correlativoVenta+",'"+tipoPago+"',"+montoCompra+","+glTotalNc+","+montoCar+","+saldoCar+","+montoTre+","+saldoTre+","+saldoTotal+",out,out)}");
				
				cs.execute();    
	            
				retornoNotaCredito.setCodigo(cs.getInt(14));
				retornoNotaCredito.setMensaje(cs.getString(15));
				
	            cs.close();
			} catch (Exception e) {
				retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoNotaCredito.setMensaje("Mensaje de error: "+e.getMessage());
				logger.traceError("insertIntoCredito", e);
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("insertIntoCredito", "Problemas de conexion a la base de datos Modelo Extendido");
			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoNotaCredito.setMensaje("Conexion nula");
		}
		
    	logger.endTrace("insertIntoCredito", "Finalizado","RetornoNotaCredito: "+retornoNotaCredito.toString());
		return retornoNotaCredito;
	}

	public boolean actualizarEsNCArticuloVenta(Long correlativoVenta, Integer esNC){
		logger.initTrace("actualizarEsNCArticuloVenta", "Integer correlativoVenta: " + correlativoVenta+Constantes.VACIO + " Integer esNC: "+esNC+Constantes.VACIO);
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_MANEJO_NC.PROC_ACT_ES_NC_ARTICULO_VENTA(?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setInt(1, esNC);
				cs.setLong(2, correlativoVenta);
				cs.registerOutParameter(3, OracleTypes.NUMBER);
				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				
				logger.traceInfo("actualizarEsNCArticuloVenta", "{CALL CAVIRA_MANEJO_NC.PROC_ACT_ES_NC_ARTICULO_VENTA("+esNC+","+correlativoVenta+",out,out)}");
				
				cs.execute();
				rsOut = cs.getInt(3);
				rsMsjOut = cs.getString(4);
				if (rsOut == Constantes.NRO_CERO) {
					respuesta = Constantes.VERDADERO;
				} else {
					logger.traceInfo("actualizarEsNCArticuloVenta", "Codigo: "+rsOut+" Error: "+rsMsjOut);
				}
			} catch (Exception e) {
				logger.traceError("actualizarEsNCArticuloVenta", "Error al actualizar Articulo Venta: ", e);
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("actualizarEsNCArticuloVenta", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("actualizarEsNCArticuloVenta", "Finalizado", "respuesta: " + respuesta);

		return respuesta;
	}

}