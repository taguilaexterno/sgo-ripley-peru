package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.DatoFacturacion;
import cl.ripley.omnicanalidad.bean.Despacho;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;
import cl.ripley.omnicanalidad.bean.TarjetaBancaria;
import cl.ripley.omnicanalidad.bean.TipoDoc;
import cl.ripley.omnicanalidad.bean.Vendedor;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

/**
 * NotaVentaDAOImpl posee las funciones que 
 * permite trabajar con notas de venta
 * 
 * @author Mauro Sanhueza T
 * @date 04-08-2016
 * @version     %I%, %G%
 * @since 1.0 
 */
@Repository
public class NotaVentaDAOImpl implements NotaVentaDAO{

	private static final AriLog logger = new AriLog(NotaVentaDAOImpl.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	/**
	 * cargaOrdenesCompraCajaTransito es la funcion que permite cargas todas las notas 
	 * de venta que seran seleccionadas desde modelo extendido que no posean una caja
	 * asignada. Se les asiganara una caja para que queden invisibles a cualquier otro
	 * proceso que cambie su estado mediante el uso de esta
	 * 
	 * @param nroCajaTransito 	numero que asignaremos a notas de venta para marcar 
	 * 							en proceso principal de validacion
	 * @param volumen			cantida de notas de venta que cargamos
	 * @param estadosIniciales	estados para filtrar notas de venta sin caja, estos
	 * 							estados son lo que no llega de la caja virtual
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */
	public List<NotaVenta> cargaOrdenesCompraCajaTransito(String nroCajaTransito, String volumen, String estadosIniciales){
		
		logger.initTrace("cargaOrdenesCompraCajaTransito", "String: "+nroCajaTransito+", String: "+volumen+", String: "+estadosIniciales);
		
//		Connection conn = null;
//		CallableStatement cs = null;
//		Date parsedDate = null;
//		Timestamp timestamp = null;
		List<NotaVenta> listaOrdenCompra = null;
//		try {
//			
//			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
//			
//			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
//			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_ORDENES_COMPRA_EVAL(?,?,?,?,?,?)}";
//			cs = conn.prepareCall(procSQL);
//			cs.setInt(1, Integer.parseInt(nroCajaTransito));
//			cs.setInt(2, Integer.parseInt(volumen));
//			cs.setString(3, estadosIniciales);
//			cs.registerOutParameter(4, OracleTypes.CURSOR);
//			cs.registerOutParameter(5, OracleTypes.NUMBER);
//			cs.registerOutParameter(6, OracleTypes.VARCHAR);
//			logger.traceInfo("cargaOrdenesCompraCajaTransito", "procSQL: CAVIRA_ORDEN_COMPRA.PROC_CARGA_ORDENES_COMPRA_EVAL("+nroCajaTransito+","+volumen+","+estadosIniciales+",?,?,?)");
//			cs.execute();
//			logger.traceInfo("cargaOrdenesCompraCajaTransito", "out1: cursor");
//			logger.traceInfo("cargaOrdenesCompraCajaTransito", "out2: "+cs.getInt(5));
//			logger.traceInfo("cargaOrdenesCompraCajaTransito", "out3: "+cs.getString(6));
//			ResultSet rs = (ResultSet) cs.getObject(4);
//			if(rs != null){
//				listaOrdenCompra = new ArrayList<NotaVenta>();
//				while (rs.next()) {
//					NotaVenta ordenCompra = new NotaVenta();
//					ordenCompra.setCorrelativoVenta(Integer.parseInt(rs.getString("CORRELATIVO_VENTA")));
//					
//					parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
//					timestamp = new java.sql.Timestamp(parsedDate.getTime());
//					ordenCompra.setFechaBoleta(timestamp);
//					
//					ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
//					ordenCompra.setMontoVenta(Integer.parseInt(rs.getString("MONTO_VENTA")));
//					ordenCompra.setMontoDescuento(Integer.parseInt(rs.getString("MONTO_DESCUENTO")));
//					ordenCompra.setTipoDescuento(Integer.parseInt(rs.getString("TIPO_DESCUENTO")));
//					ordenCompra.setRutComprador(Integer.parseInt(rs.getString("RUT_COMPRADOR")));
//					ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
//					ordenCompra.setNumeroSucursal(Integer.parseInt(rs.getString("NUMERO_SUCURSAL")));
//					ordenCompra.setCodigoRegalo(Integer.parseInt(rs.getString("CODIGO_REGALO")));
//					ordenCompra.setTipoRegalo(Integer.parseInt(rs.getString("TIPO_REGALO")));
//					ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Integer.parseInt(rs.getString("NUMERO_BOLETA")) : null  );
//					
//					if(rs.getString("FECHA_BOLETA") != null){
//						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFechaBoleta(timestamp);
//					}
//					
//					if(rs.getString("HORA_BOLETA")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraBoleta(timestamp);
//					}
//					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(rs.getString("NUMERO_CAJA")): null);
//					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ? Integer.parseInt(rs.getString("CORRELATIVO_BOLETA")) : null);
//					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(rs.getString("RUT_BOLETA")) : null);
//					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
//					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_NOTA_CREDITO")) : null);
//					
//					if(rs.getString("FEC_NOTA_CREDITO")!= null){
//						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFecNotaCredito(timestamp);
//					}
//					if(rs.getString("HORA_NOTA_CREDITO")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraNotaCredito(timestamp);
//					}		
//					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_CAJA_NOTA_CREDITO")) : null);
//					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("CORRELATIVO_NOTA_CREDITO")): null);
//					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("RUT_NOTA_CREDITO")) : null);
//					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
//					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
//					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
//					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
//					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
//					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(rs.getString("TIPO_DOC")) : null);
//					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
//					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Integer.parseInt(rs.getString("FOLIO_NC_SII")) : null);
//					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
//					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
//					ordenCompra.setUsuario(rs.getString("USUARIO"));
//					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//					
//					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
//					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Integer.parseInt(rs.getString("RUT_CLIENTE")) : null);
//					//ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? Integer.parseInt(rs.getString("TELEFONO_CLIENTE")) : null);
//					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? rs.getString("TELEFONO_CLIENTE") : null);
//					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
//					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(rs.getString("COMUNA_CLIENTE")) : null);
//					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(rs.getString("REGION_CLIENTE")) : null);
//					ordenCompra.setDireccionDespacho(rs.getString("DIRECCION_CLIENTE") != null ? rs.getString("DIRECCION_CLIENTE") : null);
//					
//					listaOrdenCompra.add(ordenCompra);
//	            }
//			}	
//		} catch (Exception e) {
//			logger.traceError("cargaOrdenesCompraCajaTransito", e);
//			throw new AligareException("Error en cargaOrdenesCompraCajaTransito: " + e);
//		} finally {
//			PoolBDs.closeConnection(conn);
//			logger.endTrace("cargaOrdenesCompraCajaTransito", "Finalizado", "listaOrdenCompra: "+((listaOrdenCompra!=null)?""+listaOrdenCompra.size():"null"));
//		}
		return listaOrdenCompra;
	}
	
	
	/**
	 * cargaOrdenesCompraCajaTransitoPorOC es la funcion que permite cargas una nota de venta
	 * que será seleccionada desde modelo extendido que no posean una caja
	 * asignada. Se le asiganara una caja para que quede invisible a cualquier otro
	 * proceso que cambie su estado mediante el uso de esta
	 * 
	 * @param nroCajaTransito 	numero que asignaremos a notas de venta para marcar 
	 * 							en proceso principal de validacion
	 * @param volumen			cantida de notas de venta que cargamos
	 * @param estadosIniciales	estados para filtrar notas de venta sin caja, estos
	 * 							estados son lo que no llega de la caja virtual
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */
	public NotaVenta cargaOrdenesCompraCajaTransitoPorOC(String nroCajaTransito, Long oc, String estadosIniciales){
		
		logger.initTrace("cargaOrdenesCompraCajaTransitoPorOC", "nroCajaTransito String: "+nroCajaTransito+", oc Long: "+oc+", estadosIniciales String: "+estadosIniciales);
		
		Connection conn = null;
		CallableStatement cs = null;
		Date parsedDate = null;
		Timestamp timestamp = null;
		NotaVenta ordenCompra = new NotaVenta();
		
		
		try {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
			
			/**
			 * PROCEDURE PROC_CARGA_OC_VAL_SIN_CAJA(  IN_nCAJA_TRANSITO IN NUMBER,
                                              IN_vESTADOS_INICIALES IN VARCHAR2,
                                              IN_nORDEN_COMPRA IN NUMBER,
                                              OUT_cRetorno OUT SYS_REFCURSOR,
                                              OUT_nCOD OUT NUMBER,
                                              OUT_vMJE OUT VARCHAR2)
			 */
			
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_OC_VAL_SIN_CAJA(?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, Integer.parseInt(nroCajaTransito));
			cs.setString(2, estadosIniciales);
			cs.setLong(3, oc);
			cs.registerOutParameter(4, OracleTypes.CURSOR);
			cs.registerOutParameter(5, OracleTypes.NUMBER);
			cs.registerOutParameter(6, OracleTypes.VARCHAR);
			logger.traceInfo("cargaOrdenesCompraCajaTransitoPorOC", "procSQL: CAVIRA_ORDEN_COMPRA.PROC_CARGA_OC_VAL_SIN_CAJA("+nroCajaTransito+","+oc+","+estadosIniciales+",?,?,?)");
			cs.execute();
			logger.traceInfo("cargaOrdenesCompraCajaTransitoPorOC", "out1: cursor");
			logger.traceInfo("cargaOrdenesCompraCajaTransitoPorOC", "out2: "+cs.getInt(5));
			logger.traceInfo("cargaOrdenesCompraCajaTransitoPorOC", "out3: "+cs.getString(6));
			ResultSet rs = (ResultSet) cs.getObject(4);
			if(rs != null){
				while (rs.next()) {					
					ordenCompra.setCorrelativoVenta(Long.parseLong(Util.quitarEspacios(rs.getString("CORRELATIVO_VENTA"))));
					
					parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
					timestamp = new java.sql.Timestamp(parsedDate.getTime());
					ordenCompra.setFechaBoleta(timestamp);
					
					ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
					ordenCompra.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
					ordenCompra.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
					ordenCompra.setTipoDescuento(Integer.parseInt(Util.quitarEspacios(rs.getString("TIPO_DESCUENTO"))));
					ordenCompra.setRutComprador(Long.parseLong(Util.quitarEspacios(rs.getString("RUT_COMPRADOR"))));
					ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
					ordenCompra.setNumeroSucursal(Integer.parseInt(Util.quitarEspacios(rs.getString("NUMERO_SUCURSAL"))));
					ordenCompra.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
					ordenCompra.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
					ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("NUMERO_BOLETA"))) : null  );
					
					if(rs.getString("FECHA_BOLETA") != null){
						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());
						ordenCompra.setFechaBoleta(timestamp);
					}
					
					if(rs.getString("HORA_BOLETA")!= null){
						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
						ordenCompra.setHoraBoleta(timestamp);
					}
					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("NUMERO_CAJA"))): null);
					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ?Long.parseLong(Util.quitarEspacios(rs.getString("CORRELATIVO_BOLETA"))) : null);
					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("RUT_BOLETA"))) : null);
					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("NUM_NOTA_CREDITO"))) : null);
					
					if(rs.getString("FEC_NOTA_CREDITO")!= null){
						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());
						ordenCompra.setFecNotaCredito(timestamp);
					}
					if(rs.getString("HORA_NOTA_CREDITO")!= null){
						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
						ordenCompra.setHoraNotaCredito(timestamp);
					}		
					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("NUM_CAJA_NOTA_CREDITO"))) : null);
					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("CORRELATIVO_NOTA_CREDITO"))): null);
					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("RUT_NOTA_CREDITO"))) : null);
					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("TIPO_DOC"))) : null);
					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("FOLIO_NC_SII"))) : null);
					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					ordenCompra.setUsuario(rs.getString("USUARIO"));
					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					
					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("RUT_CLIENTE"))) : null);
					//ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? Integer.parseInt(rs.getString("TELEFONO_CLIENTE")) : null);
					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? rs.getString("TELEFONO_CLIENTE") : null);
					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("COMUNA_CLIENTE"))) : null);
					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("REGION_CLIENTE"))) : null);
					ordenCompra.setDireccionDespacho(rs.getString("DIRECCION_CLIENTE") != null ? rs.getString("DIRECCION_CLIENTE") : null);
					
	            }
			}	
		} catch (Exception e) {
			logger.traceError("cargaOrdenesCompraCajaTransitoPorOC", e);
			throw new AligareException("Error en cargaOrdenesCompraCajaTransitoPorOC: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("cargaOrdenesCompraCajaTransitoPorOC", "Finalizado", "ordenCompra: "+((ordenCompra!=null)?""+ordenCompra.getCorrelativoVenta():"null"));
		}
		return ordenCompra;
	}
	
	

	/**
	 * cargaOrdenesCompraCajaTransitoSeguras es la funcion que permite cargas todas las notas 
	 * de venta que seran seleccionadas desde modelo extendido que posean una caja en transito
	 * asignada. 
	 * 
	 * @param nroCajaTransito 	numero para filtrar las notas de venta para 
	 * 							proceso final de validacion
	 * @param volumen			cantidas de notas de venta que cargamos
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */
	public List<NotaVenta> cargaOrdenesCompraCajaTransitoSeguras(	String nroCajaTransito, 
																	String volumen) {
		
		logger.initTrace("cargaOrdenesCompraCajaTransitoSeguras", "String: "+nroCajaTransito+", String: "+volumen);
		
//		Connection conn = null;
//		CallableStatement cs = null;
//		Date parsedDate = null;
//		Timestamp timestamp = null;
		List<NotaVenta> listaOrdenCompra = null;
//		try {
//			
//			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
//			
//			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
//			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_NVENTA_VALIDAS(?,?,?,?,?)}";
//			cs = conn.prepareCall(procSQL);
//			cs.setInt(1, Integer.parseInt(nroCajaTransito));
//			cs.setInt(2, Integer.parseInt(volumen));
//			cs.registerOutParameter(3, OracleTypes.CURSOR);
//			cs.registerOutParameter(4, OracleTypes.NUMBER);
//			cs.registerOutParameter(5, OracleTypes.VARCHAR);
//			logger.traceInfo("cargaOrdenesCompraCajaTransitoSeguras", "procSQL: CAVIRA_ORDEN_COMPRA.PROC_CARGA_NVENTA_VALIDAS("+nroCajaTransito+","+volumen+",out,out,out)");
//
//			cs.execute();
//			logger.traceInfo("cargaOrdenesCompraCajaTransitoSeguras", "out1: cursor");
//			logger.traceInfo("cargaOrdenesCompraCajaTransitoSeguras", "out2: "+cs.getInt(4));
//			logger.traceInfo("cargaOrdenesCompraCajaTransitoSeguras", "out3: "+cs.getString(5));
//
//			ResultSet rs = (ResultSet) cs.getObject(3);
//			if(rs != null){
//				listaOrdenCompra = new ArrayList<NotaVenta>();
//				while (rs.next()) {
//					NotaVenta ordenCompra = new NotaVenta();
//					ordenCompra.setCorrelativoVenta(Integer.parseInt(rs.getString("CORRELATIVO_VENTA")));
//					
//					parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
//					timestamp = new java.sql.Timestamp(parsedDate.getTime());
//					ordenCompra.setFechaBoleta(timestamp);
//					
//					ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
//					ordenCompra.setMontoVenta(Integer.parseInt(rs.getString("MONTO_VENTA")));
//					ordenCompra.setMontoDescuento(Integer.parseInt(rs.getString("MONTO_DESCUENTO")));
//					ordenCompra.setTipoDescuento(Integer.parseInt(rs.getString("TIPO_DESCUENTO")));
//					ordenCompra.setRutComprador(Integer.parseInt(rs.getString("RUT_COMPRADOR")));
//					ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
//					ordenCompra.setNumeroSucursal(Integer.parseInt(rs.getString("NUMERO_SUCURSAL")));
//					ordenCompra.setCodigoRegalo(Integer.parseInt(rs.getString("CODIGO_REGALO")));
//					ordenCompra.setTipoRegalo(Integer.parseInt(rs.getString("TIPO_REGALO")));
//					ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Integer.parseInt(rs.getString("NUMERO_BOLETA")) : null  );
//					
//					if(rs.getString("FECHA_BOLETA") != null){
//						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFechaBoleta(timestamp);
//					}
//					
//					if(rs.getString("HORA_BOLETA")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraBoleta(timestamp);
//					}
//					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(rs.getString("NUMERO_CAJA")): null);
//					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ? Integer.parseInt(rs.getString("CORRELATIVO_BOLETA")) : null);
//					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(rs.getString("RUT_BOLETA")) : null);
//					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
//					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_NOTA_CREDITO")) : null);
//					
//					if(rs.getString("FEC_NOTA_CREDITO")!= null){
//						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFecNotaCredito(timestamp);
//					}
//					if(rs.getString("HORA_NOTA_CREDITO")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraNotaCredito(timestamp);
//					}		
//					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_CAJA_NOTA_CREDITO")) : null);
//					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("CORRELATIVO_NOTA_CREDITO")): null);
//					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("RUT_NOTA_CREDITO")) : null);
//					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
//					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
//					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
//					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
//					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
//					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(rs.getString("TIPO_DOC")) : null);
//					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
//					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Integer.parseInt(rs.getString("FOLIO_NC_SII")) : null);
//					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
//					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
//					ordenCompra.setUsuario(rs.getString("USUARIO"));
//					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//					
//					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
//					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Integer.parseInt(rs.getString("RUT_CLIENTE")) : null);
//					//ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? Integer.parseInt(rs.getString("TELEFONO_CLIENTE")) : null);
//					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? rs.getString("TELEFONO_CLIENTE") : null);
//					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
//					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(rs.getString("COMUNA_CLIENTE")) : null);
//					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(rs.getString("REGION_CLIENTE")) : null);
//					ordenCompra.setDireccionDespacho(rs.getString("DIRECCION_CLIENTE") != null ? rs.getString("DIRECCION_CLIENTE") : null);
//
//					listaOrdenCompra.add(ordenCompra);
//	            }
//			}	
//		} catch (Exception e) {
//			logger.traceError("cargaOrdenesCompraCajaTransitoSeguras", e);
//			throw new AligareException("Error en cargaOrdenesCompraCajaTransitoSeguras: " + e);
//		} finally {
//			PoolBDs.closeConnection(conn);
//			logger.endTrace("cargaOrdenesCompraCajaTransitoSeguras", "Finalizado", "listaOrdenCompra: "+((listaOrdenCompra!=null)?""+listaOrdenCompra.size():"null"));
//		}
		return listaOrdenCompra;
	}
	
	public NotaVenta cargarOrdenOC(Long oc){
		
		Connection conn = null;
		CallableStatement cs = null;
                ResultSet rs = null;
		Date parsedDate = null;
		Timestamp timestamp = null;
		NotaVenta ordenCompra = null;
		
		try {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
			
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_NVENTA_VALIDAS_OC(?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1,  oc);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.execute();
			
			rs = (ResultSet) cs.getObject(2);
			if(rs != null && rs.next()){
                                ordenCompra = new NotaVenta();		
				ordenCompra.setCorrelativoVenta(Long.parseLong((rs.getString("CORRELATIVO_VENTA"))));
				parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
				timestamp = new java.sql.Timestamp(parsedDate.getTime());
				ordenCompra.setFechaBoleta(timestamp);
				ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
				ordenCompra.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
				ordenCompra.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
				ordenCompra.setTipoDescuento(Integer.parseInt(rs.getString("TIPO_DESCUENTO")));
				ordenCompra.setRutComprador(Long.parseLong(Util.quitarEspacios(rs.getString("RUT_COMPRADOR"))));
				ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
				ordenCompra.setNumeroSucursal(Integer.parseInt(rs.getString("NUMERO_SUCURSAL")));
				ordenCompra.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
				ordenCompra.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
				ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("NUMERO_BOLETA"))) : null  );
					
					if(rs.getString("FECHA_BOLETA") != null){
						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());
						ordenCompra.setFechaBoleta(timestamp);
					}
					
					if(rs.getString("HORA_BOLETA")!= null){
						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
						ordenCompra.setHoraBoleta(timestamp);
					}
					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("NUMERO_CAJA"))): null);
					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("CORRELATIVO_BOLETA"))) : null);
					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("RUT_BOLETA"))) : null);
					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("NUM_NOTA_CREDITO"))) : null);
					
					if(rs.getString("FEC_NOTA_CREDITO")!= null){
						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());
						ordenCompra.setFecNotaCredito(timestamp);
					}
					if(rs.getString("HORA_NOTA_CREDITO")!= null){
						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
						ordenCompra.setHoraNotaCredito(timestamp);
					}		
					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("NUM_CAJA_NOTA_CREDITO"))) : null);
					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("CORRELATIVO_NOTA_CREDITO"))): null);
					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("RUT_NOTA_CREDITO")) : null);
					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("TIPO_DOC"))) : null);
					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("FOLIO_NC_SII"))) : null);
					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					ordenCompra.setUsuario(rs.getString("USUARIO"));
					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					
					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Long.parseLong(Util.quitarEspacios(rs.getString("RUT_CLIENTE"))) : null);
					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? (rs.getString("TELEFONO_CLIENTE")) : null);
					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("COMUNA_CLIENTE"))) : null);
					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(Util.quitarEspacios(rs.getString("REGION_CLIENTE"))) : null);
					ordenCompra.setDireccionDespacho(rs.getString("DIRECCION_CLIENTE") != null ? rs.getString("DIRECCION_CLIENTE") : null);
			}
		}
		catch (Exception e) {
		logger.traceError("cargarOrdenOC", e);
		throw new AligareException("Error en cargarOrdenOC: " + e);
		} finally {
                    PoolBDs.closeResultSet(rs);
                    PoolBDs.closeStatement(cs);
		PoolBDs.closeConnection(conn);
		logger.endTrace("cargarOrdenOC", "Finalizado", "ordenCompra: "+((ordenCompra!=null)?""+ordenCompra:"null"));
		}
		
		
		return ordenCompra;
	}
	/**
	 * actualizarPorNotaCredito es la funcion que permite actualizar las notas de 
	 * venta en el proceso de generacion de notas de credito
	 * 
	 * @param correlativoVenta 			numero identificador de note de venta
	 * @param numeroNotaCredito			numero de nota de credito
	 * @param folioNcSII				folioNcSII
	 * @param fechaNotaCredito			fecha nota credito
	 * @param horaNotaCredito			hora nota credito
	 * @param numCajaNotaCredito		numero de caja nota credito
	 * @param correlativoNotaCredito	correlativo identificador de nota de credito
	 * @param rutNotaCredito			rutNotaCredito
	 * @param estado					estado
	 * @param
	 * @return Integer					retorno de actualizacion
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */
	@Override
	public Integer actualizarPorNotaCredito(Integer correlativoVenta, 
											Integer numeroNotaCredito, 
											Integer folioNcSII,
											Timestamp fechaNotaCredito, 
											Timestamp horaNotaCredito, 
											Integer numCajaNotaCredito,
											Integer correlativoNotaCredito, 
											Integer rutNotaCredito, 
											Integer estado) {

		return null;
	}

	@Override
	public ConcurrentLinkedQueue<NotaVentaMail> obtenerOrdenesParaEnvioMail(Integer nroCaja, Integer nroSucusal, Integer nroEstado) {

		logger.initTrace("obtenerOrdenesParaEnvioMail", "Integer nroCaja: " + nroCaja + " - Integer nroSucusal: "
				+ nroSucusal + " - Integer nroEstado: " + nroEstado, new KeyLog("Numero Caja", nroCaja+Constantes.VACIO), new KeyLog("Numero sucursal", nroSucusal+Constantes.VACIO), new KeyLog("Estado", nroEstado+Constantes.VACIO));

		ConcurrentLinkedQueue<NotaVentaMail> listado = new ConcurrentLinkedQueue<NotaVentaMail>();
		String inTabla = "";
		
		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
		
		if (nroEstado.intValue() == 2){
			inTabla = "HORA_BOLETA";
		} else {
			inTabla = "HORA_NOTA_CREDITO";
		}
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_MAIL_OBT_ORDENES(?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				
				if (nroCaja != null){
					cs.setInt(1, nroCaja);
				}
				else{
					cs.setInt(1, new Integer(Constantes.NRO_MENOSUNO));
				}				
				
				cs.setInt(2, nroSucusal);
				cs.setInt(3, nroEstado);
				cs.setString(4, inTabla);
				cs.registerOutParameter(5, OracleTypes.CLOB);
				cs.registerOutParameter(6, OracleTypes.CURSOR);
				cs.registerOutParameter(7, OracleTypes.NUMBER);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerOrdenesParaEnvioMail", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_MAIL_OBT_ORDENES("+((nroCaja != null)?nroCaja:Constantes.NRO_MENOSUNO)+","+nroSucusal+","+nroEstado+",'"+inTabla+"',outc,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(6);

				if (rs != null) {
					//logger.traceInfo("obtenerOrdenesParaEnvioMail", "Query: " + cs.getString(5));
					while (rs.next()) {
						NotaVenta notaVenta = new NotaVenta();
						notaVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						notaVenta.setMinutosCompra(rs.getInt("MINUTOS"));
						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
						notaVenta.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						notaVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						notaVenta.setRutComprador(Validaciones.validaLongCeroXNull(rs.getLong("RUT_COMPRADOR")));
						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						notaVenta.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						notaVenta.setNumeroBoleta(Validaciones.validaLongCeroXNull(rs.getLong("NUMERO_BOLETA")));
						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						notaVenta.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						//notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
						notaVenta.setNumNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("NUM_NOTA_CREDITO")));
						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						notaVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
						notaVenta.setFolioNcSii(Validaciones.validaLongCeroXNull(rs.getLong("FOLIO_NC_SII")));
						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
						notaVenta.setUsuario(rs.getString("USUARIO"));
						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));

						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TIPO_TARJETA")));
						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
						tarjetaBancaria.setMontoTarjeta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_TARJETA")));
						tarjetaBancaria.setVd(rs.getString("VD"));
						tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
						tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
						tarjetaBancaria.setCodigoConvenio(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));
						
						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));

						TipoDoc tipoDoc = new TipoDoc();
						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

						Vendedor vendedor = new Vendedor();
						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));

						List<ArticuloVentaTramaDTE> articuloVentas = generacionDTE.obtenerArticuloVentas(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						
						NotaVentaMail tramaDTE = new NotaVentaMail();
						tramaDTE.setNotaVenta(notaVenta);
						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
						tramaDTE.setDatoFacturacion(datoFacturacion);
						tramaDTE.setDespacho(despacho);
						tramaDTE.setTipoDoc(tipoDoc);
						tramaDTE.setArticuloVentas(articuloVentas);
						listado.add(tramaDTE);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerOrdenesParaEnvioMail", "Error en obtenerOrdenesParaEnvioMail: ", e);
				logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerOrdenesParaEnvioMail", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado",
				"ConcurrentLinkedQueue<NotaVentaMail>.size: " + ((listado != null) ? listado.size() : Constantes.NRO_CERO));
		return listado;
	}

	@Override
	public boolean actualizarNotaVentaCnUrlPPL(Long correlativoVenta, String urlPPL, String nombreColumna) {
		logger.initTrace("actualizarNotaVentaCnUrlPPL", "Integer correlativoVenta: " + correlativoVenta + " String urlPPL: "+urlPPL + " String nombreColumna: "+nombreColumna, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
		boolean respuesta = Constantes.FALSO;
		int rsOut = Constantes.NRO_UNO;
		String rsMsjOut = Constantes.VACIO;
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {
				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_UPDATE_MAIL_FOLIO(?,?,?,?,?)}";
				logger.traceInfo("actualizarNotaVentaCnUrlPPL", "Actualiza Nota Venta SP[MODEL_EXTEND]: CAVIRA_MANEJO_BOLETAS.PROC_UPDATE_MAIL_FOLIO("+correlativoVenta+",'"+urlPPL+"','"+nombreColumna+"',outCode,outMsj)", new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				cs = conn.prepareCall(procSQL);
				cs.setLong(1, correlativoVenta);
				cs.setString(2, urlPPL);
				cs.setString(3, nombreColumna);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.execute();
				rsOut = cs.getInt(4);
				rsMsjOut = cs.getString(5);
				logger.traceInfo("actualizarNotaVentaCnUrlPPL", "Resultados de CAVIRA_MANEJO_BOLETAS.PROC_UPDATE_MAIL_FOLIO: outCode: "+rsOut+" outMsj: "+rsMsjOut, new KeyLog("Correlativo venta", correlativoVenta+Constantes.VACIO));
				respuesta = (rsOut == Constantes.NRO_CERO);
			} catch (Exception e) {
				logger.traceError("actualizarNotaVentaCnUrlPPL", "Error al actualizar nota de venta en SP CAVIRA_MANEJO_BOLETAS.PROC_UPDATE_MAIL_FOLIO: ", e);
				respuesta = Constantes.FALSO;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("actualizarNotaVentaCnUrlPPL", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("actualizarNotaVentaCnUrlPPL", "Finalizado", "boolean: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("actualizarNotaVentaCnUrlPPL", "Finalizado", "boolean: " + String.valueOf(respuesta));

		return respuesta;

	}

	@Override
	public ConcurrentLinkedQueue<NotaVentaMail> obtenerOrdenesParaEnvioMailTransac(Integer nroCaja, Integer nroSucusal, Integer nroEstado) {

		logger.initTrace("obtenerOrdenesParaEnvioMail", "Integer nroCaja: " + nroCaja + " - Integer nroSucusal: "
				+ nroSucusal + " - Integer nroEstado: " + nroEstado, new KeyLog("Numero Caja", nroCaja+Constantes.VACIO), new KeyLog("Numero sucursal", nroSucusal+Constantes.VACIO), new KeyLog("Estado", nroEstado+Constantes.VACIO));

		ConcurrentLinkedQueue<NotaVentaMail> listado = new ConcurrentLinkedQueue<NotaVentaMail>();
		String inTabla = "";
		
		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
		
		if (nroEstado.intValue() == 2){
			inTabla = "HORA_BOLETA";
		} else {
			inTabla = "HORA_NOTA_CREDITO";
		}
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_MAIL_OBT_ORDENES_TRANSAC(?,?,?,?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				
				if (nroCaja != null){
					cs.setInt(1, nroCaja);
				}
				else{
					cs.setInt(1, new Integer(Constantes.NRO_MENOSUNO));
				}				
				
				cs.setInt(2, nroSucusal);
				cs.setInt(3, nroEstado);
				cs.setString(4, inTabla);
				cs.registerOutParameter(5, OracleTypes.CLOB);
				cs.registerOutParameter(6, OracleTypes.CURSOR);
				cs.registerOutParameter(7, OracleTypes.NUMBER);
				cs.registerOutParameter(8, OracleTypes.VARCHAR);
				logger.traceInfo("obtenerOrdenesParaEnvioMail", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_MAIL_OBT_ORDENES("+((nroCaja != null)?nroCaja:Constantes.NRO_MENOSUNO)+","+nroSucusal+","+nroEstado+",'"+inTabla+"',outc,outc,outn,outv)}");
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(6);

				if (rs != null) {
					//logger.traceInfo("obtenerOrdenesParaEnvioMail", "Query: " + cs.getString(5));
					while (rs.next()) {
						NotaVenta notaVenta = new NotaVenta();
						notaVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						notaVenta.setMinutosCompra(rs.getInt("MINUTOS"));
						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
						notaVenta.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						notaVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						notaVenta.setRutComprador(Validaciones.validaLongCeroXNull(rs.getLong("RUT_COMPRADOR")));
						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						notaVenta.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						notaVenta.setNumeroBoleta(Validaciones.validaLongCeroXNull(rs.getLong("NUMERO_BOLETA")));
						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						notaVenta.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						//notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
						notaVenta.setNumNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("NUM_NOTA_CREDITO")));
						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						notaVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
						notaVenta.setFolioNcSii(Validaciones.validaLongCeroXNull(rs.getLong("FOLIO_NC_SII")));
						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
						notaVenta.setUsuario(rs.getString("USUARIO"));
						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));

						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("TIPO_TARJETA")));
						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
						tarjetaBancaria.setMontoTarjeta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_TARJETA")));
						tarjetaBancaria.setVd(rs.getString("VD"));
						tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
						tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
						tarjetaBancaria.setCodigoConvenio(Validaciones.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));
						
						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));

						TipoDoc tipoDoc = new TipoDoc();
						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

						Vendedor vendedor = new Vendedor();
						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));

						List<ArticuloVentaTramaDTE> articuloVentas = generacionDTE.obtenerArticuloVentas(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						
						NotaVentaMail tramaDTE = new NotaVentaMail();
						tramaDTE.setNotaVenta(notaVenta);
						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
						tramaDTE.setDatoFacturacion(datoFacturacion);
						tramaDTE.setDespacho(despacho);
						tramaDTE.setTipoDoc(tipoDoc);
						tramaDTE.setArticuloVentas(articuloVentas);
						listado.add(tramaDTE);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerOrdenesParaEnvioMail", "Error en obtenerOrdenesParaEnvioMail: ", e);
				logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerOrdenesParaEnvioMail", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado",
				"ConcurrentLinkedQueue<NotaVentaMail>.size: " + ((listado != null) ? listado.size() : Constantes.NRO_CERO));
		return listado;
	}

	/**
	 * Sobrecarga de metodo obtenerOrdendesParaEnvio de Mail
	 */
	@Override
	public NotaVentaMail obtenerOrdenesParaEnvioMailTransac(Long correlativoVenta) {

		logger.initTrace("obtenerOrdenesParaEnvioMail", "Integer correlativoVenta: " + correlativoVenta);

		
		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
		NotaVentaMail tramaDTE = null;

		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;

			try {

				String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_MAIL_OBT_ORDENES_TRANSAC(?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				
				cs.setLong(1, correlativoVenta);
				cs.registerOutParameter(2, OracleTypes.CLOB);//6
				cs.registerOutParameter(3, OracleTypes.CURSOR);//7
				cs.registerOutParameter(4, OracleTypes.NUMBER);//8
				cs.registerOutParameter(5, OracleTypes.VARCHAR);//9
				cs.execute();
				ResultSet rs = (ResultSet) cs.getObject(3);

				if (rs != null) {
					while (rs.next()) {
						NotaVenta notaVenta = new NotaVenta();
						notaVenta.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
						notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
						notaVenta.setEstado(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ESTADO")));
						notaVenta.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
						notaVenta.setMontoDescuento(Validaciones.validaBigDecimalMenosUnoXNull(rs.getBigDecimal("MONTO_DESCUENTO")));
						notaVenta.setTipoDescuento(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESCUENTO")));
						notaVenta.setRutComprador(Validaciones.validaLongCeroXNull(rs.getLong("RUT_COMPRADOR")));
						notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
						notaVenta.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
						notaVenta.setCodigoRegalo(Validaciones.validaLongCeroXNull(rs.getLong("CODIGO_REGALO")));
						notaVenta.setTipoRegalo(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_REGALO")));
						notaVenta.setNumeroBoleta(Validaciones.validaLongCeroXNull(rs.getLong("NUMERO_BOLETA")));
						notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
						notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
						notaVenta.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
						notaVenta.setCorrelativoBoleta(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_BOLETA")));
						notaVenta.setRutBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_BOLETA")));
						notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
						notaVenta.setNumNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("NUM_NOTA_CREDITO")));
						notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
						notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
						notaVenta.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("NUM_CAJA_NOTA_CREDITO")));
						notaVenta.setCorrelativoNotaCredito(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_NOTA_CREDITO")));
						notaVenta.setRutNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_NOTA_CREDITO")));
						notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
						notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
						notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
						notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
						notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
						notaVenta.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("NOTAVENTA_TIPODOC")));
						notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
						notaVenta.setFolioNcSii(Validaciones.validaLongCeroXNull(rs.getLong("FOLIO_NC_SII")));
						notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
						notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
						notaVenta.setUsuario(rs.getString("USUARIO"));
						notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));

						TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
						tarjetaBancaria.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
						tarjetaBancaria.setTipoTarjeta(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ID_CANAL")));
						tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
						tarjetaBancaria.setMontoTarjeta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_TARJETA")));
						//tarjetaBancaria.setVd(rs.getString("VD"));
						
						DatoFacturacion datoFacturacion = new DatoFacturacion();
						datoFacturacion.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
						datoFacturacion.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("FACT_ADDRESS_ID")));
						datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
						datoFacturacion.setComunaFacturaDes(Util.getVacioPorNulo(rs.getString("COMUNA_FACTURA_DES")));
						datoFacturacion.setCiudadFacturaDes(Util.getVacioPorNulo(rs.getString("CIUDAD_FACTURA_DES")));
						datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
						datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
						datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
						datoFacturacion.setGiroFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("GIRO_FACTURA_ID")));
						datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
						datoFacturacion.setRegionFacturaId(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURA_ID")));
						datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

						Despacho despacho = new Despacho();
						despacho.setCorrelativoVenta(Validaciones.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
						despacho.setRutDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("RUT_DESPACHO")));
						despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
						despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
						despacho.setSucursalDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("SUCURSAL_DESPACHO")));
						despacho.setComunaDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_DESPACHO")));
						despacho.setRegionDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_DESPACHO")));
						despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
						despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
						despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
						despacho.setObservacion(rs.getString("OBSERVACION"));
						despacho.setRutCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_RUT_CLIENTE")));
						despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
						despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
						despacho.setTipoCliente(Validaciones.validaIntegerCeroXNull(rs.getInt("DESPACHO_TIPO_CLIENTE")));
						despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
						despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
						despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
						despacho.setTipoDespacho(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DESPACHO")));
						despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
						despacho.setCodigoRegalo(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("DESPACHO_CODIGO_REGALO")));
						despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
						despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
						despacho.setRegionFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("REGION_FACTURACION")));
						despacho.setComunaFacturacion(Validaciones.validaIntegerCeroXNull(rs.getInt("COMUNA_FACTURACION")));
						despacho.setEnvioDte(Validaciones.validaIntegerCeroXNull(rs.getInt("ENVIO_DTE")));
						despacho.setAddressId(Validaciones.validaIntegerCeroXNull(rs.getInt("ADDRESS_ID")));
						despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
						despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));

						TipoDoc tipoDoc = new TipoDoc();
						tipoDoc.setTipoDoc(Validaciones.validaIntegerCeroXNull(rs.getInt("TIPO_DOC")));
						tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
						tipoDoc.setDocOrigen(Validaciones.validaIntegerCeroXNull(rs.getInt("DOC_ORIGEN")));
						tipoDoc.setCodBo(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_BO")));
						tipoDoc.setCodPpl(Validaciones.validaIntegerCeroXNull(rs.getInt("COD_PPL")));
						tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

						Vendedor vendedor = new Vendedor();
						vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
						vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR"));

						List<ArticuloVentaTramaDTE> articuloVentas = generacionDTE.obtenerArticuloVentas(Validaciones.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
						
						tramaDTE = new NotaVentaMail();
						tramaDTE.setNotaVenta(notaVenta);
						tramaDTE.setTarjetaBancaria(tarjetaBancaria);
						tramaDTE.setDatoFacturacion(datoFacturacion);
						tramaDTE.setDespacho(despacho);
						tramaDTE.setTipoDoc(tipoDoc);
						tramaDTE.setArticuloVentas(articuloVentas);
					}
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("obtenerOrdenesParaEnvioMail", "Error en obtenerOrdenesParaEnvioMail: ", e);
				logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: null");
				return null;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("obtenerOrdenesParaEnvioMail", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado", "ConcurrentLinkedQueue<NotaVentaMail>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		logger.endTrace("obtenerOrdenesParaEnvioMail", "Finalizado","");
		return tramaDTE;
	}


	@Override
	public List<Long> obtenerNumerosOCsByEstado(Integer caja, Integer cantidadOCs, Integer estado) {
		logger.initTrace("obtenerNumerosOCsByEstado", 
				"Integer caja: " + String.valueOf(caja) 
				+ " - Integer cantidadOCs: " + String.valueOf(cantidadOCs)
				+ " - Integer estado: " + String.valueOf(estado), 
				new KeyLog("Caja", String.valueOf(caja)), 
				new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)),
				new KeyLog("Estado", String.valueOf(estado)));

		List<Long> listado = new ArrayList<Long>(cantidadOCs);
		
		try (Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND)) {
			
			if (conn != null) {
				PreparedStatement ps = null;
				
				String sql = "SELECT CORRELATIVO_VENTA FROM NOTA_VENTA WHERE ESTADO = ? AND NUMERO_CAJA = ? AND ROWNUM <= ? ORDER BY CORRELATIVO_VENTA ASC";
				
				logger.traceInfo("obtenerNumerosOCsByEstado", "SQL Ejecutado: " + sql + "  ------------ Parametros", 
						new KeyLog("ESTADO", String.valueOf(estado)),
						new KeyLog("NRO_CAJA", String.valueOf(caja)),
						new KeyLog("ROWNUM (cantidadOCs)", String.valueOf(cantidadOCs)));
				
				ps = conn.prepareStatement(sql);
				
				ps.setInt(1, estado);
				ps.setInt(2, caja);
				ps.setInt(3, cantidadOCs);
				
				ResultSet rs = ps.executeQuery();
				
				//Agrego los numeros de OC al listado
				while(rs.next()) {
					
					listado.add(rs.getLong(1));
					
				}

			}
			
		} catch (SQLException e) {
			logger.traceError("obtenerNumerosOCsByEstado", e);
			logger.endTrace("obtenerNumerosOCsByEstado", "Finalizado OK Lista Nula", null);
			return null;
		}

		
		logger.endTrace("obtenerNumerosOCsByEstado", "Finalizado OK", null);
		
		return listado;
	}


	@Override
	public boolean asignarCaja(Integer caja, Long oc) {
		logger.initTrace("asignarCaja", 
				"Integer caja: " + String.valueOf(caja) 
				+ " - Long oc: " + String.valueOf(oc),
				new KeyLog("Caja", String.valueOf(caja)), 
				new KeyLog("OC", String.valueOf(oc)));

		boolean resp = Boolean.TRUE;
		
		try (Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND)) {
			
			if (conn != null) {
				PreparedStatement ps = null;
				
				String sql = "UPDATE NOTA_VENTA SET NUMERO_CAJA = ? WHERE CORRELATIVO_VENTA = ?";
				
				logger.traceInfo("asignarCaja", "SQL Ejecutado: " + sql + "  ------------ Parametros", 
						new KeyLog("NRO_CAJA", String.valueOf(caja)),
						new KeyLog("CORRELATIVO_VENTA", String.valueOf(oc)));
				
				ps = conn.prepareStatement(sql);
				
				ps.setInt(1, caja);
				ps.setLong(2, oc);
				
				resp = ps.executeUpdate() > 0;

			}
			
		} catch (SQLException e) {
			logger.traceError("asignarCaja", e);
			logger.endTrace("asignarCaja", "Finalizado OK Lista Nula", null);
			return Boolean.FALSE;
		}

		
		logger.endTrace("asignarCaja", "Finalizado OK", null);
		
		return resp;
	}
	
	
}
