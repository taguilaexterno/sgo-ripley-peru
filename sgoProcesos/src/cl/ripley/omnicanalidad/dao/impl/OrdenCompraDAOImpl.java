package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVenta;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.DatoFacturacion;
import cl.ripley.omnicanalidad.bean.Despacho;
import cl.ripley.omnicanalidad.bean.DocumentoElectronico;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.MotivoRechazo;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.NotaVentaRechazo;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.TarjetaBancaria;
import cl.ripley.omnicanalidad.bean.TarjetaRegaloEmpresa;
import cl.ripley.omnicanalidad.bean.TarjetaRipley;
import cl.ripley.omnicanalidad.bean.TipoDespacho;
import cl.ripley.omnicanalidad.bean.TipoDoc;
import cl.ripley.omnicanalidad.bean.Validacion;
import cl.ripley.omnicanalidad.bean.Vendedor;
import cl.ripley.omnicanalidad.bean.XValidacionOc;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

@Repository
public class OrdenCompraDAOImpl implements OrdenCompraDAO {

	private static final AriLog logger = new AriLog(OrdenCompraDAOImpl.class,"RIP16-004", PlataformaType.JAVA);

	public List<NotaVenta> cargaOrdenesCompraCajaTransito(String nroCajaTransito, String volumen, String estadosIniciales){
		
		logger.initTrace("cargaOrdenesCompraCajaTransito", "String: "+nroCajaTransito+", String: "+volumen+", String: "+estadosIniciales);
		
//		Connection conn = null;
//		CallableStatement cs = null;
//		Date parsedDate = null;
//		Timestamp timestamp = null;
		List<NotaVenta> listaOrdenCompra = null;
//		try {
//			
//			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
//			
//			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
//			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_ORDENES_COMPRA_EVAL(?,?,?,?,?,?)}";
//			cs = conn.prepareCall(procSQL);
//			cs.setInt(1, Integer.parseInt(nroCajaTransito));
//			cs.setInt(2, Integer.parseInt(volumen));
//			cs.setString(3, estadosIniciales);
//			cs.registerOutParameter(4, OracleTypes.CURSOR);
//			cs.registerOutParameter(5, OracleTypes.NUMBER);
//			cs.registerOutParameter(6, OracleTypes.VARCHAR);
//			
//			cs.execute();    
//			ResultSet rs = (ResultSet) cs.getObject(4);
//			if(rs != null){
//				listaOrdenCompra = new ArrayList<NotaVenta>();
//				while (rs.next()) {
//					NotaVenta ordenCompra = new NotaVenta();
//					ordenCompra.setCorrelativoVenta(Integer.parseInt(rs.getString("CORRELATIVO_VENTA")));
//					
//					parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
//					timestamp = new java.sql.Timestamp(parsedDate.getTime());
//					ordenCompra.setFechaBoleta(timestamp);
//					
//					ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
//					ordenCompra.setMontoVenta(Integer.parseInt(rs.getString("MONTO_VENTA")));
//					ordenCompra.setMontoDescuento(Integer.parseInt(rs.getString("MONTO_DESCUENTO")));
//					ordenCompra.setTipoDescuento(Integer.parseInt(rs.getString("TIPO_DESCUENTO")));
//					ordenCompra.setRutComprador(Integer.parseInt(rs.getString("RUT_COMPRADOR")));
//					ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
//					ordenCompra.setNumeroSucursal(Integer.parseInt(rs.getString("NUMERO_SUCURSAL")));
//					ordenCompra.setCodigoRegalo(Integer.parseInt(rs.getString("CODIGO_REGALO")));
//					ordenCompra.setTipoRegalo(Integer.parseInt(rs.getString("TIPO_REGALO")));
//					ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Integer.parseInt(rs.getString("NUMERO_BOLETA")) : null  );
//					
//					if(rs.getString("FECHA_BOLETA") != null){
//						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFechaBoleta(timestamp);
//					}
//					
//					if(rs.getString("HORA_BOLETA")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraBoleta(timestamp);
//					}
//					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(rs.getString("NUMERO_CAJA")): null);
//					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ? Integer.parseInt(rs.getString("CORRELATIVO_BOLETA")) : null);
//					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(rs.getString("RUT_BOLETA")) : null);
//					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
//					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_NOTA_CREDITO")) : null);
//					
//					if(rs.getString("FEC_NOTA_CREDITO")!= null){
//						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFecNotaCredito(timestamp);
//					}
//					if(rs.getString("HORA_NOTA_CREDITO")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraNotaCredito(timestamp);
//					}		
//					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_CAJA_NOTA_CREDITO")) : null);
//					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("CORRELATIVO_NOTA_CREDITO")): null);
//					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("RUT_NOTA_CREDITO")) : null);
//					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
//					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
//					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
//					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
//					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
//					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(rs.getString("TIPO_DOC")) : null);
//					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
//					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Integer.parseInt(rs.getString("FOLIO_NC_SII")) : null);
//					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
//					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
//					ordenCompra.setUsuario(rs.getString("USUARIO"));
//					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//					
//					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
//					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Integer.parseInt(rs.getString("RUT_CLIENTE")) : null);
//					//ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? Integer.parseInt(rs.getString("TELEFONO_CLIENTE")) : null);
//					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? rs.getString("TELEFONO_CLIENTE") : null);
//					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
//					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(rs.getString("COMUNA_CLIENTE")) : null);
//					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(rs.getString("REGION_CLIENTE")) : null);
//					
//					listaOrdenCompra.add(ordenCompra);
//	            }
//			}	
//		} catch (Exception e) {
//			logger.traceError("cargaOrdenesCompraCajaTransito", e);
//			throw new AligareException("Error en cargaOrdenesCompraCajaTransito: " + e);
//		} finally {
//			PoolBDs.closeConnection(conn);
//			logger.endTrace("cargaOrdenesCompraCajaTransito", "Finalizado", "listaOrdenCompra: "+((listaOrdenCompra != null)?""+listaOrdenCompra.size():"null"));
//		}
		return listaOrdenCompra;
	}

	public List<NotaVenta> cargaOrdenesCompraCajaTransitoSeguras(	String nroCajaTransito, 
																	String volumen) {
		
		logger.initTrace("cargaOrdenesCompraCajaTransitoSeguras", "String: "+nroCajaTransito+", String: "+volumen);
		
//		Connection conn = null;
//		CallableStatement cs = null;
//		Date parsedDate = null;
//		Timestamp timestamp = null;
		List<NotaVenta> listaOrdenCompra = null;
//		try {
//			
//			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//			SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
//			
//			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
//			String procSQL = "{CALL CAVIRA_ORDEN_COMPRA.PROC_CARGA_NVENTA_VALIDAS(?,?,?,?,?)}";
//			cs = conn.prepareCall(procSQL);
//			cs.setInt(1, Integer.parseInt(nroCajaTransito));
//			cs.setInt(2, Integer.parseInt(volumen));
//			cs.registerOutParameter(3, OracleTypes.CURSOR);
//			cs.registerOutParameter(4, OracleTypes.NUMBER);
//			cs.registerOutParameter(5, OracleTypes.VARCHAR);
//			
//			cs.execute();    
//			ResultSet rs = (ResultSet) cs.getObject(3);
//			if(rs != null){
//				listaOrdenCompra = new ArrayList<NotaVenta>();
//				while (rs.next()) {
//					NotaVenta ordenCompra = new NotaVenta();
//					ordenCompra.setCorrelativoVenta(Integer.parseInt(rs.getString("CORRELATIVO_VENTA")));
//					
//					parsedDate = dateFormat.parse(rs.getString("FECHA_CREACION"));
//					timestamp = new java.sql.Timestamp(parsedDate.getTime());
//					ordenCompra.setFechaBoleta(timestamp);
//					
//					ordenCompra.setEstado(Integer.parseInt(rs.getString("ESTADO")));
//					ordenCompra.setMontoVenta(Integer.parseInt(rs.getString("MONTO_VENTA")));
//					ordenCompra.setMontoDescuento(Integer.parseInt(rs.getString("MONTO_DESCUENTO")));
//					ordenCompra.setTipoDescuento(Integer.parseInt(rs.getString("TIPO_DESCUENTO")));
//					ordenCompra.setRutComprador(Integer.parseInt(rs.getString("RUT_COMPRADOR")));
//					ordenCompra.setDvComprador(rs.getString("DV_COMPRADOR"));
//					ordenCompra.setNumeroSucursal(Integer.parseInt(rs.getString("NUMERO_SUCURSAL")));
//					ordenCompra.setCodigoRegalo(Integer.parseInt(rs.getString("CODIGO_REGALO")));
//					ordenCompra.setTipoRegalo(Integer.parseInt(rs.getString("TIPO_REGALO")));
//					ordenCompra.setNumeroBoleta(rs.getString("NUMERO_BOLETA") != null ? Integer.parseInt(rs.getString("NUMERO_BOLETA")) : null  );
//					
//					if(rs.getString("FECHA_BOLETA") != null){
//						parsedDate = dateFormat.parse(rs.getString("FECHA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFechaBoleta(timestamp);
//					}
//					
//					if(rs.getString("HORA_BOLETA")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_BOLETA"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraBoleta(timestamp);
//					}
//					ordenCompra.setNumeroCaja(rs.getString("NUMERO_CAJA") != null ? Integer.parseInt(rs.getString("NUMERO_CAJA")): null);
//					ordenCompra.setCorrelativoBoleta(rs.getString("CORRELATIVO_BOLETA") != null ? Integer.parseInt(rs.getString("CORRELATIVO_BOLETA")) : null);
//					ordenCompra.setRutBoleta(rs.getString("RUT_BOLETA") != null ? Integer.parseInt(rs.getString("RUT_BOLETA")) : null);
//					ordenCompra.setDvBoleta(rs.getString("DV_BOLETA"));
//					ordenCompra.setNumNotaCredito(rs.getString("NUM_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_NOTA_CREDITO")) : null);
//					
//					if(rs.getString("FEC_NOTA_CREDITO")!= null){
//						parsedDate = dateFormat.parse(rs.getString("FEC_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());
//						ordenCompra.setFecNotaCredito(timestamp);
//					}
//					if(rs.getString("HORA_NOTA_CREDITO")!= null){
//						parsedDate = hourFormat.parse(rs.getString("HORA_NOTA_CREDITO"));
//						timestamp = new java.sql.Timestamp(parsedDate.getTime());					
//						ordenCompra.setHoraNotaCredito(timestamp);
//					}		
//					ordenCompra.setNumCajaNotaCredito(rs.getString("NUM_CAJA_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("NUM_CAJA_NOTA_CREDITO")) : null);
//					ordenCompra.setCorrelativoNotaCredito(rs.getString("CORRELATIVO_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("CORRELATIVO_NOTA_CREDITO")): null);
//					ordenCompra.setRutNotaCredito(rs.getString("RUT_NOTA_CREDITO") != null ? Integer.parseInt(rs.getString("RUT_NOTA_CREDITO")) : null);
//					ordenCompra.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
//					ordenCompra.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
//					ordenCompra.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
//					ordenCompra.setOptcounter(rs.getString("OPTCOUNTER") != null ? Integer.parseInt(rs.getString("OPTCOUNTER")) : null);
//					ordenCompra.setOrigenVta(rs.getString("ORIGEN_VTA"));
//					ordenCompra.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
//					ordenCompra.setTipoDoc(rs.getString("TIPO_DOC") != null ? Integer.parseInt(rs.getString("TIPO_DOC")) : null);
//					ordenCompra.setFolioSii(rs.getString("FOLIO_SII"));
//					ordenCompra.setFolioNcSii(rs.getString("FOLIO_NC_SII") != null ? Integer.parseInt(rs.getString("FOLIO_NC_SII")) : null);
//					ordenCompra.setUrlDoce(rs.getString("URL_DOCE"));
//					ordenCompra.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
//					ordenCompra.setUsuario(rs.getString("USUARIO"));
//					ordenCompra.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
//					
//					ordenCompra.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
//					ordenCompra.setRutCliente(rs.getString("RUT_CLIENTE") != null ? Integer.parseInt(rs.getString("RUT_CLIENTE")) : null);
//					//ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? Integer.parseInt(rs.getString("TELEFONO_CLIENTE")) : null);
//					ordenCompra.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE") != null ? rs.getString("TELEFONO_CLIENTE") : null);
//					ordenCompra.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
//					ordenCompra.setComunaDespacho(rs.getString("COMUNA_CLIENTE") != null ? Integer.parseInt(rs.getString("COMUNA_CLIENTE")) : null);
//					ordenCompra.setRegionDespacho(rs.getString("REGION_CLIENTE") != null ? Integer.parseInt(rs.getString("REGION_CLIENTE")) : null);
//					
//					listaOrdenCompra.add(ordenCompra);
//	            }
//			}	
//		} catch (Exception e) {
//			logger.traceError("cargaOrdenesCompraCajaTransitoSeguras", e);
//			throw new AligareException("Error en cargaOrdenesCompraCajaTransitoSeguras: " + e);
//		} finally {
//			PoolBDs.closeConnection(conn);
//			logger.endTrace("cargaOrdenesCompraCajaTransitoSeguras", "Finalizado", "listaOrdenCompra: "+((listaOrdenCompra != null)?""+listaOrdenCompra.size():"null"));
//		}
		return listaOrdenCompra;
	}

	@Override
	public List<ArticulosVentaOC> getArticulosByOC(Long correlativoOC) {
		logger.initTrace("getArticulosByOC", "Ingreso a metodo; correlativoOC=" + correlativoOC);
		
		List<ArticulosVentaOC> articulos = new ArrayList<ArticulosVentaOC>(20);
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;
		
		try {
			cs = conn.prepareCall("{call SGO_MANEJO_OC.PROC_GET_ARTICULOS_OC(?,?,?,?,?)}");
			cs.setLong(1, correlativoOC);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			cs.execute();
			
			String query = cs.getString(3);
			BigDecimal cod = cs.getBigDecimal(4);
			String mensaje = cs.getString(5);
			
			logger.traceInfo("getArticulosByOC", "query: " + query);
			logger.traceInfo("getArticulosByOC", "cod: " + cod);
			logger.traceInfo("getArticulosByOC", "mensaje: " + mensaje);
			
			if(cod.intValue() != 0) {
				throw new Exception(mensaje);
			}
			
			ResultSet rs = (ResultSet) cs.getObject(2);
			
			while(rs != null && rs.next()) {
				ArticulosVentaOC articulosVenta = new ArticulosVentaOC();
				
				ArticuloVenta a = new ArticuloVenta();
				a.setCodArticulo(rs.getString("COD_ARTICULO"));
				a.setCodBodega(rs.getString("COD_BODEGA"));
				a.setCodDespacho(rs.getString("COD_DESPACHO"));
				a.setColor(rs.getInt("COLOR"));
				a.setCorrelativoItem(rs.getInt("CORRELATIVO_ITEM"));
				a.setCorrelativoNotaCredito(rs.getLong("CORRELATIVO_NOTA_CREDITO"));
				a.setCorrelativoVenta(rs.getLong("CORRELATIVO_VENTA"));
				a.setCosto(rs.getInt("COSTO"));
				a.setDepto(rs.getString("DEPTO"));
				a.setDeptoGlosa(rs.getString("DEPTO_GLOSA"));
				a.setDescRipley(rs.getString("DESC_RIPLEY"));
				a.setDespachoId(rs.getLong("DESPACHO_ID"));
				a.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
				a.setEsNC(rs.getInt("ES_NC"));
				a.setEsregalo(rs.getString("ESREGALO"));
				a.setEstadoVenta(rs.getString("ESTADO_VENTA"));
				a.setFechaDespacho(rs.getDate("FECHA_DESPACHO"));
				a.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
				a.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
				a.setIndicadorEg(rs.getInt("INDICADOR_EG"));
				a.setMensaje(rs.getString("MENSAJE"));
				a.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
				a.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
				a.setNumNotaCredito(rs.getInt("NUM_NOTA_CREDITO"));
				a.setOrdenMkp(rs.getString("ORDEN_MKP"));
				a.setPrecio(rs.getBigDecimal("PRECIO"));
				a.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
				a.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
				a.setTipoDespacho(rs.getString("TIPO_DESPACHO"));
				a.setTipoPapelRegalo(rs.getString("TIPO_PAPEL_REGALO"));
				a.setTipoRegalo(rs.getInt("TIPO_REGALO"));
				a.setUnidades(rs.getInt("UNIDADES"));
				a.setEsMkp(rs.getInt("ES_MKP"));
				a.setCodigoSunat(rs.getString("CODIGO_SUNAT"));
				
				//Precio * Unidades para construcción de boleta.
				try{
					a.setPrecioPorUnidades(a.getPrecio().multiply(new BigDecimal(a.getUnidades())));
				}
				catch(Exception e){
					logger.traceError("getArticulosByOC", "Error en getArticulosByOC: Precio Por Unidades", e);
				}
				IdentificadorMarketplace i = new IdentificadorMarketplace();
				i.setOrdenMkp(rs.getString("MKP_ORDEN_MKP"));
				i.setRutProveedor(rs.getString("RUT_PROVEEDOR"));
				i.setEstado(Validaciones.validaIntegerCeroXNull(rs.getInt("ESTADO")));
				i.setEsMkp(Validaciones.validaIntegerMenosUnoXNull(rs.getInt("ES_MKP")));
				i.setMontoVenta(Validaciones.validaBigDecimalCeroXNull(rs.getBigDecimal("MONTO_VENTA")));
				i.setNroBoleta(Validaciones.validaIntegerCeroXNull(rs.getInt("NRO_BOLETA")));
				i.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
				i.setNumeroCaja(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_CAJA")));
				i.setNumNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NOTA_CREDITO")));
				i.setNumCajaNotaCredito(Validaciones.validaIntegerCeroXNull(rs.getInt("MKP_NUM_CAJA_NOTA_CREDITO")));
				i.setFecNotaCredito(rs.getTimestamp("MKP_FEC_NOTA_CREDITO"));
				i.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
				i.setNumeroSucursal(Validaciones.validaIntegerCeroXNull(rs.getInt("NUMERO_SUCURSAL")));
				i.setRazonSocial(rs.getString("RAZON_SOCIAL"));

				
				articulosVenta.setArticuloVenta(a);
				articulosVenta.setIdentificadorMarketplace(i);
				
				articulos.add(articulosVenta);
			}
			
		} catch(Exception e) {
			logger.traceError("getArticulosByOC", "Error en getArticulosByOC: ", e);
		} finally {
			PoolBDs.closeConnection(conn);
		}
		} else {
			logger.traceInfo("getArticulosByOC", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("getArticulosByOC", "Finalizado", "List<ArticulosVentaOC>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}
		return articulos;
	}

	@Override
	public List<OrdenesDeCompra> obtenerDatosBoleta(Integer nroCaja, Integer nroSucusal, Long oc) {
		logger.initTrace("obtenerDatosBoleta","nroCaja: "+ nroCaja + "nroSucusal: "+nroSucusal, new KeyLog("Correlativo venta", oc+Constantes.VACIO));
		ArrayList<OrdenesDeCompra> ordenesCompra = new ArrayList<OrdenesDeCompra>();
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			CallableStatement cs = null;
		try {
			String procSQL = "{CALL SGO_MANEJO_OC.PROC_OBTENER_OC_BOLETA(?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			if (nroCaja == null)
				cs.setNull(1, OracleTypes.NUMBER);
			else
				cs.setInt(1, nroCaja);
			
			if (nroSucusal == null)
				cs.setNull(2, OracleTypes.NUMBER);
			else
				cs.setInt(2, nroSucusal);

			if(oc == null)
				cs.setNull(3, OracleTypes.NUMBER);
			else
				cs.setLong(3, oc);
			
			cs.registerOutParameter(4, OracleTypes.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR);
			cs.registerOutParameter(6, OracleTypes.NUMBER);
			cs.registerOutParameter(7, OracleTypes.VARCHAR);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(5);
			logger.traceInfo("obtenerDatosBoleta", "Query: " + ((String) cs.getObject(4)));
			if (rs != null) {
				while (rs.next()) {
					String tipoPago = "";
					
					NotaVenta notaVenta = new NotaVenta();
					notaVenta.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("CORRELATIVO_VENTA")));
					notaVenta.setFechaCreacion(rs.getTimestamp("FECHA_CREACION"));
					notaVenta.setFechaHoraCreacion(rs.getTimestamp("FECHA_HORA_CREACION"));
					notaVenta.setHorasAdministrativas(rs.getInt("HORAS_ADMINISTRATIVAS"));
					notaVenta.setEstado(rs.getInt("ESTADO"));
					notaVenta.setMontoVenta(rs.getBigDecimal("MONTO_VENTA"));
					notaVenta.setMontoDescuento(rs.getBigDecimal("MONTO_DESCUENTO"));
					notaVenta.setTipoDescuento(rs.getInt("TIPO_DESCUENTO"));
					notaVenta.setRutComprador(rs.getLong("RUT_COMPRADOR"));
					notaVenta.setDvComprador(rs.getString("DV_COMPRADOR"));
					notaVenta.setNumeroSucursal(rs.getInt("NUMERO_SUCURSAL"));
					notaVenta.setCodigoRegalo(rs.getLong("CODIGO_REGALO"));
					notaVenta.setTipoRegalo(rs.getInt("TIPO_REGALO"));
					notaVenta.setNumeroBoleta(rs.getLong("NUMERO_BOLETA"));
					notaVenta.setFechaBoleta(rs.getTimestamp("FECHA_BOLETA"));
					notaVenta.setHoraBoleta(rs.getTimestamp("HORA_BOLETA"));
					notaVenta.setNumeroCaja(rs.getInt("NUMERO_CAJA"));
					notaVenta.setCorrelativoBoleta(rs.getLong("CORRELATIVO_BOLETA"));
					notaVenta.setRutBoleta(rs.getInt("RUT_BOLETA"));
					notaVenta.setDvBoleta(rs.getString("DV_BOLETA"));
					notaVenta.setNumNotaCredito(rs.getLong("NUM_NOTA_CREDITO"));
					notaVenta.setFecNotaCredito(rs.getTimestamp("FEC_NOTA_CREDITO"));
					notaVenta.setHoraNotaCredito(rs.getTimestamp("HORA_NOTA_CREDITO"));
					notaVenta.setNumCajaNotaCredito(rs.getInt("NUM_CAJA_NOTA_CREDITO"));
					notaVenta.setCorrelativoNotaCredito(rs.getLong("CORRELATIVO_NOTA_CREDITO"));
					notaVenta.setRutNotaCredito(rs.getInt("RUT_NOTA_CREDITO"));
					notaVenta.setDvNotaCredito(rs.getString("DV_NOTA_CREDITO"));
					notaVenta.setTvnveGlsOreExo(rs.getString("TVNVE_GLS_ORE_EXO"));
					notaVenta.setTipoPromocion(rs.getString("TIPO_PROMOCION"));
					notaVenta.setOrigenVta(rs.getString("ORIGEN_VTA"));
					notaVenta.setEjecutivoVta(rs.getString("EJECUTIVO_VTA"));
					notaVenta.setTipoDoc(rs.getInt("NOTAVENTA_TIPODOC"));
					notaVenta.setFolioSii(rs.getString("FOLIO_SII"));
					notaVenta.setFolioNcSii(rs.getLong("FOLIO_NC_SII"));
					notaVenta.setUrlDoce(rs.getString("URL_DOCE"));
					notaVenta.setUrlNotaCredito(rs.getString("URL_NOTA_CREDITO"));
					notaVenta.setUsuario(rs.getString("USUARIO"));
					notaVenta.setIndicadorMkp(rs.getString("INDICADOR_MKP"));
					notaVenta.setNroTrx(rs.getLong("NV_NRO_TRX"));
					
					TarjetaRipley tarjetaRipley = new TarjetaRipley();
					tarjetaRipley.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TR_CORRELATIVO_VENTA")));
					tarjetaRipley.setAdministradora(rs.getInt("ADMINISTRADORA"));
					tarjetaRipley.setEmisor(rs.getInt("EMISOR"));
					tarjetaRipley.setTarjeta(rs.getInt("TARJETA"));
					tarjetaRipley.setRutTitular(rs.getInt("RUT_TITULAR"));
					tarjetaRipley.setRutPoder(rs.getInt("RUT_PODER"));
					tarjetaRipley.setPlazo(rs.getInt("PLAZO"));
					tarjetaRipley.setDiferido(rs.getInt("DIFERIDO"));
					tarjetaRipley.setMontoCapital(rs.getBigDecimal("MONTO_CAPITAL"));
					tarjetaRipley.setMontoPie(rs.getBigDecimal("MONTO_PIE"));
					tarjetaRipley.setDescuentoCar(rs.getBigDecimal("DESCUENTO_CAR"));
					tarjetaRipley.setCodigoDescuento(rs.getInt("CODIGO_DESCUENTO"));
					tarjetaRipley.setPrefijoTitular(rs.getInt("PREFIJO_TITULAR"));
					tarjetaRipley.setDvTitular(rs.getString("DV_TITULAR"));
					tarjetaRipley.setTipoCliente(rs.getInt("TIPO_CLIENTE"));
					tarjetaRipley.setTipoCredito(rs.getInt("TIPO_CREDITO"));
					tarjetaRipley.setPrefijoPoder(rs.getInt("PREFIJO_PODER"));
					tarjetaRipley.setDvPoder(rs.getString("DV_PODER"));
					tarjetaRipley.setValorCuota(rs.getBigDecimal("VALOR_CUOTA"));
					tarjetaRipley.setFechaPrimerVencto(rs.getTimestamp("FECHA_PRIMER_VENCTO"));
					tarjetaRipley.setSernacFinanCae(rs.getInt("SERNAC_FINAN_CAE"));
					tarjetaRipley.setSernacFinanCt(rs.getInt("SERNAC_FINAN_CT"));
					tarjetaRipley.setTvtriMntMntFnd(rs.getInt("TVTRI_MNT_MNT_FND"));
					tarjetaRipley.setTvtriGlsPrjTsaInt(rs.getString("TVTRI_GLS_PRJ_TSA_INT"));
					tarjetaRipley.setTvtriGlsPrjTsaEfe(rs.getString("TVTRI_GLS_PRJ_TSA_EFE"));
					tarjetaRipley.setTvtriCodImpTsaEfe(rs.getInt("TVTRI_COD_IMP_TSA_EFE"));
					tarjetaRipley.setTvtriMntMntEva(rs.getInt("TVTRI_MNT_MNT_EVA"));
					tarjetaRipley.setGlosaFinancieraGsic(rs.getString("GLOSA_FINANCIERA_GSIC"));
					tarjetaRipley.setPan(new BigInteger(rs.getString("PAN").trim()));
					tarjetaRipley.setCodigoCore(rs.getInt("CODIGO_CORE"));
					tarjetaRipley.setCodigoAutorizacion(new BigInteger(rs.getString("CODIGO_AUTORIZACION").trim()));
//					if (tarjetaRipley.getCorrelativoVenta() != null){
//						tipoPago = Constantes.TIPO_PAGO_RIPLEY;
//					}

					TarjetaBancaria tarjetaBancaria = new TarjetaBancaria();
					tarjetaBancaria.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TB_CORRELATIVO_VENTA")));
					tarjetaBancaria.setTipoTarjetaStr(rs.getString("TIPO_TARJETA"));
					tarjetaBancaria.setCodigoAutorizador(rs.getString("CODIGO_AUTORIZADOR"));
					tarjetaBancaria.setMontoTarjeta(rs.getBigDecimal("MONTO_TARJETA"));
					tarjetaBancaria.setDescripcionFormaPago(rs.getString("FORMA_PAGO"));
					tarjetaBancaria.setGlosa(rs.getString("MP_GLOSA"));
					tarjetaBancaria.setBinNumber(rs.getString("MP_BIN"));
					tarjetaBancaria.setUltimosDigitosTarjeta(rs.getString("MP_ULTIMOS_DIGITOS"));
					tarjetaBancaria.setDescripcionCanal(rs.getString("MP_DESC_CANAL"));
					tarjetaBancaria.setIdentificadorTransaccion(Util.validaLongCeroXNull(rs.getLong("MP_IDENT_TRANSAC")));
//					tarjetaBancaria.setVd(rs.getString("VD"));
					//tarjetaBancaria.setMedioAcceso(rs.getString("MEDIO_ACCESO"));
					//tarjetaBancaria.setOneclickBuyorder(rs.getString("ONECLICK_BUYORDER"));
//					if (tarjetaBancaria.getCorrelativoVenta()!= null){
//						if (tarjetaBancaria.getVd() != null){
//							if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_M)){
//								tipoPago = Constantes.TIPO_PAGO_MERCADO_PAGO;
//							}
//							else if (tarjetaBancaria.getVd().equalsIgnoreCase(Constantes.TBK_S)){
//								tipoPago = Constantes.TIPO_PAGO_TBK_DEBITO;
//							} else {
//								tipoPago = Constantes.TIPO_PAGO_TBK_CREDITO;
//							}
//						}
//					}
//					tarjetaBancaria.setCodigoConvenio(Util.validaIntegerCeroXNull(rs.getInt("CODIGO_CONVENIO")));

					TarjetaRegaloEmpresa tarjetaRegaloEmpresa = new TarjetaRegaloEmpresa();
					tarjetaRegaloEmpresa.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("TRE_CORRELATIVO_VENTA")));
					tarjetaRegaloEmpresa.setAdministradora(rs.getInt("TRE_ADMINISTRADORA"));
					tarjetaRegaloEmpresa.setEmisor(rs.getInt("TRE_EMISOR"));
					tarjetaRegaloEmpresa.setTarjeta(rs.getInt("TRE_TARJETA"));
					tarjetaRegaloEmpresa.setCodigoAutorizador(rs.getInt("TRE_COD_AUTORIZADOR"));
					tarjetaRegaloEmpresa.setRutCliente(rs.getInt("RUT_CLIENTE"));
					tarjetaRegaloEmpresa.setDvCliente(rs.getString("DV_CLIENTE"));
					tarjetaRegaloEmpresa.setMonto(rs.getBigDecimal("MONTO"));
					tarjetaRegaloEmpresa.setNroTarjeta(rs.getInt("NRO_TARJETA"));
					tarjetaRegaloEmpresa.setFlag(rs.getInt("FLAG"));
					if (tarjetaRegaloEmpresa.getCorrelativoVenta() != null){
						tipoPago = Constantes.TIPO_PAGO_TRE;
					}

					DatoFacturacion datoFacturacion = new DatoFacturacion();
					datoFacturacion.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("DF_CORRELATIVO_VENTA")));
					datoFacturacion.setAddressId(rs.getInt("FACT_ADDRESS_ID"));
					datoFacturacion.setDireccionFacturaDes(rs.getString("DIRECCION_FACTURA_DES"));
					datoFacturacion.setComunaFacturaDes(rs.getString("COMUNA_FACTURA_DES"));
					datoFacturacion.setCiudadFacturaDes(rs.getString("CIUDAD_FACTURA_DES"));
					datoFacturacion.setRutFactura(rs.getString("RUT_FACTURA"));
					datoFacturacion.setRazonSocialFactura(rs.getString("RAZON_SOCIAL_FACTURA"));
					datoFacturacion.setTelefonoFactura(rs.getString("TELEFONO_FACTURA"));
					datoFacturacion.setGiroFacturaId(rs.getInt("GIRO_FACTURA_ID"));
					datoFacturacion.setGiroFacturaDes(rs.getString("GIRO_FACTURA_DES"));
					datoFacturacion.setRegionFacturaId(rs.getInt("REGION_FACTURA_ID"));
					datoFacturacion.setRegionFacturaDes(rs.getString("REGION_FACTURA_DES"));

					Despacho despacho = new Despacho();
					despacho.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getLong("D_CORRELATIVO_VENTA")));
					despacho.setRutDespacho(rs.getInt("RUT_DESPACHO"));
					despacho.setNombreDespacho(rs.getString("NOMBRE_DESPACHO"));
					despacho.setDespachoId(rs.getLong("DESPACHO_ID"));
					despacho.setFechaDespacho(rs.getTimestamp("FECHA_DESPACHO"));
					despacho.setSucursalDespacho(rs.getInt("SUCURSAL_DESPACHO"));
					despacho.setComunaDespacho(rs.getInt("COMUNA_DESPACHO"));
					despacho.setRegionDespacho(rs.getInt("REGION_DESPACHO"));
					despacho.setDireccionDespacho(rs.getString("DIRECCION_DESPACHO"));
					despacho.setTelefonoDespacho(rs.getString("TELEFONO_DESPACHO"));
					despacho.setJornadaDespacho(rs.getString("JORNADA_DESPACHO"));
					despacho.setObservacion(rs.getString("OBSERVACION"));
					despacho.setRutCliente(rs.getInt("DESPACHO_RUT_CLIENTE"));
					despacho.setDireccionCliente(rs.getString("DIRECCION_CLIENTE"));
					despacho.setTelefonoCliente(rs.getString("TELEFONO_CLIENTE"));
					despacho.setTipoCliente(rs.getInt("DESACHO_TIPO_CLIENTE"));
					despacho.setNombreCliente(rs.getString("NOMBRE_CLIENTE"));
					despacho.setApellidoPatCliente(rs.getString("APELLIDO_PAT_CLIENTE"));
					despacho.setApellidoMatCliente(rs.getString("APELLIDO_MAT_CLIENTE"));
					despacho.setTipoDespacho(rs.getInt("TIPO_DESPACHO"));
					despacho.setEmailCliente(rs.getString("EMAIL_CLIENTE"));
					despacho.setCodigoRegalo(rs.getInt("DESPACHO_CODIGO_REGALO"));
					despacho.setMensajeTarjeta(rs.getString("MENSAJE_TARJETA"));
					despacho.setAceptaCorreo(rs.getString("ACEPTA_CORREO"));
					despacho.setRegionFacturacion(rs.getInt("REGION_FACTURACION"));
					despacho.setComunaFacturacion(rs.getInt("COMUNA_FACTURACION"));
					despacho.setEnvioDte(rs.getInt("ENVIO_DTE"));
					despacho.setAddressId(rs.getInt("ADDRESS_ID"));
					despacho.setComunaDespachoDes(rs.getString("COMUNA_DESPACHO_DES"));
					despacho.setRegionDespachoDes(rs.getString("REGION_DESPACHO_DES"));
					
					TipoDespacho tipoDespacho = new TipoDespacho();
					tipoDespacho.setTipDespachoId(rs.getInt("TIP_DESPACHO_ID"));
					tipoDespacho.setDescTipDespacho(rs.getString("DESC_TIP_DESPACHO"));
					tipoDespacho.setNomTipoDespacho(rs.getString("NOM_TIPO_DESPACHO"));
					tipoDespacho.setGlosaDespacho(rs.getString("GLOSA_DESPACHO"));

					TipoDoc tipoDoc = new TipoDoc();
					tipoDoc.setTipoDoc(rs.getInt("TIPO_DOC"));
					tipoDoc.setDescTipodoc(rs.getString("DESC_TIPODOC"));
					tipoDoc.setDocOrigen(rs.getInt("DOC_ORIGEN"));
					tipoDoc.setCodBo(rs.getInt("COD_BO"));
					tipoDoc.setCodPpl(rs.getInt("COD_PPL"));
					tipoDoc.setSubTipoDoc(rs.getString("SUB_TIPO_DOC"));

					Vendedor vendedor = new Vendedor();
					vendedor.setRutVendedor(rs.getString("RUT_VENDEDOR"));
					vendedor.setNombreVendedor(rs.getString("NOMBRE_VENDEDOR")!=null?rs.getString("NOMBRE_VENDEDOR"):"INTERNET");
					
					NotaVentaRechazo notaVentaRechazo = new NotaVentaRechazo();
					notaVentaRechazo.setCorrelativoVenta(Util.validaLongCeroXNull(rs.getInt("NVR_CORRELATIVO_VENTA")));
					notaVentaRechazo.setCodMotivo(rs.getInt("COD_MOTIVO"));
					notaVentaRechazo.setFechaBoleta(rs.getTimestamp("NVR_FECHA_BOLETA"));
					notaVentaRechazo.setIdValidacion(rs.getInt("ID_VALIDACION"));
					
					MotivoRechazo motivoRechazo = new MotivoRechazo();
					motivoRechazo.setCodMotivo(rs.getInt("MR_COD_MOTIVO"));
					motivoRechazo.setMotivo(rs.getString("MR_MOTIVO"));
					
					Validacion validacion = new Validacion();
					validacion.setId(rs.getInt("ID"));
					validacion.setDescripcion(rs.getString("DESCRIPCION"));
					validacion.setQuery(rs.getString("QUERY"));
					validacion.setEstado(rs.getInt("VAL_ESTADO"));

					XValidacionOc xValidacionOc = new XValidacionOc();
					xValidacionOc.setIdValidacion(rs.getInt("XV_ID_VALIDACION"));
					xValidacionOc.setMensajeError(rs.getString("MENSAJE_ERROR"));
					xValidacionOc.setQueryValidacion(rs.getString("QUERY_VALIDACION"));
					xValidacionOc.setValorOkErr(rs.getString("VALOR_OK_ERR"));
					xValidacionOc.setQueryActivo(rs.getInt("QUERY_ACTIVO"));
					xValidacionOc.setEstadoFinalOc(rs.getInt("ESTADO_FINAL_OC"));
					
					notaVenta.setTipoPago(tipoPago);
					
					DocumentoElectronico documentoElectronico = new DocumentoElectronico();
					
					documentoElectronico.setXmlTDE(rs.getClob("XML_TDE"));
					documentoElectronico.setPdf417(rs.getClob("PDF417"));
					documentoElectronico.setTramaDTE(rs.getClob("TRAMA_DTE"));
					if(documentoElectronico.getTramaDTE() != null) {
						String dte=documentoElectronico.getTramaDTE().getSubString(1, (int) documentoElectronico.getTramaDTE().length());
						String[] arrayTramaDTE=dte.split("\\n");
						Long nroTrx=0L;
						for(String trx:arrayTramaDTE) {
							if(trx.toLowerCase().contains(Constantes.TRAMA_TRX_RPOS.toLowerCase())) {
								nroTrx = Long.valueOf(trx.substring(Constantes.NRO_SIETE).trim());
								break;
							}
						}
						notaVenta.setNroTrxRpos(nroTrx);
					}
					
					OrdenesDeCompra ordenCompra = new OrdenesDeCompra();
					ordenCompra.setNotaVenta(notaVenta);
					ordenCompra.setTarjetaRipley(tarjetaRipley);
					ordenCompra.setTarjetaBancaria(tarjetaBancaria);
//					ordenCompra.setTarjetaRegaloEmpresa(tarjetaRegaloEmpresa);
					ordenCompra.setDatoFacturacion(datoFacturacion);
					ordenCompra.setDespacho(despacho);
					ordenCompra.setTipoDespacho(tipoDespacho);
					ordenCompra.setTipoDoc(tipoDoc);
					ordenCompra.setVendedor(vendedor);
					ordenCompra.setNotaVentaRechazo(notaVentaRechazo);
					ordenCompra.setMotivoRechazo(motivoRechazo);
					ordenCompra.setValidacion(validacion);
					ordenCompra.setxValidacionOc(xValidacionOc);
					ordenCompra.setDocumentoElectronico(documentoElectronico);
					
					ordenesCompra.add(ordenCompra);
				}
			}

		} catch (Exception e) {
			logger.traceError("obtenerDatosBoleta", "Error en obtenerOrdenes: ", e);
		} finally {
			PoolBDs.closeConnection(conn);
		}
		} else {
			logger.traceInfo("obtenerDatosBoleta", "Problemas de conexion a la base de datos MODELO EXTENDIDO");
			logger.endTrace("obtenerDatosBoleta", "Finalizado", "List<OrdenesDeCompra>: AligareException");
			throw new AligareException("Error: Problemas de conexion a la base de datos MODELO EXTENDIDO");
		}

		logger.endTrace("obtenerDatosBoleta", "Finalizado", "ordenesCompra: "+ordenesCompra.size());
		return ordenesCompra;
	}
}
