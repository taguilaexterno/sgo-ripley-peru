package cl.ripley.omnicanalidad.dao.impl;

import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ripley.restservices.pplclient.OnlinePortType;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.ResultadoPPL;
import cl.ripley.omnicanalidad.dao.PaperlessDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Validaciones;
import oracle.jdbc.OracleTypes;

@Repository
public class PaperlessDAOImpl implements PaperlessDAO{

	private static final AriLog logger = new AriLog(PaperlessDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	private String targetEndpoint;
	
	@Autowired
	private OnlinePortType stub;
	
	private XPathExpression codigoExpr;
	
	private XPathExpression mensajeExpr;
	
	@Autowired
	public PaperlessDAOImpl(Parametros parametros, XPath xpath ) throws XPathExpressionException {
		targetEndpoint = parametros.buscaValorPorNombre("TARGETENDPOINT");
		
		codigoExpr = xpath.compile("//Codigo");
		mensajeExpr = xpath.compile("//Mensaje");
	}
	
	public PaperlessDAOImpl(String targetEndpoint){
		this.targetEndpoint = targetEndpoint;
	}
	
	@Override
	public ResultadoPPL generacionDte(int rut, String user, String pass, String trama, int tipoGeneracion, int tipo_retorno, int segundosTimeOut) {
		logger.initTrace("generacionDte", "int: "+rut+" - String: "+user+" - String: "+pass+" - String: trama - int: "+tipoGeneracion+" - int: "+tipo_retorno, new KeyLog("RUT", String.valueOf(rut)), new KeyLog("USER", user), new KeyLog("TIPO_DOC", String.valueOf(tipoGeneracion)));
		ResultadoPPL ppl = null;
//		try {
//			logger.traceInfo("generacionDte", "targetEndpoint: "+targetEndpoint);
//			
//			OnlineStub stub = new OnlineStub(targetEndpoint);
//			OnlineGenerationDte request = new OnlineGenerationDte();
//			request.setArgs0(rut);
//			request.setArgs1(user);
//			request.setArgs2(pass);
//			request.setArgs3(trama);
//			request.setArgs4(tipoGeneracion);
//			request.setArgs5(tipo_retorno);
//			OnlineGenerationDteResponse response = new OnlineGenerationDteResponse();
//
//			ServiceClient serviceClient = new ServiceClient();
//			serviceClient = stub._getServiceClient();
//			Options options = new Options();
//			options = serviceClient.getOptions();
//			options.setTimeOutInMilliSeconds(segundosTimeOut * Constantes.NRO_MIL);
//			serviceClient.setOptions(options);
//			stub._setServiceClient(serviceClient);
//
//			response = stub.onlineGenerationDte(request);
//			ppl = setearValores(response.get_return());
//		} catch(AxisFault e){
//			logger.traceInfo("generacionDte", "AxisFault code: "+e.getFaultCode());
//			logger.traceInfo("generacionDte", "AxisFault msg: "+e.getMessage());
//			logger.traceError("generacionDte", "Ha ocurrido un error en la llamada al servicio OnlineGenerationDte: "+ e.getMessage(), e);
//			//ppl = null;
//			
//	    	  ppl = new ResultadoPPL();
//	    	  ppl.setCodigo(Constantes.NRO_UNO);
//	    	  ppl.setMensaje(e.getMessage());
//			
//		} catch (Exception e) {
//			logger.traceError("generacionDte", "Ha ocurrido un error en la llamada al servicio OnlineGenerationDte: "+ e.getMessage(), e);
//			ppl = null;
//		}
		logger.endTrace("generacionDte", "Finalizado", "ResultadoPPL: "+((ppl != null)?ppl.toString():"null"));
		return ppl;
	}

	@Override
	public ResultadoPPL generacionBoleta(int rut, String user, String pass, String trama, int tipoGeneracion, int tipo_retorno, int segundosTimeOut) {
		logger.initTrace("generacionBoleta", "int: "+rut+" - String: "+user+" - String: "+pass+" - String: trama - int: "+tipoGeneracion+" - int: "+tipo_retorno, new KeyLog("RUT", String.valueOf(rut)), new KeyLog("USER", user), new KeyLog("TIPO_DOC", String.valueOf(tipoGeneracion)));
		ResultadoPPL ppl = null;
		String mensaje = null;
//		try {
//			logger.traceInfo("generacionBoleta", "targetEndpoint: "+targetEndpoint);
//		
//			OnlineStub stub = new OnlineStub(targetEndpoint);
//			OnlineGenerationBol request = new OnlineGenerationBol();
//			request.setArgs0(rut);
//			request.setArgs1(user);
//			request.setArgs2(pass);
//			request.setArgs3(trama);
//			request.setArgs4(tipoGeneracion);
//			request.setArgs5(tipo_retorno);
//			OnlineGenerationBolResponse response = new OnlineGenerationBolResponse();
//
//			ServiceClient serviceClient = new ServiceClient();
//			serviceClient = stub._getServiceClient();
//			Options options = new Options();
//			options = serviceClient.getOptions();
//			options.setTimeOutInMilliSeconds(segundosTimeOut * Constantes.NRO_MIL);
//			serviceClient.setOptions(options);
//			stub._setServiceClient(serviceClient);
//
//			response = stub.onlineGenerationBol(request);
//			ppl = setearValoresDTE(response.get_return());
//			mensaje = response.get_return();
//		} catch(AxisFault e){
//			logger.traceInfo("generacionBoleta", "AxisFault code: "+e.getFaultCode());
//			logger.traceInfo("generacionBoleta", "AxisFault msg: "+e.getMessage());
//			logger.traceError("generacionBoleta", "Ha ocurrido un error en la llamada al servicio OnlineGenerationDte: "+ e.getMessage(), e);
//			//ppl = null;
//			
//	    	  ppl = new ResultadoPPL();
//	    	  ppl.setCodigo(Constantes.NRO_UNO);
//	    	  ppl.setMensaje(e.getMessage());
//			
//		} catch (Exception e) {
//			logger.traceError("generacionBoleta", "Ha ocurrido un error en la llamada al servicio OnlineGenerationDte: "+ e.getMessage(), e);
//			ppl = null;
//		}
		logger.endTrace("generacionBoleta", "Finalizado", "ResultadoPPL: "+((ppl != null)?mensaje:"null"));
		logger.endTrace("generacionBoleta", "Finalizado", "ResultadoPPL: "+((ppl != null)?ppl.toString():"null"));
		return ppl;
	}
	
	@Override
	public ResultadoPPL generacionBoleta2(String rut, String user, String pass, String trama, int tipoGeneracion, int tipo_retorno) {
		logger.initTrace("generacionBoleta2", "int: "+rut+" - String: "+user+" - String: "+pass+" - String: trama - int: "+tipoGeneracion+" - int: "+tipo_retorno, new KeyLog("RUT", String.valueOf(rut)), new KeyLog("USER", user), new KeyLog("TIPO_DOC", String.valueOf(tipoGeneracion)));
		ResultadoPPL ppl = null;
		String mensaje = null;
		try {
			logger.traceInfo("generacionBoleta2", "Trama que se envía a PPL", new KeyLog("TRAMA", trama));
			logger.traceInfo("generacionBoleta2", "targetEndpoint: "+targetEndpoint);
		

			String response = stub.onlineGeneration(rut, user, pass, trama, tipoGeneracion, tipo_retorno);
			logger.traceInfo("generacionBoleta2", "respuesta onlineGeneration: "+response);
			ppl = setearValoresDTE(response);
			mensaje = response;
		} catch(Exception e){
//			logger.traceInfo("generacionBoleta2", "AxisFault code: "+e.getFaultCode());
			logger.traceInfo("generacionBoleta2", "AxisFault msg: "+e.getMessage());
			logger.traceError("generacionBoleta2", "Ha ocurrido un error en la llamada al servicio OnlineGenerationDte: "+ e.getMessage(), e);
			//ppl = null;
			
	    	  ppl = new ResultadoPPL();
	    	  ppl.setCodigo(Constantes.NRO_UNO);
	    	  ppl.setMensaje(e.getMessage());
			
		} 
		
		logger.endTrace("generacionBoleta2", "Finalizado", "ResultadoPPL: "+((ppl != null)?mensaje:"null"));
		logger.endTrace("generacionBoleta2", "Finalizado", "ResultadoPPL: "+((ppl != null)?ppl.toString():"null"));
		return ppl;
	}

	private ResultadoPPL setearValores(String resultado) {
		ResultadoPPL ppl = null;
		try {
			    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			    InputSource is = new InputSource();
			    is.setCharacterStream(new StringReader(resultado));

			    Document doc = db.parse(is);
			    NodeList nodes = doc.getElementsByTagName("Respuesta");

			    for (int i = Constantes.NRO_CERO; i < nodes.getLength(); i++) {
			      Element element = (Element) nodes.item(i);

			      NodeList name = element.getElementsByTagName("Codigo");
			      Element line = (Element) name.item(Constantes.NRO_CERO);
			      
			      String codigo = getCharacterDataFromElement(line);
                  
			      NodeList title = element.getElementsByTagName("Mensaje");
			      line = (Element) title.item(Constantes.NRO_CERO);
			      String mensaje = getCharacterDataFromElement(line);
			      
			      if(!Validaciones.validarVacio(codigo) && !Validaciones.validarVacio(mensaje)){
			    	  ppl = new ResultadoPPL();
			    	  ppl.setCodigo(Integer.parseInt(codigo));
			    	  ppl.setMensaje(mensaje);
			    	  break;
			      }
			    }
		} catch (Exception e) {
			throw new AligareException("Error en seteo de valores[setearValores]: "+e.getMessage());
		}
		return ppl;
	}
		
	private ResultadoPPL setearValoresDTE(String resultado) {
		ResultadoPPL ppl = null;
		try {
				String result = resultado.replace(Constantes.AMPER_NO_SCAPED, Constantes.AMPER_SCAPED);
			    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			    InputSource is = new InputSource();
			    is.setCharacterStream(new StringReader(result));

			    Document doc = db.parse(is);
			    
			    String codigo = codigoExpr.evaluate(doc);
			    String ted = mensajeExpr.evaluate(doc);
			    
			    if(!Validaciones.validarVacio(codigo) && !Validaciones.validarVacio(ted)){
			    	  
			    	  if(Constantes.STRING_CERO.equals(codigo)) {
			    		  
			    		  String mensaje = ted;		      

				    	  if(!Validaciones.validarVacio(mensaje)){
					    	  ppl = new ResultadoPPL();
					    	  ppl.setCodigo(Integer.parseInt(codigo));
					    	  ppl.setMensaje(mensaje);
					    	  ppl.setTed(ted);
					      }
			    		  
			    	  } else {
			    		  
			    		  ppl = new ResultadoPPL();
				    	  ppl.setCodigo(Integer.parseInt(codigo));
				    	  ppl.setMensaje(ted);
				    	  ppl.setTed(ted);
			    		  
			    	  }
			    	  
			      } 
			    
		} catch (Exception e) {
			throw new AligareException("Error en seteo de valores[setearValores]: "+e.getMessage());
		}
		return ppl;
	}


	private String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	    return Constantes.VACIO;
	}

	@Override
	public ResultadoPPL recuperacionOnline(String rut, String user, String pass, String tipoDoc, String nroFolio, int tipoRetorno, int segundosTimeOut) {
		logger.initTrace("recuperacionOnline", "int: "+rut+" - String: "+user+" - String: "+pass+" - String: "+tipoDoc+" - String: "+nroFolio+" - int: "+tipoRetorno, new KeyLog("RUT", String.valueOf(rut)), new KeyLog("USER", user), new KeyLog("NRO_FOLIO", String.valueOf(nroFolio)));
		ResultadoPPL ppl = null;
		try {
			logger.traceInfo("recuperacionOnline", "targetEndpoint: "+targetEndpoint);
			
			String response = stub.onlineRecovery(rut, user, pass, Integer.parseInt(tipoDoc), nroFolio, tipoRetorno);//onlineGenerationBol(request);
			logger.traceInfo("recuperacionOnline", "respuesta onlineRecovery: "+response);
			ppl = setearValoresDTE(response);
		} catch (Exception e) {
			logger.traceError("recuperacionOnline", "Ha ocurrido un error en la llamada al servicio recuperacionOnline: "+ e.getMessage(), e);
			ppl = null;
		}
		logger.endTrace("recuperacionOnline", "Finalizado", "ResultadoPPL: "+((ppl != null)?ppl.toString():"null"));
		return ppl;

	}

	@Override
	public List<ResultadoPPL> getListadoErroresPPL(int codigoMsg) {
		logger.initTrace("getListadoErroresPPL", "int codigoMsg: "+codigoMsg);
		
		List<ResultadoPPL> listadoErroresPPL = new ArrayList<ResultadoPPL>();

		Connection conn = null;
		CallableStatement cs = null;

		try {

			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_MANEJO_BOLETAS.PROC_GET_ERROR_PPL(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, codigoMsg);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			logger.traceInfo("getListadoErroresPPL", "{CALL CAVIRA_MANEJO_BOLETAS.PROC_GET_ERROR_PPL("+codigoMsg+",outv,outc,outn,outv)}");
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(3);
			if (rs != null) {
				while (rs.next()) {
					ResultadoPPL resultado = new ResultadoPPL();
					resultado.setCodigo(rs.getInt("COD_PPL"));
					resultado.setMensaje(rs.getString("MSG_PPL"));
					
					listadoErroresPPL.add(resultado);
				}
			}
			cs.close();
		} catch (Exception e) {
			logger.traceError("getListadoErroresPPL", e);
			throw new AligareException("Error en getListadoErroresPPL: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("getListadoErroresPPL", "Finalizado", "listadoErroresPPL: "+((listadoErroresPPL!=null)?""+listadoErroresPPL.size():"null"));
		}
		
		return listadoErroresPPL;
	}
}
