package cl.ripley.omnicanalidad.dao.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;

@Repository
public class ParametrosDAOImpl implements ParametrosDAO{
	private static final AriLog logger = new AriLog(ParametrosDAOImpl.class,
			Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Override
	public void getParametrosCajaVirtual(int funcion, ArrayList<Parametro> pList, String balance_jar)
			throws AligareException {
		logger.initTrace("getParametrosCajaVirtual", "Parametros: parametros", 
				new KeyLog("funcion", ""+ funcion));
		//variables
		Connection conn = null;
		CallableStatement cs = null;
		Parametros p = null;
		String cajaNC="", cajaBoletas="";
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_CONF.PROC_OBTIENE_PROPERTIES(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, Constantes.PARAM_OMNICANALIDAD);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			
			cs.execute();    
            ResultSet rs = (ResultSet) cs.getObject(3);  
            
            while (rs !=null & rs.next()) {  
            	Parametro par = new Parametro();
            	par.setNombre(rs.getString("NAME"));
            	par.setValor(rs.getString("VALUE"));
            	if(rs.getString("NAME").equalsIgnoreCase("NRO_CAJA_BOLETAS"))cajaBoletas = rs.getString("VALUE");
            	if(rs.getString("NAME").equalsIgnoreCase("NRO_CAJA_NC")) cajaNC = rs.getString("VALUE");
            	pList.add(par);            	
            }    
            cs.close();
            
            if (funcion == Constantes.FUNCION_BOLETA){
				Parametro par = new Parametro();
				Parametro parDos = new Parametro();
				par.setNombre("FUNCION");
				par.setValor("2");
				pList.add(par);  
				parDos.setNombre("NRO_CAJA");
				parDos.setValor(cajaBoletas);
				pList.add(parDos);  
            }
            else if (funcion == Constantes.FUNCION_NC){
            	Parametro par = new Parametro();
    			Parametro parDos = new Parametro();
            	par.setNombre("FUNCION");
            	par.setValor("1");
            	pList.add(par);  
            	
            	parDos.setNombre("NRO_CAJA");
            	parDos.setValor(cajaNC);
				pList.add(parDos);  
            }
           
            
			
		} catch (Exception e) {
			logger.traceError("getParametrosCajaVirtual", e);
			logger.endTrace("getParametrosCajaVirtual", "Finalizado", "AligareException");
			throw new AligareException("Error en NombreMetodo: " + e);
		} finally {
			PoolBDs.closeConnection(conn);
			p = new Parametros();
            p.setParametros(pList);
            if(balance_jar.equalsIgnoreCase(Constantes.NRO_DOS+"")){reemplazarCajaBalanceador(p);}
			logger.endTrace("getParametrosCajaVirtual", "Finalizado", null);
		}
		
	}

	@Override
	public void getParametrosOmnicanalidad(ArrayList<Parametro> pList, String balance_jar)
			throws AligareException {
		logger.initTrace("getParametrosOmnicanalidad", "Parametros: parametros", 
				new KeyLog("funcion",""));
		//variables
		Connection conn = null;
		CallableStatement cs = null;
		Parametros p = null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			String procSQL = "{CALL CAVIRA_CONF.PROC_OBTIENE_PROPERTIES(?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setInt(1, Constantes.PARAM_OMNICANALIDAD);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			
			cs.execute();    
            ResultSet rs = (ResultSet) cs.getObject(3);  
            
            while (rs !=null & rs.next()) {  
            	Parametro par = new Parametro();
            	par.setNombre(rs.getString("NAME"));
            	par.setValor(rs.getString("VALUE"));
            	pList.add(par);            	
            }    
            cs.close();

		} catch (Exception e) {
			logger.traceError("getParametrosOmnicanalidad", e);
			logger.endTrace("getParametrosOmnicanalidad", "Finalizado", "AligareException");
			throw new AligareException("Error en getParametrosOmnicanalidad: " + e);
		} finally {
			PoolBDs.closeConnection(conn);   
			
			p = new Parametros();
            p.setParametros(pList);
            if(balance_jar.equalsIgnoreCase(Constantes.NRO_DOS+"")){reemplazarCajaBalanceador(p);}
			logger.endTrace("getParametrosOmnicanalidad", "Finalizado", null);
		}
	}

	@Override
	public boolean updateParametro(Parametro parametro) {
		logger.initTrace("updateParametro","String parametro: "+parametro.getNombre());
		
		boolean retorno = Constantes.VERDADERO;
		CallableStatement cs = null;
		Connection conn = null;

		conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if (conn != null) {
			try {
				
				String procSQL = "{CALL CAVIRA_CONF.UPD_XSTOREPROPERTIES(?,?,?,?,?,?)}";

				cs = conn.prepareCall(procSQL);

				cs.setString(1, parametro.getNombre());
				cs.setString(2, parametro.getValor());
				cs.setInt(3, Integer.parseInt(parametro.getPropertyTypeId()));

				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.registerOutParameter(6, OracleTypes.NUMBER);
				
				logger.traceInfo("updateParametro", "{CALL SGO_MANEJO_CAJA.UPD_XSTOREPROPERTIES("+parametro.getNombre()+","+parametro.getValor()+","+Integer.parseInt(parametro.getPropertyTypeId())+",outv,outv,outn)}");
				
				cs.execute();

				String query = cs.getString(4);
				String mensaje = cs.getString(5);
				BigDecimal codigo = cs.getBigDecimal(6);

				logger.traceInfo("updateParametro", "Query: " + query);
				logger.traceInfo("updateParametro", "mensaje: " + mensaje);
				logger.traceInfo("updateParametro", "codigo: " + codigo);
				
				if (codigo.intValue() != Constantes.NRO_CERO) {
					logger.traceError("updateParametro", "Error al actualizar", new Exception("CodError: " + codigo.toString() + " MjeError"+mensaje));
					retorno = Constantes.FALSO;
				}
	
			} catch (Exception e) {
				logger.traceError("updateParametro","Error durante la ejecuci�n de updateParametro", e);
				logger.endTrace("updateParametro", "Finalizado", "retorno: "+retorno);
				return retorno;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		} else {
			logger.traceInfo("updateParametro","Problemas de conexion a la base de datos");
		}		
		logger.endTrace("updateParametro", "Finalizado", "retorno: "+retorno);

		return retorno;

	}
	
	/*
	 * Si est� activo el balanceador se utilizan cajas nuevas
	 */
	private void reemplazarCajaBalanceador(Parametros p) {
		logger.initTrace("reemplazarCajaBalanceador","inicio");
		
		try {
			p.reemplazaValorLista("NRO_CAJA_BOLETAS", p.buscaValorPorNombre("NRO_CAJA_BOLETAS_BLC"));
			logger.traceInfo("reemplazarCajaBalanceador", "NRO_CAJA_BOLETAS ha cambiado para ejecucion paralela : " + p.buscaValorPorNombre("NRO_CAJA_BOLETAS"));
			p.reemplazaValorLista("NRO_CAJA_NC", p.buscaValorPorNombre("NRO_CAJA_NC_BLC"));				
			logger.traceInfo("reemplazarCajaBalanceador", "NRO_CAJA_NC ha cambiado para ejecucion paralela : " + p.buscaValorPorNombre("NRO_CAJA_NC"));
			
			if (p.buscaValorPorNombre("FUNCION").equalsIgnoreCase(Constantes.FUNCION_NC+"")) {
				p.reemplazaValorLista("NRO_CAJA", p.buscaValorPorNombre("NRO_CAJA_NC_BLC"));
				logger.traceInfo("reemplazarCajaBalanceador",  "NRO_CAJA ha cambiado para ejecucion paralela : " + p.buscaValorPorNombre("NRO_CAJA"));
			} 
			else if (p.buscaValorPorNombre("FUNCION").equalsIgnoreCase(Constantes.FUNCION_BOLETA+"")) {
				p.reemplazaValorLista("NRO_CAJA", p.buscaValorPorNombre("NRO_CAJA_BOLETAS_BLC"));
				logger.traceInfo("reemplazarCajaBalanceador",  "NRO_CAJA ha cambiado para ejecucion paralela : " + p.buscaValorPorNombre("NRO_CAJA"));
			}
			else {
				p.reemplazaValorLista("NRO_CAJA_TRANSITO", p.buscaValorPorNombre("NRO_CAJA_TRANSITO_BLC"));
				logger.traceInfo("reemplazarCajaBalanceador",  "NRO_CAJA_TRANSITO ha cambiado para ejecucion paralela : " + p.buscaValorPorNombre("NRO_CAJA_TRANSITO"));
			}
			
		} catch (Exception e) {
			logger.traceError("reemplazarCajaBalanceador", e);
			logger.endTrace("reemplazarCajaBalanceador", "Finalizado", "AligareException");
			throw new AligareException("Error al asignar caja" + e);
		}
			
	}

}
