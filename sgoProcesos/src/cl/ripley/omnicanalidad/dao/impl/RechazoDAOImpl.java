package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.RechazoDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;

public class RechazoDAOImpl implements RechazoDAO{
	private static final AriLog logger = new AriLog(RechazoDAOImpl.class, "RIP16-004", PlataformaType.JAVA);
	@Override
	public RetornoEjecucion fegifePrcInsTreint(TramaDTE trama)throws AligareException {
		logger.initTrace("fegifePrcInsTreint", "Trama: trama");
		RetornoEjecucion retornoEjecucion = null;
		Connection conn = PoolBDs.getConnection(BDType.TRE);
		CallableStatement cs = null;
		if (conn!=null){
			try {
				String prcSQL = "";
				String sql = "";
				if (!trama.getArticuloVentas().isEmpty()){
					prcSQL = "{CALL FEGIFE_PRC_INS_TREINT (?,?,?,?,?,?,?,?)}";
					cs = conn.prepareCall(prcSQL);
					cs.setInt(1, (trama.getTarjetaRegaloEmpresa().getNroTarjeta()!=null)?trama.getTarjetaRegaloEmpresa().getNroTarjeta():Constantes.NRO_CERO);
					sql = "BEGIN FEGIFE_PRC_ACT_SLD_CONF("+ ((trama.getTarjetaRegaloEmpresa().getNroTarjeta()!=null)?trama.getTarjetaRegaloEmpresa().getNroTarjeta().intValue():Constantes.NRO_CERO);
					cs.setString(2, Constantes.TIPO_EVENTO_RC);//valor en duro
					sql = sql + ","+Constantes.TIPO_EVENTO_RC;
					cs.setInt(3, (trama.getTarjetaRegaloEmpresa().getMonto()!=null?trama.getTarjetaRegaloEmpresa().getMonto().intValue()*Constantes.NRO_MENOSUNO:Constantes.NRO_CERO));
					sql = sql + ","+(trama.getTarjetaRegaloEmpresa().getMonto()!=null?trama.getTarjetaRegaloEmpresa().getMonto().intValue()*Constantes.NRO_MENOSUNO:Constantes.NRO_CERO);
					cs.setInt(4, trama.getTarjetaRegaloEmpresa().getCorrelativoVenta().intValue());
					sql = sql + "," + trama.getTarjetaRegaloEmpresa().getCorrelativoVenta().intValue();
					cs.setInt(5, Constantes.NRO_CERO);//valor en duro
					sql = sql + "," +Constantes.NRO_CERO;
					cs.setNull(6, OracleTypes.NULL);
					sql = sql + ",null); END;";
					cs.registerOutParameter(7, OracleTypes.VARCHAR);
					cs.registerOutParameter(8, OracleTypes.NUMBER);
					logger.traceInfo("fegifePrcInsTreint", "Creacion TRE BackOffice -"+sql);
					cs.execute();
					retornoEjecucion = new RetornoEjecucion();
					if (cs.getInt(8)==Constantes.EJEC_SIN_ERRORES){
						retornoEjecucion.setCodigo(cs.getInt(8));
						retornoEjecucion.setMensaje("PL: FEGIFE_PRC_INS_TREINT. Ejecutado con exito!");
					}
					else{
						retornoEjecucion.setCodigo(cs.getInt(8));
						retornoEjecucion.setMensaje("Imposible Registrar forma de Pago Tarjeta Regalos Empresa Error "
								+ "- FEGIFE_PRC_ACT_SLD_CONF "+cs.getInt(8));
					}
					cs.close();
				}
				else{
					retornoEjecucion = new RetornoEjecucion();
					retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
					retornoEjecucion.setMensaje("No vienen datos en la trama de fegifePrcInsTreint");
					logger.traceInfo("fegifePrcInsTreint", "No vienen datos en la trama de fegifePrcInsTreint");
				}
				
				
			} catch (Exception e) {
				logger.traceError("fegifePrcInsTreint", "Error durante la ejecución de fegifePrcInsTreint", e);
				retornoEjecucion = new RetornoEjecucion();
				retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoEjecucion.setMensaje("Error durante la ejecución de fegifePrcInsTreint");
				logger.endTrace("fegifePrcInsTreint", "Finalizado", "retornoEjecucion: " + retornoEjecucion.toString());
				return retornoEjecucion;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}
		else{
			logger.traceInfo("fegifePrcInsTreint","Problemas de conexion a la base de datos TRE");
		}
		logger.endTrace("fegifePrcInsTreint", "Finalizado", "retornoEjecucion: " + retornoEjecucion.toString());
		return retornoEjecucion;
	}
	@Override
	public RetornoEjecucion fegifePrcActSldConf(TramaDTE trama)throws AligareException {
		logger.initTrace("fegifePrcActSldConf", "Trama: trama");
		RetornoEjecucion retornoEjecucion = null;
		Connection conn = PoolBDs.getConnection(BDType.TRE);
		CallableStatement cs = null;
		if (conn!=null){
			try {
				String prcSQL = "";
				String sql = "";
				if (!trama.getArticuloVentas().isEmpty()){
					prcSQL = "{CALL FEGIFE_PRC_ACT_SLD_CONF (?,?,?,?,?)}";
					cs = conn.prepareCall(prcSQL);
					cs.setInt(1, (trama.getTarjetaRegaloEmpresa().getNroTarjeta()!=null)?trama.getTarjetaRegaloEmpresa().getNroTarjeta():Constantes.NRO_CERO);
					sql = "BEGIN FEGIFE_PRC_ACT_SLD_CONF("+ ((trama.getTarjetaRegaloEmpresa().getNroTarjeta()!=null)?trama.getTarjetaRegaloEmpresa().getNroTarjeta().intValue():Constantes.NRO_CERO);
					
					cs.setInt(2, (trama.getTarjetaRegaloEmpresa().getMonto()!=null?trama.getTarjetaRegaloEmpresa().getMonto().intValue():Constantes.NRO_CERO));
					sql = sql + ","+(trama.getTarjetaRegaloEmpresa().getMonto()!=null?trama.getTarjetaRegaloEmpresa().getMonto().intValue():Constantes.NRO_CERO);
					
					cs.setInt(3, Constantes.NRO_CERO);
					sql = sql + "," + Constantes.NRO_CERO;
					sql = sql + "); END;";
					
					cs.registerOutParameter(4, OracleTypes.VARCHAR);
					cs.registerOutParameter(5, OracleTypes.NUMBER);
					logger.traceInfo("fegifePrcActSldConf", "Creacion TRE BackOffice -"+sql);
					cs.execute();
					
					retornoEjecucion = new RetornoEjecucion();
					if (cs.getInt(5)==Constantes.EJEC_SIN_ERRORES){
						retornoEjecucion.setCodigo(cs.getInt(5));
						retornoEjecucion.setMensaje("PL: FEGIFE_PRC_ACT_SLD_CONF. Ejecutado con exito!");
					}
					else{
						retornoEjecucion.setCodigo(cs.getInt(5));
						retornoEjecucion.setMensaje("Imposible Registrar forma de Pago Tarjeta Regalos Empresa Error "
								+ "- FEGIFE_PRC_ACT_SLD_CONF "+cs.getInt(5));
					}
					cs.close();
				}
				else{
					retornoEjecucion = new RetornoEjecucion();
					retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
					retornoEjecucion.setMensaje("No vienen datos en la trama de fegifePrcActSldConf");
					logger.traceInfo("fegifePrcActSldConf", "No vienen datos en la trama de fegifePrcActSldConf");
				}
				
				
			} catch (Exception e) {
				logger.traceError("fegifePrcActSldConf", "Error durante la ejecución de fegifePrcActSldConf", e);
				retornoEjecucion = new RetornoEjecucion();
				retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoEjecucion.setMensaje("Error durante la ejecución de fegifePrcActSldConf");
				logger.endTrace("fegifePrcActSldConf", "Finalizado", "retornoEjecucion: " + retornoEjecucion.toString());
				return retornoEjecucion;
			} finally {
				PoolBDs.closeConnection(conn);
			}
		}
		else{
			logger.traceInfo("fegifePrcActSldConf","Problemas de conexion a la base de datos TRE");
		}
		logger.endTrace("fegifePrcActSldConf", "Finalizado", "retornoEjecucion: " + retornoEjecucion.toString());
		return retornoEjecucion;
	}



}
