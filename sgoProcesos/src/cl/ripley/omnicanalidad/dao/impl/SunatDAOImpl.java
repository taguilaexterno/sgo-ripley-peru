package cl.ripley.omnicanalidad.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.dao.SunatDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;

/**Clase DAO para operaciones de caja.
 *
 * @author Martin Corrales (Aligare).
 * @since 08-01-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Repository
public class SunatDAOImpl implements SunatDAO {

	private static final AriLog logger = new AriLog(SunatDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Override
	public boolean insertCodigosSunat(HashMap<String,String> codigosSunat) throws AligareException {
		logger.initTrace("insertCodigosSunat", "HashMap<String,String> codigosSunat");
		Connection conn = null;
		boolean resp = false;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			PreparedStatement ps = null;
			String sqlLimpiarTabla = " DELETE FROM CV_CODIGOS_SUNAT ";
			ps = conn.prepareStatement(sqlLimpiarTabla);
			ps.executeUpdate();
			String insertSQL ="";
			insertSQL = "INSERT ALL ";
			Set<Entry<String,String>> set = codigosSunat.entrySet();
		    Iterator<Entry<String, String>> iterator = set.iterator();
		    while(iterator.hasNext()) {
		        Map.Entry<String, String> codigo = (Entry<String, String>)iterator.next();
		        insertSQL		+= "            INTO CV_CODIGOS_SUNAT (SUBLINEA,CODIGO_SUNAT) VALUES('" + codigo.getKey() + "','" + codigo.getValue() + "') ";
		    }
			insertSQL += " SELECT * FROM DUAL ";
			logger.traceInfo("insertCodigosSunat", "Insert: "+ insertSQL);
			ps = conn.prepareStatement(insertSQL);
			int resultadoInsert = ps.executeUpdate();
			logger.traceInfo("insertCodigosSunat", "resultado del insert en CV_CODIGOS_SUNAT: "+ resultadoInsert);
			if (resultadoInsert > 0) {
				resp = true;
			}
			ps.close();
		} catch (SQLException e) {
			logger.traceInfo("insertCodigosSunat", "SQLException: "+ e);
			throw new AligareException("Ha ocurrido un error SQLException.");
		} catch (Exception e) {
			logger.traceInfo("insertCodigosSunat", "Exception: "+ e);
			throw new AligareException("Imposible realizar conexion a 'MODELO_EXTENDIDO' en estos momentos, contacte al Administrador o intentelo mas tarde.");
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("insertCodigosSunat", "Finalizado", "Se crean codigos sunat?: "+resp);
		}
		return resp;
	}

}
