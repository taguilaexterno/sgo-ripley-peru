package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;

import org.springframework.stereotype.Repository;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.dao.ValidacionNotaVentaDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.PoolBDs;
import oracle.jdbc.OracleTypes;
/**
 * ValidacionNotaVentaDAOImpl posee las funciones que 
 * permite trabajar con valdiaicones de notas de venta
 * 
 * @author Mauro Sanhueza T
 * @date 04-08-2016
 * @version     %I%, %G%
 * @since 1.0 
 */
@Repository
public class ValidacionNotaVentaDAOImpl implements ValidacionNotaVentaDAO {

	private static final AriLog logger = new AriLog(ValidacionNotaVentaDAOImpl.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	/**
	 * aplicaXValidacionesOrdenCompraTransito es la funcion que permite aplicar valdiaciones 
	 * de forma a nota de venta cargada 
	 * 
	 * @param correlativoVenta 	numero correlativo de nota de venta al cual aplicaremos
	 * 							validaciones de forma 
	 * 
	 * @return respuesta Integer que retorna resultado de validacion
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */	
	public Integer aplicaXValidacionesOrdenCompraTransito(Long correlativoVenta, boolean flagRpos){

		logger.initTrace("aplicaXValidacionesOrdenCompraTransito", "Integer correlativoVenta: "+correlativoVenta.intValue());
		
		Integer respuesta = null;
		Connection conn = null;
		CallableStatement cs = null;
		String procSQL;
		String nombreProc;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			if(conn == null){
				logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "No existe conexión");
				return respuesta;
			}
			//Flag True solo para RPOS
			if(flagRpos) 
				nombreProc = Constantes.NAME_XVALID_RPOS;
			else
				nombreProc = Constantes.NAME_XVALID_NVENTA;
			procSQL = "{CALL CAVIRA_CRON_VALIDACIONES."+nombreProc+"(?,?,?,?,?)}";
			
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoVenta);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			cs.registerOutParameter(4, OracleTypes.NUMBER);
			cs.registerOutParameter(5, OracleTypes.VARCHAR);
			logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "procSQL: CAVIRA_CRON_VALIDACIONES."+nombreProc+"("+correlativoVenta+",out2n,out3n,out4n,out5v)");
			cs.execute();    
			logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "out2n*: "+cs.getInt(2)+", out3n: "+cs.getInt(3)+", out4n: "+cs.getInt(4));
			logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "out5v: "+cs.getString(5));
			if (cs.getInt(2) == Constantes.NRO_UNO){
				logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "CORRELATIVO: "+correlativoVenta.intValue()+ " Dejado con ESTADO y CAJA sin cambios");
			} else {
				logger.traceInfo("aplicaXValidacionesOrdenCompraTransito", "CORRELATIVO: "+correlativoVenta.intValue()+ " Dejado con ESTADO: "+cs.getInt(3)+ " y CAJA sin cambios");
			}
			respuesta = cs.getInt(2);
		} catch (Exception e) {
			logger.traceError("aplicaXValidacionesOrdenCompraTransito", e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("aplicaXValidacionesOrdenCompraTransito", "Finalizado", "Integer respuesta: "+respuesta.intValue());
		}
		return respuesta;
	}

	/**
	 * aplicaValidacionesMedioPago es la funcion que permite aplicar validaciones 
	 * de medios de pago a nota de venta cargada 
	 * 
	 * @param correlativoVenta 	numero correlativo de nota de venta al cual aplicaremos
	 * 							validaciones de forma 
	 * @param rutComprador		rut comprador nota de venta
	 * @param emailCliente		email de cliente obtenido de despacho
	 * @param telefonoCliente	telefono de cliente obtenido de despacho
	 * @param tipoDespacho		tipo de despacho obtenida desde articulo de venta
	 * @param comunaDespacho  	comuna de cliente obtenido de despacho
	 * @param regionDespacho	region de cliente obtenido de despacho
	 * @param tipoDoc			tipo de documento especifica si es recaudacion o boleta
	 * 
	 * @return respuesta Integer que retorna resultado de validacion
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */	
	public Integer aplicaValidacionesMedioPago(	Long correlativoVenta,
												Long rutComprador,
												String emailCliente,
												String telefonoCliente,
												String tipoDespacho,
												Integer comunaDespacho,
												Integer regionDespacho,
												String direccionDespacho,
												Integer tipoDoc) {
		
		logger.initTrace("aplicaValidacionesMedioPago", "Integer correlativoVenta: "+correlativoVenta+", Integer rutComprador: "+rutComprador+", String emailCliente: "+emailCliente+
												", String telefonoCliente: "+telefonoCliente+",String tipoDespacho: "+tipoDespacho+", Integer comunaDespacho: "+comunaDespacho+", Integer regionDespacho: "+regionDespacho);
		
		Integer respuesta = null;
		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			if(conn == null){
				logger.traceInfo("aplicaValidacionesMedioPago", "No existe coneccion");
				return respuesta;
			}
			String procSQL = "{CALL CAVIRA_CRON_VALIDACIONES.PROC_EJEC_PAG_SEGURO_NVENTA(?,?,?,?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoVenta);
			cs.setLong(2, rutComprador);
			cs.setString(3, emailCliente);
/*
			int tam1 = telefonoCliente.length();
			int tam2 = tam1-9;
			
			cs.setInt(4, (tam1 > 9)?Integer.parseInt(telefonoCliente.substring(tam2,tam1)):Integer.parseInt(telefonoCliente)); //FIXME
			//cs.setInt(4, telefonoCliente);
*/
			cs.setString(4, telefonoCliente);
			
			cs.setString(5, tipoDespacho);
			cs.setInt(6, comunaDespacho);
			cs.setInt(7, regionDespacho);
			cs.setString(8, direccionDespacho);
			cs.setInt(9, tipoDoc);
			cs.registerOutParameter(10, OracleTypes.NUMBER);
			cs.registerOutParameter(11, OracleTypes.NUMBER);
			cs.registerOutParameter(12, OracleTypes.VARCHAR);
			logger.traceInfo("aplicaValidacionesMedioPago", "procSQL: CAVIRA_CRON_VALIDACIONES.PROC_EJEC_PAG_SEGURO_NVENTA("+correlativoVenta+","+rutComprador+",'"+emailCliente+"','"+telefonoCliente+"','"+tipoDespacho+"',"+comunaDespacho+","+regionDespacho+",'"+direccionDespacho+"',"+tipoDoc+",out10n,out11n,out12v)");
			cs.execute();    
			respuesta = cs.getInt(10);
			logger.traceInfo("aplicaValidacionesMedioPago", "out10n*: "+cs.getInt(10)+", out11n: "+cs.getInt(11));
			logger.traceInfo("aplicaValidacionesMedioPago", "out12: "+cs.getString(12));

		} catch (Exception e) {
			logger.traceError("aplicaValidacionesMedioPago", e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("aplicaValidacionesMedioPago", "Finalizado", "Integer respuesta: "+respuesta.intValue());
		}
		return respuesta;
	}
	
	/**
	 * aplicaValidacionesMedioPagoSeguro es la funcion que permite aplicar validaciones 
	 * de reglas a nota de venta cargada 
	 * 
	 * @param correlativoVenta 	numero correlativo de nota de venta al cual aplicaremos
	 * 							validaciones de forma 
	 * @param rutComprador		rut comprador nota de venta
	 * @param emailCliente		email de cliente obtenido de despacho
	 * @param telefonoCliente	telefono de cliente obtenido de despacho
	 * @param tipoDespacho		tipo de despacho obtenida desde articulo de venta
	 * @param comunaDespacho  	comuna de cliente obtenido de despacho
	 * @param regionDespacho	region de cliente obtenido de despacho
	 * @param tipoDoc			tipo de documento especifica si es recaudacion o boleta
	 * 
	 * @return respuesta Integer que retorna resultado de validacion
	 * 
	 * @version     %I%, %G%
	 * @since 1.0 
	 */	
	public Integer aplicaValidacionesMedioPagoSeguro(	Long correlativoVenta,
														Long rutComprador,
														String emailCliente,
														String telefonoCliente,
														String tipoDespacho,
														String indicadorMkp,
														Integer comunaDespacho,
														Integer regionDespacho,
														String direccionDespacho,
														Integer tipoDoc) {

		logger.initTrace("aplicaValidacionesMedioPago", "Integer correlativoVenta: "+correlativoVenta+", Integer rutComprador: "+rutComprador+", String emailCliente: "+emailCliente+
				", String telefonoCliente: "+telefonoCliente+", String tipoDespacho: "+tipoDespacho+", Integer comunaDespacho: "+comunaDespacho+", Integer regionDespacho: "+regionDespacho);

		Integer respuesta = null;
		Connection conn = null;
		CallableStatement cs = null;
		try {
			conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
			if(conn == null){
				logger.traceInfo("aplicaValidacionesMedioPagoSeguro", "No existe coneccion");
				return respuesta;
			}
			String procSQL = "{CALL CAVIRA_CRON_VALIDACIONES.PROC_EJEC_REGLAS_ADMIN_NVENTA(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
			cs = conn.prepareCall(procSQL);
			cs.setLong(1, correlativoVenta);
			cs.setLong(2, rutComprador);
			cs.setString(3, emailCliente);
			
/*
			telefonoCliente = telefonoCliente.replaceAll(Constantes.SPACE, Constantes.VACIO).trim();
			int tam1 = telefonoCliente.length();
			int tam2 = tam1-9;
			
			cs.setInt(4, (tam1 > 9)?Integer.parseInt(telefonoCliente.substring(tam2,tam1)):Integer.parseInt(telefonoCliente)); //FIXME
			//cs.setInt(4, telefonoCliente);
*/
			cs.setString(4, telefonoCliente);
			
			cs.setString(5, tipoDespacho);
			cs.setInt(6, comunaDespacho);
			cs.setInt(7, regionDespacho);
			cs.setString(8, direccionDespacho);
			cs.setInt(9, tipoDoc);
			cs.setInt(10, new Integer(indicadorMkp));
			cs.registerOutParameter(11, OracleTypes.NUMBER);
			cs.registerOutParameter(12, OracleTypes.NUMBER);
			cs.registerOutParameter(13, OracleTypes.VARCHAR);
			logger.traceInfo("aplicaValidacionesMedioPagoSeguro", "procSQL: CAVIRA_CRON_VALIDACIONES.PROC_EJEC_REGLAS_ADMIN_NVENTA("+correlativoVenta+","+rutComprador+",'"+emailCliente+"','"+telefonoCliente+"','"+tipoDespacho+"',"+comunaDespacho+","+regionDespacho+",'"+direccionDespacho+"',"+tipoDoc+","+indicadorMkp+",out11n,out12n,out13v)");
			cs.execute();    
			respuesta = cs.getInt(11);
			logger.traceInfo("aplicaValidacionesMedioPago", "out11n*: "+cs.getInt(11)+", out12n: "+cs.getInt(12));
			logger.traceInfo("aplicaValidacionesMedioPago", "out13v: "+cs.getString(13));
		} catch (Exception e) {
			logger.traceError("aplicaValidacionesMedioPago", e);
		} finally {
			PoolBDs.closeConnection(conn);
			logger.endTrace("aplicaValidacionesMedioPago", "Finalizado", "Integer respuesta: "+respuesta.intValue());
		}
		return respuesta;
	}

}
