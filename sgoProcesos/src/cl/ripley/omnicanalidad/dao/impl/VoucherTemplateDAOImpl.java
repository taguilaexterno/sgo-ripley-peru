package cl.ripley.omnicanalidad.dao.impl;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.util.BDType;
import cl.ripley.omnicanalidad.util.PoolBDs;

@Repository
public class VoucherTemplateDAOImpl implements VoucherTemplateDAO{
	private static final AriLog logger = new AriLog(VoucherTemplateDAOImpl.class,"RIP16-004", PlataformaType.JAVA);
	
	@Override
	public TemplateVoucher getTemplateByLlave(String llave) {
		logger.initTrace("getTemplateByLlave", "String llave: "+llave);
		TemplateVoucher template = new TemplateVoucher();
		
		Connection conn = PoolBDs.getConnection(BDType.MODEL_EXTEND);
		if(conn != null){
			CallableStatement cs = null;
			
			try {
				
				String procSQL = "{CALL CAVIRA_MANEJO_TEMPLATE.PROC_OBTENER_TEMPLATE(?,?,?,?,?)}";
				cs = conn.prepareCall(procSQL);
				cs.setString(1, llave);
				cs.registerOutParameter(2, OracleTypes.VARCHAR);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.registerOutParameter(4, OracleTypes.NUMBER);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				logger.traceInfo("getTemplateByLlave", "{CALL CAVIRA_MANEJO_TEMPLATE.PROC_OBTENER_TEMPLATE('"+llave+"',outv,outc,outn,outv)}");
				cs.execute();    
				ResultSet rs = (ResultSet) cs.getObject(3);
				//logger.traceInfo("getTemplateByLlave", "Query: "+((String)cs.getObject(2)));
				if (rs != null && rs.next()) {
					template.setLlave(rs.getString("LLAVE"));
					template.setNombre(rs.getString("NOMBRE"));
					template.setUrl(rs.getString("URL"));
					Clob clob = rs.getClob("HTML");
					if (clob == null) {
						logger.traceInfo("getTemplateByLlave", "Campo HTML no cargado en template de correos desde SGO");
						return new TemplateVoucher();
					}
					template.setHtml(clob.getSubString(1, (int) clob.length()));
					template.setEstado(rs.getInt("ESTADO"));
					
					rs.close();
				}
				cs.close();
			} catch (Exception e) {
				logger.traceError("getTemplateByLlave", "Error durante la ejecuci�n de getTemplateByLlave", e);
				logger.endTrace("getTemplateByLlave", "Finalizado", null);
				return null;
			} finally {
				PoolBDs.closeConnection(conn);			
			}
		}else{
			logger.traceInfo("getTemplateByLlave", "Problemas de conexion a la base de datos");
		}
		logger.endTrace("getTemplateByLlave", "Finalizado", null);

		return template;
	}

	
	
}
