package cl.ripley.omnicanalidad.logic;

import java.util.ArrayList;
import java.util.Properties;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.impl.ParametrosDAOImpl;
import cl.ripley.omnicanalidad.logic.boleta.MaestroBoleta;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.logic.mail.MaestroMail;
import cl.ripley.omnicanalidad.logic.notaCredito.MaestroNotaCredito;
import cl.ripley.omnicanalidad.logic.validacion.MaestroValidacion;
import cl.ripley.omnicanalidad.util.CodErrorSystemExit;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.ContextListener;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;

public class Cavira {
	private static final AriLog logger = new AriLog(Cavira.class,"RIP16-004", PlataformaType.JAVA);
	public static String SWITCH_ONOFF_TBK_NC;
	public static String SWITCH_ONOFF_BALANCEADOR = "SWITCH_ONOFF_BALANCEADOR";
	public static void main(String[] args) {
		MaestroValidacion maestroValidacion;
		MaestroBoleta maestroBoleta;
		MaestroNotaCredito maestroNC;
		MaestroMail maestroMail;
		String balance_jar;
		
		int error = CodErrorSystemExit.SALIDA_OK;
		
		try {
			Properties prop = new Properties();
			ContextListener context = new ContextListener();
			prop = context.contextInit(prop, args[Constantes.NRO_CERO].trim());
			//logger.initTrace("process", "String[] args.length: "+args.length);
			PoolBDs.OpenConnections(prop);	
			Parametros parametros = new Parametros();
			Parametros parametrosSW = new Parametros();
			ParametrosDAO parametrosDAO = new ParametrosDAOImpl();
			parametros.setParametros(new ArrayList<Parametro>());
			parametrosSW.setParametros(new ArrayList<Parametro>());

			Constantes.RUTA_LOG = prop.getProperty("rutaFiles");
			int op = Constantes.NRO_TRES;
			
			if (args.length != Constantes.NRO_CERO) {
				op = Integer.parseInt(args[Constantes.NRO_CERO].trim());
				logger.initTrace("main", "opcion: "+op);
			}
			
			String switchOnOff = "";
			
//			OBTENER PARAMETROS BD
			balance_jar = prop.getProperty("balance_jar")==null?"1":prop.getProperty("balance_jar");
			
			parametrosDAO.getParametrosOmnicanalidad(parametrosSW.getParametros(), balance_jar);	
			
//			Validacion switch TBK/SGO/Caja Cerrada
			if (op == Constantes.FUNCION_NC){
				switchOnOff = "SWITCH_ONOFF_NC";
				SWITCH_ONOFF_TBK_NC = parametrosSW.buscaValorPorNombre("SWITCH_ONOFF_TBK_NC");
			} else {
				if (op == Constantes.FUNCION_EMAIL){
					switchOnOff = "SWITCH_ONOFF_MAIL";
				} else {
					switchOnOff = "SWITCH_ONOFF_SGO";
					if (Constantes.STRING_CERO.equalsIgnoreCase(parametrosSW.buscaValorPorNombre("SWITCH_ONOFF_SGO"))){
						if (Constantes.STRING_UNO.equalsIgnoreCase(parametrosSW.buscaValorPorNombre("FLAG_CAJA_CERRADA"))){
							//cerrar caja
							parametrosDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), balance_jar);
							new OperacionesCaja().cajaVirtualCerrar(parametros);
							
							//actualizar flag a 0
							Parametro parametro = parametrosSW.buscaParametroPorNombre("FLAG_CAJA_CERRADA");
							parametro.setPropertyTypeId(String.valueOf(Constantes.NRO_NOVENTANUEVE));
							parametro.setValor(Constantes.STRING_CERO);
							if (!parametrosDAO.updateParametro(parametro)){
								logger.traceInfo("main", "Error al actualizar parametro FLAG_CAJA_CERRADA a 0");
							}
						}
					} else {
						if (!Constantes.STRING_UNO.equalsIgnoreCase(parametrosSW.buscaValorPorNombre("FLAG_CAJA_CERRADA"))){
							
							//actualizar flag a 1
							Parametro parametro = parametrosSW.buscaParametroPorNombre("FLAG_CAJA_CERRADA");
							parametro.setPropertyTypeId(String.valueOf(Constantes.NRO_NOVENTANUEVE));
							parametro.setValor(Constantes.STRING_UNO);
							if (!parametrosDAO.updateParametro(parametro)){
								logger.traceInfo("main", "Error al actualizar parametro FLAG_CAJA_CERRADA a 1");
							}
						}
					}
				}
			}

			if (Constantes.STRING_CERO.equalsIgnoreCase(parametrosSW.buscaValorPorNombre(switchOnOff))){
				error = CodErrorSystemExit.CVIRTUAL_APAGADO;
				logger.initTrace("main", "PARAMETRO SWITCH ON OFF "+op+" APAGADO");
				PoolBDs.CloseConnections();
				logger.endTrace("main", "Finalizado", "salida: "+error);
				Util.pasarGarbageCollector();
				System.exit(error);
			}
//			Validar si es caja balanceadora el parametro se encuentre activo
			else if (balance_jar.equalsIgnoreCase(Constantes.STRING_DOS) &&
					Constantes.STRING_CERO.equalsIgnoreCase(parametrosSW.buscaValorPorNombre(SWITCH_ONOFF_BALANCEADOR))){
				error = CodErrorSystemExit.CVIRTUAL_APAGADO;
				logger.initTrace("main", "PARAMETRO SWITCH ON OFF Balanceador "+op+" APAGADO");
				PoolBDs.CloseConnections();
				logger.endTrace("main", "Finalizado", "salida: "+error);
				Util.pasarGarbageCollector();
				System.exit(error);
			}
			
			
			switch (op) {
				case Constantes.FUNCION_NC:
					logger.traceInfo("main", "NC: "+Constantes.FUNCION_NC);
					parametrosDAO.getParametrosCajaVirtual(Constantes.FUNCION_NC, parametros.getParametros(), balance_jar);
					maestroNC = new MaestroNotaCredito(parametros);
					maestroNC.process();
	
					error = CodErrorSystemExit.SALIDA_OK;
					break;
				case Constantes.FUNCION_BOLETA:
					logger.traceInfo("main", "BOLETA: "+Constantes.FUNCION_BOLETA);
					parametrosDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), balance_jar);
					maestroBoleta = new MaestroBoleta(parametros);
					maestroBoleta.process();
	
					error = CodErrorSystemExit.SALIDA_OK;
					break;
					
				case Constantes.FUNCION_VALIDACION:
					logger.traceInfo("main", "VALIDACION: "+Constantes.FUNCION_VALIDACION);
					parametrosDAO.getParametrosOmnicanalidad(parametros.getParametros(), balance_jar);
					maestroValidacion = new MaestroValidacion(parametros);
					maestroValidacion.process();
	
					error = CodErrorSystemExit.SALIDA_OK;
					break;	

				case Constantes.FUNCION_EMAIL:
					logger.traceInfo("main", "EMAIL: "+Constantes.FUNCION_EMAIL);
					parametrosDAO.getParametrosOmnicanalidad(parametros.getParametros(), balance_jar);
					maestroMail = new MaestroMail(parametros);
					maestroMail.process();
	
					error = CodErrorSystemExit.SALIDA_OK;
					break;	

				default:
					logger.traceInfo("main", "ERROR: MAIN EJECUTADO SIN OPCION");
					
					error = CodErrorSystemExit.ERROR_CONTROLADO;
					break;
			}

		} catch (AligareException e){
			error = CodErrorSystemExit.ERROR_BD_CAIDA;
			logger.traceInfo("main", "ERROR: BASE DE DATOS CAIDA");
		} catch (NullPointerException e){
			error = CodErrorSystemExit.FILE_SYSTEM_FULL;
			logger.traceInfo("main", "ERROR: FILESYSTEM SIN ESPACIO");
		} catch (Exception e) {
			error = CodErrorSystemExit.ERROR_CONTROLADO;
			logger.traceInfo("main", "ERROR: ERROR NO CONTROLADO : " + e.getMessage());
		} finally {
			PoolBDs.CloseConnections();
			logger.endTrace("main", "Finalizado", "salida: "+error);
			Util.pasarGarbageCollector();
			System.exit(error);
		}
	}
}