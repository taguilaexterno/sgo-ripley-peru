package cl.ripley.omnicanalidad.logic;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.impl.CajaVirtualDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.ParametrosDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.ContextListener;
import cl.ripley.omnicanalidad.util.PoolBDs;

public class Test {

	public static void main(String[] args) throws RemoteException {
/*
		Timestamp time = FechaUtil.stringToTimestamp("2016-08-19 16:11:49", "yyyy-MM-dd hh:mm:ss");//Constantes.FECHA_DDMMYYYY_HHMMSS2);//"yyyy-MM-dd HH24:mi:SS");
		
		System.out.println(time);
		System.out.println(formatTimestamp(Constantes.FECHA_DDMMYYYY3.replaceAll("-", "/"), time));
		
		//FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYYYY,   FechaUtil.( 2016-08-19 16:11:49.0   ) )
		
		System.out.println(new String("hola").equalsIgnoreCase(null));
		
		//ResultadoPPL ppl = null;
		
		Integer precio = new Integer(200);
		Integer dscto = new Integer(20);
		Integer cantidad = new Integer(2);
		Integer resultado = new Integer(0);
		
		
		resultado =  (int) (precio - Math.floor(dscto / cantidad));
		
		System.out.println("FIRV resultado: "+resultado);
		
		String telefonoCliente = "0924242343";
		System.out.println(telefonoCliente);
		int tam1 = telefonoCliente.length();
		System.out.println(tam1);
		int tam2 = tam1-9;
		System.out.println(tam2);
		System.out.println((tam1 > 9)?Integer.parseInt(telefonoCliente.substring(tam2,tam1)):Integer.parseInt(telefonoCliente));


		Properties prop = new Properties();
		ContextListener context = new ContextListener();
		prop = context.contextInit(prop, "2");
		PoolBDs.OpenConnections(prop);
		Parametros parametrosBol = new Parametros();
		//Parametros parametrosNC = new Parametros();
		ParametrosDAO parametrosDAO = new ParametrosDAOImpl();
		parametrosBol.setParametros(new ArrayList<Parametro>());
		
		parametrosDAO.getParametrosCajaVirtual(2,parametrosBol.getParametros());
		
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		caja.sysdateFromDual("DD/MM/YYYY HH24:MI:SS", parametrosBol);*/
		
/*			
		parametrosNC.setParametros(new ArrayList<Parametro>());
	
		CajaVirtualDAO cajaVirtualDao = new CajaVirtualDAOImpl();
		cajaVirtualDao.generaArchivoDeCierre(parametrosBol, 12278, "C:\\temp\\xddg.xls");
*/
		
//		PaperlessDAO paperless = new PaperlessDAOImpl("");
//		List<ResultadoPPL> listado = paperless.getListadoErroresPPL(-98);
//		for (int p=0;p<listado.size();p++){
//			System.out.println("FIRV ResultadoPPL cod: "+listado.get(p).toString());
//		}
		
		Properties prop = new Properties();
		ContextListener context = new ContextListener();
		prop = context.contextInit(prop, "2");
		PoolBDs.OpenConnections(prop);
		Parametros parametrosBol = new Parametros();
		//Parametros parametrosNC = new Parametros();
		ParametrosDAO parametrosDAO = new ParametrosDAOImpl();
		parametrosBol.setParametros(new ArrayList<Parametro>());
		
		parametrosDAO.getParametrosCajaVirtual(2,parametrosBol.getParametros(), "1");
		
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		caja.existeCajaCerrada(parametrosBol, "29/09/2017");
		
		String variable = "Msg: ORA-00001: restricci�n �nica (MARKETPLACE_ADM.INDEX_ID_APERTURA_CIERRE) violada";
		
		if (variable.indexOf("ORA-00001")>0){
			
			System.out.println("encontrado");
			
		}
		else{
			
			System.out.println("No encontrado");
			
		}
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		int nroTrx    = caja.resNroTransaccion(parametrosBol, null);
		boolean cajaAbierta=false;
		int maxIntentos = Integer.parseInt(parametrosBol.buscaValorPorNombre("INTENTOS_APER_CAJA_ERROR_CONSTRAINT"));
		int intentos = 1;
		RetornoEjecucion retorno = new RetornoEjecucion();
		int nroIdApertura = 4956;
		//BPL Se ejecuta para el error UniqueConstraint
		while (!cajaAbierta && intentos  <= maxIntentos){
			System.out.println("Intento de ejecuci�n de Registrar Apertura Cierre en Modelo Extendido " + intentos);

			//se obtiene mayor id apertura
			//int nroIdApertura = caja.obtieneMayorIdAperturaCierre();
			
//			retorno = caja.registraAperturaCierreCaja(parametrosBol,nroTrx,0,"17/10/2017","",Constantes.TIPO_LLAMADO_APERTURA,nroIdApertura);	
			retorno = null;
			if (retorno.getCodigo().intValue() == Constantes.EJEC_SIN_ERRORES){
				cajaAbierta=true;							
			}
			else if(retorno.getCodigo().intValue() == Constantes.EJEC_CON_ERRORES 
					&& retorno.getMensaje().indexOf(Constantes.ERROR_UNIQUE_CONSTRAINT)>Constantes.NRO_CERO ){
				intentos++;
				nroIdApertura++;
			}
			else{
				intentos = maxIntentos;
			}
		}
	}
	
	
//	public static void TestCortarStrGGC(){
//		
//		String GFG = "@@MONTO A FINANCIAR              10060@IMPUESTO TIMBRES                  40@TASA MENSUAL   3,02% /@COSTO TOTAL DEL CREDITO        11140@VALOR  CUOTA  MES 1 A  3          2775@VALOR  CUOTA  MES  4            2776@COMISION:  000000000000@";
//		
//		
//	}
	
	public static String tramaToPPL() throws RemoteException{

		
//		OnlineStub stub = new OnlineStub("http://10.0.157.125:8060/axis2/services/Online.OnlineHttpSoap11Endpoint/");
/*
		EndpointReference to = new EndpointReference("http://10.0.157.156:8060/axis2/services/Online.OnlineHttpSoap11Endpoint/");
		ServiceClient serviceClient = new ServiceClient();
		Options options = serviceClient.getOptions();
		options.setTo(to);
		serviceClient.setOptions(options);
		
		stub._setServiceClient(serviceClient);
*/
		String trama = "EN|61|0|2017-10-13|||1|3|83382700-6|COMERCIAL ECCSA S.A.|GRANDES TIENDAS|521300||||039|075016470|CASA MATRIZ: HUERFANOS 1052 4� PISO INTERIOR|SANTIAGO|SANTIAGO|10777930-2|Prueba ITF  ||direccion 1111 |SANTIAGO||13947209-8||3260|0|19.00|620|3880"+
		"DE|1|0|SKU|2094020940000||PENDRIVE SAKAR 8GB STAR WARS STAR T|1|1390||||1390"+
		"DE|2|0|SKU|2000303728982||COSTO DE ENVIO EST RM SANTIAGO SP|1|2490||||2490"+
		"ST|1|SUBTOTAL|3880"+
		"RE|1|39||527745|2017-10-13|1|DEVOLUCI�N MERC."+
		"PE||39|8|10:17:09|85502484|www.ripley.cl|0966666666|TRES MIL OCHOCIENTOS OCHENTA||xxxxxx@gmail.com||||||"+
		"PLID|PEN|1|NRO TRANSACCION/DOCUMENTO: 9992"+
		"PLID|PEN|2|ORDEN DE COMPRA: 9742011"+
		"PLID|PEN|3|VENDEDOR:"+
		"PLID|PEN|4|"+
		"PLID|PEN|5|"+
		"PLID|PEN|6|VEN 85502484   FECHA 13/10/17"+
		"PLID|PEN|7|SUPERVISOR 87654321"+
		"PLID|PEN|8|"+
		"PLID|PDE|1|1|DEMON||| 0390050000179287530001 - [9324160]"+
		"PLID|PDE|2|1|DEMON|||   - []"+
		"PLID|PMP|1|TARJETA BANCARIA DEBITO"+
		"PLID|PMP|2|CTA. CTE. NRO.: "+
		"PLID|PMP|3|NOMBRE TIT.: Prueba ITF  "+
		"PLID|PLAT|1|MARCA: Debito"+
		"PLID|PLAT|2|Efectivo21708"+
		"PLID|PLAT|3|COD. AUTORIZADOR: 1213"+
		"PLID|PLAT|4|ACEPTO PAGAR SEGUN CONTRATO CON EMISOR"+
		"PLID|PLDT|1|DIRECCION DESPACHO - []"+
		"PLID|PLDT|2|direccion 1111 "+
		"PLID|PLDT|3|REGION: REG.METROPOLITANA"+
		"PLID|PLDT|4|COMUNA: SANTIAGO"+
		"PLID|PLDT|5|";
		
//		OnlineGenerationBol request = new OnlineGenerationBol();
//
//		request.setArgs0(83382700);
//		request.setArgs1("gen_ripley");
//		request.setArgs2("abc123");
//		request.setArgs3(trama);
//		request.setArgs4(1);
//		request.setArgs5(6);
//		
//		OnlineGenerationBolResponse response = new OnlineGenerationBolResponse();
//		
//		response = stub.onlineGenerationBol(request);
		
//		System.out.println(response.get_return());
		return null;
	}
	
	public static String formatTimestamp(String formato, Timestamp timestamp) {
		String salida = null;
		try {
			SimpleDateFormat fmtFecha = new SimpleDateFormat(formato);
			salida = fmtFecha.format(timestamp);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return salida;
	}

	
}
