package cl.ripley.omnicanalidad.logic.boleta;

import cl.ripley.omnicanalidad.util.PredicadosProcesoBoleta;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.text.StrBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ripley.dao.ICajaDAO;
import com.ripley.dao.IInsertaPagoEnTiendaDAO;
import com.ripley.dao.IOrdenCompraDAO;
import com.ripley.dao.dto.DetalleTrxBoDTO;
import com.ripley.dao.dto.DocumentoElectronicoDTO;
import com.ripley.dao.dto.InsertaPagoEnTiendaDTO;
import com.ripley.dao.dto.InsertaPagoEnTiendaRespDTO;
import com.ripley.dao.dto.TraceImpresionDTO;
import com.ripley.dao.dto.TraceRposDTO;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallBoleta;
import com.ripley.restservices.model.backOfficeRest.RequestBO;
import com.ripley.restservices.model.bcvRest.PrepareCallBoletaBCV;
import com.ripley.restservices.model.bcvRest.RequestBcv;
import com.ripley.restservices.model.bcvRest.ResponseBcv;
import com.ripley.restservices.model.bigticket.ObjectBT;
import com.ripley.restservices.model.bigticket.PrepareCallBoletaBigTicketDTO;
import com.ripley.restservices.model.bigticket.ResponseBT;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.impresion.ConsultaOcImpresionResponse;
import com.ripley.restservices.model.pagoTiendaRest.PagoEnTienda;
import com.ripley.restservices.model.rpos.RposResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVenta;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.ArticulosVentaOC;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.Linea;
import cl.ripley.omnicanalidad.bean.OrdenesDeCompra;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.ResultadoPPL;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.OrdenCompraDAO;
import cl.ripley.omnicanalidad.dao.PaperlessDAO;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.dao.impl.VoucherTemplateDAOImpl;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.ClienteSMTP;
import cl.ripley.omnicanalidad.util.CodErrorSystemExit;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.NumerosAPalabras;
import cl.ripley.omnicanalidad.util.PoolBDs;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;
import java.util.function.Predicate;
import org.apache.commons.lang3.SerializationUtils;

/**
 * Clase que contiene la lógica de la impresión de boletas.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 * <br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class EsclavoBoleta extends Thread implements Runnable {

    private static final AriLog logger = new AriLog(EsclavoBoleta.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    @Autowired
    private ParametrosDAO paramDAO;

    @Autowired
    private CajaVirtualDAO caja;

    @Autowired
    private GeneracionDTEDAO generacionDTE;

    @Autowired
    private PaperlessDAO paperless;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IInsertaPagoEnTiendaDAO insertaPagoEnTiendaDAO;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private NotaVentaDAO notaVentaDAO;

    @Autowired
    private IOrdenCompraDAO ordenCompraDAO;

    @Autowired
    private ICajaDAO cajaDAO;

    @Autowired
    private OperacionesCaja operacionesCaja;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonHttpMessageConverter;

    @Autowired
    private OrdenCompraDAO ordenesCompraDAO;

    private String folioSiiRpos;
    private Integer numCajaRpos;

    public EsclavoBoleta() {

    }

    /**
     * Generación de la cabecera de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionCabecera2(TramaDTE trama, Parametros parametros) throws AligareException {

        StrBuilder respuesta = new StrBuilder();
        //Inicio Encabezado
        respuesta.append(Constantes.TRAMA_ENCABEZADO).append(Constantes.PIPE);
        //Codigo documento PPL
        //pos 1
        respuesta.append(String.format("%02d", trama.getTipoDoc().getCodPpl())).append(Constantes.PIPE);

        //Serie + "-" + correlativo documento
        //Se setea la serie sin correlativo, al parecer este correlativo lo da PPL al llamar a SUNAT
        //pos 2
        String initSerie = Constantes.VACIO;

        //mientra deberemos generar un correlativo ya que PPL Peru no lo hace.
        respuesta.append(initSerie).append(Constantes.PIPE);
        //tipo NC
        //pos 3
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //referencia NC
        //pos 4
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //sustento
        //pos 5
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //fecha emision
        //pos 6
        String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, parametros);
        respuesta.append(fechaEmision).append(Constantes.PIPE);
        //tipo moneda
        //pos 7
        respuesta.append(parametros.buscaValorPorNombre("TIPO_MONEDA")).append(Constantes.PIPE);
        //ruc emisor
        //pos 8
        respuesta.append(parametros.buscaValorPorNombre("RUT_EMISOR")).append(Constantes.PIPE);
        //tipo identificador emisor
        //pos 9
        respuesta.append("6").append(Constantes.PIPE);
        //nombre comercial emisor
        //pos 10
        respuesta.append(parametros.buscaValorPorNombre("COMERCIAL")).append(Constantes.PIPE);
        //razon social emisor
        //pos 11
        respuesta.append(parametros.buscaValorPorNombre("RAZON_SOCIAL_EMISOR")).append(Constantes.PIPE);
        //codigo ubigeo emisor
        //pos 12
        respuesta.append(parametros.buscaValorPorNombre("COD_UBIGEO_EMISOR")).append(Constantes.PIPE);
        //direccion emisor
        //pos 13
        respuesta.append(parametros.buscaValorPorNombre("DIRECCION_EMISOR")).append(Constantes.PIPE);
        //departamento emisor
        //pos 14
        respuesta.append(parametros.buscaValorPorNombre("DEPTO_EMISOR")).append(Constantes.PIPE);
        //provincia emisor
        //pos 15
        respuesta.append(parametros.buscaValorPorNombre("PROVINCIA_EMISOR")).append(Constantes.PIPE);
        //distrito emisor
        //pos 16
        respuesta.append(parametros.buscaValorPorNombre("DISTRITO_EMISOR")).append(Constantes.PIPE);

        String ruc = null;
        String tipoIdentReceptor = null;
        String razonSocialRecep = null;

        if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA) {

            //ruc receptor
            //pos 17
            ruc = trama.getDatoFacturacion().getRutFactura();

            //tipo identificador receptor
            //pos 18
            tipoIdentReceptor = String.valueOf(Constantes.NRO_SEIS);

            //razon social receptor
            //pos 19
            razonSocialRecep = trama.getDatoFacturacion().getRazonSocialFactura();

        } else {

            //ruc receptor
            //pos 17
            ruc = String.valueOf(trama.getDespacho().getRutCliente());

            //tipo identificador receptor
            //pos 18
            tipoIdentReceptor = String.valueOf(Constantes.NRO_UNO);

            //razon social receptor
            //pos 19
            razonSocialRecep = trama.getDespacho().getNombreCliente();

        }

        //ruc receptor
        //pos 17
        respuesta.append(ruc).append(Constantes.PIPE);

        //tipo identificador receptor
        //pos 18
        respuesta.append(tipoIdentReceptor).append(Constantes.PIPE);

        //razon social receptor
        //pos 19
        if (razonSocialRecep == null) {
            razonSocialRecep = Util.rellenarString(Constantes.VACIO, Constantes.NRO_TRES, Constantes.FALSO, Constantes.CHAR_GUION);
        }
        if (Constantes.NRO_TRES > razonSocialRecep.trim().length()) {
            razonSocialRecep = Util.rellenarString(razonSocialRecep.trim(), Constantes.NRO_TRES, Constantes.FALSO, Constantes.CHAR_GUION);
        }
        respuesta.append(razonSocialRecep).append(Constantes.PIPE);

        //direccion receptor
        //pos 20
        String direccionReceptor = trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? trama.getDespacho().getDireccionDespacho() : trama.getDespacho().getDireccionCliente();
        respuesta.append(direccionReceptor).append(Constantes.PIPE);

        //montos
        BigDecimal monto_venta = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);//trama.getNotaVenta().getMontoVenta();
        BigDecimal iva = new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
        BigDecimal tasa_iva = iva.divide(new BigDecimal(100));

        BigDecimal montoTotalIvaD = monto_venta.subtract(monto_venta.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP));
        BigDecimal montoNetoInt = monto_venta.subtract(montoTotalIvaD);

        //monto neto
        //pos 21
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoNetoInt)).append(Constantes.PIPE);
        //monto impuesto
        //pos 22
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);
        //monto descuentos
        //pos 23
        respuesta.append(Constantes.NRO_CERO).append(Constantes.PIPE);
        //monto recargos
        //pos 24
        respuesta.append(Constantes.NRO_CERO).append(Constantes.PIPE);
        //monto total
        //pos 25
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, monto_venta)).append(Constantes.PIPE);
        //cod concepto tributario
        //pos 26
        respuesta.append(parametros.buscaValorPorNombre("COD_CONCEPTO_TRIBUTARIO")).append(Constantes.PIPE);
        //total venta neta
        //pos 27
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, monto_venta)).append(Constantes.PIPE);

        //numero documento identidad
        //pos 28
        respuesta.append(ruc).append(Constantes.PIPE);
        //tipo documento identidad
        //pos 29
        respuesta.append(tipoIdentReceptor).append(Constantes.PIPE);

        //codigo pais emisor
        //pos 30
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//urbanizacion emisor
//		//pos 31
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida codigo UB
//		//pos 32
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida direccion com
//		//pos 33
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida urbanizacion
//		//pos 34
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida provincia
//		//pos 35
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida departamento
//		//pos 36
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida distrito
//		//pos 37
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//partida codigo pais
//		//pos 38
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//llegada codigo UB
//		//pos 39
//		String partidaCodUB = String.format("%1$02d%2$04d", trama.getDespacho().getRegionFacturacion(), trama.getDespacho().getComunaFacturacion());
//		respuesta.append(partidaCodUB).append(Constantes.PIPE);
//		//llegada direccion com
//		//pos 40
//		respuesta.append(trama.getDespacho().getDireccionDespacho()).append(Constantes.PIPE);
//		//llegada urbanizacion
//		//pos 41
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//llegada provincia
//		//pos 42
//		respuesta.append(trama.getDespacho().getComunaDespachoDes()).append(Constantes.PIPE);
//		//llegada distrito
//		//pos 43
//		respuesta.append(trama.getDespacho().getRegionDespachoDes()).append(Constantes.PIPE);
//		//llegada distrito
//		//pos 44
//		respuesta.append(trama.getDespacho().getDireccionDespacho()).append(Constantes.PIPE);
//		//llegada codigo pais
//		//pos 45
//		respuesta.append(parametros.buscaValorPorNombre("CODIGO_PAIS")).append(Constantes.PIPE);
//		//placa vehiculo
//		//pos 46
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//certificado habilitacion
//		//pos 47
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//marca vehiculo
//		//pos 48
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//licencia conducir
//		//pos 49
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//ruc transportista
//		//pos 50
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//tipo documento ruc transportista
//		//pos 51
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//razon social transportista
//		//pos 52
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//condiciones de pago
//		//pos 53
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//orden de compra (no la de ripley)
//		//pos 54
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//nro contrato
//		//pos 55
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//nota general documento
//		//pos 56
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//valor seguro
//		//pos 57
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//valor flete
//		//pos 58
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//valor FOB
//		//pos 59
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//redondeo
//		//pos 60
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//codigo proveedor
//		//pos 61
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//tipo operacion
//		//pos 62
//		respuesta.append(String.format("%02d", trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? Constantes.NRO_OCHO : Constantes.NRO_DOCE)).append(Constantes.PIPE);
//		//total anticipos
//		//pos 63
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//codigo UBIGEO
//		//pos 64
//		respuesta.append(partidaCodUB).append(Constantes.PIPE);
//		//direccion completa
//		//pos 65
//		respuesta.append(trama.getDespacho().getDireccionDespacho()).append(Constantes.PIPE);
//		//urbanizacion
//		//pos 66
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//provincia
//		//pos 67
//		respuesta.append(trama.getDespacho().getComunaDespachoDes()).append(Constantes.PIPE);
//		//departamento
//		//pos 68
//		respuesta.append(trama.getDespacho().getRegionDespachoDes()).append(Constantes.PIPE);
//		//distrito
//		//pos 69
//		respuesta.append(trama.getDespacho().getDireccionDespacho()).append(Constantes.PIPE);
//		//codigo pais
//		//pos 70
//		respuesta.append(parametros.buscaValorPorNombre("CODIGO_PAIS")).append(Constantes.PIPE);
//		//placa vehiculo
//		//pos 71
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//fecha vencimiento factura
//		//pos 72
//		respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		//modalidad traslado remitente
//		//pos 73
//		respuesta.append(Constantes.VACIO);

        respuesta.appendNewLine();

        return respuesta;
    }

    /**
     * Generación del detalle de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionDetalle(TramaDTE trama, Parametros pcv) throws AligareException {
        StrBuilder respuesta = new StrBuilder();
        String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
        List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
        if (articuloVentas.size() > 0) {
            int secuencia = 1;
            for (int i = 0; i < articuloVentas.size(); i++) {
                ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
                if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { //tiene ripley y mkp
                    if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP) { // si es mkp me lo salto
                        continue;
                    }
                }

                //inicio trama
                respuesta.append(Constantes.TRAMA_DETALLE).append(Constantes.PIPE);
                //correlativo detalle
                //pos 1
                respuesta.append(secuencia++).append(Constantes.PIPE);

                //precio unitario
                //pos 2
                respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, articuloVentaTrama.getArticuloVenta().getPrecio())).append(Constantes.PIPE);

                //unidad medida
                //pos 3
                String uniMed = Constantes.UNIDAD_MEDIDA_PRODUCTO;
                if (Constantes.VACIO.equals(articuloVentaTrama.getArticuloVenta().getCodArticulo())
                        && Constantes.VACIO.equals(articuloVentaTrama.getArticuloVenta().getCodDespacho())
                        && Constantes.NRO_CERO == articuloVentaTrama.getArticuloVenta().getIndicadorEg()) {

                    uniMed = Constantes.UNIDAD_MEDIDA_PRODUCTO;

                } else {

                    uniMed = Constantes.UNIDAD_MEDIDA_SERVICIO;

                }
                respuesta.append(uniMed).append(Constantes.PIPE);

                //cantidad unidades
                //pos 4
//				respuesta.append(Constantes.UNIDAD_MEDIDA_SERVICIO.equals(uniMed) ? 
//						Constantes.NRO_UNO : articuloVentaTrama.getArticuloVenta().getUnidades()).append(Constantes.PIPE);
                respuesta.append(articuloVentaTrama.getArticuloVenta().getUnidades()).append(Constantes.PIPE);
                //monto item
                BigDecimal iva = new BigDecimal(pcv.buscaValorPorNombre("VALORIVA"));
                BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
                BigDecimal unoMasIGV = Constantes.NRO_UNO_B.add(tasa_iva);
                BigDecimal precio = articuloVentaTrama.getArticuloVenta().getPrecio();
                BigDecimal unidades = new BigDecimal(articuloVentaTrama.getArticuloVenta().getUnidades());
                BigDecimal valorVentaItem = ((precio.multiply(unidades)).subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento())).divide(unoMasIGV, 2, RoundingMode.HALF_UP);
                BigDecimal valorVentaUnitario = precio.divide(unoMasIGV, 2, RoundingMode.HALF_UP);
                //pos 5
                respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorVentaItem)).append(Constantes.PIPE);

                //codigo producto
                //pos 6
                respuesta.append(articuloVentaTrama.getArticuloVenta().getCodArticulo()).append(Constantes.PIPE);

                //tipo precio venta. Por defecto 01
                //pos 7
                respuesta.append("01").append(Constantes.PIPE);

                //valor venta unitario
                //pos 8
                respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorVentaUnitario)).append(Constantes.PIPE);

                //valor venta item
                //pos 9
                respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorVentaItem)).append(Constantes.PIPE);

                //nro de lotes
                //pos 10
                respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
                //marca
                //pos 11
                respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
                //pais origen item
                //pos 12
                respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
                //pais origen item
                //pos 13
                respuesta.append(Constantes.VACIO);

                respuesta.appendNewLine();

                //TODO: Agregar todos las subestructuras de DE
                respuesta.append(genDedi(articuloVentaTrama, pcv))
                        .append(genDedr(articuloVentaTrama, pcv))
                        .append(genDeim(articuloVentaTrama, pcv));

            }
        }

        return respuesta;
    }

    /**
     * Genera el DEDI de la trama PPL
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 16-04-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param articulo
     * @return
     */
    private StrBuilder genDedi(ArticuloVentaTramaDTE articulo, Parametros pcv) {

//		BigDecimal iva =  new BigDecimal(pcv.buscaValorPorNombre("VALORIVA"));
//		BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
//		BigDecimal unoMasIGV = Constantes.NRO_UNO_B.add(tasa_iva);
        BigDecimal valorDescuento = (articulo.getArticuloVenta().getMontoDescuento() != null) ? articulo.getArticuloVenta().getMontoDescuento() : Constantes.NRO_CERO_B;
        //.divide(unoMasIGV, 2, RoundingMode.HALF_UP);

        StrBuilder respuesta = new StrBuilder();

        respuesta.append(Constantes.TRAMA_DETALLE_DESCRIPCION_ITEM).append(Constantes.PIPE)
                //pos1
                //descripcion item
                .append(articulo.getArticuloVenta().getDescRipley()
                        //CUD
                        + ((articulo.getArticuloVenta().getCodDespacho() == null) ? Constantes.VACIO : "@@" + articulo.getArticuloVenta().getCodDespacho())
                        //Descuento
                        + (valorDescuento.compareTo(BigDecimal.ZERO) == 0 ? Constantes.VACIO : "@@" + Constantes.TRAMA_DESC_DESCUENTO + "@@" + valorDescuento + Constantes.GUION)).append(Constantes.PIPE)
                //pos 2
                //nota complementario a la descripcion del item
                .append(Constantes.VACIO).append(Constantes.PIPE)
                //pos 3
                .append(Constantes.VACIO).append(Constantes.PIPE)
                //pos 4
                .append(Constantes.VACIO).append(Constantes.PIPE)
                //pos 5
                .append(Constantes.VACIO).append(Constantes.PIPE);

        //Codigo Producto SUNAT
        String codigoSunat = articulo.getArticuloVenta().getCodigoSunat();
        codigoSunat = Util.validarCodigoSunat(codigoSunat, pcv.buscaValorPorNombre("CODIGO_DEFAULT_PRODUCTO_SUNAT"));
        //pos 6
        respuesta.append(codigoSunat).append(Constantes.PIPE).appendNewLine();

        BigDecimal precio = articulo.getArticuloVenta().getPrecio();
        BigDecimal unidades = new BigDecimal(articulo.getArticuloVenta().getUnidades());
        BigDecimal valorVentaItem = precio.multiply(unidades);

        //Segunda parte
        //pos 1
        respuesta.append(Constantes.TRAMA_DETALLE_DESCRIPCION_ITEM).append(Constantes.PIPE)
                //pos 2
                .append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorVentaItem));

        respuesta.appendNewLine();

        return respuesta;

    }

    /**
     * Genera el DEDR de la trama PPL
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 16-04-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param articulo
     * @return
     */
    private StrBuilder genDedr(ArticuloVentaTramaDTE articulo, Parametros pcv) {

        StrBuilder respuesta = new StrBuilder();

        //Recargo
        respuesta.append(Constantes.TRAMA_DETALLE_DESCUENTOS_RECARGOS).append(Constantes.PIPE);

        BigDecimal iva = new BigDecimal(pcv.buscaValorPorNombre("VALORIVA"));
        BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
        BigDecimal unoMasIGV = Constantes.NRO_UNO_B.add(tasa_iva);
        BigDecimal precio = articulo.getArticuloVenta().getPrecio();
        BigDecimal unidades = new BigDecimal(articulo.getArticuloVenta().getUnidades());
        BigDecimal monto = precio.multiply(unidades);
        BigDecimal valorVentaItem = monto.divide(unoMasIGV, 2, RoundingMode.HALF_UP);
        BigDecimal valorDescuento = (articulo.getArticuloVenta().getMontoDescuento() != null
                ? articulo.getArticuloVenta().getMontoDescuento() : Constantes.NRO_CERO_B).divide(unoMasIGV, 2, RoundingMode.HALF_UP);
        BigDecimal factorDescuento = BigDecimal.ZERO;
        if (monto.compareTo(BigDecimal.ZERO) != 0) {
            factorDescuento = (articulo.getArticuloVenta().getMontoDescuento()).divide(monto, 2, RoundingMode.HALF_UP);
        }
        //pos1
        //indicador tipo (cargo = true; descuento = false)
        respuesta.append(String.valueOf(Boolean.FALSE)).append(Constantes.PIPE)
                //pos 2
                //monto descuento o recargo
                .append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorDescuento)).append(Constantes.PIPE)
                //pos 3
                .append(Constantes.STRING_CEROX2).append(Constantes.PIPE)
                //pos 4
                .append(String.format(Locale.US, Constantes.FORMATO_5_DECIMALES, factorDescuento)).append(Constantes.PIPE)
                //pos5
                .append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, valorVentaItem));

        respuesta.appendNewLine();

        return respuesta;

    }

    /**
     * Genera el DEIM de la trama PPL
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 16-04-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param articulo
     * @return
     */
    private StrBuilder genDeim(ArticuloVentaTramaDTE articulo, Parametros pcv) {

        StrBuilder respuesta = new StrBuilder();

        //montos
        BigDecimal monto_articulo = articulo.getArticuloVenta().getPrecio()
                .multiply(new BigDecimal(articulo.getArticuloVenta().getUnidades()))
                .subtract(articulo.getArticuloVenta().getMontoDescuento() != null
                        ? articulo.getArticuloVenta().getMontoDescuento() : Constantes.NRO_CERO_B);

        BigDecimal iva = new BigDecimal(pcv.buscaValorPorNombre("VALORIVA"));
        BigDecimal tasa_iva = iva.divide(new BigDecimal(100));

        BigDecimal montoTotalIvaD = monto_articulo.subtract(monto_articulo.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP));
        BigDecimal montoNetoInt = monto_articulo.subtract(montoTotalIvaD);

        //Impuesto por item
        respuesta.append(Constantes.TRAMA_DETALLE_IMPUESTO).append(Constantes.PIPE)
                //pos1
                //importe total de tributo para el item
                .append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);

        //pos 2
        //base imponible
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoNetoInt)).append(Constantes.PIPE)
                //pos 3
                //importe explicito a tributar
                .append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);

        String tasaImpuesto = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, iva);
        String afectacionIGV = Constantes.STRING_DIEZ;
        String identificacionTributo = Constantes.IDENT_TRIBUTO;
        String nombreTributo = Constantes.NOMBRE_TRIBUTO_IGV;
        String codigoTipoTributo = Constantes.COD_TIP_TRIBUTO_VAT;

        if (Constantes.NRO_CERO_COMA_CEROCERO_B.compareTo(montoTotalIvaD) == 0) {
            tasaImpuesto = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, Constantes.NRO_CERO_COMA_CEROCERO_B);
            afectacionIGV = Constantes.STRING_TREINTA;
            identificacionTributo = Constantes.IDENT_TRIBUTO_9998;
            nombreTributo = Constantes.NOMBRE_TRIBUTO_INA;
            codigoTipoTributo = Constantes.COD_TIP_TRIBUTO_FRE;
        }

        //pos 4
        //tasa impuesto
        respuesta.append(tasaImpuesto).append(Constantes.PIPE)
                //pos 5
                //tipo impuesto
                .append(Constantes.VACIO).append(Constantes.PIPE)
                //pos 6
                //afectacion del IGV
                .append(afectacionIGV).append(Constantes.PIPE)
                //pos 7
                //sistema del ISC
                .append(Constantes.VACIO).append(Constantes.PIPE)
                //pos 8
                //identificacion del tributo
                .append(identificacionTributo).append(Constantes.PIPE)
                //pos 9
                //nombre del tributo
                .append(nombreTributo).append(Constantes.PIPE)
                //pos 10
                //codigo tipo tributo
                .append(codigoTipoTributo).append(Constantes.PIPE);
        respuesta.appendNewLine();

        return respuesta;

    }

    /**
     * Generación del documento Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionDoc(TramaDTE trama, Parametros parametros) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        //inicio trama doc
        respuesta.append(Constantes.TRAMA_DOC).append(Constantes.PIPE);

        //montos
        BigDecimal monto_venta = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);//trama.getNotaVenta().getMontoVenta();
        BigDecimal iva = new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
        BigDecimal tasa_iva = iva.divide(new BigDecimal(100));

        BigDecimal montoTotalIvaD = monto_venta.subtract(monto_venta.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP));
        BigDecimal montoNetoInt = monto_venta.subtract(montoTotalIvaD);

        //codigos conceptos tributarios
        //pos 1
        respuesta.append(parametros.buscaValorPorNombre("COD_CONCEPTO_TRIBUTARIO")).append(Constantes.PIPE);
        //monto neto
        //pos 2
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoNetoInt)).append(Constantes.PIPE);
        //monto total documento percepcion
        //pos 3
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, new BigDecimal(Constantes.NRO_CERO))).append(Constantes.PIPE);
        //base imponible percepcion
        //pos 4
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, new BigDecimal(Constantes.NRO_CERO))).append(Constantes.PIPE);
        //porcentaje detraccion
        //pos 5
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //regimen percepcion
        //pos 6
        respuesta.append(Constantes.VACIO);

        respuesta.appendNewLine();

        return respuesta;
    }

    /**
     * Generación de la seccion DN de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionDn(TramaDTE trama) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        //inicio trama doc
        respuesta.append(Constantes.TRAMA_DN).append(Constantes.PIPE);

        //montos
        BigDecimal monto_venta = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);

        //numero filas. Siempre 1
        //pos 1
        respuesta.append(Constantes.NRO_UNO).append(Constantes.PIPE);
        //codigo leyenda
        //pos 2
        respuesta.append(Constantes.CODIGO_LEYENDA).append(Constantes.PIPE);
        //glosa leyenda
        //pos 3
        respuesta.append(NumerosAPalabras.convertir(monto_venta)).append(" SOLES").append(Constantes.PIPE);

        //desc tramo viaje
        //pos 4
        respuesta.append(Constantes.VACIO);

        respuesta.appendNewLine();

        return respuesta;
    }

    /**
     * Generación de la sección DR de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionDr(TramaDTE trama) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        //inicio trama doc
        respuesta.append(Constantes.TRAMA_DR).append(Constantes.PIPE);

        //tipo movimiento. siempre false.
        //pos 1
        respuesta.append(Boolean.toString(Boolean.FALSE)).append(Constantes.PIPE);
        //codigo motivo
        //pos 2
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //glosa
        //pos 3
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //moneda
        //pos 4
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //monto
        //pos 5
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //identificacion tributo
        //pos 6
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //nombre tributo
        //pos 7
        respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
        //codigo tipo tributo
        //pos 8
        respuesta.append(Constantes.VACIO).appendNewLine();

        return respuesta;
    }

    /**
     * Generación de la sección DI de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionDi(TramaDTE trama, Parametros parametros) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        //montos
        BigDecimal montoVentaAfecto = Util.getSubtotalAfecto(trama, Constantes.ES_MKP_RIPLEY, parametros);
        BigDecimal iva = new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
        BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
        BigDecimal montoTotalIvaD = montoVentaAfecto.subtract(montoVentaAfecto.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP));

        respuesta.append(Constantes.TRAMA_DI).append(Constantes.PIPE);
        //sumatoria tributo 1
        //pos 1
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);
        //sumatoria tributo 2
        //pos 2
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);
        //identificacion tributo
        //pos 3
        respuesta.append(Constantes.IDENT_TRIBUTO).append(Constantes.PIPE);
        //nombre tributo
        //pos 4
        respuesta.append(Constantes.NOMBRE_TRIBUTO_IGV).append(Constantes.PIPE);
        //codigo tipo tributo
        //pos 5
        respuesta.append(Constantes.COD_TIP_TRIBUTO_VAT).append(Constantes.PIPE);
        //pos 6
        respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoVentaAfecto.subtract(montoTotalIvaD))).appendNewLine();

        BigDecimal montoVentaInAfecto = Util.getSubtotalInAfecto(trama, Constantes.ES_MKP_RIPLEY, parametros);
        if (montoVentaInAfecto.compareTo(Constantes.NRO_CERO_B) != 0) {
            respuesta.append(Constantes.TRAMA_DI).append(Constantes.PIPE);
            //sumatoria tributo 1
            //pos 1
            respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoTotalIvaD)).append(Constantes.PIPE);
            //sumatoria tributo 2
            //pos 2
            respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, Constantes.NRO_CERO_COMA_CEROCERO_B)).append(Constantes.PIPE);
            //identificacion tributo
            //pos 3
            respuesta.append(Constantes.IDENT_TRIBUTO_9998).append(Constantes.PIPE);
            //nombre tributo
            //pos 4
            respuesta.append(Constantes.NOMBRE_TRIBUTO_INA).append(Constantes.PIPE);
            //codigo tipo tributo
            //pos 5
            respuesta.append(Constantes.COD_TIP_TRIBUTO_FRE).append(Constantes.PIPE);
            //pos 6
            respuesta.append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoVentaInAfecto)).appendNewLine();
        }

        return respuesta;
    }

    /**
     * Generación de la sección PES de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param idx
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionPes(TramaDTE trama, int idx) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        String seccionMensaje = Constantes.VACIO;

        switch (idx) {
            case Constantes.NRO_UNO:
                seccionMensaje = Constantes.MENSAJES_AT;
                break;

            case Constantes.NRO_DOS:
                seccionMensaje = Constantes.MENSAJES_DT;
                break;

            default:
                seccionMensaje = Constantes.VACIO;
                break;
        }

        respuesta.append(Constantes.TRAMA_PES).append(Constantes.PIPE);
        //codigo tipo tributo
        //pos 1
        respuesta.append(seccionMensaje).appendNewLine();
        return respuesta;
    }

    /**
     * Generación de la sección PESD de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param type
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionPesd(TramaDTE trama, Parametros parametros, boolean flagRpos) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        int fila = Constantes.NRO_CERO;
        String formaPago = Util.getFormaPago(trama);

        if (Constantes.FORMA_PAGO_TIENDA.equals(formaPago)) {

            formaPago = Constantes.FORMA_PAGO_EFECTIVO;

            switch (String.valueOf(trama.getTarjetaBancaria().getGlosa())) {

                case Constantes.MP_GLOSA_RIPLEY:
                    formaPago = Constantes.FORMA_PAGO_TARJETA_RIPLEY;
                    break;

                case Constantes.MP_GLOSA_BANCARIA:
                    formaPago = Constantes.FORMA_PAGO_TDC;
                    break;

                case Constantes.MP_GLOSA_EFECTIVO:
                    formaPago = Constantes.FORMA_PAGO_EFECTIVO;
                    break;

                default:
                    break;

            }

        }

        boolean isTB = Boolean.FALSE;
        boolean isTR = Boolean.FALSE;
        boolean isEfectivo = Boolean.FALSE;
        boolean isPaypal = Boolean.FALSE;
        boolean pagoEntrega = Boolean.FALSE;
        boolean isSafety = Boolean.FALSE;

//		switch(type) {
//		case MEDIO_PAGO:
        String panDoc = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
                && !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
                ? Util.getPan(trama) : Constantes.VACIO;
        String codigoAutorizadorDoc = Constantes.VACIO;
        String montoDoc = Constantes.VACIO;

        String fechaPrimerVtoDoc = Constantes.VACIO;

        if (Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {

            isTR = Boolean.TRUE;
            fechaPrimerVtoDoc = String.format("%1$td/%1$tm/%1$tY", new Date(trama.getTarjetaRipley().getFechaPrimerVencto().getTime()));
            codigoAutorizadorDoc = String.valueOf(trama.getTarjetaRipley().getCodigoAutorizacion());
            montoDoc = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, trama.getTarjetaRipley().getMontoCapital());

        } else if (trama.getTarjetaBancaria().getCorrelativoVenta() != null
                && trama.getTarjetaBancaria().getCorrelativoVenta().longValue() != Constantes.NRO_CERO) {

            montoDoc = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, trama.getTarjetaBancaria().getMontoTarjeta());

            codigoAutorizadorDoc = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
                    && !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
                    ? trama.getTarjetaBancaria().getCodigoAutorizador() : Constantes.VACIO;

            if (Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)) {
                //efectivo
                isEfectivo = Boolean.TRUE;

            } else if (Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
                //paypal
                isPaypal = Boolean.TRUE;

            } else if (Constantes.FORMA_PAGO_DEBITO.equals(formaPago) || Constantes.FORMA_PAGO_TDC.equals(formaPago)) {
                // tarjeta bancaria
                isTB = Boolean.TRUE;

            } else {

                pagoEntrega = Boolean.TRUE;
                fechaPrimerVtoDoc = Constantes.VACIO;

            }

        }

        respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("MEDIO DE PAGO: ");

        if (Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)
                && Constantes.CANAL_SAFETYPAY.equals(trama.getTarjetaBancaria().getDescripcionCanal())) {

            respuesta.append(Constantes.CANAL_SAFETYPAY);
            isSafety = Boolean.TRUE;

        } else {

            respuesta.append(formaPago);

        }

        respuesta.appendNewLine();

        if (isTR) {

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("N° TARJETA : ").append(panDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("MONTO : ").append(montoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COD.AUTORIZACION : ").append(codigoAutorizadorDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("PRIMER VENCIMIENTO : ").append(fechaPrimerVtoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("VALOR CUOTA S/ : ").append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, trama.getTarjetaRipley().getValorCuota() != null ? trama.getTarjetaRipley().getValorCuota() : BigDecimal.ZERO)).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("CUOTAS : ").append(trama.getTarjetaRipley().getPlazo()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();

        } else if (isEfectivo || isTB) {

            if (isSafety) {

                respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                        .append(++fila).append(Constantes.PIPE)
                        .append("CODIGO PAGO: ").append(codigoAutorizadorDoc).appendNewLine();

            } else {

                String codAux = isEfectivo ? "CIP : *********" + codigoAutorizadorDoc : "N° TARJETA : " + panDoc;

                respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                        .append(++fila).append(Constantes.PIPE)
                        .append(codAux).appendNewLine();

            }

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("MONTO : ").append(montoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COD.AUTORIZACION : ").append(!isSafety ? codigoAutorizadorDoc : Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("VALOR CUOTA S/ : ").append(trama.getTarjetaBancaria().getIdCanal().intValue() != Constantes.CANAL_CONTRAENTREGA_BANCARIA.intValue()
                    && trama.getTarjetaBancaria().getIdCanal().intValue() != Constantes.CANAL_CONTRA_ENTREGA.intValue()
                    ? montoDoc : Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();

        } else if (isPaypal) {

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("ID PAYPAL : ").append(codigoAutorizadorDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("MONTO : ").append(montoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COD.AUTORIZACION : ").append(codigoAutorizadorDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("VALOR CUOTA S/ : ").append(montoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("TIPO CAMBIO S/ : ").append(Constantes.VACIO).appendNewLine()
                    //				.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    //				.append(++fila).append(Constantes.PIPE)
                    //				.append("USUARIO PAYPAL : ").append(trama.getDespacho().getEmailIdPaypal() == null ? Constantes.VACIO : trama.getDespacho().getEmailIdPaypal()).appendNewLine()
                    //				.append("USUARIO PAYPAL : ").append(Constantes.VACIO).appendNewLine()

                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();

        } else if (pagoEntrega) {

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("MONTO : ").append(montoDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COD.AUTORIZACION : ").append(codigoAutorizadorDoc).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("VALOR CUOTA S/ : ").append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();

        }

        Long identificadorTransaccion = trama.getTarjetaBancaria().getIdentificadorTransaccion();

        if (identificadorTransaccion != null && identificadorTransaccion != Constantes.NRO_CERO) {

            respuesta
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("ID DE TRANSACCION : ").append(identificadorTransaccion).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("_________________________________________").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .appendNewLine();

        }
        if (!flagRpos) {
            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("DOCUMENTOS REFERENCIADOS").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("_________________________________________").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("TIPO DOCUMENTO: ").append(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? Constantes.FACTURA : Constantes.BOLETA).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("ORDEN DE COMPRA: ").append(trama.getNotaVenta().getCorrelativoVenta()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("FECHA: ").append(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros)).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("DIRECCION DESPACHO: ").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("Dirección: ").append(trama.getDespacho().getDireccionDespacho()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("DISTRITO: ").append(trama.getDespacho().getComunaDespachoDes()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("PROVINCIA: ").append(trama.getDespacho().getRegionDespachoDes()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("DEPARTAMENTO: ").append(trama.getDespacho().getRegionDespachoDes()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("RECIBI CONFORME MERCADERIA Y/O SERVICIOS").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("- POR CUENTA DE TIENDAS POR DEPARTAMENTO RIPLEY S.A").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();
        } else {
            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("DOCUMENTOS REFERENCIADOS").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("_________________________________________").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("TIPO DOCUMENTO: ").append(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? Constantes.FACTURA : Constantes.BOLETA).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("ORDEN DE COMPRA: ").append(trama.getNotaVenta().getCorrelativoVenta()).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("FECHA: ").append(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros)).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("ENTREGA INMEDIATA").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("RECIBI CONFORME MERCADERIA Y/O SERVICIOS").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("- POR CUENTA DE TIENDAS POR DEPARTAMENTO RIPLEY S.A").appendNewLine()
                    .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append(Constantes.VACIO).appendNewLine();
        }

//			break;
//			
//		case PUNTOS:
//		BigDecimal porcentajePuntos = new BigDecimal(parametros.buscaValorPorNombre("PORCENTAJE_PUNTOS")).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
        BigDecimal montoCompra = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);

//		BigDecimal totalPuntos = montoCompra.multiply(porcentajePuntos);
        String totalPuntos = String.valueOf((int) Math.floor(Double.parseDouble(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, montoCompra))));

        if (Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("UD. GANO ").append(totalPuntos).append(" PUNTOS RIPLEY ").appendNewLine();

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COMPRANDO CON SU TARJETA RIPLEY").appendNewLine();

        } else if (trama.getTarjetaBancaria().getCorrelativoVenta() != null
                && trama.getTarjetaBancaria().getCorrelativoVenta().longValue() != Constantes.NRO_CERO) {

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("UD. HUBIESE GANADO ").append(totalPuntos).append(" PUNTOS ").appendNewLine();

            respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                    .append(++fila).append(Constantes.PIPE)
                    .append("COMPRANDO CON SU TARJETA RIPLEY").appendNewLine();

        }

        respuesta.append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append(Constantes.VACIO).appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("==========================================").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("Condiciones Cambio y Devolución").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("1. Producto nuevo y sin señales de uso.").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("2. Empaques completos y en buen estado.").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("3. Plazo de 7 días desde la entrega.").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("4. Presenta tu comprobante de compra y DNI.").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("Conoce otras condiciones en www.ripley.com.pe").appendNewLine()
                .append(Constantes.TRAMA_PESD).append(Constantes.PIPE)
                .append(++fila).append(Constantes.PIPE)
                .append("==========================================").appendNewLine();

//			break;
//			
//		default:
//			break;
//		}
        return respuesta;
    }

    /**
     * Generación de la sección PE de Paperless.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param parametros
     * @param trx
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionPe(TramaDTE trama, Parametros parametros, Long trx, boolean flagRpos, SucursalRpos datosSuc) throws AligareException {
        StrBuilder respuesta = new StrBuilder();
        //1
        String hora = caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros);
        //2
        String tienda = "";
        if (!flagRpos) {
            tienda = Constantes.TIENDA_ONLINE;
        } else {
            tienda = String.valueOf(datosSuc.getSucursalPpl());
        }
        //3
        String nroCaja = String.valueOf(trama.getNotaVenta().getNumeroCaja());
        //4
        Long transaccion = trx;
        //5
        String cajero = "";
        if (!flagRpos) {
            cajero = trama.getVendedor().getRutVendedor();
        } else {
            cajero = trama.getNotaVenta().getEjecutivoVta();
        }
        //6
        String nombreCajero = "";
        if (!flagRpos) {
            nombreCajero = trama.getVendedor().getNombreVendedor();
        } else {
            nombreCajero = trama.getVendedor().getNombreVendedor().substring(Constantes.NRO_CERO, Constantes.NRO_SEIS);
        }
        //				trama.getNotaVenta().getEjecutivoVta() : trama.getNotaVenta().getOrigenVta();
        //7
        String serie = "";
//		if(!flagRpos)
        serie = parametros.buscaValorPorNombre("TERMINAL");
//		else
//			serie= datosSuc.getSerieSunat();
        //8
        String ticket = Constantes.VACIO;
        //9
        String sucursal = "";
        if (!flagRpos) {
            sucursal = parametros.buscaValorPorNombre("SUCURSAL") + " - TIENDA VIRTUAL";
        } else {
            sucursal = String.valueOf(datosSuc.getSucursal());
        }
        //10
        String direccionTienda = "";
        if (!flagRpos) {
            direccionTienda = parametros.buscaValorPorNombre("DIRECCION_ORIGEN");
        } else {
            direccionTienda = datosSuc.getDireccion();
        }
        //11
        String telefono = trama.getDespacho().getTelefonoCliente();
        //12
        String subtotal = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY));
        //13
        int unidadesVendidas = 0;
        for (ArticuloVentaTramaDTE art : trama.getArticuloVentas()) {

            if (Constantes.ES_MKP_RIPLEY == art.getIdentificadorMarketplace().getEsMkp()) {

                unidadesVendidas += art.getArticuloVenta().getUnidades();

            }

        }
        //14
        String tiendaReferencia = Constantes.VACIO;
        //15
        String cajaReferencia = Constantes.VACIO;
        //16
        String vendedorReferencia = Constantes.VACIO;
        //17
        String formaPago = Util.getFormaPago(trama);
        //18
        String oc = String.valueOf(trama.getNotaVenta().getCorrelativoVenta());

        //19
        String numCuenta = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
                && !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
                ? Util.getPan(trama) : Constantes.VACIO;
        //20
        String mesDiferido = trama.getTarjetaBancaria().getDiferido();
        //21
        String codConvenio = Constantes.VACIO;
        //22
        String numCuotas = Constantes.VACIO;
        //23
        String mesVencimiento = Constantes.VACIO;
        //24
        String montoCuota = Constantes.VACIO;
        //25
        String codVendedor = trama.getVendedor().getRutVendedor();
        //26
        String codAutoriz = Constantes.VACIO;

        if (Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {

            numCuotas = String.valueOf(trama.getTarjetaRipley().getPlazo());
            mesVencimiento = String.format("%1$td/%1$tm/%1$tY", new Date(trama.getTarjetaRipley().getFechaPrimerVencto().getTime()));

            if (trama.getTarjetaRipley().getValorCuota() != null) {

                montoCuota = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, trama.getTarjetaRipley().getValorCuota());

            }

            codAutoriz = String.valueOf(trama.getTarjetaRipley().getCodigoAutorizacion());

        } else {

            if (trama.getTarjetaBancaria().getMontoTarjeta() != null) {

                montoCuota = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
                        && !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
                        ? String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, trama.getTarjetaBancaria().getMontoTarjeta()) : Constantes.VACIO;

            }

            codAutoriz = !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
                    && !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
                    ? trama.getTarjetaBancaria().getCodigoAutorizador() : Constantes.VACIO;

        }

        //27
        String codNovios = Constantes.VACIO;
        //28
        String inicial = Constantes.VACIO;
        //29
        String numeroInternoSap = Constantes.VACIO;
        //30
        String impresora = trama.getNotaVenta().getEnviaDoceFisico().intValue() == Constantes.NRO_UNO
                ? parametros.buscaValorPorNombre("IMPRESORA") : Constantes.VACIO;

        //31
        String email = trama.getDespacho().getEmailCliente();

        //32
        String detraccion = Constantes.VACIO;
        //33
        String distritoTienda = parametros.buscaValorPorNombre("DISTRITO_TIENDA");
        //34
        String provinciaTienda = parametros.buscaValorPorNombre("PROVINCIA_TIENDA");
        //35
        String deptoTienda = parametros.buscaValorPorNombre("DEPTO_TIENDA");
        //36
        String tipDocLoyalty = Constantes.VACIO;
        //37
        String numDocLoyalty = Constantes.VACIO;
        //38
        String direccion = Constantes.VACIO;
        //39
        String codBarra = caja.sysdateFromDual(Constantes.FECHA_DDMMYY, parametros)
                + String.format("%1$05d%2$03d%3$04d",
                        trama.getNotaVenta().getNumeroSucursal(),
                        Integer.parseInt(nroCaja),
                        transaccion);

        String recaudador = Constantes.VACIO;

        if (trama.getPagoEnTienda().getCorrelativoVenta() != null && trama.getPagoEnTienda().getCorrelativoVenta().intValue() != Constantes.NRO_CERO) {

            PagoEnTienda pt = trama.getPagoEnTienda();

            ticket = String.valueOf(pt.getNroTransaccion());
            recaudador = pt.getRecaudadorNombres() + Constantes.ESPACIO_X_5 + pt.getRecaudadorCodigo();

        } else {

            ticket = String.valueOf(trx);

        }

        respuesta.append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Hora").append(Constantes.PIPE).append(hora).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Tienda").append(Constantes.PIPE).append(tienda).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Caja").append(Constantes.PIPE).append(nroCaja).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Trx").append(Constantes.PIPE).append(transaccion).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Cajero").append(Constantes.PIPE).append(cajero).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("NombreCajero").append(Constantes.PIPE).append(nombreCajero + " " + cajero).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Serie").append(Constantes.PIPE).append(serie).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Ticket").append(Constantes.PIPE).append(ticket).appendNewLine();

        if (trama.getPagoEnTienda().getCorrelativoVenta() != null && trama.getPagoEnTienda().getCorrelativoVenta().intValue() != Constantes.NRO_CERO) {

            respuesta.append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                    .append("RECAUDADOR: ").append(Constantes.PIPE).append(recaudador).appendNewLine();

        }

        respuesta.append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Sucursal").append(Constantes.PIPE).append(sucursal).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("DirecciónTienda").append(Constantes.PIPE).append(direccionTienda).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Telefono").append(Constantes.PIPE).append(telefono).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Subtotal").append(Constantes.PIPE).append(subtotal).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Unidades").append(Constantes.PIPE).append(unidadesVendidas).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("TiendaReferencia").append(Constantes.PIPE).append(tiendaReferencia).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CajaReferencia").append(Constantes.PIPE).append(cajaReferencia).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("VendedorReferencia").append(Constantes.PIPE).append(vendedorReferencia).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("FormaPago").append(Constantes.PIPE).append(formaPago).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("OCompra").append(Constantes.PIPE).append(oc).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("NCuenta").append(Constantes.PIPE).append(numCuenta).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("MesDiferido").append(Constantes.PIPE).append(mesDiferido).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodConvenio").append(Constantes.PIPE).append(codConvenio).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("NCuotas").append(Constantes.PIPE).append(numCuotas).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("MesVenc").append(Constantes.PIPE).append(mesVencimiento).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("MontoCuota").append(Constantes.PIPE).append(montoCuota).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodVendedor").append(Constantes.PIPE).append(codVendedor).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodAutoriz").append(Constantes.PIPE).append(codAutoriz).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodNovios").append(Constantes.PIPE).append(codNovios).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Inicial").append(Constantes.PIPE).append(inicial).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("NInterno").append(Constantes.PIPE).append(numeroInternoSap).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Impresora").append(Constantes.PIPE).append(impresora).appendNewLine()
                //Se limina para no enviar correo.
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Email").append(Constantes.PIPE).append(email).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Detraccion").append(Constantes.PIPE).append(detraccion).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("DistritoTienda").append(Constantes.PIPE).append(distritoTienda).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("ProvinciaTienda").append(Constantes.PIPE).append(provinciaTienda).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("DptoTienda").append(Constantes.PIPE).append(deptoTienda).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("TipDocLoyalty").append(Constantes.PIPE).append(tipDocLoyalty).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("NroDocLoyalty").append(Constantes.PIPE).append(numDocLoyalty).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("Direccion").append(Constantes.PIPE).append(direccion).appendNewLine()
                .append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodBarra").append(Constantes.PIPE).append(codBarra).appendNewLine();

        Integer establecimientoSunat = Integer.valueOf(parametros.buscaValorPorNombre(Constantes.ESTABLECIMIENTO_COMERCIAL));

        respuesta.append(Constantes.TRAMA_PE).append(Constantes.PIPE)
                .append("CodEstSUNAT").append(Constantes.PIPE).append(String.format(Constantes.FORMATO_4_CEROS_IZQ, establecimientoSunat)).appendNewLine();

        return respuesta;
    }

    /**
     * Generación de la sección ENEX de Paperless.
     *
     * @author Martin Corrales (Aligare).
     * @since 14-11-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param parametros
     * @param trx
     * @return
     * @throws AligareException
     */
//	private StrBuilder generacionENEX(TramaDTE trama, Parametros parametros, Long trx) throws AligareException{
    private StrBuilder generacionEnex(TramaDTE trama, Parametros parametros) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        //1 Version UBL
        String versionUbl = Constantes.PPL_VERSION_UBL;

        //2 Tipo de Operación 
        String tipoOperacion = parametros.buscaValorPorNombre(Constantes.PPL_TAG_TIPO_OPERACION);

        //3 Orden de Compra
        String correlativoVenta = Constantes.VACIO;

        //4 Redondeo
        String redondeo = Constantes.VACIO;

        //5 Total Anticipos
        String totalAnticipos = Constantes.VACIO;

        //6 Fecha de Vencimiento de la Factura
        String fechaVencimientoFactura = Constantes.VACIO;

        //7 Hora de Emisión
        LocalDateTime ldtFechaTrx = LocalDateTime.parse(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, parametros), Constantes.DATE_TIME_FORMATER_DDMMYYYY_HHMMSS);
        String horaEmision = Constantes.DATE_TIME_FORMATER_HH_MI_SS.format(ldtFechaTrx);
//		String horaEmision = caja.sysdateFromDual(Constantes.FECHA_HH24MISS, parametros);

        //8 Código asignado por SUNAT para el establecimiento anexo declarado en el RUC
        Integer establecimientoSunat = Integer.valueOf(parametros.buscaValorPorNombre(Constantes.ESTABLECIMIENTO_COMERCIAL));

        //9 Total Precio de Venta
        BigDecimal monto_venta = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);

        respuesta.append(Constantes.TRAMA_ENEX)
                .append(Constantes.PIPE).append(versionUbl)
                .append(Constantes.PIPE).append(tipoOperacion)
                .append(Constantes.PIPE).append(correlativoVenta)
                .append(Constantes.PIPE).append(redondeo)
                .append(Constantes.PIPE).append(totalAnticipos)
                .append(Constantes.PIPE).append(fechaVencimientoFactura)
                .append(Constantes.PIPE).append(horaEmision)
                .append(Constantes.PIPE).append(String.format(Constantes.FORMATO_4_CEROS_IZQ, establecimientoSunat))
                .append(Constantes.PIPE).append(String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, monto_venta)).appendNewLine();

        return respuesta;
    }

    /**
     * Obtiene el código único de grilla.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param sucursal
     * @param caja
     * @param nroTransaccion
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder getCodigoUnicoGrilla2(String sucursal, String caja, int nroTransaccion, Parametros parametros) throws AligareException {
        StrBuilder respuesta = new StrBuilder();
        respuesta.append(Constantes.TRAMA_NUMERO_UNICO_NU);
        String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
        String fecha = this.caja.sysdateFromDual(Constantes.FECHA_YYMMDD_HHMISS, parametros);
        String fechaArr[] = fecha.split(Constantes.SPACE);
        String codigoCaja = Util.rellenarString(caja, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
        String transaccion = Util.rellenarString(nroTransaccion + Constantes.VACIO, Constantes.NRO_SEIS, Constantes.VERDADERO, Constantes.CHAR_CERO);

        respuesta.append(codigoSucursal).append(Constantes.GUION).append(codigoCaja).append(Constantes.GUION).append(transaccion);
        respuesta.append(Constantes.GUION).append(fechaArr[0]).append(Constantes.GUION).append(fechaArr[1]);

        return respuesta;
    }

    /**
     * Obtiene glosa acepto
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @return
     * @throws AligareException
     */
    private StrBuilder getGlosaAcepto(TramaDTE trama) throws AligareException {
        StrBuilder respuesta = new StrBuilder();

        if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null) {
            respuesta.append(Constantes.TRAMA_PLAT_TRE_3).appendNewLine();
        }
//		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
//			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_1).appendNewLine();
//			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_2).appendNewLine();
//			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_3).appendNewLine();
//		}
//		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
/*
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
         */
//			respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR).appendNewLine();
//		}

        respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR).appendNewLine();

        return respuesta;
    }

    /**
     * Envía correo de recaudación
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param nroVoucher
     * @param pcv
     * @return
     * @throws AligareException
     */
    public boolean enviarEmailRecaudacion(TramaDTE trama, String nroVoucher, Parametros pcv) throws AligareException {
        boolean resultado = false;
        TemplateVoucher template = new TemplateVoucher();
        VoucherTemplateDAO templateDao = new VoucherTemplateDAOImpl();

        if (Constantes.IND_MKP_REGALO.equals(trama.getNotaVenta().getIndicadorMkp())) {
            template = templateDao.getTemplateByLlave("VOUCHER_RECA");
            if (template.getLlave() == null) {
                logger.traceInfo("enviarEmailRecaudacion", "Correo llave 'VOUCHER_RECA' no se encontr� o est� desactivado.");
                return false;
            }
            resultado = enviarEmailRecaudacionCDF(trama, nroVoucher, template, pcv);
        } else {
            template = templateDao.getTemplateByLlave("VOUCHER_MKP");
            if (template.getLlave() == null) {
                logger.traceInfo("enviarEmailRecaudacion", "Correo llave 'VOUCHER_MKP' no se encontr� o est� desactivado.");
                return false;
            }
            resultado = enviarEmailRecaudacionMKP(trama, nroVoucher, template, pcv);
        }

        return resultado;
    }

    /**
     * Envía correo de recaudación CDF.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param nroVoucher
     * @param template
     * @param pcv
     * @return
     * @throws AligareException
     */
    public boolean enviarEmailRecaudacionCDF(TramaDTE trama, String nroVoucher, TemplateVoucher template, Parametros pcv) throws AligareException {
        boolean resultado = false;
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		ClubDAO clubDAO = new ClubDAOImpl();
//		ClienteClub clienteClub = new ClienteClub();

        String destinatario = trama.getDespacho().getEmailCliente();

        String urlCloudFront = pcv.buscaValorPorNombre("URL_CLOUDFRONT");
        String nombreCliente = trama.getDespacho().getNombreCliente();
        String tipoTransaccion = "";
        if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) {
            tipoTransaccion = "Marketplace";
        } else if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)) {
            tipoTransaccion = "Club de Regalos";
        }

        String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, pcv);
        String rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
        String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null) ? trama.getNotaVenta().getCodigoRegalo().toString() : Constantes.VACIO;

        String rutRecaudacion = "";
//		logger.traceInfo("enviarEmailRecaudacion", "Cod Regalo1: "+trama.getNotaVenta().getCodigoRegalo());
//		if (trama.getNotaVenta().getCodigoRegalo() != null){
//			logger.traceInfo("enviarEmailRecaudacion", "Cod Regalo2: "+trama.getNotaVenta().getCodigoRegalo());
//			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
//			if (clienteClub != null){
//				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
//				logger.traceInfo("enviarEmailRecaudacion", "Rut Recaudacion: "+rutRecaudacion);
//			}
//		}

        String rutEvento = rutRecaudacion;
        String medioPago = "";
        if (trama.getTarjetaRipley().getCorrelativoVenta() != null) {
            medioPago = "TARJETA RIPLEY";
        }
        if (trama.getTarjetaBancaria().getCorrelativoVenta() != null) {
            if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)) {
                medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
            } else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)) {
                medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
            } else {
                medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
            }

            if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)) {
                medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
            } else {
                medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
            }

            medioPago = medioPago.toUpperCase();
        }
        if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null) {
            medioPago = "TARJETA REGALO EMPRESAS";
        }
        if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null && trama.getTarjetaRipley().getCorrelativoVenta() != null) {
            medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
        }

        String glosaFinanciera = "";

        StrBuilder respuesta = new StrBuilder();
        if (trama.getTarjetaRipley().getCorrelativoVenta() != null
                && trama.getTarjetaRipley().getGlosaFinancieraGsic() != null) {
            glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replace("[" + pcv.buscaValorPorNombre("LISTA_CARACTER_RARO") + "]", "");
            glosaFinanciera = glosaFinanciera.replace("@", "@@");
            String glosas[] = glosaFinanciera.split("@@");
            for (int g = 0; g < glosas.length; g++) {
                respuesta.append(glosas[g]).appendNewLine();
            }
            glosaFinanciera = respuesta.toString();
        }

        try {
            String html = template.getHtml();
            HashMap<String, String> valores = new HashMap<String, String>();
            valores.put("Url_cloudfront", urlCloudFront);
            valores.put("Nombre", nombreCliente);
            valores.put("Tipo_transaccion", tipoTransaccion);
            valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
            valores.put("Fecha_emision", fechaEmision);
            valores.put("Nro_evento", nroEvento);
            valores.put("Rut_recaudacion", rutEvento);
            valores.put("Nro_voucher", nroVoucher);
            valores.put("Rut", rutCliente);
            valores.put("Medio_Pago", medioPago);
            valores.put("Glosa_financiera", glosaFinanciera);
            HashMap<String, String> lista = new HashMap<String, String>();

            String codArticulo = "";
            String descRipley = "";
            int unidades = 0;
            int precio = 0;
            int valor_linea_detalle = 0;
            int subtotal = 0;
            int total = 0;
            int contador = 1;
            List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
            if (articuloVentas.size() > 0) {
                for (int i = 0; i < articuloVentas.size(); i++) {
                    ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
                    if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { //tiene ripley y mkp
                        if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY) { // si es ripley me lo salto
                            continue;
                        }
                    }
                    codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
                    descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
                    unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
                    precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
                    valor_linea_detalle = precio * unidades;
                    subtotal = subtotal + (valor_linea_detalle);
                    total = total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
                    lista.put("Codigo_Producto" + contador, codArticulo);
                    lista.put("Descripcion" + contador, descRipley);
                    lista.put("Cantidad" + contador, String.valueOf(unidades));
                    lista.put("Precio_Unidad" + contador, String.valueOf(precio));
                    lista.put("Precio_Producto" + contador, String.valueOf(valor_linea_detalle));
                    contador++;
                }
            }
            valores.put("Subtotal", String.valueOf(subtotal));
            valores.put("Total_compra", String.valueOf(total));

            ClienteSMTP clienteSmtp = new ClienteSMTP();
            html = clienteSmtp.completarHTML(html, valores, lista);
            resultado = clienteSmtp.enviarCorreo(pcv.buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "VOUCHER RECAUDACION OC: " + trama.getNotaVenta().getCorrelativoVenta(), html, pcv.buscaValorPorNombre("REMITENTE"), destinatario, pcv.buscaValorPorNombre("SILVERPOP_GROUP"));

        } catch (Exception e) {
            resultado = false;
        }
        return resultado;
    }

    /**
     * Envía correo de recaudación Marketplace.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param nroVoucher
     * @param template
     * @param pcv
     * @return
     * @throws AligareException
     */
    public boolean enviarEmailRecaudacionMKP(TramaDTE trama, String nroVoucher, TemplateVoucher template, Parametros pcv) throws AligareException {
        boolean resultado = false;
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		ClubDAO clubDAO = new ClubDAOImpl();
//		ClienteClub clienteClub = new ClienteClub();

        String destinatario = trama.getDespacho().getEmailCliente();

        String urlCloudFront = pcv.buscaValorPorNombre("URL_CLOUDFRONT");
        String sucursal = pcv.buscaValorPorNombre("CODIGO_SUCURSAL");
        String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CINCO, Constantes.VERDADERO, Constantes.CHAR_CERO);
        String nroCaja = pcv.buscaValorPorNombre("NRO_CAJA");
        String codigoCaja = Util.rellenarString(nroCaja, Constantes.NRO_TRES, Constantes.VERDADERO, Constantes.CHAR_CERO);
        String nroVoucherX10 = Util.rellenarString(nroVoucher, Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO);
        String nombreCliente = trama.getDespacho().getNombreCliente() + Constantes.SPACE + trama.getDespacho().getApellidoPatCliente();

        String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, pcv);
        String rutCliente = String.valueOf(trama.getDespacho().getRutCliente());
        String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null) ? trama.getNotaVenta().getCodigoRegalo().toString() : Constantes.VACIO;

        String codBoTipoDoc = trama.getTipoDoc().getCodBo().toString();
        String descTipoDoc = trama.getTipoDoc().getDescTipodoc();

        String rutRecaudacion = "";
//		if (trama.getNotaVenta().getCodigoRegalo() != null){
//			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
//			if (clienteClub != null){
//				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
//			}
//		}

        String rutEvento = rutRecaudacion;
        String medioPago = "";
        String titularMedioPago = "";
        String nroConvenioMP = "";
        String codAutorizadorMP = "";

        String nroUnico = getCodigoUnicoGrilla2(sucursal, nroCaja, Integer.parseInt(nroVoucher), pcv).toString();
        String glosaAcepto = getGlosaAcepto(trama).toString();

        if (trama.getTarjetaRipley().getCorrelativoVenta() != null) {
            medioPago = "TARJETA RIPLEY";
            codAutorizadorMP = trama.getTarjetaRipley().getCodigoAutorizacion().toString();
            titularMedioPago = "RUT CLIENTE: " + trama.getTarjetaRipley().getRutTitular();
            titularMedioPago = titularMedioPago + Constantes.SPACE + Constantes.GUION + Constantes.SPACE;
            titularMedioPago = titularMedioPago + "NOMBRE TIT.: ";
            titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getNombreCliente()) + Constantes.SPACE;
            titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoPatCliente()) + Constantes.SPACE;
            titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoMatCliente());
        }
        if (trama.getTarjetaBancaria().getCorrelativoVenta() != null) {
            if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)) {
                medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
            } else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)) {
                medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
            } else {
                medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
            }

            if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)) {
                medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
            } else {
                medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
            }

            medioPago = medioPago.toUpperCase();
            codAutorizadorMP = trama.getTarjetaBancaria().getCodigoAutorizador();
            if (trama.getTarjetaBancaria().getCodigoConvenio() != null) {
                nroConvenioMP = trama.getTarjetaBancaria().getCodigoConvenio().toString();
            }
        }
        if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null) {
            medioPago = "TARJETA REGALO EMPRESAS";
            codAutorizadorMP = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador().toString();
        }
        if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null && trama.getTarjetaRipley().getCorrelativoVenta() != null) {
            medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
        }

        String glosaFinanciera = "";

        StrBuilder respuesta = new StrBuilder();
        if (trama.getTarjetaRipley().getCorrelativoVenta() != null
                && trama.getTarjetaRipley().getGlosaFinancieraGsic() != null) {
            glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replace("[" + pcv.buscaValorPorNombre("LISTA_CARACTER_RARO") + "]", "");
            glosaFinanciera = glosaFinanciera.replace("@", "@@");
            String glosas[] = glosaFinanciera.split("@@");
            for (int g = 0; g < glosas.length; g++) {
                respuesta.append(glosas[g]).appendNewLine();
            }
            glosaFinanciera = respuesta.toString();
        }

        try {
            String html = template.getHtml();
            HashMap<String, String> valores = new HashMap<String, String>();
            valores.put("Url_cloudfront", urlCloudFront);
            valores.put("Nombre", nombreCliente);
            valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
            valores.put("Fecha_hora_emision", fechaEmision);

            valores.put("Nro_evento", nroEvento);
            valores.put("Rut_recaudacion", rutEvento);

            valores.put("Sucursal", codigoSucursal);
            valores.put("Caja", codigoCaja);
            valores.put("Nro_voucher", nroVoucher);
            valores.put("Nro_voucher_x10", nroVoucherX10);
            valores.put("Rut", rutCliente);

            valores.put("CodBO_Tipo_Doc", codBoTipoDoc);
            valores.put("Desc_Tipo_Doc", descTipoDoc);

            valores.put("Medio_Pago", medioPago);
            valores.put("Datos_titular_TRipley", titularMedioPago);
            valores.put("Nro_Convenio", nroConvenioMP);
            valores.put("Nro_autorizador", codAutorizadorMP);
            valores.put("Nro_unico", nroUnico);
            valores.put("Glosa_financiera", glosaFinanciera);
            valores.put("Glosa_Acepto", glosaAcepto);
            valores.put("Glosa_aviso", Constantes.GLOSA_AVISO);

            HashMap<String, String> lista = new HashMap<String, String>();

            String codArticulo = "";
            String descRipley = "";
            int unidades = 0;
            int precio = 0;
            int valor_linea_detalle = 0;
            int subtotal = 0;
            int total = 0;
            int contador = 1;
            String descuentoS = "";
            int descuentoI = 0;
            List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
            if (articuloVentas.size() > 0) {
                for (int i = 0; i < articuloVentas.size(); i++) {
                    ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
                    if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { //tiene ripley y mkp
                        if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY) { // si es ripley me lo salto
                            continue;
                        }
                    }
                    descuentoS = "";
                    descuentoI = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue();
                    if (descuentoI > 0) {
                        descuentoS = "-" + descuentoI;
                    }

                    codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
                    descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
                    unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
                    precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
                    valor_linea_detalle = precio * unidades;
                    subtotal = subtotal + (valor_linea_detalle);
                    total = total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
                    lista.put("Descuento" + contador, descuentoS);

                    lista.put("Codigo_Producto" + contador, codArticulo);
                    lista.put("Descripcion" + contador, descRipley);
                    lista.put("Cantidad" + contador, String.valueOf(unidades));
                    lista.put("Precio_Unidad" + contador, String.valueOf(precio));
                    lista.put("Precio_Producto" + contador, String.valueOf(valor_linea_detalle));

                    lista.put("Sub_Orden" + contador, articuloVentaTrama.getIdentificadorMarketplace().getOrdenMkp());
                    lista.put("Rut_proveedor" + contador, articuloVentaTrama.getIdentificadorMarketplace().getRutProveedor());
                    lista.put("Razon_social_proveedor" + contador, articuloVentaTrama.getIdentificadorMarketplace().getRazonSocial());

                    contador++;
                }
            }
            valores.put("Subtotal", String.valueOf(subtotal));
            valores.put("Total_compra", String.valueOf(total));

            ClienteSMTP clienteSmtp = new ClienteSMTP();
            html = clienteSmtp.completarHTML(html, valores, lista);
            resultado = clienteSmtp.enviarCorreo(pcv.buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "VOUCHER RECAUDACION MKP OC: " + trama.getNotaVenta().getCorrelativoVenta(), html, pcv.buscaValorPorNombre("REMITENTE"), destinatario, pcv.buscaValorPorNombre("SILVERPOP_GROUP"));

        } catch (Exception e) {
            resultado = false;
        }
        return resultado;
    }
    
    /**
     * Generación de la sección FP de Paperless.
     *
     * @author Tamara Águila Rodríguez (Aligare).
     * @since 09-03-2021
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul> 
     * @param parametros
     * @return
     * @throws AligareException
     */
    private StrBuilder generacionFp(TramaDTE trama, Parametros parametros) throws AligareException {
    	StrBuilder respuesta = new StrBuilder();
    	if(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA) {//si es factura se envia la nueva linea
    		//Posición 0
            respuesta.append(Constantes.TRAMA_FP).append(Constantes.PIPE);
            //Posición 1
            respuesta.append(Constantes.TRAMA_FORMA_PAGO).append(Constantes.PIPE);
            //Posición 2 
            respuesta.append(Constantes.TRAMA_CONTADO).append(Constantes.PIPE);
            //Posición 3
            respuesta.append(Constantes.PIPE);
            //Posición 4
            respuesta.append(Constantes.PIPE);
            //Posición 5
            //String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, parametros);
            //respuesta.append(Constantes.PIPE);	
            respuesta.appendNewLine();
    	}
        return respuesta;
    }
    

    /**
     * Procesa la nota de venta a partir del número de orden de compra, la
     * función y el número de caja.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param correlativoVenta
     * @param funcionMetodo
     * @param numCaja
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
    public GenericResponse procesarNotaVenta(Long correlativoVenta, Integer funcionMetodo, Integer numCaja, boolean isPagoTienda, PagoEnTienda pagoEnTienda, Integer sucursal) throws RestServiceTransactionException {
        logger.initTrace("procesarNotaVenta", "correlativoVenta = " + String.valueOf(correlativoVenta) + " - funcionMetodo = " + String.valueOf(funcionMetodo)
                + " - numCaja = " + String.valueOf(numCaja),
                new KeyLog("correlativoVenta", String.valueOf(correlativoVenta)),
                new KeyLog("funcionMetodo", String.valueOf(funcionMetodo)),
                new KeyLog("numCaja", String.valueOf(numCaja)),
                new KeyLog("sucursal", String.valueOf(sucursal)));

        GenericResponse resp = new GenericResponse();
        SucursalRpos datosSuc = new SucursalRpos();
        resp.setCodigo(Constantes.NRO_CERO);
        resp.setMensaje("OK");
        boolean flagRpos = false;
        TramaDTE inOrdenCompra = null;

        Integer trazaPPL = null;
        Integer trazaBO = null;
        Integer trazaBT = null;
        Integer trazaBCV = null;

        Parametros parametros = new Parametros();
        parametros.setParametros(new ArrayList<Parametro>());

        paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");

        parametros.reemplazaValorLista(Constantes.NRO_CAJA_BOLETAS, String.valueOf(numCaja));
        parametros.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(numCaja));
        if (sucursal != Constantes.NRO_CERO) {
            flagRpos = true;
            parametros.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(sucursal));
            parametros.reemplazaValorLista(Constantes.PARAM_CODIGO_SUCURSAL, String.format("%03d", sucursal));
            parametros.reemplazaValorLista(Constantes.PARAM_DESPACHO_RPOS, Constantes.STRING_UNO);
            datosSuc = caja.getDatosSuc(Integer.valueOf(parametros.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)),
                    Integer.valueOf(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
        }

        Parametro param = new Parametro();
        param.setNombre(Constantes.FUNCION);
        param.setValor(String.valueOf(funcionMetodo));
        parametros.getParametros().add(param);

        if (isPagoTienda) {
            //Si es pago en tienda entonces inserto datos de Medio de pago en ME y actualizo Medio de Pago Actual. NRO_CAJA_PAGO_TIENDA
            Integer nroCaja = Integer.valueOf(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_PAGO_TIENDA));
            parametros.reemplazaValorLista(Constantes.NRO_CAJA_BOLETAS, String.valueOf(nroCaja));
            parametros.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(nroCaja));

            InsertaPagoEnTiendaDTO insPagoTiendaDto = conversionService.convert(pagoEnTienda, InsertaPagoEnTiendaDTO.class);
            InsertaPagoEnTiendaRespDTO insPTRespDTO = null;
            try {

                insPTRespDTO = insertaPagoEnTiendaDAO.insertaPagoEnTienda(insPagoTiendaDto);

            } catch (Exception e) {
                logger.traceError("procesarNotaVenta", e.getMessage(), e);
                resp.setCodigo(Constantes.NRO_MENOSUNO);
                resp.setMensaje(e.getMessage());
                return resp;
            }

            if (insPTRespDTO.getCodigo() != Constantes.NRO_CERO) {
                resp.setCodigo(insPTRespDTO.getCodigo());
                resp.setMensaje(insPTRespDTO.getMensaje());
                return resp;
            }

            if (!notaVentaDAO.asignarCaja(nroCaja, correlativoVenta)) {
                resp.setCodigo(Constantes.NRO_MENOSUNO);
                resp.setMensaje("NOK: No se pudo asignar caja");
                return resp;
            }

        }

        List<TramaDTE> qOrdenCompra = generacionDTE.obtenerUnaOrdenParaGeneracion(correlativoVenta,
                new Integer(parametros.buscaParametroPorNombre("NRO_CAJA").getValor()),
                new Integer(parametros.buscaParametroPorNombre("SUCURSAL").getValor()),
                Constantes.NRO_UNO);

        if (qOrdenCompra == null || qOrdenCompra.isEmpty()) {

            logger.traceInfo("procesarNotaVenta", "No se encuentra la Orden: " + correlativoVenta, new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
            resp.setCodigo(Constantes.NRO_MENOSUNO);
            resp.setMensaje("No se encuentra la Orden");

            return resp;

        }

        List<DocumentoElectronicoDTO> docsElectronicos = ordenCompraDAO.getDocsElectronicosByCorrelativoVenta(correlativoVenta);

        Optional<DocumentoElectronicoDTO> optDocumentoElectronicoDTO = Optional.empty();

        if (!docsElectronicos.isEmpty()) {

            optDocumentoElectronicoDTO = Optional.ofNullable(docsElectronicos.get(Constantes.NRO_CERO));

            logger.traceInfo("procesarNotaVenta", "Orden de compra " + correlativoVenta + " ya tiene folio de PPL "
                    + (optDocumentoElectronicoDTO.isPresent() ? optDocumentoElectronicoDTO.get().getXmlDTE() : Constantes.VACIO));

        }

        Optional<List<DetalleTrxBoDTO>> optDetalleTrxBo = Optional.ofNullable(ordenCompraDAO.getDetalleTrxBoByCorrelativoVenta(correlativoVenta));

        Optional<List<TraceImpresionDTO>> optListTraceImpresionDTO = Optional.ofNullable(ordenCompraDAO.getTraceImpresionByOC(correlativoVenta));

        //JORTUZAR: recupero la traza si es que hay una, de lo contrario el Optional será vacío y se procesará todo.
        Optional<TraceImpresionDTO> optTraceImpresion = Optional.empty();

        if (optListTraceImpresionDTO.isPresent()) {

            optTraceImpresion = optListTraceImpresionDTO.get().stream()
                    .findFirst();

        }

        if (optTraceImpresion.isPresent()) {
            trazaPPL = optTraceImpresion.get().getPpl();
        }
        if (optTraceImpresion.isPresent()) {
            trazaBO = optTraceImpresion.get().getBo();
        }
        if (optTraceImpresion.isPresent()) {
            trazaBT = optTraceImpresion.get().getBt();
        }
        if (optTraceImpresion.isPresent()) {
            trazaBCV = optTraceImpresion.get().getBcv();
        }

        final boolean isRipleyProcessed = optTraceImpresion.isPresent() && optTraceImpresion.get().getBo() == Constantes.NRO_UNO;
        final boolean isMkpProcessed = optTraceImpresion.isPresent() && optTraceImpresion.get().getMkp() == Constantes.NRO_UNO;

        boolean isNewTrx = Boolean.FALSE;

        logger.traceInfo("procesarNotaVenta", "Orden de compra " + correlativoVenta + " traza TraceImpresionDTO = " + String.valueOf(optTraceImpresion.isPresent() ? optTraceImpresion.get() : null));

        ResultadoPPL resultadoPpl = new ResultadoPPL();
        Integer estadoActual = null;
        String fechaDDMMYYYY = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros);
        String fechaDDMMYYYYHHMMSS = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, parametros);
        String fechaYYYYMMDD = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, parametros);
        String xmlMail = "";
        int a = 0;
        int z = qOrdenCompra.size();
        Integer nroCaja = Integer.valueOf(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
        Integer nroSuc = Integer.valueOf(parametros.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
        LocalDateTime ldt = LocalDateTime.parse(fechaDDMMYYYYHHMMSS, DateTimeFormatter.ofPattern(Constantes.FECHA_DDMMYYYY_HHMMSS_LDT));

        logger.traceInfo("procesarNotaVenta", "************************ qOrdenCompra.size=" + qOrdenCompra.size());

        for (Iterator<TramaDTE> iterator = qOrdenCompra.iterator(); iterator.hasNext();) { //obtengo una OC de la cola
            inOrdenCompra = iterator.next();
            logger.traceInfo("procesarNotaVenta", "EsclavoBoleta " + this.getName() + " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " " + (++a) + " de " + z);
            estadoActual = inOrdenCompra.getNotaVenta().getEstado();
            int nroTransaccionAnterior = 0;

            int nroTransaccion = Constantes.NRO_CERO;

            Optional<String> optTrxPpl = Optional.empty();

            Optional<String> optFechaPpl = Optional.empty();

            if (optDocumentoElectronicoDTO.isPresent()) {

                optTrxPpl = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                        Constantes.TOKEN_TRX, Constantes.NRO_DOS);

                optFechaPpl = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                        Constantes.TOKEN_ENCABEZADO, Constantes.NRO_SEIS);

            }

            LocalDate localDateValid = null;

            if (optFechaPpl.isPresent()) {

                localDateValid = LocalDate.parse(optFechaPpl.get(), Constantes.DATE_TIME_FORMATER_YYYY_MM_DD);

            }

            Optional<LocalDate> optLocalDateValid = Optional.ofNullable(localDateValid);
            boolean esMismaFecha = optLocalDateValid.isPresent() ? optLocalDateValid.get().isEqual(ldt.toLocalDate()) : Boolean.FALSE;

            if (((Constantes.IND_MKP_RIPLEY.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp())
                    || Constantes.IND_MKP_AMBAS.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp()))
                    && !isRipleyProcessed && !esMismaFecha)
                    || Constantes.IND_MKP_MKP.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {

                nroTransaccion = operacionesCaja.cajaVirtual(parametros, Boolean.TRUE);
                isNewTrx = Boolean.TRUE;
                logger.traceInfo("procesarNotaVenta", "TRANSACCION NUEVA : " + nroTransaccion);

            } else {

                nroTransaccion = optTrxPpl.isPresent() ? Integer.valueOf(optTrxPpl.get()) : inOrdenCompra.getNotaVenta().getCorrelativoBoleta().intValue();
                logger.traceInfo("procesarNotaVenta", "TRANSACCION A REPROCESAR : " + nroTransaccion);
            }

            if (nroTransaccion == Constantes.NRO_CERO) {

                throw new RestServiceTransactionException("No se pudo obtener número de transacción", CodErrorSystemExit.APERTURA_CIERRE);

            }

            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_SIETE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 7", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
            }

            String nroFolio = optDocumentoElectronicoDTO.isPresent() ? optDocumentoElectronicoDTO.get().getXmlDTE().trim() : "BTV1-123456";

            String codUnico = getCodigoUnicoGrilla2(parametros.buscaValorPorNombre("CODIGO_SUCURSAL"), parametros.buscaValorPorNombre("NRO_CAJA"), nroTransaccion, parametros).toString();
            codUnico = codUnico.replace(Constantes.GUION, Constantes.VACIO);

            //generar trama para ir a PPL (ESTA TRAMA ES SOLO PARA INDICADOR_MKP = 0 y 2) y ES_MKP = 0
            StrBuilder trama = new StrBuilder();
            if ((Constantes.IND_MKP_RIPLEY.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp())
                    || Constantes.IND_MKP_AMBAS.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp()))) {

                logger.traceInfo("procesarNotaVenta", "BOLETA: " + (inOrdenCompra.getNotaVenta().getCorrelativoVenta()));

                //JORTUZAR: Si PPL ya fue procesado entonces no debo enviar nuevamente la trama
                if (!optTraceImpresion.isPresent() || optTraceImpresion.get().getPpl() == Constantes.NRO_CERO) {
                    TraceImpresionDTO tiDTO = new TraceImpresionDTO();
                    tiDTO.setCorrelativoVenta(correlativoVenta);
                    try {
                        ordenCompraDAO.upsertTraceImpresion(tiDTO);
                        PoolBDs.commitMESpringTrx();
                        logger.traceInfo("procesarNotaVenta", "Realiza primer upsertTraceImpresion", new KeyLog("CORRELATIVO", String.valueOf(correlativoVenta)));
                    } catch (DataAccessException e) {
                        logger.traceError("procesarNotaVenta", "Error al grabar traza procesar Nota Venta; " + e.getMessage(), e);
                    }
                    try {
                        trama
                                .append(generacionCabecera2(inOrdenCompra, parametros))
                                .append(generacionEnex(inOrdenCompra, parametros))
                                //							.append(generacionDoc(inOrdenCompra, parametros))
                                .append(generacionDn(inOrdenCompra))
                                .append(generacionDetalle(inOrdenCompra, parametros))
                                .append(generacionDi(inOrdenCompra, parametros))                               
                                .append(generacionFp(inOrdenCompra, parametros))//Nueva linea FP	                                                             
                                .append(generacionPe(inOrdenCompra, parametros, Long.parseLong(String.valueOf(nroTransaccion)), flagRpos, datosSuc))
                                .append(generacionPes(inOrdenCompra, Constantes.NRO_UNO))
                                .append(generacionPesd(inOrdenCompra, parametros, flagRpos))
                                .append(generacionPes(inOrdenCompra, Constantes.NRO_DOS))
                                .append(Constantes.TAB).append(Constantes.TRAMA_FIN_TRAMA);
                    } catch (AligareException e) {
                        logger.traceInfo("procesarNotaVenta", "Error en generacion trama PPL", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));

                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }
                        if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), String.valueOf(Constantes.NRO_CERO), fechaDDMMYYYYHHMMSS, Constantes.ERROR, "En generaqcion trama PPL BOL - msj: " + e.getMessage(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)) {
                            logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }

                        throw new RestServiceTransactionException("Error en generacion trama PPL", Constantes.NRO_VEINTINUEVE);
                    } catch (Exception ex) {
                        logger.traceError("procesarNotaVenta, error al generar trama", ex);
                        resp.setCodigo(Constantes.NRO_MENOSUNO);
                        resp.setMensaje("procesarNotaVenta, error al generar trama");
                        return resp;
                    }

                    if (Constantes.NRO_UNO == Integer.parseInt(parametros.buscaValorPorNombre(Constantes.PPL_SWITCH))) {
                        if (!flagRpos) {
                            resultadoPpl = paperless.generacionBoleta2(parametros.buscaValorPorNombre("PPL_RUT"), parametros.buscaValorPorNombre("PPL_LOGIN"), parametros.buscaValorPorNombre("PPL_PASS"), trama.toString(), Constantes.PPL_TIPO_GENERACION, Constantes.NRO_CUATRO);
                        } else {
                            resultadoPpl = paperless.generacionBoleta2(parametros.buscaValorPorNombre("PPL_RUT"), parametros.buscaValorPorNombre("PPL_LOGIN"), parametros.buscaValorPorNombre("PPL_PASS"), trama.toString(), Constantes.PPL_TIPO_GENERACION, Constantes.NRO_SEIS);
                        }

                        if (resultadoPpl != null && resultadoPpl.getCodigo() == Constantes.NRO_CERO) {
                            trazaPPL = Constantes.NRO_UNO;
                            if (!flagRpos) {
                                nroFolio = resultadoPpl.getMensaje();
                                if (!generacionDTE.ingresarDocumentoElectronico(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_BOLETAS)), inOrdenCompra.getNotaVenta().getNumeroSucursal(), trama.toString(), resultadoPpl.getTed(), xmlMail, "")) {
                                    logger.traceInfo("procesarNotaVenta", "Error al ingresar documento electronico", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                                }
                            } else {
                                String[] arrayPpl = resultadoPpl.getMensaje().split("\\|");
                                nroFolio = arrayPpl[Integer.valueOf(parametros.buscaValorPorNombre("PPL_POSICION_SERIE"))].concat(Constantes.GUION
                                        + arrayPpl[Integer.valueOf(parametros.buscaValorPorNombre("PPL_POSICION_CORRELATIVO"))]);
                                if (!generacionDTE.ingresarDocumentoElectronico(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_BOLETAS)), inOrdenCompra.getNotaVenta().getNumeroSucursal(), trama.toString(), String.valueOf(nroFolio), xmlMail, resultadoPpl.getMensaje())) {
                                    logger.traceInfo("procesarNotaVenta", "Error al ingresar documento electronico Rpos", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                                }
                            }

                            logger.initTrace("procesarNotaVenta", "Folio: " + nroFolio + " | TED: " + resultadoPpl.getTed());

                            //aumento el nroTrx en la tabla de control de transacciones
                            try {
                                cajaDAO.updTrxCaja(nroCaja, nroSuc, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
                            } catch (AligareException e) {
                                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                                }
                                throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
                            }

                            //Commit para grabar que la trx se envió a PPL.
                            PoolBDs.commitMESpringTrx();

                            //JORTUZAR: inserto a la traza que PPL está OK
                            TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                            ti.setCorrelativoVenta(correlativoVenta);
                            //ti.setPpl(Constantes.NRO_UNO);
                            ti.setPpl(trazaPPL);
                            try {
                                ordenCompraDAO.upsertTraceImpresion(ti);
                                PoolBDs.commitMESpringTrx();
                            } catch (DataAccessException e) {
                                logger.traceError("procesarNotaVenta", "Error al grabar traza PPL; " + e.getMessage(), e);
                            }
                            if (flagRpos) {
                                if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")), Constantes.NRO_DOS)) {
                                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2 Rpos", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                                }
                                return resp;
                            }

                        } else {

                            if (resultadoPpl != null && resultadoPpl.getMensaje() != null) {

                                List<ResultadoPPL> listadoErrores = paperless.getListadoErroresPPL(resultadoPpl.getCodigo());
                                boolean errorPPLSaltar = false;
                                if (listadoErrores.size() > 0) {

                                    for (int p = 0; p < listadoErrores.size(); p++) {
                                        ResultadoPPL errorPpl = listadoErrores.get(p);
                                        if (resultadoPpl.getMensaje().equalsIgnoreCase(errorPpl.getMensaje())) {
                                            errorPPLSaltar = true;
                                            break;
                                        }
                                    }

                                } else {
                                    errorPPLSaltar = false;
                                }

                                if (errorPPLSaltar) {
                                    logger.traceInfo("procesarNotaVenta", "Error en respuesta PPL BOLETA, codigo: " + resultadoPpl.getCodigo() + " - mensaje: " + resultadoPpl.getMensaje(), new KeyLog("codigo", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("mensaje", resultadoPpl.getMensaje()));

                                    if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                        logger.traceInfo("procesarNotaVenta", "Error al volver OC a estado Original, por error en PPL Cod " + resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                                    }

                                    throw new RestServiceTransactionException("Error en respuesta PPL BOLETA, codigo: " + resultadoPpl.getCodigo() + " - mensaje: " + resultadoPpl.getMensaje(),
                                            Constantes.NRO_UNO);
                                }
                            } else {
                                logger.traceInfo("procesarNotaVenta", "Error en respuesta PPL BOLETA, mensaje NULO");

                                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                    logger.traceInfo("procesarNotaVenta", "Error al volver OC a estado Original, por error en PPL Cod " + resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                                }

                                throw new RestServiceTransactionException("Error en respuesta PPL BOLETA, mensaje NULO", Constantes.NRO_UNO);
                            }

                            logger.traceInfo("procesarNotaVenta", "Error en respuesta PPL FOLIO", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));

                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                            if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), String.valueOf(Constantes.NRO_CERO), fechaDDMMYYYYHHMMSS, Constantes.ERROR, "En PPL FOLIO BOL - msj: " + resultadoPpl.getMensaje(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)) {
                                logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }

                            throw new RestServiceTransactionException("Error en respuesta PPL FOLIO. Codigo PPL = " + resultadoPpl.getCodigo() + " - Mensaje PPL = " + resultadoPpl.getMensaje(),
                                    Constantes.NRO_VEINTINUEVE);
                        }
                    } else {
                        nroFolio = "BTV1-123456";

                        //aumento el nroTrx en la tabla de control de transacciones
                        try {
                            cajaDAO.updTrxCaja(nroCaja, nroSuc, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
                        } catch (AligareException e) {
                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                            }
                            throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
                        }

                        //Commit para grabar que la trx al BO se grabó sin problemas.
                        PoolBDs.commitMESpringTrx();
                    }

                } else if (isNewTrx) {

                    //aumento el nroTrx en la tabla de control de transacciones
                    try {
                        cajaDAO.updTrxCaja(nroCaja, nroSuc, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
                    } catch (AligareException e) {
                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                        }
                        throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
                    }

                    //Commit para grabar que la trx al BO se grabó sin problemas.
                    PoolBDs.commitMESpringTrx();

                }

                // El objetivo de este bloque es respaldar las trazas antes que se envien a los satelites.
                // Imagino que debe ser para facilitar un envio posterior manual en caso de fallo del programa.
                if (!optTraceImpresion.isPresent() || optTraceImpresion.get().getBo() == Constantes.NRO_CERO) {
                    try {
                        //JORTUZAR: Se llena tabla para respaldar JSON
                        ObjectMapper om = jacksonHttpMessageConverter.getObjectMapper();

                        Optional<String> jsonBo = Optional.empty();
                        Optional<String> jsonBoMkp = Optional.empty();
                        Optional<List<String>> jsonBT = Optional.empty();
                        Optional<String> jsonBCV = Optional.empty();

                        if (Constantes.IND_MKP_RIPLEY.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())
                                || Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {

                            PrepareBoCallBoleta source = new PrepareBoCallBoleta();
                            source.setBoleta(nroFolio);
                            source.setEsMkp(Boolean.FALSE);
                            if (flagRpos) {
                                inOrdenCompra.getNotaVenta().setPcMac(parametros.buscaValorPorNombre(Constantes.PROP_PC_MAC_RPOS));
                                inOrdenCompra.getVendedor().setRutVendedor(inOrdenCompra.getNotaVenta().getEjecutivoVta());
                            }
                            source.setInOrdenCompra(regenerarTramaDteBOOL(inOrdenCompra));
                            source.setNroTransaccion(Long.valueOf(nroTransaccion));
                            source.setNroTrxAnterior(Long.valueOf(nroTransaccionAnterior));
                            source.setParametros(parametros);
                            source.setRipleyProcessed(isRipleyProcessed);
                            source.setMkpProcessed(isMkpProcessed);

                            RequestBO req = conversionService.convert(source, RequestBO.class);

                            try {
                                jsonBo = Optional.ofNullable(om.writeValueAsString(req));
                            } catch (JsonProcessingException e) {
                                logger.traceError("procesarNotaVenta", e);
                            }

                            if (Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {

                                source = new PrepareBoCallBoleta();
                                source.setBoleta(nroFolio);
                                source.setEsMkp(Boolean.TRUE);
                                source.setInOrdenCompra(inOrdenCompra);
                                source.setNroTransaccion(Long.valueOf(isRipleyProcessed ? nroTransaccion : nroTransaccion + Constantes.NRO_UNO));
                                source.setNroTrxAnterior(Long.valueOf(isRipleyProcessed ? (optTrxPpl.isPresent() ? Integer.valueOf(optTrxPpl.get()) : inOrdenCompra.getNotaVenta().getCorrelativoBoleta().intValue()) : nroTransaccion));
                                source.setParametros(parametros);
                                source.setRipleyProcessed(isRipleyProcessed);
                                source.setMkpProcessed(isMkpProcessed);

                                req = conversionService.convert(source, RequestBO.class);

                                try {
                                    jsonBoMkp = Optional.ofNullable(om.writeValueAsString(req));
                                } catch (JsonProcessingException e) {
                                    logger.traceError("procesarNotaVenta", e);
                                }

                            }

                            Optional<TramaDTE> optInOrdenCompra = Optional.of(inOrdenCompra);
                            Optional<Long> optTrx = Optional.of(Long.valueOf(nroTransaccion));

                            //JORTUZAR: Solo en el caso que sea un reproceso y ya se haya emitido la boleta PPL,
                            //			entonces rescato el documento electrónico si es que no está emitido obtengo los datos que se
                            //			utilizaron en la boleta ya que de lo contrario BT y BCV quedan con problemas para reconocer
                            //			la nota de venta (OC).
                            if (!optDocumentoElectronicoDTO.isPresent()) {

                                docsElectronicos = ordenCompraDAO.getDocsElectronicosByCorrelativoVenta(correlativoVenta);

                                optDocumentoElectronicoDTO = docsElectronicos.stream().findFirst();

                            }

                            Optional<String> optTrxPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                    Constantes.TOKEN_TRX, Constantes.NRO_DOS);

                            Optional<String> optFechaPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                    Constantes.TOKEN_ENCABEZADO, Constantes.NRO_SEIS);

                            Optional<String> optHoraPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                    Constantes.TOKEN_HORA, Constantes.NRO_DOS);

                            String fechaLocal = !optFechaPPL.isPresent() || !optHoraPPL.isPresent() ? null
                                    : (optFechaPPL.get() + Constantes.ESPACIO + optHoraPPL.get());

                            Optional<String> optFechaLocal = Optional.ofNullable(fechaLocal);

                            LocalDateTime ldtFechaTrx = LocalDateTime.now();

                            if (optFechaLocal.isPresent()) {

                                ldtFechaTrx = LocalDateTime.parse(optFechaLocal.get(), Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS);

                            } else {

                                ldtFechaTrx = LocalDateTime.parse(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, parametros), Constantes.DATE_TIME_FORMATER_DDMMYYYY_HHMMSS);

                            }

                            Optional<LocalDateTime> optLdtFechaTrx = Optional.of(ldtFechaTrx);

                            List<String> listJsonBT = new ArrayList<String>();
                            if (!flagRpos) {
                                Predicate<ArticuloVentaTramaDTE> predicadoEsCostoEnvio = PredicadosProcesoBoleta.obtenerPredicadoEsCostoEnvio();
                                Predicate<ArticuloVentaTramaDTE> predicadoEsExtraGarantia = PredicadosProcesoBoleta.obtenerPredicadoEsExtraGarantia();
                                Predicate<ArticuloVentaTramaDTE> predicadoEsMarketPlaceRipley = PredicadosProcesoBoleta.obtenerPredicadoEsMarketPlaceRipley();
                                listJsonBT = inOrdenCompra.getArticuloVentas().stream()
                                        .filter(predicadoEsMarketPlaceRipley)
                                        .filter(predicadoEsCostoEnvio.negate())
                                        .filter(predicadoEsExtraGarantia.negate())
                                        .map(it -> {
                                            PrepareCallBoletaBigTicketDTO pcbdto = new PrepareCallBoletaBigTicketDTO();
                                            pcbdto.setArt(it);
                                            pcbdto.setFechaBoleta(optLdtFechaTrx.get());
                                            pcbdto.setInOrdenCompra(optInOrdenCompra.get());
                                            pcbdto.setNroTransaccion(optTrxPPL.isPresent() ? Long.valueOf(optTrxPPL.get()) : optTrx.get());

                                            ObjectBT obj = conversionService.convert(pcbdto, ObjectBT.class);
                                            return obj;
                                        })
                                        .map(it -> {
                                            try {
                                                return om.writeValueAsString(it);
                                            } catch (JsonProcessingException e) {
                                                logger.traceError("procesarNotaVenta", e);
                                                return null;
                                            }
                                        })
                                        .collect(Collectors.toCollection(ArrayList::new));

                                jsonBT = Optional.ofNullable(listJsonBT);
                            }

                            PrepareCallBoletaBCV sourceBCV = new PrepareCallBoletaBCV();
                            sourceBCV.setFolioPPL(optDocumentoElectronicoDTO.isPresent() ? optDocumentoElectronicoDTO.get().getXmlDTE() : nroFolio);
                            sourceBCV.setNroTrx(optTrxPPL.isPresent() ? Long.valueOf(optTrxPPL.get()) : Long.valueOf(nroTransaccion));
                            if (flagRpos) {
                                inOrdenCompra.getNotaVenta().setNumeroSucursal(datosSuc.getSucursalPpl());
                                parametros.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(datosSuc.getSucursalPpl()));
                                parametros.reemplazaValorLista(Constantes.PARAM_CODIGO_SUCURSAL, String.format("%03d", datosSuc.getSucursalPpl()));
                                sourceBCV.setFlagRpos(flagRpos);
                            }
                            sourceBCV.setTrama(inOrdenCompra);
                            sourceBCV.setPcv(parametros);
                            sourceBCV.setLdtFechaTrx(ldtFechaTrx);
                            RequestBcv reqBCV = conversionService.convert(sourceBCV, RequestBcv.class);
                            try {
                                jsonBCV = Optional.ofNullable(om.writeValueAsString(reqBCV));
                            } catch (JsonProcessingException e) {
                                logger.traceError("procesarNotaVenta", e);
                            }
                            if (flagRpos) {
                                inOrdenCompra.getNotaVenta().setNumeroSucursal(sucursal);
                                parametros.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(sucursal));
                                parametros.reemplazaValorLista(Constantes.PARAM_CODIGO_SUCURSAL, String.format("%03d", sucursal));
                            }
                        }

                        if (jsonBo.isPresent()) {
                            System.out.println("json ripley: " + jsonBo.get());
                            ordenCompraDAO.insertJsonBk(correlativoVenta, Constantes.TIPO_LLAMADA_BO, jsonBo.get());
                        }

                        if (jsonBoMkp.isPresent()) {
                            System.out.println("json mkp: " + jsonBoMkp.get());
                            ordenCompraDAO.insertJsonBk(correlativoVenta, Constantes.TIPO_LLAMADA_BO, jsonBoMkp.get());
                        }

                        if (!flagRpos) {
                            if (jsonBT.isPresent()) {
                                jsonBT.get().forEach(it -> ordenCompraDAO.insertJsonBk(correlativoVenta, Constantes.TIPO_LLAMADA_BT, it));
                            }
                        }

                        if (jsonBCV.isPresent()) {
                            ordenCompraDAO.insertJsonBk(correlativoVenta, Constantes.TIPO_LLAMADA_BCV, jsonBCV.get());

                        }

                        //JORTUZAR: grabo los json
                        PoolBDs.commitMESpringTrx();
                    } catch (AligareException e) {
                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                        }
                        throw new RestServiceTransactionException("Error al registrar la informacion de la oc en la tabla CV_JSON_BROKER, AligareException: " + e.getMessage(), Constantes.NRO_VEINTINUEVE);
                    } catch (Exception e) {
                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                        }
                        throw new RestServiceTransactionException("Error al registrar la informacion de la oc en la tabla CV_JSON_BROKER, exception: " + e.getMessage(), Constantes.NRO_VEINTINUEVE);
                    }
                }

                //JORTUZAR: Genero trx si no se han enviado a BO todavía (reprocesos)
                if (!optTraceImpresion.isPresent() || optTraceImpresion.get().getBo() == Constantes.NRO_CERO) {
                    // NOTE: Se envia la traza a Servicio de BO (endpoint http post) dentro de creacionTransaccionesDte.
                    boolean estadoCreacionTransaccionesDte = generacionDTE.creacionTransaccionesDte(parametros, inOrdenCompra, nroTransaccion, nroTransaccion, codUnico, nroFolio, isRipleyProcessed, isMkpProcessed);
                    boolean estadoEnvioTrxBO = false;
                    if (estadoCreacionTransaccionesDte) {
                        estadoEnvioTrxBO = envioTrxBO(inOrdenCompra, parametros, nroTransaccion, nroFolio, isRipleyProcessed, isMkpProcessed);
                    }
                    if (estadoEnvioTrxBO) {
                        trazaBO = Constantes.NRO_UNO;
                    }
                    if (!estadoEnvioTrxBO) {
                        logger.traceInfo("procesarNotaVenta", "Error en crear transacciones DTE", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("CODIGO UNICO", codUnico));

                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }
                        if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) {
                            if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), nroFolio, fechaDDMMYYYYHHMMSS, Constantes.ERROR, "En satelites - orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)) {
                                logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        } else {
                            if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), nroFolio, fechaDDMMYYYYHHMMSS, Constantes.ERROR, "En satelites - orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_CERO, Constantes.NRO_CERO)) {
                                logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        }

                        throw new RestServiceTransactionException("Error en crear transacciones DTE",
                                Constantes.NRO_VEINTINUEVE);

                    } else {

                        //					if (estadoActual.intValue() == Constantes.NRO_TRECE){
                        //						if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTITRES), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_NC")), Constantes.NRO_DOS)){
                        //						//if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTITRES), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
                        //							logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 23", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        //						}
                        //					} else
                        //					
                        //					if (estadoActual.intValue() == Constantes.NRO_CATORCE){
                        //						if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTICUATRO), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_NC")), Constantes.NRO_DOS)){
                        //						//if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTICUATRO), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
                        //							logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 24", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        //						}
                        //					} 
                        //					else
                        if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")), Constantes.NRO_DOS)) {
                            //if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        } else {
                            /*						
							if (!enviarEmailBoleta(inOrdenCompra, nroFolio)){
								logger.traceInfo("process", "Error en envio email boleta", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO FOLIO PPL", nroFolio));
							}
                             */
//								generacionDTE.updateDireccionDespachoHistorico(inOrdenCompra);
                        }
                        if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) {
                            if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), nroFolio, fechaDDMMYYYYHHMMSS, Constantes.IMPRESION, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_CERO, Constantes.NRO_CERO)) {
                                logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        } else {
                            if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), nroFolio, fechaDDMMYYYYHHMMSS, Constantes.IMPRESION, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)) {
                                logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        }

                        //JORTUZAR: inserto a la traza que BO está OK
                        TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                        ti.setCorrelativoVenta(correlativoVenta);
                        ti.setPpl(trazaPPL);
                        ti.setBo(trazaBO);
                        try {
                            ordenCompraDAO.upsertTraceImpresion(ti);
                            PoolBDs.commitMESpringTrx();
                        } catch (DataAccessException e) {
                            logger.traceError("procesarNotaVenta", "Error al grabar traza BO; " + e.getMessage(), e);
                        }

                    }

                }

                //JORTUZAR: Envío a BT solo si no se ha enviado todavía a BT
                if (!optTraceImpresion.isPresent() || optTraceImpresion.get().getBt() == Constantes.NRO_CERO) {
                    if (!flagRpos) {
                        Optional<TramaDTE> optInOrdenCompra = Optional.of(inOrdenCompra);
                        Optional<Long> optNroTrxAnterior = Optional.of(Long.valueOf(nroTransaccion));

                        //JORTUZAR: Solo en el caso que sea un reproceso y ya se haya emitido la boleta PPL,
                        //			entonces rescato el documento electrónico si es que no está emitido obtengo los datos que se
                        //			utilizaron en la boleta ya que de lo contrario BT y BCV quedan con problemas para reconocer
                        //			la nota de venta (OC).
                        if (!optDocumentoElectronicoDTO.isPresent()) {

                            docsElectronicos = ordenCompraDAO.getDocsElectronicosByCorrelativoVenta(correlativoVenta);

                            optDocumentoElectronicoDTO = docsElectronicos.stream().findFirst();

                        }

                        Optional<String> optTrxPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                Constantes.TOKEN_TRX, Constantes.NRO_DOS);

                        Optional<String> optFechaPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                Constantes.TOKEN_ENCABEZADO, Constantes.NRO_SEIS);

                        Optional<String> optHoraPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                                Constantes.TOKEN_HORA, Constantes.NRO_DOS);

                        String fechaLocal = !optFechaPPL.isPresent() || !optHoraPPL.isPresent() ? null
                                : (optFechaPPL.get() + Constantes.ESPACIO + optHoraPPL.get());

                        Optional<String> optFechaLocal = Optional.ofNullable(fechaLocal);

                        LocalDateTime ldtFechaTrx = LocalDateTime.now();

                        if (optFechaLocal.isPresent()) {

                            ldtFechaTrx = LocalDateTime.parse(optFechaLocal.get(), Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS);

                        } else {

                            ldtFechaTrx = LocalDateTime.parse(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, parametros), Constantes.DATE_TIME_FORMATER_DDMMYYYY_HHMMSS);

                        }

                        Optional<LocalDateTime> optLdtFechaTrx = Optional.of(ldtFechaTrx);

                        Predicate<ArticuloVentaTramaDTE> predicadoEsCostoEnvio = PredicadosProcesoBoleta.obtenerPredicadoEsCostoEnvio();
                        Predicate<ArticuloVentaTramaDTE> predicadoEsExtraGarantia = PredicadosProcesoBoleta.obtenerPredicadoEsExtraGarantia();
                        Predicate<ArticuloVentaTramaDTE> predicadoEsMarketPlaceRipley = PredicadosProcesoBoleta.obtenerPredicadoEsMarketPlaceRipley();

                        List<String> resultadosBT = inOrdenCompra.getArticuloVentas().stream()
                                .filter(predicadoEsMarketPlaceRipley)
                                .filter(predicadoEsCostoEnvio.negate())
                                .filter(predicadoEsExtraGarantia.negate())
                                .collect(ArrayList<String>::new,
                                        (lista, art) -> {
                                            try {
                                                objectBigTicket(optInOrdenCompra.get(), parametros, (optTrxPPL.isPresent() ? Long.valueOf(optTrxPPL.get()) : optNroTrxAnterior.get()), art, optLdtFechaTrx.get());//TODO: Enviar nroTrx anterior
                                                lista.add("OK; Correlativo Venta = " + art.getArticuloVenta().getCorrelativoVenta() + "; Correlativo Item = " + art.getArticuloVenta().getCorrelativoItem());
                                            } catch (Exception e) {
                                                logger.traceError("procesarNotaVenta", "Problema en llamada a BigTicket. Mensaje error = " + e.getMessage(), e);
                                                lista.add("NOK; Correlativo Venta = " + art.getArticuloVenta().getCorrelativoVenta() + "; Correlativo Item = " + art.getArticuloVenta().getCorrelativoItem());
                                            }
                                        },
                                        List::addAll);

                        logger.traceInfo("procesarNotaVenta", "resultados llamadas a BT = " + String.valueOf(resultadosBT));

                        boolean nok = resultadosBT.stream()
                                .anyMatch(it -> it.contains("NOK;"));

                        if (nok) {
                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }

                            resp.setCodigo(Constantes.NRO_VEINTINUEVE);
                            resp.setMensaje("Fallo al enviar artículos a BT. Ver logs.");
                            return resp;

                        } else {
                            trazaBT = Constantes.NRO_UNO;
                            //JORTUZAR: inserto a la traza que BT está OK
                            TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                            ti.setCorrelativoVenta(correlativoVenta);
//                            ti.setPpl(Constantes.NRO_UNO);
//                            ti.setBo(Constantes.NRO_UNO);
//                            ti.setBt(Constantes.NRO_UNO);
                            ti.setPpl(trazaPPL);
                            ti.setBo(trazaBO);
                            ti.setBt(trazaBT);
                            try {
                                ordenCompraDAO.upsertTraceImpresion(ti);
                                PoolBDs.commitMESpringTrx();
                            } catch (DataAccessException e) {
                                logger.traceError("procesarNotaVenta", "Error al grabar traza BT; " + e.getMessage(), e);
                            }
                        }
                    } else {
                        TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                        ti.setCorrelativoVenta(correlativoVenta);
//                        ti.setPpl(Constantes.NRO_UNO);
//                        ti.setBo(Constantes.NRO_UNO);
//                        ti.setBt(Constantes.NRO_CERO);
                        trazaBT = Constantes.NRO_CERO;//??
                        ti.setPpl(trazaPPL);
                        ti.setBo(trazaBO);
                        ti.setBt(trazaBT);
                        try {
                            ordenCompraDAO.upsertTraceImpresion(ti);
                            PoolBDs.commitMESpringTrx();
                        } catch (DataAccessException e) {
                            logger.traceError("procesarNotaVenta", "Error al grabar traza BT; " + e.getMessage(), e);
                        }
                    }

                }

                //Llamar a BCV.
                boolean resultBcv = Boolean.TRUE;

                //JORTUZAR: Enviar BCV solo si el switch está en 1 y si no se ha procesado
                if (Constantes.NRO_UNO == Integer.parseInt(parametros.buscaValorPorNombre(Constantes.BCV_SWITCH))
                        && (!optTraceImpresion.isPresent() || optTraceImpresion.get().getBcv() == Constantes.NRO_CERO)) {
                    try {
                        if (flagRpos) {
                            inOrdenCompra.getNotaVenta().setNumeroSucursal(datosSuc.getSucursalPpl());
                            parametros.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(datosSuc.getSucursalPpl()));
                            parametros.reemplazaValorLista(Constantes.PARAM_CODIGO_SUCURSAL, String.format("%03d", datosSuc.getSucursalPpl()));
                        }
                        resultBcv = llamarBcv(inOrdenCompra, parametros, Long.valueOf(nroTransaccion), nroFolio, optDocumentoElectronicoDTO, flagRpos);
                        if (flagRpos) {
                            inOrdenCompra.getNotaVenta().setNumeroSucursal(sucursal);
                            parametros.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(sucursal));
                            parametros.reemplazaValorLista(Constantes.PARAM_CODIGO_SUCURSAL, String.format("%03d", sucursal));
                        }
                    } catch (Exception e) {
                        logger.traceError("procesarNotaVenta", "Problema en llamada a BCV. Mensaje error = " + e.getMessage(), e);
                        resultBcv = Boolean.FALSE;
                    }

                }

                if (!resultBcv) {

                    logger.traceInfo("procesarNotaVenta", "Error al enviar orden a BCV", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));

                    //JORTUZAR: Si BCV se cae no se debe hacer rollback de la TRX ya que el sistema que manda es BOOL
                    // y en este punto la TRX ya se encuentra en BOOL.
                    resp.setCodigo(Constantes.NRO_VEINTINUEVE);
                    resp.setMensaje("Error al enviar orden a Broker - BCV");

                    if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                        logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                    }

                    return resp;

                } else {

                    //JORTUZAR: inserto a la traza que BCV está OK
                    trazaBCV = Constantes.NRO_UNO;
                    TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                    ti.setCorrelativoVenta(correlativoVenta);
                    ti.setPpl(trazaPPL);
                    ti.setBo(trazaBO);
                    ti.setBt(trazaBT);
                    ti.setBcv(trazaBCV);
                    try {
                        ordenCompraDAO.upsertTraceImpresion(ti);
                        PoolBDs.commitMESpringTrx();
                    } catch (DataAccessException e) {
                        logger.traceError("procesarNotaVenta", "Error al grabar traza BCV; " + e.getMessage(), e);
                    }
                }
                if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")), Constantes.NRO_DOS)) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }

            } else if (optTraceImpresion.isPresent()
                    && optTraceImpresion.get().getPpl() == Constantes.NRO_UNO
                    && optTraceImpresion.get().getBo() == Constantes.NRO_UNO
                    && optTraceImpresion.get().getBt() == Constantes.NRO_UNO
                    && optTraceImpresion.get().getBcv() == Constantes.NRO_UNO) {

                //JORTUZAR: Actualizo a 2 porque ya estaba procesada
                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), Constantes.NRO_DOS, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }

            } else if (optTraceImpresion.isPresent()
                    && optTraceImpresion.get().getPpl() == Constantes.NRO_UNO
                    && optTraceImpresion.get().getBo() == Constantes.NRO_UNO
                    && optTraceImpresion.get().getBt() == Constantes.NRO_CERO
                    && optTraceImpresion.get().getBcv() == Constantes.NRO_UNO
                    && flagRpos) {
                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), Constantes.NRO_DOS, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }
            }
            logger.traceInfo("procesarNotaVenta", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));

            //MKP después de las boletas
            //			if (!generacionDTE.buscaFormaDePago(parametros, inOrdenCompra)){
            //				logger.traceInfo("procesarNotaVenta", "Error en buscaFormaDePago", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
            //			}
            //Se comenta por desición con el equipo de SGO Perú ya que no se utiliza en otra parte del código además de consultar al BO.
            //if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() != null){
            //			if (Validaciones.validaInteger(inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta())) {
            //				TVirtualCodBancaria tvirtual = new TVirtualCodBancaria();
            //				tvirtual = generacionDTE.getCodigoConvenioXTipo(inOrdenCompra.getTarjetaBancaria().getTipoTarjeta().intValue());
            //				inOrdenCompra.getTarjetaBancaria().setCodigoConvenio(tvirtual.getCodBoleta());
            //				//TODO actualizar valor en BD
            //				if (!generacionDTE.updateConvenioBancaria(inOrdenCompra)){
            //					logger.traceInfo("process", "Error al actualizar codigo convenio", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("CODIGO CONVENIO", String.valueOf(inOrdenCompra.getTarjetaBancaria().getCodigoConvenio())));
            //				}
            //			}
            //generar voucher para INDICADOR_MKP = 1, 2 y 3  y si 2, tambien ES_MKP = 1
            if (!Constantes.IND_MKP_RIPLEY.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())
                    && !isMkpProcessed) {

                nroTransaccionAnterior = isRipleyProcessed
                        ? (optTrxPpl.isPresent() ? Integer.valueOf(optTrxPpl.get()) : inOrdenCompra.getNotaVenta().getCorrelativoBoleta().intValue()) : nroTransaccion;

                if (Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {

                    if (!isRipleyProcessed) {
//								nroTransaccionAnterior = nroTransaccion;
                        nroTransaccion = nroTransaccion + Constantes.NRO_UNO;
                    } else {
                        nroTransaccion = operacionesCaja.cajaVirtual(parametros, Boolean.TRUE);
                    }
                    logger.traceInfo("procesarNotaVenta", "nroTransaccionAnterior:" + nroTransaccionAnterior + "nroTransaccion:" + nroTransaccion);
//							nroTransaccion = operacionesCaja.cajaVirtual(parametros, Boolean.TRUE);

                    if (nroTransaccion == Constantes.NRO_CERO) {

                        throw new RestServiceTransactionException("No se pudo obtener número de transacción", CodErrorSystemExit.APERTURA_CIERRE);

                    }

                }

                //JORTUZAR: si es solo mkp respaldo solo su json
                if (Constantes.IND_MKP_MKP.equalsIgnoreCase(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {
                    Optional<String> jsonBoMkp = Optional.empty();
                    PrepareBoCallBoleta source = new PrepareBoCallBoleta();

                    ObjectMapper om = jacksonHttpMessageConverter.getObjectMapper();

                    source.setBoleta(String.valueOf(nroTransaccion));
                    source.setEsMkp(Boolean.TRUE);
                    source.setInOrdenCompra(inOrdenCompra);
                    source.setNroTransaccion(Long.valueOf(nroTransaccion));
                    source.setNroTrxAnterior(Long.valueOf(nroTransaccionAnterior));
                    source.setParametros(parametros);
                    source.setRipleyProcessed(isRipleyProcessed);
                    source.setMkpProcessed(isMkpProcessed);

                    RequestBO req = conversionService.convert(source, RequestBO.class);

                    try {
                        jsonBoMkp = Optional.ofNullable(om.writeValueAsString(req));
                    } catch (JsonProcessingException e) {
                        logger.traceError("procesarNotaVenta", e);
                    }

                    if (jsonBoMkp.isPresent()) {
                        ordenCompraDAO.insertJsonBk(correlativoVenta, Constantes.TIPO_LLAMADA_BO, jsonBoMkp.get());
                    }

                    //JORTUZAR: inserto el json de solo MKP.
                    PoolBDs.commitMESpringTrx();

                }

                codUnico = getCodigoUnicoGrilla2(parametros.buscaValorPorNombre("CODIGO_SUCURSAL"), parametros.buscaValorPorNombre("NRO_CAJA"), nroTransaccion, parametros).toString();
                codUnico = codUnico.replace(Constantes.GUION, Constantes.VACIO);

                logger.traceInfo("procesarNotaVenta", "RECAUDACION: " + (inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
                if (!enviarEmailRecaudacion(inOrdenCompra, String.valueOf(nroTransaccion), parametros)) {//TODO: agregar parametros pcv
                    logger.traceInfo("procesarNotaVenta", "Error en envio email recaudacion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                    /*
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
								logger.traceInfo("process", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
							}
							if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroTransaccion), FechaUtil.getCurrentDateString(), Constantes.ERROR, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO)){
								logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
							}

							continue;
                     */
                }// else{

                if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)) {
                    //						if (!generacionDTE.creacionRecaudacionesCDF(parametros, inOrdenCompra, new Integer(nroTransaccion), codUnico, new Integer(nroTransaccion))){
                    //							logger.traceInfo("process", "Error en satelites CDF", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("CODIGO UNICO", codUnico));
                    //							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
                    //								logger.traceInfo("process", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                    //							}
                    //							if (!generacionDTE.registraTransaccion(parametros,new Integer(nroTransaccion), new Integer(nroTransaccion),fechaDDMMYYYYHHMMSS, Constantes.ERROR, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)){
                    //								logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                    //							}
                    //							continue;
                    //						} else{
                    if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaDDMMYYYYHHMMSS, Constantes.RR, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)) {
                        logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                    }
                    //						}
                } else {
                    if (!generacionDTE.creacionRecaudacionesMKP(parametros, inOrdenCompra, nroTransaccion, codUnico, nroTransaccion, nroTransaccionAnterior)) {
                        logger.traceInfo("process", "Error en satelites MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("CODIGO UNICO", codUnico));
                        if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                            logger.traceInfo("process", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }

                        if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaDDMMYYYYHHMMSS, Constantes.ERROR_MKP, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_CERO)) {
                            logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }
                        continue;
                    } else {

                        //Llamada a BO para MKP
                        try {
                            generacionDTE.envioTrxBo(inOrdenCompra, parametros, Long.valueOf(nroTransaccion), Long.valueOf(nroTransaccionAnterior), Boolean.TRUE, String.valueOf(nroTransaccion), isRipleyProcessed, isMkpProcessed);
                            //aumento el nroTrx en la tabla de control de transacciones
                            try {
                                cajaDAO.updTrxCaja(nroCaja, nroSuc, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
                            } catch (AligareException e) {
                                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                                }
                                throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
                            }

                            //Commit para grabar que la trx al BO se grabó sin problemas.
                            PoolBDs.commitMESpringTrx();
                        } catch (Exception e) {
                            logger.traceError("procesarNotaVenta", "Problema al enviar trx MKP al BO", e);

                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }

                            if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaDDMMYYYYHHMMSS, Constantes.ERROR_MKP, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_CERO)) {
                                logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }

                            throw new RestServiceTransactionException("Problema al enviar trx MKP al BO", Constantes.NRO_VEINTINUEVE);
                        }

                        if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaDDMMYYYYHHMMSS, Constantes.RM, "orden: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_CERO)) {
                            logger.traceInfo("procesarNotaVenta", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }

                        //JORTUZAR: inserto a la traza que Mkp está OK
                        TraceImpresionDTO ti = optTraceImpresion.isPresent() ? optTraceImpresion.get() : new TraceImpresionDTO();
                        ti.setCorrelativoVenta(correlativoVenta);

                        String indicadorMKP = inOrdenCompra.getNotaVenta().getIndicadorMkp();
                        if (indicadorMKP != null) {
                            indicadorMKP = indicadorMKP.trim();
                        }
                        if (Constantes.IND_MKP_MKP.equalsIgnoreCase(indicadorMKP)) {

                            ti.setPpl(Constantes.NRO_CERO);
                            ti.setBo(Constantes.NRO_UNO);
                            ti.setBt(Constantes.NRO_CERO);
                            ti.setBcv(Constantes.NRO_CERO);
                            ti.setMkp(Constantes.NRO_UNO);

                        } else {

                            ti.setPpl(Constantes.NRO_UNO);
                            ti.setBo(Constantes.NRO_UNO);
                            ti.setBt(Constantes.NRO_UNO);
                            ti.setBcv(Constantes.NRO_UNO);
                            ti.setMkp(Constantes.NRO_UNO);

                        }

                        try {
                            ordenCompraDAO.upsertTraceImpresion(ti);
                            PoolBDs.commitMESpringTrx();
                        } catch (DataAccessException e) {
                            logger.traceError("procesarNotaVenta", "Error al grabar traza MKP; " + e.getMessage(), e);
                        }

                        //						}
                    }

                    if (!inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { //AMBAS SE DEJA PARA MAS ADELANTE

                        if (estadoActual.intValue() == Constantes.NRO_TRECE) {
                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), Constantes.NRO_VEINTITRES, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 23", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                            //UpdateMkp
                            if (!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_VEINTITRES, parametros, nroTransaccion, fechaDDMMYYYY, nroTransaccionAnterior)) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 23 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        } else if (estadoActual.intValue() == Constantes.NRO_CATORCE) {
                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTICUATRO), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 24", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                            //UpdateMkp
                            if (!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_VEINTICUATRO, parametros, nroTransaccion, fechaDDMMYYYY, nroTransaccionAnterior)) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 24 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        } else {

                            if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }

//										generacionDTE.updateDireccionDespachoHistorico(inOrdenCompra);
                            //UpdateMkp
                            if (!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_DOS, parametros, nroTransaccion, fechaDDMMYYYY, nroTransaccionAnterior)) {
                                logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                            }
                        }
                        //nroTransaccionAnterior = nroTransaccion;
                    } else if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)) { //BPL Correci�n no cambia estado para MKP mixtas.
                        //UpdateMkp
                        if (!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_DOS, parametros, nroTransaccion, fechaDDMMYYYY, nroTransaccionAnterior)) {
                            logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                        }
                    }

                }

                //actualizo el nroTrx en la tabla de control de transacciones
                try {
                    cajaDAO.updTrxCaja(nroCaja, nroSuc, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
                } catch (AligareException e) {
                    if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                        logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccionAnterior)));
                    }
                    throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
                }

                //Commit para grabar que la trx al BO se grabó sin problemas.
                PoolBDs.commitMESpringTrx();

                if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS), nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")), Constantes.NRO_DOS)) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }

            } else if (isMkpProcessed) {

                //JORTUZAR: Actualizo a 2 porque ya se encontraba procesada
                if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), Constantes.NRO_DOS, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }

                Optional<DetalleTrxBoDTO> optDetalleTrxBoItem = optDetalleTrxBo.get().stream()
                        .filter(it -> it.getEstadoGenOC().trim().equals(Constantes.RM))
                        .findFirst();

                //UpdateMkp
                if (!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_DOS, parametros, (optDetalleTrxBoItem.isPresent() ? optDetalleTrxBoItem.get().getNroTrx().intValue() : nroTransaccion), fechaDDMMYYYY, nroTransaccionAnterior)) {
                    logger.traceInfo("procesarNotaVenta", "Error al actualizar la orden a 2 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
                }

                if (!optDetalleTrxBoItem.isPresent()) {
                    //Commit para grabar que la trx al BO se grabó sin problemas.
                    PoolBDs.commitMESpringTrx();
                }

            }

        }
        logger.endTrace("procesarNotaVenta", "Finalizado", null);
        return resp;
    }

    /**
     * Prepara y realiza invocación a servicio de BigTicket.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param inOrdenCompra
     * @param parametros
     * @param nroTransaccion
     * @param art
     * @throws RestServiceTransactionException
     */
    private void objectBigTicket(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, ArticuloVentaTramaDTE art, LocalDateTime ldt) throws RestServiceTransactionException {

        logger.initTrace("objectBigTicket", "inOrdenCompra =" + inOrdenCompra + " nroTransaccion =" + nroTransaccion + " art =" + art + "ldt =" + ldt);

        PrepareCallBoletaBigTicketDTO pcbdto = new PrepareCallBoletaBigTicketDTO();
        pcbdto.setArt(art);
        pcbdto.setFechaBoleta(ldt);
        pcbdto.setInOrdenCompra(inOrdenCompra);
        pcbdto.setNroTransaccion(nroTransaccion);

        ObjectBT objectBT = conversionService.convert(pcbdto, ObjectBT.class);

        String url = parametros.buscaValorPorNombre("URL_BIGTICKET");

        logger.traceInfo("objectBigTicket", "Url Bigticket = " + url, new KeyLog("Url Big Tiket", url));

        ResponseBT resp = null;

        try {
            resp = restTemplate.postForObject(parametros.buscaValorPorNombre("URL_BIGTICKET"), objectBT, ResponseBT.class);
        } catch (Exception ex) {
            logger.traceError("objectBigTicket", "Exception calling postForObject", ex);
        } finally {
            logger.endTrace("objectBigTicket", "Respuesta =" + resp, "Resp =" + String.valueOf(resp));
        }

        Optional<ResponseBT> optResp = Optional.ofNullable(resp);

        if (!optResp.isPresent()) {
            throw new RestServiceTransactionException("Problema al enviar BT", Constantes.NRO_MENOSUNO);
        } else if (optResp.isPresent() && !Constantes.STRING_UNO.equals(optResp.get().getResult())) {
            throw new RestServiceTransactionException("Problema al enviar BT", Constantes.NRO_MENOSUNO);
        } else {
            logger.traceInfo("objectBigTicket", "optResp is present and is equal to 1");
        }

    }

    /**
     * Prepara y realiza invocación de servicio de BCV
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param pcv
     * @param nroTrx
     * @param folioPPL
     * @return
     * @throws UnsupportedEncodingException
     */
    private boolean llamarBcv(TramaDTE trama, Parametros pcv, Long nroTrx, String folioPPL, Optional<DocumentoElectronicoDTO> optDocumentoElectronicoDTO, boolean flagRpos) throws UnsupportedEncodingException {

        logger.initTrace("llamarBcv", "trama - pcv - nroTrx",
                new KeyLog("trama", String.valueOf(trama)),
                new KeyLog("nroTrx", String.valueOf(nroTrx)));

        boolean resultado = Boolean.TRUE;

        //JORTUZAR: Solo en el caso que sea un reproceso y ya se haya emitido la boleta PPL,
        //			entonces rescato el documento electrónico si es que no está emitido obtengo los datos que se
        //			utilizaron en la boleta ya que de lo contrario BT y BCV quedan con problemas para reconocer
        //			la nota de venta (OC).
        if (!optDocumentoElectronicoDTO.isPresent()) {

            List<DocumentoElectronicoDTO> docsElectronicos = ordenCompraDAO.getDocsElectronicosByCorrelativoVenta(trama.getNotaVenta().getCorrelativoVenta());

            optDocumentoElectronicoDTO = docsElectronicos.stream().findFirst();

        }

        Optional<String> optTrxPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                Constantes.TOKEN_TRX, Constantes.NRO_DOS);

        Optional<String> optFechaPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                Constantes.TOKEN_ENCABEZADO, Constantes.NRO_SEIS);

        Optional<String> optHoraPPL = Util.getValueFromPPLTrama(optDocumentoElectronicoDTO.get().getTramaDTE(),
                Constantes.TOKEN_HORA, Constantes.NRO_DOS);

        PrepareCallBoletaBCV sourceBCV = new PrepareCallBoletaBCV();
        sourceBCV.setFolioPPL(optDocumentoElectronicoDTO.isPresent() ? optDocumentoElectronicoDTO.get().getXmlDTE() : folioPPL);
        sourceBCV.setNroTrx(optTrxPPL.isPresent() ? Long.valueOf(optTrxPPL.get()) : nroTrx);
        sourceBCV.setPcv(pcv);
        sourceBCV.setTrama(trama);
        if (flagRpos) {
            sourceBCV.setFlagRpos(flagRpos);
        }

        String fechaLocal = !optFechaPPL.isPresent() || !optHoraPPL.isPresent() ? null
                : (optFechaPPL.get() + Constantes.ESPACIO + optHoraPPL.get());

        Optional<String> optFechaLocal = Optional.ofNullable(fechaLocal);

        LocalDateTime ldtFechaTrx = LocalDateTime.now();

        if (optFechaLocal.isPresent()) {
            ldtFechaTrx = LocalDateTime.parse(optFechaLocal.get(), Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS);
        } else {
            ldtFechaTrx = LocalDateTime.parse(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, pcv), Constantes.DATE_TIME_FORMATER_DDMMYYYY_HHMMSS);
        }

        sourceBCV.setLdtFechaTrx(ldtFechaTrx);

        RequestBcv req = conversionService.convert(sourceBCV, RequestBcv.class);

        String url = pcv.buscaValorPorNombre("URL_BCV");

        logger.traceInfo("llamarBcv", "URL Servicio REST BCV" + url, new KeyLog("URL", url));

        logger.traceInfo("llamarBcv", "Invocando Servicio REST BCV");

        ResponseBcv resp = null;

        try {
            resp = restTemplate.postForObject(url, req, ResponseBcv.class);
        } catch (RestClientException ex) {
            logger.traceError("llamarBcv", "Error al invocar servicio BCV", ex);
            resp = new ResponseBcv();
            resp.setCodigo(String.valueOf(Constantes.NRO_MENOSUNO));
            resp.setMensaje("Error al invocar servicio BCV: " + ex.getMessage());
        }

        logger.traceInfo("llamarBcv", "Resultado invocacion servicio Broker BCV",
                new KeyLog("Response BCV", String.valueOf(resp)));

        String codigoRespuesta = null;
        if (resp != null) {
            codigoRespuesta = resp.getCodigo();
            codigoRespuesta = codigoRespuesta.trim();
        }

        if (!String.valueOf(Constantes.NRO_CERO).equals(codigoRespuesta)) {

            resultado = Boolean.FALSE;
            if (resp != null) {
                logger.traceInfo("llamarBcv", "Problema en servicio Broker BCV",
                        new KeyLog("Codigo Respuesta", resp.getCodigo()),
                        new KeyLog("Mensaje Respuesta", resp.getMensaje()));
            } else {
                logger.traceInfo("llamarBcv", "Problema en servicio Broker BCV",
                        new KeyLog("Respuesta nula", null));
            }

        }

        logger.endTrace("llamarBcv", "Llamada BCV Finalizada. Resultado = " + resultado, "Resp " + resp);

//		logger.endTrace("llamarBcv", "Llamada BCV Finalizada. Resultado = " + resultado, "Resp " + "OK");
        return resultado;
    }

    public GenericResponse obtenerOCsImpresion(Integer numCaja, Integer cantidadOCs) {
        logger.initTrace("obtenerOCsImpresion", "Integer caja: " + String.valueOf(numCaja) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs),
                new KeyLog("Caja", String.valueOf(numCaja)), new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));

        ConsultaOcImpresionResponse resp = new ConsultaOcImpresionResponse();

        resp.setCodigo(Constantes.NRO_CERO);
        resp.setMensaje("OK");

        resp.setNumerosOCs(ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_UNO, numCaja, cantidadOCs, Constantes.NRO_CERO));

        logger.endTrace("obtenerOCsImpresion", "Finalizado OK", String.valueOf(resp));
        return resp;
    }

    /**
     * Metodo que obtiene datos de la OC mediante un Servicio y lo formatea
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return xml
     */
    public String obtenerXMLPorOC(Long oc, Integer idRpos, Integer sucursal) {
        logger.initTrace("obtenerXMLPorOC", "Integer OC: " + String.valueOf(oc));
        String xmlDefinitivo = "", xmlFormat = "";
        int count = 0;
        Parametros parametros = new Parametros();
        parametros.setParametros(new ArrayList<Parametro>());
        paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");
        String url = String.valueOf(parametros.buscaValorPorNombre(Constantes.URL_OC_DATOS)).concat(String.valueOf(oc));

        try {
            //Mientras no se corrija los permisos del servicio, ira con header
            HttpHeaders headers = new HttpHeaders();
            headers.add(Constantes.STRING_HEADER_USER, Constantes.STRING_HEADER_SERVICIO);
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<String> xml = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

            if (xml.toString().toLowerCase().contains(Constantes.URL_ERROR_OC_DATOS.toLowerCase())) {
                return Constantes.URL_ERROR_OC_DATOS;
            }

            //Se formatea xml
            String[] xmlArray = xml.toString().split("\\n");
            for (String c : xmlArray) {
                if (count > Constantes.NRO_DOS && count < xmlArray.length - Constantes.NRO_DOS) {
                    xmlDefinitivo += c;
                }
                count++;
            }
            xmlFormat = xmlDefinitivo.replaceAll("</Header>", "</Header> <applicationData> ").replaceAll(
                    "<rply:OrdenCoC>", "<ordenCoC>").replaceAll("</rply:OrdenCoC>", "</applicationData> </ordenCoC>").replaceAll(
                    "</notaVenta>", "<tipoOc>RPOS</tipoOc> <idRpos>" + String.valueOf(idRpos) + "</idRpos> <sucursalRpos>" + String.valueOf(sucursal)
                    + "</sucursalRpos> </notaVenta>");
            logger.endTrace("obtenerXMLPorOC", "Finalizado OK", xmlFormat);
        } catch (Exception e) {
            logger.traceError("obtenerXMLPorOC", "Error al obtener xml", e);
            xmlFormat = Constantes.URL_ERROR_OC_DATOS;
        }
        return xmlFormat;
    }

    /**
     * Metodo que obtiene traceRpos
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return xml
     */
    public Optional<TraceRposDTO> obtenerTraceRpos(Long correlativoVenta) {
        logger.initTrace("obtenerTraceRpos", "Long OC: " + String.valueOf(correlativoVenta));
        Optional<List<TraceRposDTO>> optListTraceRposDTO = Optional.ofNullable(ordenCompraDAO.getTraceRPOS(correlativoVenta));
        Optional<TraceRposDTO> optTraceRpos = Optional.empty();
        if (optListTraceRposDTO.isPresent()) {
            optTraceRpos = optListTraceRposDTO.get().stream().findFirst();
        }
        logger.endTrace("obtenerTraceRpos", "Finalizado OK", optTraceRpos.toString());
        return optTraceRpos;
    }

    /**
     * Metodo que actualiza traceRpos
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return xml
     */
    public void actualizarTraceRpos(Optional<TraceRposDTO> optTraceRpos, Long correlativoVenta, Date fechaInicio, int getOcTv, int insertaOc,
            int validaOc, int imprimirOc, int generaJson, String tiempoGetOcTv, String tiempoInsertaOc, String tiempoValidaOc,
            String tiempoImprimirOc, String tiempoGeneraJson, Date fechaFin, boolean reproc) {
        logger.initTrace("actualizarTraceRpos", "Long OC: " + String.valueOf(correlativoVenta));

        if (!reproc) {
            TraceRposDTO tr = optTraceRpos.isPresent() ? optTraceRpos.get() : new TraceRposDTO();
            tr.setCorrelativoVenta(correlativoVenta);
            tr.setFechaInicio(fechaInicio);
            tr.setGetOcTv(getOcTv);
            tr.setTiempoGetOcTv(tiempoGetOcTv);
            tr.setInsertaOc(insertaOc);
            tr.setTiempoInsertaOc(tiempoInsertaOc);
            tr.setValidaOc(validaOc);
            tr.setTiempoValidaOc(tiempoValidaOc);
            tr.setImprimirOc(imprimirOc);
            tr.setTiempoImprimirOc(tiempoImprimirOc);
            tr.setGeneraJson(generaJson);
            tr.setTiempoGeneraJson(tiempoGeneraJson);
            if (fechaFin != null) {
                tr.setFechaFin(fechaFin);
            }

            try {
                ordenCompraDAO.upsertTraceRPOS(tr);
                //PoolBDs.commitMESpringTrx();
            } catch (DataAccessException e) {
                logger.traceError("actualizarTraceRpos", "Error al grabar traza Rpos; " + e.getMessage(), e);
            }
            logger.endTrace("actualizarTraceRpos", "Finalizado OK", "Finalizado OK");
        } else {
            TraceRposDTO tr = new TraceRposDTO();
            tr.setCorrelativoVenta(correlativoVenta);
            tr.setValidaOc(validaOc);
            tr.setImprimirOc(imprimirOc);
            tr.setGeneraJson(generaJson);

            try {
                ordenCompraDAO.reprocTraceRPOS(tr);
                //PoolBDs.commitMESpringTrx();
            } catch (DataAccessException e) {
                logger.traceError("actualizarTraceRpos", "Error al grabar reprocTraceRPOS; " + e.getMessage(), e);
            }
            logger.endTrace("actualizarTraceRpos", "Finalizado OK", "Finalizado OK");
        }
    }

    /**
     * Metodo que genera el Json para imprimir boleta fisica de RPOS
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @return Json
     */
    public RposResponse generarJsonRpos(Long oc) {

        logger.initTrace("generarJsonRpos", "Oc: " + String.valueOf(oc));
        OrdenesDeCompra orden;
        List<ArticulosVentaOC> listaArticulos;
        RposResponse resp = new RposResponse();
        try {
            orden = obtenerDatosTramaBoleta(new Long(oc));
            listaArticulos = obtenerArticulosTramaBoleta(new Long(oc));
            SucursalRpos sucursalRpos = caja.getDatosSuc(orden.getNotaVenta().getNumeroSucursal(), orden.getNotaVenta().getNumeroCaja());
            if (orden.getNotaVenta() == null || orden.getNotaVenta().getFechaCreacion() == null
                    || orden.getNotaVenta().getCorrelativoVenta() == null || listaArticulos == null) {
                resp.setMensaje(Constantes.ERROR);
                return resp;
            }

            Parametros parametros = new Parametros();
            parametros.setParametros(new ArrayList<Parametro>());
            paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");
            Parametro param = new Parametro();
            param.setNombre(Constantes.FUNCION);
            param.setValor(String.valueOf(Constantes.FUNCION_BOLETA));
            parametros.getParametros().add(param);

            String codigoBarra = crearCodigoBarra(orden);
            String subTotal = obtenerSubTotalBoleta(listaArticulos);
            String totalExento = Constantes.STRING_CERO;
            String total = obtenerTotal(subTotal, totalExento);
            String medioPago = obtenerMedioPago(orden);
            boolean boolTRipley = medioPago.equals(Constantes.FORMA_PAGO_TARJETA_RIPLEY) ? true : false;
            String nroMedioPago = obtenerNroMedioPago(medioPago, orden);
            String nroUnico = obtenerNroUnico(orden);
            BigDecimal ivaBD = new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
            String iva = Util.formatNumber2(String.valueOf((Double.parseDouble(total) * Double.parseDouble(ivaBD.toString()))
                    / Double.parseDouble(Constantes.STRING_UNO.concat(ivaBD.toString()))));
            String totalNeto = Util.formatNumber2(String.valueOf(Double.parseDouble(total) - Double.parseDouble(iva)));
            String montoEscrito = Util.MontoEscrito(total, Constantes.VERDADERO);
            String ted = "";
            if (orden.getDocumentoElectronico().getPdf417() != null) {
                ted = orden.getDocumentoElectronico().getPdf417().getSubString(1, (int) orden.getDocumentoElectronico().getPdf417().length());
            }
            int countLista = 0;
            folioSiiRpos = orden.getNotaVenta().getFolioSii();
            numCajaRpos = orden.getNotaVenta().getNumeroCaja();
            logger.traceInfo("generarJsonRpos", "BPL OrdenInfo: " + ted);

            List<Linea> lineas = new ArrayList<Linea>();
            Linea linea = new Linea();
            //Cabecera Inicio
            linea = new Linea();
            linea.setText("RIPLEY");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("TIENDAS POR DEPARTAMENTO RIPLEY S.A.");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("CALLE LAS BEGONIAS 545-577");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("SAN ISIDRO - LIMA");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("RUC 20337564373");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText(orden.getNotaVenta().getFolioSii());
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("CONSTANCIA DE EMISIÓN DEL");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("COMPROBANTE ELECTRÓNICO");
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);

            //Codigo de barra
            linea = new Linea();
            linea.setTipo("BARCODE_GS1_128");
            linea.setValue(codigoBarra);
            lineas.add(linea);
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);

            //Datos sucursal
            linea = new Linea();
            linea.setText(String.format("%05d", sucursalRpos.getSucursalPpl()) + "/" + String.format("%03d", orden.getNotaVenta().getNumeroCaja()) + " "
                    + FechaUtil.formatDate("dd/MM/yy", orden.getNotaVenta().getHoraBoleta()) + " "
                    + FechaUtil.formatDate("HH:mm", orden.getNotaVenta().getHoraBoleta()) + " " + String.format("%04d", orden.getNotaVenta().getNroTrxRpos()));
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            if (orden.getNotaVenta().getEjecutivoVta() != null && !orden.getNotaVenta().getEjecutivoVta().isEmpty()) {
                linea.setText(String.format("%010d", Integer.parseInt(orden.getNotaVenta().getEjecutivoVta()))
                        + " VENDEDOR SUCUR: " + orden.getNotaVenta().getNumeroSucursal());
            } else {
                linea.setText("0000000000 VENDEDOR SUCUR: 1" + String.format("%02d", orden.getNotaVenta().getNumeroSucursal()));
            }
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("SERIE: " + parametros.buscaValorPorNombre("TERMINAL") + " " + parametros.buscaValorPorNombre("TIPO_VENTA_RPOS") + " VENTA");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("DIRECCION: " + sucursalRpos.getDireccionCorta());
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText("DISTRITO: " + sucursalRpos.getDistrito());
            linea.setTipo("TEXT");
            lineas.add(linea);
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);

            linea = new Linea();
            linea.setTipo("TEXT_COLUMN_2");
            linea.setText1("TICKET NRO :");
            linea.setText2(" " + String.format("%04d", orden.getNotaVenta().getNroTrxRpos()));
            linea.setSizeColumn1("13");
            linea.setSizeColumn3("10");
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_COLUMN_2");
            linea.setText1("Doc Cliente :");
            linea.setText2(" DNI " + String.format(Constantes.FORMATO_8_CEROS_IZQ, orden.getDespacho().getRutCliente()));
            linea.setSizeColumn1("13");
            linea.setSizeColumn3("10");
            lineas.add(linea);
            if (orden.getDespacho().getNombreCliente() != null && orden.getDespacho().getApellidoPatCliente() != null && (" "
                    + orden.getDespacho().getNombreCliente() + " " + orden.getDespacho().getApellidoPatCliente()).length() <= Constantes.NRO_VEINTISEIS) {
                linea = new Linea();
                linea.setTipo("TEXT_COLUMN_2");
                linea.setText1("Nombre Cliente :");
                linea.setText2(" " + orden.getDespacho().getNombreCliente() + " " + orden.getDespacho().getApellidoPatCliente());
                linea.setSizeColumn1("13");
                linea.setSizeColumn3("10");
                lineas.add(linea);
            } else {
                linea = new Linea();
                linea.setTipo("TEXT_LEFT_RIGHT");
                linea.setTextLeft("Nombre Cliente :");
                linea.setTextRight(orden.getDespacho().getNombreCliente());
                lineas.add(linea);
                linea = new Linea();
                linea.setTipo("TEXT_LEFT_RIGHT");
                linea.setTextLeft("");
                linea.setTextRight(orden.getDespacho().getApellidoPatCliente());
                lineas.add(linea);
            }
            if (orden.getDespacho().getEmailCliente() != null && orden.getDespacho().getEmailCliente().length() <= Constantes.NRO_VEINTITRES) {
                linea = new Linea();
                linea.setTipo("TEXT_COLUMN_2");
                linea.setText1("eMail :");
                linea.setText2(" " + orden.getDespacho().getEmailCliente());
                linea.setSizeColumn1("13");
                linea.setSizeColumn3("10");
                lineas.add(linea);
            } else {
                char[] aCaracteres = orden.getDespacho().getEmailCliente().toCharArray();
                String desc1 = "", desc2 = "";
                int count = 0;
                while (count < aCaracteres.length) {
                    if (count <= Constantes.NRO_VEINTISEIS) {
                        desc1 += aCaracteres[count];
                        count++;
                    } else {
                        desc2 += aCaracteres[count];
                        count++;
                    }
                }
                linea = new Linea();
                linea.setTipo("TEXT_LEFT_RIGHT");
                linea.setTextLeft("eMail :");
                linea.setTextRight(desc1);
                lineas.add(linea);
                linea = new Linea();
                linea.setTipo("TEXT_LEFT_RIGHT");
                linea.setTextLeft("");
                linea.setTextRight(desc2);
                lineas.add(linea);
            }

            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);

            //listaArticulos
            if (listaArticulos != null && !listaArticulos.isEmpty()) {
                ArticulosVentaOC articulo = null;
                for (int i = 0; i < listaArticulos.size(); i++) {
                    articulo = listaArticulos.get(i);
                    if ((articulo.getArticuloVenta().getCodArticulo().length() + articulo.getArticuloVenta().getDescRipley().length())
                            <= Constantes.NRO_VEINTISEIS) {
                        linea = new Linea();
                        linea.setTipo("TEXT_LEFT_RIGHT");
                        linea.setTextLeft(articulo.getArticuloVenta().getCodArticulo() + " " + articulo.getArticuloVenta().getDescRipley());
                        linea.setTextRight(Util.formatNumber2(articulo.getArticuloVenta().getPrecioPorUnidades().toString()));
                        lineas.add(linea);
                    } else {
                        linea = new Linea();
                        linea.setTipo("TEXT_LEFT_RIGHT");
                        linea.setTextLeft(articulo.getArticuloVenta().getCodArticulo());
                        linea.setTextRight("");
                        lineas.add(linea);
                        if (articulo.getArticuloVenta().getDescRipley().length() <= Constantes.NRO_VEINTISEIS) {
                            linea = new Linea();
                            linea.setTipo("TEXT_LEFT_RIGHT");
                            linea.setTextLeft(articulo.getArticuloVenta().getDescRipley());
                            linea.setTextRight(Util.formatNumber2(articulo.getArticuloVenta().getPrecioPorUnidades().toString()));
                            lineas.add(linea);
                        } else {
                            String[] arrayCad = articulo.getArticuloVenta().getDescRipley().split(" ");
                            String desc1 = "", desc2 = "";
                            int count = 0;
                            while (count < arrayCad.length) {
                                if (count <= (arrayCad.length / Constantes.NRO_DOS)) {
                                    desc1 += arrayCad[count] + " ";
                                    count++;
                                } else {
                                    desc2 += arrayCad[count] + " ";
                                    count++;
                                }
                            }
                            linea = new Linea();
                            linea.setTipo("TEXT_LEFT_RIGHT");
                            linea.setTextLeft(desc1);
                            linea.setTextRight("");
                            lineas.add(linea);
                            linea = new Linea();
                            linea.setTipo("TEXT_LEFT_RIGHT");
                            linea.setTextLeft(desc2);
                            linea.setTextRight(Util.formatNumber2(articulo.getArticuloVenta().getPrecioPorUnidades().toString()));
                            lineas.add(linea);
                        }
                    }
                    if (articulo.getArticuloVenta().getMontoDescuento() != null && articulo.getArticuloVenta().getMontoDescuento().compareTo(BigDecimal.ZERO) > 0) {
                        linea = new Linea();
                        linea.setTipo("TEXT_LEFT_RIGHT");
                        linea.setTextLeft(String.valueOf(articulo.getArticuloVenta().getUnidades()) + " X " + Util.formatNumber2(
                                articulo.getArticuloVenta().getPrecio().toString()) + " DESCUENTO");
                        linea.setTextRight(Util.formatNumber2(articulo.getArticuloVenta().getMontoDescuento().toString()) + "-");
                        lineas.add(linea);
                    } else {
                        linea = new Linea();
                        linea.setTipo("TEXT");
                        linea.setText(String.valueOf(articulo.getArticuloVenta().getUnidades()) + " X " + Util.formatNumber2(articulo.getArticuloVenta().getPrecio().toString()));
                        lineas.add(linea);
                    }

                    if (!articulo.getArticuloVenta().getDescRipley().equals(Constantes.NAME_COSTO_ENVIO)) {
                        countLista += articulo.getArticuloVenta().getUnidades();
                    }
                }
            }
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft("SUBTOTAL S/");
            linea.setTextRight(subTotal);
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT");
            linea.setText("NRO DE UNIDADES: " + String.valueOf(countLista));
            lineas.add(linea);

            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft("OP. GRAVADAS");
            linea.setTextRight(totalNeto);
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft("I.G.V. (18%)");
            linea.setTextRight(iva);
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft("TOTAL A PAGAR S/");
            linea.setTextRight(total);
            lineas.add(linea);
            linea = new Linea();
            linea.setText("SON: " + montoEscrito);
            linea.setBold("1");
            linea.setTipo("TEXT");
            lineas.add(linea);

            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);
            //Medio de Pago
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft(medioPago);
            linea.setTextRight("");
            lineas.add(linea);
            linea = new Linea();
            linea.setTipo("TEXT_LEFT_RIGHT");
            linea.setTextLeft(nroMedioPago);
            linea.setTextRight(total);
            lineas.add(linea);
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);
            linea = new Linea();

            if (boolTRipley) {
                linea.setTipo("TEXT_LEFT_RIGHT");
                linea.setTextLeft("MONTO");
                linea.setTextRight(total);
                lineas.add(linea);

                if (orden.getTarjetaRipley() != null) {
                    if (orden.getTarjetaRipley().getFechaPrimerVencto() != null) {
                        linea = new Linea();
                        linea.setTipo("TEXT_LEFT_RIGHT");
                        linea.setTextLeft("PRIMER VENCIMIENTO");
                        linea.setTextRight(new SimpleDateFormat("dd/MM/yyyy").format(orden.getTarjetaRipley().getFechaPrimerVencto()));
                        lineas.add(linea);
                    }
                    if (orden.getTarjetaRipley().getValorCuota() != null) {
                        linea = new Linea();
                        linea.setTipo("TEXT_LEFT_RIGHT");
                        linea.setTextLeft("VALOR CUOTA (S/ )");
                        linea.setTextRight(Util.formatNumber2(orden.getTarjetaRipley().getValorCuota().toString()));
                        lineas.add(linea);
                    }
                    if (orden.getTarjetaRipley().getPlazo() != null && orden.getTarjetaRipley().getDiferido() != null) {
                        String diferido = obtenerDiferido(orden.getTarjetaRipley().getDiferido());
                        linea = new Linea();
                        linea.setTipo("TEXT");
                        linea.setText("CUOTAS: " + String.valueOf(orden.getTarjetaRipley().getPlazo()) + "	- TIPO: " + diferido);
                        lineas.add(linea);
                    } else if (orden.getTarjetaRipley().getPlazo() != null && orden.getTarjetaRipley().getDiferido() == null) {
                        linea = new Linea();
                        linea.setTipo("TEXT");
                        linea.setText("CUOTAS: " + String.valueOf(orden.getTarjetaRipley().getPlazo()) + "	- TIPO: ");
                        lineas.add(linea);
                    } else if (orden.getTarjetaRipley().getPlazo() == null && orden.getTarjetaRipley().getDiferido() != null) {
                        String diferido = obtenerDiferido(orden.getTarjetaRipley().getDiferido());
                        linea = new Linea();
                        linea.setTipo("TEXT");
                        linea.setText("CUOTAS:		- TIPO: " + diferido);
                        lineas.add(linea);
                    }
                    //Salto de linea
                    linea = new Linea();
                    linea.setTipo("LINE_BREAK");
                    lineas.add(linea);
                    linea = new Linea();
                    linea.setText("ORDEN DE COMPRA: " + String.valueOf(oc));
                    linea.setTipo("TEXT");
                    lineas.add(linea);

                    //Salto de linea
                    linea = new Linea();
                    linea.setTipo("LINE_BREAK");
                    lineas.add(linea);
                    //Salto de linea
                    linea = new Linea();
                    linea.setTipo("LINE_BREAK");
                    lineas.add(linea);

                    linea = new Linea();
                    linea.setText("UD. GANO " + String.valueOf((int) Math.floor(Double.parseDouble(total))) + " PTOS. A ABONAR EN 48H");
                    linea.setTipo("TEXT");
                    lineas.add(linea);
                    linea = new Linea();
                    linea.setText("LIMITE ACUMULACION WWW.RIPLEYPUNTOS.PE");
                    linea.setTipo("TEXT");
                    lineas.add(linea);
                }
            } else {
                //Salto de linea
                linea = new Linea();
                linea.setTipo("LINE_BREAK");
                lineas.add(linea);
                linea = new Linea();
                linea.setText("ORDEN DE COMPRA: " + String.valueOf(oc));
                linea.setTipo("TEXT");
                lineas.add(linea);

                //Salto de linea
                linea = new Linea();
                linea.setTipo("LINE_BREAK");
                lineas.add(linea);
                linea = new Linea();
                linea.setText("SI TIENE TC RIPLEY ACTIVA, ESTA COMPRA");
                linea.setAlign("CENTER");
                linea.setTipo("TEXT");
                lineas.add(linea);
                linea = new Linea();
                linea.setText("GANA RIPLEYPUNTOS GO.ABONO 15 MES SGTE");
                linea.setAlign("CENTER");
                linea.setTipo("TEXT");
                lineas.add(linea);
            }
            //Salto de linea
            linea = new Linea();
            linea.setTipo("LINE_BREAK");
            lineas.add(linea);
            linea = new Linea();
            linea.setText(parametros.buscaValorPorNombre("FIRMA_JSON_1"));
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText(parametros.buscaValorPorNombre("FIRMA_JSON_2"));
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);
            linea = new Linea();
            linea.setText(parametros.buscaValorPorNombre("FIRMA_JSON_3"));
            linea.setAlign("CENTER");
            linea.setTipo("TEXT");
            lineas.add(linea);

            /*
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("GRACIAS POR SU COMPRA");
			linea.setAlign("CENTER");
			linea.setTipo("TEXT");
			lineas.add(linea);
			
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("===========================");
			linea.setAlign("CENTER");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("CAMBIO O DEVOLUCION");
			linea.setAlign("CENTER");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("Conoce las condiciones para acceder al");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("beneficio de cambio o devolución:");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("1. El producto debe estar nuevo y sin");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("señales de uso.");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("2. Con los empaques originales completos");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("y en buen estado.");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("3. El plazo transcurrido desde la fecha");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("de compra no debe superar los 7 días.");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("4. Debes presentar tu boleta o factura");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("y DNI vigente.");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("Conoce otras condiciones para cambio o");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("devolución en www.ripley.com.pe");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("===========================");
			linea.setAlign("CENTER");
			linea.setTipo("TEXT");
			lineas.add(linea);
			
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			//Sector Timbre Electronico.
			linea = new Linea();
			linea.setValue(ted);
			linea.setTipo("SYMBOL_PDF417_STANDARD");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("Autorizado mediante resolucion Nro.");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("018005001156/SUNAT. Reprentacion");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("impresa del comprobante electronico");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("consulte el documento ingresando a:");
			linea.setTipo("TEXT");
			lineas.add(linea);
			linea = new Linea();
			linea.setText("WWW.ripley.com.pe");
			linea.setTipo("TEXT");
			lineas.add(linea);
			
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
			//Salto de linea
			linea = new Linea();
			linea.setTipo("LINE_BREAK");
			lineas.add(linea);
             */
            linea = new Linea();
            linea.setTipo("CUT");
            lineas.add(linea);

            resp.setLineas(lineas);
        } catch (Exception e) {
            logger.traceError("generarJsonRpos", e);
            resp.setMensaje(Constantes.ERROR);
        }
        logger.endTrace("generarJsonRpos", "Finalizado OK", "");
        return resp;
    }

    /**
     * Metodo que obtiene trama de boleta mediante OC
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return OrdenesDeCompra
     */
    private OrdenesDeCompra obtenerDatosTramaBoleta(Long oc) {
        logger.initTrace("obtenerDatosTramaBoleta", "Long OC: " + String.valueOf(oc));

        OrdenesDeCompra trama = new OrdenesDeCompra();
        List<OrdenesDeCompra> datosBoleta = new ArrayList<OrdenesDeCompra>();
        datosBoleta = ordenesCompraDAO.obtenerDatosBoleta(null, null, oc);
        if (datosBoleta != null && datosBoleta.size() > 0) {
            trama = datosBoleta.get(0);
        }
        logger.endTrace("obtenerDatosTramaBoleta", "Finalizado OK", "");
        return trama;
    }

    /**
     * Metodo que obtiene articulos de boleta mediante OC
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return List<ArticulosVentaOC>
     */
    private List<ArticulosVentaOC> obtenerArticulosTramaBoleta(Long oc) {
        logger.initTrace("obtenerArticulosTramaBoleta", "Long OC: " + String.valueOf(oc));
        List<ArticulosVentaOC> articulos = new ArrayList<ArticulosVentaOC>();
        articulos = ordenesCompraDAO.getArticulosByOC(oc);
        logger.endTrace("obtenerArticulosTramaBoleta", "Finalizado OK", "");
        return articulos;
    }

    /**
     * Metodo que genera codigo de barra por OC
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param oc
     * @return String
     */
    private String crearCodigoBarra(OrdenesDeCompra orden) {
        String codigoCreado;
        String fecha = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYY2, orden.getNotaVenta().getFechaCreacion());
        String sucursal = String.format("%05d", orden.getNotaVenta().getNumeroSucursal());
        String nroCaja = String.format("%03d", orden.getNotaVenta().getNumeroCaja());
        String trx = String.format("%04d", orden.getNotaVenta().getNroTrxRpos());
        codigoCreado = fecha + sucursal + nroCaja + trx;
        return codigoCreado;
    }

    /**
     * Metodo que obtiene sub total de la boleta
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @param List<ArticulosVentaOC>
     * @return String
     */
    private String obtenerSubTotalBoleta(List<ArticulosVentaOC> listaArticulos) {
        BigDecimal suma = BigDecimal.ZERO;
        if (listaArticulos != null) {
            suma = listaArticulos.stream()
                    .filter(articulo -> articulo.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.NRO_CERO)
                    .map(articulo -> articulo.getArticuloVenta().getPrecioPorUnidades().subtract(articulo.getArticuloVenta().getMontoDescuento()))
                    .reduce(suma, BigDecimal::add);
        }
        return Util.formatNumber2(suma.toString());
    }

    private String obtenerMedioPago(OrdenesDeCompra orden) {
        String medioPago = "";
        if (orden.getTarjetaRipley() != null) {
            medioPago = Constantes.FORMA_PAGO_TARJETA_RIPLEY;
        }

        if (orden.getTarjetaBancaria() != null && orden.getTarjetaBancaria().getCorrelativoVenta() != null) {
            medioPago = orden.getTarjetaBancaria().getDescripcionCanal();
        }

//		if (orden.getTarjetaRegaloEmpresa() != null) {
//			if(orden.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null)
//				medioPago = Constantes.BOL_FORMA_PAGO_TAR_REG_EMP;
//			if(orden.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && orden.getTarjetaRipley().getCorrelativoVenta()!=null)
//				medioPago = Constantes.BOL_FORMA_PAGO_TRECRIP;
//		}
        return medioPago.toUpperCase();
    }

    private String obtenerNroMedioPago(String medioPago, OrdenesDeCompra orden) {
        String nroMedioPago = "";
        if (medioPago.equals(Constantes.FORMA_PAGO_TARJETA_RIPLEY) && orden.getTarjetaRipley().getPan() != null) {
            nroMedioPago = String.valueOf(orden.getTarjetaRipley().getPan()).replaceAll(String.valueOf(orden.getTarjetaRipley().getPan()).substring(6, 12), Constantes.TRAMA_ASTERISCOS_X6);
        } else if ((medioPago.contains(Constantes.NAME_VISA) || medioPago.contains(Constantes.NAME_MASTERCARD) || medioPago.contains(Constantes.NAME_DINERS)
                || medioPago.contains(Constantes.NAME_AMEX)) && orden.getTarjetaBancaria() != null) {
            nroMedioPago = orden.getTarjetaBancaria().getBinNumber() + "******" + orden.getTarjetaBancaria().getUltimosDigitosTarjeta();
        } else {
            nroMedioPago = "000000******0000";
        }
        return nroMedioPago;
    }

    private String obtenerNroUnico(OrdenesDeCompra orden) {
        String numeroUnico = "NU";
        String fecha = FechaUtil.formatTimestamp(Constantes.FECHA_DDMMYY3, orden.getNotaVenta().getHoraBoleta());
        String nroCaja = ((orden.getNotaVenta().getNumeroCaja() != null) ? orden.getNotaVenta().getNumeroCaja().toString() : Constantes.VACIO);
        String nroBoleta = orden.getNotaVenta().getNumeroBoleta() != null ? orden.getNotaVenta().getNumeroBoleta().toString() : Constantes.VACIO;
        fecha = fecha != null ? fecha : Constantes.VACIO;
        numeroUnico += Constantes.STRING_CERO + Constantes.STRING_CERO + orden.getNotaVenta().getNumeroSucursal();
        numeroUnico += Constantes.GUION;
        numeroUnico += nroCaja.length() == 1 ? Constantes.STRING_CERO + Constantes.STRING_CERO + Constantes.STRING_CERO + nroCaja : Constantes.STRING_CERO + Constantes.STRING_CERO + nroCaja;
        numeroUnico += Constantes.GUION;
        numeroUnico += nroBoleta;
        numeroUnico += Constantes.GUION;
        numeroUnico += fecha.replace(Constantes.GUION, Constantes.VACIO);
        numeroUnico += Constantes.GUION;
        numeroUnico += "120000";
        return numeroUnico;
    }

    private String obtenerTotal(String subTotal2, String totalExento2) {
        Float suma = 0f;
        suma = Float.parseFloat(subTotal2) + Float.parseFloat(totalExento2);
        return Util.formatNumber2(String.valueOf(suma));
    }

    private String getRutClienteConDv(String rut) {
        return rut + Util.getDigitoValidadorRut2(rut);
    }

    private String obtenerDiferido(Integer diferido) {
        String dif = "";
        if (diferido == Constantes.NRO_CERO) {
            dif = Constantes.NAME_DIFERIDO_CERO;
        } else if (diferido == Constantes.NRO_DOS) {
            dif = Constantes.NAME_DIFERIDO_DOS;
        }
        return dif;
    }

    /**
     * Metodo que busca el número de caja en base al idRpos
     *
     * @author Pablo Salazar Osorio (Aligare).
     * @since 20-11-2018
     * <br/><br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param Integer, Integer
     * @return Integer
     */
    public Map<String, Integer> getNumCajaRpos(Long oc) {
        logger.initTrace("getNumCajaRpos", "Long oc: " + String.valueOf(oc));
        Map<String, Integer> resp = caja.getCajaSucRpos(oc);
        logger.endTrace("getNumCajaRpos", "Finalizado OK", "");
        return resp;
    }

    public void cambiaEstadoRpos(Long oc, Integer estado) {
        logger.initTrace("cambiaEstadoRpos", "Long oc: " + String.valueOf(oc));
        if (!generacionDTE.actualizarNotaVentaEstadoFolio(oc, estado, folioSiiRpos, numCajaRpos, Constantes.NRO_DOS)) {
            logger.traceInfo("cambiaEstadoRpos", "Error al actualizar la orden a " + String.valueOf(estado), new KeyLog("CORRELATIVO", String.valueOf(oc)));
        }
        logger.endTrace("cambiaEstadoRpos", "Finalizado OK", "");
    }

    public void cambiaEstadoCajaSuc(Long oc, Integer estado, Integer caja, Integer sucursal) {
        logger.initTrace("cambiaEstadoCajaSuc", "Long oc: " + String.valueOf(oc));
        if (!generacionDTE.actualizarNotaVenta(oc, estado, caja, sucursal)) {
            logger.traceInfo("cambiaEstadoCajaSuc", "Error al actualizar la orden a " + String.valueOf(estado), new KeyLog("CORRELATIVO", String.valueOf(oc)));
        }
        logger.endTrace("cambiaEstadoCajaSuc", "Finalizado OK", "");
    }

    private TramaDTE regenerarTramaDteBOOL(TramaDTE inOrdenCompra) {
        try {
            // TODO: implementar clone();
            TramaDTE tmpTramaDTE = SerializationUtils.clone(inOrdenCompra);//CLONE
            List<ArticuloVentaTramaDTE> tmpListArticuloVentaTramaDTE = tmpTramaDTE.getArticuloVentas();
            ArticuloVentaTramaDTE tmpArticuloVentaTramaDTE = null;
            ArticuloVenta tmpArticuloVenta = null;
            String auxCUD = null;
            String auxDescRipley = null;
            int fixedSize = 22;
            int count = 0;
            for (int i = 0; i < tmpListArticuloVentaTramaDTE.size(); i++) {
                tmpArticuloVentaTramaDTE = tmpListArticuloVentaTramaDTE.get(i);
                tmpArticuloVenta = tmpArticuloVentaTramaDTE.getArticuloVenta();
                auxDescRipley = tmpArticuloVenta.getDescRipley();
                if (auxDescRipley != null && auxDescRipley.trim().toUpperCase().startsWith(Constantes.PREFIJO_EXTRA_GARANTIA)) {
                    auxCUD = getFixedSerialNumber(count, fixedSize);
                    tmpArticuloVenta.setCodDespacho(auxCUD);
                    tmpArticuloVentaTramaDTE.setArticuloVenta(tmpArticuloVenta);
                    tmpListArticuloVentaTramaDTE.set(i, tmpArticuloVentaTramaDTE);
                    count++;
                }
            }
            tmpTramaDTE.setArticuloVentas(tmpListArticuloVentaTramaDTE);
            return tmpTramaDTE;
        } catch (Exception e) {
            logger.traceError("filtrarTramaDteBOOL", "Error al filtrar tram DTE para BOOL:", e);
            throw e;
        }
    }

    private boolean envioTrxBO(TramaDTE inOrdenCompra, Parametros parametros, Integer nroTransaccion, String boleta,
            boolean isRipleyProcessed, boolean isMkpProcessed) {
        boolean resultado = true;
        try {
            generacionDTE.envioTrxBo(regenerarTramaDteBOOL(inOrdenCompra),
                    parametros, Long.valueOf(nroTransaccion), null,
                    Boolean.FALSE, boleta, isRipleyProcessed, isMkpProcessed);
        } catch (Exception e) {
            resultado = false;
            logger.traceError("creacionTransaccionesDteB", "Problema al enviar trx Ripley al BO", e);
        }
        return resultado;
    }

    private String getFixedSerialNumber(int number, int fixedLenght) {
        String str = String.valueOf(number);
        if (str.length() < fixedLenght) {
            for (int i = fixedLenght; str.length() < fixedLenght; i--) {
                str = "0".concat(str);
            }
            return str;
        } else {
            throw new AligareException("Cantidad de digitos en parametro number no puede exceder a parametro fixedLenght");
        }
    }

}
