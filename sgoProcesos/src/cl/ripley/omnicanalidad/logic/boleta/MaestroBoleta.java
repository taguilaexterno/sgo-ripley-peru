package cl.ripley.omnicanalidad.logic.boleta;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.dao.impl.GeneracionDTEDAOImpl;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.Constantes;

/**
 * MaestroBoleta
 * 
 * Controla la ejecuci�n de los esclavos. Primero accede a base de datos para cargar en forma completa
 * la lista de folios y numeros de ordenes a procesar.
 *
 * @author Fabian Riveros V.
 *
 */
public class MaestroBoleta {

	private static final AriLog logger = new AriLog(MaestroBoleta.class,"RIP16-004", PlataformaType.JAVA);
    /**
     * Cola con los folios a procesar
     */
	private ConcurrentLinkedQueue<TramaDTE> qOrdenCompra = null;
	private Semaphore sem = null;
	
	private List<EsclavoBoleta> slaves = null;
	private Parametros parametros;
	
	
	/**
	 * Constructor que inicializa lista de esclavos
	 *  
	 */
	public MaestroBoleta(Parametros pPars) throws AligareException {
		parametros = pPars;
		slaves = new ArrayList<EsclavoBoleta>();
		qOrdenCompra = new ConcurrentLinkedQueue<TramaDTE>();
	}

	/**
	 * Gatilla el proceso. Retorna una vez que se procesando todas las OCs
	 */
	public void process() throws AligareException {
		logger.initTrace("process", null);
		
		//inicializa DAOs
		Integer nroCaja = new Integer(parametros.buscaParametroPorNombre("NRO_CAJA").getValor()); //new Integer(82);
		Integer nroSucusal = new Integer(parametros.buscaParametroPorNombre("SUCURSAL").getValor());
		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
		
		
		//llena la cola de OCs
		Integer nroEstado = new Integer(Constantes.NRO_UNO);
		qOrdenCompra = generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado);
		
		nroEstado = new Integer(Constantes.NRO_TRECE);
		qOrdenCompra.addAll(generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado));

		nroEstado = new Integer(Constantes.NRO_CATORCE);
		qOrdenCompra.addAll(generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado));
		
		if (qOrdenCompra.size() == 0){
			int nroTransaccion = new OperacionesCaja().cajaVirtual(parametros, false);
			logger.traceInfo("process", "SIN ordenes que procesar: "+nroTransaccion);
		} else {
			logger.initTrace("process", "Se procesaran "+qOrdenCompra.size()+ " ordenes");
			/*
			 * Se inicializan los Esclavos.
			 */
			initSlaves();
	
			/*
			 * Se Espera que los esclavos terminen. Cada uno manda un notify al master cuando termina, 
			 * por lo mismo se espera tantos wait como esclavos existan.
			 */
			try {
				sem.acquire(slaves.size());
			} catch (InterruptedException e) {
				logger.traceError("process", "Error(InterruptedException) en process: ", e);
				return;
			}
		}
		logger.endTrace("process", "Finalizado", null);
	}

	/**
	 * Levanta todos los threads esclavos
	 */
	private void initSlaves() throws AligareException {
		logger.initTrace("initSlaves", "Integer nroCaja, Integer nroSucusal, Integer nroEstado");

		int numThread = 1;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();
		sem = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
//			EsclavoBoleta slave = new EsclavoBoleta(idx, this);
			EsclavoBoleta slave = new EsclavoBoleta();
			slaves.add(slave);
			try {
				sem.acquire();
			} catch (Exception e) {
				logger.traceError("initSlaves", "Error(InterruptedException) en initSlaves(): ", e);
				return;
			}
			
			slave.start();
		}
		logger.endTrace("initSlaves", "Finalizado", null);
	}

	public ConcurrentLinkedQueue<TramaDTE> getqOrdenCompra() {
		return qOrdenCompra;
	}

	public Semaphore getSem() {
		return sem;
	}

	public Parametros getParametros() {
		return parametros;
	}

}
