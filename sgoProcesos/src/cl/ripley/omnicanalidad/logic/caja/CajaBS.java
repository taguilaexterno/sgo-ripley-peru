package cl.ripley.omnicanalidad.logic.caja;

import com.ripley.restservices.model.generic.GenericResponse;

import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Capa de negocio para operaciones caja
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 08-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface CajaBS {

	/**Cierra y abre la caja especificada en los parámetros
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 08-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param numCaja Número de caja.
	 * @param funcion función del proceso (BOLETA o NC).
	 * @return {@linkplain GenericResponse}
	 */
	GenericResponse cerrarAperturarCaja(Integer numCaja, Integer funcion) throws RestServiceTransactionException;
	
}
