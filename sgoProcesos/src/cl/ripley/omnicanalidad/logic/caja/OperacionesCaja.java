package cl.ripley.omnicanalidad.logic.caja;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ripley.dao.ICajaDAO;
import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.TotalesCierre;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Clase con funcionalidad de operaciones de caja (abrir y cerrar caja).
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class OperacionesCaja{
	private static final AriLog logger = new AriLog(OperacionesCaja.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private CajaVirtualDAO caja;
	@Autowired
	private ICajaDAO cajaDAO;
	
	/**Apertura la caja llamando al servicio de BackOffice.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param pcv
	 * @param fecha
	 * @param fechaCierre
	 * @param fechaHora
	 * @return
	 * @throws AligareException
	 */
	public boolean aperturarCaja(Parametros pcv, String fecha, String fechaCierre, String fechaHora) throws AligareException {
		logger.initTrace("aperturarCaja", "Parametros: parametros", 
				new KeyLog("Parametros", pcv.toString()));
		
		
		Integer nroCaja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		int nroTrx    = cajaVirtual(pcv, Boolean.FALSE);
		String newFecha = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, pcv);
		String newFechaHora = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD_HH_MI_SS, pcv);
		
		if(!fecha.equals(newFecha)) {
			
			cajaDAO.updTrxCaja(nroCaja, sucursal, newFecha, Long.valueOf(Constantes.NRO_CERO));
			
			nroTrx    = cajaVirtual(pcv, Boolean.FALSE);
			
			cajaDAO.updTrxCaja(nroCaja, sucursal, newFecha, Long.valueOf(nroTrx));
			
		} else {
			
			cajaDAO.updTrxCaja(nroCaja, sucursal, newFecha, Long.valueOf(nroTrx));
			
		}
		
		int maxIntentos = Integer.parseInt(pcv.buscaValorPorNombre("INTENTOS_APER_CAJA_ERROR_CONSTRAINT"));
		boolean cajaAbierta=false;
		int intentos = Constantes.NRO_UNO; //inicio para hacer log facil de leer
		
		RetornoEjecucion retorno = new RetornoEjecucion();
		try {
			logger.traceInfo("aperturarCaja", "Ejecutando caja.existeCajaCerrada en aperturarCaja");
				//Busca en tablas Nota_Venta e Identificador_Marketplace 
				caja.cambiaFechaBoletasSinProcesar(newFecha, pcv);
				//valida que sucursal est� abierta
						
				boolean aperturaBO = caja.aperturaCajaNew(Long.valueOf(nroTrx), pcv, newFecha, newFechaHora);
				logger.traceInfo("aperturarCaja", "Resultado apertura caja en BO", new KeyLog("Resultado Apertura Broker BO", String.valueOf(aperturaBO)));
				
				if(!aperturaBO) {
					
					return Boolean.FALSE;
					
				}
						
						//BPL Se ejecuta para el error UniqueConstraint
						while (!cajaAbierta && intentos  <= maxIntentos){
							logger.traceInfo("aperturarCaja", "Intento de ejecución de Registrar Apertura Cierre en Modelo Extendido " + intentos);
							//se obtiene mayor id apertura
							Long nroIdApertura = caja.obtieneMayorIdAperturaCierre();
							retorno = caja.registraAperturaCierreCaja(pcv,nroTrx,0,newFechaHora,"",Constantes.TIPO_LLAMADO_APERTURA,nroIdApertura);						
							if (retorno.getCodigo().intValue() == Constantes.EJEC_SIN_ERRORES){
								cajaAbierta=true;	
							}
							else if(retorno.getCodigo().intValue() == Constantes.EJEC_CON_ERRORES 
									&& retorno.getMensaje().indexOf(Constantes.ERROR_UNIQUE_CONSTRAINT)>Constantes.NRO_CERO ){
								intentos++;
							}
							else{
								intentos = maxIntentos;
							}
						}
			
		} catch (Exception e) {
			logger.traceError("aperturarCaja", e);
			throw new AligareException("Error en aperturarCaja: ");
		}	finally {		
			logger.endTrace("aperturarCaja", "Finalizado", "Caja Abierta?: " + cajaAbierta);
		}
		return cajaAbierta;
	}
	
	/**Realiza el cierre de la caja llamando al servicio de BackOffice.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param pcv
	 * @param fecha
	 * @param fechaHora
	 * @return
	 * @throws AligareException
	 */
	public boolean cerrarCaja(Parametros pcv, String fecha, String fechaHora) throws AligareException {
		
		logger.initTrace("cerrarCaja", "Parametros: parametros", new KeyLog("NroCaja", pcv.buscaValorPorNombre("NRO_CAJA")), 
				new KeyLog("Sucursal", pcv.buscaValorPorNombre("SUCURSAL")), 
				new KeyLog("fecha: ", fecha), new KeyLog("fechaHora: ",fechaHora));
		//variables
		boolean cajaCerrada=false; 
		try {
			//Se obtiene nroTrx
			int nroTrx = cajaVirtual(pcv, Boolean.FALSE, fecha, fechaHora);
			
			Integer nroCaja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
			Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
			
			cajaDAO.updTrxCaja(nroCaja, sucursal, fecha, Long.valueOf(nroTrx));
			
			Long nroIdAperturaCierre = caja.obtieneIdAperturaCierre(pcv);
			
			//Totales u
			TotalesCierre tcierre = caja.totales(pcv, fecha, nroTrx, nroIdAperturaCierre);
			
			
			logger.traceInfo("cerrarCaja", "Ejecutando caja.existeCajaCerrada en cerrarCaja");
			logger.traceInfo("cerrarCaja", "Ejecutando caja.cambioEstadoCaja en cerrarCaja");
			logger.traceInfo("cerrarCaja", "Ejecutando caja.cierreCajaIntegrada en cerrarCaja");
				
				boolean cierreBO = caja.cierreCajaIntegradaNew(new Long(nroTrx), pcv, fecha, fechaHora, tcierre);
				logger.traceInfo("cerrarCaja", "Resultado cierre caja en BO", new KeyLog("Resultado Cierre Broker BO", String.valueOf(cierreBO)));
				if(!cierreBO) {
					return Boolean.FALSE;
				}
					logger.traceInfo("cerrarCaja", "Ejecutando caja.obtieneIdAperturaCierre en cerrarCaja");
					if (caja.registraAperturaCierreCaja(pcv,0,nroTrx,"",fechaHora,Constantes.TIPO_LLAMADO_CIERRE,nroIdAperturaCierre)
							.getCodigo() == Constantes.EJEC_SIN_ERRORES){
						cajaCerrada=Boolean.TRUE;	
					}
					
//					if (cajaCerrada && Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")) == Constantes.FUNCION_BOLETA){
//							//totales mail cuadratura
//							TotalCuadratura totalCuadratura = caja.datosMailCuadratura(pcv, nroIdAperturaCierre);
//							String ruta =Constantes.RUTA_INFORMES+Constantes.NOMBRE_ARCHIVO_CIERRE_BOL+pcv.buscaValorPorNombre("SUCURSAL")+pcv.buscaValorPorNombre("NRO_CAJA")+caja.sysdateFromDual(Constantes.YYYYMMYY_HHMI, pcv).replaceAll(" ", "_")+Constantes.EXTENSION_CIERRE;
//							//generaci�n archivo excel
//							caja.generaArchivoDeCierre(pcv,nroIdAperturaCierre,ruta);
//							//preparación de correo...
//							VoucherTemplateDAO voucherTemplate = new VoucherTemplateDAOImpl();
//							TemplateVoucher tv = new TemplateVoucher();
//							tv = voucherTemplate.getTemplateByLlave("CIERRE_CAJA");
//							String html = tv.getHtml();
//							HashMap<String, String> valores = new HashMap<String, String>();
//							valores.put("fechaCierre", fecha.toString());
//							valores.put("tituloCierre", "VENTAS Y RECAUDACIONES");
//							valores.put("horaCierre", fechaHora.toString());
//							valores.put("txtTipo","");
//							
//							valores.put("txtTotalVentas", String.format(Locale.US, "%.2f", totalCuadratura.getBoletasFacturas()));
//							valores.put("txtTotalReca", String.format(Locale.US, "%.2f", totalCuadratura.getRecaudaciones()));
//							valores.put("txtTotalEfectivo", "0");
//							valores.put("txtUnidades", "Unidades de productos (solo Ventas)");							
//							valores.put("txtTotalUnidades", String.format(Locale.US, "%.2f", totalCuadratura.getUnidadesImp()));
//							
//							valores.put("txtTotales", "Totales Ventas y Recaudaciones");	
//							valores.put("txtTotal", String.format(Locale.US, "%.2f", totalCuadratura.getBoletasFacturas().add(totalCuadratura.getRecaudaciones()) ));
//							
//							valores.put("nroCaja",pcv.buscaValorPorNombre("NRO_CAJA"));
//							valores.put("Url_cloudfront", pcv.buscaValorPorNombre("URL_CLOUDFRONT"));
//							
//							//HashMap<String, String> lista = new HashMap<String, String>();
//							
//							ClienteSMTP clienteSmtp = new ClienteSMTP();
//							html = clienteSmtp.completarHTML(html, valores, null);
//							String sFichero = ruta;
//							java.io.File fichero = new java.io.File(sFichero);
//							java.util.List<java.io.File> listado = new java.util.ArrayList<java.io.File>();
//							listado.add(fichero);
//							
//							//TODO Destinatarios de los correos.
//							clienteSmtp.enviarCorreoConAdjunto(pcv.buscaValorPorNombre("URL"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "CIERRE", html, pcv.buscaValorPorNombre("REMITENTE"), pcv.buscaValorPorNombre("MAIL_CUADRATURA"), pcv.buscaValorPorNombre("MAIL_CC_CUADRATURA"),listado);
//								
//						
//					}else if (cajaCerrada && Integer.parseInt(pcv.buscaValorPorNombre("FUNCION")) == Constantes.FUNCION_NC){
//						//totales mail cuadratura
//						TotalCuadratura totalCuadratura = caja.datosMailCuadratura(pcv, nroIdAperturaCierre);
//						String ruta=Constantes.RUTA_INFORMES+Constantes.NOMBRE_ARCHIVO_CIERRE_NC+caja.sysdateFromDual(Constantes.YYYYMMYY_HHMI, pcv).replaceAll(" ", "_")+Constantes.EXTENSION_CIERRE;;
//						//correo por nota credito
//						//generación archivo excel
//						caja.generaArchivoDeCierre(pcv,nroIdAperturaCierre, ruta);
//						//preparación de correo...
//						VoucherTemplateDAO voucherTemplate = new VoucherTemplateDAOImpl();
//						TemplateVoucher tv = new TemplateVoucher();
//						tv = voucherTemplate.getTemplateByLlave("CIERRE_CAJA");
//						String html = tv.getHtml();
//						HashMap<String, String> valores = new HashMap<String, String>();
//						valores.put("fechaCierre", fecha.toString());
//						valores.put("tituloCierre", "INFORME CIERRE NC");
//						valores.put("horaCierre", fechaHora.toString());
//						valores.put("txtTipo","N/C");
//						
//						valores.put("txtTotalVentas", String.format(Locale.US, "%.2f", totalCuadratura.getBoletasFacturas()));
//						valores.put("txtTotalReca", String.format(Locale.US, "%.2f", totalCuadratura.getRecaudaciones()));
//						valores.put("txtTotalEfectivo", String.format(Locale.US, "%.2f", tcierre.getMontoEfectivo().multiply(Constantes.NRO_MENOSUNO_B) ));
//						
//						valores.put("txtUnidades", Constantes.VACIO);							
//						valores.put("txtTotalUnidades", Constantes.VACIO);	
//						
//						valores.put("txtTotales", "Totales<br>* N/C Ventas y Recaudaciones");		
//						valores.put("txtTotal", String.format(Locale.US, "%.2f", totalCuadratura.getBoletasFacturas().add(totalCuadratura.getRecaudaciones()) ));
//						
//						valores.put("nroCaja",pcv.buscaValorPorNombre("NRO_CAJA"));
//						valores.put("Url_cloudfront", pcv.buscaValorPorNombre("URL_CLOUDFRONT"));
//						
//						ClienteSMTP clienteSmtp = new ClienteSMTP();
//						html = clienteSmtp.completarHTML(html, valores, null);
//						String sFichero = ruta;
//						java.io.File fichero = new java.io.File(sFichero);
//						java.util.List<java.io.File> listado = new java.util.ArrayList<java.io.File>();
//						listado.add(fichero);
//						//Destinatarios de los correos.
//						clienteSmtp.enviarCorreoConAdjunto(pcv.buscaValorPorNombre("URL"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "CIERRE", html, pcv.buscaValorPorNombre("REMITENTE"), pcv.buscaValorPorNombre("MAIL_CUADRATURA"), pcv.buscaValorPorNombre("MAIL_CC_CUADRATURA"),listado);
//						
//					}
				
//			}			
			
			
		} catch (Exception e) {
			logger.traceError("cerrarCaja", e);
			cajaCerrada=false;
		}
		return cajaCerrada;
	}
	
	public int cajaVirtual(Parametros pcv, boolean esOC) {
		
		logger.initTrace("cajaVirtual", "Parametros: parametros", 
				new KeyLog("NroCaja", pcv.buscaValorPorNombre("NRO_CAJA")), 
				new KeyLog("Sucursal", pcv.buscaValorPorNombre("SUCURSAL")));
		
		
//		String fecha=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, pcv);
//		String fechaHora=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, pcv);
//		String fechaCierre="";
//		String fechaHoraCierre="";
		logger.traceInfo("cajaVirtual", "Ejecutando caja.actualizaSaldoVolumen en cajaVirtual");
//		caja.actualizaSaldoVolumen(pcv, fechaHora, esOC);
//		if (!caja.actualizaSaldoVolumen(pcv, fechaHora, esOC)) {
//			
//			fecha=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, pcv);
//			fechaHora=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD_HH_MI_SS, pcv);
//			fechaCierre=fecha;
//			fechaHoraCierre=fechaHora;
//
//			logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.cerrarCaja en cajaVirtual");
//			boolean resultado = OperacionesCaja.cerrarCaja(pcv, fechaCierre,fechaHoraCierre);
//			
//			if(resultado) {
//				
//				logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.aperturarCaja en cajaVirtual");
//				resultado = OperacionesCaja.aperturarCaja(pcv,fecha,fechaCierre, fechaHora);
//				
//				if(!resultado) {
//					
//					return Constantes.NRO_CERO;
//					
//				}
//				
//			} else {
//				
//				logger.traceInfo("OperacionesCaja", "Ejecutando Cirre de caja no exitoso en cajaVirtual");
//				
//			}
//			
//			
//			
//		}
		
		String fecha=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, pcv);
		
		logger.traceInfo("cajaVirtual", "Fechas Obtenidas", new KeyLog("fecha", fecha));
		
		Integer caja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		Long trx = cajaDAO.getTrxCaja(caja, sucursal, fecha);
		
		logger.traceInfo("cajaVirtual", "Caja y Nro Trx", new KeyLog("caja", String.valueOf(caja)), 
														  new KeyLog("trx", String.valueOf(trx)));
		
		if(trx != null && trx.longValue() == Constantes.NRO_CERO) {
			
			cajaDAO.updTrxCaja(caja, sucursal, fecha, Long.valueOf(Constantes.NRO_CERO));
			
			trx = cajaDAO.getTrxCaja(caja, sucursal, fecha);
			
		}
		
		logger.endTrace("cajaVirtual", "Nro Trx ", String.valueOf(trx));	

		return trx.intValue();
	}
	
	
	/**Metodo SOLO para cierre de las cajas.
	 * @author Boris Parra 
	 * @param pcv
	 * @param esOC
	 * @param fecha
	 * @param fechaHora
	 * @return nroTrx
	 */
	public int cajaVirtual(Parametros pcv, boolean esOC, String fecha, String fechaHora) {
		
		logger.initTrace("cajaVirtual", "Parametros: parametros", 
				new KeyLog("NroCaja", pcv.buscaValorPorNombre("NRO_CAJA")), 
				new KeyLog("Sucursal", pcv.buscaValorPorNombre("SUCURSAL")));

		logger.traceInfo("cajaVirtual", "Ejecutando caja.actualizaSaldoVolumen en cajaVirtual Cierre");
		
		logger.traceInfo("cajaVirtual", "Fechas Obtenidas Cierre", new KeyLog("fecha", fecha), 
																   new KeyLog("fechaHora", fechaHora));
		
		Integer caja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		Long trx = cajaDAO.getTrxCaja(caja, sucursal, fecha);
		
		logger.traceInfo("cajaVirtual", "Caja y Nro Trx Cierre", new KeyLog("caja", String.valueOf(caja)), 
														  		 new KeyLog("trx", String.valueOf(trx)));
		
		if(trx != null && trx.longValue() == Constantes.NRO_CERO) {
			
			cajaDAO.updTrxCaja(caja, sucursal, fecha, Long.valueOf(Constantes.NRO_CERO));
			
			trx = cajaDAO.getTrxCaja(caja, sucursal, fecha);
			
		}
		
		logger.endTrace("cajaVirtual", "Nro Trx Cierre", String.valueOf(trx));	

		return trx.intValue();
	}
	
	/**Para servicio de cierre y apertura de caja
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 08-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param pcv
	 * @param esOC
	 * @return {@linkplain GenericResponse}
	 */
	public GenericResponse cajaVirtual2(Parametros pcv, boolean esOC) throws RestServiceTransactionException {
		
		logger.initTrace("cajaVirtual2", "Parametros: parametros", 
				new KeyLog("NroCaja", pcv.buscaValorPorNombre("NRO_CAJA")), 
				new KeyLog("Sucursal", pcv.buscaValorPorNombre("SUCURSAL")));
		
		GenericResponse resp = new GenericResponse();
		
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje("OK");
		
		String fecha=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, pcv);
		String fechaHora=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, pcv);
		String fecha2=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, pcv);
		String fechaHora2=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD_HH_MI_SS, pcv);
		String fechaCierre="";
		String fechaHoraCierre="";
		logger.traceInfo("cajaVirtual2", "Ejecutando caja.actualizaSaldoVolumen en cajaVirtual");
		if (!caja.actualizaSaldoVolumen(pcv, fechaHora, esOC)) {
			
			fechaCierre=fecha2;
			fechaHoraCierre=fechaHora2;
			
			logger.traceInfo("cajaVirtual2", "Ejecutando OperacionesCaja.cerrarCaja en cajaVirtual");
			boolean resultado = cerrarCaja(pcv, fechaCierre,fechaHoraCierre);
			
			if(resultado) {
				
				logger.traceInfo("cajaVirtual2", "Ejecutando OperacionesCaja.aperturarCaja en cajaVirtual");
				resultado = aperturarCaja(pcv,fecha2,fechaCierre, fechaHora2);
				
				if(!resultado) {
					
					logger.traceInfo("cajaVirtual2", "Apertura de caja no exitoso.");
					
					resp.setCodigo(Constantes.NRO_MENOSUNO);
					resp.setMensaje("No se pudo aperturar la caja");
					
					logger.endTrace("cajaVirtual2", "Finalizado NOK", String.valueOf(resp));
					
					throw new RestServiceTransactionException("No se pudo aperturar la caja", Constantes.NRO_MENOSUNO);
					
				}
				
			} else {
				
				logger.traceInfo("cajaVirtual2", "Cierre de caja no exitoso.");
				
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje("No se pudo cerrar la caja");
				
				logger.endTrace("cajaVirtual2", "Finalizado NOK", String.valueOf(resp));
				
				throw new RestServiceTransactionException("No se pudo cerrar la caja", Constantes.NRO_MENOSUNO);
				
			}
			
			
			
		}
		
		logger.endTrace("cajaVirtual2", "Finalizado OK", String.valueOf(resp));
		return resp;
	}

	public boolean cajaVirtualCerrar(Parametros pcv) {
		logger.initTrace("cajaVirtualCerrar", "Parametros: parametros", 
				new KeyLog("NroCaja", pcv.buscaValorPorNombre("NRO_CAJA")), 
				new KeyLog("Sucursal", pcv.buscaValorPorNombre("SUCURSAL")));
		
		String fecha=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, pcv);
		String fechaHora=caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, pcv);
		String fechaCierre="";
		String fechaHoraCierre="";
		
		fecha=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, pcv);
		fechaHora=caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD_HH_MI_SS, pcv);
		
		fechaCierre=fecha;
		fechaHoraCierre=fechaHora;
			
		if (pcv.buscaValorPorNombre("FUNCION").equalsIgnoreCase(String.valueOf(Constantes.FUNCION_BOLETA))){
			logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.cerrarCaja en cajaVirtualCerrar");
			cerrarCaja(pcv, fechaCierre,fechaHoraCierre);
			logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.aperturarCaja en cajaVirtualCerrar");
			aperturarCaja(pcv,fecha,fechaCierre,fechaHora);	
		
		}else{
			logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.cerrarCaja en cajaVirtualCerrar");
			cerrarCaja(pcv, fechaCierre,fechaHoraCierre);
			logger.traceInfo("OperacionesCaja", "Ejecutando OperacionesCaja.aperturarCaja en cajaVirtualCerrar");
			aperturarCaja(pcv,fecha, fechaCierre,fechaHora);	
		}
		
		return true;//(caja.resNroTransaccion(pcv));
	}
	
	/**Metodo que rescata las cajas y sucursales mediante su JOB_JENKINS (RPOS)
	 *
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 19-10-2018
	 *<br/><br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param jobJenkins
	 * @return List<SucursalRpos>
	 */
	public List<SucursalRpos> cajaSucursalRpos(int jobJenkins){
		logger.initTrace("cajaSucursalRpos", "jobJenkins: " + String.valueOf(jobJenkins), 
				new KeyLog("jobJenkins", String.valueOf(jobJenkins)));
		List<SucursalRpos> resp = caja.cajaSucursalRpos(jobJenkins);
		logger.endTrace("cajaSucursalRpos", "Finalizado OK", String.valueOf(resp));
		return resp;
	}
	
	/**Metodo que rescata las cajas y sucursales mediante su JOB_URL_DOCE (RPOS)
	 *
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 11-12-2018
	 *<br/><br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param jobUrlDoce
	 * @return List<SucursalRpos>
	 */
	public List<SucursalRpos> cajaSucUrlDoce(int jobUrlDoce){
		logger.initTrace("cajaSucUrlDoce", "jobUrlDoce: " + String.valueOf(jobUrlDoce), 
				new KeyLog("jobJenkins", String.valueOf(jobUrlDoce)));
		List<SucursalRpos> resp = caja.cajaSucUrlDoce(jobUrlDoce);
		logger.endTrace("cajaSucUrlDoce", "Finalizado OK", String.valueOf(resp));
		return resp;
	}
}
