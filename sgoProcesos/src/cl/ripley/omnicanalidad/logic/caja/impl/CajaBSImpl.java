package cl.ripley.omnicanalidad.logic.caja.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.logic.caja.CajaBS;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Implementación capa de negocio para operaciones caja.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 08-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class CajaBSImpl implements CajaBS {
	
	private static final AriLog LOG = new AriLog(CajaBSImpl.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private ParametrosDAO paramDAO;
	
	@Autowired
	private OperacionesCaja operacionesCaja;
	
	@Autowired
	private Parametros parametrosCajaVirtual;

	@Override
	@Transactional(rollbackFor = {RuntimeException.class, Exception.class})
	public GenericResponse cerrarAperturarCaja(Integer numCaja, Integer funcion) throws RestServiceTransactionException {
		LOG.initTrace("cerrarAperturarCaja", "Integer numCaja, Integer funcion", 
				new KeyLog("Numero Caja", String.valueOf(numCaja)),
				new KeyLog("Función (BOLETA = 2, NC = 1)", String.valueOf(funcion)));
		
		boolean flagRpos=false;
		String[] cajasBoletas= {};
		String[] cajasNC = {};
		GenericResponse resp = new GenericResponse();
		
		if(numCaja<Constantes.NRO_CERO && numCaja != Integer.valueOf(parametrosCajaVirtual.buscaValorPorNombre(Constantes.NRO_CAJA_BOLETAS))) {
			flagRpos=true;
		}else {
			cajasBoletas = parametrosCajaVirtual.buscaValorPorNombre(Constantes.NROS_CAJAS_BOLETAS).split(Constantes.COMA);
			cajasNC = parametrosCajaVirtual.buscaValorPorNombre(Constantes.NROS_CAJAS_NC).split(Constantes.COMA);
		}
		
		Parametros pcv = new Parametros();
		pcv.setParametros(new ArrayList<Parametro>());

		if(!flagRpos) {
			LOG.traceInfo("cerrarAperturarCaja", "Tienda Virtual");
			boolean isBoleta =	Stream.of(cajasBoletas).anyMatch(it -> it.equals(String.valueOf(numCaja)));
			if(isBoleta) {
				paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, pcv.getParametros(), "1");
				pcv.reemplazaValorLista(Constantes.NRO_CAJA_BOLETAS, String.valueOf(numCaja));
				pcv.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(numCaja));
				Parametro param = new Parametro();
				param.setNombre(Constantes.FUNCION);
				param.setValor(String.valueOf(Constantes.FUNCION_BOLETA));
				pcv.getParametros().add(param);
				
			} else {
				paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_NC, pcv.getParametros(), "1");
				pcv.reemplazaValorLista(Constantes.NRO_CAJA_NC, String.valueOf(numCaja));
				pcv.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(numCaja));
				
				Parametro param = new Parametro();
				param.setNombre(Constantes.FUNCION);
				param.setValor(String.valueOf(Constantes.FUNCION_NC));
				pcv.getParametros().add(param);
			}
			
			resp = operacionesCaja.cajaVirtual2(pcv, Boolean.FALSE);
			
		}else {
			try {
				LOG.traceInfo("cerrarAperturarCaja", "Rpos");
				//como regla en el Job cierreAperturaCaja para RPOS, el número de caja sera negativo para no interactuar con alguna caja habilitada
				List<SucursalRpos> list = operacionesCaja.cajaSucursalRpos(numCaja*Constantes.NRO_MENOSUNO);
				paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, pcv.getParametros(), "1");
				for(SucursalRpos cajaSuc: list) {
					pcv.reemplazaValorLista(Constantes.NRO_CAJA_BOLETAS, String.valueOf(cajaSuc.getCaja()));
					pcv.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(cajaSuc.getCaja()));
					pcv.reemplazaValorLista(Constantes.PARAM_SUCURSAL_NAME, String.valueOf(cajaSuc.getSucursal()));
					Parametro param = new Parametro();
					param.setNombre(Constantes.FUNCION);
					param.setValor(String.valueOf(Constantes.FUNCION_BOLETA));
					pcv.getParametros().add(param);
					try {
						LOG.traceInfo("cerrarAperturarCaja", "cajaVirtual2, Rpos Caja= "+cajaSuc.getCaja()+", Sucursal= "+cajaSuc.getSucursal());
						operacionesCaja.cajaVirtual2(pcv, Boolean.FALSE);
						LOG.traceInfo("cerrarAperturarCaja", "cajaVirtual2 OK");
					} catch (RestServiceTransactionException e) {
						LOG.traceError("cerrarAperturarCaja Rpos, caja: "+cajaSuc.getCaja(), e);
					}
				}
				resp.setCodigo(Constantes.NRO_CERO);
				resp.setMensaje("OK");
			}catch(Exception e) {
				LOG.traceError("cerrarAperturarCaja", e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje("Error cerrarAperturarCaja caja");
				return resp;
			}
		}
		LOG.endTrace("cerrarAperturarCaja", "Finalizado", "resp = " + String.valueOf(resp));
		return resp;
	}

}
