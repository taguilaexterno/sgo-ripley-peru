package cl.ripley.omnicanalidad.logic.cyberSource.impl;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.cyberSource.OrdenesCS;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import com.ripley.dao.OrdenesCSDAO;
import com.ripley.dao.dto.OrdenesCSDTO;
import com.ripley.restservices.model.cyberSource.CyberSourceResponse;
import com.ripley.restservices.model.generic.GenericResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;  
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service
public class OrdenesCSImpl implements OrdenesCS {    
    public static final AriLog LOG = new AriLog(OrdenesCSImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
    private List<String> items;
    private List<Long> itemsOC;
    private int count;
    private String msgxml;
    private long msgnoc;
    
    @Autowired
    private OrdenesCSDAO OrdenesCSDAO; // OrdenesCSDAOImpl
//  private InsertaModeloExtendidoDAO insertaModeloExtendidoDAO

    @Override
    @Transactional(rollbackFor = Exception.class)
    public GenericResponse actualizaEstado01CS(String xml) throws Exception{
        LOG.initTrace("actualizaEstadoCS", "String xml", 
                new KeyLog("XML", String.valueOf(xml)));
//              new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));

        GenericResponse resp = new GenericResponse();
     // OrdenesCSDAO csdao = new OrdenesCSDAO();
     // CyberSourceResponse resp2 = new CyberSourceResponse()

//      LOG.traceInfo("actualizaEstadoCS", "Se va a insertar la OC = " + String.valueOf(correlativoVenta));
        try {
            String decodeXML = decode(xml);
            LOG.traceInfo("actualizaEstadoCS", "decodeXML = " + decodeXML);
            System.out.println("Decoded XML: "+decodeXML);  

            CyberSourceResponse StringXML = StringXML(decodeXML);
            
            if(StringXML.getCodigo().intValue() != Constantes.NRO_CERO) {
                LOG.traceInfo("actualizaEstadoCS", "Error al extraer XML", 
                                    new KeyLog("Codigo Respuesta", String.valueOf(StringXML.getCodigo())),
                                    new KeyLog("Mensaje Respuesta", String.valueOf(StringXML.getMensaje())));
                System.exit(-Constantes.NRO_UNO);
            }
            
            msgxml = StringXML.getXml();
            msgnoc = StringXML.getOc();
            //LOG.initTrace("actualizaEstadoCS", "msgxml = " + msgxml );
            //LOG.initTrace("actualizaEstadoCS", "msgnoc = " + msgnoc );
            System.out.println("msgxml = "+msgxml);
            System.out.println("msgnoc = "+msgnoc); 
            OrdenesCSDTO dto = OrdenesCSDAO.actualizaEstadoCS(msgxml, msgnoc);
            resp.setCodigo(dto.getCodigo());
            resp.setMensaje(dto.getMensaje());
            
        }catch (Exception e) {
                LOG.traceError("actualizaEstadoCS", e);
                resp.setCodigo(Constantes.NRO_MENOSUNO);
                resp.setMensaje(e.getMessage());
//        }finally {
//                Util.pasarGarbageCollector();
        }
        
        LOG.endTrace("actualizaEstadoCS", "Finalizado", "GenericResponse = " + String.valueOf(resp));
        Util.pasarGarbageCollector();
        return resp;
    }
    
    private static class Holder {
            public static final OrdenesCSImpl INSTANCE = new OrdenesCSImpl();
    }
    
    public static OrdenesCSImpl getInstance() {
            return OrdenesCSImpl.Holder.INSTANCE;
    }
    
    public static String encode(String url)  
    {  
        try {  
             String encodeURL=URLEncoder.encode( url, "UTF-8" );  
             return encodeURL;  
        } catch (UnsupportedEncodingException e) {  
             return "Issue while encoding" +e.getMessage();  
        }  
    }
	
    public static String decode(String url)  
    {  
        try {  
             String prevURL="";  
             String decodeURL=url;  
             String vacio="";
             while(!prevURL.equals(decodeURL))  
             {  
                  prevURL=decodeURL;  
                  decodeURL=URLDecoder.decode( decodeURL, "UTF-8" );  
             }  
             decodeURL=decodeURL.replaceAll("content=", vacio);
             decodeURL=decodeURL.replaceAll("content =", vacio);
             return decodeURL;  
        } catch (UnsupportedEncodingException e) {  
             return "Issue while decoding" +e.getMessage();  
        }  
    }
    
//    public GenericResponse actualizaEstadoCS(String xml) throws Exception{
//    public String StringXML(String xml) throws XPathExpressionException, ParserConfigurationException, IOException, SAXException, Exception
    public CyberSourceResponse StringXML(String xml) throws XPathExpressionException, ParserConfigurationException, IOException, SAXException, Exception
    {
        String item = null;
        Long itemOC = null;
        int ifila = 0;
        items = new ArrayList<>();
        itemsOC = new ArrayList<>();
        LOG.endTrace("StringXML", "Inicia", "Inicio");
	//LOG.traceInfo("StringXML", "URL TV", new KeyLog("XML", finalUrl));
        //String xml = rt.getForObject(finalUrl, String.class);
        XPathFactory fact = XPathFactory.newInstance();
        XPath xpath = fact.newXPath();

        CyberSourceResponse req = new CyberSourceResponse();
                
        try (ByteArrayInputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"))) {
            Document docOld = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
//--        XPathExpression express = xpath.compile("//*[local-name()='OrdenCoC']");
//--        XPathExpression express = xpath.compile("//*[local-name()='getXMLXPAYLOAD']");
            
//          XPathExpression express = xpath.compile("//*[local-name()='Update']");
            XPathExpression express2 = xpath.compile("//*[local-name()='CaseManagementOrderStatus']/Update");
//          XPathExpression express3 = xpath.compile("//*[local-name()='CaseManagementOrderStatus']/*[not(self::Update)]");
            XPathExpression express4 = xpath.compile("/Update/@MerchantReferenceNumber");
            
            NodeList nodeList = (NodeList) express2.evaluate(docOld, XPathConstants.NODESET);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                Document doc = builder.newDocument();
                node = doc.importNode(node, Boolean.TRUE);
                doc.appendChild(node);
                String it = domToStringXml(doc); // Utils.domToStringXml(doc);
                LOG.traceInfo("StringXML", "String", new KeyLog("XML como String", String.valueOf(it)));
                items.add(it);
                
                Long oc = Long.valueOf((String) express4.evaluate(doc, XPathConstants.STRING));
                itemsOC.add(oc);
                        
                
                //LOG.endTrace("Stringoc", "Stringoc", String.valueOf(oc));
                //Long oc = Long.valueOf((String) express4.evaluate(doc, XPathConstants.STRING));
                //req.setOc(oc);
            }
        }
        
        if(count < items.size()) {
            ifila = count++;
            itemOC = itemsOC.get(ifila);
            item = items.get(ifila);
            req.setOc(itemOC);
            req.setXml(item);
            req.setCodigo(Constantes.NRO_CERO);
            req.setMensaje("OK");
        } else {
            itemOC = null;
            item = null;
            req.setOc(itemOC);
            req.setXml(item);
            req.setCodigo(Constantes.NRO_MENOSUNO);
            req.setMensaje("Error");
            //items = null;
            //count = Constantes.CERO;
        }

        LOG.endTrace("StringXML", "Finalizado", "item = " + String.valueOf(item));
        return req; 
        //return item;
    }
    
    //public static final 
    public String domToStringXml(Document doc) throws Exception {

        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        tf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

        try(Writer writer = new StringWriter()) {

                tf.transform(new DOMSource(doc), new StreamResult(writer));
                return ((StringWriter) writer).toString();
        }

    }

}
