package cl.ripley.omnicanalidad.logic.insertaModeloExtendido;

import com.ripley.restservices.model.generic.GenericResponse;

/**Interface de servicio para insertar una OC al Modelo Extendido
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface IInsertaModeloExtendidoService {

	/**Método que se encarga de llamar al DAO para insertar una OC nueva al modelo extendido.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 26-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param xml
	 * @param correlativoVenta
	 * @return
	 * @throws Exception
	 */
	GenericResponse insertaModeloExtendido(String xml, Long correlativoVenta) throws Exception;
	
	/**Obtiene el último Id Interno de OCs de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 16-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws Exception
	 */
	GenericResponse getLastId() throws Exception;
	
	/**Actualiza el último Id Interno de OCs de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 16-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws Exception
	 */
	GenericResponse updLastId(String lastId) throws Exception;
	
	/**Obtiene la última fecha que se consultaron OCs de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 04-06-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws Exception
	 */
	GenericResponse getLastInsertOCDateTime() throws Exception;
	
	/**Actualiza el la última fecha que se consultaron OCs de la TV
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 04-06-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return
	 * @throws Exception
	 */
	GenericResponse updLastInsertOCDateTime(String lastTime) throws Exception;
	
}
