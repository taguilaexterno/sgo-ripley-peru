package cl.ripley.omnicanalidad.logic.insertaModeloExtendido;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ripley.dao.InsertaModeloExtendidoDAO;
import com.ripley.dao.dto.GetLastIdDTO;
import com.ripley.dao.dto.GetLastInsertOCTimeDTO;
import com.ripley.dao.dto.InsertaModelExtendDTO;
import com.ripley.dao.dto.UpdLastIdDTO;
import com.ripley.dao.dto.UpdLastInserOCTimeDTO;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Implementacion de {@linkplain IInsertaModeloExtendidoService} para insertar una OC nueva al modelo extendido.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class InsertaModeloExtendidoService implements IInsertaModeloExtendidoService {
	
	public static final AriLog LOG = new AriLog(InsertaModeloExtendidoService.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private InsertaModeloExtendidoDAO insertaModeloExtendidoDAO;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse insertaModeloExtendido(String xml, Long correlativoVenta) throws Exception {
		LOG.initTrace("insertaModeloExtendido", "String xml, Long correlativoVenta", 
				new KeyLog("XML", String.valueOf(xml)),
				new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
		
		GenericResponse resp = new GenericResponse();
		
		LOG.traceInfo("insertaModeloExtendido", "Se va a insertar la OC = " + String.valueOf(correlativoVenta));
		InsertaModelExtendDTO dto = insertaModeloExtendidoDAO.insertaModeloExtendido(xml, correlativoVenta);
		
		resp.setCodigo(dto.getCodigo());
		resp.setMensaje(dto.getMensaje());
		
		LOG.endTrace("insertaModeloExtendido", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}

	@Override
	public GenericResponse getLastId() throws Exception {
		LOG.initTrace("getLastId", "Inicio");
		
		GenericResponse resp = new GenericResponse();
		
		GetLastIdDTO dto = insertaModeloExtendidoDAO.getLastId();
		
		resp.setCodigo(dto.getCodigo());
		resp.setMensaje(dto.getMensaje());
		resp.setData(dto.getLastId());
		
		LOG.endTrace("getLastId", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse updLastId(String lastId) throws Exception {
		LOG.initTrace("updLastId", "String lastId", new KeyLog("lastId", String.valueOf(lastId)));
		
		GenericResponse resp = new GenericResponse();
		
		UpdLastIdDTO dto = insertaModeloExtendidoDAO.updLastId(lastId);
		
		if(dto.getCodigo().intValue() != Constantes.NRO_CERO) {
			
			LOG.endTrace("updLastId", "Finalizado", "UpdLastIdDTO = " + String.valueOf(dto));
			throw new RestServiceTransactionException(dto.getMensaje(), dto.getCodigo());
			
		}
		
		resp.setCodigo(dto.getCodigo());
		resp.setMensaje(dto.getMensaje());
		
		LOG.endTrace("updLastId", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}

	@Override
	public GenericResponse getLastInsertOCDateTime() throws Exception {
		LOG.initTrace("getLastInsertOCDateTime", "Inicio");
		
		GenericResponse resp = new GenericResponse();
		
		GetLastInsertOCTimeDTO dto = insertaModeloExtendidoDAO.getLastInsertOCDateTime();
		
		resp.setCodigo(dto.getCodigo());
		resp.setMensaje(dto.getMensaje());
		resp.setData(dto.getLastDateTime());
		
		LOG.endTrace("getLastInsertOCDateTime", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}

	@Override
	public GenericResponse updLastInsertOCDateTime(String lastTime) throws Exception {
		LOG.initTrace("updLastInsertOCDateTime", "String lastTime", new KeyLog("lastTime", String.valueOf(lastTime)));
		
		GenericResponse resp = new GenericResponse();
		
		UpdLastInserOCTimeDTO dto = insertaModeloExtendidoDAO.updLastInsertOCDateTime(lastTime);
		
		if(dto.getCodigo().intValue() != Constantes.NRO_CERO) {
			
			LOG.endTrace("updLastInsertOCDateTime", "Finalizado", "UpdLastInserOCTimeDTO = " + String.valueOf(dto));
			throw new RestServiceTransactionException(dto.getMensaje(), dto.getCodigo());
			
		}
		
		resp.setCodigo(dto.getCodigo());
		resp.setMensaje(dto.getMensaje());
		
		LOG.endTrace("updLastInsertOCDateTime", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}

}
