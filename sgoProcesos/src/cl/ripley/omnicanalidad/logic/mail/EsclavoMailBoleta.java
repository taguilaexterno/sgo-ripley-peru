package cl.ripley.omnicanalidad.logic.mail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ripley.dao.IOrdenCompraDAO;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.mail.ConsultaOcMailResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.ResultadoPPL;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.PaperlessDAO;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.dao.impl.CajaVirtualDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.NotaVentaDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.PaperlessDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.VoucherTemplateDAOImpl;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.ClienteSMTP;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;
import cl.ripley.omnicanalidad.xml.GeneracionXml;


@Service
public class EsclavoMailBoleta extends Thread implements Runnable {
	private static final AriLog logger = new AriLog(EsclavoMailBoleta.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    /**
     * Cola con los folios a procesar
     */
	ConcurrentLinkedQueue<NotaVentaMail> qOrdenCompra;
	ConcurrentLinkedQueue<NotaVentaMail> qOrdenCompraMail;
	
	/**
	 * Maestro de ejecuci�n
	 */
	MaestroMail master;
	
	@Autowired
	private ParametrosDAO paramDAO;
	
	@Autowired
	private PaperlessDAO paperless;
	
	@Autowired
	private NotaVentaDAO notaVentaDAO;
	
	@Autowired
	private ClienteSMTP clienteSMTP;
	
	@Autowired
	private VoucherTemplateDAO templateDao;
	
	@Autowired
	private CajaVirtualDAO caja;
	
	@Autowired
	private IOrdenCompraDAO ordenCompraDAO;
	
	@Autowired
	private OperacionesCaja operacionesCaja;
	
	public EsclavoMailBoleta() {
		
	}
	
	/**
	 * Constructor define el nombre del esclavo con el numeral entregado como id.
	 * 
	 * @param id identificador conocido asignado por el maestro.
	 */
	public EsclavoMailBoleta(int id, MaestroMail pMaster){
		this(id, pMaster, pMaster.getqOrdenCompra(), pMaster.getqOrdenCompraMail());
	}
	
	public EsclavoMailBoleta(int id, MaestroMail pMaster, ConcurrentLinkedQueue<NotaVentaMail> pOrdenCompra,  ConcurrentLinkedQueue<NotaVentaMail> pOrdenCompraMail){
		this.setName("SLAVE BOLETA "+id);
		master = pMaster;
		qOrdenCompra = pOrdenCompra;
		qOrdenCompraMail = pOrdenCompraMail;
	}
	
	@Override
	public void run() throws AligareException {

		this.process();
		super.run();

		master.getSemBol().release();
	}
	
	public void process() throws AligareException {
		logger.initTrace("process", null);

		NotaVentaMail inOrdenCompra = null;
		int a = 0;
		int zx = qOrdenCompra.size();
		int z = qOrdenCompraMail.size();
		
		while ((inOrdenCompra = qOrdenCompraMail.poll()) != null){ //obtengo una OC de la cola
			logger.traceInfo("process", "MAIL PPL"+this.getName()+ " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " "+ (++a) + " de "+z);

			int minutosCompraMin = Integer.parseInt(master.getParametros().buscaValorPorNombre("TIME_PPL_EMAIL_MINUTOS"));
			
			if (minutosCompraMin > inOrdenCompra.getNotaVenta().getMinutosCompra().intValue()){
				continue;
			}

			if (inOrdenCompra.getNotaVenta().getFolioSii() == null){
				continue;
			}
			
//			if (!enviarEmailBoleta(inOrdenCompra)){
//				logger.traceInfo("process", "Error en envio email boleta", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO FOLIO PPL", inOrdenCompra.getNotaVenta().getFolioSii().toString()));
//			}

			logger.traceInfo("process", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
		}
		
		
		
		while ((inOrdenCompra = qOrdenCompra.poll()) != null){ //obtengo una OC de la cola
			logger.traceInfo("process", "MAIL PPL"+this.getName()+ " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " "+ (++a) + " de "+zx);

			int minutosCompraMin = Integer.parseInt(master.getParametros().buscaValorPorNombre("TIME_PPL_EMAIL_MINUTOS"));
			
			if (minutosCompraMin > inOrdenCompra.getNotaVenta().getMinutosCompra().intValue()){
				continue;
			}

			if (inOrdenCompra.getNotaVenta().getFolioSii() == null){
				continue;
			}
			
//			if (!enviarEmailBoletaTransac(inOrdenCompra)){
//				logger.traceInfo("process", "Error en envio email boleta", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO FOLIO PPL", inOrdenCompra.getNotaVenta().getFolioSii().toString()));
//			}

			logger.traceInfo("process", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
		}
		
		
		logger.endTrace("process", "Finalizado", null);
	}
	
	public boolean enviarEmailBoleta(NotaVentaMail trama) throws AligareException {
		boolean resultado = false;
		
		NotaVentaDAO notaVenta = new NotaVentaDAOImpl();
		
		PaperlessDAO paperless = new PaperlessDAOImpl(master.getParametros().buscaValorPorNombre("TARGETENDPOINT_PPL"));
		ResultadoPPL resultadoPpl = new ResultadoPPL();
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		TemplateVoucher template = new TemplateVoucher();
		VoucherTemplateDAO templateDao = new VoucherTemplateDAOImpl();
		String asunto = "";
		String tipoDoc = "";
		String urlPaperless = "";
//		String flagMail="";
		
		 String swith_all_correo = master.getParametros().buscaValorPorNombre("SWITCH_ALL_CORREO_SGO");
	     String swith_correo_sgo_normal = master.getParametros().buscaValorPorNombre("SWITCH_CORREO_SGO");
	     String swith_correo_sgo_transac = master.getParametros().buscaValorPorNombre("SWITCH_CORREO_SGO_TRANSAC");
	     String swith_correo_por_mpos = master.getParametros().buscaValorPorNombre("SWITCH_CORREO_SGO_MPOS");
		
	
		if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TREINTANUEVE){
			asunto = "Boleta Electr�nica Ripley.com";
			template = templateDao.getTemplateByLlave("BOLETA");
			tipoDoc = trama.getTipoDoc().getCodPpl().toString();
		} else {
			asunto = "Factura Electr�nica Ripley.com";
			template = templateDao.getTemplateByLlave("FACTURA");
			tipoDoc = trama.getTipoDoc().getCodPpl().toString();
		}

		if (template.getLlave() == null){
			logger.traceInfo("process", "Correo '"+asunto+"' no se encontro o esta desactivado.");
			return false;
		}
		asunto = asunto + " OC: "+trama.getNotaVenta().getCorrelativoVenta();
		
		String nroFolio = trama.getNotaVenta().getFolioSii().toString();
			
		resultadoPpl = paperless.recuperacionOnline(master.getParametros().buscaValorPorNombre("PPL_RUT"), master.getParametros().buscaValorPorNombre("PPL_LOGIN"), master.getParametros().buscaValorPorNombre("PPL_PASS"), tipoDoc, nroFolio, Constantes.NRO_DOS, Integer.parseInt(master.getParametros().buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));
		//resultadoPpl = null;
		if (resultadoPpl != null && resultadoPpl.getCodigo() != Constantes.NRO_CERO){
			if (resultadoPpl.getCodigo() == Constantes.NRO_MENOSDOCE){
				logger.traceInfo("enviarEmailBoleta", "Folio no encontrado en PPL aun", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
				return true;
			}
			logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL MAIL", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
			return false;
		} else {
			if (resultadoPpl != null && resultadoPpl.getMensaje() != null){
				urlPaperless = resultadoPpl.getMensaje();
				if (!notaVenta.actualizarNotaVentaCnUrlPPL(trama.getNotaVenta().getCorrelativoVenta(), urlPaperless, "URL_DOCE")){
					logger.traceInfo("enviarEmailBoleta", "Error al actualizar url PPL ("+urlPaperless+") en NOTA_VENTA campo URL_DOCE");
					return false;
				}
			} else {
				logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL MAIL, mensaje NULO");
				return false;
			}
		}

		if (template.getLlave() == null){
			logger.traceInfo("process", "Correo '"+asunto+"' no se encontr� o est� desactivado.");
			return false;
		}
		
       
        
        if(swith_all_correo.equalsIgnoreCase("ON")){
	       
        	if (swith_correo_sgo_normal.equalsIgnoreCase("ON") && swith_correo_por_mpos.equalsIgnoreCase("ON") ) {
        		return resultado;                
	        }
        	
        	
        	if (swith_correo_sgo_normal.equalsIgnoreCase("ON") && swith_correo_por_mpos.equalsIgnoreCase("OFF") ) {
        		return resultado;                
	        }
        	
        	if (swith_correo_sgo_normal.equalsIgnoreCase("OFF") && swith_correo_por_mpos.equalsIgnoreCase("ON") ) {
        		return resultado;                
	        }
        	     	
        	
        	if(swith_correo_sgo_transac.equalsIgnoreCase("OFF")){
        	
				String destinatario = trama.getDespacho().getEmailCliente();
				String urlCloudFront = master.getParametros().buscaValorPorNombre("URL_CLOUDFRONT");
				
				String rutCliente = trama.getNotaVenta().getRutComprador().toString() + trama.getNotaVenta().getDvComprador();
		
				String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_YYYYMMDD, master.getParametros());
				
				try {
					String html = template.getHtml();
					HashMap<String, String> valores = new HashMap<String, String>();
					valores.put("Url_cloudfront", urlCloudFront);
					valores.put("Url_paperless", urlPaperless);
					valores.put("Fecha_emision", fechaEmision);
					valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
					valores.put("Nro_folio", nroFolio);
					valores.put("Rut_compra", rutCliente);
		
					int unidades = 0;
					int precio = 0;
					int valor_linea_detalle = 0;
					int subtotal = 0;
					int total = 0;
		
					List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
					if (articuloVentas.size() > 0){
						for (int i = 0;i < articuloVentas.size(); i++){
							ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
							if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
								if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es ripley me lo salto
									continue;
								}
							}
		
							unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
							precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
							valor_linea_detalle = precio * unidades;
							subtotal =  subtotal + (valor_linea_detalle);
							total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
		
						}
					}
					valores.put("Total_compra", String.valueOf(total));
					
					ClienteSMTP clienteSmtp = new ClienteSMTP();
					html = clienteSmtp.completarHTML(html, valores, null);
					resultado = clienteSmtp.enviarCorreo(master.getParametros().buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(master.getParametros().buscaValorPorNombre("PORT")), asunto, html, master.getParametros().buscaValorPorNombre("REMITENTE"), destinatario, master.getParametros().buscaValorPorNombre("SILVERPOP_GROUP"));
					
				} catch (Exception e) {
					resultado = false;
				}
        	}
        }else{
    		logger.traceInfo("process", "Todo tipo de envio de correo desactivado.");
    		return false;
        }
		return resultado;
	}
	
	
	
	
//	public boolean enviarEmailBoletaTransac(NotaVentaMail trama) throws AligareException {
//		boolean resultado = false;
//		
//		NotaVentaDAO notaVenta = new NotaVentaDAOImpl();
//		
//
//		String urlPaperless = "";
//		String flagMail="";
//		
////		 String swith_all_correo = master.getParametros().buscaValorPorNombre("SWITCH_ALL_CORREO_SGO");
////	     String swith_correo_sgo_normal = master.getParametros().buscaValorPorNombre("SWITCH_CORREO_SGO");
//	     String swith_correo_sgo_transac = master.getParametros().buscaValorPorNombre("SWITCH_CORREO_SGO_TRANSAC");
//		
//	 	String nroFolio = trama.getNotaVenta().getFolioSii().toString();
//		
//	
//		
//     	if(swith_correo_sgo_transac.equalsIgnoreCase("ON")){
//     		
//     		flagMail = trama.getNotaVenta().getUrlDoce();
//     		
//     		logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac...","Valor URL_DOCE Actualizada - " + flagMail);
//     		
//     		
//     		if(flagMail == null || flagMail.length() == 0){
//     		
//     		logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac...","Generando XML - Silverpop");
//        	String xmlMail = GeneracionXml.GeneracionXmlTransacc(trama	, master.getParametros(), nroFolio, master.getParametros().buscaValorPorNombre("NRO_CAJA_BOLETAS") );
//            
//    		SGOTransactXML enviarCorreoPorSilverpop = new SGOTransactXML();
//    		urlPaperless="#";
//			if(!enviarCorreoPorSilverpop.enviarCorreo(xmlMail, master.getParametros())){
//				logger.initTrace("EsclavoMailBoleta","Error Silverpop");
//				logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac","Error Silverpop, Correo no enviado...");
//				urlPaperless="@";
//			}else{
//				logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac","OK mail via XML - Silverpop");
//				if (notaVenta.actualizarNotaVentaCnUrlPPL(trama.getNotaVenta().getCorrelativoVenta(), urlPaperless, "URL_DOCE")){
//					logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac...","URL_DOCE Actualizada - Silverpop");
//				}
//			}
//			logger.initTrace("EsclavoMailBoleta - enviarEmailBoletaTransac...","OK mail via XML - Silverpop");
//		
//    	}
//     	}
//		
//		
//		return resultado;
//	}
	
	/**
	 * Envia boleta a través de servicio SilverPop XML
	 * @param correlativoVenta
	 * @return RetornoBaseDTO
	 * @throws AligareException
	 * 
	 */
	public GenericResponse enviarEmailBoletaSilverPopXML(Long correlativoVenta) throws AligareException {
		//TODO para que funcione para Peru debe cambiarse el formato de xml.
		logger.initTrace("enviarEmailBoletaSilverPopXML", "CorrelativoVenta", new KeyLog("OC",correlativoVenta+"") );
		GenericResponse retornoBaseDTO = null;
		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());
		
		try {
			paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");	
			
			NotaVentaDAO notaVenta = new NotaVentaDAOImpl();
			NotaVentaMail trama = notaVenta.obtenerOrdenesParaEnvioMailTransac(correlativoVenta);
	
			String urlPaperless = "";
	
			String nroFolio = trama.getNotaVenta().getFolioSii().toString()!=Constantes.VACIO?trama.getNotaVenta().getFolioSii().toString():null;
	
			if(nroFolio!=null){
				//Inicio URLDoce
				ResultadoPPL resultadoPpl = paperless.recuperacionOnline(parametros.buscaValorPorNombre("PPL_RUT"), 
						parametros.buscaValorPorNombre("PPL_LOGIN"), 
						parametros.buscaValorPorNombre("PPL_PASS"), 
						trama.getTipoDoc().getCodPpl().toString(), 
						trama.getNotaVenta().getFolioSii(), Constantes.NRO_DOS, 
						Integer.parseInt(parametros.buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));

				logger.traceInfo("enviarEmailBoletaSilverPopXML(updateUrlDoce)", "resultadoPpl", new KeyLog("Url Ppl", resultadoPpl.getMensaje()));
				/*
				if (resultadoPpl!=null && resultadoPpl.getCodigo() == Constantes.EJEC_SIN_ERRORES) {
					boolean actualizaUrlDoce = notaVentaDAO.actualizarNotaVentaCnUrlPPL(correlativoVenta, resultadoPpl.getMensaje(), Constantes.COLUMNA_NV_URL_BOLETA);
					if(actualizaUrlDoce)
						logger.traceInfo("enviarEmailBoletaSilverPopXML(updateUrlDoce)", "se actualiza Url Doce para la OC "+correlativoVenta);
					else
						logger.traceInfo("enviarEmailBoletaSilverPopXML(updateUrlDoce)", "No se puede actualizar la Url Doce para la OC "+correlativoVenta );
				}else 
					logger.traceInfo("enviarEmailBoletaSilverPopXML(updateUrlDoce)", "Falla al obtener la Url del servicio PPL para la OC "+correlativoVenta );
				trama.getNotaVenta().setUrlDoce(resultadoPpl.getMensaje());
				*/
				if (resultadoPpl!=null && resultadoPpl.getCodigo() == Constantes.EJEC_SIN_ERRORES)
					trama.getNotaVenta().setUrlDoce(resultadoPpl.getMensaje());
				//Fin URLDoce
				
				String xmlMail = GeneracionXml.GeneracionXmlTransacc(trama, parametros, nroFolio, trama.getNotaVenta().getNumeroCaja().toString());
				logger.traceInfo("xmlMail", "xmlMail", new KeyLog("xmlMail", xmlMail));
				
				SGOTransactXML enviarCorreoPorSilverpop = new SGOTransactXML();
				urlPaperless=Constantes.URL_CORREO_NO_ENVIADO;
				retornoBaseDTO = new GenericResponse();
				retornoBaseDTO = enviarCorreoPorSilverpop.enviarCorreo(xmlMail, parametros);
			
				//Se consulta respuesta para marcar correos enviados
				if(retornoBaseDTO.getCodigo().intValue() == Constantes.EJEC_SIN_ERRORES){
					urlPaperless=Constantes.URL_CORREO_ENVIADO;
				}
			
				notaVenta.actualizarNotaVentaCnUrlPPL(trama.getNotaVenta().getCorrelativoVenta(), urlPaperless, "URL_DOCE");
			}else {
				retornoBaseDTO = new GenericResponse();
				retornoBaseDTO.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoBaseDTO.setMensaje("No existe numero de folio para la oc "+correlativoVenta);			
			}
		} catch(Exception e) {
			logger.traceError("obtenerOCsMailUpdateUrlDoce", "Error: ", e);
			retornoBaseDTO = new GenericResponse();
			retornoBaseDTO.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoBaseDTO.setMensaje("No existe numero de folio para la oc "+correlativoVenta);		
		} 
		logger.endTrace("enviarEmailBoletaSilverPopXML", "Finalizado", retornoBaseDTO+"" );
		return retornoBaseDTO;
	}
	
	/**
	 * Envia boleta a través de servicio SilverPop SMTP
	 * @param correlativoVenta
	 * @return RetornoBaseDTO
	 * @throws AligareException
	 * 
	 */
	public GenericResponse enviarEmailBoletaSilverPopSMTP(Long correlativoVenta) throws AligareException {
		
		logger.initTrace("enviarEmailBoletaSilverPopSMTP", "CorrelativoVenta", new KeyLog("OC",correlativoVenta+"") );

		GenericResponse retornoBaseDTO = null;
		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());
		
		TemplateVoucher template = new TemplateVoucher();
		String asunto = "", urlPaperless = "";
		
		paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");	
		NotaVentaMail trama = notaVentaDAO.obtenerOrdenesParaEnvioMailTransac(correlativoVenta);

			logger.traceInfo("enviarEmailBoletaSilverPopSMTP", "enviarEmailBoletaSilverPopSMTP Tienda Virtual");
			urlPaperless = parametros.buscaParametroPorNombre("URL_DOC_ELECTRONICO").getValor();

			String codeOc = String.valueOf(trama.getNotaVenta().getCorrelativoVenta());
			urlPaperless+= codeOc;
			if(trama.getNotaVenta().getFolioSii()!=null){
				String nroFolio = trama.getNotaVenta().getFolioSii().toString()!=Constantes.VACIO?trama.getNotaVenta().getFolioSii().toString():null;

				retornoBaseDTO = new GenericResponse();
				
				if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TRES){//3 - Cod PPl Peru Boleta Impresa
					asunto = "Boleta Electrónica Ripley.com";
					template = templateDao.getTemplateByLlave("BOLETA_PERU");
				} else {
					asunto = "Factura Electrónica Ripley.com";
					template = templateDao.getTemplateByLlave("FACTURA");
				}
				
				if (template.getLlave() == null){
					logger.traceInfo("enviarEmailBoletaSilverPopSMTP", "Correo '"+asunto+"' no se encontro o esta desactivado.");
				}
				
				asunto = asunto + " OC: "+trama.getNotaVenta().getCorrelativoVenta();
				
				String destinatario = trama.getDespacho().getEmailCliente()!=null?trama.getDespacho().getEmailCliente().replaceAll("\\s",""):"";
				String urlCloudFront = parametros.buscaValorPorNombre("URL_CLOUDFRONT");
				String rutCliente = String.valueOf(trama.getNotaVenta().getRutComprador());
				String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_YYYYMMDD, parametros);
				
				String html = template.getHtml();
				HashMap<String, String> valores = new HashMap<String, String>();
				valores.put("Url_cloudfront", urlCloudFront);
				valores.put("Url_paperless", urlPaperless);//TODO construir URL SGO
				valores.put("Fecha_emision", fechaEmision);
				valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
				valores.put("Nro_folio", nroFolio);
				valores.put("Rut_compra", rutCliente);
				//TODO se debe revisar el cálculo.
				BigDecimal unidades = BigDecimal.ZERO, precio = BigDecimal.ZERO, valorLineaDetalle = BigDecimal.ZERO, subtotal = BigDecimal.ZERO,total = BigDecimal.ZERO;

				List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
				if (articuloVentas.size() > 0){
					for (int i = 0;i < articuloVentas.size(); i++){
						ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
						
						//TODO Agregar si existen productos MKP (MERCADO RIPLEY)
						if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
							if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es MKP se salta
								continue;
							}
						}

						unidades = BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades());
						precio = articuloVentaTrama.getArticuloVenta().getPrecio();
						valorLineaDetalle = precio.multiply(unidades);
						subtotal =  subtotal.add(valorLineaDetalle);
						total =  total.add(valorLineaDetalle).subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento());

					}
				}
				
				valores.put("Total_compra", String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, total));
				
				html = clienteSMTP.completarHTML(html, valores, null);
				urlPaperless=Constantes.URL_CORREO_NO_ENVIADO;
				if (clienteSMTP.enviarCorreo(parametros.buscaValorPorNombre("SILVERPOP_HOST"), 
						Integer.parseInt(parametros.buscaValorPorNombre("PORT")), 
						asunto, html, parametros.buscaValorPorNombre("REMITENTE"), 
						destinatario, parametros.buscaValorPorNombre("SILVERPOP_GROUP"))) {

					urlPaperless=Constantes.URL_CORREO_ENVIADO;

				}else {
					retornoBaseDTO.setCodigo(Constantes.EJEC_CON_ERRORES);
					retornoBaseDTO.setMensaje("Error al enviar correo para la oc "+correlativoVenta);	
				}
				//Se actualiza la URL_DOCE con una marca
				notaVentaDAO.actualizarNotaVentaCnUrlPPL(trama.getNotaVenta().getCorrelativoVenta(), urlPaperless, "URL_DOCE");

			}else {
				retornoBaseDTO = new GenericResponse();
				retornoBaseDTO.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoBaseDTO.setMensaje("No existe numero de folio para la oc "+correlativoVenta);			
			}

			logger.endTrace("enviarEmailBoletaSilverPopSMTP", "Finalizado", retornoBaseDTO+"" );
		return retornoBaseDTO;
	}
	
	/**
	 * Metodo que actualiza la URL con la boleta del SII 
	 * @param correlativoVenta
	 * @return GenericResponse
	 * @throws AligareException
	 * @throws NullPointerException
	 * @author Boris Parra Luman [Aligare]
	 */
	public GenericResponse updateUrlDoce (Long correlativoVenta) throws AligareException, NullPointerException {
		
		logger.initTrace("updateUrlDoce", "correlativoVenta", new KeyLog("correlativoVenta", correlativoVenta+""));	
		
		GenericResponse resultado = null;
		ResultadoPPL resultadoPpl = null;
		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());		
		paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");
		
		try {
					
			NotaVentaMail trama = notaVentaDAO.obtenerOrdenesParaEnvioMailTransac(correlativoVenta);
			
			//trama.getNotaVenta().setFolioSii("435290");//TODO borrar, era solo para probar servicio ppl
			
			resultadoPpl = paperless.recuperacionOnline(parametros.buscaValorPorNombre("PPL_RUT"), 
														parametros.buscaValorPorNombre("PPL_LOGIN"), 
														parametros.buscaValorPorNombre("PPL_PASS"), 
														trama.getTipoDoc().getCodPpl().toString(), 
														trama.getNotaVenta().getFolioSii(), Constantes.NRO_DOS, 
														Integer.parseInt(parametros.buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));
			
			logger.traceInfo("updateUrlDoce", "resultadoPpl", new KeyLog("Url Ppl", resultadoPpl.getMensaje()));
			
			if (resultadoPpl!=null && resultadoPpl.getCodigo() == Constantes.EJEC_SIN_ERRORES) {
				if (notaVentaDAO.actualizarNotaVentaCnUrlPPL(correlativoVenta, 
															 resultadoPpl.getMensaje(), 
															 Constantes.COLUMNA_NV_URL_BOLETA)) {
					resultado = new GenericResponse();
					resultado.setData(resultadoPpl.getMensaje());
				}
				else {
					resultado = new GenericResponse();
					resultado.setCodigo(Constantes.EJEC_CON_ERRORES);
					resultado.setMensaje("No se puede actualizar la Url Doce para la OC "+correlativoVenta);
					logger.traceInfo("updateUrlDoce", "No se puede actualizar la Url Doce para la OC "+correlativoVenta );
				}
				
			}
			else {
				
				resultado = new GenericResponse();
				resultado.setCodigo(Constantes.EJEC_CON_ERRORES);
				resultado.setMensaje("Falla al obtener la Url del servicio PPL para la OC "+correlativoVenta);
				logger.traceInfo("updateUrlDoce", "Falla al obtener la Url del servicio PPL para la OC "+correlativoVenta);

			}
		} 
		
		catch (Exception e) {
			logger.traceError("updateUrlDoce", "Error Catch ", e);
			resultado = new GenericResponse();
			resultado.setCodigo(Constantes.EJEC_CON_ERRORES);
			resultado.setMensaje("Error en el envio de correo para la OC  " + correlativoVenta + " " +e.getMessage());
		}
		
		logger.endTrace("updateUrlDoce", "Finalizacion", resultado.getMensaje());
		return resultado;
	}
	
	public GenericResponse obtenerOCsMail(Integer numCaja, Integer cantidadOCs) {
		logger.initTrace("obtenerOCsMail", "Integer caja: " + String.valueOf(numCaja) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs), 
				new KeyLog("Caja", String.valueOf(numCaja)), new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));
		ConsultaOcMailResponse resp = new ConsultaOcMailResponse();
		List<Long> listaOCs = new ArrayList<Long>();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje("OK");
		try {
			if(numCaja<Constantes.NRO_CERO) {
				List<SucursalRpos> list = new ArrayList<SucursalRpos>();
				List<List<Long>> listaOCsRpos = new ArrayList<List<Long>>();
				list = operacionesCaja.cajaSucUrlDoce(numCaja*Constantes.NRO_MENOSUNO);
				
				for(SucursalRpos cajaSuc: list) {
					List<Long> listaOCTemp = new ArrayList<Long>();
					try {
						logger.traceInfo("obtenerOCsMail", "obtenerOCsMail Rpos Caja= "+cajaSuc.getCaja()+", Sucursal= "+cajaSuc.getSucursal());
						listaOCTemp=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, cajaSuc.getCaja(), cajaSuc.getSucursal(), cantidadOCs, Constantes.NRO_UNO);
						listaOCsRpos.add(listaOCTemp);
					} catch (Exception e) {
						logger.traceError("obtenerOCsMail Rpos, caja: "+cajaSuc.getCaja()+", sucursal: "+cajaSuc.getSucursal(), e);
					}
				}
				for(List<Long> lt:  listaOCsRpos) {
					listaOCs.addAll(lt);
				}
			}else{
				listaOCs=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, numCaja, cantidadOCs, Constantes.NRO_UNO);
			}
		}catch(Exception e) {
			logger.traceError("obtenerOCsMail", "Error: ", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("NOK");
			return resp;
		}
		
		resp.setNumerosOCs(listaOCs);
		logger.endTrace("obtenerOCsMail", "Finalizado OK", String.valueOf(resp));
		return resp;
	}
	
	public GenericResponse obtenerOCsMailUpdateUrlDoce(Integer numCaja, Integer cantidadOCs) {
		logger.initTrace("obtenerOCsMailUpdateUrlDoce", "Integer caja: " + String.valueOf(numCaja) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs), 
				new KeyLog("Caja", String.valueOf(numCaja)), new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));
		ConsultaOcMailResponse resp = new ConsultaOcMailResponse();
		List<Long> listaOCs = new ArrayList<Long>();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje("OK");
		try {
			if(numCaja<Constantes.NRO_CERO) {
				List<SucursalRpos> list = new ArrayList<SucursalRpos>();
				List<List<Long>> listaOCsRpos = new ArrayList<List<Long>>();
				list = operacionesCaja.cajaSucUrlDoce(numCaja*Constantes.NRO_MENOSUNO);
				
				for(SucursalRpos cajaSuc: list) {
					List<Long> listaOCTemp = new ArrayList<Long>();
					try {
						logger.traceInfo("obtenerOCsMailUpdateUrlDoce", "obtenerOCsMailUpdateUrlDoce Rpos Caja= "+cajaSuc.getCaja()+", Sucursal= "+cajaSuc.getSucursal());
						listaOCTemp=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, cajaSuc.getCaja(), cajaSuc.getSucursal(), cantidadOCs, Constantes.NRO_DOS);
						listaOCsRpos.add(listaOCTemp);
					} catch (Exception e) {
						logger.traceError("obtenerOCsMailUpdateUrlDoce Rpos, caja: "+cajaSuc.getCaja()+", sucursal: "+cajaSuc.getSucursal(), e);
					}
				}
				for(List<Long> lt:  listaOCsRpos) {
					listaOCs.addAll(lt);
				}
			}else{
				listaOCs=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, numCaja, cantidadOCs, Constantes.NRO_DOS);
			}
		}catch(Exception e) {
			logger.traceError("obtenerOCsMailUpdateUrlDoce", "Error: ", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("NOK");
			return resp;
		}
		
		resp.setNumerosOCs(listaOCs);
		logger.endTrace("obtenerOCsMailUpdateUrlDoce", "Finalizado OK", String.valueOf(resp));
		return resp;
	}
	
	/**
	 * Metodo que actualiza la URL_DOCE para las OC de RPOS y Tienda Virtual
	 * @param jobUrlDoce
	 * @param cantidadOCs
	 * @return GenericResponse
	 * @throws AligareException
	 * @throws NullPointerException
	 * @author Pablo Salazar Osorio [Aligare]
	 */
	public GenericResponse updateUrlDocePPL(Integer jobUrlDoce, Integer cantidadOCs) {
		logger.initTrace("updateUrlDoce", "Integer jobUrlDoce: " + String.valueOf(jobUrlDoce) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs), 
				new KeyLog("jobUrlDoce", String.valueOf(jobUrlDoce)), new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));
		GenericResponse resp = new GenericResponse();
		List<Long> listaOCs = new ArrayList<Long>();
		try {
			Parametros parametros = new Parametros();
			parametros.setParametros(new ArrayList<Parametro>());		
			paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");
			
			if(jobUrlDoce<Constantes.NRO_CERO) {
				//RPOS
				List<SucursalRpos> list = new ArrayList<SucursalRpos>();
				List<List<Long>> listaOCsRpos = new ArrayList<List<Long>>();
				list = operacionesCaja.cajaSucUrlDoce(jobUrlDoce*Constantes.NRO_MENOSUNO);
				
				for(SucursalRpos cajaSuc: list) {
					List<Long> listaOCTemp = new ArrayList<Long>();
					try {
						logger.traceInfo("updateUrlDoce", "obtenerOCsMail Rpos Caja= "+cajaSuc.getCaja()+", Sucursal= "+cajaSuc.getSucursal());
						listaOCTemp=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, cajaSuc.getCaja(), cajaSuc.getSucursal(), cantidadOCs, Constantes.NRO_UNO);
						listaOCsRpos.add(listaOCTemp);
					} catch (Exception e) {
						logger.traceError("updateUrlDoce Rpos, caja: "+cajaSuc.getCaja()+", sucursal: "+cajaSuc.getSucursal(), e);
					}
				}
				for(List<Long> lt:  listaOCsRpos)
					listaOCs.addAll(lt);
			}else{
				//Tienda Virtual
				listaOCs=ordenCompraDAO.getOrdenesByEstadoAndCaja(Constantes.NRO_DOS, jobUrlDoce, cantidadOCs, Constantes.NRO_UNO);
			}

			logger.traceInfo("updateUrlDoce", "List OC"+listaOCs);
			for(Long oc: listaOCs) {
				NotaVentaMail trama = notaVentaDAO.obtenerOrdenesParaEnvioMailTransac(oc);
				ResultadoPPL resultadoPpl = paperless.recuperacionOnline(parametros.buscaValorPorNombre("PPL_RUT"), 
						parametros.buscaValorPorNombre("PPL_LOGIN"), 
						parametros.buscaValorPorNombre("PPL_PASS"), 
						trama.getTipoDoc().getCodPpl().toString(), 
						trama.getNotaVenta().getFolioSii(), Constantes.NRO_DOS, 
						Integer.parseInt(parametros.buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));

				logger.traceInfo("updateUrlDoce", "resultadoPpl", new KeyLog("Url Ppl", resultadoPpl.getMensaje()));
				if (resultadoPpl!=null && resultadoPpl.getCodigo() == Constantes.EJEC_SIN_ERRORES) {
						boolean actualizaUrlDoce = notaVentaDAO.actualizarNotaVentaCnUrlPPL(oc, resultadoPpl.getMensaje(), Constantes.COLUMNA_NV_URL_BOLETA);
						if(actualizaUrlDoce)
							logger.traceInfo("updateUrlDoce", "se actualiza Url Doce para la OC "+oc);
						else
							logger.traceInfo("updateUrlDoce", "No se puede actualizar la Url Doce para la OC "+oc );
					}else 
						logger.traceInfo("updateUrlDoce", "Falla al obtener la Url del servicio PPL para la OC "+oc );
				}

		}catch(Exception e) {
			logger.traceError("updateUrlDoce", "Error: ", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("NOK");
			return resp;
		}
		logger.endTrace("updateUrlDoce", "Finalizado OK", String.valueOf(resp));
		return resp;
	}
	
	
	
}
