package cl.ripley.omnicanalidad.logic.mail;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;
import cl.ripley.omnicanalidad.bean.ResultadoPPL;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.PaperlessDAO;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.dao.impl.CajaVirtualDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.NotaVentaDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.PaperlessDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.VoucherTemplateDAOImpl;
import cl.ripley.omnicanalidad.util.ClienteSMTP;
import cl.ripley.omnicanalidad.util.Constantes;

public class EsclavoMailNC extends Thread implements Runnable {
	private static final AriLog logger = new AriLog(EsclavoMailNC.class,"RIP16-004", PlataformaType.JAVA);

    /**
     * Cola con los folios a procesar
     */
	ConcurrentLinkedQueue<NotaVentaMail> qOrdenCompra;
	
	/**
	 * Maestro de ejecuci�n
	 */
	MaestroMail master;
	
	/**
	 * Constructor define el nombre del esclavo con el numeral entregado como id.
	 * 
	 * @param id identificador conocido asignado por el maestro.
	 */
	public EsclavoMailNC(int id, MaestroMail pMaster){
		this(id, pMaster, pMaster.getqOrdenCompra());
	}
	
	public EsclavoMailNC(int id, MaestroMail pMaster, ConcurrentLinkedQueue<NotaVentaMail> pOrdenCompra){
		this.setName("SLAVE BOLETA "+id);
		master = pMaster;
		qOrdenCompra = pOrdenCompra;
	}
	
	@Override
	public void run() throws AligareException {

		this.process();
		super.run();

		master.getSemNC().release();
	}
	
	public void process() throws AligareException {
		logger.initTrace("process", null);

		NotaVentaMail inOrdenCompra = null;
		int a = 0;
		int z = qOrdenCompra.size();
		while ((inOrdenCompra = qOrdenCompra.poll()) != null){ //obtengo una OC de la cola
			logger.traceInfo("process", "MAIL PPL NC"+this.getName()+ " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " "+ (++a) + " de "+z);
			
			int minutosCompraMin = Integer.parseInt(master.getParametros().buscaValorPorNombre("TIME_PPL_EMAIL_MINUTOS"));
			
			if (minutosCompraMin > inOrdenCompra.getNotaVenta().getMinutosCompra().intValue()){
				continue;
			}
			
			if (inOrdenCompra.getNotaVenta().getFolioSii() == null || inOrdenCompra.getNotaVenta().getFolioNcSii() == null){
				continue;
			}
			
			if (!enviarEmailNotaCredito(inOrdenCompra)){
				logger.traceInfo("process", "Error en envio email nota credito", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO FOLIO PPL", inOrdenCompra.getNotaVenta().getFolioSii().toString()), new KeyLog("NRO FOLIO NC PPL", inOrdenCompra.getNotaVenta().getFolioNcSii().toString()));
			}

			logger.traceInfo("process", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
		}
		logger.endTrace("process", "Finalizado", null);
	}
	
	public boolean enviarEmailNotaCredito(NotaVentaMail trama) throws AligareException {
		boolean resultado = false;
		
		NotaVentaDAO notaVenta = new NotaVentaDAOImpl();
		
		PaperlessDAO paperless = new PaperlessDAOImpl(master.getParametros().buscaValorPorNombre("TARGETENDPOINT_PPL"));
		ResultadoPPL resultadoPpl = new ResultadoPPL();
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		TemplateVoucher template = new TemplateVoucher();
		VoucherTemplateDAO templateDao = new VoucherTemplateDAOImpl();
		String urlPaperless = "";
		String asunto = "Nota de cr�dito Ripley.com";
		template = templateDao.getTemplateByLlave("NOTA_CREDITO");
		
		if (template.getLlave() == null){
			logger.traceInfo("process", "Correo '"+asunto+"' no se encontr� o est� desactivado.");
			return false;
		}
		asunto = asunto + " OC: "+trama.getNotaVenta().getCorrelativoVenta();
		
		String tipoDoc = trama.getTipoDoc().getCodPpl().toString();
		
		String nroFolio = trama.getNotaVenta().getFolioNcSii().toString();
		String nroNC = trama.getNotaVenta().getNumNotaCredito().toString();
		
//		resultadoPpl = paperless.recuperacionOnline(Integer.parseInt(master.getParametros().buscaValorPorNombre("PPL_RUT")), master.getParametros().buscaValorPorNombre("PPL_LOGIN"), master.getParametros().buscaValorPorNombre("PPL_PASS"), tipoDoc, nroFolio, Constantes.NRO_DOS, Integer.parseInt(master.getParametros().buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));
		resultadoPpl = null;
		if (resultadoPpl != null && resultadoPpl.getCodigo() != Constantes.NRO_CERO){
			if (resultadoPpl.getCodigo() == Constantes.NRO_MENOSDOCE){
				logger.traceInfo("enviarEmailBoleta", "Folio no encontrado en PPL aun", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
				return true;
			}
			logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL MAIL", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
			return false;
		} else {
			if (resultadoPpl != null && resultadoPpl.getMensaje() != null){
				urlPaperless = resultadoPpl.getMensaje();
				
				//FIXME esto esta mal, pero Ariel insiste
				urlPaperless = urlPaperless + "&tmpl=83382700/61ch.jasper";
				
				if (!notaVenta.actualizarNotaVentaCnUrlPPL(trama.getNotaVenta().getCorrelativoVenta(), urlPaperless, "URL_NOTA_CREDITO")){
					logger.traceInfo("enviarEmailBoleta", "Error al actualizar url PPL ("+urlPaperless+") en NOTA_VENTA campo URL_NOTA_CREDITO");
					return false;
				}
			} else {
				logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL MAIL, resultadoPpl nulo");
				return false;
			}
		}

		if (template.getLlave() == null){
			logger.traceInfo("process", "Correo '"+asunto+"' no se encontro o est� desactivado.");
			return false;
		}
		
		String destinatario = trama.getDespacho().getEmailCliente();
		String urlCloudFront = master.getParametros().buscaValorPorNombre("URL_CLOUDFRONT");

		String rutCliente = trama.getNotaVenta().getRutComprador().toString() + trama.getNotaVenta().getDvComprador();

		String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_YYYYMMDD, master.getParametros());
		
		//FIXME esto esta mal, pero Ariel insiste
		//urlPaperless = urlPaperless + "&tmpl=83382700/61ch.jasper";
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Url_paperless", urlPaperless);
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Nro_folio", nroFolio);
			valores.put("Rut_compra", rutCliente);

			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;

			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si no es ripley me lo salto
							continue;
						}
					}

					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_DOS){ // si no es nota de credito, me lo salto
						continue;
					}

					if (!nroNC.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getNumNotaCredito().toString())){ //si el numero nota credito de la nota de venta es distinto al del articulo, me lo salto
						continue;
					}
					
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}

				}
			}
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, null);
			resultado = clienteSmtp.enviarCorreo(master.getParametros().buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(master.getParametros().buscaValorPorNombre("PORT")), asunto, html, master.getParametros().buscaValorPorNombre("REMITENTE"), destinatario, master.getParametros().buscaValorPorNombre("SILVERPOP_GROUP"));

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}
}
