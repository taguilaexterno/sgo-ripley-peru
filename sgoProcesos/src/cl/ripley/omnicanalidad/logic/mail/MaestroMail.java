package cl.ripley.omnicanalidad.logic.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.impl.NotaVentaDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

public class MaestroMail {
	private static final AriLog logger = new AriLog(MaestroMail.class, "RIP16-004", PlataformaType.JAVA);
	private Parametros parametros;
	
	private Semaphore semBol = null;
	private Semaphore semNC = null;
	private List<EsclavoMailBoleta> slavesBol = null;
	private List<EsclavoMailNC> slavesNC = null;
	
	private ConcurrentLinkedQueue<NotaVentaMail> qOrdenCompra = null;
	private ConcurrentLinkedQueue<NotaVentaMail> qOrdenCompraMail = null;
	
	public MaestroMail(Parametros pPars) throws AligareException {
		parametros = pPars;
		slavesBol = new ArrayList<EsclavoMailBoleta>();
		slavesNC = new ArrayList<EsclavoMailNC>();
		qOrdenCompra = new ConcurrentLinkedQueue<NotaVentaMail>();
		qOrdenCompraMail = new ConcurrentLinkedQueue<NotaVentaMail>();
	}
	
	/**
	 * Gatilla el proceso. Retorna una vez que se procesando todas las OCs
	 */
	public void process() throws AligareException {
		logger.initTrace("process", null);
		
		//inicializa DAOs
		Integer nroCajaBoleta	= new Integer(parametros.buscaParametroPorNombre("NRO_CAJA_BOLETAS").getValor());
		Integer nroCajaNC		= new Integer(parametros.buscaParametroPorNombre("NRO_CAJA_NC").getValor());
		
		//Integer nroCaja			= null;
		
		Integer nroSucusal		= new Integer(parametros.buscaParametroPorNombre("SUCURSAL").getValor());
		NotaVentaDAO notaVentaDAO = new NotaVentaDAOImpl();
		
		//llena la cola de OCs boleta
		logger.traceInfo("process", "Se procesaran boletas para envio de email");
		Integer nroEstado = new Integer(Constantes.NRO_DOS);
		//qOrdenCompra = notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCaja, nroSucusal, nroEstado);
		qOrdenCompra = notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCajaBoleta, nroSucusal, nroEstado);
		qOrdenCompraMail = notaVentaDAO.obtenerOrdenesParaEnvioMailTransac(nroCajaBoleta, nroSucusal, nroEstado);

		if (qOrdenCompra.size() == 0){
			logger.traceInfo("process", "SIN ordenes que procesar");
		} else {
			logger.initTrace("process", "Se procesaran "+qOrdenCompra.size()+ " boletas para envio de email");
			/*
			 * Se inicializan los Esclavos.
			 */
			initSlavesBoleta();
			/*
			 * Se Espera que los esclavos terminen. Cada uno manda un notify al master cuando termina, 
			 * por lo mismo se espera tantos wait como esclavos existan.
			 */
			try {
				semBol.acquire(slavesBol.size());
			} catch (InterruptedException e) {
				logger.traceError("process", "Error(InterruptedException) en process: ", e);
				logger.endTrace("process", "Finalizado", null);
				return;
			}
		}

		
		//llena la cola de OCs NC
		logger.traceInfo("process", "Se procesaran notas de credito para envio de email");
		nroEstado = new Integer(Constantes.NRO_TRES);
		//qOrdenCompra = notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCaja, nroSucusal, nroEstado);
		qOrdenCompra = notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCajaNC, nroSucusal, nroEstado);
		nroEstado = new Integer(Constantes.NRO_CUATRO);
		//qOrdenCompra.addAll(notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCaja, nroSucusal, nroEstado));
		qOrdenCompra.addAll(notaVentaDAO.obtenerOrdenesParaEnvioMail(nroCajaNC, nroSucusal, nroEstado));

		if (qOrdenCompra.size() == 0){
			logger.traceInfo("process", "SIN ordenes que procesar");
		} else {
			logger.initTrace("process", "Se procesaran "+qOrdenCompra.size()+ " notas de credito para envio de email");
			/*
			 * Se inicializan los Esclavos.
			 */
			initSlavesNC();
			/*
			 * Se Espera que los esclavos terminen. Cada uno manda un notify al master cuando termina, 
			 * por lo mismo se espera tantos wait como esclavos existan.
			 */
			try {
				semNC.acquire(slavesNC.size());
			} catch (InterruptedException e) {
				logger.traceError("process", "Error(InterruptedException) en process: ", e);
				logger.endTrace("process", "Finalizado", null);
				return;
			}
		}

		
		logger.endTrace("process", "Finalizado", null);
	}

	/**
	 * Levanta todos los threads esclavos
	 */
	private void initSlavesBoleta() throws AligareException {
		logger.initTrace("initSlaves", "Integer nroCaja, Integer nroSucusal, Integer nroEstado");

		int numThread = 1;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();
		semBol = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
			EsclavoMailBoleta slave = new EsclavoMailBoleta(idx, this);
			slavesBol.add(slave);
			try {
				semBol.acquire();
			} catch (Exception e) {
				logger.traceError("initSlaves", "Error(InterruptedException) en initSlaves(): ", e);
				logger.endTrace("initSlaves", "Finalizado", null);
				return;
			}
			
			slave.start();
		}
		logger.endTrace("initSlaves", "Finalizado", null);
	}

	/**
	 * Levanta todos los threads esclavos
	 */
	private void initSlavesNC() throws AligareException {
		logger.initTrace("initSlaves", "Integer nroCaja, Integer nroSucusal, Integer nroEstado");

		int numThread = 1;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();
		semNC = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
			EsclavoMailNC slave = new EsclavoMailNC(idx, this);
			slavesNC.add(slave);
			try {
				semNC.acquire();
			} catch (Exception e) {
				logger.traceError("initSlaves", "Error(InterruptedException) en initSlaves(): ", e);
				logger.endTrace("initSlaves", "Finalizado", null);
				return;
			}
			
			slave.start();
		}
		logger.endTrace("initSlaves", "Finalizado", null);
	}
	
	public Semaphore getSemBol() {
		return semBol;
	}

	public Semaphore getSemNC() {
		return semNC;
	}

	public Parametros getParametros() {
		return parametros;
	}
	
	public ConcurrentLinkedQueue<NotaVentaMail> getqOrdenCompra() {
		return qOrdenCompra;
	}

	public ConcurrentLinkedQueue<NotaVentaMail> getqOrdenCompraMail() {
		return qOrdenCompraMail;
	}
}
