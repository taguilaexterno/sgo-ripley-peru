package cl.ripley.omnicanalidad.logic.mail;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RespuestaSilverpop;
import cl.ripley.omnicanalidad.util.Constantes;

public class SGOTransactXML {
	private static final AriLog logger = new AriLog(SGOTransactXML.class,"RIP16-004", PlataformaType.JAVA);
	
	public GenericResponse enviarCorreo(String xmlRequest, Parametros pPars){
		
		logger.initTrace("enviarCorreo","enviarCorreo Correo por Silverpop"); 
		Parametros parametros = pPars ;
		String sendEncoding = "utf-8";
		HttpURLConnection urlConn = null;
		OutputStream out = null;
		InputStream in = null;
		GenericResponse respuestaDTO = new GenericResponse();
		String apiURL = parametros.buscaParametroPorNombre("TRANSACT_APIURL").getValor();//"http://transact3.silverpop.com/XTMail";
		
		
		logger.traceInfo("enviarCorreo","enviarCorreo URL Silverpop : " + apiURL);
				String xmlResponse = null;
		
		try{
			URL url = new URL(apiURL);
			urlConn = (HttpURLConnection)url.openConnection();
			urlConn.setRequestMethod("POST");
			urlConn.setDoOutput(true);
			urlConn.setRequestProperty("Content-Type", "text/xml;charset=" + sendEncoding);
			urlConn.connect();
			out = urlConn.getOutputStream();
			out.write(xmlRequest.getBytes(sendEncoding));
			out.flush();
			
			in = urlConn.getInputStream();
			
			InputStreamReader inReader = new InputStreamReader(in, sendEncoding);
			StringBuffer responseBuffer = new StringBuffer();
			char[] buffer = new char[1024];
			int bytes;
			while((bytes = inReader.read(buffer)) != -1){
				responseBuffer.append(buffer, 0, bytes);
			}
			xmlResponse = responseBuffer.toString();
			
			logger.traceInfo("enviarCorreo","enviarCorreo Respuesta Silverpop XX : " + xmlResponse);
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			logger.traceInfo("enviarCorreo","Error envio correo: Problemas con el envio de mail via Silverpop");
			respuestaDTO.setMensaje("Error envio correo: Problemas con el envio de mail via Silverpop");
			respuestaDTO.setCodigo(Constantes.EJEC_CON_ERRORES);
			return respuestaDTO;
		} finally{
			if(out != null){
				try{
					out.close();
				}catch(Exception e) {}
			}if(in != null){
				try{
					in.close();
				}catch(Exception e) {}
			}if(urlConn != null){
				urlConn.disconnect();
			}
		}
		
		RespuestaSilverpop respuestaSilverpop = getRespuestaError(xmlResponse);	
		respuestaDTO.setCodigo(respuestaSilverpop.getCodigoError()!=null?Integer.parseInt(respuestaSilverpop.getCodigoError()):Constantes.EJEC_CON_ERRORES);
		respuestaDTO.setMensaje(respuestaSilverpop.getDescripcionError());
		respuestaDTO.setData(xmlResponse);
		return respuestaDTO;
	}
	
	private RespuestaSilverpop getRespuestaError(String xmlResponse){
		
		RespuestaSilverpop respuesta = new RespuestaSilverpop();
		try{
			if (xmlResponse.indexOf("<RECIPIENT_DETAIL>")>0){
				xmlResponse = xmlResponse.split("<RECIPIENT_DETAIL>")[1].split("</RECIPIENT_DETAIL>")[0];
				respuesta.setCodigoError(xmlResponse.split("<ERROR_CODE>")[1].split("</ERROR_CODE>")[0]);
				respuesta.setDescripcionError(xmlResponse.split("<ERROR_STRING>")[1].split("</ERROR_STRING>")[0]);
			}else{
				respuesta.setCodigoError(xmlResponse.split("<ERROR_CODE>")[1].split("</ERROR_CODE>")[0]);
				respuesta.setDescripcionError(xmlResponse.split("<ERROR_STRING>")[1].split("</ERROR_STRING>")[0]);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			respuesta.setCodigoError(Constantes.EJEC_CON_ERRORES + "");
			respuesta.setDescripcionError("Error al analizar respuesta");
		}
		
		return respuesta;
	}

}
