package cl.ripley.omnicanalidad.logic.notaCredito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.text.StrBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ripley.dao.ICajaDAO;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.impresion.ConsultaOcImpresionResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.ClienteClub;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TemplateVoucher;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.AnulacionRecaudacionDAO;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.ClubDAO;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.dao.NotaCreditoDAO;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.VoucherTemplateDAO;
import cl.ripley.omnicanalidad.dao.impl.CajaVirtualDAOImpl;
import cl.ripley.omnicanalidad.dao.impl.ClubDAOImpl;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.ClienteSMTP;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

@Service
public class EsclavoNotaCredito extends Thread implements Runnable {
	private static final AriLog logger = new AriLog(EsclavoNotaCredito.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private ParametrosDAO paramDAO;
	
	@Autowired
	private CajaVirtualDAO caja;
	
	@Autowired
	private GeneracionDTEDAO generacionDTE;
	
	@Autowired
	private VoucherTemplateDAO templateDao;
	
	@Autowired
	private AnulacionRecaudacionDAO anulacionRecaudacion;
	
	@Autowired
	private NotaCreditoDAO notaCredito;
	
	@Autowired
	private RechazoBusiness rechazo;
	
	@Autowired
	private OperacionesCaja operacionesCaja;
	
	@Autowired
	private ICajaDAO cajaDAO;
	
	public EsclavoNotaCredito() {
		
	}
	
	public void process() throws AligareException {
//		logger.initTrace("process", null);
//		String  SWITCH_ONOFF_TBK_NC =  Cavira.SWITCH_ONOFF_TBK_NC;
//		TramaDTE inOrdenCompra = null;
//		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
//		AnulacionRecaudacionDAO anulacionRecaudacion = new AnulacionRecaudacionDAOImpl();
//		NotaCreditoDAO notaCredito = new NotaCreditoDAOImpl();
//		CajaVirtualDAO cajaVirtual = new CajaVirtualDAOImpl();
//		PaperlessDAO paperless = new PaperlessDAOImpl(master.getParametros().buscaValorPorNombre("TARGETENDPOINT"));
//		ResultadoPPL resultadoPpl = new ResultadoPPL();
//		Integer estadoActual = null;
//		Integer newEstado = new Integer(0);
//		RechazoBusiness rechazo = new RechazoBusiness();
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		String fecha = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, master.getParametros());
//		String fechaHora = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, master.getParametros());
//		int a = 0;
//		int z = qOrdenCompra.size();
//		while ((inOrdenCompra = qOrdenCompra.poll()) != null){
//			NotaCreditoBusiness businessNC = new NotaCreditoBusiness(master.getParametros(), inOrdenCompra);
//			logger.traceInfo("process", "EsclavoNC "+this.getName()+ " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " "+ (++a) + " de "+z);
//			try {
//				estadoActual = inOrdenCompra.getNotaVenta().getEstado();
//				
//				if (estadoActual.intValue() == Constantes.NRO_CERO){
//	
//					if (!notaCredito.actualizarEsNCArticuloVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_UNO))){
//						logger.traceInfo("process", "Error al actualizar articulos de venta a estado es_nc 1", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//						continue;
//					}
//	
//					if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() != null || inOrdenCompra.getNotaVenta().getFolioSii() != null){
//						if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TRECE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
//							logger.traceInfo("process", "Error al actualizar la orden a "+Constantes.NRO_TRECE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//						}
//						continue;
//					} else {
//						if (!rechazo.rechazoGeneral(inOrdenCompra, master.getParametros(), fechaHora)){
//							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//								logger.traceInfo("process", "Error al actualizar la orden a "+Constantes.NRO_TREINTANUEVE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//						} else {
//							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TRES), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//								logger.traceInfo("process", "Error al actualizar la orden a "+Constantes.NRO_TRES, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//							
//							//UpdateMkp
//							if(!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_TRES, master.getParametros(), Constantes.NRO_CERO, fecha)){
//								logger.traceInfo("process", "Error al actualizar la orden a 3 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//
//						}
//						continue;
//					}
//				} else {
//					if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
//						newEstado = new Integer(Constantes.NRO_TRES);
//					}
//					else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
//						newEstado = new Integer(Constantes.NRO_CUATRO);
//					}
//				
//					if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() == null  && inOrdenCompra.getNotaVenta().getCorrelativoBoleta() == null){  //.getFolioSii() == null){
//						if (!rechazo.rechazoGeneral(inOrdenCompra, master.getParametros(), fechaHora)){
//							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//								logger.traceInfo("process", "Error al actualizar la orden a "+Constantes.NRO_TREINTANUEVE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//						} else {
//							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//								logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//						}
//						continue;
//					} else {
//	
//						if (!getNumNC(inOrdenCompra, Constantes.NRO_MENOSUNO)){
//							newEstado = new Integer(Constantes.NRO_DOS);
//							logger.traceInfo("process", "Se deja orden "+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+" en estado "+newEstado+" debido a que no tiene marcado articulo en es_nc");
//							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//								logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//							
//							if (!generacionDTE.registraTransaccion(master.getParametros(),new Integer(0), new Integer(0), fechaHora, Constantes.ERROR, "NC NO GENERADA - OC sin ES_NC - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)){
//								logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//							
//							continue;
//						}
//						
//						int nroTransaccion = OperacionesCaja.cajaVirtual(master.getParametros(), true);
//							
//						String nroFolio = "654321";
//						
//						//generar voucher para INDICADOR_MKP = 1, 2 y 3  y si 2, tambien ES_MKP = 1
//						if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || 
//								inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS) ||
//								inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
//							logger.traceInfo("process", "RECAUDACION: "+(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
//							boolean tieneNC = true;
//							if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
//								tieneNC = getNumNC(inOrdenCompra, Constantes.ES_MKP_RIPLEY);
//							} else {
//								tieneNC = getNumNC(inOrdenCompra, Constantes.ES_MKP_MKP);
//							}
//							
//							if (tieneNC){
//								
//								if (!enviarEmailAnulacion(inOrdenCompra, String.valueOf(nroTransaccion))){
//									logger.traceInfo("process", "Error en envio email anulaci�n", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//								}
//								if (inOrdenCompra.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL){
//									newEstado = new Integer(Constantes.NRO_TRES);
//								}
//								else if (inOrdenCompra.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_PARCIAL){
//									newEstado = new Integer(Constantes.NRO_CUATRO);
//								}
//								else {
//									logger.traceInfo("process", "Error al actualizar la orden, por estado desconocido", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("ESTADO", String.valueOf(inOrdenCompra.getNotaVenta().getEstado())));
//									if (!generacionDTE.registraTransaccion(master.getParametros(),new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//										logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//									continue;
//								}
//								
//								
//								
//								//LLAMADA AL FLUJO DE TRANSBANK
//								//Primero se valida que el JAR de reproceso TBK est� funcionando, de lo contrario no se inserta en la tabla ---JPRM
//								if (SWITCH_ONOFF_TBK_NC.equalsIgnoreCase(Constantes.STRING_UNO)){
//									if(inOrdenCompra.getTarjetaBancaria().getVd() != null && inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//										businessNC.anularTBK(master.getParametros(), inOrdenCompra, getSubtotalNC(inOrdenCompra),nroTransaccion);
//									//SIGUE EL FLUJO NORMAL
//									}
//								}
//								
//								if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
//									if (!anulacionRecaudacion.AnulacionCDF(inOrdenCompra, master.getParametros(), fecha, fechaHora)){
//										logger.traceInfo("process", "Error en satelites CDF", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//											logger.traceInfo("process", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										if (!generacionDTE.registraTransaccion(master.getParametros(),new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										continue;
//									}
//								} else {
//									if (!anulacionRecaudacion.AnulacionMKP(inOrdenCompra, master.getParametros(), fecha, fechaHora)){
//										logger.traceInfo("process", "Error en satelites MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//											logger.traceInfo("process", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										continue;
//									}
//								}
//								
//								if (!inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //AMBAS SE DEJA PARA MAS ADELANTE
//									
//									if (getNumNCvsArticulos(inOrdenCompra)){
//										newEstado = new Integer(Constantes.NRO_TRES);
//									}
//
//									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//										logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//									if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP)){
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ANRM, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//									} else {
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ANRR, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//									}
//
//									if (!anulacionRecaudacion.updateIdentificadorMkpReca(inOrdenCompra, master.getParametros(), nroTransaccion, fecha)){
//										logger.traceInfo("process", "Error al registrar Transaccion MKPReca", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//			
//								} else {
//									if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroTransaccion), fechaHora, Constantes.ANRM, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
//										logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//
//									if (!anulacionRecaudacion.updateIdentificadorMkpReca(inOrdenCompra, master.getParametros(), nroTransaccion, fecha)){
//										logger.traceInfo("process", "Error al registrar Transaccion MKPReca", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//								}
//							}
//			
//						}
//						
//						//generar trama para ir a PPL (ESTA TRAMA ES SOLO PARA INDICADOR_MKP = 0 y 2) y ES_MKP = 0
//						StrBuilder trama = new StrBuilder();
//						if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_RIPLEY) || 
//								inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
//							
//							logger.traceInfo("process", "BOLETA: "+(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
//							
//							if (getNumNC(inOrdenCompra, Constantes.ES_MKP_RIPLEY)){
//								if (inOrdenCompra.getNotaVenta().getFolioSii() == null){
//									logger.traceInfo("process", "Orden sin folio SII, se envia a impresion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
//										newEstado = new Integer(Constantes.NRO_TRECE);
//									}
//									else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
//										newEstado = new Integer(Constantes.NRO_CATORCE);
//									}
//									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
//										logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//		
//									continue;
//								}
//								
//								if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
//									nroTransaccion = cajaVirtual.resNroTransaccion(master.getParametros(), null);
//								}
//								
//								try{
//									trama.append(Constantes.TRAMA_INICIO_TRAMA);//.append(Constantes.TAB).append(Constantes.TRAMA_TRAINING).append(Constantes.TAB);
//									trama.append(generacionCabecera(inOrdenCompra));
//									trama.append(generacionDetalle(inOrdenCompra));
//									trama.append(generacionSubtotal(inOrdenCompra));
//									trama.append(generacionDescuentosRegalos(inOrdenCompra));
//									trama.append(generacionReferencia(inOrdenCompra));
//									trama.append(generacionPersonalizado(inOrdenCompra, nroTransaccion));
//									trama.append(generacionPersonalizadoEncabezado(inOrdenCompra, nroTransaccion));
//									trama.append(generacionPersonalizadoDetalle(inOrdenCompra));
//									trama.append(generacionPersonalizadoMedioPago(inOrdenCompra, nroTransaccion));
//									trama.append(generacionPersonalizadoLeyendaAntesTimbre(inOrdenCompra));
//									trama.append(generacionPersonalizadoLeyendaDespuesTimbre(inOrdenCompra));
//									trama.append(Constantes.TAB).append(Constantes.TRAMA_FIN_TRAMA);
//								} catch (AligareException e){
//									logger.traceInfo("process", "Error en generacion trama PPL", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//										logger.traceInfo("process", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//									if (!generacionDTE.registraTransaccion(master.getParametros(),new Integer(nroTransaccion), new Integer(0), fechaHora, Constantes.ERROR, "En generaqcion trama PPL NC - msj: "+e.getMessage(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)){
//										logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//									continue;
//								}
//								
//								logger.traceInfo("process", "TRAMA NOTA DE CREDITO: \n"+(trama.toString()));
//				
//								if (!checkAnulacionEnLinea(estadoActual, inOrdenCompra)){
////									resultadoPpl = paperless.generacionDte(Integer.parseInt(master.getParametros().buscaValorPorNombre("PPL_RUT")), master.getParametros().buscaValorPorNombre("PPL_LOGIN"), master.getParametros().buscaValorPorNombre("PPL_PASS"), trama.toString(), Constantes.PPL_TIPO_GENERACION, Constantes.PPL_TIPO_RETORNO, Integer.parseInt(master.getParametros().buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));
//									resultadoPpl = null;
//									if (resultadoPpl != null && resultadoPpl.getCodigo() == Constantes.NRO_CERO){
//										nroFolio = resultadoPpl.getMensaje();
//									} else {
//										
//										List<ResultadoPPL> listadoErrores = paperless.getListadoErroresPPL(resultadoPpl.getCodigo());
//										boolean errorPPLSaltar = false;
//										if (listadoErrores.size() > 0){
//											
//											for (int p = 0; p < listadoErrores.size(); p++){
//												ResultadoPPL errorPpl = listadoErrores.get(p);
//												if (resultadoPpl.getMensaje().equalsIgnoreCase(errorPpl.getMensaje())){
//													errorPPLSaltar = true;
//													break;
//												}
//											}
//											
//										} else {
//											errorPPLSaltar = false;
//										}
//
//										if (errorPPLSaltar){
//											if (Constantes.NRO_MENOSNOVENTAOCHO == resultadoPpl.getCodigo()){
//												logger.traceInfo("process", "Error en respuesta PPL NC, codigo: "+resultadoPpl.getCodigo());
//												if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//													logger.traceInfo("process", "Error al volver OC a estado Original, por error en PPL Cod "+resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//												}
//												continue;
//											}
//										} else{
//											logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL NC, mensaje NULO");
//											if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//												logger.traceInfo("process", "Error al volver OC a estado Original, por error en PPL Cod "+resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//											}
//											continue;
//										}
//										
//										
//										logger.traceInfo("process", "Error en respuesta PPL FOLIO", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
//										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//											logger.traceInfo("process", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(0), fechaHora, Constantes.ERROR, "En PPL FOLIO NC - msj: "+resultadoPpl.getMensaje(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										continue;
//									}
//								} else {
//									nroFolio = String.valueOf(nroTransaccion);
//								}
//								
//								//LLAMADA AL FLUJO DE TRANSBANK
//								//LLAMADA AL FLUJO DE TRANSBANK
//								//Primero se valida que el JAR de reproceso TBK est� funcionando, de lo contrario no se inserta en la tabla ---JPRM
//								if (SWITCH_ONOFF_TBK_NC.equalsIgnoreCase(Constantes.STRING_UNO)){
//										if(inOrdenCompra.getTarjetaBancaria().getVd() != null && inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//										//if(inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//											businessNC.anularTBK(master.getParametros(), inOrdenCompra, getSubtotalNC(inOrdenCompra),nroTransaccion);
//										//SIGUE EL FLUJO NORMAL
//										}
//								}
//								RetornoNotaCredito retornoNC = new RetornoNotaCredito();
//								retornoNC =	businessNC.generar(master.getParametros(), inOrdenCompra, new Integer(nroTransaccion), nroFolio, FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3));
//								
//								if (retornoNC.getCodigo().intValue() != 1){
//									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//										logger.traceInfo("process", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("COD RETORNO", String.valueOf(retornoNC.getCodigo().intValue())),new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("MSG ERROR", String.valueOf(retornoNC.getMensaje())));
//									}
//									if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroFolio), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+" "+retornoNC.getMensaje(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//										logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//								} else {
//									logger.traceInfo("process", "estadoActual: "+estadoActual);
//									if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
//										newEstado = new Integer(Constantes.NRO_TRES);
//										if (getNumNCvsArticulos(inOrdenCompra)){
//											newEstado = new Integer(Constantes.NRO_TRES);
//										}
//
//									}
//									else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
//										newEstado = new Integer(Constantes.NRO_CUATRO);
//										if (getNumNCvsArticulos(inOrdenCompra)){
//											newEstado = new Integer(Constantes.NRO_TRES);
//										}
//									}
//									else {
//										logger.traceInfo("process", "Error al actualizar la orden, por estado desconocido", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("ESTADO", String.valueOf(inOrdenCompra.getNotaVenta().getEstado())));
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroFolio), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										continue;
//									}
//									logger.traceInfo("process", "newEstado: "+newEstado);
//									
//									if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, nroFolio, Integer.parseInt(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")), Constantes.NRO_TRES)){
//										logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									} else {
//
//									}
//									//UpdateMkp
//									if (!businessNC.updateMkpNotaCredito(inOrdenCompra, master.getParametros(), newEstado.intValue(), nroTransaccion, fechaHora)){
//										logger.traceInfo("process", "Error al actualizar Identificador Marketplace en estado "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//									}
//									if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroFolio), fechaHora, Constantes.IMPRESION, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_CERO, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//									} else {
//										if (!generacionDTE.registraTransaccion(master.getParametros(), new Integer(nroTransaccion), new Integer(nroFolio), fechaHora, Constantes.IMPRESION, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("process", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//									}
//				
//								}
//							}else {
//								if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
//									newEstado = new Integer(Constantes.NRO_TRES);
//								}
//								else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
//									newEstado = new Integer(Constantes.NRO_CUATRO);
//								}
//								
//								if (getNumNCvsArticulos(inOrdenCompra)){
//									newEstado = new Integer(Constantes.NRO_TRES);
//								}
//
//								if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(master.getParametros().buscaValorPorNombre("NRO_CAJA_NC")))){
//									logger.traceInfo("process", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//								}
//							}
//							//Update esNC en Dos
//							if (!notaCredito.actualizarEsNCArticuloVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS))){
//								logger.traceInfo("process", "Error al actualizar articulos de venta a estado es_nc 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//							}
//							logger.traceInfo("process", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//						}
//					}
//				}
//			} catch (AligareException ae){
//				logger.traceInfo("process", "ERROR RARO");
//				logger.traceError("process", ae);
//			}
//		}
//		logger.endTrace("process", "Finalizado", null);
	}
	
//	private StrBuilder generacionCabecera(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		respuesta.append(Constantes.TRAMA_ENCABEZADO).append(Constantes.PIPE);
//		respuesta.append(trama.getTipoDoc().getCodPpl()).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_FOLIO).append(Constantes.PIPE);
////		respuesta.append(FechaUtil.getCurrentDateYYYYMMDDString()).append(Constantes.PIPE);
//		respuesta.append(caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, master.getParametros())).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_TIPO_DESPACHO).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_TIPO_TRASLADO).append(Constantes.PIPE);
//		if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TREINTANUEVE){
//			respuesta.append(Constantes.TRAMA_IND_MONTO_BRUTO).append(Constantes.PIPE);
//		} else {
//			respuesta.append(Constantes.TRAMA_IND_MONTO_BRUTO_F).append(Constantes.PIPE);
//		}
//		respuesta.append(Constantes.TRAMA_IND_SERVICIO).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("RUT_EMISOR")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("RAZON_SOCIAL_EMISOR")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("GIRO_EMISOR")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("ACTECO")).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_CODIGO_TRASLADO).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_FOLIO_AUT).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_FECHA_AUT).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("CODIGO_SUCURSAL")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("CDGSIISUCUR")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("DIRECCION_ORIGEN")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("COMUNA_ORIGEN")).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("CIUDAD_ORIGEN")).append(Constantes.PIPE);
//		
//		String dv_rut_cli = Util.getDigitoValidadorRut(trama.getNotaVenta().getRutComprador().toString());
//		String dv_rut_sol = Util.getDigitoValidadorRut(master.getParametros().buscaValorPorNombre("RUT_SOLICITA"));
//		
//		
//		if (trama.getTipoDoc().getCodBo().intValue() == Constantes.NRO_SEIS){
//		
//			respuesta.append(trama.getNotaVenta().getRutComprador()).append(Constantes.GUION).append(dv_rut_cli).append(Constantes.PIPE);
//			respuesta.append(trama.getDespacho().getNombreCliente()).append(Constantes.SPACE);
//			respuesta.append(trama.getDespacho().getApellidoPatCliente()).append(Constantes.SPACE);
//			respuesta.append(trama.getDespacho().getApellidoMatCliente()).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_GIRO_RECEPTOR).append(Constantes.PIPE);
//			respuesta.append(trama.getDespacho().getDireccionCliente()).append(Constantes.PIPE);
//			respuesta.append(trama.getDespacho().getComunaDespachoDes()).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_CIUDAD_RECEPTOR).append(Constantes.PIPE);
//			
//			respuesta.append(master.getParametros().buscaValorPorNombre("RUT_SOLICITA")).append(Constantes.GUION).append(dv_rut_sol).append(Constantes.PIPE);
//			
//			respuesta.append(Constantes.TRAMA_COMUNA_DESTINO).append(Constantes.PIPE);
//
//		} else {
//			
//			respuesta.append(trama.getDatoFacturacion().getRutFactura()).append(Constantes.PIPE);
//			respuesta.append(trama.getDatoFacturacion().getRazonSocialFactura()).append(Constantes.PIPE);
//			respuesta.append(Util.quitarSaltos(trama.getDatoFacturacion().getGiroFacturaDes())).append(Constantes.PIPE);
//			respuesta.append(Util.quitarSaltos(trama.getDatoFacturacion().getDireccionFacturaDes())).append(Constantes.PIPE);
//			respuesta.append(trama.getDatoFacturacion().getComunaFacturaDes()).append(Constantes.PIPE);
//			respuesta.append(trama.getDatoFacturacion().getCiudadFacturaDes()).append(Constantes.PIPE);
//			respuesta.append(trama.getNotaVenta().getRutComprador()).append(Constantes.GUION).append(dv_rut_cli).append(Constantes.PIPE);
//			respuesta.append(Constantes.PIPE);
//		}
//		
//		
//		
//		//montos
//		Integer monto_venta = new Integer(getSubtotalNC(trama));//trama.getNotaVenta().getMontoVenta();
//		BigDecimal iva =  new BigDecimal(master.getParametros().buscaValorPorNombre("VALORIVA"));
//		double tasa_iva = iva.doubleValue() / 100;
//
//		int monto_neto = (int)(monto_venta / (1 + tasa_iva));
//		int montoNetoInt = monto_neto;
//		int montoTotalIva = monto_venta.intValue() - montoNetoInt;
//		
//		respuesta.append(montoNetoInt).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_MONTO_EXENTO).append(Constantes.PIPE);
//		respuesta.append(master.getParametros().buscaValorPorNombre("VALORIVA")).append(Constantes.PIPE);
//		respuesta.append(montoTotalIva).append(Constantes.PIPE);
//		
//		respuesta.append(monto_venta);
//		
//		//Requerimiento nuevo valor tipo pago contable con switch en tabla parametros
//		String tramaTipoPagoSwitch = master.getParametros().buscaValorPorNombre("TIPO_PAGO_CONTADO_SWITCH");
//		logger.traceInfo("process", "tramaTipoPagoSwitch", new KeyLog("tramaTipoPagoSwitch", tramaTipoPagoSwitch));
//		if(tramaTipoPagoSwitch != null && tramaTipoPagoSwitch.equalsIgnoreCase(Constantes.STRING_UNO)) {
//			logger.traceInfo("process", "tramaTipoPagoSwitch", new KeyLog("tramaTipoPagoSwitch", "Concatena |1"));
//			respuesta.append(Constantes.PIPE).append(Constantes.NRO_UNO);
//		}
//		
//		respuesta.appendNewLine();
//		
//		return respuesta;
//	}
//	
//	private StrBuilder generacionDetalle(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
//		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//		int valor_linea_detalle = 0;
//		if (articuloVentas.size() > 0){
//			int secuencia = 1;
//			for (int i = 0;i < articuloVentas.size(); i++){
//				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//				if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
//					if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es mkp, me lo salto
//						continue;
//					}
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es nota de credito, me lo salto
//					continue;
//				}
//
//				logger.traceInfo("generacionDetalle", "productoTrama: "+articuloVentaTrama.getArticuloVenta().getCodArticulo());
//				respuesta.append(Constantes.TRAMA_DETALLE).append(Constantes.PIPE);
//				respuesta.append(secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_IND_EXCEPCION).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_TIPO_CODIGO).append(Constantes.PIPE);
//				respuesta.append(articuloVentaTrama.getArticuloVenta().getCodArticulo()).append(Constantes.PIPE);
//				if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() !=null && articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue() > 0){
//					respuesta.append(Constantes.TRAMA_DESC_DESCUENTO).append(Constantes.PIPE);
//				} else {
//					respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//				}
//				respuesta.append(Util.cambiaCaracteres(articuloVentaTrama.getArticuloVenta().getDescRipley())).append(Constantes.PIPE);
//				respuesta.append(articuloVentaTrama.getArticuloVenta().getUnidades()).append(Constantes.PIPE);
//				respuesta.append(articuloVentaTrama.getArticuloVenta().getPrecio()).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PORC_DESCUENTO).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_MONTO_DESCUENTO).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_COD_IMPUESTO).append(Constantes.PIPE);
//				
//				valor_linea_detalle = articuloVentaTrama.getArticuloVenta().getPrecio().intValue() * articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
//				respuesta.append(valor_linea_detalle).appendNewLine();
//
//				secuencia++;
//			}
//		}
//		
//		return respuesta;
//	}
//
//	private StrBuilder generacionSubtotal(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		
//		int acumulado = getSubtotalNC(trama);
//
//		respuesta.append(Constantes.TRAMA_SUBTOTALES).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_NRO_SUBTOTAL).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_SUBTOTAL).append(Constantes.PIPE);
//		respuesta.append(acumulado).appendNewLine();
//		return respuesta;
//	}
//	
//	private int getSubtotalNC(TramaDTE trama) throws AligareException{
//		int acumulado = 0;
//		
//		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
//		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//		int valor_linea_detalle = 0;
//
//		if (articuloVentas.size() > 0){
//			for (int i = 0;i < articuloVentas.size(); i++){
//				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//				if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
//					if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es mkp, me lo salto
//						continue;
//					}
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ //no es NC, me lo salto
//					continue;
//				}
//				
//				valor_linea_detalle = articuloVentaTrama.getArticuloVenta().getPrecio().intValue() * articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
//				if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
//					acumulado =  acumulado + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
//				} else {
//					acumulado =  acumulado + valor_linea_detalle;
//				}
//			}
//		}
//
//		return acumulado;
//	}
//
//	private StrBuilder generacionDescuentosRegalos(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
//		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//
//		if (articuloVentas.size() > 0){
//			int secuencia = 1;
//			for (int i = 0;i < articuloVentas.size(); i++){
//				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//				if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
//					if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es mkp, me lo salto
//						continue;
//					}
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es NC, me lo salto
//					continue;
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getMontoDescuento()!= null && articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue() > 0){
//					respuesta.append(Constantes.TRAMA_DESCUENTOS_RECARGOS).append(Constantes.PIPE);
//					respuesta.append(secuencia).append(Constantes.PIPE);
//					respuesta.append(Constantes.TRAMA_TIPO_MOVIMIENTO).append(Constantes.PIPE);
//					respuesta.append(Constantes.TRAMA_DESCUENTO_TOTAL).append(Constantes.PIPE);
//					respuesta.append(Constantes.TRAMA_DESCUENTO_UNIDAD).append(Constantes.PIPE);
//					respuesta.append(articuloVentaTrama.getArticuloVenta().getMontoDescuento()).append(Constantes.PIPE);
//					/*
//					if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_TREINTANUEVE){
//						respuesta.append(Constantes.NRO_UNO).appendNewLine();
//					} else {
//						respuesta.append(Constantes.TRAMA_DESCUENTO_EXENCION).appendNewLine();
//					}
//					*/
//					
//					respuesta.appendNewLine();
//					
//					secuencia++;
//				}
//			}
//		}
//		
//		return respuesta;
//	}
//	
//	private StrBuilder generacionReferencia(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		int secuencia = 0;
//		
//		respuesta.append(Constantes.TRAMA_REFERENCIA).append(Constantes.PIPE);
//		respuesta.append(++secuencia).append(Constantes.PIPE);
//		int codPpl = 0;
//		if (trama.getTipoDoc().getCodBo().intValue() != Constantes.NRO_SEIS){
//		//if (trama.getTipoDoc().getCodPpl().intValue() == Constantes.NRO_SESENTAUNO){
//			codPpl = Constantes.NRO_TREINTATRES;
//		} else codPpl = Constantes.NRO_TREINTANUEVE;
//		//respuesta.append(trama.getTipoDoc().getCodPpl()).append(Constantes.PIPE);
//		respuesta.append(codPpl).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_INDICADOR_GLOBAL).append(Constantes.PIPE);
//		
//		String folio_doc = "";
//		if (trama.getNotaVenta().getFolioSii() == null){
//			folio_doc = trama.getNotaVenta().getNumeroBoleta().toString();
//		} else {
//			folio_doc = trama.getNotaVenta().getFolioSii().toString();
//		}
//		respuesta.append(folio_doc).append(Constantes.PIPE);
//		
//		respuesta.append(FechaUtil.formatTimestamp(Constantes.FECHA_YYYYMMDD, trama.getNotaVenta().getFechaBoleta())).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_COD_USO_REF).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_RAZON_REFERENCIA).appendNewLine();
//		
//		return respuesta;
//	}
//	
//	private StrBuilder generacionPersonalizado(TramaDTE trama, int nroTransaccion) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		int acumulado = getSubtotalNC(trama);
//		String numero = "";
//
//		numero = String.valueOf(acumulado);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_DIRECCION_SUCURSAL).append(Constantes.PIPE);
////		respuesta.append(master.getParametros().buscaValorPorNombre("SUCURSAL")).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_TREINTANUEVE).append(Constantes.PIPE);		//Numero sucursal -- issue prod factura
////		respuesta.append(Constantes.PIPE);		//Numero Local
//		respuesta.append(master.getParametros().buscaValorPorNombre("NRO_CAJA")).append(Constantes.PIPE);
////		respuesta.append(FechaUtil.getCurrentHoursString()).append(Constantes.PIPE);
//		respuesta.append(caja.sysdateFromDual(Constantes.FECHA_HH24MISS, master.getParametros())).append(Constantes.PIPE);
//		
//		
//		if (Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta())){
//			respuesta.append(master.getParametros().buscaValorPorNombre("RUT_VENDEDOR")).append(Constantes.PIPE);
//		} else {
//			respuesta.append(trama.getNotaVenta().getEjecutivoVta()).append(Constantes.PIPE);
//		}
//		respuesta.append(master.getParametros().buscaValorPorNombre("PAGINA_WEB")).append(Constantes.PIPE);
//		respuesta.append(trama.getDespacho().getTelefonoCliente()).append(Constantes.PIPE);
//		respuesta.append(NumeroALetra.Convertir(numero, Constantes.VERDADERO).trim()).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_IMPRESORA).append(Constantes.PIPE);
//		
//		if (Validaciones.validarVacio(trama.getDespacho().getEmailCliente())){
//			respuesta.append(Constantes.VACIO).append(Constantes.PIPE);
//		} else {
//			respuesta.append(trama.getDespacho().getEmailCliente()).append(Constantes.PIPE);
//		}
//		respuesta.append(Constantes.TRAMA_CODIGO_BARRA).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_DOC_REFERENCIA_NC).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_CORRELATIVO_VENTA).append(Constantes.PIPE);
//		respuesta.append(Constantes.PIPE).append(Constantes.PIPE).appendNewLine();
//		
//		return respuesta;
//	}
//
//	private StrBuilder generacionPersonalizadoEncabezado(TramaDTE trama, int nroTransaccion) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_UNO).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_NRO_TRANSACCION).append(nroTransaccion).appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_DOS).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_ORDEN_COMPRA).append(trama.getNotaVenta().getCorrelativoVenta()).appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_TRES).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_VENDEDOR);
//
//		if (Validaciones.validarVacio((trama.getVendedor()!=null)?trama.getVendedor().getNombreVendedor():null)){
//			respuesta.append(Constantes.VACIO).appendNewLine();
//		} else {
//			respuesta.append(trama.getVendedor().getNombreVendedor()).appendNewLine();
//		}
//
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_CUATRO).append(Constantes.PIPE).appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_CINCO).append(Constantes.PIPE).appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_SEIS).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_VEN);
//		
//		if (Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta())){
//			respuesta.append(master.getParametros().buscaValorPorNombre("RUT_VENDEDOR"));
//		} else {
//			respuesta.append(trama.getNotaVenta().getEjecutivoVta());
//		}
////		respuesta.append(Constantes.TRAMA_FECHA).append(FechaUtil.getCurrentDateDDMMYYStringSlash()).appendNewLine();
//		respuesta.append(Constantes.TRAMA_FECHA).append(caja.sysdateFromDual(Constantes.FECHA_DD_MM_YY, master.getParametros())).appendNewLine();
//
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_SIETE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_SUPERVISOR).append(master.getParametros().buscaValorPorNombre("RUT_SUPERVISOR")).appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PERSONALIZADO_ENC).append(Constantes.PIPE);
//		respuesta.append(Constantes.NRO_OCHO).append(Constantes.PIPE).appendNewLine();
//
//		return respuesta;
//	}
//	
//	private StrBuilder generacionPersonalizadoDetalle(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
//		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//		int descuento = 0;
//		if (articuloVentas.size() > 0){
//			int secuencia1	= 1;
//			int secuencia2	= 1;
//			for (int i = 0;i < articuloVentas.size(); i++){
//				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//				if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
//					if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es mkp, me lo salto
//						continue;
//					}
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es nota de credito, me lo salto
//					continue;
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null && articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue() > 0){
//					if ((articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO) 
//						&& (articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO)){
//						
//						descuento = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue() * Constantes.NRO_MENOSUNO;
//						respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_PERSONALIZADO_DET).append(Constantes.PIPE);
//						respuesta.append(secuencia2).append(Constantes.PIPE);
//						respuesta.append(secuencia1).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_ZONA_DET_2).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_CODIGO_DET).append(Constantes.PIPE);
//						respuesta.append(descuento).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_DESC_DESCUENTO).appendNewLine();
//						secuencia2++;
//						respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_PERSONALIZADO_DET).append(Constantes.PIPE);
//						respuesta.append(secuencia2).append(Constantes.PIPE);
//						respuesta.append(secuencia1).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_ZONA_DET_1).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_CODIGO_DET).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_DESCUENTO_DET).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_BT).append(articuloVentaTrama.getArticuloVenta().getCodDespacho());
//						respuesta.append(" - [").append(articuloVentaTrama.getArticuloVenta().getDespachoId()).append("]").appendNewLine();
//						
//					} else {
//						if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
//							descuento = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue() * Constantes.NRO_MENOSUNO;
//	
//							respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//							respuesta.append(Constantes.TRAMA_PERSONALIZADO_DET).append(Constantes.PIPE);
//							respuesta.append(secuencia2).append(Constantes.PIPE);
//							respuesta.append(secuencia1).append(Constantes.PIPE);
//							respuesta.append(Constantes.TRAMA_ZONA_DET_1).append(Constantes.PIPE);
//							respuesta.append(Constantes.TRAMA_CODIGO_DET).append(Constantes.PIPE);
//							respuesta.append(Constantes.TRAMA_DESCUENTO_DET).append(Constantes.PIPE);
//							respuesta.append(Constantes.TRAMA_BT).append(articuloVentaTrama.getArticuloVenta().getCodDespacho());
//							respuesta.append(" - [").append(articuloVentaTrama.getArticuloVenta().getDespachoId()).append("]").appendNewLine();
//						}
//					}
//					secuencia1++;
//					secuencia2++;
//				} else {
//					//if ((articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO) 
//					//		&& (articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO)){
//						
//						respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_PERSONALIZADO_DET).append(Constantes.PIPE);
//						respuesta.append(secuencia2).append(Constantes.PIPE);
//						respuesta.append(secuencia1).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_ZONA_DET_2).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_CODIGO_DET).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_DESCUENTO_DET).append(Constantes.PIPE);
//						respuesta.append(Constantes.TRAMA_BT).append(articuloVentaTrama.getArticuloVenta().getCodDespacho());
//						respuesta.append(" - [").append(articuloVentaTrama.getArticuloVenta().getDespachoId()).append("]").appendNewLine();
//						secuencia2++;
//
//					//}
//				}
//			}
//		}
//		
//		return respuesta;
//	}
//
//	private StrBuilder generacionPersonalizadoMedioPago(TramaDTE trama, int nroTransaccion) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//
//		int secuencia	= 0;
//
//		String formaPagoNC = "";
//		String numCuenta = "";
//		
//		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){ // ES TARJETA RIPLEY
//			formaPagoNC = Constantes.TRAMA_TARJETA_RIPLEY;
//			numCuenta = getNroCuenta(trama);
//		} else if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){ // TARJETA BANCARIA
//			if (trama.getTarjetaBancaria().getVd() != null){
//				if (trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_S)){
//					formaPagoNC = Constantes.TRAMA_TARJETA_DEBITO_NC;
//				} else if (trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_M)){
//					formaPagoNC = Constantes.TRAMA_MERCADO_PAGO_NC;
//				} else {
//					formaPagoNC = Constantes.TRAMA_TARJETA_CREDITO_NC;
//				}
//			}
//		} else {
//			formaPagoNC = Constantes.TRAMA_TAR_REGALO_EMPRESA;
//		}
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PER_MEDIO_PAGO).append(Constantes.PIPE);
//		respuesta.append(++secuencia).append(Constantes.PIPE);
//		respuesta.append(formaPagoNC).appendNewLine();
//
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PER_MEDIO_PAGO).append(Constantes.PIPE);
//		respuesta.append(++secuencia).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_CTA_CTE_NRO);
//		
//		if (numCuenta.length() != Constantes.NRO_CERO){
//			respuesta.append(Constantes.TRAMA_ASTERISCOS_X6).append(numCuenta.substring(numCuenta.length()-4, numCuenta.length()));
//		}
//		respuesta.appendNewLine();
//		
//		respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_PER_MEDIO_PAGO).append(Constantes.PIPE);
//		respuesta.append(++secuencia).append(Constantes.PIPE);
//		respuesta.append(Constantes.TRAMA_NOMBRE_TITULAR);
//		respuesta.append(trama.getDespacho().getNombreCliente()).append(Constantes.SPACE);
//		respuesta.append(trama.getDespacho().getApellidoPatCliente()).append(Constantes.SPACE);
//		respuesta.append(trama.getDespacho().getApellidoMatCliente()).appendNewLine();
//		
//		return respuesta;
//	}
//	
//	private String getNroCuenta(TramaDTE trama) throws AligareException{
//		String respuesta = "";
//		
//		String pod = trama.getTarjetaRipley().getRutPoder().toString();
//		String prefijo = trama.getTarjetaRipley().getPrefijoTitular().toString();
//		String tit = trama.getTarjetaRipley().getRutTitular().toString();
//		String digito = trama.getTarjetaRipley().getDvTitular().toString();
//
//		pod = Util.rellenarString(pod, Constantes.NRO_SIETE, Constantes.VERDADERO, Constantes.CHAR_CERO);
//		prefijo = Util.rellenarString(prefijo, Constantes.NRO_DOS, Constantes.VERDADERO, Constantes.CHAR_CERO);
//		tit = Util.rellenarString(tit, Constantes.NRO_OCHO, Constantes.VERDADERO, Constantes.CHAR_CERO);
//		
//		if (digito.equalsIgnoreCase(Constantes.K)){
//			digito = Constantes.STRING_UNOX2;
//		} else if (digito.equalsIgnoreCase(Constantes.STRING_CERO)){
//			digito = Constantes.STRING_CEROX2;
//		} else {
//			digito = Constantes.STRING_CERO + digito;
//		}
//		
//		if (trama.getTarjetaRipley().getCodigoCore() != null && trama.getTarjetaRipley().getCodigoCore().intValue() == Constantes.NRO_UNO){
//			respuesta = pod + prefijo + tit + digito;
//		} else{
//			respuesta = trama.getTarjetaRipley().getPan().toString();
//		}
//		
//		return respuesta;
//	}
//
//	private StrBuilder generacionPersonalizadoLeyendaAntesTimbre(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//		
//		int secuencia	= 0;
//
//		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){ // ES TARJETA RIPLEY
//			
//			if (trama.getTarjetaRipley().getGlosaFinancieraGsic()!= null){
//				String glosa = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+master.getParametros().buscaValorPorNombre("LISTA_CARACTER_RARO")+"]", "");
//				glosa = glosa.replaceAll("@", "@@");
//				String glosas[] = glosa.split("@@");
//				for (int g = 0; g < glosas.length; g++){
//					respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//					respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//					respuesta.append(++secuencia).append(Constantes.PIPE);
//					respuesta.append(glosas[g]).appendNewLine();
//				}
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_COD_AUTORIZADOR).append(trama.getTarjetaRipley().getCodigoAutorizacion()).appendNewLine();
//
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE).appendNewLine();
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PLAT_CAR_1).appendNewLine();
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PLAT_CAR_2).appendNewLine();
//			} //TODO FALTA UN ELSE
//		}
//
//		
//		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
//			String formaPagoNC = "";
//			String convenio = "";
//			if (trama.getTarjetaBancaria().getVd() != null){
//				if (trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_S)){
//					formaPagoNC = Constantes.TRAMA_PLAT_DEBITO;
//					TVirtualCodBancaria tvirtual = new TVirtualCodBancaria();
//					GeneracionDTEDAO daoDTE = new GeneracionDTEDAOImpl();
//					tvirtual = daoDTE.getCodigoConvenioXTipo(trama.getTarjetaBancaria().getTipoTarjeta().intValue());
//					convenio = Constantes.TRAMA_EFECTIVO + tvirtual.getCodBoleta().toString();
//				} else if (trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_M)){
//					TVirtualCodBancaria tvirtual = new TVirtualCodBancaria();
//					formaPagoNC = Constantes.TRAMA_PLAT_MPAGO;
//					convenio = Constantes.TRAMA_EFECTIVO + tvirtual.getCodBoleta().toString();
//				} else {
//					formaPagoNC = Constantes.TRAMA_PLAT_BANCARIA;
//					TVirtualCodBancaria tvirtual = new TVirtualCodBancaria();
//					GeneracionDTEDAO daoDTE = new GeneracionDTEDAOImpl();
//					tvirtual = daoDTE.getCodigoConvenioXTipo(trama.getTarjetaBancaria().getTipoTarjeta().intValue());
//					convenio = Constantes.TRAMA_CONVENIO + tvirtual.getCodBoleta().toString();
//				}
//			}
//
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PLAT_TBK_1).append(formaPagoNC);
//			if (trama.getDespacho().getCodigoRegalo() != null && trama.getDespacho().getCodigoRegalo().intValue() > 0){
//				respuesta.append(Constantes.TRAMA_PLAT_TBK_2).append(trama.getDespacho().getCodigoRegalo());
//			}
//			respuesta.appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE).append(convenio).appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PLAT_TBK_3).append(trama.getTarjetaBancaria().getCodigoAutorizador()).appendNewLine();
//			
//			if (trama.getTarjetaBancaria().getOneclickBuyorder() != null){
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_ID_TBK).append(trama.getTarjetaBancaria().getOneclickBuyorder()).appendNewLine();
//			}
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE).append(Constantes.TRAMA_ACEPTO_PAGAR).appendNewLine();
//		}
//		
//		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
//			if (trama.getTarjetaRegaloEmpresa().getMonto().intValue() > Constantes.NRO_CERO){
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PLAT_TRE_1).append(trama.getTarjetaRegaloEmpresa().getNroTarjeta()).appendNewLine();
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PLAT_TRE_2).append(trama.getTarjetaRegaloEmpresa().getMonto()).appendNewLine();
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PLAT_TRE_3).appendNewLine();
//			}
//		}
//
//		if (trama.getDespacho().getCodigoRegalo() != null && trama.getDespacho().getCodigoRegalo().intValue() > 0){ // SI TIENE CODIGO DE REGALO
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_REGALO_DOS_1).appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_REGALO_DOS_2).appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_REGALO_DOS_3).appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_REGALO_DOS_4).appendNewLine();
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_A_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_REGALO_DOS_5).appendNewLine();
//		}
//		
//		return respuesta;
//	}
//	
//	private StrBuilder generacionPersonalizadoLeyendaDespuesTimbre(TramaDTE trama) throws AligareException{
//		StrBuilder respuesta = new StrBuilder();
//
//		String indicadorMkp = trama.getNotaVenta().getIndicadorMkp();
//		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//		if (articuloVentas.size() > 0){
//			int secuencia	= 0;
//
//			for (int i = 0;i < articuloVentas.size(); i++){
//				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//				if (indicadorMkp.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
//					if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){ // si es mkp, me lo salto
//						continue;
//					}
//				}
//				
//				if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es nota de credito, me lo salto
//					continue;
//				}
//				
//				
//				if ((articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO) 
//						&& (articuloVentaTrama.getArticuloVenta().getDescRipley().indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO)){
//
//					//TODO vta ripley (quizas sumar) revisar logica
//				}
//
//			}
//			
//			//if (trama.getDatoFacturacion().getCorrelativoVenta() != null){
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_D_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_DIRECCION_DESPACHO).append(trama.getDespacho().getDespachoId()).append("]").appendNewLine();
//				
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_D_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(trama.getDespacho().getDireccionDespacho()).appendNewLine();
//				
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_D_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_REGION).append(trama.getDespacho().getRegionDespachoDes()).appendNewLine();
//	
//				respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_PER_LEYENDA_D_TIMBRE).append(Constantes.PIPE);
//				respuesta.append(++secuencia).append(Constantes.PIPE);
//				respuesta.append(Constantes.TRAMA_COMUNA).append(trama.getDespacho().getComunaDespachoDes()).appendNewLine();
//			//}
//			respuesta.append(Constantes.TRAMA_IMPRESION_DETALLE).append(Constantes.PIPE);
//			respuesta.append(Constantes.TRAMA_PER_LEYENDA_D_TIMBRE).append(Constantes.PIPE);
//			respuesta.append(++secuencia).append(Constantes.PIPE).appendNewLine();
//		}
//		
//		return respuesta;
//	}
//
	private boolean getNumNC(TramaDTE trama, int esMkp) throws AligareException{
		boolean resultado = false;
		
		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		if (articuloVentas.size() > 0){
			
			if (esMkp == Constantes.NRO_MENOSUNO){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null){
						if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() == Constantes.NRO_UNO){
							resultado=true;
							break;
						}
					}
				}
			} else {
				//if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
					for (int i = 0;i < articuloVentas.size(); i++){
						ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp){
							if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() == Constantes.NRO_UNO){
								resultado=true;
								break;
							}
						} else {
							continue;
						}
					}
				//} else {
				//	resultado=true;
				//}
			}
		}
		logger.traceInfo("getNumNC", "oc: " + trama.getNotaVenta().getCorrelativoVenta() + " - esMkp: "+ esMkp + " - resultado: "+resultado);
		return resultado;
	}
//	
	private boolean getNumNCvsArticulos(TramaDTE trama) throws AligareException{
		boolean resultado = false;
		
		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		int numNC = 0;
		if (articuloVentas.size() > 0){
			for (int i = 0;i < articuloVentas.size(); i++){
				ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
				if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_CERO){
					numNC++;
				}
			}
		}
		
		if (articuloVentas.size() == numNC){
			resultado = true;
		}
		
		logger.traceInfo("getNumNC", "numNC: "+ numNC + " - resultado: "+resultado);
		return resultado;
	}
//	
	public boolean enviarEmailAnulacion(TramaDTE trama, String nroVoucher, Parametros pcv) throws AligareException {
		boolean resultado = false;
		TemplateVoucher template = new TemplateVoucher();
		
		if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.STRING_TRES)){
			template = templateDao.getTemplateByLlave("VOUCHER_ANULA");
			if (template.getLlave() == null){
				logger.traceInfo("enviarEmailAnulacion", "Correo llave 'VOUCHER_ANULA' no se encontr� o est� desactivado.");
				return false;
			}
			resultado = enviarEmailAnulacionCDF(trama, nroVoucher, template, pcv);
		} else {
			template = templateDao.getTemplateByLlave("ANULACION_MKP");
			if (template.getLlave() == null){
				logger.traceInfo("enviarEmailAnulacion", "Correo llave 'ANULACION_MKP' no se encontr� o est� desactivado.");
				return false;
			}
			resultado = enviarEmailAnulacionMKP(trama, nroVoucher, template, pcv);
		}
		return resultado;
	}
//
	public boolean enviarEmailAnulacionCDF(TramaDTE trama, String nroVoucher, TemplateVoucher template, Parametros pcv) throws AligareException {
		boolean resultado = false;
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		ClubDAO clubDAO = new ClubDAOImpl();
		ClienteClub clienteClub = new ClienteClub();
		
		String destinatario = trama.getDespacho().getEmailCliente();

		String urlCloudFront = pcv.buscaValorPorNombre("URL_CLOUDFRONT");
		String nombreCliente = trama.getDespacho().getNombreCliente();
		String tipoTransaccion = "";
		if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
			tipoTransaccion = "Marketplace";
		} else if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
			tipoTransaccion = "Club de regalos";
		}
		
		String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, pcv);
		String rutCliente = "";
		if (trama.getDespacho().getRutCliente() != null){
			rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		}
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;
		
		String rutRecaudacion = "";
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}

		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+pcv.buscaValorPorNombre("LISTA_CARACTER_RARO")+"]", "");
			glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
			String glosas[] = glosaFinanciera.split("@@");
			for (int g = 0; g < glosas.length; g++){
				respuesta.append(glosas[g]).appendNewLine();
			}
			glosaFinanciera = respuesta.toString();
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Tipo_transaccion", tipoTransaccion);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_emision", fechaEmision);
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Rut", rutCliente);
			valores.put("Medio_Pago", medioPago);
			valores.put("Glosa_financiera", glosaFinanciera);
			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp() != null && articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}

					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es nota de credito, me lo salto
						continue;
					}

					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(pcv.buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "VOUCHER ANULACION OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, pcv.buscaValorPorNombre("REMITENTE"), destinatario, pcv.buscaValorPorNombre("SILVERPOP_GROUP"));

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}
//
	private StrBuilder getCodigoUnicoGrilla(String sucursal, String caja, int nroTransaccion, Parametros pcv) throws AligareException{
		StrBuilder respuesta = new StrBuilder();
		CajaVirtualDAO cajaVirtual = new CajaVirtualDAOImpl();
		respuesta.append(Constantes.TRAMA_NUMERO_UNICO_NU);
		String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
//		String fecha = FechaUtil.getCurrentDateTimeSimpleString();
		String fecha = cajaVirtual.sysdateFromDual(Constantes.FECHA_YYMMDD_HHMISS, pcv);
		String fechaArr[] = fecha.split(Constantes.SPACE);
		String codigoCaja = Util.rellenarString(caja, Constantes.NRO_CUATRO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String transaccion = Util.rellenarString(nroTransaccion+Constantes.VACIO, Constantes.NRO_SEIS, Constantes.VERDADERO, Constantes.CHAR_CERO);
		
		respuesta.append(codigoSucursal).append(Constantes.GUION).append(codigoCaja).append(Constantes.GUION).append(transaccion);
		respuesta.append(Constantes.GUION).append(fechaArr[0]).append(Constantes.GUION).append(fechaArr.length < Constantes.NRO_DOS ? "000000" : fechaArr[1]);
		
		return respuesta;
	}
//
	private StrBuilder getGlosaAcepto(TramaDTE trama) throws AligareException{
		StrBuilder respuesta = new StrBuilder();
		
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			respuesta.append(Constantes.TRAMA_PLAT_TRE_3).appendNewLine();
		}
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_1).appendNewLine();
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_2).appendNewLine();
			respuesta.append(Constantes.TRAMA_PLAT_CAR_1_3).appendNewLine();
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
/*
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			} else {
				respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR);
			}
*/
			respuesta.append(Constantes.TRAMA_ACEPTO_PAGAR).appendNewLine();
		}
		
		return respuesta;
	}
	
	public boolean enviarEmailAnulacionMKP(TramaDTE trama, String nroVoucher, TemplateVoucher template, Parametros pcv) throws AligareException {
		boolean resultado = false;
		CajaVirtualDAO caja = new CajaVirtualDAOImpl();
		ClubDAO clubDAO = new ClubDAOImpl();
		ClienteClub clienteClub = new ClienteClub();

		String destinatario = trama.getDespacho().getEmailCliente();

		String urlCloudFront = pcv.buscaValorPorNombre("URL_CLOUDFRONT");
		String sucursal = pcv.buscaValorPorNombre("CODIGO_SUCURSAL");
		String codigoSucursal = Util.rellenarString(sucursal, Constantes.NRO_CINCO, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroCaja = pcv.buscaValorPorNombre("NRO_CAJA");
		String codigoCaja = Util.rellenarString(nroCaja, Constantes.NRO_TRES, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nroVoucherX10 = Util.rellenarString(nroVoucher, Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO);
		String nombreCliente = trama.getDespacho().getNombreCliente() + Constantes.SPACE + trama.getDespacho().getApellidoPatCliente();
		String direccionCliente = trama.getDespacho().getDireccionCliente();

		String fechaEmision = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, pcv);
		String rutCliente = Util.formatoRUT(trama.getDespacho().getRutCliente() + Util.getDigitoValidadorRut(trama.getDespacho().getRutCliente().toString()));
		String nroEvento = (trama.getNotaVenta().getCodigoRegalo() != null)?trama.getNotaVenta().getCodigoRegalo().toString():Constantes.VACIO;

		String codBoTipoDoc = trama.getTipoDoc().getCodBo().toString();
		String descTipoDoc  = trama.getTipoDoc().getDescTipodoc();

		String rutRecaudacion = "";
		if (trama.getNotaVenta().getCodigoRegalo() != null){
			clienteClub = clubDAO.getClienteClub(new Integer(nroEvento), Constantes.NRO_UNO);
			if (clienteClub != null){
				rutRecaudacion = Util.formatoRUT(clienteClub.getRut());
			}
		}

		String rutEvento = rutRecaudacion;
		String medioPago = "";
		String titularMedioPago = "";
		String nroConvenioMP = "";
		String codAutorizadorMP = "";
		
		String nroUnico = getCodigoUnicoGrilla(sucursal, nroCaja, Integer.parseInt(nroVoucher), pcv).toString();
		String glosaAcepto = getGlosaAcepto(trama).toString();
		
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			medioPago = "TARJETA RIPLEY";
			codAutorizadorMP = trama.getTarjetaRipley().getCodigoAutorizacion().toString();
			titularMedioPago = "RUT CLIENTE: "+trama.getTarjetaRipley().getRutTitular();
			titularMedioPago = titularMedioPago + Constantes.SPACE + Constantes.GUION + Constantes.SPACE;
			titularMedioPago = titularMedioPago + "NOMBRE TIT.: ";
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getNombreCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoPatCliente()) + Constantes.SPACE;
			titularMedioPago = titularMedioPago + Util.getVacioPorNulo(trama.getDespacho().getApellidoMatCliente());
		}
		if (trama.getTarjetaBancaria().getCorrelativoVenta() != null){
			if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_ONE_CLICK)){
				medioPago = Constantes.BOL_FORMA_PAGO_WO + Constantes.SPACE;
			} else if (trama.getTarjetaBancaria().getMedioAcceso() != null && trama.getTarjetaBancaria().getMedioAcceso().equalsIgnoreCase(Constantes.TARJETA_MERCADO_PAGO)){
				medioPago = Constantes.BOL_FORMA_PAGO_MP + Constantes.SPACE;
			} else {
				medioPago = Constantes.BOL_FORMA_PAGO_TB + Constantes.SPACE;
			}
			
			if (trama.getTarjetaBancaria().getVd() != null && trama.getTarjetaBancaria().getVd().equalsIgnoreCase(Constantes.BOL_VALOR_VD_N)){
				medioPago = medioPago + Constantes.TRAMA_PLAT_CREDITO;
			} else {
				medioPago = medioPago + Constantes.TRAMA_PLAT_DEBITO;
			}

			medioPago = medioPago.toUpperCase();
			codAutorizadorMP = trama.getTarjetaBancaria().getCodigoAutorizador();
			if (trama.getTarjetaBancaria().getCodigoConvenio() != null){
				nroConvenioMP = trama.getTarjetaBancaria().getCodigoConvenio().toString();
			}
		}
		if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta() != null){
			medioPago = "TARJETA REGALO EMPRESAS";
			codAutorizadorMP = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador().toString();
		}
		if(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null && trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			medioPago = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
		}
		
		String glosaFinanciera = "";
		
		StrBuilder respuesta = new StrBuilder();
		if (trama.getTarjetaRipley().getCorrelativoVenta() != null){
			if(trama.getTarjetaRipley().getGlosaFinancieraGsic() != null) {
				
				glosaFinanciera = trama.getTarjetaRipley().getGlosaFinancieraGsic().replaceAll("["+pcv.buscaValorPorNombre("LISTA_CARACTER_RARO")+"]", "");
				glosaFinanciera = glosaFinanciera.replaceAll("@", "@@");
				String glosas[] = glosaFinanciera.split("@@");
				for (int g = 0; g < glosas.length; g++){
					respuesta.append(glosas[g]).appendNewLine();
				}
				glosaFinanciera = respuesta.toString();
				
			}
			
		}
		
		try {
			String html = template.getHtml();
			HashMap<String, String> valores = new HashMap<String, String>();
			valores.put("Url_cloudfront", urlCloudFront);
			valores.put("Nombre", nombreCliente);
			valores.put("Orden_compra", String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
			valores.put("Fecha_hora_emision", fechaEmision);
			
			valores.put("Nro_evento", nroEvento);
			valores.put("Rut_recaudacion", rutEvento);
			
			valores.put("Sucursal", codigoSucursal);
			valores.put("Caja", codigoCaja);
			valores.put("Nro_voucher", nroVoucher);
			valores.put("Nro_voucher_x10", nroVoucherX10);
			valores.put("Rut", rutCliente);
			valores.put("DireccionCliente", direccionCliente);
			
			valores.put("CodBO_Tipo_Doc", codBoTipoDoc);
			valores.put("Desc_Tipo_Doc", descTipoDoc);
			
			valores.put("Medio_Pago", medioPago);
			valores.put("Datos_titular_TRipley", titularMedioPago);
			valores.put("Nro_Convenio", nroConvenioMP);
			valores.put("Nro_autorizador", codAutorizadorMP);
			valores.put("Nro_unico", nroUnico);
			valores.put("Glosa_financiera", glosaFinanciera);
			valores.put("Glosa_Acepto", glosaAcepto);

			HashMap<String, String> lista = new HashMap<String, String>();
			
			String codArticulo = "";
			String descRipley = "";
			int unidades = 0;
			int precio = 0;
			int valor_linea_detalle = 0;
			int subtotal = 0;
			int total = 0;
			int contador = 1;
			String descuentoS	= "";
			int descuentoI		= 0;

			List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
			if (articuloVentas.size() > 0){
				for (int i = 0;i < articuloVentas.size(); i++){
					ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
					if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //tiene ripley y mkp
						if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY){ // si es ripley me lo salto
							continue;
						}
					}

					if (articuloVentaTrama.getArticuloVenta().getEsNC() != null && articuloVentaTrama.getArticuloVenta().getEsNC().intValue() != Constantes.NRO_UNO){ // si no es nota de credito, me lo salto
						continue;
					}
					
					descuentoS	= "";
					descuentoI = articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue();
					if (descuentoI > 0){
						descuentoS = "-"+descuentoI;
					}

					codArticulo = articuloVentaTrama.getArticuloVenta().getCodArticulo();
					descRipley = articuloVentaTrama.getArticuloVenta().getDescRipley();
					unidades = articuloVentaTrama.getArticuloVenta().getUnidades().intValue();
					precio = articuloVentaTrama.getArticuloVenta().getPrecio().intValue();
					valor_linea_detalle = precio * unidades;
					subtotal =  subtotal + (valor_linea_detalle);
					if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() != null){
						total =  total + (valor_linea_detalle - articuloVentaTrama.getArticuloVenta().getMontoDescuento().intValue());
					} else {
						total =  total + valor_linea_detalle;
					}
					lista.put("Codigo_Producto"+contador, codArticulo);
					lista.put("Descripcion"+contador, descRipley);
					lista.put("Cantidad"+contador, String.valueOf(unidades));
					lista.put("Precio_Unidad"+contador, String.valueOf(precio));
					lista.put("Precio_Producto"+contador, String.valueOf(valor_linea_detalle));
					lista.put("Descuento"+contador, descuentoS);

					lista.put("Sub_Orden"+contador, articuloVentaTrama.getIdentificadorMarketplace().getOrdenMkp());
					lista.put("Rut_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRutProveedor());
					lista.put("Razon_social_proveedor"+contador, articuloVentaTrama.getIdentificadorMarketplace().getRazonSocial());
					
					contador++;
				}
			}
			valores.put("Subtotal", String.valueOf(subtotal));
			valores.put("Total_compra", String.valueOf(total));
			
			ClienteSMTP clienteSmtp = new ClienteSMTP();
			html = clienteSmtp.completarHTML(html, valores, lista);
			resultado = clienteSmtp.enviarCorreo(pcv.buscaValorPorNombre("SILVERPOP_HOST"), Integer.parseInt(pcv.buscaValorPorNombre("PORT")), "VOUCHER ANULACION MKP OC: "+trama.getNotaVenta().getCorrelativoVenta(), html, pcv.buscaValorPorNombre("REMITENTE"), destinatario, pcv.buscaValorPorNombre("SILVERPOP_GROUP"));

		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}

	private boolean checkAnulacionEnLinea(Integer estado, TramaDTE trama) throws AligareException{
		boolean resultado = true;
		if (estado.intValue() == 0){
			if (trama.getTarjetaBancaria().getCorrelativoVenta()!=null){
				resultado = false;
			}
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	
	/**Procesa la nota de venta a partir del número de orden de compra, la función y el número de caja.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 07-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param correlativoVenta
	 * @param funcionMetodo
	 * @param numCaja
	 * @return
	 */
	@Transactional(rollbackFor = {RuntimeException.class, Exception.class})
	public GenericResponse procesarAnulacionMkp(Long correlativoVenta, Integer funcionMetodo, Integer numCaja) throws RestServiceTransactionException {
		logger.initTrace("procesarAnulacionMkp", "correlativoVenta = " + String.valueOf(correlativoVenta) + " - funcionMetodo = " + String.valueOf(funcionMetodo) +
				" - numCaja = " + String.valueOf(numCaja), 
				new KeyLog("correlativoVenta", String.valueOf(correlativoVenta)),
				new KeyLog("funcionMetodo", String.valueOf(funcionMetodo)),
				new KeyLog("numCaja", String.valueOf(numCaja)));


		GenericResponse resp = new GenericResponse();

		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje("OK");


		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());

		paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_NC, parametros.getParametros(), "1");

		parametros.reemplazaValorLista(Constantes.NRO_CAJA_NC, String.valueOf(numCaja));
		parametros.reemplazaValorLista(Constantes.NRO_CAJA_NAME, String.valueOf(numCaja));

		Parametro param = new Parametro();
		param.setNombre(Constantes.FUNCION);
		param.setValor(String.valueOf(funcionMetodo));
		parametros.getParametros().add(param);

		final Integer numeroCaja = numCaja;
		final Integer numeroSucursal = new Integer(parametros.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		List<TramaDTE> qOrdenCompra =	Stream.of(Constantes.NRO_CERO, Constantes.NRO_VEINTITRES, Constantes.NRO_VEINTICUATRO)
										.collect(ArrayList<TramaDTE>::new, 
												(list, it) -> list.addAll(generacionDTE.obtenerUnaOrdenParaGeneracion(	correlativoVenta,
																														numeroCaja, 
																														numeroSucursal, 
																														it)), 
												List::addAll);


		if(qOrdenCompra == null || qOrdenCompra.isEmpty()) {

			logger.traceInfo("procesarAnulacionMkp", "No se encuentra la Orden: " + correlativoVenta, new KeyLog("Correlativo Venta", String.valueOf(correlativoVenta)));
			resp.setCodigo(-1);
			resp.setMensaje("No se encuentra la Orden");

			return resp;

		}

		Integer estadoActual = null;
		Integer newEstado = Constantes.NRO_CERO;
		String fecha = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY, parametros);
		String fechaHora = caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS, parametros);
		String fechaYYYYMMDD = caja.sysdateFromDual(Constantes.FECHA_YYYY_MM_DD, parametros);
		String xmlMail="";
		int a = 0;
		int z = qOrdenCompra.size();

		TramaDTE inOrdenCompra = null;

		for (Iterator<TramaDTE> iterator = qOrdenCompra.iterator(); iterator.hasNext(); ) {

			inOrdenCompra = iterator.next();
			NotaCreditoBusiness businessNC = new NotaCreditoBusiness(parametros, inOrdenCompra);
			logger.traceInfo("procesarAnulacionMkp", "EsclavoNC "+this.getName()+ " procesando orden " + inOrdenCompra.getNotaVenta().getCorrelativoVenta() + " "+ (++a) + " de "+z);
			try {
				
				estadoActual = inOrdenCompra.getNotaVenta().getEstado();
				
				if (estadoActual.intValue() == Constantes.NRO_CERO){
	
					if (!notaCredito.actualizarEsNCArticuloVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_UNO))){
						logger.traceInfo("procesarAnulacionMkp", "Error al actualizar articulos de venta a estado es_nc 1", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
						continue;
					}
	
					if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() != null || inOrdenCompra.getNotaVenta().getFolioSii() != null){
						if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TRECE), Integer.valueOf(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
							logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+Constantes.NRO_TRECE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
						}
						continue;
					} else {
						if (!rechazo.rechazoGeneral(inOrdenCompra, parametros, fechaHora)){
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+Constantes.NRO_TREINTANUEVE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
						} else {
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TRES), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+Constantes.NRO_TRES, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
							
							//UpdateMkp
							if(!generacionDTE.updateIdentificadorMkpBoleta(inOrdenCompra, Constantes.NRO_TRES, parametros, Constantes.NRO_CERO, fecha, Constantes.NRO_CERO)){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 3 MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}

						}
						continue;
					}
				} else {
					if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
						newEstado = new Integer(Constantes.NRO_TRES);
					}
					else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
						newEstado = new Integer(Constantes.NRO_CUATRO);
					}
				
					if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() == null  && inOrdenCompra.getNotaVenta().getCorrelativoBoleta() == null){  //.getFolioSii() == null){
						if (!rechazo.rechazoGeneral(inOrdenCompra, parametros, fechaHora)){
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+Constantes.NRO_TREINTANUEVE, new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
						} else {
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
						}
						continue;
					} else {
	
						if (!getNumNC(inOrdenCompra, Constantes.NRO_MENOSUNO)){
							newEstado = new Integer(Constantes.NRO_DOS);
							logger.traceInfo("procesarAnulacionMkp", "Se deja orden "+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+" en estado "+newEstado+" debido a que no tiene marcado articulo en es_nc");
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
							
							if (!generacionDTE.registraTransaccion(parametros,Constantes.NRO_CERO, String.valueOf(Constantes.NRO_CERO), fechaHora, Constantes.ERROR, "NC NO GENERADA - OC sin ES_NC - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)){
								logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
							
							continue;
						}
						
						int nroTransaccion = operacionesCaja.cajaVirtual(parametros, Boolean.TRUE);
							
						String nroFolio = "654321";
						
						int switchNC = Integer.parseInt(parametros.buscaValorPorNombre(Constantes.SWITCH_NC));
						
						//generar voucher para INDICADOR_MKP = 1, 2 y 3  y si 2, tambien ES_MKP = 1
						if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || 
								inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS) ||
								inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
							logger.traceInfo("procesarAnulacionMkp", "RECAUDACION: "+(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
							boolean tieneNC = true;
							if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
								tieneNC = getNumNC(inOrdenCompra, Constantes.ES_MKP_RIPLEY);
							} else {
								tieneNC = getNumNC(inOrdenCompra, Constantes.ES_MKP_MKP);
							}
							
							if (tieneNC){
								
								if (!enviarEmailAnulacion(inOrdenCompra, String.valueOf(nroTransaccion), parametros)){
									logger.traceInfo("procesarAnulacionMkp", "Error en envio email anulación", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
								}
								if (inOrdenCompra.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL){
									newEstado = new Integer(Constantes.NRO_TRES);
								}
								else if (inOrdenCompra.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_PARCIAL){
									newEstado = new Integer(Constantes.NRO_CUATRO);
								}
								else {
									logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden, por estado desconocido", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("ESTADO", String.valueOf(inOrdenCompra.getNotaVenta().getEstado())));
									if (!generacionDTE.registraTransaccion(parametros,nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
										logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
									continue;
								}
								
								
								
								//LLAMADA AL FLUJO DE TRANSBANK
								//Primero se valida que el JAR de reproceso TBK est� funcionando, de lo contrario no se inserta en la tabla ---JPRM
//								if (SWITCH_ONOFF_TBK_NC.equalsIgnoreCase(Constantes.STRING_UNO)){
//									if(inOrdenCompra.getTarjetaBancaria().getVd() != null && inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//										businessNC.anularTBK(parametros, inOrdenCompra, getSubtotalNC(inOrdenCompra),nroTransaccion);
//									//SIGUE EL FLUJO NORMAL
//									}
//								}
								
								if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO)){
									if (!anulacionRecaudacion.AnulacionCDF(inOrdenCompra, parametros, fecha, fechaHora)){
										logger.traceInfo("procesarAnulacionMkp", "Error en satelites CDF", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
										if (!generacionDTE.registraTransaccion(parametros,nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
										continue;
									}
								} else {
									if (!anulacionRecaudacion.anulacionMKP2(inOrdenCompra, parametros, fecha, fechaHora, nroTransaccion)){
										logger.traceInfo("procesarAnulacionMkp", "Error en satelites MKP", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
										if (!generacionDTE.registraTransaccion(parametros,nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
										throw new RestServiceTransactionException("Error en satelites MKP", Constantes.NRO_TREINTANUEVE);
									}
								}
								
								if (!inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){ //AMBAS SE DEJA PARA MAS ADELANTE
									
									if (getNumNCvsArticulos(inOrdenCompra)){
										newEstado = new Integer(Constantes.NRO_TRES);
									}

									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
										logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
									if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP)){
										if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ANRM, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
									} else {
										if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ANRR, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
									}

									if (!anulacionRecaudacion.updateIdentificadorMkpReca(inOrdenCompra, parametros, nroTransaccion, fecha)){
										logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion MKPReca", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
			
								} else {
									
									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, numeroCaja)){
										logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
									
									if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroTransaccion), fechaHora, Constantes.ANRM, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_UNO, Constantes.NRO_UNO)){
										logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}

									if (!anulacionRecaudacion.updateIdentificadorMkpReca(inOrdenCompra, parametros, nroTransaccion, fecha)){
										logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion MKPReca", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
									
								}
							}
			
						}
						
						//Si llego hasta aquí entonces siempre debo aumentar el número de trx
						//aumento el nroTrx en la tabla de control de transacciones
						try {
							cajaDAO.updTrxCaja(numeroCaja, numeroSucursal, fechaYYYYMMDD, Long.valueOf(nroTransaccion));
						}catch(AligareException e) {
							if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), Constantes.NRO_TREINTANUEVE, numeroCaja)){
								logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
							}
							throw new RestServiceTransactionException("Error en aumentar el nroTrx en la tabla de control de transacciones", Constantes.NRO_VEINTINUEVE);
						}
						
						//Para el caso de Perú se va a cortar el proceso desde aquí en adelante ya que no hay notas de crédito. Solo anulaciones MKP.
						if(switchNC > Constantes.NRO_CERO) {
							
							//generar trama para ir a PPL (ESTA TRAMA ES SOLO PARA INDICADOR_MKP = 0 y 2) y ES_MKP = 0
							StrBuilder trama = new StrBuilder();
							if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_RIPLEY) || 
									inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
								
								logger.traceInfo("procesarAnulacionMkp", "BOLETA: "+(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
								
								if (getNumNC(inOrdenCompra, Constantes.ES_MKP_RIPLEY)){
									if (inOrdenCompra.getNotaVenta().getFolioSii() == null){
										logger.traceInfo("procesarAnulacionMkp", "Orden sin folio SII, se envia a impresion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
											newEstado = new Integer(Constantes.NRO_TRECE);
										}
										else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
											newEstado = new Integer(Constantes.NRO_CATORCE);
										}
										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")))){
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
			
										continue;
									}
									
									if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
										nroTransaccion = operacionesCaja.cajaVirtual(parametros, Boolean.TRUE);
									}
									
//									try{
//										trama.append(Constantes.TRAMA_INICIO_TRAMA);//.append(Constantes.TAB).append(Constantes.TRAMA_TRAINING).append(Constantes.TAB);
//										trama.append(generacionCabecera(inOrdenCompra));
//										trama.append(generacionDetalle(inOrdenCompra));
//										trama.append(generacionSubtotal(inOrdenCompra));
//										trama.append(generacionDescuentosRegalos(inOrdenCompra));
//										trama.append(generacionReferencia(inOrdenCompra));
//										trama.append(generacionPersonalizado(inOrdenCompra, nroTransaccion));
//										trama.append(generacionPersonalizadoEncabezado(inOrdenCompra, nroTransaccion));
//										trama.append(generacionPersonalizadoDetalle(inOrdenCompra));
//										trama.append(generacionPersonalizadoMedioPago(inOrdenCompra, nroTransaccion));
//										trama.append(generacionPersonalizadoLeyendaAntesTimbre(inOrdenCompra));
//										trama.append(generacionPersonalizadoLeyendaDespuesTimbre(inOrdenCompra));
//										trama.append(Constantes.TAB).append(Constantes.TRAMA_FIN_TRAMA);
//									} catch (AligareException e){
//										logger.traceInfo("procesarAnulacionMkp", "Error en generacion trama PPL", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
//										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_VEINTINUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
//											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 29", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										if (!generacionDTE.registraTransaccion(parametros,new Integer(nroTransaccion), new Integer(0), fechaHora, Constantes.ERROR, "En generaqcion trama PPL NC - msj: "+e.getMessage(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_CERO)){
//											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//										continue;
//									}
//									
//									logger.traceInfo("procesarAnulacionMkp", "TRAMA NOTA DE CREDITO: \n"+(trama.toString()));
//					
//									if (!checkAnulacionEnLinea(estadoActual, inOrdenCompra)){
////										resultadoPpl = paperless.generacionDte(Integer.parseInt(parametros.buscaValorPorNombre("PPL_RUT")), parametros.buscaValorPorNombre("PPL_LOGIN"), parametros.buscaValorPorNombre("PPL_PASS"), trama.toString(), Constantes.PPL_TIPO_GENERACION, Constantes.PPL_TIPO_RETORNO, Integer.parseInt(parametros.buscaValorPorNombre("PPL_WS_TIMEOUT_SEGUNDOS")));
//										resultadoPpl = null;
//										if (resultadoPpl != null && resultadoPpl.getCodigo() == Constantes.NRO_CERO){
//											nroFolio = resultadoPpl.getMensaje();
//										} else {
//											
//											List<ResultadoPPL> listadoErrores = paperless.getListadoErroresPPL(resultadoPpl.getCodigo());
//											boolean errorPPLSaltar = false;
//											if (listadoErrores.size() > 0){
//												
//												for (int p = 0; p < listadoErrores.size(); p++){
//													ResultadoPPL errorPpl = listadoErrores.get(p);
//													if (resultadoPpl.getMensaje().equalsIgnoreCase(errorPpl.getMensaje())){
//														errorPPLSaltar = true;
//														break;
//													}
//												}
//												
//											} else {
//												errorPPLSaltar = false;
//											}
	//
//											if (errorPPLSaltar){
//												if (Constantes.NRO_MENOSNOVENTAOCHO == resultadoPpl.getCodigo()){
//													logger.traceInfo("procesarAnulacionMkp", "Error en respuesta PPL NC, codigo: "+resultadoPpl.getCodigo());
//													if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
//														logger.traceInfo("procesarAnulacionMkp", "Error al volver OC a estado Original, por error en PPL Cod "+resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//													}
//													continue;
//												}
//											} else{
//												logger.traceInfo("enviarEmailBoleta", "Error en respuesta PPL NC, mensaje NULO");
//												if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), estadoActual, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
//													logger.traceInfo("procesarAnulacionMkp", "Error al volver OC a estado Original, por error en PPL Cod "+resultadoPpl.getCodigo(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//												}
//												continue;
//											}
//											
//											
//											logger.traceInfo("procesarAnulacionMkp", "Error en respuesta PPL FOLIO", new KeyLog("CODIGO", String.valueOf(resultadoPpl.getCodigo())), new KeyLog("MENSAJE", resultadoPpl.getMensaje()));
//											if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
//												logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//											}
//											if (!generacionDTE.registraTransaccion(parametros, new Integer(nroTransaccion), new Integer(0), fechaHora, Constantes.ERROR, "En PPL FOLIO NC - msj: "+resultadoPpl.getMensaje(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//												logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//											}
//											continue;
//										}
//									} else {
										nroFolio = String.valueOf(nroTransaccion);
//									}
									
									//LLAMADA AL FLUJO DE TRANSBANK
									//LLAMADA AL FLUJO DE TRANSBANK
									//Primero se valida que el JAR de reproceso TBK est� funcionando, de lo contrario no se inserta en la tabla ---JPRM
//									if (SWITCH_ONOFF_TBK_NC.equalsIgnoreCase(Constantes.STRING_UNO)){
//											if(inOrdenCompra.getTarjetaBancaria().getVd() != null && inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//											//if(inOrdenCompra.getTarjetaBancaria().getVd().equalsIgnoreCase("N") && Validaciones.validarVacio(inOrdenCompra.getTarjetaBancaria().getMedioAcceso())){
//												businessNC.anularTBK(parametros, inOrdenCompra, getSubtotalNC(inOrdenCompra),nroTransaccion);
//											//SIGUE EL FLUJO NORMAL
//											}
//									}
//									RetornoNotaCredito retornoNC = new RetornoNotaCredito();
//									retornoNC =	businessNC.generar(parametros, inOrdenCompra, nroTransaccion, nroFolio, FechaUtil.stringToTimestamp(fechaHora, Constantes.FECHA_DDMMYYYY_HHMMSS3));
									
//									if (retornoNC.getCodigo().intValue() != 1){
//										if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_TREINTANUEVE), new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
//											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a 39", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("COD RETORNO", String.valueOf(retornoNC.getCodigo().intValue())),new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("MSG ERROR", String.valueOf(retornoNC.getMensaje())));
//										}
//										if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroFolio), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+" "+retornoNC.getMensaje(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
//											logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
//										}
//									} else {
										logger.traceInfo("procesarAnulacionMkp", "estadoActual: "+estadoActual);
										if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
											newEstado = new Integer(Constantes.NRO_TRES);
											if (getNumNCvsArticulos(inOrdenCompra)){
												newEstado = new Integer(Constantes.NRO_TRES);
											}

										}
										else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
											newEstado = new Integer(Constantes.NRO_CUATRO);
											if (getNumNCvsArticulos(inOrdenCompra)){
												newEstado = new Integer(Constantes.NRO_TRES);
											}
										}
										else {
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden, por estado desconocido", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)), new KeyLog("ESTADO", String.valueOf(inOrdenCompra.getNotaVenta().getEstado())));
											if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroFolio), fechaHora, Constantes.ERROR, "En satelites - orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
												logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
											}
											continue;
										}
										logger.traceInfo("procesarAnulacionMkp", "newEstado: "+newEstado);
										
										if (!generacionDTE.actualizarNotaVentaEstadoFolio(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, nroFolio, Integer.parseInt(parametros.buscaValorPorNombre("NRO_CAJA_NC")), Constantes.NRO_TRES)){
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										} else {

										}
										//UpdateMkp
										if (!businessNC.updateMkpNotaCredito(inOrdenCompra, parametros, newEstado.intValue(), nroTransaccion, fechaHora)){
											logger.traceInfo("procesarAnulacionMkp", "Error al actualizar Identificador Marketplace en estado "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
										}
										if (inOrdenCompra.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
											if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroFolio), fechaHora, Constantes.IMPRESION, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_CERO, Constantes.NRO_UNO)){
												logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
											}
										} else {
											if (!generacionDTE.registraTransaccion(parametros, nroTransaccion, String.valueOf(nroFolio), fechaHora, Constantes.IMPRESION, "orden: "+inOrdenCompra.getNotaVenta().getCorrelativoVenta(), inOrdenCompra, Constantes.NRO_DOS, Constantes.NRO_UNO)){
												logger.traceInfo("procesarAnulacionMkp", "Error al registrar Transaccion", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
											}
										}
					
//									}
								}else {
									if (estadoActual.intValue() == Constantes.NOTA_CREDITO_TOTAL){
										newEstado = new Integer(Constantes.NRO_TRES);
									}
									else if (estadoActual.intValue() == Constantes.NOTA_CREDITO_PARCIAL){
										newEstado = new Integer(Constantes.NRO_CUATRO);
									}
									
									if (getNumNCvsArticulos(inOrdenCompra)){
										newEstado = new Integer(Constantes.NRO_TRES);
									}

									if (!generacionDTE.actualizarNotaVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), newEstado, new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC")))){
										logger.traceInfo("procesarAnulacionMkp", "Error al actualizar la orden a "+newEstado.intValue(), new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())), new KeyLog("NRO TRANSACCION", String.valueOf(nroTransaccion)));
									}
								}
								//Update esNC en Dos
								if (!notaCredito.actualizarEsNCArticuloVenta(inOrdenCompra.getNotaVenta().getCorrelativoVenta(), new Integer(Constantes.NRO_DOS))){
									logger.traceInfo("procesarAnulacionMkp", "Error al actualizar articulos de venta a estado es_nc 2", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
								}
								logger.traceInfo("procesarAnulacionMkp", "Finalizado", new KeyLog("CORRELATIVO", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
							}
							
						}
						
					}
				}
			} catch (AligareException ae){
				logger.traceInfo("procesarAnulacionMkp", "ERROR RARO");
				logger.traceError("procesarAnulacionMkp", ae);
				logger.endTrace("procesarAnulacionMkp", "Finalizado", null);
				throw new RestServiceTransactionException("Problema en anulación", Constantes.NRO_MENOSUNO, ae);
			}
		}
		logger.endTrace("procesarAnulacionMkp", "Finalizado", null);
		return resp;
	}
	
	public GenericResponse obtenerNumerosOCsAnulacion(Integer caja, Integer cantidadOCs) {
		logger.initTrace("obtenerNumerosOCsAnulacion", 
				"Integer caja: " + String.valueOf(caja) 
				+ " - Integer cantidadOCs: " + String.valueOf(cantidadOCs), 
				new KeyLog("Caja", String.valueOf(caja)), 
				new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)));

		ConsultaOcImpresionResponse resp = new ConsultaOcImpresionResponse();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje("OK");
		resp.setNumerosOCs(new ArrayList<Long>(cantidadOCs));
		
		logger.traceInfo("obtenerNumerosOCsAnulacion", 
				"Agrego Números de OCs con los siguientes parámetros: Integer caja: " + String.valueOf(caja) 
				+ " - Integer cantidadOCs: " + String.valueOf(cantidadOCs)
				+ " - Integer estado: " + Constantes.NRO_CERO + ", " + Constantes.NRO_VEINTITRES + ", " + Constantes.NRO_VEINTICUATRO, 
				new KeyLog("Caja", String.valueOf(caja)), 
				new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)),
				new KeyLog("Estado", String.valueOf(Constantes.NRO_CERO)),
				new KeyLog("Estado", String.valueOf(Constantes.NRO_VEINTITRES)),
				new KeyLog("Estado", String.valueOf(Constantes.NRO_VEINTICUATRO)));
		
		Stream.of(Constantes.NRO_CERO, Constantes.NRO_VEINTITRES, Constantes.NRO_VEINTICUATRO)
		.collect(() -> resp.getNumerosOCs(),
				(list, it) -> list.addAll(generacionDTE.obtenerNumerosOCsByEstado(caja, cantidadOCs, it, Constantes.FUNCION_NC)),
				List::addAll);
		
		logger.endTrace("obtenerNumerosOCsAnulacion", "Finalizado OK", null);
		
		return resp;
	}

}
