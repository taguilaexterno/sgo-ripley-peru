package cl.ripley.omnicanalidad.logic.notaCredito;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.dao.impl.GeneracionDTEDAOImpl;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.util.Constantes;

public class MaestroNotaCredito {
	private static final AriLog logger = new AriLog(MaestroNotaCredito.class,"RIP16-004", PlataformaType.JAVA);
	private ConcurrentLinkedQueue<TramaDTE> qOrdenCompra = null;
	private List<EsclavoNotaCredito> slaves = null;
	private Semaphore sem = null;
	private Parametros parametros;

	public MaestroNotaCredito(Parametros pPars) {
		parametros = pPars;
		slaves = new ArrayList<EsclavoNotaCredito>();
		qOrdenCompra = new ConcurrentLinkedQueue<TramaDTE>();
	}
	
	public void process() {
		logger.initTrace("process", null);

		//inicializa DAOs
		Integer nroCaja = new Integer(parametros.buscaParametroPorNombre("NRO_CAJA").getValor()); //new Integer(82);
		Integer nroSucusal = new Integer(parametros.buscaParametroPorNombre("SUCURSAL").getValor());
		GeneracionDTEDAO generacionDTE = new GeneracionDTEDAOImpl();
		
		//llena la cola de OCs
		Integer nroEstado = new Integer(Constantes.NRO_CERO);
		qOrdenCompra = generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado);

		nroEstado = new Integer(Constantes.NRO_VEINTITRES);
		qOrdenCompra.addAll(generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado));
		
		nroEstado = new Integer(Constantes.NRO_VEINTICUATRO);
		qOrdenCompra.addAll(generacionDTE.obtenerOrdenesParaGeneracion(nroCaja, nroSucusal, nroEstado));
		if (qOrdenCompra.size() == 0){
			int nroTransaccion = new OperacionesCaja().cajaVirtual(parametros, false);
			logger.traceInfo("process", "SIN ordenes que procesar: "+nroTransaccion);
		} else {
			logger.initTrace("process", "Se procesaran "+qOrdenCompra.size()+ " ordenes");

			initSlaves();
	
			/*
			 * Se Espera que los esclavos terminen. Cada uno manda un notify al master cuando termina, 
			 * por lo mismo se espera tantos wait como esclavos existan.
			 */
			try {
				sem.acquire(slaves.size());
			} catch (InterruptedException e) {
				logger.traceError("process", "Error(InterruptedException) en process: ", e);
				logger.endTrace("process", "Finalizado", null);
				return;
			}
		}
		logger.endTrace("process", "Finalizado", null);
	}
	
	/**
	 * Levanta todos los threads esclavos
	 */
	private void initSlaves() {
		logger.initTrace("initSlaves", "Integer nroCaja, Integer nroSucusal, Integer nroEstado");

		int numThread = 1;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();
		sem = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
//			EsclavoNotaCredito slave = new EsclavoNotaCredito(idx, this);
			EsclavoNotaCredito slave = new EsclavoNotaCredito();
			slaves.add(slave);
			try {
				sem.acquire();
			} catch (Exception e) {
				logger.traceError("initSlaves", "Error(InterruptedException) en initSlaves(): ", e);
				logger.endTrace("initSlaves", "Finalizado", null);
				return;
			}
			
			slave.start();
		}
		logger.endTrace("initSlaves", "Finalizado", null);
	}

	public ConcurrentLinkedQueue<TramaDTE> getqOrdenCompra() {
		return qOrdenCompra;
	}

	public Semaphore getSem() {
		return sem;
	}

	public Parametros getParametros() {
		return parametros;
	}

}
