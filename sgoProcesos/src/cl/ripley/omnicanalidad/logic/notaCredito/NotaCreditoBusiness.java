package cl.ripley.omnicanalidad.logic.notaCredito;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
//import cl.ripley.omnicanalidad.bean.Anulacion;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.IdentificadorMarketplace;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoNotaCredito;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.NotaCreditoDAO;
//import cl.ripley.omnicanalidad.dao.impl.TransbankDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;

@Service
public class NotaCreditoBusiness {
	
	private static final AriLog logger = new AriLog(NotaCreditoBusiness.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	private Parametros parametros;
	private TramaDTE tramaDTE;
	
	Date fecha;
	Integer tipo;
	
	@Autowired
	private NotaCreditoDAO notaCreditoDAO;
	
	public NotaCreditoBusiness(){
		
	}
	
	public NotaCreditoBusiness(Parametros parametros, TramaDTE tramaDTE){
		setParametros(parametros);
		setTramaDTE(tramaDTE);		
		tipo = Constantes.NRO_CERO;
	}
	
	public boolean updateMkpNotaCredito(TramaDTE trama, Parametros pcv, int estado, int transaccion, String fecha) throws AligareException{
		logger.initTrace("updateMkpNotaCredito", "Parametros: parametros - TramaDTE: trama - Estado: " + estado, new KeyLog("CorrelativoVenta: ", trama.getNotaVenta().getCorrelativoVenta().toString()));
		RetornoNotaCredito retornoNotaCredito = null; 
		String mjeSalida;
		String tipoIdent = trama.getNotaVenta().getIndicadorMkp();
		Timestamp fechaCreacionNV = (trama.getNotaVenta().getFechaCreacion() != null)?trama.getNotaVenta().getFechaCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
		long correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();
		int numeroCaja = Integer.parseInt(pcv.buscaValorPorNombre("NRO_CAJA"));
		if (tipoIdent.equalsIgnoreCase(Constantes.IND_MKP_RIPLEY) || tipoIdent.equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
			List<IdentificadorMarketplace> listaMkp = trama.getIdentificadoresMarketplace();
			List<ArticuloVentaTramaDTE> listaArtVenta = trama.getArticuloVentas();

			if (!listaMkp.isEmpty()&&!listaArtVenta.isEmpty()){
				
				for (IdentificadorMarketplace identificadorMarketplace : listaMkp) {
					String indMkp = identificadorMarketplace.getOrdenMkp();
					for (ArticuloVentaTramaDTE obj : listaArtVenta) {
						
						if (!indMkp.equalsIgnoreCase(obj.getArticuloVenta().getOrdenMkp())){
							continue;
						}
						
						if (obj.getArticuloVenta().getEsNC().intValue()!=Constantes.esNC){
							continue;
						}						
						
						if(identificadorMarketplace.getEsMkp().intValue() == Constantes.NRO_CERO){
							
							retornoNotaCredito = notaCreditoDAO.updateMarketPlace(estado, transaccion, numeroCaja, fechaCreacionNV, correlativoVenta, indMkp);
							mjeSalida = "PL Actualizacion MKP[updateMarketPlace]: " + retornoNotaCredito.getMensaje();
							if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){						
								retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
								retornoNotaCredito.setMensaje(mjeSalida);
								logger.traceError("updateMkpNotaCredito","Imposible actualizar MKP Error - ",new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
								logger.endTrace("updateMkpNotaCredito", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
								return false;
							}
							logger.traceInfo("updateMkpNotaCredito", "Actualizacion MKP en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
						}else{
							continue;
						}
					}
				}
			}
		}
		
		return true;
	}
	 
	public RetornoNotaCredito generar(	Parametros parametros,
										TramaDTE trama,
										Integer transaccion,
										String nroFolio,
										Timestamp fechaTransaccion){
		
		logger.initTrace("generar", "Parametros: parametros - TramaDTE: trama - Integer: " + transaccion + "Timestamp: " + fechaTransaccion, new KeyLog("Correlativo Venta", trama.getNotaVenta().getCorrelativoVenta()+Constantes.VACIO));
//		RetornoNotaCredito retornoNotaCredito = new RetornoNotaCredito();
//		fecha = new Date();
//		String mjeSalida=Constantes.VACIO;
//		String formaPago=Constantes.VACIO;
//		String tipoPagoWebp = Constantes.VACIO;
//		String tipoPago=Constantes.VACIO;
////		int funcion =  Integer.parseInt(parametros.buscaValorPorNombre("FUNCION"));
//		String supervisor = parametros.buscaValorPorNombre("RUT_SUPERVISOR");
//		String vendedor = parametros.buscaValorPorNombre("RUT_VENDEDOR");
//		tipo = Constantes.NRO_ONCE;
//		Integer sucursal = trama.getNotaVenta().getNumeroSucursal();//(Validaciones.validaInteger(trama.getNotaVenta().getNumeroSucursal()))?trama.getNotaVenta().getNumeroSucursal().intValue():Constantes.NRO_CERO;
//		Integer numeroCaja = new Integer(parametros.buscaValorPorNombre("NRO_CAJA_NC"));//trama.getNotaVenta().getNumeroCaja();//(Validaciones.validaInteger(trama.getNotaVenta().getNumeroCaja()))?trama.getNotaVenta().getNumeroCaja().intValue():Constantes.NRO_CERO;
//		Long correlativoVenta = trama.getNotaVenta().getCorrelativoVenta();//(Validaciones.validaInteger(trama.getNotaVenta().getCorrelativoVenta()))?trama.getNotaVenta().getCorrelativoVenta().intValue():Constantes.NRO_CERO;
//		Long nroBoleta = trama.getNotaVenta().getNumeroBoleta();//(Validaciones.validaInteger(trama.getNotaVenta().getNumeroBoleta()))?trama.getNotaVenta().getNumeroBoleta().intValue():Constantes.NRO_CERO;
//		long glTotalNC = notaCreditoDAO.getTotalNc(trama);
//		
//		try {
//			int docOrigen = (Validaciones.validaInteger(tramaDTE.getTipoDoc().getDocOrigen()))?tramaDTE.getTipoDoc().getDocOrigen().intValue():Constantes.NRO_MENOSUNO;
//			int tipoDoc = (Validaciones.validaInteger(tramaDTE.getTipoDoc().getTipoDoc()))?tramaDTE.getTipoDoc().getTipoDoc().intValue():Constantes.NRO_MENOSUNO;
//			Integer tipoDocNC = (docOrigen != tipoDoc)?tipoDoc:null;//Constantes.NRO_CERO;
//			Integer wsTipo = null;//Constantes.NRO_CERO;
//			Integer codAut = null;//Constantes.NRO_CERO;
//			String servicioNCA = Constantes.VACIO;
//			String rutFacturaBo = Constantes.VACIO;
//			
//			if(tipoDocNC == Constantes.NRO_SIETE){
//				servicioNCA = Util.rellenarString(Constantes.VACIO, 181, Constantes.FALSO, Constantes.CHAR_SPACE); 
//				String rutClienteBO = ((Validaciones.validaInteger(trama.getDespacho().getRutCliente()))? trama.getDespacho().getRutCliente().toString() : Constantes.VACIO);
//				String nomClienteBO = (((trama.getDespacho().getNombreCliente() != null)? trama.getDespacho().getNombreCliente() : Constantes.VACIO)+ Constantes.SPACE 
//						+((trama.getDespacho().getApellidoPatCliente() != null)? trama.getDespacho().getApellidoPatCliente() : Constantes.VACIO)
//						+ Constantes.SPACE + ((trama.getDespacho().getApellidoMatCliente() != null)? trama.getDespacho().getApellidoMatCliente() : Constantes.VACIO));
//				nomClienteBO = Util.rellenarString(nomClienteBO, 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49);
//				String dirClienteBO = ((trama.getDespacho().getDireccionCliente() != null)? trama.getDespacho().getDireccionCliente() : Constantes.VACIO);
//				String comunaBO = ((trama.getDatoFacturacion().getComunaFacturaDes() != null)? trama.getDatoFacturacion().getComunaFacturaDes() : Constantes.VACIO);
//				
//				String rutClienteBOFin = Util.rellenarString(rutClienteBO, Constantes.NRO_DIEZ, Constantes.VERDADERO,Constantes.CHAR_CERO);
//				String nomClienteBOFin = Util.rellenarString(Util.cambiaCaracteres(nomClienteBO), 51, Constantes.FALSO,Constantes.CHAR_SPACE).substring(0, 50);
//				String dirClienteBOFin = Util.rellenarString(Util.cambiaCaracteres(dirClienteBO), 90, Constantes.FALSO,Constantes.CHAR_SPACE).substring(0, 89);
//				String comunaBOFin = Util.rellenarString(Util.cambiaCaracteres(comunaBO), 30, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 29);
//
//				servicioNCA = rutClienteBOFin + nomClienteBOFin + dirClienteBOFin + comunaBOFin;
//			}else{
//				servicioNCA = Util.rellenarString(Constantes.VACIO, 252, Constantes.FALSO, Constantes.CHAR_SPACE);
//				
//				rutFacturaBo = ((trama.getDatoFacturacion().getRutFactura() != null)? Util.rellenarString(trama.getDatoFacturacion().getRutFactura().substring(0,
//												(trama.getDatoFacturacion().getRutFactura().length()- Constantes.NRO_UNO)),Constantes.NRO_DIEZ, Constantes.VERDADERO, Constantes.CHAR_CERO):Constantes.STRING_CERO);
//				String razonSocialFacturaBo = Util.rellenarString(
//						Util.cambiaCaracteres((trama.getDatoFacturacion().getRazonSocialFactura() != null)
//								? trama.getDatoFacturacion().getRazonSocialFactura() : Constantes.VACIO),
//						51, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 50);
//				String direccionFacturaBo = Util.rellenarString((((trama.getDatoFacturacion().getDireccionFacturaDes() != null)
//						? trama.getDatoFacturacion().getDireccionFacturaDes() : Constantes.VACIO) + ((trama.getDatoFacturacion().getAddressId() != null)
//								? trama.getDatoFacturacion().getAddressId() : Constantes.VACIO) + ((trama.getDatoFacturacion().getCiudadFacturaDes() != null)
//										? trama.getDatoFacturacion().getCiudadFacturaDes() : Constantes.VACIO)),90, Constantes.FALSO,Constantes.CHAR_SPACE).substring(0, 89);
//				String comunaFacturaBo = Util.rellenarString(
//						Util.cambiaCaracteres((trama.getDatoFacturacion().getComunaFacturaDes() != null)
//								? trama.getDatoFacturacion().getComunaFacturaDes() : Constantes.VACIO),
//						30, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 29);
//				String ciudadFacturaBo = Util.rellenarString(
//						((trama.getDatoFacturacion().getCiudadFacturaDes() != null)
//								? trama.getDatoFacturacion().getCiudadFacturaDes() : Constantes.VACIO),
//						20, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 19);
//				String telefonoFacturaBo = Util.rellenarString(
//						Util.cambiaCaracteres(((trama.getDatoFacturacion().getTelefonoFactura() != null)
//								? trama.getDatoFacturacion().getTelefonoFactura() : Constantes.VACIO)),
//						16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15);
//				String giroFacturaBo = Util.rellenarString(
//						((trama.getDatoFacturacion().getGiroFacturaDes() != null)
//								? trama.getDatoFacturacion().getGiroFacturaDes() : Constantes.VACIO),
//						35, Constantes.VERDADERO, Constantes.CHAR_SPACE);
//
//				servicioNCA = rutFacturaBo + razonSocialFacturaBo + direccionFacturaBo + comunaFacturaBo
//						+ ciudadFacturaBo + telefonoFacturaBo + giroFacturaBo;
//			}
//			
//			Integer rutTransbank = trama.getNotaVenta().getRutComprador();
//			String rutEjecutivoVta = (!Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta())?trama.getNotaVenta().getEjecutivoVta():Constantes.STRING_CERO);
//			
//			// Creacion Tarjeta Bancaria
//			if(Validaciones.validaLong(trama.getTarjetaBancaria().getCorrelativoVenta())){
//				formaPago= Constantes.BOL_FORMA_PAGO_TB;
//				wsTipo = trama.getTarjetaBancaria().getTipoTarjeta();//(Validaciones.validaInteger(trama.getTarjetaBancaria().getTipoTarjeta()))?trama.getTarjetaBancaria().getTipoTarjeta().intValue():Constantes.NRO_CERO;
//				codAut = (trama.getTarjetaBancaria().getCodigoAutorizador()!=null)?Integer.parseInt(trama.getTarjetaBancaria().getCodigoAutorizador()):null;//Constantes.NRO_CERO;
//				tipoPagoWebp = Constantes.TIPO_PAGO_WEBP;
//				String vd = (!Validaciones.validarVacio(trama.getTarjetaBancaria().getVd()))?trama.getTarjetaBancaria().getVd():Constantes.BOL_VALOR_VD_N;
//				
//				if((Constantes.BOL_VALOR_VD_S.equalsIgnoreCase(vd)) 
//					&& !Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())){
//					retornoNotaCredito = notaCreditoDAO.actualizaEfectivoCV(fechaTransaccion, sucursal, numeroCaja,transaccion,glTotalNC,Constantes.NRO_CERO);
//					mjeSalida = "PL BANCARIA[actualizaEfectivoCV-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//				}else if(Constantes.TARJETA_MERCADO_PAGO.equalsIgnoreCase(trama.getTarjetaBancaria().getMedioAcceso())){//nuevo
//					retornoNotaCredito = notaCreditoDAO.insertTransbankCv(fechaTransaccion, sucursal, numeroCaja,transaccion, "MP", "C", rutTransbank, 
//							 Constantes.NRO_CERO, glTotalNC, Constantes.NRO_CERO, Constantes.NRO_CERO, Constantes.NRO_CERO, String.valueOf(codAut), Constantes.STRING_CEROx4);
//					mjeSalida = "PL BANCARIA[insertTransbankCv-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//				}else{
//					retornoNotaCredito = notaCreditoDAO.insertaBancariaBack(fechaTransaccion, sucursal, numeroCaja, transaccion,
//							           wsTipo, glTotalNC, String.valueOf(correlativoVenta));
//					mjeSalida="PL BANCARIA[insertaBancariaBack-"+formaPago+"]: "+ retornoNotaCredito.getMensaje();
//				}
//				
//				if(retornoNotaCredito.getCodigo().intValue() != Constantes.EJEC_SIN_ERRORES){
//					tipo = Constantes.NRO_CERO;
//					// se obtiene medio de pago
////					int tipoDePago = obtieneTipoDePago(trama);
//					//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//					retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//							numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//							supervisor, vendedor);
//					if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//						mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//					}
//*/
//					retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//					retornoNotaCredito.setMensaje(mjeSalida);
//					logger.traceError("generar",
//							"Imposible crear forma de Pago Bancaria Error - ",
//							new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//					logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//					return retornoNotaCredito;
//				}
//				logger.traceInfo("generar", "Creacion NC T.Bancaria en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//			}
//			
//			BigInteger codAutor = (Validaciones.validaBigInteger(trama.getTarjetaRipley().getCodigoAutorizacion()))?trama.getTarjetaRipley().getCodigoAutorizacion():new BigInteger(Constantes.VACIO);
//			BigDecimal montoCompra = trama.getNotaVenta().getMontoVenta();
//			Integer administradora = trama.getTarjetaRipley().getAdministradora();//(Validaciones.validaInteger(trama.getTarjetaRipley().getAdministradora()))?trama.getTarjetaRipley().getAdministradora().intValue():Constantes.NRO_CERO;
//			Integer emisor = trama.getTarjetaRipley().getEmisor();//(Validaciones.validaInteger(trama.getTarjetaRipley().getEmisor()))?trama.getTarjetaRipley().getEmisor().intValue():Constantes.NRO_CERO;
//			Integer tarjeta = trama.getTarjetaRipley().getTarjeta();//(Validaciones.validaInteger(trama.getTarjetaRipley().getTarjeta()))?trama.getTarjetaRipley().getTarjeta().intValue():Constantes.NRO_CERO;
//			Integer rutTitular = trama.getTarjetaRipley().getRutTitular();//(Validaciones.validaInteger(trama.getTarjetaRipley().getRutTitular()))?trama.getTarjetaRipley().getRutTitular().intValue():Constantes.NRO_CERO;
//			Integer rutPoder = trama.getTarjetaRipley().getRutPoder();//(Validaciones.validaInteger(trama.getTarjetaRipley().getRutPoder()))?trama.getTarjetaRipley().getRutPoder().intValue():Constantes.NRO_CERO;
//			Integer plazo = trama.getTarjetaRipley().getPlazo();//(Validaciones.validaInteger(trama.getTarjetaRipley().getPlazo()))?trama.getTarjetaRipley().getPlazo().intValue():Constantes.NRO_CERO;
//			Integer diferido = trama.getTarjetaRipley().getDiferido();//(Validaciones.validaInteger(trama.getTarjetaRipley().getDiferido()))?trama.getTarjetaRipley().getDiferido().intValue():Constantes.NRO_CERO;
//			BigDecimal montoPie = trama.getTarjetaRipley().getMontoPie();//(Validaciones.validaInteger(trama.getTarjetaRipley().getMontoPie()))?trama.getTarjetaRipley().getMontoPie().intValue():Constantes.NRO_CERO;
//			BigDecimal descuentoCar = trama.getTarjetaRipley().getDescuentoCar();//(Validaciones.validaInteger(trama.getTarjetaRipley().getDescuentoCar()))?trama.getTarjetaRipley().getDescuentoCar().intValue():Constantes.NRO_CERO;
//			Integer codigoDescuento = trama.getTarjetaRipley().getCodigoDescuento();//(Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoDescuento()))?trama.getTarjetaRipley().getCodigoDescuento().intValue():Constantes.NRO_CERO;
//			BigInteger pan = (Validaciones.validaBigInteger(trama.getTarjetaRipley().getPan()))?trama.getTarjetaRipley().getPan():null;
//			int wFlagNcTre = Constantes.NRO_UNO;
//			BigDecimal saldoTre= Constantes.NRO_CERO_B, saldoCar=Constantes.NRO_CERO_B, saldoTotal=Constantes.NRO_CERO_B;//, montoTotal=Constantes.NRO_CERO;
//			BigDecimal montoTarRipley = Constantes.NRO_CERO_B;
//			
//			//Creacion tarjeta ripley
//			if(Validaciones.validaLong(trama.getTarjetaRipley().getCorrelativoVenta())){
//				String tipoTar = Constantes.VACIO;
//				formaPago = Constantes.BOL_FORMA_PAGO_CAR;
//				BigDecimal montoTre = new BigDecimal(Constantes.NRO_CERO);
//				BigDecimal montoCar = Validaciones.validaBigDecimal(trama.getTarjetaRipley().getMontoCapital())?trama.getTarjetaRipley().getMontoCapital() : Constantes.NRO_CERO_B;
//				
//				if(Validaciones.validaLong(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())){
//					tipoTar = Constantes.TIPO_TAR_COM;
//					montoTre = (Validaciones.validaBigDecimal(trama.getTarjetaRegaloEmpresa().getMonto()))?trama.getTarjetaRegaloEmpresa().getMonto() : Constantes.NRO_CERO_B;
//				}
//				
//				BigDecimal montoCapital =  (Validaciones.validaBigDecimal(trama.getTarjetaRipley().getMontoCapital())?trama.getTarjetaRipley().getMontoCapital() : Constantes.NRO_CERO_B);
//						
//				if(Constantes.TIPO_TAR_COM.equalsIgnoreCase(tipoTar)){
//					Saldos saldos = new Saldos();
//					saldos = notaCreditoDAO.getSaldos(correlativoVenta);
//					
//					if (saldos != null) {
//						saldoTre   = saldos.getSaldoTRE();	
//						montoTre   = saldos.getSaldoTRE();
//						saldoCar   = saldos.getSaldoCAR();
//						montoCar   =  saldos.getSaldoCAR();
//						saldoTotal =  saldos.getSaldoTotal();
//						//montoTotal =  saldos.getSaldoTotal();
//						
//						if (glTotalNC < montoCapital && glTotalNC < saldoCar){
//							montoTarRipley = glTotalNC;
//							wFlagNcTre = Constantes.NRO_CERO; //no graba en tre
//						}else{
//							montoTarRipley = saldoCar;
//							wFlagNcTre = Constantes.NRO_UNO; //graba en tre
//						}
//					}else{
//						saldoCar = montoCar;
//						saldoTre = montoTre;
//						montoTarRipley = saldoCar;
//						if (montoTre > 0) wFlagNcTre = Constantes.NRO_UNO;
//						else wFlagNcTre = Constantes.NRO_CERO;
//						saldoTotal = (Validaciones.validaInteger(montoCompra))?montoCompra.longValue():Constantes.NRO_CERO;
//					}
//				}else{
//					montoTarRipley = glTotalNC;
//				}
//				
//				this.tipo = Constantes.NRO_QUINCE;
//				
//				if (montoTarRipley > Constantes.NRO_CERO){
//					Integer codigoCore = trama.getTarjetaRipley().getCodigoCore();//(Validaciones.validaInteger(trama.getTarjetaRipley().getCodigoCore()))?trama.getTarjetaRipley().getCodigoCore().intValue():Constantes.NRO_CERO;
//					BigDecimal codPan = null;	
//					BigDecimal tipoCodAuto = null;	
//					if(codigoCore == Constantes.NRO_UNO){
//						codPan = new BigDecimal(Util.rellenarString(pan.toString(), 16, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 15));
//						tipoCodAuto = new BigDecimal(codAutor.toString());
//					}else{
//						codPan = new BigDecimal(pan.toString());
//						tipoCodAuto = new BigDecimal((codAutor.toString() + Constantes.STRING_CEROX2));
//					}
//					retornoNotaCredito = notaCreditoDAO.insertaRipleyBack(fechaTransaccion, sucursal, numeroCaja, transaccion, administradora,emisor, tarjeta, rutTitular, 
//							rutPoder, plazo, diferido, montoTarRipley, montoPie, descuentoCar, codigoDescuento, 
//							tipoCodAuto, Constantes.NRO_CERO, codPan);
//					mjeSalida = "PL RIPLEY[insertaRipleyBack-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//					
//					if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//						tipo = Constantes.NRO_CERO;
//						// se obtiene medio de pago
////						int tipoDePago = obtieneTipoDePago(trama);
//						//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//						retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//								numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//								supervisor, vendedor);
//						if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//							mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//						}
//*/
//						retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoNotaCredito.setMensaje(mjeSalida);
//						logger.traceError("generar",
//								"Imposible crear forma de pago Ripley Error - ",
//								new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//						logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//						return retornoNotaCredito;
//					}
//					logger.traceInfo("generar", "Creacion NC T.Ripley en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//				}
//			}	
//			
//			long montoTarRegalo;//,wMontoTotal;
//			
//			// Creacion TRE
//			if(Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getCorrelativoVenta())){
//				formaPago = (Validaciones.validarVacio(formaPago))?Constantes.BOL_FORMA_PAGO_TRE:Constantes.BOL_FORMA_PAGO_CAR + Constantes.GUION + formaPago;
//				if(Validaciones.validaInteger(trama.getTarjetaRipley().getCorrelativoVenta())){
//					tipo=Constantes.NRO_QUINCE;
//				}else{
//					tipo=Constantes.NRO_ONCE;
//				}
//				
//				Timestamp fechaBoleta = fechaTransaccion;	
//				long montoTarjeta=Constantes.NRO_CERO;
//				wsTipo =  trama.getTarjetaRegaloEmpresa().getTarjeta();//(Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getTarjeta()))?trama.getTarjetaRegaloEmpresa().getTarjeta().intValue():Constantes.NRO_CERO;
//				Integer nroTarjeta = trama.getTarjetaRegaloEmpresa().getNroTarjeta();//(Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getNroTarjeta()))?trama.getTarjetaRegaloEmpresa().getNroTarjeta().intValue():Constantes.NRO_CERO;
//				//int codAdmin = (Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getAdministradora()))?trama.getTarjetaRegaloEmpresa().getAdministradora().intValue():Constantes.NRO_CERO;
//				administradora = trama.getTarjetaRegaloEmpresa().getAdministradora();
//				codAut = trama.getTarjetaRegaloEmpresa().getCodigoAutorizador();//(Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getCodigoAutorizador()))?trama.getTarjetaRegaloEmpresa().getCodigoAutorizador().intValue():Constantes.NRO_CERO;
//				emisor = trama.getTarjetaRegaloEmpresa().getEmisor();//(Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getEmisor()))?trama.getTarjetaRegaloEmpresa().getEmisor().intValue():Constantes.NRO_CERO;
//				tarjeta = trama.getTarjetaRegaloEmpresa().getTarjeta();
//				Integer flag = trama.getTarjetaRegaloEmpresa().getFlag();//(trama.getTarjetaRegaloEmpresa().getFlag()!=null)?trama.getTarjetaRegaloEmpresa().getFlag().intValue():Constantes.NRO_CERO;
//				long rutCliente = (Validaciones.validaInteger(trama.getTarjetaRegaloEmpresa().getRutCliente()))?trama.getTarjetaRegaloEmpresa().getRutCliente().longValue():Constantes.NRO_CERO;
//				rutCliente = Long.parseLong((rutCliente + Constantes.VACIO + Util.getDigitoValidadorRut2(String.valueOf(rutCliente))));
//				long wMontoTre = Constantes.NRO_CERO;
//				if (wFlagNcTre == Constantes.NRO_UNO) {
//					if (tipo == Constantes.NRO_QUINCE){
//						montoTarRegalo = saldoTre;
//						//wMontoTotal = montoTarRipley + saldoTre;
//						if(montoTarRipley<glTotalNC){
//							wMontoTre = glTotalNC - montoTarRipley;
//						}
//					}else{
//						montoTarRegalo = glTotalNC;
//					}
//					
//					long montoEfectivo=Constantes.NRO_CERO;
//					if (wMontoTre > Constantes.NRO_CERO){
//						montoEfectivo=wMontoTre;
//					}else{
//						montoEfectivo=montoTarRegalo;
//					}
//					
//					retornoNotaCredito = notaCreditoDAO.actualizaTarRegaloEmpresa(fechaTransaccion, sucursal, numeroCaja, 
//							transaccion, administradora, emisor, tarjeta, codAut, rutCliente, montoEfectivo, nroTarjeta, flag);
//					
//					mjeSalida = "PL TRE[actualizaTarRegaloEmpresa-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//					
//					if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//						tipo = Constantes.NRO_UNO;
//						// se obtiene medio de pago
////						int tipoDePago = obtieneTipoDePago(trama);
//						//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//						retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//								numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//								supervisor, vendedor);
//						if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//							mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//						}
//*/
//						retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoNotaCredito.setMensaje(mjeSalida);
//						logger.traceError("generar",
//								"Imposible crear forma de Pago TARJETA REGALO EMPRESA Error - ",
//								new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//						logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//						return retornoNotaCredito;
//					}
//					logger.traceInfo("generar", "Creacion TRE BackOffice en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//					
//					if (wMontoTre > Constantes.NRO_CERO){
//						montoTarjeta=wMontoTre;
//					}else{
//						montoTarjeta=montoTarRegalo;
//					}
//					
//					retornoNotaCredito = notaCreditoDAO.fegife_prc_act_sld_nc(nroTarjeta, montoTarjeta, 
//							numeroCaja, correlativoVenta, fechaBoleta);
//					
//					mjeSalida = "PL TRE[fegife_prc_act_sld_nc-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//					
//					if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//						// se obtiene medio de pago
////						int tipoDePago = obtieneTipoDePago(trama);
//						//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//						retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//								numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//								supervisor, vendedor);
//						if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//							mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//						}
//*/
//						retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoNotaCredito.setMensaje(mjeSalida);
//						logger.traceError("generar",
//								"Imposible Actualizar Saldo TARJETA REGALO EMPRESA Error - ",
//								new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//						logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//						return retornoNotaCredito;
//					}
//					logger.traceInfo("generar", "Creacion TRE BackOffice en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//				}
//			}			
//				
//			// Creacion Transaccion
//			//int wsNroItem = 0;
//			if(Validaciones.validaInteger(trama.getNotaVenta().getCorrelativoVenta())){
//				Timestamp fechaCreacionNV = (trama.getNotaVenta().getFechaCreacion() != null)?trama.getNotaVenta().getFechaCreacion():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
//				montoCompra = trama.getNotaVenta().getMontoVenta();
//				if(!Validaciones.validarVacio(rutEjecutivoVta) && !Constantes.STRING_CERO.equals(rutEjecutivoVta)){
//					vendedor = rutEjecutivoVta.substring(0, (rutEjecutivoVta.length()+Constantes.NRO_MENOSUNO));
//				}
//
//				Integer codigoBoFac = new Integer(Constantes.NRO_CERO);
//				Integer codigoBoBol = new Integer(Constantes.NRO_CERO);
//				Integer codigoBo = new Integer(Constantes.NRO_CERO);
//				
//				if(trama.getTipoDoc().getDocOrigen().equals(trama.getTipoDoc().getTipoDoc())){
//					codigoBoFac = trama.getTipoDoc().getCodBo();
//				}else{
//					codigoBoBol = trama.getTipoDoc().getCodBo();
//				}
//				//codigoBo = trama.getTipoDoc().getCodBo();
//				codigoBo = (Validaciones.validaInteger(codigoBoFac) && codigoBoFac.intValue() == Constantes.NRO_UNO)?codigoBoFac:codigoBoBol;
//				Timestamp fechaBoleta = (trama.getNotaVenta().getFechaBoleta() != null)?trama.getNotaVenta().getFechaBoleta():FechaUtil.stringToTimestamp("01/01/1900", Constantes.FECHA_DDMMYYYY4);
//				Integer numeroCajaOriginal = (Validaciones.validaInteger(trama.getNotaVenta().getNumeroCaja()))?trama.getNotaVenta().getNumeroCaja():new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS"));//new Integer(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS"));//(Validaciones.validaInteger(trama.getNotaVenta().getNumeroCaja()))?trama.getNotaVenta().getNumeroCaja().intValue():Constantes.NRO_CERO;
//				Integer numeroBoleta = trama.getNotaVenta().getNumeroBoleta();//(Validaciones.validaInteger(trama.getNotaVenta().getNumeroBoleta()))?trama.getNotaVenta().getNumeroBoleta():Constantes.NRO_CERO;
//				Integer rutComprador = trama.getNotaVenta().getRutComprador();//(Validaciones.validaInteger(trama.getNotaVenta().getRutComprador()))?trama.getNotaVenta().getRutComprador():Constantes.NRO_CERO;
//				Integer montoDescuento = (Validaciones.validaInteger(trama.getNotaVenta().getMontoDescuento()))?trama.getNotaVenta().getMontoDescuento():Constantes.NRO_CERO;
//				Integer correlativoBoleta = trama.getNotaVenta().getCorrelativoBoleta();
//				
//				retornoNotaCredito = notaCreditoDAO.insertaTransaccionBack4(fechaTransaccion, sucursal, numeroCaja, transaccion, transaccion, 
//						tipo, glTotalNC, sucursal, fechaBoleta, numeroCajaOriginal, 
//						numeroBoleta, Constantes.NRO_CERO, Integer.parseInt(supervisor), Integer.parseInt(vendedor), 
//						Constantes.NRO_CERO, rutComprador, montoDescuento, Constantes.NRO_CERO, Constantes.NRO_CERO, fechaTransaccion, Constantes.NRO_CERO, Constantes.NRO_CERO, 
//						correlativoVenta, correlativoBoleta, fechaCreacionNV, new Integer(nroFolio), codigoBo, servicioNCA, Constantes.VACIO); // se cambia nroBoleta por nroFolio
//				
//				mjeSalida = "PL Transaccion NC[insertaTransaccionBack4-"+formaPago+"]: " + retornoNotaCredito.getMensaje();
//				
//				if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//					// se obtiene medio de pago
////					int tipoDePago = obtieneTipoDePago(trama);
//					//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//					retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//							numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//							supervisor, vendedor);
//					if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//						mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//					}
//*/
//					CajaVirtualDAO caja = new CajaVirtualDAOImpl();
//					caja.saltarFolio(parametros, transaccion);
//					
//					retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//					retornoNotaCredito.setMensaje(mjeSalida);
//					logger.traceError("generar",
//							"Imposible crear Encabezado Transaccion Error - ",
//							new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//					logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//					return retornoNotaCredito;
//				}else{
//					logger.traceInfo("generar", "Inserta NC Transaccion BackOffice en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//					
//					int estado=(Validaciones.validaInteger(trama.getNotaVenta().getEstado()) && trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL)
//							?Constantes.NRO_TRES:Constantes.NRO_CUATRO;
//				
//					retornoNotaCredito = notaCreditoDAO.updateNotaVenta(estado, transaccion, new Integer(nroFolio), fechaTransaccion, fechaTransaccion, 
//						numeroCaja, transaccion, Integer.parseInt(vendedor), correlativoVenta);// se cambia nroBoleta por nroFolio
//					
//					mjeSalida = "PL Actualizacion Nota venta final[updateNotaVenta]: " + retornoNotaCredito.getMensaje();
//					
//					if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//						// se obtiene medio de pago
////						int tipoDePago = obtieneTipoDePago(trama);
//						//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//						retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//								numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//								supervisor, vendedor);
//						if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//							mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//						}
//*/
//						retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoNotaCredito.setMensaje(mjeSalida);
//						logger.traceError("generar",
//								"Imposible actualizar Nota Venta Error - ",
//								new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//						logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//						return retornoNotaCredito;
//					}
//					logger.traceInfo("generar", "Actualizacion Nota venta estado final en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//					//se mueve los updates al finalizar los procesos.
////					int tipoIdent = (trama.getNotaVenta().getIndicadorMkp()!=null)?Integer.parseInt(trama.getNotaVenta().getIndicadorMkp()):Constantes.NRO_CERO;
////					if (tipoIdent==Constantes.NRO_DOS){
////						List<IdentificadorMarketplace> listaMkp = trama.getIdentificadoresMarketplace();
////						for (IdentificadorMarketplace identificadorMarketplace : listaMkp) {
////							String indMkp = identificadorMarketplace.getOrdenMkp();
////							if(Validaciones.validaInteger(identificadorMarketplace.getEsMkp()) && identificadorMarketplace.getEsMkp() == Constantes.NRO_CERO){
////								retornoNotaCredito = notaCreditoDAO.updateMarketPlace(estado, transaccion, numeroCaja, fechaCreacionNV, correlativoVenta, indMkp);
////								
////								mjeSalida = "PL Actualizacion MKP[updateMarketPlace]: " + retornoNotaCredito.getMensaje();
////									
////								if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
////									// se obtiene medio de pago
////									int tipoDePago = obtieneTipoDePago(trama);
////									//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
////									retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
////											numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
////											supervisor, vendedor);
////									if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
////										mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
////									}
////									retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
////									retornoNotaCredito.setMensaje(mjeSalida);
////									logger.traceError("generar",
////											"Imposible actualizar MKP Error - ",
////											new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
////									logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
////									return retornoNotaCredito;
////								}
////								logger.traceInfo("generar", "Actualizacion MKP en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
////							}else{
////								continue;
////							}
////						}
////					}
//					
//					if(Validaciones.validaInteger(trama.getDespacho().getCorrelativoVenta())){
//						String direccionDespacho = Util.cambiaCaracteres(trama.getDespacho().getDireccionDespacho());
//						String emailCliete = Util.cambiaCaracteres(trama.getDespacho().getEmailCliente());
//						String nombreInvitado = Util.cambiaCaracteres(trama.getDespacho().getNombreCliente());
//						String apellidoPaterno = Util.cambiaCaracteres((!Validaciones.validarVacio(trama.getDespacho().getApellidoPatCliente()))?trama.getDespacho().getApellidoPatCliente():"Apellido no descrito");
//						String apellidoMaterno = Util.cambiaCaracteres((!Validaciones.validarVacio(trama.getDespacho().getApellidoMatCliente()))?trama.getDespacho().getApellidoMatCliente():"Apellido no descrito");
//						Integer codigoRegalo = (Validaciones.validaInteger(trama.getNotaVenta().getCodigoRegalo()) && trama.getNotaVenta().getCodigoRegalo().intValue() > Constantes.NRO_CERO)
//												?trama.getNotaVenta().getCodigoRegalo():new Integer(Constantes.NRO_CERO);
//						//String venedorDos = ((!Validaciones.validarVacio(vendedor))?vendedor:Constantes.VACIO) + Util.getDigitoValidadorRut2(((!Validaciones.validarVacio(vendedor))?vendedor:Constantes.VACIO));
//						
//						List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
//						if (articuloVentas.size() > Constantes.NRO_CERO){
//							for (int i = Constantes.NRO_CERO;i < articuloVentas.size(); i++){
//								ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
//								
//								if (articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_MKP){
//									continue;
//								}
//								
//								String mensajeTarjeta = Constantes.SPACE;
//								String codDespacho = articuloVentaTrama.getArticuloVenta().getCodDespacho();
//								String costoEnvio=articuloVentaTrama.getArticuloVenta().getDescRipley();
//								String jornadaDespacho = (!Validaciones.validarVacio(articuloVentaTrama.getArticuloVenta().getCodDespacho()))?articuloVentaTrama.getArticuloVenta().getCodDespacho():Constantes.VACIO;
//								String jornadaDespachoBo = Constantes.VACIO;
//								if (Constantes.BOL_JORNADA_AM.equalsIgnoreCase(jornadaDespacho)){
//									jornadaDespachoBo=Constantes.STRING_UNO;
//								}else if(Constantes.BOL_JORNADA_PM.equalsIgnoreCase(jornadaDespacho)){
//									jornadaDespachoBo=Constantes.STRING_DOS;
//								}
//								
//								int tipoRegalo = Constantes.NRO_CERO;
//								if (Constantes.SI.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getEsregalo())){
//									tipoRegalo = Constantes.NRO_UNO;
//									mensajeTarjeta = (!Validaciones.validarVacio(trama.getDespacho().getMensajeTarjeta()))?trama.getDespacho().getMensajeTarjeta():Constantes.VACIO;
//								}
//								
//								String cud = Constantes.VACIO;
//								Integer sucursalDespacho = articuloVentaTrama.getDespacho().getSucursalDespacho();
//								Integer vPrecio = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getPrecio()))?articuloVentaTrama.getArticuloVenta().getPrecio():new Integer(Constantes.NRO_CERO);
//								Integer correlativoItem = articuloVentaTrama.getArticuloVenta().getCorrelativoItem();
//								Integer vCantPrd = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getUnidades()))?articuloVentaTrama.getArticuloVenta().getUnidades():new Integer(Constantes.NRO_CERO);
//								Integer vDesc = (Validaciones.validaInteger(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))?articuloVentaTrama.getArticuloVenta().getMontoDescuento():new Integer(Constantes.NRO_CERO);
//								long codArticulo=Long.parseLong(articuloVentaTrama.getArticuloVenta().getCodArticulo());
//								Integer tipoDescuento = articuloVentaTrama.getArticuloVenta().getTipoDescuento();
//								
//								if ((notaCreditoDAO.tieneCud(articuloVentaTrama.getArticuloVenta().getCodArticulo()) == Constantes.NRO_CERO) && (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() == Constantes.NRO_UNO)){
//									if(codigoRegalo.intValue() == Constantes.NRO_CERO && !Validaciones.validarVacio(articuloVentaTrama.getArticuloVenta().getMensaje())){
//										tipoRegalo = Constantes.NRO_NOVENTANUEVE;
//									}
//									
//									// Creacion de Despacho para Big Tiocket
//									if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) == Constantes.NRO_CERO))
//											|| ((costoEnvio.indexOf(Constantes.TRAMA_EG) == Constantes.NRO_CERO))){
////									if ((Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14))) 
////											|| (Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2)))){
//											cud = Constantes.SPACE;
//									}else{
//										if(codDespacho != null && !Validaciones.validarVacio(codDespacho) && codDespacho.length() < Constantes.NRO_VEINTIDOS){
//											cud = codDespacho.substring(0, 16) + Constantes.STRING_CERO + codDespacho.substring(16, 21);
//										}else{
//											cud = codDespacho.trim();
//										}
//									}
//									
//									retornoNotaCredito = notaCreditoDAO.fNotaCredito(cud, Constantes.STRING_UNO, sucursal, numeroCaja, transaccion, 
//											vendedor, fechaTransaccion, Constantes.NRO_TRES);
//									
//									mjeSalida = "PL Creacion de NCredito[fNotaCredito]: " + retornoNotaCredito.getMensaje();
//										
//									if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//										// se obtiene medio de pago
////										int tipoDePago = obtieneTipoDePago(trama);
//										//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//										retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//												numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//												supervisor, vendedor);
//										if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//											mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//										}
//*/
//										retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//										retornoNotaCredito.setMensaje(mjeSalida);
//										logger.traceError("generar",
//												"Imposible de grabar NCredito en Bticket Error - ",
//												new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//										logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//										return retornoNotaCredito;
//									}
//									logger.traceInfo("generar", "Creacion de NCredito en Bticket en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)), new KeyLog("Correlativo item", String.valueOf(correlativoItem)));
//																		
//									// Creacion REGISTRO PARA NOVIOS
//									if(codigoRegalo > Constantes.NRO_CERO){
//										String vMrc = (tipoRegalo == Constantes.NRO_NOVENTANUEVE|| tipoRegalo == Constantes.NRO_CERO)?"< NR >":"< SR >";
//										long vPrcCmp = vPrecio.longValue() - vDesc.longValue(); //(vPrecio.longValue() * vCantPrd.longValue()) - vDesc.longValue(); //FIXME
//										
//								//merge .22
//										Integer precio = null;
//										// Se calcula precio
//										if (articuloVentaTrama.getArticuloVenta().getMontoDescuento() < 0) {
//											precio = articuloVentaTrama.getArticuloVenta().getPrecio() - articuloVentaTrama.getArticuloVenta().getMontoDescuento();
//										} else {
//											//precio = articuloVentaTrama.getArticuloVenta().getPrecio(); //merge .22
//											precio = (int) (articuloVentaTrama.getArticuloVenta().getPrecio() - Math.floor(articuloVentaTrama.getArticuloVenta().getMontoDescuento() / vCantPrd));
//										}
//										vPrcCmp = precio;// - articuloVentaTrama.getArticuloVenta().getMontoDescuento(); // merge .22
//								//merge .22										
//										
//										String mensajeInvitado=Constantes.VACIO;
//										if (tipoRegalo == Constantes.NRO_NOVENTANUEVE){
//											mensajeInvitado = Constantes.STRING_CERO;
//										}else{
//											mensajeInvitado = mensajeTarjeta;
//										}
//										
//										if ((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO) 
//											&& costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO){
//											
//											retornoNotaCredito = notaCreditoDAO.lglreg_prc_mov_lis_vlr(codigoRegalo.toString(), codArticulo, 
//													(sucursal), fechaTransaccion, transaccion, Constantes.NRO_ONCE, Constantes.NRO_UNO, Constantes.NRO_CERO, vCantPrd, vPrcCmp,
//													transaccion, numeroCaja, tipo, correlativoItem, direccionDespacho, vMrc, null, emailCliete, nombreInvitado, 
//													apellidoPaterno, apellidoMaterno, Constantes.VACIO, Constantes.NRO_CERO,
//													mensajeInvitado, Constantes.NRO_CERO, Constantes.NRO_CERO, Constantes.NRO_CERO, Constantes.NRO_CERO);
//											
//											mjeSalida = "PL novios[lglreg_prc_mov_lis_vlr]: " + retornoNotaCredito.getMensaje();
//												
//											if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//												logger.traceInfo("generar", "Imposible de grabar en Novios Error - Mensaje: " + retornoNotaCredito.getMensaje(), new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//											}
//											logger.traceInfo("generar", "Creacion de REGISTRO NOVIOS en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)), new KeyLog("Correlativo item", String.valueOf(correlativoItem)));
//										}
//									}
//									
//									//Creacion de Despacho para Backoffice
//									if(Validaciones.validaInteger(articuloVentaTrama.getDespacho().getCorrelativoVenta())){
//										//correlativoVenta = articuloVentaTrama.getDespacho().getCorrelativoVenta();	
//										Integer rutClienteDos = articuloVentaTrama.getDespacho().getRutCliente();
//										long rutCliente = Long.parseLong(((Validaciones.validaInteger(rutClienteDos))?rutClienteDos.toString():Constantes.VACIO) + Util.getDigitoValidadorRut2(((Validaciones.validaInteger(rutClienteDos))?rutClienteDos.toString():Constantes.VACIO)));
//										Integer rutDespachoDos = articuloVentaTrama.getDespacho().getRutDespacho();
////										Integer rutDespachoCambio = (Validaciones.validaInteger(rutDespachoDos) && rutDespachoDos.intValue() != Constantes.NRO_CERO)?rutDespachoDos:rutClienteDos;
////										String rutDespacho = ((Validaciones.validaInteger(rutDespachoCambio))?rutDespachoCambio.toString():Constantes.VACIO) + Util.getDigitoValidadorRut2(((Validaciones.validaInteger(rutDespachoCambio))?rutDespachoCambio.toString():Constantes.VACIO));
//										String rutDespacho2 = ((Validaciones.validaInteger(rutDespachoDos))?rutDespachoDos.toString():rutClienteDos.toString());
//										String nombreDespacho = Util.cambiaCaracteres(articuloVentaTrama.getDespacho().getNombreDespacho());
//										Timestamp fechaDespacho = articuloVentaTrama.getDespacho().getFechaDespacho();
//										Integer comunaDespacho = articuloVentaTrama.getDespacho().getComunaDespacho();
//										Integer regionDespacho = articuloVentaTrama.getDespacho().getRegionDespacho();
//										String telefonoDespacho = articuloVentaTrama.getDespacho().getTelefonoDespacho();
//										String observacion = Util.cambiaCaracteres((!Validaciones.validarVacio(articuloVentaTrama.getDespacho().getObservacion()))?articuloVentaTrama.getDespacho().getObservacion():"sin observacion");
//										String direccionCliente = articuloVentaTrama.getDespacho().getDireccionCliente();
//										String telefonoCliente = articuloVentaTrama.getDespacho().getTelefonoCliente();
//										//Integer tipoCliente = articuloVentaTrama.getDespacho().getTipoCliente();
//										String nombreCliente = Util.cambiaCaracteres(Util.rellenarString((articuloVentaTrama.getDespacho().getNombreCliente()+Constantes.SPACE+articuloVentaTrama.getDespacho().getApellidoPatCliente()+Constantes.SPACE+articuloVentaTrama.getDespacho().getApellidoMatCliente()), 50, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 49));
//										//Integer tipoDespacho = articuloVentaTrama.getDespacho().getTipoDespacho();
//										//direccionDespacho = Util.cambiaCaracteres(articuloVentaTrama.getDespacho().getDireccionDespacho());
//										//emailCliete = Util.cambiaCaracteres(articuloVentaTrama.getDespacho().getEmailCliente());
//										//nombreInvitado = Util.cambiaCaracteres(articuloVentaTrama.getDespacho().getNombreCliente());
//										//apellidoPaterno = Util.cambiaCaracteres((!Validaciones.validarVacio(articuloVentaTrama.getDespacho().getApellidoPatCliente()))?articuloVentaTrama.getDespacho().getApellidoPatCliente():"Apellido no descrito");
//										//apellidoMaterno = Util.cambiaCaracteres((!Validaciones.validarVacio(articuloVentaTrama.getDespacho().getApellidoMatCliente()))?articuloVentaTrama.getDespacho().getApellidoMatCliente():"Apellido no descrito");
//										Integer color = articuloVentaTrama.getArticuloVenta().getColor();
//
//										if (((costoEnvio.indexOf(Constantes.TRAMA_COSTO_ENVIO) != Constantes.NRO_CERO))
//												&& ((costoEnvio.indexOf(Constantes.TRAMA_EG) != Constantes.NRO_CERO))){
////										if ((!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14))) 
////												&& (!Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2)))){
//
//											retornoNotaCredito = notaCreditoDAO.insertaDespachoBack(fechaBoleta, sucursal, numeroCaja, transaccion, 
//													cud, color, Constantes.NRO_UNO, Long.parseLong(rutDespacho2), nombreDespacho, fechaDespacho, 
//													sucursalDespacho, comunaDespacho, regionDespacho, direccionDespacho, 
//													telefonoDespacho, Constantes.NRO_CERO, jornadaDespachoBo, observacion, Constantes.NRO_UNO, "A1", 
//													rutCliente, direccionCliente, telefonoCliente, Constantes.NRO_UNO, nombreCliente);
//											
//											mjeSalida = "PL inserta despacho[insertaDespachoBack]: " + retornoNotaCredito.getMensaje();
//												
//											if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//												// se obtiene medio de pago
////												int tipoDePago = obtieneTipoDePago(trama);
//												//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//												retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//														numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//														supervisor, vendedor);
//												if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//													mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//												}
//*/
//												retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//												retornoNotaCredito.setMensaje(mjeSalida);
//												logger.traceError("generar",
//														"Imposible crear registro Despacho en Backoffice Error - ",
//														new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//												logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//												return retornoNotaCredito;
//											}
//											logger.traceInfo("generar", "Creacion Despacho BackOffice en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)), new KeyLog("Correlativo item", String.valueOf(correlativoItem)));
//										}
//									}
//								}
//								
//								if (articuloVentaTrama.getArticuloVenta().getEsNC().intValue() == Constantes.NRO_UNO){
//									
//									//if (((!Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(Util.rellenarString(costoEnvio, 15, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 14))) &&
//										//(!Constantes.TRAMA_EG.equalsIgnoreCase(Util.rellenarString(costoEnvio, 3, Constantes.FALSO, Constantes.CHAR_SPACE).substring(0, 2))) &&
//									//	sucursalDespacho != Constantes.NRO_CERO)){
//										
//										retornoNotaCredito = notaCreditoDAO.insertaArticulosBack(fechaTransaccion, sucursal, numeroCaja, transaccion, // fechaCreacionNV = fechaTransaccion
//												correlativoItem, codArticulo, Integer.parseInt(vendedor), codigoRegalo, Constantes.NRO_CERO, vPrecio, 
//												vCantPrd, vDesc, tipoDescuento, cud, Constantes.NRO_CERO, Constantes.NRO_CINCO);
//										
//										mjeSalida = "PL inserta articulos[insertaArticulosBack]: " + retornoNotaCredito.getMensaje();
//											
//										if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//											// se obtiene medio de pago
////											int tipoDePago = obtieneTipoDePago(trama);
//											//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//											retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//													numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//													supervisor, vendedor);
//											if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//												mjeSalida = mjeSalida + retornoNotaCredito.getMensaje();
//											}
//*/
//											retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//											retornoNotaCredito.setMensaje(mjeSalida);
//											logger.traceError("generar",
//													"Imposible crear registro Articulos en Backoffice Error - ",
//													new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//											logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//											return retornoNotaCredito;
//										}
//										logger.traceInfo("generar", "Creacion de Articulo en BackOffice en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)), new KeyLog("Correlativo item", String.valueOf(correlativoItem)));
//									//}
//									
//									if (!Validaciones.validarVacio(articuloVentaTrama.getArticuloVenta().getCodArticulo())){
//										
//										//retornoNotaCredito = notaCreditoDAO.updateArticuloVenta(nroBoleta, fechaTransaccion, fechaTransaccion, numeroCaja, 
//										retornoNotaCredito = notaCreditoDAO.updateArticuloVenta(transaccion, fechaTransaccion, fechaTransaccion, numeroCaja, 
//												transaccion, Integer.parseInt(vendedor), correlativoVenta, cud, correlativoItem);
//										
//										mjeSalida = "PL actualiza articulo[updateArticuloVenta]: " + retornoNotaCredito.getMensaje();
//											
//										if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//											retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//											retornoNotaCredito.setMensaje(mjeSalida);
//											logger.traceError("generar",
//													"Imposible actualizar articulos en Modelo Extendido Error - ",
//													new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//											logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//											return retornoNotaCredito;
//										}
//										logger.traceInfo("generar", "Actualizacion de articulo en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)), new KeyLog("Correlativo item", String.valueOf(correlativoItem)));
//									}
//								}
//							}
//						}
//					}else{
//						// se obtiene medio de pago
////						int tipoDePago = obtieneTipoDePago(trama);
//						//Se ejecuta anulaci�n de folio, si no logra anular, concatena mensaje.
///*
//						retornoNotaCredito = notaCreditoDAO.anulacionFolio(fechaTransaccion, funcion, sucursal, 
//								numeroCaja, transaccion, nroBoleta, correlativoVenta, tipoDePago, 
//								supervisor, vendedor);
//						if (retornoNotaCredito.getCodigo()!=Constantes.EJEC_SIN_ERRORES){
//							mjeSalida = "No existe Despacho Error - " + retornoNotaCredito.getMensaje();
//						}
//*/
//						retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//						retornoNotaCredito.setMensaje(mjeSalida);
//						logger.traceError("generar", new Exception(retornoNotaCredito.getMensaje()));
//						logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//						return retornoNotaCredito;						
//					}
//				}
//			}	
//					
//			int idCorrNc = notaCreditoDAO.obtieneMaxCorrNC();
//			String tipoPagoCar = Constantes.VACIO;
//			String tipoPagoTre = Constantes.VACIO;
//			tipoPago = Constantes.VACIO;
//			long montoCar = Constantes.NRO_CERO;
//			if (Validaciones.validaInteger(trama.getTarjetaRipley().getMontoCapital())){
//				montoCar = trama.getTarjetaRipley().getMontoCapital().longValue();
//				tipoPagoCar = Constantes.BOL_FORMA_PAGO_CAR;
//			}
//			else{
//				montoCar = Constantes.NRO_CERO;
//				saldoCar = Constantes.NRO_CERO;	
//			}
//			//int wsNroTar = Constantes.NRO_CERO;
//			long montoTre = Constantes.NRO_CERO;
//			MontoNroTarje mt = notaCreditoDAO.obtieneMontoNroTarje(correlativoVenta);
//			//Integer nroTarjeta = new Integer(Constantes.NRO_CERO);
//			if (mt != null){
//				//nroTarjeta = new Integer(mt.getNroTarjeta());
//				montoTre = mt.getMonto();
//				tipoPagoTre = Constantes.BOL_FORMA_PAGO_TRE;
//			}else{
//				montoTre = Constantes.NRO_CERO;
//				saldoTre = Constantes.NRO_CERO;
//				tipoPagoTre = Constantes.VACIO;
//			}					
//							
//			long saldo = Constantes.NRO_CERO;
//			
//			if (Constantes.TIPO_PAGO_WEBP.equalsIgnoreCase(tipoPagoWebp)){
//				Saldos saldos = notaCreditoDAO.getSaldos(correlativoVenta);
//				if (saldos != null){
//					saldoTre = saldos.getSaldoTRE();
//					montoTre = saldos.getSaldoTRE();
//					
//					saldoCar = saldos.getSaldoCAR();
//					montoCar = saldos.getSaldoCAR();
//					
//					saldoTotal = saldos.getSaldoTotal();
//					//montoTotal = saldos.getSaldoTotal();
//				}else{
//					saldoCar = montoCar;
//					saldoTre = montoTre;
//					saldoTotal = (Validaciones.validaInteger(montoCompra))?montoCompra.longValue():Constantes.NRO_CERO;
//				}
//				
//				if (!Constantes.VACIO.equalsIgnoreCase(tipoPagoCar)){
//					if (!Constantes.VACIO.equalsIgnoreCase(tipoPagoTre)){
//						tipoPago = Constantes.BOL_FORMA_PAGO_COM;
//					}else{
//						tipoPago =Constantes.BOL_FORMA_PAGO_CAR;
//					}
//				}else if(!Constantes.VACIO.equalsIgnoreCase(tipoPagoTre)){
//					tipoPago = Constantes.BOL_FORMA_PAGO_TRE;
//				}
//				
//				if (Constantes.BOL_FORMA_PAGO_COM.equalsIgnoreCase(tipoPago)){ //Case "COM"
//					if(trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_TOTAL){
//						if (glTotalNC == (saldoCar + saldoTre)){
//							saldoTotal = saldoTotal - montoCar - saldoTre;
//							saldoCar = Constantes.NRO_CERO;
//							saldoTre = Constantes.NRO_CERO;
//						}
//					}else/*if(trama.getNotaVenta().getEstado().intValue() == Constantes.NOTA_CREDITO_PARCIAL)*/{ 
//						if(glTotalNC > montoCar){
//							saldoTre = saldoTre - glTotalNC - saldoCar;
//							saldoTotal = saldoTotal - glTotalNC;
//							saldoCar = Constantes.NRO_CERO;
//						}else{
//							saldoCar = saldoCar - glTotalNC;
//							saldoTotal = saldoTotal - glTotalNC;
//						}
//					}
//				}else if(Constantes.BOL_FORMA_PAGO_CAR.equalsIgnoreCase(tipoPago)){ //case "CAR"
//					saldo = saldoCar - glTotalNC;
//					saldoCar = saldo;
//				}else if(Constantes.BOL_FORMA_PAGO_TRE.equalsIgnoreCase(tipoPago)){ //case "TRE"
//					if(glTotalNC == montoTre){
//						montoTre = Constantes.NRO_CERO;
//						saldoTre = Constantes.NRO_CERO;
//					}else{
//						saldo = saldoTre-glTotalNC;
//						saldoTre = saldo;
//					}
//				}
//				
//				retornoNotaCredito = notaCreditoDAO.insertIntoCredito(idCorrNc, nroBoleta, fechaTransaccion, correlativoVenta, 
//						tipoPago, montoCompra, glTotalNC, montoCar, saldoCar, montoTre, saldoTre, saldoTotal);
//				
//				mjeSalida = "PL insert Nota Credito[insertIntoCredito]: " + retornoNotaCredito.getMensaje();
//					
//				if(retornoNotaCredito.getCodigo() != Constantes.EJEC_SIN_ERRORES){
//					retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//					retornoNotaCredito.setMensaje(mjeSalida);
//					logger.traceError("generar",
//							"Imposible insertar Nota Credito en Modelo Extendido Error - ",
//							new Exception("Mensaje: " + retornoNotaCredito.getMensaje()));
//					logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//					return retornoNotaCredito;
//				}
//				logger.traceInfo("generar", "Insercion Nota Credito en forma correcta", new KeyLog("Correlativo venta", String.valueOf(correlativoVenta)));
//			}
//		}catch (Exception e) {
//			retornoNotaCredito.setCodigo(Constantes.EJEC_CON_ERRORES);
//			retornoNotaCredito.setMensaje("Error en la ejecucion: "+e.getMessage());
//			logger.traceError("NotaCrecidoBusiness", e);
//			logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
//			return retornoNotaCredito;
//		}
//		retornoNotaCredito.setCodigo(Constantes.NRO_UNO);
//		retornoNotaCredito.setMensaje(Constantes.VACIO);
//		logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: "+retornoNotaCredito.getCodigo());
		logger.endTrace("generar", "Finalizado", "retornoNotaCredito codigo: null");
//		return retornoNotaCredito;
		return null;
	}

	public Parametros getParametros() {
		return parametros;
	}

	public void setParametros(Parametros parametros) {
		this.parametros = parametros;
	}

	public TramaDTE getTramaDTE() {
		return tramaDTE;
	}

	public void setTramaDTE(TramaDTE tramaDTE) {
		this.tramaDTE = tramaDTE;
	}
	
//	private int obtieneTipoDePago(TramaDTE trama){
//		
//		int tipoDePago=Constantes.NRO_ONCE; //Medio de pago es 11 cuando no es tarjeta Ripley
//		if (Validaciones.validaInteger(trama.getTarjetaRipley().getCorrelativoVenta())){
//			tipoDePago = Constantes.NRO_QUINCE;//tarjeta ripley  15
//		}
//		
//		return tipoDePago;
//	}
	
	/*
	 * Metodo de nego
	 */
	public boolean anularTBK(Parametros parametros, TramaDTE trama, int montoAnulado, int notaDeCredito){
//		logger.initTrace("anularTBK", "Parametros de entrada: params, Trama, montoAnulado, ", new KeyLog("CorrelativoVenta: ", trama.getNotaVenta().getCorrelativoVenta().toString()));
//		
		boolean result = false;
//		
//		trama.getTarjetaBancaria().getTipoTarjeta();
//		logger.traceInfo("anularTBK", "TARJETA BANCARIA", new KeyLog("TIPO: ",trama.getTarjetaBancaria().getTipoTarjeta().toString()));
//
//		//      ESTE CODIGO SE UTILIZA CUANDO SE SUBE A PRODUCCION
//		String codigoAutorizacion = trama.getTarjetaBancaria().getCodigoAutorizador();
//		
//		String codigoComercio = ""; 
//		if (Validaciones.validarVacio(trama.getNotaVenta().getEjecutivoVta())){
//			codigoComercio = parametros.buscaValorPorNombre("CODIGO_COMERCIO");
//		} else {
//			codigoComercio = parametros.buscaValorPorNombre("CODIGO_COMERCIO_FONOCOMPRA");
//		}
//		
//		String ordenDeCompra = trama.getNotaVenta().getCorrelativoVenta().toString();
//		BigDecimal montoAutorizado = new BigDecimal(trama.getTarjetaBancaria().getMontoTarjeta());
//		
//		Anulacion anulacion = new Anulacion();
//		
//		TransbankDAO tbkDAO = new TransbankDAOImpl();
//		long time_start, time_end;
//		time_start = System.currentTimeMillis();
//		try {
//			anulacion.setCodigoAutorizacion(codigoAutorizacion);
//			anulacion.setCodigoComercio(Long.valueOf(codigoComercio));
//			anulacion.setMontoAnulado(new BigDecimal(montoAnulado));
//			anulacion.setMontoAutorizado(montoAutorizado);
//			anulacion.setOrdenCompra(ordenDeCompra);
//			anulacion.setNotaDeCredito(notaDeCredito);
//			
//			anulacion.setEstado(Constantes.TBK_ESTADO_INICIAL); 
//			//guardar caso exitoso
//			logger.traceInfo("anularTBK", "NOTA DE CREDITO CREADA PARA OC :  ", new KeyLog("OC:",ordenDeCompra));
//			anulacion.setCodigoError(Constantes.TBK_ESTADO_PENDIENTE);
//			anulacion.setDescripcionError(Constantes.TBK_GLOSA_PENDIENTE);
//			tbkDAO.insertarAnulaTBK(anulacion);
//			time_end = System.currentTimeMillis();
//			logger.traceInfo("anularTBK", "NULLYFY: ", new KeyLog("Tiempo de ejecucion Nullify:  ",(( time_end - time_start ) / 1000) +" segundos"));
//			result = true;
//			
//		} catch (Exception e) {
//			result = false;
//			/*
//			//guardar caso erroneo
//			time_end = System.currentTimeMillis();
//			logger.traceInfo("anularTBK", "NULLYFY: ", new KeyLog("Tiempo de ejecucion Nullify:  ",(( time_end - time_start ) / 1000) +" segundos"));
//			logger.traceInfo("anularTBK", "Error, registrando en BBDD ", new KeyLog("EXCEPTION", e.getCause().toString()));
//			Util util = new Util();
//			ErroresTransbank erroresTBK = new ErroresTransbank();
//			Map<Integer,String> errorMap = erroresTBK.getErrorMap();
//			int codigo = util.formateaCodigoErrorTransbank(e.getCause().toString());
//			anulacion.setCodigoError(String.valueOf(codigo));
//			anulacion.setDescripcionError(errorMap.get(codigo));
//			tbkDAO.insertarAnulaTBK(anulacion);
//			*/
//			logger.traceInfo("anularTBK", "Error en ejecucion de insertarAnulaTBK");
//			logger.traceError("anularTBK", e);
//		}
//		
//		logger.endTrace("anularTBK", "El resultado de la anulacion TBK es: ", String.valueOf(result));
		return result;
		
	}
	
	
}