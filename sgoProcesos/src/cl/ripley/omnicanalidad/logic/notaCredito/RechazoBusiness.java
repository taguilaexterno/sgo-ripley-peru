package cl.ripley.omnicanalidad.logic.notaCredito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.EstadoMirakl;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.RetornoEjecucion;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.MiraklDAO;
import cl.ripley.omnicanalidad.dao.RechazoDAO;
import cl.ripley.omnicanalidad.dao.impl.RechazoDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

@Service
public class RechazoBusiness {

	private static final AriLog logger = new AriLog(RechazoBusiness.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private MiraklDAO miraklDAO;
	
	public boolean rechazoGeneral(TramaDTE trama, Parametros pcv, String fechaHora) throws AligareException{
		logger.initTrace("rechazoGeneral", "Trama");
		boolean rechazoTRE=true, rechazoCAR=true;
		try {
			
			if (trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP) || 
					trama.getNotaVenta().getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_AMBAS)){
				EstadoMirakl estadoMirakl = new EstadoMirakl();
				
				estadoMirakl.setCorrelativoVenta(trama.getNotaVenta().getCorrelativoVenta());
				estadoMirakl.setOrdenMkp(String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));
				estadoMirakl.setApi(Constantes.OR_TRES);
				estadoMirakl.setEstado(Constantes.MKP_PENDIENTE_PETICION);
				estadoMirakl.setFechaEstado(fechaHora);
				estadoMirakl.setUsuarioEstado(pcv.buscaValorPorNombre("USUARIO"));
				
				boolean retorno = miraklDAO.insertarEstadoMKL(estadoMirakl);
				logger.traceInfo("rechazoGeneral", "retorno rechazoGeneral insertarEstadoMKL: "+retorno);
			}

			if (rechazoTarjetaRegaloEmpresa(trama).getCodigo().intValue()!=Constantes.EJEC_SIN_ERRORES){
				rechazoTRE = false;
			}
			
			if (rechazoTarjetaRipley(trama).getCodigo().intValue()!=Constantes.EJEC_SIN_ERRORES){
				rechazoCAR = false;
			}
			
		} catch (Exception e) {
			logger.traceError("rechazoGeneral", e);
		}
		
		logger.endTrace("rechazoGeneral", "Finalizado", "�Se ha ejecutado un rechazo TRE?: "+rechazoTRE+ " �Se ha ejecutado un rechazo CAR?: "+rechazoCAR);
		
		return !(!rechazoTRE && !rechazoCAR);
	}
	
	private RetornoEjecucion rechazoTarjetaRegaloEmpresa(TramaDTE trama ){
		RetornoEjecucion retornoEjecucion=null;
		logger.initTrace("rechazoTarjetaRegaloEmpresa", "Trama: trama");
		try {
			if (trama.getTarjetaRegaloEmpresa().getCorrelativoVenta()!=null){
				RechazoDAO rechazoDAO = new RechazoDAOImpl();
				retornoEjecucion = new RetornoEjecucion();
				//Ejecuci�n de PL en TRE FEGIFE_PRC_INS_TREINT
				retornoEjecucion = rechazoDAO.fegifePrcInsTreint(trama);
				if (retornoEjecucion.getCodigo().intValue()!=Constantes.EJEC_SIN_ERRORES){
					logger.traceInfo("rechazoTarjetaRegaloEmpresa", "Error en respuesta: fegifePrcInsTreint "+retornoEjecucion.getMensaje());
					logger.endTrace("rechazoTarjetaRegaloEmpresa", "Finalizado", "retornoEjecucion: "+retornoEjecucion.toString());
					return retornoEjecucion;
				}
				//Ejecuci�n de PL en TRE FEGIFE_PRC_ACT_SLD_CONF
				retornoEjecucion = rechazoDAO.fegifePrcActSldConf(trama);
				if (retornoEjecucion.getCodigo().intValue()!=Constantes.EJEC_SIN_ERRORES){
					logger.traceInfo("rechazoTarjetaRegaloEmpresa", "Error en respuesta: fegifePrcActSldConf "+retornoEjecucion.getMensaje());
					logger.endTrace("rechazoTarjetaRegaloEmpresa", "Finalizado", "retornoEjecucion: "+retornoEjecucion.toString());
					return retornoEjecucion;
				}
			}
			else{
				retornoEjecucion = new RetornoEjecucion();
				retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
				retornoEjecucion.setMensaje("Sin datos en trama de Tarjeta de Regalo Ripley");
				logger.traceInfo("rechazoTarjetaRegaloEmpresa", "Sin datos en trama de Tarjeta de Regalo Ripley. Cod:" + Constantes.EJEC_CON_ERRORES);
			}
			
		} catch (Exception e) {
			logger.traceError("rechazoTarjetaRegaloEmpresa", e);
			retornoEjecucion = new RetornoEjecucion();
			retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoEjecucion.setMensaje(e.getMessage());
			logger.endTrace("rechazoTarjetaRegaloEmpresa", "Finalizado", "retornoEjecucion: "+retornoEjecucion.toString());
			return retornoEjecucion;
		}
		
		logger.endTrace("rechazoTarjetaRegaloEmpresa", "Finalizado", "retornoEjecucion: "+retornoEjecucion.toString());
		return retornoEjecucion;
	}
	
	private RetornoEjecucion rechazoTarjetaRipley(TramaDTE trama){
		RetornoEjecucion retornoEjecucion=null;
		logger.initTrace("rechazoTarjetaRipley", "Trama: trama");
		if(trama.getTarjetaRipley().getCorrelativoVenta()!=null){
			retornoEjecucion = new RetornoEjecucion();
			retornoEjecucion.setCodigo(Constantes.EJEC_SIN_ERRORES);
			retornoEjecucion.setMensaje("OK! en duro");
			logger.traceInfo("rechazoTarjetaRipley", "OK! en duro. Cod:" + Constantes.EJEC_CON_ERRORES);

		}else{
			retornoEjecucion = new RetornoEjecucion();
			retornoEjecucion.setCodigo(Constantes.EJEC_CON_ERRORES);
			retornoEjecucion.setMensaje("Sin datos en trama de Tarjeta de Regalo Ripley");
			logger.traceInfo("rechazoTarjetaRipley", "Sin datos en trama de Tarjeta de Regalo Ripley. Cod:" + Constantes.EJEC_CON_ERRORES);
		}
		logger.endTrace("rechazoTarjetaRipley", "Finalizado", "retornoEjecucion: "+retornoEjecucion.toString());
		return retornoEjecucion;
	}
}
