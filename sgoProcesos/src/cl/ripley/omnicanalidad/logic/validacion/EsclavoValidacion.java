package cl.ripley.omnicanalidad.logic.validacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.validacion.ConsultaCajasValidacionResponse;
import com.ripley.restservices.model.validacion.ConsultaOcValidacionResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.dao.ValidacionNotaVentaDAO;
import cl.ripley.omnicanalidad.dao.impl.ValidacionNotaVentaDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

/**
 * EsclavoValidacion hilo esclavo que posee las funciones que permite trabajar
 * con validacion de nota venta
 *
 * @author Mauro Sanhueza T
 * @date 04-08-2016
 * @version %I%, %G%
 * @since 1.0
 */
@Service
public class EsclavoValidacion extends Thread implements Runnable {

    private static final AriLog logger = new AriLog(ValidacionNotaVentaDAOImpl.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    ConcurrentLinkedQueue<NotaVenta> qNotaVenta;

    MaestroValidacion master;

    private boolean valReglasAdmin;

    Integer statusXValidaciones;
    Integer statusMPValidaciones;
    Integer statusAdmValidaciones;

    //Rest Services
    @Autowired
    private NotaVentaDAO notaVentaDAO;

    @Autowired
    private ParametrosDAO paramDAO;

    @Autowired
    private ValidacionNotaVentaDAO validacionNotaVentaDao;

    @Autowired
    private Parametros parametrosCaja;

    /**
     * EsclavoValidacion constructor define el nombre del esclavo con el numeral
     * entregado como id.
     *
     * @param id identificador conocido asignado por el maestro.
     * @param pMaster	maestro
     * @param qNotaVenta	cola de notas de venta
     * @param validaInicio	flag que indica si la validacion asociada al hilo es
     * de inicio o termino
     * @param nombreEsclavo	nombre del esclavo
     *
     * @version %I%, %G%
     * @since 1.0
     */
    public EsclavoValidacion(int id,
            MaestroValidacion pMaster,
            ConcurrentLinkedQueue<NotaVenta> qNotaVenta,
            boolean validaInicio,
            String nombreEsclavo) {
        this.setName("ESCLAVO " + id);
        master = pMaster;
        this.qNotaVenta = qNotaVenta;
        validacionNotaVentaDao = new ValidacionNotaVentaDAOImpl();
        valReglasAdmin = validaInicio;
    }

    public EsclavoValidacion() {
        // TODO Auto-generated constructor stub
    }

    /**
     * run metodo principal de ejecucion del hilo
     *
     * @version %I%, %G%
     * @since 1.0
     */
    @Override
    public void run() {
        logger.initTrace("run", "Thread: " + this.getName().toString());
        this.process();
        super.run();

        master.getSem().release();
        logger.endTrace("run", "Finalizado", "Thread: " + this.getName().toString());
    }

    /**
     * process funcion que inicia validacion de notas de venta
     *
     * @version %I%, %G%
     * @since 1.0
     */
    public void process() {
        logger.initTrace("process", "");
        NotaVenta notaVenta = null;
        logger.traceInfo("process", "valReglasAdmin: " + valReglasAdmin);
        if (!valReglasAdmin) {
            //System.out.println("valReglasAdmin " + valReglasAdmin);
            //obtengo un correlativo de venta de la cola	
            while ((notaVenta = this.qNotaVenta.poll()) != null) {
                //ejecutamos xvalidaciones
                logger.traceInfo("process", "ejecutamos xvalidaciones: " + notaVenta.getCorrelativoVenta());
                statusXValidaciones = aplicaXValidaciones(notaVenta.getCorrelativoVenta());
                logger.traceInfo("process", "statusXValidaciones: " + statusXValidaciones.intValue());
                if (statusXValidaciones != null && statusXValidaciones != 0) {
                    logger.traceInfo("process", "ejecutamos validaciones medio pago: " + notaVenta.getCorrelativoVenta());
                    statusMPValidaciones = aplicaValidacionesMedioPago(notaVenta);
                }
            }
        }
        if (valReglasAdmin) {
            //System.out.println("valReglasAdmin " + valReglasAdmin);
            while ((notaVenta = this.qNotaVenta.poll()) != null) {
                logger.traceInfo("process", "ejecutamos validaciones administracion: " + notaVenta.getCorrelativoVenta() + " - Ind MKP: " + notaVenta.getIndicadorMkp());
                //if (notaVenta.getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_REGALO) && notaVenta.getIndicadorMkp().equalsIgnoreCase(Constantes.IND_MKP_MKP)){
                //	logger.traceInfo("process", "CORRELATIVO: "+notaVenta.getCorrelativoVenta()+ " es REGALO, por lo que no se realizan las validaciones de administracion" );
                //} else {
                aplicaValidacionesAdministracion(notaVenta);
                //}

            }
        }
        logger.endTrace("process", "Finalizado", "Thread: " + this.getName().toString());
    }

    /**
     * aplicaXValidaciones funcion que aplica validaciones de forma a nota de
     * venta.
     *
     * @param correlativoVenta	correlativo identificador nota de venta
     *
     * @return Integer exito de ejecucion validaciones de forma
     *
     * @version %I%, %G%
     * @since 1.0
     */
    private Integer aplicaXValidaciones(Long correlativoVenta) {
        logger.initTrace("aplicaXValidaciones", "Integer: correlativoVenta: " + correlativoVenta.intValue());
        Integer valido = null;
        try {
            valido = validacionNotaVentaDao.aplicaXValidacionesOrdenCompraTransito(correlativoVenta, Boolean.FALSE);
        } catch (Exception e) {
            logger.traceError("aplicaXValidaciones", e);
        }
        logger.endTrace("aplicaXValidaciones", "Finalizado", "valido: " + valido);
        return valido;
    }

    /**
     * aplicaValidacionesMedioPago funcion que aplica validaciones de medios de
     * pago a notas de venta
     *
     * @param notaVenta	nota de venta
     *
     * @return Integer exito de ejecucion validaciones de medios de pago
     *
     * @version %I%, %G%
     * @since 1.0
     */
    private Integer aplicaValidacionesMedioPago(NotaVenta notaVenta) {
        logger.initTrace("aplicaValidacionesMedioPago", "NotaVenta: notaVenta");
        Integer valido = null;
        try {
            valido = validacionNotaVentaDao.aplicaValidacionesMedioPago(notaVenta.getCorrelativoVenta(),
                    notaVenta.getRutComprador(),
                    notaVenta.getEmailCliente(),
                    notaVenta.getTelefonoCliente(),
                    notaVenta.getTipoDespacho(),
                    notaVenta.getComunaDespacho(),
                    notaVenta.getRegionDespacho(),
                    notaVenta.getDireccionDespacho(),
                    notaVenta.getTipoDoc());
        } catch (Exception e) {
            logger.traceError("aplicaValidacionesMedioPago", e);
        }
        logger.endTrace("aplicaValidacionesMedioPago", "Finalizado", "valido: " + valido);
        return valido;
    }

    /**
     * aplicaValidacionesAdministracion funcion que aplica validaciones de
     * reglas a notas de venta
     *
     * @param notaVenta	nota de venta
     *
     * @return Integer exito de ejecucion validaciones de forma
     *
     * @version %I%, %G%
     * @since 1.0
     */
    private Integer aplicaValidacionesAdministracion(NotaVenta notaVenta) {
        logger.initTrace("aplicaValidacionesAdministracion", "NotaVenta: notaVenta");
        Integer valido = null;
        try {
            valido = validacionNotaVentaDao.aplicaValidacionesMedioPagoSeguro(notaVenta.getCorrelativoVenta(),
                    notaVenta.getRutComprador(),
                    notaVenta.getEmailCliente(),
                    notaVenta.getTelefonoCliente(),
                    notaVenta.getTipoDespacho(),
                    notaVenta.getIndicadorMkp(),
                    notaVenta.getComunaDespacho(),
                    notaVenta.getRegionDespacho(),
                    notaVenta.getDireccionDespacho(),
                    notaVenta.getTipoDoc());
        } catch (Exception e) {
            logger.traceError("aplicaValidacionesAdministracion", e);
        }
        logger.endTrace("aplicaValidacionesAdministracion", "Finalizado", "valido: " + valido);
        return valido;
    }

    /**
     * Validacion Rest Services Validación de forma Xvalidaciones (ordenes con
     * estados iniciales) Marca las ordenes con una caja en transito para que
     * nada más las procese.
     *
     * @param oc
     * @return
     */
    public GenericResponse ValidacionNotaVentaSinCaja(Long oc) {

        logger.initTrace("ValidacionNotaVentaSinCaja", "NotaVenta: notaVenta");

        GenericResponse genericResponse = new GenericResponse();
        Parametros parametros = new Parametros();
        parametros.setParametros(new ArrayList<Parametro>());
        /**
         * cargaNotasVenta(parametros.buscaParametroPorNombre("NRO_CAJA_TRANSITO").getValor(),
         * parametros.buscaParametroPorNombre("VOLUMEN_OC").getValor(),
         * parametros.buscaParametroPorNombre("ESTADOS_INICIALES_VALIDACION_CAJA").getValor(),
         * Constantes.NRO_CERO);
         */

        paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");

        //Se valida que la orden tenga un estado inicial y si es así se marca con caja en transito
        NotaVenta notaVenta = notaVentaDAO.cargaOrdenesCompraCajaTransitoPorOC(parametros.buscaParametroPorNombre("NRO_CAJA_TRANSITO").getValor(),
                oc,
                parametros.buscaParametroPorNombre("ESTADOS_INICIALES_VALIDACION_CAJA").getValor());

        //Se aplica validación Xvalidaciones solamente
        Integer valido = validacionNotaVentaDao.aplicaXValidacionesOrdenCompraTransito(oc, Boolean.FALSE);

        if (valido.equals(Constantes.VALIDACION_OK)) {
            genericResponse.setCodigo(Constantes.NRO_CERO);
            genericResponse.setMensaje("OK " + notaVenta.getCorrelativoVenta());
        } else {
            genericResponse.setCodigo(Constantes.NRO_MENOSUNO);
            genericResponse.setMensaje("NOK: Error al ejecutar XValidaciones " + notaVenta.getCorrelativoVenta());
            return genericResponse;

        }

        //Se aplica validación Medio Pago
        valido = validacionNotaVentaDao.aplicaValidacionesMedioPago(notaVenta.getCorrelativoVenta(),
                notaVenta.getRutComprador(),
                notaVenta.getEmailCliente(),
                notaVenta.getTelefonoCliente(),
                notaVenta.getTipoDespacho(),
                notaVenta.getComunaDespacho(),
                notaVenta.getRegionDespacho(),
                notaVenta.getDireccionDespacho(),
                notaVenta.getTipoDoc());

        if (valido.equals(Constantes.VALIDACION_OK_2)) {
            genericResponse.setCodigo(Constantes.NRO_CERO);
            genericResponse.setMensaje("OK");
        } else {
            genericResponse.setCodigo(Constantes.NRO_UNO);
            genericResponse.setMensaje("Error al ejecutar AplicaValidacionesMedioPago! " + notaVenta.getCorrelativoVenta());
            return genericResponse;
        }

        logger.traceInfo("ValidacionNotaVentaSinCaja", "BPL SALIDA: " + notaVenta.getCorrelativoVenta());

        logger.endTrace("ValidacionNotaVentaSinCaja", "Retorno", "");
        return genericResponse;
    }

    public GenericResponse ValidacionNotaVentaReglas(Long oc) {

        GenericResponse genericResponse = new GenericResponse();
        Parametros parametros = new Parametros();
        parametros.setParametros(new ArrayList<Parametro>());
        /**
         * cargaNotasVenta(parametros.buscaParametroPorNombre("NRO_CAJA_TRANSITO").getValor(),
         * parametros.buscaParametroPorNombre("VOLUMEN_OC").getValor(),
         * parametros.buscaParametroPorNombre("ESTADOS_INICIALES_VALIDACION_CAJA").getValor(),
         * Constantes.NRO_UNO);
         *
         *
         * pkg PROC_CARGA_NVENTA_VALIDAS
         */

        paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");

        NotaVenta notaVenta = notaVentaDAO.cargarOrdenOC(oc);

        Integer valido = -1;
        if (notaVenta != null) {
            valido = validacionNotaVentaDao.aplicaValidacionesMedioPagoSeguro(notaVenta.getCorrelativoVenta(),
                    notaVenta.getRutComprador(),
                    notaVenta.getEmailCliente(),
                    notaVenta.getTelefonoCliente(),
                    notaVenta.getTipoDespacho(),
                    notaVenta.getIndicadorMkp(),
                    notaVenta.getComunaDespacho(),
                    notaVenta.getRegionDespacho(),
                    notaVenta.getDireccionDespacho(),
                    notaVenta.getTipoDoc());
        }

        if (valido.equals(Constantes.VALIDACION_OK_2)) {
            genericResponse.setCodigo(Constantes.NRO_CERO);
            genericResponse.setMensaje("OK");
        } else {
            genericResponse.setCodigo(Constantes.NRO_UNO);
            genericResponse.setMensaje("NOK: Error al ejecutar aplicaValidacionesMedioPagoSeguro " + oc);
            return genericResponse;
        }

        return genericResponse;
    }

    /**
     * Obtiene lista de OCs por estado y caja. La cantidad de registros está
     * dada por el parámetro cantidadOCs.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-03-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param caja
     * @param cantidadOCs
     * @param estado
     * @return
     */
    public GenericResponse obtenerOCsByEstadoYCaja(Integer caja, Integer cantidadOCs, Integer estado) {
        logger.initTrace("obtenerOCsByEstadoYCaja", "Integer caja: " + String.valueOf(caja) + " - Integer cantidadOCs: " + String.valueOf(cantidadOCs),
                new KeyLog("Caja", String.valueOf(caja)),
                new KeyLog("Cantidad OCs", String.valueOf(cantidadOCs)),
                new KeyLog("Estado", String.valueOf(estado)));

        ConsultaOcValidacionResponse resp = new ConsultaOcValidacionResponse();

        resp.setCodigo(Constantes.NRO_CERO);
        resp.setMensaje("OK");

        resp.setNumerosOCs(notaVentaDAO.obtenerNumerosOCsByEstado(caja, cantidadOCs, estado));

        logger.endTrace("obtenerOCsByEstadoYCaja", "Finalizado OK", String.valueOf(resp));
        return resp;
    }

    /**
     * Obtiene cajas de SGO
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-03-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param sucursal
     * @return
     */
    public GenericResponse obtenerCajas() {
        logger.initTrace("obtenerCajas", "Inicio método");

        ConsultaCajasValidacionResponse resp = new ConsultaCajasValidacionResponse();

        resp.setCodigo(Constantes.NRO_CERO);
        resp.setMensaje("OK");

        List<Integer> cajas = new ArrayList<>();

        Arrays.asList(parametrosCaja.buscaValorPorNombre(Constantes.NROS_CAJAS_BOLETAS).split(Constantes.COMA))
                .forEach(cajaStr -> {
                    cajas.add(Integer.valueOf(cajaStr));
                });

        resp.setCajas(cajas);

        logger.endTrace("obtenerCajas", "Finalizado OK", String.valueOf(resp));
        return resp;
    }

    public GenericResponse asignarCaja(Integer caja, Long oc) {
        logger.initTrace("asignarCaja",
                "Integer caja: " + String.valueOf(caja)
                + " - Long oc: " + String.valueOf(oc),
                new KeyLog("Caja", String.valueOf(caja)),
                new KeyLog("OC", String.valueOf(oc)));

        GenericResponse resp = new GenericResponse();
        resp.setCodigo(Constantes.NRO_CERO);
        resp.setMensaje("OK");

        if (!notaVentaDAO.asignarCaja(caja, oc)) {
            resp.setCodigo(Constantes.NRO_MENOSUNO);
            resp.setMensaje("NOK: No se pudo asignar caja");
        }

        logger.endTrace("obtenerCajas", "Finalizado OK", String.valueOf(resp));
        return resp;
    }

    /**
     * Validacion para RPOS de forma Xvalidaciones (ordenes con estados
     * iniciales)
     *
     * @param oc
     * @param numCaja
     * @return
     */
    public GenericResponse ValidacionRPOS(Long oc) {

        logger.initTrace("ValidacionRPOS", "NotaVenta: notaVenta");

        GenericResponse genericResponse = new GenericResponse();
        //Se aplica validación Xvalidaciones solamente, Flag ira True para RPOS
        Integer valido = validacionNotaVentaDao.aplicaXValidacionesOrdenCompraTransito(oc, Boolean.TRUE);

        if (valido.equals(Constantes.VALIDACION_OK)) {
            genericResponse.setCodigo(Constantes.NRO_UNO);
            genericResponse.setMensaje("OK OC: " + String.valueOf(oc));
        } else {
            genericResponse.setCodigo(Constantes.NRO_MENOSUNO);
            genericResponse.setMensaje("NOK: Error al ejecutar XValidaciones OC: " + String.valueOf(oc));
            return genericResponse;
        }

        logger.traceInfo("ValidacionRPOS", "BPL SALIDA: " + String.valueOf(oc));
        logger.endTrace("ValidacionRPOS", "Retorno", "");
        return genericResponse;
    }

}
