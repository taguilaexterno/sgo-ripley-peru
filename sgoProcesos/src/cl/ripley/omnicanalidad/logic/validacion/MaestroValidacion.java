package cl.ripley.omnicanalidad.logic.validacion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.NotaVenta;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.NotaVentaDAO;
import cl.ripley.omnicanalidad.dao.impl.NotaVentaDAOImpl;
import cl.ripley.omnicanalidad.util.Constantes;

/**
 * MaestroValidacion posee las funciones que 
 * permite trabajar con esclaovs de validacion nota venta
 * 
 * @author Mauro Sanhueza T
 * @date 04-08-2016
 * @version     %I%, %G%
 * @since 1.0  
 */
public class MaestroValidacion {

	private static final AriLog logger = new AriLog(MaestroValidacion.class,"RIP16-004", PlataformaType.JAVA);
	
	private ConcurrentLinkedQueue<NotaVenta> qNotaVentaInicial = null; 
	private ConcurrentLinkedQueue<NotaVenta> qNotaVentaFinal = null; 
	private Semaphore sem; 
	
	private List<EsclavoValidacion> esclavosValidacion;

	Parametros parametros;
	
	private NotaVentaDAO notaVentaDAO;

	/**
	 * MaestroValidacion constructor
	 * 
	 * @param pars 	carga todos los parametros de que usa el flujo de caja virtual
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */
	public MaestroValidacion(Parametros pars){
		//esclavosValidacion = new ArrayList<EsclavoValidacion>();
		qNotaVentaInicial = new ConcurrentLinkedQueue<NotaVenta>();
		qNotaVentaFinal = new ConcurrentLinkedQueue<NotaVenta>();		
		parametros = pars;
		notaVentaDAO = new NotaVentaDAOImpl();
	}

	/**
	 * process es la funcion principal que comienza la validacion de notas de venta
	 * ademas posee el semaforo que espera el termino de cada hilo
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */
	public void process() {

		logger.initTrace("process", "");
		
		cargaNotasVenta(parametros.buscaParametroPorNombre("NRO_CAJA_TRANSITO").getValor(), 
						parametros.buscaParametroPorNombre("VOLUMEN_OC").getValor(),
						parametros.buscaParametroPorNombre("ESTADOS_INICIALES_VALIDACION_CAJA").getValor(),
						Constantes.NRO_CERO);

		initSlavesValidacionNotaVentaSinCaja();		

		try {
			sem.acquire(esclavosValidacion.size());
		} catch (InterruptedException e) {
			logger.traceError("process", e);
		}
		
		cargaNotasVenta(parametros.buscaParametroPorNombre("NRO_CAJA_TRANSITO").getValor(), 
						parametros.buscaParametroPorNombre("VOLUMEN_OC").getValor(),
						parametros.buscaParametroPorNombre("ESTADOS_INICIALES_VALIDACION_CAJA").getValor(),
						Constantes.NRO_UNO);
		
		initSlavesValidacionNotaVentaReglas();

		try {
			sem.acquire(esclavosValidacion.size());
		} catch (InterruptedException e) {
			logger.traceError("process", e);
		}
		logger.endTrace("process", "Terminado", "");
	}

	/**
	 * initSlavesValidacionNotaVentaSinCaja es la funcion ejecutas los hilos que generan
	 * la validacion de notas venta inicial sin una caja asignada.
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */	
	private void initSlavesValidacionNotaVentaSinCaja() {
		
		logger.initTrace("initSlavesValidacionNotaVentaSinCaja", "");
		esclavosValidacion = new ArrayList<EsclavoValidacion>();
		
		int numThread = 1;
		//indicador para flujo de validacion
		boolean validaInicio = false;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();//Integer.valueOf("1").intValue(); 
		sem = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
			EsclavoValidacion esclavoValidacion = new EsclavoValidacion(idx, this, qNotaVentaInicial,validaInicio, "SLAVE VAL INI");
			esclavosValidacion.add(esclavoValidacion);
			try {
				sem.acquire();
			} catch (InterruptedException e) {
				logger.traceError("initSlavesValidacionNotaVentaSinCaja", e);
			}
			esclavoValidacion.start();
		}
		logger.endTrace("initSlavesValidacionNotaVentaSinCaja", "Terminado", "");
	}

	/**
	 * initSlavesValidacionNotaVentaReglas es la funcion que ejecuta los hilos que generan
	 * la validacion de notas venta final con una caja asignada.
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */	
	private void initSlavesValidacionNotaVentaReglas() {
		
		logger.initTrace("initSlavesValidacionNotaVentaReglas", "");
		esclavosValidacion = new ArrayList<EsclavoValidacion>();
		
		int numThread = 1;
		//indicador para flujo de validacion
		boolean validaInicio = true;

		numThread = Integer.valueOf(parametros.buscaParametroPorNombre("HILOS_GENERACION_BOLETA").getValor()).intValue();//Integer.valueOf("1").intValue(); 
		sem = new Semaphore(numThread);
	
		for (int idx = 0; idx < numThread; idx++) {
			EsclavoValidacion esclavoValidacion = new EsclavoValidacion(idx, this, qNotaVentaFinal,validaInicio, "SLAVE MP");
			esclavosValidacion.add(esclavoValidacion);
			try {
				sem.acquire();
			} catch (InterruptedException e) {
			    logger.traceError("initSlavesValidacionNotaVentaReglas", e);
			}
			esclavoValidacion.start();
		}
		logger.endTrace("initSlavesValidacionNotaVentaReglas", "Terminado", "");
	}	

	/**
	 * cargaNotasVenta es la funcion que permite cargas todas las notas 
	 * de venta que seran seleccionadas desde modelo extendido. Se les asiganara una caja 
	 * para que queden invisibles a cualquier otro proceso que cambie su estado mediante 
	 * el uso de esta
	 * 
	 * @param nroCajaTransito 	numero que asignaremos a notas de venta para marcar 
	 * 							en proceso principal de validacion
	 * @param volumen			cantida de notas de venta que cargamos
	 * @param estadosIniciales	estados para filtrar notas de venta sin caja, estos
	 * 							estados son lo que no llega de la caja virtual
	 * 
	 * @version     %I%, %G%
	 * @since 1.0  
	 */
	public void cargaNotasVenta(String nroCajaTransito, String volumen, String estadosIniciales, int cola){
		
		logger.initTrace("cargaNotasVenta", "");
		try{
			if (cola == Constantes.NRO_CERO){
				List<NotaVenta> lista = notaVentaDAO.cargaOrdenesCompraCajaTransito(nroCajaTransito,volumen,estadosIniciales);
				logger.traceInfo("cargaNotasVenta", "List<NotaVenta> lista notaVentaDAO.cargaOrdenesCompraCajaTransito .size: "+lista.size());
				if(lista != null && lista.size() > 0){
					for(NotaVenta oc : lista){
						qNotaVentaInicial.add(oc);
					}
				}
			}
			
			if (cola == Constantes.NRO_UNO){
				List<NotaVenta> lista2 = notaVentaDAO.cargaOrdenesCompraCajaTransitoSeguras(nroCajaTransito,volumen);
				logger.traceInfo("cargaNotasVenta", "List<NotaVenta> lista2 notaVentaDAO.cargaOrdenesCompraCajaTransitoSeguras .size: "+lista2.size());
				if(lista2 != null && lista2.size() > 0){
					for(NotaVenta oc : lista2){
						qNotaVentaFinal.add(oc);
					}
				}
			}
		}catch(Exception e){
			logger.traceError("cargaNotasVenta", e);
		}
		logger.endTrace("cargaNotasVenta", "Terminado", "qNotaVentaInicial: "+qNotaVentaInicial.size() + " qNotaVentaFinal: "+qNotaVentaFinal.size());
	}

	public Semaphore getSem() {
		return sem;
	}
	
}
