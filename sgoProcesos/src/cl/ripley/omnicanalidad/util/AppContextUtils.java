package cl.ripley.omnicanalidad.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class AppContextUtils implements ApplicationContextAware {
	
	private static ApplicationContext ctx;

	public static ApplicationContext getApplicationContext() {
		
		return ctx;
	}

	@Override
	@Autowired
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;
		
	}

}
