package cl.ripley.omnicanalidad.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;

/**Clase que se utiliza como generador de cliente SMTP.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class ClienteSMTP {
	
	private static final AriLog logger = new AriLog(ClienteSMTP.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	public ClienteSMTP(){
		
	}
	
	/**Reemplaza los placeholder del HTML por los valores reales.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param html
	 * @param valores
	 * @param lista
	 * @return
	 */
	public String completarHTML(String html, HashMap<String, String> valores, HashMap<String, String> lista){
		logger.initTrace("completarHTML", "String: html"+" - HashMap<String, String>: "+((valores != null)?valores.size():Constantes.NRO_CERO));
		try {
			if(Validaciones.validarVacio(html)){
				logger.endTrace("completarHTML", "No cumple validacion de html(vacio). Finalizado", "String: null");
				return null;
			}
			if(valores == null || valores.isEmpty()){
				logger.endTrace("completarHTML", "No cumple validacion de valores(vacio). Finalizado", "String: null");
				return null;
			}
			
			Set<String> keySet = valores.keySet();
		    for (String key : keySet) {
				if (valores.get(key) != null){
				   html = html.replace("[" + key + "]", valores.get(key));
				} else {
				   logger.traceInfo("completarHTML", "Key: ["+key+"] no encontrado");
				   html = html.replace("[" + key + "]", " ");
				}
			}
		    
		    int inicioLista = html.indexOf("<lista>");
		    if(inicioLista > Constantes.NRO_MENOSUNO){
		    	StringBuilder sbLista = new StringBuilder();
		    	int finInicioLista = inicioLista + "<lista>".length();
		    	int iniFinLista = html.indexOf("</lista>");
		    	int finLista = iniFinLista+"</lista>".length();
		    	String htmlPart1 = html.substring(Constantes.NRO_CERO, inicioLista);
		    	String htmlPart2 = html.substring(finLista);
		    	String htmlLista = html.substring(finInicioLista, iniFinLista);
			    if(lista != null && !lista.isEmpty()){
			    	int idxIni = Constantes.NRO_CERO;
			    	int idxFin = Constantes.NRO_CERO;
			    	int idxf = htmlLista.length();
			    	int val = Constantes.NRO_CERO;
			    	List<String> tokens = new ArrayList<String>();
			    	String cadena = htmlLista;
			    	while(val > Constantes.NRO_MENOSUNO){
			    		idxIni = cadena.substring(Constantes.NRO_CERO, idxf).indexOf("[");
			    		idxFin = cadena.substring(Constantes.NRO_CERO, idxf).indexOf("]");
			    		if(idxIni == Constantes.NRO_MENOSUNO || idxFin == Constantes.NRO_MENOSUNO){
			    			val = Constantes.NRO_MENOSUNO;
			    		}else{
			    			 tokens.add(cadena.substring(idxIni+Constantes.NRO_UNO, idxFin));
			    			 cadena = cadena.substring(idxFin+Constantes.NRO_UNO);
			    			 idxf = cadena.length();
			    		}
			    	}
			    	if(!tokens.isEmpty()){
			    		int cantRegistros = lista.size()/tokens.size();
			    		for (int i = Constantes.NRO_CERO; i < cantRegistros; i++) {
							String newHtmlLista = htmlLista;
							for (int k = Constantes.NRO_CERO; k < tokens.size(); k++) {
								newHtmlLista = newHtmlLista.replace("[" + tokens.get(k) + "]", "[" + tokens.get(k) + (i+Constantes.NRO_UNO) + "]");
							}
							sbLista.append(newHtmlLista+System.getProperty("line.separator"));
						}
			    		Set<String> keySet2 = lista.keySet();
			    		cadena = sbLista.toString();
			    	    for (String key : keySet2) {
			    			if (lista.get(key) != null){
			    				cadena = cadena.replace("[" + key + "]", lista.get(key));
			    			} else {
			    				logger.traceInfo("completarHTML", "Key: ["+key+"] no encontrado");
			    				cadena = cadena.replace("[" + key + "]", " ");
			    			}
			    		}
			    	    html = htmlPart1+cadena+htmlPart2;
			    	}else{
			    		sbLista.append(System.getProperty("line.separator"));
			    		html = htmlPart1+sbLista.toString()+htmlPart2;
			    	}
			    }else{
			    	sbLista.append(System.getProperty("line.separator"));
			    	html = htmlPart1+sbLista.toString()+htmlPart2;
			    }
		    }
		} catch (Exception e){  
			logger.traceError("completarHTML", "Ha ocurrido un error al reemplazar las etiquetas.", e);
			throw new AligareException("ERROR");
		}
		logger.endTrace("completarHTML", "Finalizado", "String: html");
		return html;
	}

	/**Prepara el correo y lo envía al destinatario.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param url
	 * @param puerto
	 * @param asunto
	 * @param cuerpo
	 * @param remitente
	 * @param destinatario
	 * @param grupo
	 * @return
	 */
	public boolean enviarCorreo(String url, int puerto, String asunto, String cuerpo, String remitente, String destinatario, String grupo) {
		logger.initTrace("enviarCorreo", "String: "+url+" - int: "+puerto+" - String: "+asunto+" - String: html"+" - String: "+remitente+" - String: "+destinatario, new KeyLog("remitente", remitente), new KeyLog("destinatario", destinatario), new KeyLog("asunto", asunto));
        if(Validaciones.validarVacio(url)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de url(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(asunto)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de asunto(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(cuerpo)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de cuerpo(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(remitente)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de remitente(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(remitente)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de remitente(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(destinatario)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de destinatario(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(destinatario)){
        	logger.endTrace("enviarCorreo", "No cumple validacion de destinatario(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        
		Properties properties = new Properties();  
        Session session;
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.debug", true);
        properties.put("mail.smtp.host", url);
        properties.put("mail.smtp.port", puerto);
        properties.put("mail.smtp.auth", "false");
  
        session = Session.getDefaultInstance(properties); 
        //session.setDebug(true);
        Transport t =null;
        try{  
            MimeMessage message = new MimeMessage(session);  
            message.setHeader("X-SP-Transact-Id", grupo);
            message.setFrom(new InternetAddress(remitente));
            InternetAddress address = new InternetAddress(destinatario);
            message.setRecipient(Message.RecipientType.TO, address);
            message.setSubject(asunto);
            message.setText(cuerpo, "ISO-8859-1", "html");
            t = session.getTransport("smtp");  
            t.connect();
            t.sendMessage(message, message.getAllRecipients());  
        }catch (MessagingException me){  
         logger.traceError("enviarCorreo", "Error(MessagingException) en enviarCorreo(): ", me);
        	if (t.isConnected()) {
        		try {
				 t.close();
				 } catch (MessagingException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
				 }
        	}
        	return false;
        }finally {
        	if (t.isConnected()) {
        		try {
        			t.close();
				 } catch (MessagingException e) {
				 // TODO Auto-generated catch block
					 e.printStackTrace();
				 }
        	}
        }
        logger.endTrace("enviarCorreo", "Finalizado", "boolean: true");
        return true;
    }
	
	/**Envía el correo al destinatario con un archivo adjunto.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param url
	 * @param puerto
	 * @param asunto
	 * @param cuerpo
	 * @param remitente
	 * @param destinatario
	 * @param conCopia
	 * @param adjuntos
	 * @return
	 */
	public boolean enviarCorreoConAdjunto(String url, int puerto, String asunto, String cuerpo, String remitente, String destinatario, String conCopia, List<File> adjuntos) {
		logger.initTrace("enviarCorreoConAdjunto", "String: "+url+" - int: "+puerto+" - String: "+asunto+" - String: html"+" - String: "+remitente+" - String: "+destinatario+" - Nro_adjuntos: "+((adjuntos != null)?adjuntos.size():0), new KeyLog("remitente", remitente), new KeyLog("destinatario", destinatario), new KeyLog("asunto", asunto));
		if(Validaciones.validarVacio(url)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de url(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(asunto)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de asunto(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(cuerpo)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de cuerpo(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(remitente)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de remitente(vacio). Finalizado", "boolean: false");
        	return false;
        }
        if(!Validaciones.validarEmail(remitente)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de remitente(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        if(Validaciones.validarVacio(destinatario)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de destinatario(vacio). Finalizado", "boolean: false");
        	return false;
        }
        /*
        if(!Validaciones.validarEmail(destinatario)){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de destinatario(formato invalido). Finalizado", "boolean: false");
        	return false;
        }
        */
        if(adjuntos == null || adjuntos.isEmpty()){
        	logger.endTrace("enviarCorreoConAdjunto", "No cumple validacion de adjuntos(vacio). Finalizado", "boolean: false");
        	return false;
        }
                
        Properties properties = new Properties();  
        Session session;
        
        properties.put("mail.smtp.host", url);
        properties.put("mail.smtp.starttls.enable", "true");  
        properties.put("mail.smtp.port", puerto);
        properties.put("mail.smtp.mail.sender", remitente);
        properties.put("mail.smtp.auth", "false");
        properties.put("mail.smtp.timeout", String.valueOf(TimeUnit.SECONDS.toMillis(Constantes.NRO_TREINTA)));
        properties.put("mail.smtp.connectiontimeout", String.valueOf(TimeUnit.SECONDS.toMillis(Constantes.NRO_TREINTA)));
        
  
        session = Session.getDefaultInstance(properties); 
        
        try{  
        	MimeMultipart mp = new MimeMultipart("related");
        	BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(cuerpo, "text/html; charset=utf-8");
            mp.addBodyPart(messageBodyPart);
            
        	for (File file : adjuntos) {
            	BodyPart messageBodyPart2 = new MimeBodyPart();
                DataSource ds = new FileDataSource(file);
                messageBodyPart2.setDataHandler(new DataHandler(ds));
                messageBodyPart2.setFileName(file.getName());
                messageBodyPart2.setDisposition(Part.ATTACHMENT);
                mp.addBodyPart(messageBodyPart2);
			}
        	

            MimeMessage message = new MimeMessage(session);  
            message.setFrom(new InternetAddress((String)properties.get("mail.smtp.mail.sender")));
            
            if (destinatario != null){
            	String[] destinatarioArr = StringUtils.split(destinatario,";");
            	for (int s = 0; s < destinatarioArr.length; s++){
	        		if(Validaciones.validarEmail(destinatarioArr[s].trim())){
	        			message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatarioArr[s].trim()));
	        		}
	        	}
                //message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            }
            
	        if (conCopia != null){
	        	String[] conCopiaArr = StringUtils.split(conCopia,";");
	        	for (int s = 0; s < conCopiaArr.length; s++){
	        		if(Validaciones.validarEmail(conCopiaArr[s].trim())){
	        			message.addRecipient(Message.RecipientType.CC, new InternetAddress(conCopiaArr[s].trim()));
	        		}
	        	}
	        	//message.addRecipient(Message.RecipientType.CC, new InternetAddress(conCopia));
	        }
	        
	        message.addRecipient(Message.RecipientType.BCC, new InternetAddress("agarcia@ripley.com"));
	        //message.addRecipient(Message.RecipientType.BCC, new InternetAddress("SoporteTV7@ripley.cl"));
            message.setSubject(asunto);  
            message.setContent(mp);
            Transport t = session.getTransport("smtp");  
            t.connect();
            t.sendMessage(message, message.getAllRecipients());  
            t.close();  
        }catch (MessagingException me){  
        	logger.traceError("enviarCorreoConAdjunto", "Error(MessagingException) en enviarCorreoConAdjunto(): ", me);
        	return false;
        }
        logger.endTrace("enviarCorreoConAdjunto", "Finalizado", "boolean: true");
		return true;
	}
}
