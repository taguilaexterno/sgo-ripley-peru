package cl.ripley.omnicanalidad.util;

public class CodErrorSystemExit {
	public static final int SALIDA_OK					= 0;
	public static final int ERROR_CONTROLADO			= 1;
	public static final int CAJA_NO_DISPONIBLE			= 2;
	public static final int ERROR_BD_CAIDA				= 3;
	public static final int APERTURA_CIERRE				= 4;
	public static final int FILE_SYSTEM_FULL			= 5;
	public static final int CVIRTUAL_APAGADO			= 0;

}
