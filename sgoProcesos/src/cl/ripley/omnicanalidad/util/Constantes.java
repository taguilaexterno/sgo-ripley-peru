package cl.ripley.omnicanalidad.util;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

public class Constantes {

    public static final String CODIGO_APLICACION = "SGO CORP REST SERVICES";
    public static final String VERSION_SGO_REST = "";
    public final static int FUNCION_NC = 1;
    public final static int FUNCION_BOLETA = 2;
    public final static int PARAM_OMNICANALIDAD = 1;
    public final static int FUNCION_VALIDACION = 3;
    public final static int FUNCION_EMAIL = 4;
    public final static int FUNCION_REPROCESO_TBK = 5;
    public final static int NRO_BOLETA = 0;
    public final static int EJEC_SIN_ERRORES = 0;
    public final static int EJEC_CON_ERRORES = -1;
    public final static String FECHA_DDMMYYYY_HHMMSS = "DD/MM/YYYY HH24:MI:SS";
    public final static String FECHA_DDMMYYYY_HHMMSS_G = "DD-MM-YYYY HH24:MI:SS";
    public final static String FECHA_DDMMYYYY_HHMMSS2 = "DD/MM/YYYY hh24:mi:ss";
    public final static String FECHA_DDMMYYYY_HHMMSS3 = "dd/MM/yyyy HH:mm:ss";
    public final static String FECHA_DDMMYYYY_HHMMSSSSS = "dd/MM/yy HH:mm:ss,SSSSSSSSS";
    public final static String FECHA_DDMMYYYY = "DD/MM/YYYY";
    public final static String FECHA_DDMMYYYY4 = "dd/MM/yyyy";
    public final static String FECHA_YYYY_MM_DD = "YYYY-MM-DD";
    public final static String FECHA_YYYY_MM_DD_T_HH24MISS = "YYYY-MM-DD\"T\"HH24:MI:SS";
    public final static String FECHA_YYYYMMDD = "yyyy-MM-dd";
    public final static String FECHA_YYYYMMDD2 = "YYYYMMDD";
    public final static String FECHA_YYYYMMDD3 = "yyyyMMdd";
    public final static String FECHA_DDMMYYYY2 = "DD-MM-YYYY";
    public final static String FECHA_DDMMYYYY3 = "dd-MM-yyyy";
    public final static String FECHA_DDMMYY3 = "dd-MM-yy";
    public final static String FECHA_ARCHIVO = "ddmmyyyy";
    public final static String FECHA_TIMESTAMP = "yyyy-mm-dd hh:mm:ss.fffffffff";
    public final static String FECHA_HH24MISS = "HH24:MI:SS";
    public final static String HORA_HH24MISS = "HH24MISS";
    public final static String FECHA_DDMMYY = "DDMMYY";
    public final static String FECHA_YYMMDD_HHMISS = "YYMMDD HHMISS";
    public final static String FECHA_DD_MM_YY = "DD/MM/YY";
    public final static String YYYYMMYY_HHMI = "YYYYMMDD HH24MI";
    public final static String FECHA_DDMMYY2 = "ddMMyy";
    public final static String FECHA_YYMMDD = "yyMMdd";
    public final static int ESTADO_ACTIVO = 1;
    public final static int ESTADO_INACTIVO = 0;
    public final static int TIPO_LLAMADO_APERTURA = 1;
    public final static int TIPO_LLAMADO_CIERRE = 2;

    public final static String SIN_ERRORES = "Sin Errores";
    public final static String ERROR = "ERROR";
    public final static String ERROR_MKP = "ERRORMKP";
    public final static String MAIL = "Mail";
    public final static String IMPRESION = "Impresion";
    public final static String RR = "RR";	// recaudacion
    public final static String RM = "RM";	// marketplace
    public final static String ANRR = "ANRR";	// anulacion marketplace
    public final static String ANRM = "ANRM";	// anulacion marketplace

    public final static String NOMBRE_ARCHIVO_CIERRE_BOL = "Cierre_Bol_";
    public final static String NOMBRE_ARCHIVO_CIERRE_NC = "Cierre_Nc_";
    public final static String EXTENSION_CIERRE = ".xls";
    public static String RUTA_LOG = "/";
    public static String RUTA_INFORMES = "/var/tmp/";

    public final static String DESPACHO_DOMICILIO = "SH,S,AM,PM,D";

    public final static int CIERRE_POR_HORARIO = 1;
    public final static int CIERRE_POR_VOLUMEN = 2;
    public final static int NOTA_CREDITO_PARCIAL = 24;
    public final static int NOTA_CREDITO_TOTAL = 23;

    //NUMEROS
    public final static int NRO_MENOSNOVENTAOCHO = -98;
    public final static int NRO_MENOSDOCE = -12;
    public final static int NRO_MENOSTRES = -3;
    public final static int NRO_MENOSDOS = -2;
    public final static int NRO_MENOSUNO = -1;
    public final static int NRO_CERO = 0;
    public final static int NRO_UNO = 1;
    public final static int NRO_DOS = 2;
    public final static int NRO_TRES = 3;
    public final static int NRO_CUATRO = 4;
    public final static int NRO_CINCO = 5;
    public final static int NRO_SEIS = 6;
    public final static int NRO_SIETE = 7;
    public final static int NRO_OCHO = 8;
    public final static int NRO_NUEVE = 9;
    public final static int NRO_DIEZ = 10;
    public final static int NRO_ONCE = 11;
    public final static int NRO_DOCE = 12;
    public final static int NRO_TRECE = 13;
    public final static int NRO_CATORCE = 14;
    public final static int NRO_QUINCE = 15;
    public final static int NRO_DIECINUEVE = 19;
    public final static int NRO_VEINTE = 20;
    public final static int NRO_VEINTIUNO = 21;
    public final static int NRO_VEINTIDOS = 22;
    public final static int NRO_VEINTITRES = 23;
    public final static int NRO_VEINTICUATRO = 24;
    public final static int NRO_VEINTISIETE = 27;
    public final static int NRO_VEINTINUEVE = 29;
    public final static int NRO_TREINTA = 30;
    public final static int NRO_TREINTADOS = 32;
    public final static int NRO_TREINTATRES = 33;
    public final static int NRO_TREINTACUATRO = 34;
    public final static int NRO_TREINTASIETE = 37;
    public final static int NRO_TREINTANUEVE = 39;
    public final static int NRO_CINCUENTA = 50;
    public final static int NRO_CINCUENTAYSEIS = 56;
    public final static int NRO_SESENTAUNO = 61;
    public final static int NRO_NOVENTASEIS = 96;
    public final static int NRO_NOVENTANUEVE = 99;
    public final static int NRO_CIEN = 100;
    public final static int NRO_CIENVEINTECUATRO = 124;
    public final static int NRO_CIENVEINTESIETE = 127;
    public final static int NRO_CIENOCHENTA = 180;
    public final static int NRO_MIL = 1000;

    public final static char CHAR_CERO = '0';
    public final static char CHAR_SPACE = ' ';
    public final static char CHAR_GUION = '-';

    public final static String STRING_CERO = "0";
    public final static String STRING_UNO = "1";
    public final static String STRING_DOS = "2";
    public final static String STRING_TRES = "3";
    public final static String STRING_CUATRO = "4";
    public final static String STRING_CINCO = "5";
    public final static String STRING_DIEZ = "10";
    public final static String STRING_UNOX2 = "11";
    public final static String STRING_TREINTA = "30";
    public final static String STRING_CEROX2 = "00";
    public final static String STRING_CEROX3 = "000";
    public final static String STRING_CEROx4 = "0000";

    public final static String VACIO = "";
    public final static String GUION = "-";
    public final static String SLASH = "/";
    public final static String PIPE = "|";
    public final static String PORCENTAJE = "%";
    public final static String SPACE = " ";
    public final static String PUNTO = "\\.";
    public final static String PUNTO2 = ".";
    public final static String DOSPUNTOS = ":";
    public final static String SI = "S";
    public final static String NO = "N";
    public final static String K = "K";
    public final static char TAB = (char) 9;

    public final static String IND_MKP_RIPLEY = "0";
    public final static String IND_MKP_MKP = "1";
    public final static String IND_MKP_AMBAS = "2";
    public final static String IND_MKP_REGALO = "3";
    public final static int ES_MKP_RIPLEY = 0;
    public final static int ES_MKP_MKP = 1;

    public final static boolean VERDADERO = true;
    public final static boolean FALSO = false;

    public final static String STRING_VERDADERO = "true";
    public final static String STRING_FALSO = "false";

    public final static String MSJ_REGALO_99 = "99";
    public final static String MSJ_REGALO_ERR_99 = "R 99  ";
    public final static String MSJ_REGALO_ERR_00 = "R 00  ";
    public final static String MSJ_REGALO_ERR_77 = " 77  ";

    public final static String MSJ_REGALO_SR = "< SR >";
    public final static String MSJ_REGALO_NR = "< NR >";

    //CONSTANTES DE TRAMA DE BOLETA PPL
    public final static String TRAMA_MENOR_QUE = "&lt;";
    public final static String TRAMA_ENCABEZADO = "EN";
    public final static String TRAMA_TIPO_BOLETA = "B";
    public final static String TRAMA_FOLIO = "0";
    public final static String TRAMA_TIPO_DESPACHO = "";
    public final static String TRAMA_TIPO_TRASLADO = "";
    public final static String TRAMA_IND_MONTO_BRUTO = "2";
    public final static String TRAMA_IND_MONTO_BRUTO_F = "1";
    public final static String TRAMA_IND_SERVICIO = "3";
    public final static String TRAMA_CODIGO_TRASLADO = "";
    public final static String TRAMA_FOLIO_AUT = "";
    public final static String TRAMA_FECHA_AUT = "";
    public final static String TRAMA_NO_INFORMADO = "NO INFORMADO";
    public final static String TRAMA_RUT_SOLICITA = "";
    public final static String TRAMA_GIRO_RECEPTOR = "";
    public final static String TRAMA_COMUNA_DESTINO = "";
    public final static String TRAMA_CIUDAD_RECEPTOR = "";
    public final static String TRAMA_MONTO_EXENTO = "0";

    public final static String TRAMA_DETALLE = "DE";
    public final static String TRAMA_IND_EXCEPCION = "0";
    public final static String TRAMA_TIPO_CODIGO = "SKU";
    public final static String TRAMA_DESC_DESCUENTO = "DESCUENTO";
    public final static String TRAMA_PORC_DESCUENTO = "";
    public final static String TRAMA_MONTO_DESCUENTO = "";
    public final static String TRAMA_COD_IMPUESTO = "";
    public final static String TRAMA_NRO_SUBTOTAL = "1";

    public final static String TRAMA_SUBTOTALES = "ST";
    public final static String TRAMA_SUBTOTAL = "SUBTOTAL";

    public final static String TRAMA_REFERENCIA = "RE";
    public final static String TRAMA_INDICADOR_GLOBAL = "";//0";
    public final static String TRAMA_COD_USO_REF = "1";
    public final static String TRAMA_RAZON_REFERENCIA = "DEVOLUCI�N MERC.";

    public final static String TRAMA_DESCUENTOS_RECARGOS = "DR";
    public final static String TRAMA_TIPO_MOVIMIENTO = "D";
    public final static String TRAMA_DESCUENTO_ITEM = "DESCUENTO ITEM ";
    public final static String TRAMA_DESCUENTO_TOTAL = "DESCUENTO TOTAL ";
    public final static String TRAMA_DESCUENTO_UNIDAD = "$";
    public final static String TRAMA_DESCUENTO_EXENCION = "0";

    public final static String TRAMA_PERSONALIZADO = "PE";
    public final static String TRAMA_DIRECCION_SUCURSAL = "";
    public final static String TRAMA_IMPRESORA = "";
    public final static String TRAMA_DOC_REFERENCIA = "Orden de Compra";
    public final static String TRAMA_DOC_REFERENCIA_NC = "";
    public final static String TRAMA_CODIGO_BARRA = "";
    public final static String TRAMA_CORRELATIVO_VENTA = "";

    public final static String TRAMA_IMPRESION_DETALLE = "PLID";
    public final static String TRAMA_PERSONALIZADO_ENC = "PEN";
    public final static String TRAMA_NRO_TRANSACCION = "NRO TRANSACCION/DOCUMENTO: ";
    public final static String TRAMA_VENDEDOR = "VENDEDOR: ";
    public final static String TRAMA_VENDE_SUP_AUT = "VENDE/SUPER/AUT";
    public final static String TRAMA_INTERNET = "INTERNET";
    public final static String TRAMA_CODIGO_REGALO = "CODIGO REGALO ";
    public final static String TRAMA_TIPO_REGALO = "TIPO REGALO: ";
    public final static String TRAMA_NOMBRE_CLIENTE = "NOMBRE CLIENTE: ";
    public final static String TRAMA_RUT_CLIENTE = "RUT CLIENTE: ";
    public final static String TRAMA_ORDEN_COMPRA = "ORDEN DE COMPRA: ";
    public final static String TRAMA_FECHA_OC = "FECHA OC: ";
    public final static String TRAMA_VEN = "VEN ";
    public final static String TRAMA_FECHA = "   FECHA ";
    public final static String TRAMA_SUPERVISOR = "SUPERVISOR ";

    public final static String TRAMA_PERSONALIZADO_DET = "PDE";
    public final static String TRAMA_COSTO_ENVIO = "COSTO DE ENVIO";
    public final static String TRAMA_EG = "EG";
    public final static String PREFIJO_EXTRA_GARANTIA = "EG ";

    public final static String TRAMA_EG_REEM = "EG REEMPLAZO";
    public final static String TRAMA_EG_EXT = "EG EXTIENDE";

    public final static String TRAMA_ZONA_DET_1 = "DECOM";
    public final static String TRAMA_ZONA_DET_2 = "DEMON";
    public final static String TRAMA_BT = "<BT> ";
    public final static String TRAMA_CODIGO_DET = "";
    public final static String TRAMA_DESCUENTO_DET = "";

    public final static String TRAMA_PER_MEDIO_PAGO = "PMP";
    public final static String TRAMA_MEDIO_PAGO = "Medio De Pago: ";
    public final static String TRAMA_PAN = "PAN: ";
    public final static String TRAMA_ASTERISCOS_X6 = "******";
    public final static String TRAMA_TARJETA_RIPLEY = "TARJ RIPLEY";
    public final static String TRAMA_COD_AUTORIZADOR = "COD.AUTORIZADOR: ";
    public final static String TRAMA_VUELTO = "VUELTO";
    public final static String TRAMA_TARJETA_CREDITO = "WEBPAY TARJETA DE CREDITO";
    public final static String TRAMA_TARJETA_CREDITO_NC = "TARJETA BANCARIA CREDITO";
    public final static String TRAMA_TARJETA_DEBITO = "WEBPAY TARJETA DE DEBITO";
    public final static String TRAMA_TARJETA_DEBITO_NC = "TARJETA BANCARIA DEBITO";
    public final static String TRAMA_MERCADO_PAGO = "Tarjeta Credito por Mercado Pago";
    public final static String TRAMA_MERCADO_PAGO_NC = "TARJETA BANCARIA CREDITO";
    public final static String TRAMA_CONVENIO = "CONVENIO: ";
    public final static String TRAMA_CODIGO_AUT = "CODIGO AUT.:";
    public final static String TRAMA_CODIGO_AUT_B = " B";
    public final static String TRAMA_NUMERO_UNICO = "NRO UNICO: ";
    public final static String TRAMA_NUMERO_UNICO_NU = "NU";
    public final static String TRAMA_TAR_REGALO_EMPRESA = "TARJETA REGALO EMPRESA";
    public final static String TRAMA_MONTO_TRE = "T.R.EMPRESA";
    public final static String TRAMA_NUMERO_TARJETA = "N�TARJETA.:";
    public final static String TRAMA_CTA_CTE_NRO = "CTA. CTE. NRO.: ";
    public final static String TRAMA_NOMBRE_TITULAR = "NOMBRE TIT.: ";

    public final static String TRAMA_PER_LEYENDA_A_TIMBRE = "PLAT";
    public final static String TRAMA_VENTA = "VENTA";
    public final static String TRAMA_TIPO_TRX_UNO = "1";
    public final static String TRAMA_TIPO_TRX_DOS = "2";
    public final static String TRAMA_ASTERISCOS_x41 = "***************************************";
    public final static String TRAMA_REGALO_UNO_1 = "      PROGRAMA RIPLEY PUNTOS";
    public final static String TRAMA_REGALO_UNO_2 = " Sr(a)";
    public final static String TRAMA_REGALO_UNO_3 = " Ripley Puntos Obtenidos por la Compra :  ";
    public final static String TRAMA_REGALO_UNO_4 = "*   Informaci�n sujeta a validaci�n ";
    public final static String TRAMA_REGALO_UNO_5 = "- POR CUENTA DE COMERCIAL ECCSA S.A -";

    public final static String TRAMA_REGALO_TRES_1 = "       INSCRIBASE EN RIPLEY PUNTOS";
    public final static String TRAMA_REGALO_TRES_2 = "  LLAMANDO AL 600 600 0202, EN CENTROS";
    public final static String TRAMA_REGALO_TRES_3 = "      DE SERVICIO O EN RIPLEY.CL";

    public final static String TRAMA_REGALO_DOS_1 = "   El comprador que adquiere bajo el";
    public final static String TRAMA_REGALO_DOS_2 = " sistema de Listas de regalo, faculta a";
    public final static String TRAMA_REGALO_DOS_3 = "los titulares del c�digo de regalo de";
    public final static String TRAMA_REGALO_DOS_4 = "esta compra, para que los representen en";
    public final static String TRAMA_REGALO_DOS_5 = "     sus derechos como comprador.";

    public final static String TRAMA_PLAT_TBK_1 = "MARCA: ";
    public final static String TRAMA_PLAT_TBK_2 = " CODIGO DE REGALO: ";
    public final static String TRAMA_PLAT_TBK_3 = "COD. AUTORIZADOR: ";
    public final static String TRAMA_PLAT_BANCARIA = "Bancaria";
    public final static String TRAMA_PLAT_MPAGO = "Tarjeta Credito por Mercado Pago";
    public final static String TRAMA_PLAT_DEBITO = "Debito";
    public final static String TRAMA_PLAT_CREDITO = "Credito";
    public final static String TRAMA_EFECTIVO = "Efectivo";
    public final static String TRAMA_ID_TBK = "ID TBK: ";

    public final static String TRAMA_PLAT_CAR_1 = "ACEPTO LOS CARGOS INDICADOS EN ESTE DOCUMENTO EN MI TARJETA Y PAGARLOS SEGÚN CONDICIONES DE CONTRATO";
    public final static String TRAMA_PLAT_CAR_2 = "CON CAR S.A. ASIMISMO, ACEPTO LAS CESIONES QUE CAR S.A. EFECTÚE DE ESTE CRÉDITO U OTROS VIGENTES CON CAR S.A";

    public final static String TRAMA_PLAT_CAR_1_1 = "ACEPTO LOS CARGOS INDICADOS EN ESTE DOCUMENTO EN MI TARJETA Y PAGARLOS";
    public final static String TRAMA_PLAT_CAR_1_2 = "SEGÚN CONDICIONES DE CONTRATO CON CAR S.A. ASIMISMO, ACEPTO LAS CESIONES";
    public final static String TRAMA_PLAT_CAR_1_3 = "QUE CAR S.A. EFECT�E DE ESTE CRÉDITO U OTROS VIGENTES CON CAR S.A";

    public final static String TRAMA_PLAT_TRE_1 = "FORMA DE PAGO: TARJETA REGALO EMPRESA    T. EMPRESA N°: ";
    public final static String TRAMA_PLAT_TRE_2 = " MONTO COMPRA: ";
    public final static String TRAMA_PLAT_TRE_3 = "ACEPTO LOS CARGOS INDICADOS EN ESTE DOCUMENTO EN MI TARJETA REGALO EMPRESA.";

    public final static String TRAMA_ACEPTO_PAGAR = "ACEPTO PAGAR SEGUN CONTRATO CON EMISOR";

    public final static String TRAMA_PER_LEYENDA_D_TIMBRE = "PLDT";

    public final static String TRAMA_PLDT_CAR_0 = "COMPROBANTE DE FINANCIAMIENTO";
    public final static String TRAMA_PLDT_CAR_1 = "Monto financiado corresponde al cargo total realizado";
    public final static String TRAMA_PLDT_CAR_2 = "a su Tarjeta Ripley. Esta informaci�n incluye el pago";
    public final static String TRAMA_PLDT_CAR_2_2 = "a su Tarjeta. Esta informaci�n incluye el pago";
    public final static String TRAMA_PLDT_CAR_3 = "de productos Ripley y productos de Mercado Ripley.com.";

    public final static String TRAMA_GUION_X41 = "_________________________________________";
    public final static String TRAMA_GUION_X29 = "_____________________________";
    public final static String TRAMA_VTA_RIPLEY = "PRODUCTOS RIPLEY:       ";
    public final static String TRAMA_RECA_PROVEEDOR = "MERCADO RIPLEY.COM:     ";
    public final static String TRAMA_TOTAL = "TOTAL FINANCIADO:       ";
    public final static String TRAMA_DIRECCION_DESPACHO = "DIRECCION DESPACHO - [";
    public final static String TRAMA_REGION = "REGION: ";
    public final static String TRAMA_COMUNA = "COMUNA: ";
    public final static String TRAMA_RECIBI_CONFORME = "RECIBI CONFORME MERCADERIA Y/O SERVICIOS";
    
    //Variables para nueva linea en la trama
    public final static String TRAMA_FP = "FP";
    public final static String TRAMA_FORMA_PAGO = "FormaPago";
    public final static String TRAMA_CONTADO = "Contado";

    //public final static String TRAMA_INICIO_TRAMA		= "@**@2";
    public final static String TRAMA_INICIO_TRAMA = "";//<![CDATA[";
    public final static String TRAMA_TRAINING = "0";
    //public final static String TRAMA_FIN_TRAMA		= "1*@@*";
    public final static String TRAMA_FIN_TRAMA = "";//]]>";

    // CONSTANTES PARA CREACION DE BOLETAS
    public final static String BOL_FORMA_PAGO_TB = "Tarjeta Bancaria";
    public final static String BOL_FORMA_PAGO_WO = "One Click";
    public final static String BOL_FORMA_PAGO_MP = "Mercado Pago";
    public final static String BOL_FORMA_PAGO_TRE = "TRE";
    public final static String BOL_FORMA_PAGO_CAR = "CAR";
    public final static String BOL_FORMA_PAGO_COM = "COM";
    public final static String BOL_FORMA_PAGO_EFE = "Efectivo";
    public final static String BOL_FORMA_PAGO_CT = "CAR-TRE";
    public final static String BOL_VALOR_VD_S = "S";
    public final static String BOL_VALOR_VD_D = "D";
    public final static String BOL_VALOR_VD_C = "C";
    public final static String BOL_VALOR_VD_N = "N";
    public final static String BOL_VALOR_VD_M = "M";
    public final static String BOL_JORNADA_AM = "AM";
    public final static String BOL_JORNADA_PM = "PM";
    public final static String BOL_TIPO_DES_RT = "RT";

    // CONSTANTES PAPERLESS
    public final static int PPL_TIPO_GENERACION = 1;
    public final static int PPL_TIPO_RETORNO = 6;

    // ANULACI�N RECAUDACI�N
    public final static String TIPO_PAGO_WEBP = "WEB";
    public final static String TARJETA_MERCADO_PAGO = "MP";
    public final static String TARJETA_ONE_CLICK = "WO";
    public final static String SIN_STOCK = "SIN STOCK";
    public final static int esNC = 1;

    // NOTA DE CREDITO
    public final static String TIPO_TAR_COM = "COM";
    public final static int TARJETA_RIPLEY = 1;
    public final static int TARJETA_BANCARIA = 2;
    public final static int TARJETA_REGALO_EMPRESA = 2;

    // RECHAZO 
    public final static String TIPO_EVENTO_RC = "RC";

    //MIRAKL
    public final static int MKP_PENDIENTE_PETICION = 1;
    public final static int MKP_SIN_STOCK = 12;
    public final static String PA_UNO = "PA01";
    public final static String PA_DOS = "PA02";
    public final static String OR_DOS = "OR02";
    public final static String OR_TRES = "OR03";
    public final static String SS = "SIN STOCK";
    public final static String SC = "STOCK CONFIRMADO";

    //TBK NULLIFY
    public final static int TBK_ESTADO_INICIAL = 0;
    public final static String TBK_ESTADO_PENDIENTE = "990";
    public final static String TBK_GLOSA_PENDIENTE = "PENDIENTE";

    //RETORNO BASE DE DATOS
    public static final String ERROR_UNIQUE_CONSTRAINT = "ORA-00001";

    public static final String NRO_CAJA_NAME = "NRO_CAJA";
    public static final String NRO_SUCURSAL_NAME = "NRO_SUCURSAL";
    public static final String NRO_ESTADO_NAME = "NRO_ESTADO";
    public static final String QUERY_NAME = "QUERY";
    public static final String CURSOR_NAME = "CURSOR";
    public static final String CODIGO_NAME = "CODIGO";
    public static final String MENSAJE_NAME = "MENSAJE";
    public static final String CORRELATIVO_VENTA_NAME = "CORRELATIVO_VENTA";
    public static final String PARAM_OMNICANALIDAD_NAME = "PARAM_OMNICANALIDAD";
    public static final String PARAM_NAME = "NAME";
    public static final String PARAM_VALUE = "VALUE";
    public static final String PARAM_TYPE = "TYPE";
    public static final String PARAM_COD_MENSAJE = "COD_MENSAJE";

    //VALIDACION DE BOLETAS
    public static final String MAIL_CLIENTE = "MAIL DEL CLIENTE";
    public static final String TELEFONO_CLIENTE = "NUMERO DE TELEFONO DEL CLIENTE";
    public static final int COMUNA_CLIENTE = 0;
    public static final int REGION_CLIENTE = 0;
    public static final int TIPO_DOC = 1000;

    //RETORNO VALIDACION BOLETAS
    public static final String RETORNO = "RETORNO";
    public static final String PARAM_ESTADO = "ESTADO";
    public static final String PARAM_CORRELATIVO_VENTA = "CORRELATIVO_VENTA";

    //URL DOCE
    public static final String URL_CORREO_NO_ENVIADO = "#";
    public static final String URL_CORREO_ENVIADO = "@";
    public static final String COLUMNA_NV_URL_BOLETA = "URL_DOCE";

    public static final int TIPO_DOC_FACTURA = 1;
    public static final int TIPO_DOC_BOLETA = 3;
    public static final int TIPO_DOC_IDENTIDAD = 0;
    public static final String TRAMA_DOC = "DOC";
    public static final String TRAMA_DI = "DI";
    public static final String IDENT_TRIBUTO = "1000";
    public static final String IDENT_TRIBUTO_9998 = "9998";
    public static final String NOMBRE_TRIBUTO_IGV = "IGV";
    public static final String NOMBRE_TRIBUTO_INA = "INA";
    public static final String COD_TIP_TRIBUTO_VAT = "VAT";
    public static final String COD_TIP_TRIBUTO_FRE = "FRE";
    public static final String TRAMA_DN = "DN";
    public static final String CODIGO_LEYENDA = "1000";
    public static final String TRAMA_DR = "DR";
    public static final String MENSAJES_AT = "MensajesAT";
    public static final String MENSAJES_DT = "MensajesDT";
    public static final String TRAMA_PES = "PES";
    public static final String TRAMA_PESD = "PESD";
    public static final long CANAL_EFECTIVO = 2;
    public static final long CANAL_PAYPAL = 1;
    public static final long CANAL_TARJETA_CREDITO = 3;
    public static final long CANAL_TARJETA_DEDITO = 4;
    public static final String FORMA_PAGO_TARJETA_RIPLEY = "TARJETA RIPLEY";
    public static final String TRAMA_PE = "PE";
    public static final BigDecimal NRO_CERO_B = new BigDecimal(NRO_CERO);
    public static final BigDecimal NRO_UNO_B = new BigDecimal(NRO_UNO);
    public static final int NRO_DIECISEIS = 16;
    public static final String PCTC_COORDINATE = "A1";
    public static final String PVERSION_PMG = "CTVI02";
    public static final String FECHA_ORIGINAL = "2001-01-01";
    public static final Integer NRO_SESENTA = 60;
    public static final String BIN_DIECINUEVE = "191919";
    public static final String FORMA_PAGO_CENTREGA = "CONTRA ENTREGA";
    public static final String FORMA_PAGO_PAYPAL = "PAYPAL";
    public static final String FORMA_PAGO_TDC = "TARJETA CREDITO";
    public static final String FORMA_PAGO_DEBITO = "TARJETA DEBITO";
    public static final String FORMA_PAGO_TIENDA = "PAGO TIENDA";
    public static final String FORMA_PAGO_EFECTIVO = "EFECTIVO";
    public static final String CODIGO_CONTRAEFECTIVO = "CONTRA ENTREGA";
    public static final String CODIGO_CONTRABANCARIA = "CONTRA ENTREGA BANCARIA";
    public static final String NRO_BIN_DICECIOCHO = "181818";
    public static final BigDecimal NRO_MENOSUNO_B = new BigDecimal(NRO_MENOSUNO);
    public static final String FECHA_YYYY_MM_DD_HH_MI_SS = "YYYY-MM-DD HH24:MI:SS";
    public static final String FUNCION = "FUNCION";
    public static final Integer BO_TIPO_TRX_CIERRE = 44;
    public static final String NRO_CAJA_BOLETAS = "NRO_CAJA_BOLETAS";
    public static final Integer BO_TIPO_TRX_APERTURA = 42;

    //Servicio PPL
    public static final Integer VALIDACION_OK = 1;
    public static final Integer VALIDACION_NO_OK = 0;
    public static final Integer CANAL_CONTRAENTREGA_BANCARIA = 10;
    public static final Integer CANAL_CONTRA_ENTREGA = 4;
    public static final Integer NRO_VEINTICINCO = 25;
    public static final String ES_BANCARIA = "BAN";
    public static final String NUM_EVENTO = "999999999998";
    public static final Integer NRO_NOVENTAOCHO = 98;
    public static final String NRO_RECAUDACION = "1001";

    //VALIDACIONES MEDIO PAGO
    public static final Integer VALIDACION_OK_2 = 0;

    public static final String FECHA_MMDD = "MMDD";
    public static final String FORMATO_8_CEROS_IZQ = "%08d";
    public static final String FORMATO_9_CEROS_IZQ = "%09d";
    public static final String FORMATO_4_CEROS_IZQ = "%04d";
    public static final String FORMATO_10_CEROS_IZQ = "%010d";
    public static final String FORMATO_3_CEROS_IZQ = "%03d";
    public static final String FORMATO_2_CEROS_IZQ = "%02d";
    public static final String FORMATO_6_CEROS_IZQ = "%06d";
    public static final String FORMATO_53_CEROS_IZQ = "%053d";
    public static final String FORMATO_12_CEROS_IZQ = "%012d";
    public static final String FORMATO_13_CEROS_IZQ = "%013d";
    public static final String FORMATO_14_CEROS_IZQ = "%014d";
    public static final String FORMATO_22_CEROS_IZQ = "%022d";
    public static final String FORMATO_20_ESPACIOS_DER = "%-20s";
    public static final String FECHA_YY = "YY";
    public static final String TRX_STATUS_HEADER_BCV = "80";
    public static final String SEGMENT_HEADER_BCV = "0001";
    public static final String SYSTEM_ID_HEADER_BCV = "1234";
    public static final String FLAG_HEADER_BCV = "0000";
    public static final String PARAM_VENDEDOR_NAME = "RUT_VENDEDOR";
    public static final String PARAM_SUCURSAL_NAME = "SUCURSAL";
    public static final String NOMBRE_CAJERO = "TIENDA VIRTUAL";
    public static final String FORMATO_FECHA = "%1$td-%1$tm-%1$tY";
    public static final String FORMATO_FECHA_2 = "%1$td/%1$tm/%1$tY";
    public static final String FORMATO_FECHA_3 = "%1$td-%1$tm-%1$tY %1$tH:%1$tM:%1$tS";
    public static final String FORMATO_FECHA_4 = "%1$tY-%1$tm-%1$td";
    public static final String FORMATO_FECHA_5 = "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS";
    public static final String FORMATO_FECHA_6 = "%1$td%1$tm%1$tY";
    public static final String SERIE_FACTURA_NAME = "SERIE_FACTURA";
    public static final String SERIE_BOLETA_NAME = "SERIE_BOLETA";
    public static final String FORMATO_MONTOS = "%.2f";
    public static final String PARAM_VALOR_IVA_NAME = "VALORIVA";
    public static final String PARAM_TENDER_ID_NAME = "TENDER_ID";
    public static final String MKP = "MK";
    public static final String ESPACIO = " ";
    public static final String PPL_SWITCH = "PPL_SWITCH";
    public static final String BCV_SWITCH = "BCV_SWITCH";
    public static final String NROS_CAJAS_BOLETAS = "NROS_CAJAS_BOLETAS";
    public static final String COMA = ",";
    public static final String CANAL_SAFETYPAY = "SAFETYPAY";
    public static final Integer NRO_VEINTISEIS = 26;
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String NRO_CAJA_PAGO_TIENDA = "NRO_CAJA_PAGO_TIENDA";
    public static final String MONTO_SUPERIOR_RECEPTOR = "MONTO_SUPERIOR_RECEPTOR";
    public static final String UNIDAD_MEDIDA_PRODUCTO = "NIU";
    public static final String UNIDAD_MEDIDA_SERVICIO = "ST";
    public static final String TRAMA_DETALLE_DESCRIPCION_ITEM = "DEDI";
    public static final String TRAMA_DETALLE_DESCUENTOS_RECARGOS = "DEDR";
    public static final String TRAMA_DETALLE_IMPUESTO = "DEIM";
    public static final String TRAMA_ENEX = "ENEX";
    public static final String ESPACIO_X_5 = "     ";
    public static final String MP_GLOSA_RIPLEY = "RIP";
    public static final String MP_GLOSA_BANCARIA = "BAN";
    public static final String MP_GLOSA_EFECTIVO = "EFE";
    public static final String OK = "OK";
    public static final String TIENDA_ONLINE = "ONLINE";
    public static final String TRX_XML_NAME = "TransactionXml";
    public static final String METHOD_XML_NAME = "Route";
    public static final String SYSTEM_XML_NAME = "1234";
    public static final String STORE_XML_NAME = "00000060";
    public static final String TERMINAL_XML_NAME = "0112";
    public static final String CHECKER_XML_NAME = "0";
    public static final String USUARIO_TIENDA_VIRTUAL = "UsuarioTiendaVirtual";
    public static final String PARAM_TERMINAL = "TERMINAL";
    public static final String DCREQ = "DCREQ";
    public static final String COMPLETE = "Complete";
    public static final String FALSE_STRING = "False";
    public static final String FORMATO_6_CEROS_DER = "%-06d";
    public static final String FORMATO_2_CEROS_DER = "%-02d";
    public static final String FORMATO_2_DECIMALES = "%.2f";
    public static final String FORMATO_5_DECIMALES = "%.5f";
    public static final String FACTURA = "Factura";
    public static final String BOLETA = "Boleta";
    public static final String ESTABLECIMIENTO_COMERCIAL = "ESTABLECIMIENTO_COMERCIAL";
    public static final String AMPER_NO_SCAPED = "&";
    public static final String AMPER_SCAPED = "&amp;";
    public static final Character CHAR_ASTERISCO = '*';
    public static final Character CHAR_NUEVE = '9';
    public static final String DESC_COSTO_ENVIO = "COSTO DE ENVIO";
    public static final String FORMA_PAGO_CENTREGA_BANCARIA = "CONTRA ENTREGA BANCARIA";
    public static final String GLOSA_AVISO = "Este es un comprobante de recaudación la boleta de venta será enviada por el seller/proveedor cuando reciba su pedido.";
    public static final String NRO_CAJA_NC = "NRO_CAJA_NC";
    public static final String SWITCH_NC = "SWITCH_NC";
    public static final String NROS_CAJAS_NC = "NROS_CAJAS_NC";
    public static final int NRO_DIECISIETE = 17;
    public static final String FECHA_FORMATO_DD_MMM_YYYY = "dd/MMM/uuuu";
    public static final String FECHA_FORMATO_DD_MMM_YY = "dd-MMM-uu";
    public static final String FECHA_FORMATO_YYYY_MM_DD = "uuuu-MM-dd";
    public static final String FECHA_FORMATO_DDMMYYYY_HHMMSS = "dd-MM-uuuu HH:mm:ss";
    public static final String FECHA_DDMMYYYY_HHMMSS_LDT = "dd/MM/uuuu HH:mm:ss";
    public static final String FORMATO_HASTA_40_CHR = "%1$.40s";
    public static final String FECHA_FORMATO_YYYY_MM_DD_HH_MI_SS = "uuuu-MM-dd HH:mm:ss";
    public static final String FECHA_FORMATO_YYYY_MM_DD_T_HH_MI_SS = "uuuu-MM-dd'T'HH:mm:ss";
    public static final String FECHA_FORMATO_MMDD = "MMdd";
    public static final String FECHA_FORMATO_YY = "uu";
    public static final String FECHA_FORMATO_DDMMYYYY = "dd/MM/uuuu";
    public static final String FECHA_FORMATO_HHMISS = "HHmmss";
    public static final String FECHA_FORMATO_HH_MI_SS = "HH:mm:ss";
    public static final String FECHA_FORMATO_HH_MI = "HH:mm";
    public static final String PARAM_CODIGO_SUCURSAL = "CODIGO_SUCURSAL";

    //Local Date Time Formatters
    public static final DateTimeFormatter DATE_TIME_FORMATER_DDMMYYYY_HHMMSS = DateTimeFormatter.ofPattern(FECHA_FORMATO_DDMMYYYY_HHMMSS);
    public static final DateTimeFormatter DATE_TIME_FORMATER_YYYY_MM_DD = DateTimeFormatter.ofPattern(FECHA_FORMATO_YYYY_MM_DD);
    public static final DateTimeFormatter DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS = DateTimeFormatter.ofPattern(FECHA_FORMATO_YYYY_MM_DD_HH_MI_SS);
    public static final DateTimeFormatter DATE_TIME_FORMATER_YYYY_MM_DD_T_HH_MI_SS = DateTimeFormatter.ofPattern(FECHA_FORMATO_YYYY_MM_DD_T_HH_MI_SS);
    public static final DateTimeFormatter DATE_TIME_FORMATER_MMDD = DateTimeFormatter.ofPattern(FECHA_FORMATO_MMDD);
    public static final DateTimeFormatter DATE_TIME_FORMATER_YY = DateTimeFormatter.ofPattern(FECHA_FORMATO_YY);
    public static final DateTimeFormatter DATE_TIME_FORMATER_DDMMYYYY = DateTimeFormatter.ofPattern(FECHA_FORMATO_DDMMYYYY);
    public static final DateTimeFormatter DATE_TIME_FORMATER_HHMISS = DateTimeFormatter.ofPattern(FECHA_FORMATO_HHMISS);
    public static final DateTimeFormatter DATE_TIME_FORMATER_HH_MI_SS = DateTimeFormatter.ofPattern(FECHA_FORMATO_HH_MI_SS);

    public static final String FORMATO_10_ESPACIOS = "%10d";
    public static final int REST_TIMEOUT = 30_000;
    public static final String TIPO_LLAMADA_BO = "BO";
    public static final String TIPO_LLAMADA_BT = "BT";
    public static final String TIPO_LLAMADA_BCV = "BCV";

    public static final String TOKEN_ENCABEZADO = "EN|";
    public static final String TOKEN_TRX = "PE|Trx|";
    public static final String TOKEN_HORA = "PE|Hora|";

    public static final String PPL_TAG_TIPO_OPERACION = "PPL_TIPO_OPERACION";
    public static final String PPL_VERSION_UBL = "2.1";

    //Rpos
    public static final String URL_OC_DATOS = "URL_OC_DATOS";
    public static final String PROP_PC_MAC_RPOS = "PC_MAC_RPOS";
    public static final String URL_ERROR_OC_DATOS = "Error";
    public static final String STRING_HEADER_SERVICIO = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 safari/537.36";
    public static final String STRING_HEADER_USER = "user-agent";
    public static final String NAME_XVALID_RPOS = "PROC_EJEC_XVALID_RPOS";
    public static final String NAME_XVALID_NVENTA = "PROC_EJEC_XVALID_NVENTA";
    public static final String TIPO_PAGO_TRE = "TRE";
    public static final String VERBOLETA = "verBoleta";
    public static final String VERFACTURA = "verFactura";
    public static final String NAME_RAZON_SOCIAL_EMISOR = "RAZON_SOCIAL_EMISOR";
    public static final String NAME_RUT_EMISOR = "RUT_EMISOR";
    public static final String NAME_GIRO_EMISOR = "GIRO_EMISOR";
    public static final String NAME_DIRECCION_ORIGEN = "DIRECCION_ORIGEN";
    public static final String NAME_TRAMA_DIRECCION_SUCURSAL_ = "TRAMA_DIRECCION_SUCURSAL_";
    public static final String NAME_CIUDAD_ORIGEN = "CIUDAD_ORIGEN";
    public static final String NAME_COMUNA_ORIGEN = "COMUNA_ORIGEN";
    public static final String NAME_PAGINA_WEB = "PAGINA_WEB";
    public final static String BOL_FORMA_PAGO_TRECRIP = "TARJETA REGALO EMPRESAS con TARJETA RIPLEY";
    public final static String BOL_FORMA_PAGO_TAR_REG_EMP = "TARJETA REGALO EMPRESAS";
    public final static String TRAMA_PLAT_DEBITO_RPOS = "DEBITO";
    public final static String TRAMA_PLAT_CREDITO_RPOS = "CREDITO";
    public final static String TRAMA_FORMA_PAGO_TB_RPOS = "TARJ BANC";
    public final static String PARAM_NUMERO_SUCURSAL = "NUMERO_SUCURSAL";
    public final static String PARAM_NUMERO_CAJA = "NUMERO_CAJA";
    public final static String PARAM_DIRECCION = "DIRECCION";
    public final static String PARAM_LOCAL_TIENDA = "LOCAL_TIENDA";
    public final static String PARAM_DESCRIPCION = "DESCRIPCION";
    public final static String PARAM_COD_SUNAT = "COD_SUNAT";
    public final static String PARAM_ACTIVO = "ACTIVO";
    public final static String PARAM_JOB_JENKINS = "JOB_JENKINS";
    public final static String PARAM_CONFIG_SGO = "CONFIG_SGO";
    public final static String PARAM_SERIE_SUNAT = "SERIE_SUNAT";
    public final static String PARAM_DISTRITO = "DISTRITO";
    public final static String PARAM_DIRECCION_CORTA = "DIRECCION_CORTA";
    public final static String PARAM_SUCURSAL_PPL = "SUCURSAL_PPL";
    public final static String PARAM_SUCURSAL_RPOS = "SUCURSAL_RPOS";
    public final static String NAME_COSTO_ENVIO = "COSTO DE ENVIO";
    public final static String PARAM_DESPACHO_RPOS = "DESPACHO_DOMICILIO_RPOS";
    public final static String TRAMA_TRX_RPOS = "Trx";
    public final static String NAME_DIFERIDO_CERO = "Normal";
    public final static String NAME_DIFERIDO_DOS = "Diferido";
    public final static String NAME_VISA = "VISA";
    public final static String NAME_MASTERCARD = "MASTERCARD";
    public final static String NAME_DINERS = "DINERS";
    public final static String NAME_AMEX = "AMEX";

    //Regiones AWS
    public final static String AWS_REGION_AP_NORTHEAST_1 = "AP_NORTHEAST_1";
    public final static String AWS_REGION_AP_NORTHEAST_2 = "AP_NORTHEAST_2";
    public final static String AWS_REGION_AP_SOUTH_1 = "AP_SOUTH_1";
    public final static String AWS_REGION_AP_SOUTHEAST_1 = "AP_SOUTHEAST_1";
    public final static String AWS_REGION_AP_SOUTHEAST_2 = "AP_SOUTHEAST_2";
    public final static String AWS_REGION_CA_CENTRAL_1 = "CA_CENTRAL_1";
    public final static String AWS_REGION_CN_NORTH_1 = "CN_NORTH_1";
    public final static String AWS_REGION_CN_NORTHWEST_1 = "CN_NORTHWEST_1";
    public final static String AWS_REGION_EU_CENTRAL_1 = "EU_CENTRAL_1";
    public final static String AWS_REGION_EU_NORTH_1 = "EU_NORTH_1";
    public final static String AWS_REGION_EU_WEST_1 = "EU_WEST_1";
    public final static String AWS_REGION_EU_WEST_2 = "EU_WEST_2";
    public final static String AWS_REGION_EU_WEST_3 = "EU_WEST_3";
    public final static String AWS_REGION_SA_EAST_1 = "SA_EAST_1";
    public final static String AWS_REGION_US_EAST_1 = "US_EAST_1";
    public final static String AWS_REGION_US_EAST_2 = "US_EAST_2";
    public final static String AWS_REGION_US_GOV_EAST_1 = "US_GOV_EAST_1";
    public final static String AWS_REGION_US_WEST_1 = "US_WEST_1";
    public final static String AWS_REGION_US_WEST_2 = "US_WEST_2";
    public final static String AWS_REGION_DEFAULT_REGION = "DEFAULT_REGION";

    public final static BigDecimal NRO_CERO_COMA_CEROCERO_B = new BigDecimal("0.00");

    //Se añade para PagoEfectivo
    //SECRETKEY_QA
    public final static String SECRETKEY = "O57gGkdOPgmWxmnsnaKgYSwB8BC24Rt9U17WkyL5";
    //SECRETKEY_PRD
   // public final static String SECRETKEY = "KCGILnKrPIH53PMm1+jNXcyNPWcz+dBFdiQcDESt";
}
