package cl.ripley.omnicanalidad.util;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import cl.aligare.ara.util.AligareException;
 
public class ContextListener {
 		
    /**
     * Carga configuracion de properties y lo retorna
     */
    public Properties contextInit(Properties prop, String opcion) {
    	try {            
    		    FileInputStream file;


    		    String pathBD = "./Configuraciones_cvirtualaBD.properties";
    		    
    		    String path = Constantes.VACIO;
    		    if(Constantes.STRING_UNO.equalsIgnoreCase(opcion)){
    		    	path = "./Configuraciones_cvirtuala1.properties";
    		    } else  if(Constantes.STRING_DOS.equalsIgnoreCase(opcion)){
    		    	path = "./Configuraciones_cvirtuala2.properties";
    		    } else  if(Constantes.STRING_TRES.equalsIgnoreCase(opcion)){
    		    	path = "./Configuraciones_cvirtuala3.properties";
    		    } else  if(Constantes.STRING_CUATRO.equalsIgnoreCase(opcion)){
    		    	path = "./Configuraciones_cvirtuala4.properties";
    		    }
    		    file = new FileInputStream(pathBD);
    		    prop.load(file);
    		    file.close();


/*
    		    String path = Constantes.VACIO;
    		    if(Constantes.STRING_CUATRO.equalsIgnoreCase(opcion)){
    		    	path = "./Configuraciones_cvirtuala4.properties";
    		    }else{
    		    	path = "./Configuraciones_cvirtuala.properties";
    		    }
*/
    		    
    		    
    		    file = new FileInputStream(path);
    		    prop.load(file);
    		    file.close();
    		    
    		    PropertyConfigurator.configure(prop);
    	       	BasicConfigurator.configure();
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw new AligareException("No se ha podido cargar el archivo 'Configuraciones_cvirtuala.properties'.");
    	}
    	
    	return prop;
    }
     
}
