package cl.ripley.omnicanalidad.util;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

public class FechaUtil {
	
	private static final AriLog logger = new AriLog(FechaUtil.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    public static String calendarToString(Calendar c, int format, int tipo, String separator){
        DecimalFormat dformat = new DecimalFormat(Constantes.STRING_CEROX2);
        DecimalFormat yformat = new DecimalFormat(Constantes.STRING_CEROx4);

        StringBuffer sb = new StringBuffer();
        
        if (format == Constantes.NRO_UNO){
	        sb.append(dformat.format(c.get(Calendar.DATE)));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.MONTH) + Constantes.NRO_UNO));
	        sb.append(separator);
	        sb.append(yformat.format(c.get(Calendar.YEAR)));
        } else if (format == Constantes.NRO_CERO) {
	        sb.append(yformat.format(c.get(Calendar.YEAR)));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.MONTH) + Constantes.NRO_UNO));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.DATE)));
        } else if (format == Constantes.NRO_DOS){
	        sb.append(dformat.format(c.get(Calendar.DATE)));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.MONTH) + Constantes.NRO_UNO));
	        sb.append(separator);
	        sb.append(yformat.format(c.get(Calendar.YEAR)).substring(2,4));
        } else {
	        sb.append(yformat.format(c.get(Calendar.YEAR)).substring(2,4));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.MONTH) + Constantes.NRO_UNO));
	        sb.append(separator);
	        sb.append(dformat.format(c.get(Calendar.DATE)));
        }

        if (tipo == Constantes.NRO_UNO){
            sb.append(Constantes.SPACE);
            sb.append(dformat.format(c.get(Calendar.HOUR_OF_DAY)));
            sb.append(Constantes.DOSPUNTOS);
            sb.append(dformat.format(c.get(Calendar.MINUTE)));
            sb.append(Constantes.DOSPUNTOS);
            sb.append(dformat.format(c.get(Calendar.SECOND)));
        } else if (tipo == Constantes.NRO_DOS){
            sb.append(Constantes.SPACE);
            sb.append(dformat.format(c.get(Calendar.HOUR_OF_DAY)));
            sb.append(dformat.format(c.get(Calendar.MINUTE)));
            sb.append(dformat.format(c.get(Calendar.SECOND)));
        } else {
        	//nada
        }

        return sb.toString();
    }

    /**
     * Retorna un String con la fecha en el formato
     * "dd/mm/yyyy hh:mm:ss" de la fecha actual
     */
    public static String getCurrentDateTimeString(){
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_UNO, Constantes.NRO_UNO, Constantes.SLASH);
    }

    /**
     * Retorna un String con la fecha en el formato
     * "YYMMDD HHMMSS" de la fecha actual
     */
    public static String getCurrentDateTimeSimpleString(){
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_TRES, Constantes.NRO_DOS, Constantes.VACIO);
    }
    /**
     * Retorna un String con la fecha en el formato
     * "dd/mm/yyyy" of the Current Date (now)
     * @return String date dd/mm/yyyy
     */
    public static String getCurrentDateString(){
        // convert Date to Calendar
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_UNO, Constantes.NRO_CERO, Constantes.SLASH);
    }
    
    /**
     * Retorna un String con la fecha en el formato
     * "yyyy-mm-dd" of the Current Date (now)
     * @return String date yyyy-mm-dd
     */
    public static String getCurrentDateYYYYMMDDString(){
        // convert Date to Calendar
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_CERO, Constantes.NRO_CERO, Constantes.GUION);
    }
    
    /**
     * Retorna un String con la fecha en el formato
     * "DDMMYY" of the Current Date (now)
     * @return String date DDMMYY
     */
    public static String getCurrentDateDDMMYYString(){
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_DOS, Constantes.NRO_CERO, Constantes.VACIO);
    }
    /**
     * Retorna un String con la fecha en el formato
     * "YYMMDD" of the Current Date (now)
     * @return String date YYMMDD
     */
    public static String getCurrentDateYYMMDDString(){
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_TRES, Constantes.NRO_CERO, Constantes.VACIO);
    }

    /**
     * Retorna un String con la fecha en el formato
     * "DDMMYY" of the Current Date (now)
     * @return String date DD/MM/YY
     */
    public static String getCurrentDateDDMMYYStringSlash(){
        Calendar cal = Calendar.getInstance();
        return FechaUtil.calendarToString(cal, Constantes.NRO_DOS, Constantes.NRO_CERO, Constantes.SLASH);
    }
    /**
     * Retorna un String con la hora en el formato
     * "hh:mm:ss" de la fecha actual
     */
    public static String getCurrentHoursString(){
        String fecha = getCurrentDateTimeString();
        return fecha.split(Constantes.SPACE)[Constantes.NRO_UNO];
    }
    
	/**
	 * Devuelve el componente hora-minuto de un Timestamp (HH:mm)
	 * @author jespejo - Juan Carlos Espejo Gavilano
	 * @version v1.0, May 16, 2005
	 * @param t, la fecha a truncar
	 * @return un timestamp que solo contiene el componente HH:mm
	 */
	public static Timestamp truncateTimestampHHmm(Timestamp t){
	   GregorianCalendar gc1 = new GregorianCalendar();
	   GregorianCalendar gc2 = new GregorianCalendar();
	   int hour = Constantes.NRO_CERO;
	   int minute = Constantes.NRO_CERO;

	   gc1.clear();
	   gc1.setTime(t);
	   hour = gc1.get(Calendar.HOUR_OF_DAY);
	   minute = gc1.get(Calendar.MINUTE);
	   gc2.clear();
	   gc2.roll(Calendar.HOUR_OF_DAY, hour);
	   gc2.roll(Calendar.MINUTE, minute);
	   return (new Timestamp(gc2.getTime().getTime())); 
	}
	
	/**
	 * Devuelve el componente fecha de un Timestamp (dd/MM/yyyy)
	 * @author jespejo - Juan Carlos Espejo Gavilano
	 * @version v1.0, May 16, 2005
	 * @param t, la fecha a truncar
	 * @return un timestamp que solo contiene el componente dd/MM/yyyy
	 */
	public static Timestamp truncateTimestampddMMyyyy(Timestamp t){
	   GregorianCalendar gc1 = new GregorianCalendar();
	   GregorianCalendar gc2 = null;
	   int year = Constantes.NRO_CERO;
	   int month = Constantes.NRO_CERO;
	   int day = Constantes.NRO_CERO;

	   gc1.clear();
	   gc1.setTime(t);
	   year = gc1.get(Calendar.YEAR);
	   month = gc1.get(Calendar.MONTH);
	   day = gc1.get(Calendar.DAY_OF_MONTH);
	   gc2 = new GregorianCalendar(year, month, day);
	   return (new Timestamp(gc2.getTime().getTime())); 
	}

	/**
	 * Convierte un String que contiene una fecha a el tipo Timestamp
	 * @param s
	 * @param formato. ej: yyyyMMdd
	 * @return Timestamp
	 * @throws ParseException
	 */
	public static Timestamp stringYYYYMMDDToTimestamp(String s, String formato) throws ParseException{
		Timestamp outDate = null;
		if(s != null && s.length() == Constantes.NRO_OCHO){
			long time = new SimpleDateFormat(formato).parse(s).getTime();
			outDate = new Timestamp(time);
		}
		return outDate;
	}
	
	/**
	 * Convierte un String que contiene una fecha a el tipo Timestamp
	 * 
	 * @param s
	 * @param formato.
	 *            ej: yyyyMMdd
	 * @return Timestamp
	 * @throws ParseException
	 */
	public static Timestamp stringToTimestamp(String s, String formato) {
		Timestamp outDate = null;
		if (s != null) {
			try {
				long time = new SimpleDateFormat(formato).parse(s).getTime();
				outDate = new Timestamp(time);
			}catch (Exception e) {
				//e.printStackTrace();
				return null;
			}
		}
		return outDate;
	}
	
	public static String formatDate(String formato, Date fecha) {
		String salida = null;
		try {
			SimpleDateFormat fmtFecha = new SimpleDateFormat(formato);
			salida = fmtFecha.format(fecha);
		} catch (Exception e) {
			return null;
		}
		return salida;
	}

	public static String formatTimestamp(String formato, Timestamp timestamp) {
		String salida = null;
		try {
			SimpleDateFormat fmtFecha = new SimpleDateFormat(formato);
			salida = fmtFecha.format(timestamp);
		} catch (Exception e) {
			return null;
		}
		return salida;
	}
	
	/*
	 * Metodo que cambia el formato de fecha a un string
	 * 
	 * @param dateIn
	 * @param formatIn
	 * @param formatOut
	 *            
	 * @return String
	 *
	 */
	public static String formatDate(String dateIn, String formatIn, String formatOut){
		String dateOut;
		 try {
			 SimpleDateFormat formatterIn = new SimpleDateFormat(formatIn);
			 SimpleDateFormat formatterOut = new SimpleDateFormat(formatOut);
			 Date date = formatterIn.parse(dateIn);
			 dateOut = formatterOut.format(date);
		 } catch (ParseException e) {
			 dateOut = dateIn;
	     }
		return dateOut;
	}
	
	/**
	 * Resta dos fecha que son del tipo String con el mismo formato
	 * 
	 * @param fechaMinuendo
	 * @param formato1
	 * @param fechaSustraendo
	 * @param formato2
	 * @return Integer numero de dias
	 */
	public static Integer diferenciaDias(String fechaMinuendo, String formato1, String fechaSustraendo, String formato2){
		try{
			long dias = stringToTimestamp(fechaMinuendo, formato1).getTime() - stringToTimestamp(fechaSustraendo, formato2).getTime();
			dias = dias / 1000l / 3600l / 24l;
			return new Integer(String.valueOf(dias));
		}catch (Exception e) {
			return null;
		}
	}
	
	/**
	   Compara las Fechas (Formato "dd/mm/yyyy" o "dd-mm-yyyy")
	   y retorna un int con los posibles resultados:

	     -1 : DateA < DateB
	      0 : DateA = DateB
	      1 : DateA > DateB

	*/
	public static int comparaFechas(String DateA, String DateB){
	       
		if (DateA.compareTo(DateB) == Constantes.NRO_CERO){
	        return 0;
	    }

        String sYear = DateA.substring(6, 10);
	    String sMonth = DateA.substring(3, 5);
	    String sDay = DateA.substring(0, 2);

	    int iYear = Integer.parseInt(sYear);
	    int iMonth = Integer.parseInt(sMonth);
	    int iDay = Integer.parseInt(sDay);

	    iMonth--;
	    java.util.Calendar calendarA = java.util.Calendar.getInstance();
	    calendarA.set(iYear, iMonth, iDay);

	    sYear = DateB.substring(6, 10);
	    sMonth = DateB.substring(3, 5);
	    sDay = DateB.substring(0, 2);

	    iYear = Integer.parseInt(sYear);
	    iMonth = Integer.parseInt(sMonth);
	    iDay = Integer.parseInt(sDay);
	    iMonth--;
	    java.util.Calendar calendarB = java.util.Calendar.getInstance();
	    calendarB.set(iYear, iMonth, iDay);

	    if (calendarA.after(calendarB)){
	        return Constantes.NRO_UNO;
	    }else{
	        return Constantes.NRO_MENOSUNO;
	    }
	}
	   
	/**
	 * Suma o resta los días recibidos a la fecha  
	 */
	public static Date sumarRestarDiasFecha(Date fecha, int dias){
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTime(fecha); 
	      calendar.add(Calendar.DAY_OF_YEAR, dias);  
	    
	      return calendar.getTime(); 
	    
	}
	
	/**
     * Retorna un String con la fecha en el formato ingresado de la fecha actual 
     * @param format: formato de la fecha requerida
     * @return String
     * 
     */
	public static String getCurrentDateWithFormat (String format) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		
		return localDate.format(formatter);
	}
	
	public static boolean estaEnHora(String horaInicio, String horaFin, String horaActual) {
		boolean resp = false;
		try {
			LocalTime horaActualL = LocalTime.parse(horaActual); 
		    LocalTime horaInicioL = LocalTime.parse(horaInicio); 
		    LocalTime horaFinL = LocalTime.parse(horaFin); 
		    if(horaActualL.equals(horaInicioL) || horaActualL.equals(horaFinL) || (horaActualL.isAfter(horaInicioL) && horaActualL.isBefore(horaFinL))) {
		    	resp = true;
		    } else {
		    	resp = false;
		    }
		} catch(DateTimeParseException dte) {
			logger.traceInfo("estaEnHora", "Valide los parametros de la fecha puede que no tengan el formato esperado (" + Constantes.FECHA_FORMATO_HH_MI_SS + ") o DateTimeParseException: " + dte);
	    }catch(Exception e) {
	    	logger.traceInfo("estaEnHora", "Valide los parametros de la fecha puedbe que no tengan el formato esperado (" + Constantes.FECHA_FORMATO_HH_MI_SS + ") o Exception:" + e);
	    }
		return resp;
	}
	
	public static boolean horasMinutosIguales(String hora1, String hora2) {
		boolean resp = false;
		try {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constantes.FECHA_FORMATO_HH_MI);
			LocalTime hora1L = LocalTime.parse(hora1); 
		    LocalTime hora2L = LocalTime.parse(hora2); 
		    if(hora1L.format(dateTimeFormatter).equals(hora2L.format(dateTimeFormatter))) {
		    	resp = true;
		    } else {
		    	resp = false;
		    }
		} catch(DateTimeParseException dte) {
			logger.traceInfo("horasMinutosIguales", "Valide los parametros de la fecha puede que no tengan el formato esperado (" + Constantes.FECHA_FORMATO_HH_MI + ") o DateTimeParseException: " + dte);
	    }catch(Exception e) {
	    	logger.traceInfo("horasMinutosIguales", "Valide los parametros de la fecha puedbe que no tengan el formato esperado (" + Constantes.FECHA_FORMATO_HH_MI_SS + ") o Exception:" + e);
	    }
		return resp;
	}
}
