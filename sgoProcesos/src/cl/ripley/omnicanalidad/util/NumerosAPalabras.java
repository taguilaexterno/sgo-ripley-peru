package cl.ripley.omnicanalidad.util;

import java.math.BigDecimal;
import java.util.Locale;

public class NumerosAPalabras {
	
	private static final BigDecimal CERO = new BigDecimal("0");
	private static final BigDecimal DIEZ = new BigDecimal("10");
	private static final BigDecimal TREINTA = new BigDecimal("30");
	private static final BigDecimal CIEN = new BigDecimal("100");

	private static final String[] nombreNumeros = {
			Constantes.VACIO,
			"Uno",
			"Dos",
			"Tres",
			"Cuatro",
			"Cinco",
			"Seis",
			"Siete",
			"Ocho",
			"Nueve",
			"Diez",
			"Once",
			"Doce",
			"Trece",
			"Catorce",
			"Quince",
			"Deciseis",
			"Diecisiete",
			"Dieciocho",
			"Diecinueve",
			"Veinte",
			"Veintiuno",
			"Veintidos",
			"Veintitres",
			"Veinticuatro",
			"Veinticinco",
			"Veintiseis",
			"Veintisiete",
			"Veintiocho",
			"Veintinueve"
	};
	
	private static final String[] nombreDieces = {
		Constantes.VACIO,
		"Diez",
		"Veinte",
		"Treinta",
		"Cuarenta",
		"Cincuenta",
		"Sesenta",
		"Setenta",
		"Ochenta",
		"Noventa"
	};
	
	private static final String[] nombreCienes = {
		Constantes.VACIO,
		"Ciento",
		"Doscientos",
		"Trescientos",
		"Cuatrocientos",
		"Quinientos",
		"Seiscientos",
		"Setecientos",
		"Ochocientos",
		"Novecientos"
	};
	
	
	private static String convertirMenorQueMil(final BigDecimal numero) {
		
		if(numero.compareTo(CIEN) == Constantes.NRO_CERO) {
			return "Cien";
		}
		
		String enPalabras;
		
		BigDecimal nuevoNumero = numero;
		
		BigDecimal remainder = nuevoNumero.remainder(CIEN);
		
		if(remainder.compareTo(TREINTA) < Constantes.NRO_CERO) {
			
			enPalabras = nombreNumeros[remainder.intValue()];
			nuevoNumero = nuevoNumero.divide(CIEN);
			
		} else {
			remainder = nuevoNumero.remainder(DIEZ);
			enPalabras = nombreNumeros[remainder.intValue()];
			nuevoNumero = nuevoNumero.divide(DIEZ);
			
			remainder = nuevoNumero.remainder(DIEZ);
			enPalabras = nombreDieces[remainder.intValue()] + (Constantes.VACIO.equals(enPalabras) ? Constantes.VACIO : " y " + enPalabras);
			nuevoNumero = nuevoNumero.divide(DIEZ);
			
		}
		
		if(nuevoNumero.compareTo(CERO) == Constantes.NRO_CERO) {
			
			return enPalabras;
			
		}
		
		return nombreCienes[nuevoNumero.intValue()] + " " + enPalabras;
	}
	
	public static String convertir(BigDecimal number) {
		
		if(number.compareTo(CERO) == Constantes.NRO_CERO) {
			
			return "Cero";
			
		}
		
		String resultado = "";
		
		//0 a 999 999 999 999.999 999
		
		String numero = String.format(Locale.US, Constantes.FORMATO_2_DECIMALES, number);
		
		String[] num = numero.split(Constantes.PUNTO);
		
		String entero = String.format(Constantes.FORMATO_12_CEROS_IZQ, Long.valueOf(num[Constantes.NRO_CERO]));
		String decimales = num[Constantes.NRO_UNO];
		
		// XXXnnnnnnnnn
	    int milesMillones = Integer.parseInt(entero.substring(0,3));
	    // nnnXXXnnnnnn
	    int millones  = Integer.parseInt(entero.substring(3,6));
	    // nnnnnnXXXnnn
	    int cienMiles = Integer.parseInt(entero.substring(6,9));
	    // nnnnnnnnnXXX
	    int miles = Integer.parseInt(entero.substring(9,12));
	    
	    String milesMill;
	    switch(milesMillones) {
	    
	    case Constantes.NRO_CERO:
	    	milesMill = Constantes.VACIO;
	    	break;
	    	
	    case Constantes.NRO_UNO:
	    	milesMill = "Mil millones ";
	    	break;
	    	
	    default:
	    	milesMill = convertirMenorQueMil(new BigDecimal(milesMillones)) + " mil millones ";
	    	break;
	    
	    }
	    
	    resultado = milesMill;
	    
	    String mill;
	    switch(millones) {
	    
	    case Constantes.NRO_CERO:
	    	mill = Constantes.VACIO;
	    	break;
	    	
	    case Constantes.NRO_UNO:
	    	mill = "Un millón ";
	    	break;
	    	
	    default:
	    	mill = convertirMenorQueMil(new BigDecimal(millones)) + " millones ";
	    	break;
	    
	    }
	    
	    resultado += mill;
	    
	    String cienMil;
	    switch(cienMiles) {
	    
	    case Constantes.NRO_CERO:
	    	cienMil = Constantes.VACIO;
	    	break;
	    	
	    case Constantes.NRO_UNO:
	    	cienMil = "Mil ";
	    	break;
	    	
	    default:
	    	cienMil = convertirMenorQueMil(new BigDecimal(cienMiles)) + " mil ";
	    	break;
	    
	    }
	    
	    resultado += cienMil;
	    
	    String mil = convertirMenorQueMil(new BigDecimal(miles));
	    
	    resultado += mil;
	    
	    //Decimales
	    BigDecimal decimals = new BigDecimal(decimales);
	    
	    if(decimals.compareTo(CERO) > Constantes.NRO_CERO) {
	    
	    	resultado +=  " Y " + decimals + "/" + Constantes.NRO_CIEN;
	   
	    } else if (decimals.compareTo(CERO) == Constantes.NRO_CERO) {
	   
	    	resultado +=  " Y " + Constantes.STRING_CEROX2 + "/" + Constantes.NRO_CIEN;
	   
	    }
	    
	    //Capitalize Primera Letra
	    resultado = resultado.trim().toUpperCase();
	    resultado = resultado.substring(Constantes.NRO_CERO, Constantes.NRO_UNO).toUpperCase() + resultado.substring(Constantes.NRO_UNO);
		
		return resultado;
	}

}
