package cl.ripley.omnicanalidad.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import java.sql.ResultSet;
import java.sql.Statement;

public class PoolBDs {

    private static final AriLog logger = new AriLog(PoolBDs.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    private static DataSource DTSR_TRE = null;
    private static DataSource DTSR_BO = null;
    private static DataSource DTSR_BT = null;
    private static DataSource DTSR_CLUB = null;
    private static DataSource DTSR_CAR = null;
    private static DataSource DTSR_ME = null;

    private static Properties propiedades = null;
    private static String driver = null;
    private static int maxActivos = Constantes.NRO_CERO;
    private static int maxIdle = Constantes.NRO_CERO;
    private static int minIdle = Constantes.NRO_CERO;
    private static int initialSize = Constantes.NRO_CERO;
    private static String validationQuery = null;
    private static boolean testOnBorrow = Constantes.VERDADERO;
    private static int maxWaitMillis = Constantes.NRO_CIEN;

    public static void OpenConnections(Properties prop) throws AligareException {
        logger.initTrace("OpenConnections", "Properties prop");
        //		propiedades = prop;
        //		driver = propiedades.getProperty("driverClassName");
        //        maxActivos = Integer.parseInt(propiedades.getProperty("maxTotal"));
        //        maxIdle = Integer.parseInt(propiedades.getProperty("maxIdle"));
        //        minIdle = Integer.parseInt(propiedades.getProperty("minIdle"));
        //        initialSize = Integer.parseInt(propiedades.getProperty("initialSize"));
        //        validationQuery = propiedades.getProperty("validationQuery");
        //        testOnBorrow = Boolean.parseBoolean(propiedades.getProperty("testOnBorrow"));
        //        maxWaitMillis = Integer.parseInt(propiedades.getProperty("maxWaitMillis"));
        //    	initDSTRE();
        //    	initDSBO();
        //    	initDSBT();
        //    	initDSCLUB();
        //    	initDSCAR();
        initDSME();
        logger.endTrace("OpenConnections", "Finalizado", null);
    }

    public static void CloseConnections() {
        closeDSTRE();
        closeDSBO();
        closeDSBT();
        closeDSCLUB();
        closeDSCAR();
        closeDSME();
    }

    private static void initDSTRE() throws AligareException {
        //logger.initTrace("initDSTRE", null, new KeyLog("BD", "TRE"), new KeyLog("URL", propiedades.getProperty("url1")), new KeyLog("USER",propiedades.getProperty("username1")), new KeyLog("PASS",propiedades.getProperty("password1")));
//		try {
//			BasicDataSource basicDataSource = new BasicDataSource();
//			basicDataSource.setMaxActive(maxActivos);
//			basicDataSource.setMaxIdle(maxIdle);
//			basicDataSource.setDriverClassName(driver);
//			basicDataSource.setMinIdle(minIdle);
//			basicDataSource.setInitialSize(initialSize); 
//			basicDataSource.setValidationQuery(validationQuery);
//			basicDataSource.setTestOnBorrow(testOnBorrow); 
//			basicDataSource.setMaxWait(maxWaitMillis);
//			basicDataSource.setUsername(propiedades.getProperty("username1"));
//			basicDataSource.setPassword(propiedades.getProperty("password1"));
//			basicDataSource.setUrl(propiedades.getProperty("url1"));
//			DTSR_TRE = basicDataSource;
//		} catch (Exception ex) { 
//			logger.traceError("initDSTRE", ex);
//			throw new AligareException("Imposible realizar conexion a 'TRE' en estos momentos.");
//		}

        //logger.endTrace("initDSTRE","Finalizado", null);
    }

    private static void initDSBO() throws AligareException {
        //logger.initTrace("initDSBO", null, new KeyLog("BD", "Back Office"), new KeyLog("URL", propiedades.getProperty("url2")), new KeyLog("USER",propiedades.getProperty("username2")), new KeyLog("PASS",propiedades.getProperty("password2")));
//		try {
//			BasicDataSource basicDataSource = new BasicDataSource();
//			basicDataSource.setMaxActive(maxActivos);
//			basicDataSource.setMaxIdle(maxIdle);
//			basicDataSource.setDriverClassName(driver);
//			basicDataSource.setMinIdle(minIdle);
//			basicDataSource.setInitialSize(initialSize); 
//			basicDataSource.setValidationQuery(validationQuery);
//			basicDataSource.setTestOnBorrow(testOnBorrow); 
//			basicDataSource.setMaxWait(maxWaitMillis);
//			basicDataSource.setUsername(propiedades.getProperty("username2"));
//			basicDataSource.setPassword(propiedades.getProperty("password2"));
//			basicDataSource.setUrl(propiedades.getProperty("url2"));
//			DTSR_BO = basicDataSource;
//		} catch (Exception ex) { 
//			logger.traceError("initDSBO", ex);
//			throw new AligareException("Imposible realizar conexion a 'BACK OFFICE' en estos momentos.");
//		}

        //logger.endTrace("initDSBO","Finalizado", null);
    }

    private static void initDSBT() throws AligareException {
        //logger.initTrace("initDSBT", null, new KeyLog("BD", "Big Ticket"), new KeyLog("URL", propiedades.getProperty("url4")), new KeyLog("USER",propiedades.getProperty("username4")), new KeyLog("PASS",propiedades.getProperty("password4")));
//		try {
//			BasicDataSource basicDataSource = new BasicDataSource();
//			basicDataSource.setMaxActive(maxActivos);
//			basicDataSource.setMaxIdle(maxIdle);
//			basicDataSource.setDriverClassName(driver);
//			basicDataSource.setMinIdle(minIdle);
//			basicDataSource.setInitialSize(initialSize); 
//			basicDataSource.setValidationQuery(validationQuery);
//			basicDataSource.setTestOnBorrow(testOnBorrow); 
//			basicDataSource.setMaxWait(maxWaitMillis);
//			basicDataSource.setUsername(propiedades.getProperty("username4"));
//			basicDataSource.setPassword(propiedades.getProperty("password4"));
//			basicDataSource.setUrl(propiedades.getProperty("url4"));
//			DTSR_BT = basicDataSource;
//		} catch (Exception ex) { 
//			logger.traceError("initDSBT", ex);
//			throw new AligareException("Imposible realizar conexion a 'BIG TICKET' en estos momentos.");
//		}

        //logger.endTrace("initDSBT","Finalizado", null);
    }

    private static void initDSCLUB() throws AligareException {
        //logger.initTrace("initDSCLUB", null, new KeyLog("BD", "Clubes"), new KeyLog("URL", propiedades.getProperty("url6")), new KeyLog("USER",propiedades.getProperty("username6")), new KeyLog("PASS",propiedades.getProperty("password6")));
//		try {
//			BasicDataSource basicDataSource = new BasicDataSource();
//			basicDataSource.setMaxActive(maxActivos);
//			basicDataSource.setMaxIdle(maxIdle);
//			basicDataSource.setDriverClassName(driver);
//			basicDataSource.setMinIdle(minIdle);
//			basicDataSource.setInitialSize(initialSize); 
//			basicDataSource.setValidationQuery(validationQuery);
//			basicDataSource.setTestOnBorrow(testOnBorrow); 
//			basicDataSource.setMaxWait(maxWaitMillis);
//			basicDataSource.setUsername(propiedades.getProperty("username6"));
//			basicDataSource.setPassword(propiedades.getProperty("password6"));
//			basicDataSource.setUrl(propiedades.getProperty("url6"));
//			DTSR_CLUB = basicDataSource;
//		} catch (Exception ex) { 
//			logger.traceError("initDSCLUB", ex);
//			throw new AligareException("Imposible realizar conexion a 'CLUBES' en estos momentos.");
//		}

        //logger.endTrace("initDSCLUB","Finalizado", null);
    }

    private static void initDSCAR() throws AligareException {
        //logger.initTrace("initDSCAR", null, new KeyLog("BD", "CAR"), new KeyLog("URL", propiedades.getProperty("url3")), new KeyLog("USER",propiedades.getProperty("username3")), new KeyLog("PASS",propiedades.getProperty("password3")));
//		try {
//			BasicDataSource basicDataSource = new BasicDataSource();
//			basicDataSource.setMaxActive(maxActivos);
//			basicDataSource.setMaxIdle(maxIdle);
//			basicDataSource.setDriverClassName(driver);
//			basicDataSource.setMinIdle(minIdle);
//			basicDataSource.setInitialSize(initialSize); 
//			basicDataSource.setValidationQuery(validationQuery);
//			basicDataSource.setTestOnBorrow(testOnBorrow); 
//			basicDataSource.setMaxWait(maxWaitMillis);
//			basicDataSource.setUsername(propiedades.getProperty("username3"));
//			basicDataSource.setPassword(propiedades.getProperty("password3"));
//			basicDataSource.setUrl(propiedades.getProperty("url3"));
//			DTSR_CAR = basicDataSource;
//		} catch (Exception ex) { 
//			logger.traceError("initDSCAR", ex);
//			throw new AligareException("Imposible realizar conexion a 'CAR' en estos momentos.");
//		}

        //logger.endTrace("initDSCAR","Finalizado", null);
    }

    private static void initDSME() throws AligareException {
        //logger.initTrace("initDSME", null, new KeyLog("BD", "Modelo Extendido"), new KeyLog("URL", propiedades.getProperty("url5")), new KeyLog("USER",propiedades.getProperty("username5")), new KeyLog("PASS",propiedades.getProperty("password5")));
        try {
            //            BasicDataSource basicDataSource = new BasicDataSource();
            //            basicDataSource.setMaxActive(maxActivos);
            //            basicDataSource.setMaxIdle(maxIdle);
            //            basicDataSource.setDriverClassName(driver);
            //            basicDataSource.setMinIdle(minIdle);
            //            basicDataSource.setInitialSize(initialSize); 
            //            basicDataSource.setValidationQuery(validationQuery);
            //            basicDataSource.setTestOnBorrow(testOnBorrow); 
            //            basicDataSource.setMaxWait(maxWaitMillis);
            //            basicDataSource.setUsername(propiedades.getProperty("username5"));
            //            basicDataSource.setPassword(propiedades.getProperty("password5"));
            //            basicDataSource.setUrl(propiedades.getProperty("url5"));
            DTSR_ME = AppContextUtils.getApplicationContext().getBean(DataSource.class);
        } catch (Exception ex) {
            logger.traceError("initDSME", ex);
            throw new AligareException("Imposible realizar conexion a 'MODELO EXTENDIDO' en estos momentos.");
        }

        //logger.endTrace("initDSME","Finalizado", null);
    }

    private static void closeDSTRE() {
        DTSR_TRE = null;
    }

    private static void closeDSBO() {
        DTSR_BO = null;
    }

    private static void closeDSCAR() {
        DTSR_CAR = null;
    }

    private static void closeDSBT() {
        DTSR_BT = null;
    }

    private static void closeDSME() {
        DTSR_ME = null;
    }

    private static void closeDSCLUB() {
        DTSR_CLUB = null;
    }

    public static Connection getConnection(BDType tipo) {
        logger.initTrace("getConnection", "BDType: " + tipo.name(), new KeyLog("BDType", tipo.name()));
        Connection conn = null;
        try {
            if (tipo.equals(BDType.MODEL_EXTEND)) {
                if (DTSR_ME == null) {
                    initDSME();
                }
                conn = DTSR_ME.getConnection();
            }
        } catch (Exception ex) {
            logger.traceInfo("getConnection", "Error al intentar obtener conexion[" + tipo.name() + "][intento 1]: " + ex.getMessage(), new KeyLog("BDType", tipo.name()));
        }
        logger.endTrace("getConnection", "Finalizado", "Connection: conn[" + tipo.name() + "]");
        return conn;
    }

    public static void closeConnection(Connection conn) throws AligareException {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
            logger.traceError("closeConnection", "Error al intentar cerrar conexion.", e);
        }
    }

    public static void closeStatement(Statement stmt) throws AligareException {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
            logger.traceError("closeStatement", "Error al intentar cerrar statement.", e);
        }
    }

    public static void closeResultSet(ResultSet rs) throws AligareException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            logger.traceError("closeResultSet", "Error al intentar cerrar resultSet.", e);
        }
    }

    public static void commitMESpringTrx() {

        logger.initTrace("commitMESpringTrx", Constantes.VACIO);

        Connection conn = DataSourceUtils.getConnection(DTSR_ME);
        try {
            conn.commit();
        } catch (SQLException e) {
            logger.traceError("commitMESpringTrx", "Error al hace commit.", e);
        }

        logger.endTrace("commitMESpringTrx", "Finalizado", "Connection: conn[DTSR_ME]");
    }

}
