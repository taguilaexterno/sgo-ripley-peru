/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ripley.omnicanalidad.util;

import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import java.util.function.Predicate;

/**
 *
 * @author dams
 */
public class PredicadosProcesoBoleta {

    private static String secureTrimString(String param) {
        if (param != null) {
            return param.trim();
        } else {
            return param;
        }
    }

    private static boolean esDescripcionExtraGarantia(String param) {
        boolean flag = false;
        if (param != null) {
            flag = param.trim().toUpperCase().startsWith(Constantes.PREFIJO_EXTRA_GARANTIA);
        }
        return flag;
    }
    
    private static boolean esDescripcionCostoDeEnvio(String param) {
        boolean flag = false;
        if (param != null) {
            flag = Constantes.TRAMA_COSTO_ENVIO.equalsIgnoreCase(param.trim());
        }
        return flag;
    }
 
    public static Predicate<ArticuloVentaTramaDTE> obtenerPredicadoEsCostoEnvio() {
        return element -> esDescripcionCostoDeEnvio(element.getArticuloVenta().getDescRipley());
    }

    public static Predicate<ArticuloVentaTramaDTE> obtenerPredicadoEsExtraGarantia() {
        return element
                -> esDescripcionExtraGarantia(element.getArticuloVenta().getDescRipley());
    }

    public static Predicate<ArticuloVentaTramaDTE> obtenerPredicadoEsMarketPlaceRipley() {
        return element
                -> element.getIdentificadorMarketplace().getEsMkp() == Constantes.ES_MKP_RIPLEY;
    }

}
