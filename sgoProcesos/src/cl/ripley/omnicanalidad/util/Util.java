package cl.ripley.omnicanalidad.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.amazonaws.regions.Regions;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;

/**Clase con métodos utilitarios para la aplicación.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class Util {
	private static final AriLog logger = new AriLog(Util.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	/**Reemplaza por caracteres compatibles.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param cadena
	 * @return
	 */
	public static String cambiaCaracteres(String cadena){
	     String res = Constantes.VACIO;
	     if(cadena != null){
	       for (int x=Constantes.NRO_CERO;x<cadena.length();x++){
	         char a = cadena.charAt(x);
	         int ascii = cadena.codePointAt(x);
	         if(ascii <= Constantes.NRO_TREINTADOS || ascii == Constantes.NRO_TREINTACUATRO || ascii == Constantes.NRO_TREINTANUEVE || 
	        		 ascii == Constantes.NRO_NOVENTASEIS || ascii == Constantes.NRO_CIENVEINTECUATRO || ascii == Constantes.NRO_CIENVEINTESIETE || ascii == Constantes.NRO_CIENOCHENTA){
	             res = res+Constantes.SPACE;
	         }else{
	             res = res+a;
	         }
	       }
	     }
	     
	     return res;
	}
	
	/**Rellena a la izquierda con el caracter indicado.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param cadena
	 * @param largo
	 * @param lpad
	 * @param caracter
	 * @return
	 */
	public static String rellenarString(String cadena, int largo, boolean lpad, char caracter){
		if(cadena !=null && largo > cadena.length()){
			int nro = largo - cadena.length();
			for (int i = Constantes.NRO_CERO; i < nro; i++) {
				if(lpad){
					cadena = caracter + cadena;
				}else{
					cadena = cadena + caracter;
				}
			}
		}
		
		return cadena;
	}
	
	/**Obtiene el dígito verificador del RUT
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param rut
	 * @return
	 */
	public static String getDigitoValidadorRut(String rut){
        int cantidad = rut.length();
        if (cantidad == Constantes.NRO_CERO) return Constantes.VACIO;
        int factor = Constantes.NRO_DOS;
        int suma = Constantes.NRO_CERO;
        String verificador = Constantes.VACIO;
        for(int i = cantidad; i > Constantes.NRO_CERO; i--){ 
            if(factor > Constantes.NRO_SIETE){ 
                factor = Constantes.NRO_DOS;
            }
            suma += (Integer.parseInt(rut.substring((i-Constantes.NRO_UNO), i)))*factor;
            factor++;
        }
        verificador = String.valueOf(Constantes.NRO_ONCE - suma%Constantes.NRO_ONCE);
        
        if(verificador.equals("10")){
        	return Constantes.K;
        } else if (verificador.equals("11")){
        	return Constantes.STRING_CERO;
        } else {
        	return verificador;
        }
	}
	
	/**Obtiene el dígito verificador del RUT
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param rut
	 * @return
	 */
	public static String getDigitoValidadorRut2(String rut){
        int cantidad = rut.length();
        if (cantidad == Constantes.NRO_CERO) return Constantes.VACIO;
        int factor = Constantes.NRO_DOS;
        int suma = Constantes.NRO_CERO;
        String verificador = Constantes.VACIO;
        for(int i = cantidad; i > Constantes.NRO_CERO; i--){ 
            if(factor > Constantes.NRO_SIETE){ 
                factor = Constantes.NRO_DOS;
            }
            suma += (Integer.parseInt(rut.substring((i-Constantes.NRO_UNO), i)))*factor;
            factor++;
        }
        verificador = String.valueOf(Constantes.NRO_ONCE - suma%Constantes.NRO_ONCE);
        
        if(verificador.equals("10")){
        	return String.valueOf(Constantes.NRO_ONCE);
        } else if (verificador.equals("11")){
        	return Constantes.STRING_CEROX2;
        } else {
        	return Constantes.STRING_CERO.concat(verificador);
        }
	}
	
	/**Retorna un String vacío si el String proporcionado es null.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param dato
	 * @return
	 */
	public static String getVacioPorNulo(String dato){
		if (dato == null)
			return Constantes.VACIO;
		else
			return dato;
	}
	
	/**
	 * Metodo que devuelve el RUT en formato xxx.xxx.xxx-K
	 * 
	 * @author FRiveros
	 * Oct 21, 2016
	 * @param rut
	 * @return rutFormateado
	 */
	public static String formatoRUT(String rut) {
		String rutFormateado = "";
		int longitud = 0, indice;
		String digitoVerif, rutAux, punto = Constantes.PUNTO;
		boolean termino = false;
		
		rut = rut.replaceAll("[.]", Constantes.VACIO);
		rut = rut.replaceAll("[-]", Constantes.VACIO);
		
		if(rut != null) {
			longitud = rut.length();
			rutAux = rut.substring(0,longitud - 1);
			digitoVerif = rut.substring(longitud - 1);
			rutFormateado += Constantes.GUION + digitoVerif;		
			while(!termino) {
				longitud = rutAux.length();			
				if(longitud <= 3) {
					//punto = "";
					termino = true;
					indice = 0;
				}
				else {
					indice = longitud - 3;
				}
				rutFormateado = punto + rutAux.substring(indice) + rutFormateado;
				rutAux = rutAux.substring(0,indice);
			}
			rutFormateado= rutFormateado.substring(1,rutFormateado.length());
		}		
		return rutFormateado;
	}

    public static void pasarGarbageCollector(){
        Runtime garbage = Runtime.getRuntime();
        garbage.gc();
        logger.traceInfo("pasarGarbageCollector", "Garbage collector realizado");
    }

    public static String quitarSaltos(String cadena) {
    	  return cadena.replaceAll("\n", ""); 
    }
    
	public static String quitarEspacios(String numero) {
		logger.initTrace("quitarEspacios", "String numero:" + numero);
		String num = Constantes.STRING_CERO;
		try {
			num = (numero!=null)?numero.replaceAll("\\s",""):Constantes.STRING_CERO;
		}catch(Exception e) {
			logger.traceInfo("quitarEspacios", "numero: " + numero + " Error exception: " + e);
		}
		logger.endTrace("quitarEspacios", "num", num);
		return num;
	}
    
    /**Metodo que extrae el numero del error asociado al retorno del metodo Nullify de Transbank
     * Si no puede parsear ningun codigo asignara por defecto el valor 999 (ver ErroresTransbank.java)
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param entrada
     * @return
     */
    public int formateaCodigoErrorTransbank (String entrada){
    	
    	int codigo = 0;	
    	try {
    		codigo = entrada.indexOf("(");
    		if(codigo < 0){
    			codigo = 999;
    		}
    		else{
    			String cod="";
    			cod = entrada.substring(codigo + 1, codigo + 4);
    			codigo = Integer.valueOf(cod);
    		}
    		
		} catch (Exception e) {
			codigo = 999;
		}
    	return codigo;
    }
    
    /**Obtiene el nombre de la forma de pago de la Orden de Compra.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @return
     */
    public static String getFormaPago(TramaDTE trama) {

    	String formaPago = Constantes.VACIO;

    	if(trama.getTarjetaRipley().getCorrelativoVenta() != null 
    			&& trama.getTarjetaRipley().getCorrelativoVenta().intValue() != Constantes.NRO_CERO) {

    		formaPago = Constantes.FORMA_PAGO_TARJETA_RIPLEY;

    	} else if(trama.getTarjetaBancaria().getCorrelativoVenta() != null 
    			&& trama.getTarjetaBancaria().getCorrelativoVenta().intValue() != 0) {

    		formaPago = trama.getTarjetaBancaria().getDescripcionFormaPago();


    	}

    	return formaPago;

    }
    
    /**Obtiene el Código único de despacho del articulo de venta proporcionado.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param art
     * @return
     */
    public static String getCud(ArticuloVentaTramaDTE art) {
    	String cud = Constantes.VACIO;
    	String codDespacho = Constantes.VACIO;
    	
    	if(art.getArticuloVenta().getCodDespacho()!=null) {
    		codDespacho = art.getArticuloVenta().getCodDespacho().trim();
    	}
    	
    	BigInteger cdes = !Constantes.VACIO.equals(codDespacho) ? new BigInteger(codDespacho) : BigInteger.ZERO;
    	
    	if(!Constantes.VACIO.equals(codDespacho)
    			&& cdes.compareTo(BigInteger.ZERO) == Constantes.NRO_CERO) {
    		
    		cud = String.format(Constantes.FORMATO_22_CEROS_IZQ, cdes);
    		
    	} else if (codDespacho.length() < Constantes.NRO_VEINTIDOS) {
			if(art.getArticuloVenta().getCodDespacho()!=null) {
				cud += codDespacho.substring(Constantes.NRO_CERO, Constantes.NRO_DIECISEIS) + 
						Constantes.NRO_CERO + 
						codDespacho.substring(Constantes.NRO_DIECISEIS);
			}
		}
		else {
			
			cud = codDespacho;
		}
		
		return cud;
    }
    /**Obtiene el subtotal de la Orden de Compra de acuerdo al flag esMkp.
     * Si esMkp = 1 entonces calcula el subtotal de los artículos Marketplace.
     * Si esMkp = 0 entonces calcula el subtotal de los artículos distintos a Marketplace.
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 21-02-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param trama
     * @param esMkp
     * @return
     * @throws AligareException
     */
    public static BigDecimal getSubtotal(TramaDTE trama, Integer esMkp) throws AligareException{

		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		
		BigDecimal acumulado =	articuloVentas.stream()
								.filter(articuloVentaTrama -> articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp.intValue())
								.map(articuloVentaTrama -> articuloVentaTrama.getArticuloVenta().getPrecio()
										.multiply(BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades()))
										.subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))
								.reduce(BigDecimal.ZERO, BigDecimal::add);

		return acumulado;
	}
    
    public static BigDecimal getSubtotalAfecto(TramaDTE trama, Integer esMkp, Parametros parametros) throws AligareException{

		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		BigDecimal iva =  new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
		BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
		
		BigDecimal acumulado =	articuloVentas.stream()
								.filter(articuloVentaTrama -> articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp.intValue()
								&& articuloVentaTrama.getArticuloVenta().getPrecio()
								.subtract(articuloVentaTrama.getArticuloVenta().getPrecio()
										.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP)).compareTo(Constantes.NRO_CERO_COMA_CEROCERO_B)!=0
										)
								.map(articuloVentaTrama ->  
										articuloVentaTrama.getArticuloVenta().getPrecio()
										.multiply(BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades()))
										.subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento())
										)
								.reduce(BigDecimal.ZERO, BigDecimal::add);

		return acumulado;
	}
    
    public static BigDecimal getSubtotalInAfecto(TramaDTE trama, Integer esMkp, Parametros parametros) throws AligareException{

		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		BigDecimal iva =  new BigDecimal(parametros.buscaValorPorNombre("VALORIVA"));
		BigDecimal tasa_iva = iva.divide(new BigDecimal(100));
		
		BigDecimal acumulado =	articuloVentas.stream()
								.filter(articuloVentaTrama -> articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp.intValue()
								&& articuloVentaTrama.getArticuloVenta().getPrecio()
								.subtract(articuloVentaTrama.getArticuloVenta().getPrecio()
										.divide(tasa_iva.add(new BigDecimal(1)), 2, RoundingMode.HALF_UP)).compareTo(Constantes.NRO_CERO_COMA_CEROCERO_B)== 0
										)
								.map(articuloVentaTrama ->  
										articuloVentaTrama.getArticuloVenta().getPrecio()
										.multiply(BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades()))
										.subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento())
										)
								.reduce(BigDecimal.ZERO, BigDecimal::add);

		return acumulado;
	}
    
    public static BigDecimal getSubtotalNC(TramaDTE trama, Integer esMkp) throws AligareException{

		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		
		BigDecimal acumulado =	articuloVentas.stream()
								.filter(articuloVentaTrama -> articuloVentaTrama.getIdentificadorMarketplace().getEsMkp().intValue() == esMkp.intValue() 
																&& Constantes.NRO_UNO == articuloVentaTrama.getArticuloVenta().getEsNC())
								.map(articuloVentaTrama -> articuloVentaTrama.getArticuloVenta().getPrecio()
										.multiply(BigDecimal.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades()))
										.subtract(articuloVentaTrama.getArticuloVenta().getMontoDescuento()))
								.reduce(BigDecimal.ZERO, BigDecimal::add);

		return acumulado;
	}
    
    /**Limpia el rut quitándole el guión y el dígito verificador
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 30-04-2018
     *<br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     * @param rut
     * @return
     */
    public static String cleanRut(String rut) {
    	
    	String ruc = Constantes.VACIO;
    	
    	int indexGuion = rut.indexOf(Constantes.GUION);
    	
    	ruc = rut.substring(Constantes.NRO_CERO, indexGuion > Constantes.NRO_MENOSUNO ? indexGuion : rut.length());
    	
    	return ruc;
    	
    }
    
    /**Obtiene el PAN del medio de pago de la orden de compra.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param trama
	 * @return
	 */
	public static String getPan(TramaDTE trama) {
		
		String codBin = trama.getTarjetaBancaria().getBinNumber();
		String codigoAutorizadorDoc = trama.getTarjetaBancaria().getCodigoAutorizador();
		String ultDigito = trama.getTarjetaBancaria().getUltimosDigitosTarjeta();
		String panDoc = Constantes.VACIO;
		String formaPago = getFormaPago(trama);
		
		if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
			
			panDoc = trama.getTarjetaRipley().getPan().toString();
			
			//verifico tarjeta clasica
			if(panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_CINCO).equals("96041")) {
				
				panDoc = panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_CINCO) + "*******" + panDoc.substring(panDoc.length() - 4);
				
			} else {
				
				panDoc = panDoc.substring(Constantes.NRO_CERO, Constantes.NRO_SEIS) + "******" + panDoc.substring(panDoc.length() - 4);
				
			}
			
		} else if(Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)) {
			//efectivo
			String codAutoriza = codigoAutorizadorDoc.substring(codigoAutorizadorDoc.length() - Constantes.NRO_SIETE);
			panDoc = codBin + "999" + codAutoriza;
			
		} else if(Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
			//paypal
			panDoc = codBin + "******" + ultDigito;
			
		} else if(Constantes.FORMA_PAGO_TDC.equals(formaPago)
				|| Constantes.FORMA_PAGO_DEBITO.equals(formaPago)) {
			// tarjeta bancaria
			panDoc = codBin + "******" + ultDigito;
			
		}
		
		return panDoc;
		
		
	}
	
	/**Obtiene un Optional de String a partir de un token de la trama que se envia a PPL
	 *
	 * @param trama
	 * @param token ej: EN|
	 * @param pos index en que se encuentra el valor en la línea.
	 * @return
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 02-05-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	public static Optional<String> getValueFromPPLTrama(String trama, String token, int pos) {
		
		Optional<String> optional = Optional.empty();

		if(trama.contains(token)) {
			int posTrama = trama.indexOf(token);
			String tramaAux = trama.substring(posTrama,trama.length());
			
			try(Reader r = new StringReader(tramaAux);
				BufferedReader reader = new BufferedReader(r)) {
			
				optional =	reader.lines()
						.filter(it -> it.startsWith(token))
						.map(it -> (it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_PERSONALIZADO)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_ENCABEZADO)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DETALLE)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DN)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DR)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_REFERENCIA)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TARJETA_MERCADO_PAGO)
									||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DI))? 
										it.split("\\|")[pos].substring(0, it.split("\\|")[pos].length() - 3) : 
											(it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_PES)
													||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DOC))? 
													it.split("\\|")[pos].substring(0, it.split("\\|")[pos].length() - 4) : 
														(it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_PESD)
																||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DETALLE_DESCRIPCION_ITEM)
																||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DETALLE_DESCUENTOS_RECARGOS)
																||it.split("\\|")[pos].endsWith(Constantes.ESPACIO + Constantes.TRAMA_DETALLE_IMPUESTO))? 
																		it.split("\\|")[pos].substring(0, it.split("\\|")[pos].length() - 5) : it.split("\\|")[pos])
						.findFirst();
			} catch (IOException e) {
				optional = Optional.empty();
			}
		}else {
			optional = Optional.empty();
		}
		
		return optional;
		
	}
	
	public static String validarCodigoSunat(String codigoSunat, String codigoDefault) {
		if(codigoSunat == null) {
			codigoSunat = codigoDefault;
		} else {
			if(Constantes.NRO_OCHO > codigoSunat.length()) {
				codigoSunat = Util.rellenarString(codigoSunat, Constantes.NRO_OCHO, Constantes.FALSO, Constantes.CHAR_CERO);
			} else {
				codigoSunat = codigoSunat.substring(Constantes.NRO_CERO, Constantes.NRO_OCHO);	
			}
		}
		return codigoSunat;
	}

	/**Formatea milisegundos para registrar en TraceRpos
	 *
	 * @param Date
	 * @param Date
	 * @return String
	 *
	 * @author Pablo Matias Osorio (Aligare).
	 * @since 08-10-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	public static String formatMiliseg(Date fechaInicio, Date fecha) {
		String miliseg = "";
		if(fecha != null) {
			String fec= String.valueOf(fecha.getTime() - fechaInicio.getTime());
			if(fec.length() == 1) 
				miliseg += Constantes.STRING_CEROX3+fec;
			else if(fec.length() == 2)
				miliseg += Constantes.STRING_CEROX2+fec;
			else if(fec.length() == 3) 
				miliseg += Constantes.STRING_CERO+fec;
			else 
				miliseg += fec;
		}
		else 
			miliseg += Constantes.STRING_CERO;
		return miliseg;
	}
	
	public static Long validaLongCeroXNull(long numero){
		Long dato = null;
		
		if (numero == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = Long.valueOf(numero);
		}
		
		return dato;
	}
	
	public static String formatoRUT2(String rut) {
		String rutFormateado = "";
		int longitud = 0;
		String digitoVerif, rutAux;
		rut = rut.replaceAll("[.]", Constantes.VACIO);
		rut = rut.replaceAll("[-]", Constantes.VACIO);
		if(rut != null) {
			longitud = rut.length();
			rutAux = rut.substring(0,longitud - 1);
			digitoVerif = rut.substring(longitud - 1);
			rutFormateado = rutAux + Constantes.GUION + digitoVerif;		
		}		
		return rutFormateado;
	}
	
	public static String formatNumber2(Object number) {
		BigDecimal num = null;
		if(number instanceof String) {
			num = new BigDecimal((String) number);
		} else {
			num = (BigDecimal) number;
		}
//		DecimalFormat format = new DecimalFormat("\u00A4#,##0.00");
		DecimalFormat format = new DecimalFormat("##0.00");
		format.setMaximumFractionDigits(2);
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//		symbols.setCurrencySymbol("S/");
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		symbols.setMonetaryDecimalSeparator('.');
		format.setDecimalFormatSymbols(symbols);
		format.setDecimalSeparatorAlwaysShown(Boolean.FALSE);
		String formateado = format.format(num != null ? num : 0);
		return formateado;
	}
	
	private final static String[] UNIDADES = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
    private final static String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
        "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
        "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
    private final static String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
        "setecientos ", "ochocientos ", "novecientos "};


    public static String MontoEscrito(String numero, boolean mayusculas) {
        String literal = "";
        //si el numero utiliza (.) en lugar de (,) -> se reemplaza
        numero = numero.replace(".", ",");
        //si el numero no tiene parte decimal, se le agrega ,00
        if(numero.indexOf(",")==-1){
            numero = numero + ",00";
        }
        //se valida formato de entrada -> 0,00 y 999 999 999,00
        if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {
            //se divide el numero 0000000,00 -> entero y decimal
            String Num[] = numero.split(",");            
            //se convierte el numero a literal
            if (Integer.parseInt(Num[0]) == 0) {//si el valor es cero
                literal = "cero ";
            } else if (Integer.parseInt(Num[0]) > 999999) {//si es millon
                literal = getMillones(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 999) {//si es miles
                literal = getMiles(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 99) {//si es centena
                literal = getCentenas(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 9) {//si es decena
                literal = getDecenas(Num[0]);
            } else {//sino unidades -> 9
                literal = getUnidades(Num[0]);
            }
            //devuelve el resultado en mayusculas o minusculas
            if (mayusculas) {
            	return (literal).toUpperCase()+"Y "+Num[1]+"/100 SOLES";
            } else {
                return (literal);
            }
        } else {//error, no se puede convertir
            return literal = null;
        }
    }

    /* funciones para convertir los numeros a literales */

    private static String getUnidades(String numero) {// 1 - 9
        //si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
        String num = numero.substring(numero.length() - 1);
        return UNIDADES[Integer.parseInt(num)];
    }

    private static String getDecenas(String num) {// 99                        
        int n = Integer.parseInt(num);
        if (n < 10) {//para casos como -> 01 - 09
            return getUnidades(num);
        } else if (n > 19) {//para 20...99
            String u = getUnidades(num);
            if (u.equals("")) { //para 20,30,40,50,60,70,80,90
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
            } else {
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
            }
        } else {//numeros entre 11 y 19
            return DECENAS[n - 10];
        }
    }

    private static String getCentenas(String num) {// 999 o 099
        if( Integer.parseInt(num)>99 ){//es centena
            if (Integer.parseInt(num) == 100) {//caso especial
                return " cien ";
            } else {
                 return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getDecenas(num.substring(1));
            } 
        }else{//por Ej. 099 
            //se quita el 0 antes de convertir a decenas
            return getDecenas(Integer.parseInt(num)+"");            
        }        
    }

    private static String getMiles(String numero) {// 999 999
        //obtiene las centenas
        String c = numero.substring(numero.length() - 3);
        //obtiene los miles
        String m = numero.substring(0, numero.length() - 3);
        String n="";
        //se comprueba que miles tenga valor entero
        if (Integer.parseInt(m) > 0) {
            n = getCentenas(m);           
            return n + "mil " + getCentenas(c);
        } else {
            return "" + getCentenas(c);
        }

    }

    private static String getMillones(String numero) { //000 000 000        
        //se obtiene los miles
        String miles = numero.substring(numero.length() - 6);
        //se obtiene los millones
        String millon = numero.substring(0, numero.length() - 6);
        String n = "";
        if(millon.length()>1){
            n = getCentenas(millon) + "millones ";
        }else{
            n = getUnidades(millon) + "millon ";
        }
        return n + getMiles(miles);        
    }

	/** Selecciona la region del respositorio s3 aws usando un parametro 
	 *  en la tabla xstroreproperties
	 *
	 * @param String regionAws
	 * @return Regions
	 *
	 * @author Martin Corrales (Aligare).
	 * @since 09-01-2019
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
    public static Regions getRegionAws(String regionAws) {
    	Regions region = Regions.DEFAULT_REGION;
    	
    	if(Constantes.AWS_REGION_US_EAST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.US_EAST_1;
    	
    	} else if(Constantes.AWS_REGION_SA_EAST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.SA_EAST_1;
    	
    	} else if(Constantes.AWS_REGION_US_EAST_2.equalsIgnoreCase(regionAws)) {
    		region = Regions.US_EAST_2;
    	
    	} else if(Constantes.AWS_REGION_AP_NORTHEAST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.AP_NORTHEAST_1;
    	
    	} else if(Constantes.AWS_REGION_AP_NORTHEAST_2.equalsIgnoreCase(regionAws)) {
    		region = Regions.AP_NORTHEAST_2;
    	
    	} else if(Constantes.AWS_REGION_AP_SOUTH_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.AP_SOUTH_1;
    	
    	} else if(Constantes.AWS_REGION_AP_SOUTHEAST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.AP_SOUTHEAST_1;
    	
    	} else if(Constantes.AWS_REGION_AP_SOUTHEAST_2.equalsIgnoreCase(regionAws)) {
    		region = Regions.AP_SOUTHEAST_2;
    	
    	} else if(Constantes.AWS_REGION_CA_CENTRAL_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.CA_CENTRAL_1;
    	
    	} else if(Constantes.AWS_REGION_CN_NORTH_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.CN_NORTH_1;
    	
    	} else if(Constantes.AWS_REGION_CN_NORTHWEST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.CN_NORTHWEST_1;
    	
    	} else if(Constantes.AWS_REGION_EU_CENTRAL_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.EU_CENTRAL_1;
    	
    	} else if(Constantes.AWS_REGION_EU_NORTH_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.EU_NORTH_1;
    	
    	} else if(Constantes.AWS_REGION_EU_WEST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.EU_WEST_1;
    	
    	} else if(Constantes.AWS_REGION_EU_WEST_2.equalsIgnoreCase(regionAws)) {
    		region = Regions.EU_WEST_2;
    	
    	} else if(Constantes.AWS_REGION_EU_WEST_3.equalsIgnoreCase(regionAws)) {
    		region = Regions.EU_WEST_3;
    	
    	} else if(Constantes.AWS_REGION_US_GOV_EAST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.US_GOV_EAST_1;
    	
    	} else if(Constantes.AWS_REGION_US_WEST_1.equalsIgnoreCase(regionAws)) {
    		region = Regions.US_WEST_1;
    	
    	} else if(Constantes.AWS_REGION_US_WEST_2.equalsIgnoreCase(regionAws)) {
    		region = Regions.US_WEST_2;
    	}

    	return region;
    }
    
    public static List<String> getTipoDespachoCodBodega(TramaDTE trama) throws AligareException{
    	logger.initTrace("getTipoDespachoCodBodega", "Inicio");
		List<ArticuloVentaTramaDTE> articuloVentas = trama.getArticuloVentas();
		List<String> tipoDespachoCodBodega = articuloVentas.stream()
				.filter(articuloVentaTrama -> articuloVentaTrama.getArticuloVenta().getTipoDespacho() != null 
				                              && Constantes.BOL_TIPO_DES_RT.equalsIgnoreCase(articuloVentaTrama.getArticuloVenta().getTipoDespacho())
				                              && articuloVentaTrama.getArticuloVenta().getCodBodega()!= null)
				.limit(Constantes.NRO_UNO)
				.collect(ArrayList<String>::new, 
						(lista, articulo) -> {
							try {
								lista.add(Constantes.NRO_CERO, articulo.getArticuloVenta().getTipoDespacho());
								lista.add(Constantes.NRO_UNO, articulo.getArticuloVenta().getCodBodega());
							} catch (Exception e) {
								logger.traceInfo("getTipoDespachoCodBodega", "Exception" + e.getMessage());
							}
						},List::addAll);
		logger.endTrace("getTipoDespachoCodBodega", "tipoDespachoCodBodega:" + tipoDespachoCodBodega, "finaliza");
		return tipoDespachoCodBodega;
	}
}
