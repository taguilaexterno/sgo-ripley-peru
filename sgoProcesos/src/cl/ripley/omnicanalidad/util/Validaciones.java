package cl.ripley.omnicanalidad.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validaciones {
    
    public static boolean validarVacio(String dato){
        return (dato == null || Constantes.VACIO.equals(dato.trim()));
    }
    
    @SuppressWarnings("unused")
	public static boolean validarInt(String dato) {
         try{
             if(validarVacio(dato)) return Constantes.FALSO;
             int numero = Integer.parseInt(dato);
         }catch(NumberFormatException e){
              return Constantes.FALSO;
         }
         return Constantes.VERDADERO;
    }
    
    @SuppressWarnings("unused")
	public static boolean validarFloat(String dato) {
         try{
             if(validarVacio(dato)) return Constantes.FALSO;
             float numero = Float.parseFloat(dato);
         }catch(NumberFormatException e){
              return Constantes.FALSO;
         }
         return Constantes.VERDADERO;
    }
    
    @SuppressWarnings("unused")
	public static boolean validarFecha(String dato) {
         try{
             SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FECHA_YYYYMMDD);
             Date fc = formatoFecha.parse(dato);
             String[] valores = dato.split(Constantes.GUION);
             int dia, mes, anno;
             dia = Integer.parseInt(valores[Constantes.NRO_DOS]);
             mes = Integer.parseInt(valores[Constantes.NRO_UNO]);
             anno = Integer.parseInt(valores[Constantes.NRO_CERO]);
             if(mes > Constantes.NRO_DOCE) return Constantes.FALSO;
             Calendar cal = new GregorianCalendar(anno, mes-Constantes.NRO_UNO, Constantes.NRO_UNO);
             if(dia > cal.getActualMaximum(Calendar.DAY_OF_MONTH)) return Constantes.FALSO;     
         }catch(ParseException e){
              return Constantes.FALSO;
         }catch(NumberFormatException ne){
              return Constantes.VERDADERO;
         }
         return true;
    }
    
    public static boolean validarEmail(String dato) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
 
        Matcher matcher = pattern.matcher(dato);
        return matcher.matches();
    }
    
    public static boolean validaInteger(Integer dato){
    	if (dato != null)
    	   	return true;
    	else
    		return false;
    }
    
    public static boolean validaLong(Long dato){
    	if (dato != null)
    	   	return true;
    	else
    		return false;
    }
    
    public static boolean validaBigDecimal(BigDecimal dato){
    	if (dato != null)
    	   	return true;
    	else
    		return false;
    }
    
    public static boolean validaBigInteger(BigInteger dato){
    	if (dato!=null)
    		return true;
    	else
    		return false;
    }
    
    public static Integer validaIntegerCeroXNull(int numero){
    	Integer dato = null;

		if (numero == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = new Integer(numero);
		}

    	return dato;
    }
    
    public static Long validaLongCeroXNull(long numero){
    	Long dato = null;

		if (numero == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = numero;
		}

    	return dato;
    }
    
    public static BigDecimal validaBigDecimalCeroXNull(BigDecimal numero){
    	BigDecimal dato = null;

		if (numero != null && numero.compareTo(BigDecimal.ZERO) == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = numero;
		}

    	return dato;
    }

    public static BigDecimal validaBigDecimalNullXCero(BigDecimal numero){
    	BigDecimal dato = new BigDecimal(0);
		
		if (numero != null) {
			dato=numero;
		}

    	return dato;
    }
    
    public static Integer validaIntegerMenosUnoXNull(int numero){
    	Integer dato = null;

		if (numero == Constantes.NRO_MENOSUNO){
			dato = null;
		} else {
			dato = new Integer(numero);
		}

    	return dato;
    }
    
    public static Integer validaIntegerMenosUnoXNullaCERO(int numero){
    	Integer dato = null;

		if (numero == Constantes.NRO_MENOSUNO){
			dato = 0;
		} else {
			dato = new Integer(numero);
		}

    	return dato;
    }

    
    public static BigDecimal validaBigDecimalMenosUnoXNull(BigDecimal numero){
    	BigDecimal dato = null;

		if (numero.compareTo(new BigDecimal(Constantes.NRO_MENOSUNO)) == Constantes.NRO_CERO){
			dato = null;
		} else {
			dato = numero;
		}

    	return dato;
    }
    
}
