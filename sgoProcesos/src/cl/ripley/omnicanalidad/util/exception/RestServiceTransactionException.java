package cl.ripley.omnicanalidad.util.exception;

/**Exception creado para el rollback de una transacción no finalizada correctamente.
 * Sirve especialmente en la asignación de número de transacción para el Back Office.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 03-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class RestServiceTransactionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private Integer codigo;
	
	public RestServiceTransactionException(String mensaje, Integer codigo) {
		
		super(mensaje);
		
		this.mensaje = mensaje;
		this.codigo = codigo;
		
	}
	
	public RestServiceTransactionException(String mensaje, Integer codigo, Throwable e) {
		
		super(mensaje, e);
		
		this.mensaje = mensaje;
		this.codigo = codigo;
		
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public Integer getCodigo() {
		return codigo;
	}
	
	
	
}
