package cl.ripley.omnicanalidad.xml;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.DatosTiendaMpos;
import cl.ripley.omnicanalidad.bean.NotaVentaMail;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.Personalization;
import cl.ripley.omnicanalidad.bean.Recipient;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.bean.Xtmailing;
import cl.ripley.omnicanalidad.dao.DatosTiendasmPosDAO;
import cl.ripley.omnicanalidad.dao.impl.DatosTiendasmPosDAOImpl;

public class GeneracionXml {

	public static String GeneracionXmlTransacc(TramaDTE inOrdenCompra, Parametros pPars, String nroFolio, String caja) {

		XStream xstream = new XStream();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Parametros parametros = pPars ;
		String inicioCdata="![CDATA[";
		String finCdata="]]";
		String NO_DATA = "";
		
		inOrdenCompra.getNotaVenta().setNumeroCaja(Integer.valueOf(caja));
		
		DatosTiendasmPosDAO tiendasData = new DatosTiendasmPosDAOImpl();
		
		xstream.alias("XTMAILING", Xtmailing.class);
		xstream.alias("COLUMN_NAME", String.class);
		xstream.addImplicitCollection(Xtmailing.class, "SAVE_COLUMN");
		xstream.alias("RECIPIENT", Recipient.class);
		xstream.alias("PERSONALIZATION", Personalization.class);		
		xstream.addImplicitCollection(Recipient.class, "PERSONALIZATIONS");
		
		/*Estructura fija */
		List<String> COLUMN_NAME = new ArrayList<String>();
		COLUMN_NAME.add("CAMPAIGN_ID");
		COLUMN_NAME.add("SUBJECT");
		COLUMN_NAME.add("ORDER_NUMBER");
		COLUMN_NAME.add("DOCUMENT_KIND");
		COLUMN_NAME.add("DISPATCH_MODALITY");
				
		Xtmailing correo = new Xtmailing();
		correo.setCAMPAIGN_ID(parametros.buscaParametroPorNombre("CAMPAIGN_ID").getValor());//Obtener desde paramtros
		
		String transaccionId = "TRANS-"+String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
		correo.setTRANSACTION_ID(transaccionId);
		
		correo.setSHOW_ALL_SEND_DETAIL(parametros.buscaParametroPorNombre("SHOW_ALL_SEND_DETAIL").getValor());
		correo.setSEND_AS_BATCH(parametros.buscaParametroPorNombre("SEND_AS_BATCH").getValor());
		correo.setNO_RETRY_ON_FAILURE(parametros.buscaParametroPorNombre("NO_RETRY_ON_FAILURE").getValor());
		correo.setSAVE_COLUMNS(COLUMN_NAME);

		Recipient contenido = new Recipient();
		contenido.setEMAIL(inOrdenCompra.getDespacho().getEmailCliente());
		contenido.setBODY_TYPE("HTML");
		List<Personalization> PERSONALIZATIONS = new ArrayList<Personalization>();
		
		String subjetText = "Boleta Electrónica Ripley.com OC: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta();
		
		PERSONALIZATIONS.add(new Personalization ("CAMPAIGN_ID", parametros.buscaParametroPorNombre("CAMPAIGN_ID").getValor() ));				
		PERSONALIZATIONS.add(new Personalization ("SUBJECT",subjetText));
		PERSONALIZATIONS.add(new Personalization ("ORDER_NUMBER", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta()) ));
		PERSONALIZATIONS.add(new Personalization ("NRO_FOLIO", nroFolio));
		//PERSONALIZATIONS.add(new Personalization ("NRO_FOLIO", String.valueOf(inOrdenCompra.getNotaVenta().getFolioSii())));
		PERSONALIZATIONS.add(new Personalization ("DATE", formatter.format(new Date())));
		PERSONALIZATIONS.add(new Personalization ("ORDER_TOTAL",String.valueOf(inOrdenCompra.getNotaVenta().getMontoVenta())));
		
		//URL SEGUIMIENTO
		String urlBaseSeguimiento = parametros.buscaParametroPorNombre("URL_SEGUIMIENTO_COMPRA").getValor();
		String urlSeguimientoCompra = "<" + inicioCdata + urlBaseSeguimiento+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+ finCdata + ">";
		PERSONALIZATIONS.add(new Personalization ("URL_SEGUIMIENTO_COMPRA",urlSeguimientoCompra));
		
		//URL DOC ELECTRONICO
		String urlBaseDoc = parametros.buscaParametroPorNombre("URL_DOC_ELECTRONICO").getValor();
		
		
		String codeOc = String.format("%03d", inOrdenCompra.getNotaVenta().getNumeroSucursal()) +  String.format("%03d", inOrdenCompra.getNotaVenta().getNumeroCaja()) +  String.format("%010d", inOrdenCompra.getNotaVenta().getCorrelativoVenta()) + "7727"; 
		
		String urlDocElectronico = "<" + inicioCdata + urlBaseDoc + codeOc + finCdata  + ">";				
		PERSONALIZATIONS.add(new Personalization ("URL_DOC_ELECTRONICO",urlDocElectronico));
		
		String tipoDoc="";
		if (inOrdenCompra.getTipoDoc().getTipoDoc() == 3){
			tipoDoc = "BOLETA";
		}else{
			if (inOrdenCompra.getTipoDoc().getTipoDoc() == 4){
				tipoDoc = "FACTURA";
			}else{
				tipoDoc="Otro";
			}
		}
		
		
		
		PERSONALIZATIONS.add(new Personalization ("DOCUMENT_KIND",tipoDoc));
		
		//Articulos de la venta
		
		//Iterator articulos = .getArticuloVentas();
		int indice =1;
		String modalidadRT="";
		String modalidadST="";
		String modalidadNormal="";
		
		//recorre los articulos de la venta
		List<ArticuloVentaTramaDTE> articuloVentas = inOrdenCompra.getArticuloVentas();
		for (int i = 0; i < articuloVentas.size(); i++) {
			ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
			String mapaTienda="";
			String urlMapaTienda ="";
			String direccionTienda="";
			String fechaDespachoTienda="";
			String horarioTienda="";
			//articuloVentaTrama.getArticuloVenta().getDescRipley()
			String name = "NAME_" + indice;
			String part = "PARTNUMBER_" + indice;
			String image = "IMAGE_URL_" + indice;
			String cantidad = "QUANTITY_" + indice;
			String precio = "PRICE_" + indice;
			String estado ="STATE_" + indice;

			if (articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("0") || articuloVentaTrama.getArticuloVenta().getTipoDespacho() == null){
				continue;
			}
			
			DatosTiendaMpos datosTienda = new DatosTiendaMpos();
			
			datosTienda = tiendasData.getDataTiendaByLlave(articuloVentaTrama.getArticuloVenta().getCodBodega());
			mapaTienda=datosTienda.getUrlImagen();
			
			if(mapaTienda == null ){
				urlMapaTienda = NO_DATA;
			}else{
				urlMapaTienda = "<" + inicioCdata + mapaTienda + finCdata  + ">";
			}
			
			
			if(datosTienda.getDireccion() == null){
				direccionTienda = NO_DATA;
			}else{
				direccionTienda = datosTienda.getDireccion();
			}
			
			if(datosTienda.getHorario()==null){
				horarioTienda=NO_DATA;
			}else{
				horarioTienda = datosTienda.getHorario();
			}
			
			
			Date fechaDespacho = articuloVentaTrama.getArticuloVenta().getFechaDespacho();
			if(fechaDespacho == null){
				fechaDespachoTienda = NO_DATA;
			}else{
				fechaDespachoTienda =  formatter.format(articuloVentaTrama.getArticuloVenta().getFechaDespacho());
			}
			
			
			PERSONALIZATIONS.add(new Personalization ("MAP",urlMapaTienda));
			PERSONALIZATIONS.add(new Personalization ("DISPATCH_ADDRESS", direccionTienda));
			PERSONALIZATIONS.add(new Personalization ("TIME",horarioTienda)); //horario tienda
			PERSONALIZATIONS.add(new Personalization ("ESTIMATED_DELIVERY_DATE", fechaDespachoTienda));
			
			
			//sacar de campo tipoPapel
			String urlBaseImage= parametros.buscaParametroPorNombre("IMAGE_URL").getValor();
						
			String ImagenShowed = articuloVentaTrama.getArticuloVenta().getTipoPapelRegalo();
			
			if(ImagenShowed == null || ImagenShowed.length() == 0){
				ImagenShowed = articuloVentaTrama.getArticuloVenta().getCodArticulo() + "/" + articuloVentaTrama.getArticuloVenta().getCodArticulo() +".jpg";
			}
					
			String urlImagenShowed = "<" + inicioCdata + urlBaseImage + ImagenShowed + finCdata + ">";
			
			PERSONALIZATIONS.add(new Personalization (name,articuloVentaTrama.getArticuloVenta().getDescRipley()));
			PERSONALIZATIONS.add(new Personalization (part,articuloVentaTrama.getArticuloVenta().getCodArticulo()));
			PERSONALIZATIONS.add(new Personalization (image,urlImagenShowed));
			PERSONALIZATIONS.add(new Personalization (cantidad,String.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades())));
			PERSONALIZATIONS.add(new Personalization (precio,String.valueOf(articuloVentaTrama.getArticuloVenta().getPrecio())));
			PERSONALIZATIONS.add(new Personalization (estado,"Listo para Retirar"));
			
			if (modalidadNormal.length()== 0){
				
				if(!articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("RT") && !articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("ST"))
					modalidadNormal = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			
			if (modalidadRT.length()== 0){
				
				if(articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("RT"))
					modalidadRT = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			
			if (modalidadST.length()== 0){
				if(articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("ST"))
					modalidadST = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			indice++;						
		}
		String modalidad = modalidadNormal + modalidadRT + modalidadST;
		
		PERSONALIZATIONS.add(new Personalization ("DISPATCH_MODALITY",modalidad.trim()));		
		contenido.setPERSONALIZATIONS(PERSONALIZATIONS);
			

		correo.setRecipient(contenido);
		
		String xml = xstream.toXML(correo);
		
		xml = xml.replace("__", "_");
		xml = xml.replace("&lt;!", "<!");
		xml = xml.replace("]&gt;", "]>");
		xml = xml.replace("&amp;", "&");
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n";
		String xmlMail = header + xml;
		
		System.out.println("xmlMail: " + xmlMail);
		return xmlMail;
		
		
		
	}

	
	
	
	public static String GeneracionXmlTransacc(NotaVentaMail inOrdenCompra, Parametros pPars, String nroFolio, String caja) {

		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Parametros parametros = pPars ;
		String inicioCdata="![CDATA[";
		String finCdata="]]";
		String NO_DATA = "";
		String subjetText;
		String urlDocElectronico;
		String urlDoce;
		XStream xstream = new XStream();
		
		inOrdenCompra.getNotaVenta().setNumeroCaja(Integer.valueOf(caja));
		
		DatosTiendasmPosDAO tiendasData = new DatosTiendasmPosDAOImpl();
		
		xstream.alias("XTMAILING", Xtmailing.class);
		xstream.alias("COLUMN_NAME", String.class);
		xstream.addImplicitCollection(Xtmailing.class, "SAVE_COLUMN");
		xstream.alias("RECIPIENT", Recipient.class);
		xstream.alias("PERSONALIZATION", Personalization.class);		
		xstream.addImplicitCollection(Recipient.class, "PERSONALIZATIONS");
		
		/*Estructura fija */
		List<String> COLUMN_NAME = new ArrayList<String>();
		COLUMN_NAME.add("CAMPAIGN_ID");
		COLUMN_NAME.add("SUBJECT");
		COLUMN_NAME.add("ORDER_NUMBER");
		COLUMN_NAME.add("DOCUMENT_KIND");
		COLUMN_NAME.add("DISPATCH_MODALITY");
				
		Xtmailing correo = new Xtmailing();
		correo.setCAMPAIGN_ID(parametros.buscaParametroPorNombre("CAMPAIGN_ID").getValor());//Obtener desde paramtros
		
		String transaccionId = "TRANS-"+String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
		correo.setTRANSACTION_ID(transaccionId);
		
		correo.setSHOW_ALL_SEND_DETAIL(parametros.buscaParametroPorNombre("SHOW_ALL_SEND_DETAIL").getValor());
		correo.setSEND_AS_BATCH(parametros.buscaParametroPorNombre("SEND_AS_BATCH").getValor());
		correo.setNO_RETRY_ON_FAILURE(parametros.buscaParametroPorNombre("NO_RETRY_ON_FAILURE").getValor());
		correo.setSAVE_COLUMNS(COLUMN_NAME);

		Recipient contenido = new Recipient();
		contenido.setEMAIL(inOrdenCompra.getDespacho().getEmailCliente());
		contenido.setBODY_TYPE("HTML");
		List<Personalization> PERSONALIZATIONS = new ArrayList<Personalization>();
		
		String tipoDoc="";
		if (inOrdenCompra.getTipoDoc().getTipoDoc() == 3){
			tipoDoc = "BOLETA";
			subjetText = "Boleta Electrónica Ripley.com OC: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta();
		}else{
			if (inOrdenCompra.getTipoDoc().getTipoDoc() == 1){ //4
				tipoDoc = "FACTURA";
				subjetText = "Factura Electrónica Ripley.com OC: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta();
			}else{
				tipoDoc="Otro";
				subjetText = "Documento Electrónico Ripley.com OC: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta();
			}
		}
		
		//String subjetText = "Boleta Electrónica Ripley.com OC: " + inOrdenCompra.getNotaVenta().getCorrelativoVenta();
		
		PERSONALIZATIONS.add(new Personalization ("CAMPAIGN_ID", parametros.buscaParametroPorNombre("CAMPAIGN_ID").getValor() ));				
		PERSONALIZATIONS.add(new Personalization ("SUBJECT",subjetText));
		PERSONALIZATIONS.add(new Personalization ("ORDER_NUMBER", String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta()) ));
		PERSONALIZATIONS.add(new Personalization ("NRO_FOLIO", nroFolio));
		//PERSONALIZATIONS.add(new Personalization ("NRO_FOLIO", String.valueOf(inOrdenCompra.getNotaVenta().getFolioSii())));
		PERSONALIZATIONS.add(new Personalization ("DATE", formatter.format(new Date())));
		PERSONALIZATIONS.add(new Personalization ("ORDER_TOTAL",String.valueOf(inOrdenCompra.getNotaVenta().getMontoVenta())));
		
		//URL SEGUIMIENTO
		String urlBaseSeguimiento = parametros.buscaParametroPorNombre("URL_SEGUIMIENTO_COMPRA").getValor();
		String urlSeguimientoCompra = "<" + inicioCdata + urlBaseSeguimiento+inOrdenCompra.getNotaVenta().getCorrelativoVenta()+ finCdata + ">";
		PERSONALIZATIONS.add(new Personalization ("URL_SEGUIMIENTO_COMPRA",urlSeguimientoCompra));
		
		//URL DOC ELECTRONICO
		String urlBaseDoc = parametros.buscaParametroPorNombre("URL_DOC_ELECTRONICO").getValor();
		String codeOc = String.format("%03d", inOrdenCompra.getNotaVenta().getNumeroSucursal()) +  String.format("%03d", inOrdenCompra.getNotaVenta().getNumeroCaja()) +  String.format("%010d", inOrdenCompra.getNotaVenta().getCorrelativoVenta()) + "7727"; 
		
		//URL_DOCE
		urlDoce = String.valueOf(inOrdenCompra.getNotaVenta().getUrlDoce());
		if(urlDoce != null && !urlDoce.trim().isEmpty()) {
			//urlDocElectronico = urlDoce;
			urlDocElectronico = "<" + inicioCdata + urlDoce + finCdata  + ">";
		} else {
			urlDocElectronico = "<" + inicioCdata + urlBaseDoc + codeOc + finCdata  + ">";
		}
		
		/*
		//inOrdenCompra.getNotaVenta().get
		//String urlDocElectronico = "<" + inicioCdata + urlBaseDoc + codeOc + finCdata  + ">";
		urlDocElectronico = "<" + inicioCdata + urlBaseDoc + codeOc + finCdata  + ">";
		*/
		PERSONALIZATIONS.add(new Personalization ("URL_DOC_ELECTRONICO",urlDocElectronico));
		PERSONALIZATIONS.add(new Personalization ("DOCUMENT_KIND",tipoDoc));
		
		//Articulos de la venta
		
		//Iterator articulos = .getArticuloVentas();
		int indice =1;
		String modalidadRT="";
		String modalidadST="";
		String modalidadNormal="";
		
		//recorre los articulos de la venta
		List<ArticuloVentaTramaDTE> articuloVentas = inOrdenCompra.getArticuloVentas();
		for (int i = 0; i < articuloVentas.size(); i++) {
			ArticuloVentaTramaDTE articuloVentaTrama = articuloVentas.get(i);
			String mapaTienda="";
			String urlMapaTienda ="";
			String direccionTienda="";
			String fechaDespachoTienda="";
			String horarioTienda="";
			//articuloVentaTrama.getArticuloVenta().getDescRipley()
			String name = "NAME_" + indice;
			String part = "PARTNUMBER_" + indice;
			String image = "IMAGE_URL_" + indice;
			String cantidad = "QUANTITY_" + indice;
			String precio = "PRICE_" + indice;
			String estado ="STATE_" + indice;

			if (articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("0") || articuloVentaTrama.getArticuloVenta().getTipoDespacho() == null){
				continue;
			}
			
			/*
			DatosTiendaMpos datosTienda = new DatosTiendaMpos();
			
			datosTienda = tiendasData.getDataTiendaByLlave(articuloVentaTrama.getArticuloVenta().getCodBodega());
			mapaTienda=datosTienda.getUrlImagen();
			if (mapaTienda == "ERROR") {
				urlMapaTienda = NO_DATA;
//				direccionTienda = NO_DATA;
				horarioTienda=NO_DATA;
			} else {
				if(mapaTienda == null ){
					urlMapaTienda = NO_DATA;
				}else{
					urlMapaTienda = "<" + inicioCdata + mapaTienda + finCdata  + ">";
				}

				if(datosTienda.getDireccion() == null){
					direccionTienda = NO_DATA;
				}else{
					direccionTienda = datosTienda.getDireccion();
				}
				
				if(datosTienda.getHorario()==null){
					horarioTienda=NO_DATA;
				}else{
					horarioTienda = datosTienda.getHorario();
				}
			}
			*/
			
			Date fechaDespacho = articuloVentaTrama.getArticuloVenta().getFechaDespacho();
			if(fechaDespacho == null){
				fechaDespachoTienda = NO_DATA;
			}else{
				fechaDespachoTienda =  formatter.format(articuloVentaTrama.getArticuloVenta().getFechaDespacho());
			}
			
			if (articuloVentaTrama.getArticuloVenta().getDireccionEntrega() == null) {
				direccionTienda = NO_DATA; 
			} else {
				direccionTienda = articuloVentaTrama.getArticuloVenta().getDireccionEntrega();
			}
			
			if (articuloVentaTrama.getArticuloVenta().getHorarioEntrega() == null) {
				horarioTienda = NO_DATA; 
			} else {
				horarioTienda = articuloVentaTrama.getArticuloVenta().getHorarioEntrega();
			}
			
			if (articuloVentaTrama.getArticuloVenta().getUrlGoogleEntrega() == null) {
				urlMapaTienda = NO_DATA; 
			} else {
				urlMapaTienda = articuloVentaTrama.getArticuloVenta().getUrlGoogleEntrega();
			}
			
			
			PERSONALIZATIONS.add(new Personalization ("MAP",urlMapaTienda));
			PERSONALIZATIONS.add(new Personalization ("DISPATCH_ADDRESS", direccionTienda));
			PERSONALIZATIONS.add(new Personalization ("TIME",horarioTienda)); //horario tienda
			PERSONALIZATIONS.add(new Personalization ("ESTIMATED_DELIVERY_DATE", fechaDespachoTienda));
			
			
			//sacar de campo tipoPapel
			String urlBaseImage= parametros.buscaParametroPorNombre("IMAGE_URL").getValor();
						
			String ImagenShowed = articuloVentaTrama.getArticuloVenta().getTipoPapelRegalo();
			
			if(ImagenShowed == null || ImagenShowed.length() == 0){
				ImagenShowed = articuloVentaTrama.getArticuloVenta().getCodArticulo() + "/" + articuloVentaTrama.getArticuloVenta().getCodArticulo() +".jpg";
			}
					
			String urlImagenShowed = "<" + inicioCdata + urlBaseImage + ImagenShowed + finCdata + ">";
			
			PERSONALIZATIONS.add(new Personalization (name,articuloVentaTrama.getArticuloVenta().getDescRipley()));
			PERSONALIZATIONS.add(new Personalization (part,articuloVentaTrama.getArticuloVenta().getCodArticulo()));
			PERSONALIZATIONS.add(new Personalization (image,urlImagenShowed));
			PERSONALIZATIONS.add(new Personalization (cantidad,String.valueOf(articuloVentaTrama.getArticuloVenta().getUnidades())));
			PERSONALIZATIONS.add(new Personalization (precio,String.valueOf(articuloVentaTrama.getArticuloVenta().getPrecio())));
			PERSONALIZATIONS.add(new Personalization (estado,"Listo para Retirar"));
			
			if (modalidadNormal.length()== 0){
				
				if(!articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("RT") && !articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("ST"))
					modalidadNormal = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			
			if (modalidadRT.length()== 0){
				
				if(articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("RT"))
					modalidadRT = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			
			if (modalidadST.length()== 0){
				if(articuloVentaTrama.getArticuloVenta().getTipoDespacho().equalsIgnoreCase("ST"))
					modalidadST = articuloVentaTrama.getArticuloVenta().getTipoDespacho();
			}
			indice++;						
		}
		String modalidad = modalidadNormal + modalidadRT + modalidadST;
		
		PERSONALIZATIONS.add(new Personalization ("DISPATCH_MODALITY",modalidad.trim()));		
		contenido.setPERSONALIZATIONS(PERSONALIZATIONS);
			

		correo.setRecipient(contenido);
		
		String xml = xstream.toXML(correo);
		
		xml = xml.replace("__", "_");
		xml = xml.replace("&lt;!", "<!");
		xml = xml.replace("]&gt;", "]>");
		xml = xml.replace("&amp;", "&");
		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n";
		String xmlMail = header + xml;
		
		System.out.println("xmlMail: " + xmlMail);
		return xmlMail;
	}

	
}
