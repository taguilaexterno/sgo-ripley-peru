package com.ripley.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ripley.restservices.model.bcvRest.Message;
import com.ripley.restservices.model.bcvRest.header.Transaction;

@Configuration
public class ProcesosConfig {

	@Bean
	public JAXBContext bcvJaxbContext1() throws JAXBException {
		return JAXBContext.newInstance(Message.class);
	}
	
	@Bean
	public JAXBContext bcvJaxbContext2() throws JAXBException {
		return JAXBContext.newInstance(Transaction.class);
	}
	
	@Bean
	public Marshaller bcvMarshaller1(JAXBContext bcvJaxbContext1) throws JAXBException {
		Marshaller m = bcvJaxbContext1.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		return m;
	}
	
	@Bean
	public Marshaller bcvMarshaller2(JAXBContext bcvJaxbContext2) throws JAXBException {
		Marshaller m = bcvJaxbContext2.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		return m;
	}
	
}
