package com.ripley.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan("com.ripley.scheduled")
@EnableScheduling
public class TaskConfig {
	
}
