package com.ripley.enviolog;

import java.io.File;
import java.io.FileInputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class EnvioSftp {
	public static void envio02 (String SFTPHOST, String SFTPUSER, String SFTPPASS, int SFTPPORT, String fileName) {
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        System.out.println("[INI] Preparando la información del host para sftp");
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            System.out.println("[INF] Host conectado.");
            channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("[INF] Canal sftp abierto y conectado.");
            channelSftp = (ChannelSftp) channel;
//          channelSftp.cd(SFTPWORKINGDIR);
            File f = new File(fileName);
            channelSftp.put(new FileInputStream(f), f.getName());
//          log.info("File transfered successfully to host.");
            System.out.println("[INF] Archivo transferido correctamente al host.");
        } catch (Exception ex) {
             System.err.println("[ERR] Se encontró una excepción al transferir la respuesta.");
             ex.printStackTrace();
             System.err.println(ex.getMessage());
        }
        finally{
            channelSftp.exit();
            System.out.println("[FIN] Canal sftp.");
            channel.disconnect();
            System.out.println("[FIN] Canal Desconectado.");
            session.disconnect();
            System.out.println("[FIN] Session de Host desconectado.");
        }
    }
}
