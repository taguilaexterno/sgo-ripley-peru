package com.ripley.enviolog;

import java.io.BufferedOutputStream;
import java.io.File;
//import java.io.file.Files;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import com.ripley.scheduled.TaskSunat;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;

public class tar {
	private static final AriLog LOG = new AriLog(TaskSunat.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	public static void CreateTarGZ(String inputDirectoryPath, String outputPath, String filtro) throws IOException {
        File inputFile = new File(inputDirectoryPath);
        File outputFile = new File(outputPath);
        int itarCreado = 0;
        LOG.traceInfo("CreateTarGZ", "[INI] Creando Archivo TarGz");
        
        try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
                GzipCompressorOutputStream gzipOutputStream = new GzipCompressorOutputStream(bufferedOutputStream);
                TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(gzipOutputStream)) {

            tarArchiveOutputStream.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_POSIX);
            tarArchiveOutputStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);

            List<File> files = new ArrayList<>(FileUtils.listFiles(
                    inputFile,
                    new RegexFileFilter("^(.*?)"),
                    DirectoryFileFilter.DIRECTORY
            ));

            for (int i = 0; i < files.size(); i++) {
                File currentFile = files.get(i);
                String fileName = currentFile.getName();
                if (fileName == filtro) {
                	String relativeFilePath = inputFile.toURI().relativize(
                            new File(currentFile.getAbsolutePath()).toURI()).getPath();

                    TarArchiveEntry tarEntry = new TarArchiveEntry(currentFile, relativeFilePath);
                    tarEntry.setSize(currentFile.length());

                    tarArchiveOutputStream.putArchiveEntry(tarEntry);
                    tarArchiveOutputStream.write(IOUtils.toByteArray(new FileInputStream(currentFile)));
                    tarArchiveOutputStream.closeArchiveEntry();
                    itarCreado = 1;
                }

                
            }
            if (itarCreado == 1) {
            	tarArchiveOutputStream.close();
            	LOG.traceInfo("CreateTarGZ", "[FIN] Archivo TarGz Creado");
            } else {
            	LOG.traceInfo("CreateTarGZ", "[ERR] Archivo TarGz NO Creado");
            }
            
            //System.out.println("[FIN] Archivo TarGz Creado");
        } catch ( IOException e ) {
        	LOG.traceInfo("CreateTarGZ", "[ERR] Se produjo un error de E/S");
            //System.err.println("[ERR] Se produjo un error de E/S");
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        
    }
	
	public static void EliminaFile(String inputFile) throws IOException {
		File f;
		f = new File(inputFile);
		String path1 = f.getCanonicalPath();
		String path2 = f.getAbsolutePath();
		//f.delete();
		if (!f.delete()) {
			LOG.traceInfo("EliminaFile", "[ERR] Ocurrio un problema al intentar eliminar el archivo temporal TarGz.");
			LOG.traceInfo("EliminaFile", "[INF] CanonicalPath: " + path1);
			LOG.traceInfo("EliminaFile", "[INF] AbsolutePath: " + path2);
			//System.err.println("[ERR] Ocurrio un problema al intentar eliminar el archivo temporal TarGz.");
			//System.out.println("[INF] CanonicalPath: " + path1);
			//System.out.println("[INF] AbsolutePath: " + path2);
			f.delete();
			f.deleteOnExit();
		} else {
			LOG.traceInfo("EliminaFile", "[INF] Archivo Temporal TarGz Eliminado");
			//System.out.println("[INF] Archivo Temporal TarGz Eliminado");
		}
	}
	
	public static void CreateZIP(String inputDirectoryPath, String outputPath, String filtro) throws IOException {
		// cadena que contiene la ruta donde están los archivos a comprimir
		String directorioZip = inputDirectoryPath; // "C:\\ZIP\\";
		// ruta completa donde están los archivos a comprimir
		File carpetaComprimir = new File(directorioZip);
 
		// valida si existe el directorio
		if (carpetaComprimir.exists()) {
			// lista los archivos que hay dentro del directorio
			File[] ficheros = carpetaComprimir.listFiles();
			LOG.traceInfo("CreateZIP", "[INF] Número de ficheros encontrados: " + ficheros.length);
			//System.out.println("Número de ficheros encontrados: " + ficheros.length);
 
			// ciclo para recorrer todos los archivos a comprimir
			for (int i = 0; i < ficheros.length; i++) {
				LOG.traceInfo("CreateZIP", "[INF] Nombre del fichero: " + ficheros[i].getName());
				//System.out.println("Nombre del fichero: " + ficheros[i].getName());
				String extension="";
				// p.getNombre().equals(this.getNombre())
				if (ficheros[i].getName().equals(filtro)) {
					for (int j = 0; j < ficheros[i].getName().length(); j++) {
						//obtiene la extensión del archivo
						if (ficheros[i].getName().charAt(j)=='.') {
							extension=ficheros[i].getName().substring(j, (int)ficheros[i].getName().length());
							//System.out.println(extension);
						}
					}
					try {
						// crea un buffer temporal para ir poniendo los archivos a comprimir
						//ZipOutputStream zous = new ZipOutputStream(new FileOutputStream(directorioZip + ficheros[i].getName().replace(extension, ".zip")));
						ZipOutputStream zous = new ZipOutputStream(new FileOutputStream(outputPath));
						
						String fileName = ficheros[i].getName();
						
						//nombre con el que se va guardar el archivo dentro del zip
						ZipEntry entrada = new ZipEntry(ficheros[i].getName());
						zous.putNextEntry(entrada);
						
							//System.out.println("Nombre del Archivo: " + entrada.getName());
							LOG.traceInfo("CreateZIP", "[INF] Comprimiendo..... [" + fileName +"]");
							//System.out.println("Comprimiendo.....");
							//obtiene el archivo para irlo comprimiendo
							FileInputStream fis = new FileInputStream(directorioZip+entrada.getName());
							int leer;
							byte[] buffer = new byte[1024];
							while (0 < (leer = fis.read(buffer))) {
								zous.write(buffer, 0, leer);
							}
							fis.close();
							zous.closeEntry();
						zous.close();					
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}		
			}
			LOG.traceInfo("CreateZIP", "[INF] Directorio de salida: " + directorioZip);
			//System.out.println("Directorio de salida: " + directorioZip);
		} else {
			LOG.traceInfo("CreateZIP", "[ERR] No se encontró el directorio.. [" + directorioZip + "]");
			//System.out.println("No se encontró el directorio..");
		}
	}
	
	
	public static void CreateZIP_old(String inputDirectoryPath, String outputPath, String filtro) throws IOException {
		// cadena que contiene la ruta donde están los archivos a comprimir
		String directorioZip = inputDirectoryPath; // "C:\\ZIP\\";
		// ruta completa donde están los archivos a comprimir
		File carpetaComprimir = new File(directorioZip);
 
		// valida si existe el directorio
		if (carpetaComprimir.exists()) {
			// lista los archivos que hay dentro del directorio
			File[] ficheros = carpetaComprimir.listFiles();
			System.out.println("Número de ficheros encontrados: " + ficheros.length);
 
			// ciclo para recorrer todos los archivos a comprimir
			for (int i = 0; i < ficheros.length; i++) {
				System.out.println("Nombre del fichero: " + ficheros[i].getName());
				String extension="";
				for (int j = 0; j < ficheros[i].getName().length(); j++) {
					//obtiene la extensión del archivo
					if (ficheros[i].getName().charAt(j)=='.') {
						extension=ficheros[i].getName().substring(j, (int)ficheros[i].getName().length());
						//System.out.println(extension);
					}
				}
				try {
					// crea un buffer temporal para ir poniendo los archivos a comprimir
					ZipOutputStream zous = new ZipOutputStream(new FileOutputStream(directorioZip + ficheros[i].getName().replace(extension, ".zip")));
					
					String fileName = ficheros[i].getName();
					
					//nombre con el que se va guardar el archivo dentro del zip
					ZipEntry entrada = new ZipEntry(ficheros[i].getName());
					zous.putNextEntry(entrada);
					
						//System.out.println("Nombre del Archivo: " + entrada.getName());
						System.out.println("Comprimiendo.....");
						//obtiene el archivo para irlo comprimiendo
						FileInputStream fis = new FileInputStream(directorioZip+entrada.getName());
						int leer;
						byte[] buffer = new byte[1024];
						while (0 < (leer = fis.read(buffer))) {
							zous.write(buffer, 0, leer);
						}
						fis.close();
						zous.closeEntry();
					zous.close();					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}
			System.out.println("Directorio de salida: " + directorioZip);
		} else {
			System.out.println("No se encontró el directorio..");
		}
	}
}
