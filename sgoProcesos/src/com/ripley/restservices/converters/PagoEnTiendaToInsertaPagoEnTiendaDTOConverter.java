package com.ripley.restservices.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ripley.dao.dto.InsertaPagoEnTiendaDTO;
import com.ripley.restservices.model.pagoTiendaRest.PagoEnTienda;

/**Conversor de {@linkplain PagoEnTienda} a {@linkplain InsertaPagoEnTiendaDTO}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 12-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Component("pagoEnTiendaToInsertaPagoEnTiendaDTOConverter")
public class PagoEnTiendaToInsertaPagoEnTiendaDTOConverter implements Converter<PagoEnTienda, InsertaPagoEnTiendaDTO> {

	@Override
	public InsertaPagoEnTiendaDTO convert(PagoEnTienda s) {
		InsertaPagoEnTiendaDTO dto = new InsertaPagoEnTiendaDTO();
		
		dto.setBinNumber(s.getBinNumber());
		dto.setCodigoAutorizacion(s.getCodigoAutorizacion());
		dto.setCorrelativoVenta(s.getCorrelativoVenta());
		dto.setDiferido(s.getDiferido());
		dto.setFechaHoraTrx(s.getFechaHoraTrx());
		dto.setFechaPrimerVencto(s.getFechaPrimerVencio());
		dto.setFechaTrx(s.getFechaTrx());
		dto.setGlosa(s.getGlosa());
		dto.setIdTrxPago(s.getIdTrxPago());
		dto.setMontoDescuentoRipley(s.getMontoDestoRipley());
		dto.setNroCaja(s.getNroCaja());
		dto.setNroDocumento(s.getNroDocumento());
		dto.setNroTrx(s.getNroTransaccion());
		dto.setPan(s.getPan());
		dto.setPlazo(s.getPlazo());
		dto.setRecaudadorCodigo(s.getRecaudadorCodigo());
		dto.setRecaudadorDni(s.getRecaudadorDni());
		dto.setRecaudadorNombres(s.getRecaudadorNombres());
		dto.setSucursal(s.getSucursal());
		dto.setTipoPago(s.getTipoPago());
		dto.setTipoPasarela(s.getTipoPasarela());
		dto.setTipoTarjeta(s.getTipoTarjeta());
		dto.setTipoTrx(s.getTipoTrx());
		dto.setUltimosDigitos(s.getUltimosDigidos());
		dto.setValorCuota(s.getValorCuota());
		
		return dto;
	}

}
