package com.ripley.restservices.converters;

import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ripley.restservices.model.backOfficeRest.Data;
import com.ripley.restservices.model.backOfficeRest.DataTrxAdministrativa2;
import com.ripley.restservices.model.backOfficeRest.Document;
import com.ripley.restservices.model.backOfficeRest.Header;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallApertura;
import com.ripley.restservices.model.backOfficeRest.PrimaryKey;
import com.ripley.restservices.model.backOfficeRest.RequestBO;
import com.ripley.restservices.model.backOfficeRest.TrxAdministrativa2;
import com.ripley.restservices.model.backOfficeRest.TrxAdministrativa2Row;

import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
public class PrepareBoCallAperturaToRequestBOConverter implements Converter<PrepareBoCallApertura, RequestBO> {

	@Override
	public RequestBO convert(PrepareBoCallApertura s) {

		Long trx = s.getTrx();
		Parametros pcv = s.getPcv();
		String fecha = s.getFecha();
		String fechaHora = s.getFechaHora();

		Integer nroCaja = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.NRO_CAJA_NAME));
		Integer sucursal = Integer.valueOf(pcv.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME));
		
		RequestBO request = new RequestBO();
		
		Document document = new Document();
		
		PrimaryKey pk = new PrimaryKey();
		pk.setFechaTrx(fecha);
		pk.setNroCaja(nroCaja);
		pk.setNroTransaccion(trx);
		pk.setSucursal(sucursal);
		
		document.setPrimaryKey(pk);
		
		//seteo header
		document.setHeader(new Header());
		
		Header header = document.getHeader();
		header.setTipoTrx(Constantes.BO_TIPO_TRX_APERTURA);
		header.setNroDocumento(trx);
		header.setFechaHoraTrx(fechaHora);
		header.setSupervisor(Long.valueOf(pcv.buscaValorPorNombre("RUT_SUPERVISOR")));
		header.setVendedor(Long.valueOf(pcv.buscaValorPorNombre("RUT_VENDEDOR")));
		header.setMontoTrx(Constantes.NRO_CERO_B);
		header.setRutCompraPaga(Long.valueOf(Constantes.NRO_CERO));
		header.setTrxTipoDoc(Constantes.NRO_CERO);
		header.setNroTerminal(pcv.buscaValorPorNombre("TERMINAL"));
		header.setOrigenTrx(Constantes.NRO_CERO);
		header.setEstado(Constantes.NRO_CERO);
		header.setTipoOrigen(Constantes.NRO_CERO);
		
		List<Data> dataList = document.getData();
		
		DataTrxAdministrativa2 dataTrxAdministrativa2 = new DataTrxAdministrativa2();
		
		TrxAdministrativa2 trxAdministrativa2 = new TrxAdministrativa2();
		
		List<TrxAdministrativa2Row> trxAdministrativa2Rows = trxAdministrativa2.getRow();
		
		
		TrxAdministrativa2Row trxAdmin = new TrxAdministrativa2Row();
		trxAdmin.setTipoTrx(Constantes.BO_TIPO_TRX_APERTURA);
		trxAdmin.setOrigenTrx(Constantes.NRO_CERO);
		trxAdmin.setFechaHoraTrx(fechaHora);
		trxAdmin.setSupervisor(header.getSupervisor());
		trxAdmin.setVendedor(header.getVendedor());
		trxAdmin.setTotalVentaNeta(Constantes.NRO_CERO_B);
		trxAdmin.setTotalConDespacho(Constantes.NRO_CERO_B);
		trxAdmin.setTotalRecaudacion(Constantes.NRO_CERO_B);
		//para impresion de boletas es 0 y para NC será el monto reversado
		trxAdmin.setTotalRevRecaudacion(Constantes.NRO_CERO_B);
		
		trxAdministrativa2Rows.add(trxAdmin);
		
		dataTrxAdministrativa2.setTrxAdministrativa2(trxAdministrativa2);
		//agrego segunda trx
		dataList.add(dataTrxAdministrativa2);
		
		request.setDocument(document);

		return request;
	}

}
