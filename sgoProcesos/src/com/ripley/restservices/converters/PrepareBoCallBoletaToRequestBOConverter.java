package com.ripley.restservices.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ripley.dao.impl.OrdenCompraDAO;
import com.ripley.restservices.model.backOfficeRest.DataTrxArticulos;
import com.ripley.restservices.model.backOfficeRest.DataTrxBancaria;
import com.ripley.restservices.model.backOfficeRest.DataTrxDespacho;
import com.ripley.restservices.model.backOfficeRest.DataTrxEticket;
import com.ripley.restservices.model.backOfficeRest.DataTrxMultipagosBo;
import com.ripley.restservices.model.backOfficeRest.DataTrxRecaudacionBo;
import com.ripley.restservices.model.backOfficeRest.DataTrxRecaudacionPrvBo;
import com.ripley.restservices.model.backOfficeRest.DataTrxReferenciaAddBo;
import com.ripley.restservices.model.backOfficeRest.DataTrxTarRipley;
import com.ripley.restservices.model.backOfficeRest.DataTrxTransaccionAdd;
import com.ripley.restservices.model.backOfficeRest.Document;
import com.ripley.restservices.model.backOfficeRest.Header;
import com.ripley.restservices.model.backOfficeRest.PrepareBoCallBoleta;
import com.ripley.restservices.model.backOfficeRest.PrimaryKey;
import com.ripley.restservices.model.backOfficeRest.RequestBO;
import com.ripley.restservices.model.backOfficeRest.TrxArticulos;
import com.ripley.restservices.model.backOfficeRest.TrxArticulosRow;
import com.ripley.restservices.model.backOfficeRest.TrxDespacho;
import com.ripley.restservices.model.backOfficeRest.TrxDespachoRow;
import com.ripley.restservices.model.backOfficeRest.TrxEticket;
import com.ripley.restservices.model.backOfficeRest.TrxEticketRow;
import com.ripley.restservices.model.backOfficeRest.TrxMultipagosBo;
import com.ripley.restservices.model.backOfficeRest.TrxMultipagosBoRow;
import com.ripley.restservices.model.backOfficeRest.TrxRecaudacionBo;
import com.ripley.restservices.model.backOfficeRest.TrxRecaudacionBoRow;
import com.ripley.restservices.model.backOfficeRest.TrxRecaudacionPrvBo;
import com.ripley.restservices.model.backOfficeRest.TrxRecaudacionPrvBoRow;
import com.ripley.restservices.model.backOfficeRest.TrxReferenciaAddBo;
import com.ripley.restservices.model.backOfficeRest.TrxReferenciaAddBoRow;
import com.ripley.restservices.model.backOfficeRest.TrxTarBancaria;
import com.ripley.restservices.model.backOfficeRest.TrxTarBancariaRow;
import com.ripley.restservices.model.backOfficeRest.TrxTarRipley;
import com.ripley.restservices.model.backOfficeRest.TrxTarRipleyRow;
import com.ripley.restservices.model.backOfficeRest.TrxTransaccionAdd;
import com.ripley.restservices.model.backOfficeRest.TrxTransaccionAddRow;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.Validaciones;

@Component
public class PrepareBoCallBoletaToRequestBOConverter implements Converter<PrepareBoCallBoleta, RequestBO> {

	private static final AriLog LOG = new AriLog(PrepareBoCallBoletaToRequestBOConverter.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	@Lazy
	private CajaVirtualDAO caja;
	
	@Autowired
	private OrdenCompraDAO ordenCompraDAO;
	
	@Override
	public RequestBO convert(PrepareBoCallBoleta s) throws AligareException {
		LOG.initTrace("convert", Constantes.VACIO);

		TramaDTE inOrdenCompra = s.getInOrdenCompra();
		Parametros parametros = s.getParametros();
		Long nroTransaccion = s.getNroTransaccion();
		Long nroTrxAnterior = s.getNroTrxAnterior();
		boolean esMkp = s.isEsMkp();
		String boleta = s.getBoleta();
		boolean isRipleyProcessed = s.isRipleyProcessed();
		boolean isMkpProcessed = s.isMkpProcessed();

		TrxTarRipley trxTarRipley = new TrxTarRipley();
		TrxTransaccionAdd trxTransaccionAdd = new TrxTransaccionAdd();
		TrxReferenciaAddBo trxReferenciaAddBo = new TrxReferenciaAddBo();
		TrxEticket trxEticket = new TrxEticket();
		TrxTarBancaria trxTarBancaria = new TrxTarBancaria();
		TrxDespacho trxDespacho = new TrxDespacho();
		TrxMultipagosBo trxMultipagoBo = new TrxMultipagosBo();
		TrxRecaudacionBo trxRecaudacionBo = new TrxRecaudacionBo();
		TrxRecaudacionPrvBo trxRecaudacionPrv = new TrxRecaudacionPrvBo();
		List<TrxTransaccionAddRow> listaAddRow = new ArrayList<TrxTransaccionAddRow>();
		List<TrxTarRipleyRow> listaRipleyRow = new ArrayList<TrxTarRipleyRow>();
		List<TrxReferenciaAddBoRow> listaReferenciaBoRow = new ArrayList<TrxReferenciaAddBoRow>();
		List<TrxEticketRow> listaEticketRow = new ArrayList<TrxEticketRow>();
		List<TrxTarBancariaRow> listaTarBancariaRow = new ArrayList<TrxTarBancariaRow>();
		List<TrxMultipagosBoRow> listaMultipagos = new ArrayList<TrxMultipagosBoRow>();
		List<TrxRecaudacionBoRow> listaRecaudacion = new ArrayList<TrxRecaudacionBoRow>();
		
		LocalDateTime ldtFechaTrx = LocalDateTime.parse(caja.sysdateFromDual(Constantes.FECHA_DDMMYYYY_HHMMSS_G, parametros), Constantes.DATE_TIME_FORMATER_DDMMYYYY_HHMMSS);

		RequestBO request = new RequestBO();

		request.setDocument(new Document());

		request.getDocument().setPrimaryKey(new PrimaryKey());
		request.getDocument().getPrimaryKey().setFechaTrx(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(ldtFechaTrx));
		request.getDocument().getPrimaryKey().setNroCaja(Integer.valueOf(parametros.buscaValorPorNombre(Constantes.NRO_CAJA_NAME)));
		request.getDocument().getPrimaryKey().setNroTransaccion(nroTransaccion);
		request.getDocument().getPrimaryKey().setSucursal(Integer.valueOf(parametros.buscaValorPorNombre(Constantes.PARAM_SUCURSAL_NAME)));

		request.getDocument().setHeader(generarHeader(inOrdenCompra, parametros, nroTransaccion, nroTrxAnterior, esMkp, ldtFechaTrx, isRipleyProcessed, isMkpProcessed));

		request.getDocument().setData(new ArrayList<>());

		TrxTransaccionAddRow trxTransaccionAddRow = generarTrxTransaccionAddRow(inOrdenCompra, parametros, nroTransaccion, nroTrxAnterior, esMkp, ldtFechaTrx, isRipleyProcessed);
		listaAddRow.add(trxTransaccionAddRow);
		trxTransaccionAdd.setRow(listaAddRow);

		DataTrxTransaccionAdd dta = new DataTrxTransaccionAdd();
		dta.setTrxTransaccionAdd(new TrxTransaccionAdd());
		dta.getTrxTransaccionAdd().setRow(new ArrayList<>());
		dta.getTrxTransaccionAdd().getRow().add(trxTransaccionAddRow);

		request.getDocument().getData().add(dta);

		if (inOrdenCompra.getTarjetaRipley().getCorrelativoVenta()!=null) {
			TrxTarRipleyRow trxTarRipleyRow = generarTrxTarRipleyRow(inOrdenCompra, parametros, nroTransaccion, esMkp);
			listaRipleyRow.add(trxTarRipleyRow);
			trxTarRipley.setRow(listaRipleyRow);

			DataTrxTarRipley dttr = new DataTrxTarRipley();
			dttr.setTrxTarRipley(new TrxTarRipley());
			dttr.getTrxTarRipley().setRow(new ArrayList<>());
			dttr.getTrxTarRipley().getRow().add(trxTarRipleyRow);

			request.getDocument().getData().add(dttr);

		}

		String formaPago = Util.getFormaPago(inOrdenCompra);

		if(Constantes.FORMA_PAGO_DEBITO.equalsIgnoreCase(formaPago)
				|| Constantes.FORMA_PAGO_TDC.equalsIgnoreCase(formaPago)
				|| Constantes.FORMA_PAGO_TARJETA_RIPLEY.equalsIgnoreCase(formaPago)
				|| Constantes.FORMA_PAGO_EFECTIVO.equalsIgnoreCase(formaPago)
				|| Constantes.FORMA_PAGO_PAYPAL.equalsIgnoreCase(formaPago)) {

			TrxReferenciaAddBoRow trxReferenciaAddBoRow = generarTrxReferenciaAddBORow(inOrdenCompra, parametros, nroTransaccion);
			listaReferenciaBoRow.add(trxReferenciaAddBoRow);
			trxReferenciaAddBo.setRow(listaReferenciaBoRow);

			DataTrxReferenciaAddBo dtrab = new DataTrxReferenciaAddBo();
			dtrab.setTrxReferenciaAddBo(new TrxReferenciaAddBo());
			dtrab.getTrxReferenciaAddBo().setRow(new ArrayList<>());
			dtrab.getTrxReferenciaAddBo().getRow().add(trxReferenciaAddBoRow);

			request.getDocument().getData().add(dtrab);

		}

		if(!esMkp) {

			TrxEticketRow trxEticketRow = generarTrxEticketRow(inOrdenCompra, parametros, nroTransaccion, boleta);
			listaEticketRow.add(trxEticketRow);
			trxEticket.setRow(listaEticketRow);

			DataTrxEticket dtet = new DataTrxEticket();
			dtet.setTrxEticket(new TrxEticket());
			dtet.getTrxEticket().setRow(new ArrayList<>());
			dtet.getTrxEticket().getRow().add(trxEticketRow);

			request.getDocument().getData().add(dtet);


			List<TrxDespachoRow> trxDespachoRow = generarTrxDespachoRow(inOrdenCompra, parametros, nroTransaccion, esMkp);
			trxDespacho.setRow(trxDespachoRow);

			DataTrxDespacho dtd = new DataTrxDespacho();
			dtd.setTrxDespacho(new TrxDespacho());
			dtd.getTrxDespacho().setRow(new ArrayList<>());
			dtd.getTrxDespacho().getRow().addAll(trxDespachoRow);

			request.getDocument().getData().add(dtd);

			List<TrxArticulosRow> trxArticuloRow = generarTrxArticulosRow(inOrdenCompra, parametros, nroTransaccion, esMkp);

			DataTrxArticulos dtaa = new DataTrxArticulos();
			dtaa.setTrxArticulos(new TrxArticulos());
			dtaa.getTrxArticulos().setRow(new ArrayList<>());
			dtaa.getTrxArticulos().getRow().addAll(trxArticuloRow);

			request.getDocument().getData().add(dtaa);


		}

		if (inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta()!=null
				&& !Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
				&& !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)) {
			TrxTarBancariaRow trxTarBancariaRow = generarTrxTarBancariaRow(inOrdenCompra, parametros, nroTransaccion, esMkp);
			listaTarBancariaRow.add(trxTarBancariaRow);
			trxTarBancaria.setRow(listaTarBancariaRow);

			DataTrxBancaria dtb = new DataTrxBancaria();
			dtb.setTrxTarBancaria(new TrxTarBancaria());
			dtb.getTrxTarBancaria().setRow(new ArrayList<>());
			dtb.getTrxTarBancaria().getRow().add(trxTarBancariaRow);

			request.getDocument().getData().add(dtb);
		}

		boolean isMultipagos = inOrdenCompra.getTarjetaBancaria().getCorrelativoVenta() != null 
				&& ((Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)
						&& Constantes.CANAL_SAFETYPAY.equals(inOrdenCompra.getTarjetaBancaria().getDescripcionCanal().trim()))
						|| Constantes.FORMA_PAGO_TIENDA.equals(formaPago)
						|| Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
						|| Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago)
						|| Constantes.FORMA_PAGO_PAYPAL.equals(formaPago));

		if(isMultipagos) {

			TrxMultipagosBoRow trxMultipagosBo = generarTrxMultipagosBoRow(inOrdenCompra, parametros, nroTransaccion, esMkp);
			listaMultipagos.add(trxMultipagosBo);
			trxMultipagoBo.setRow(listaMultipagos);

			DataTrxMultipagosBo dtm = new DataTrxMultipagosBo();
			dtm.setTrxMultipagosBo(new TrxMultipagosBo());
			dtm.getTrxMultipagosBo().setRow(new ArrayList<>());
			dtm.getTrxMultipagosBo().getRow().add(trxMultipagosBo);

			request.getDocument().getData().add(dtm);

		}

		if(esMkp) {

			TrxRecaudacionBoRow trxRecaudacion = generarTrxRecaudacionBoRow(inOrdenCompra, parametros, nroTransaccion);
			listaRecaudacion.add(trxRecaudacion);
			trxRecaudacionBo.setRow(listaRecaudacion);

			DataTrxRecaudacionBo dtrb = new DataTrxRecaudacionBo();
			dtrb.setTrxRecaudacionBo(new TrxRecaudacionBo());
			dtrb.getTrxRecaudacionBo().setRow(new ArrayList<>());
			dtrb.getTrxRecaudacionBo().getRow().add(trxRecaudacion);

			request.getDocument().getData().add(dtrb);

			List<TrxRecaudacionPrvBoRow> trxRecaudacionPrvBo = generarTrxRecaudacionPrv(inOrdenCompra, parametros, nroTransaccion);
			trxRecaudacionPrv.setRow(trxRecaudacionPrvBo);

			DataTrxRecaudacionPrvBo dtrpb = new DataTrxRecaudacionPrvBo();
			dtrpb.setTrxRecaudacionPrvBo(new TrxRecaudacionPrvBo());
			dtrpb.getTrxRecaudacionPrvBo().setRow(new ArrayList<>());
			dtrpb.getTrxRecaudacionPrvBo().getRow().addAll(trxRecaudacionPrvBo);

			request.getDocument().getData().add(dtrpb);
		}
		LOG.endTrace("convert", "Finaliza", "RequestBO");
		return request;
	}

	private TrxRecaudacionBoRow generarTrxRecaudacionBoRow (TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion) {
		try {
			TrxRecaudacionBoRow trxRecaudacionBoRow = new TrxRecaudacionBoRow();
			BigDecimal cantidadMkp = Util.getSubtotal(inOrdenCompra, Constantes.ES_MKP_MKP);
	
	
			trxRecaudacionBoRow.setCodigoCia(Integer.valueOf(Constantes.NRO_RECAUDACION));
			trxRecaudacionBoRow.setCodigoProd(Constantes.NRO_CERO);
			trxRecaudacionBoRow.setCodigoSubprod(Constantes.NRO_CERO);
			trxRecaudacionBoRow.setMonto(cantidadMkp);
			trxRecaudacionBoRow.setCantidad(cantidadMkp.add(Util.getSubtotal(inOrdenCompra, Constantes.ES_MKP_RIPLEY)));
			trxRecaudacionBoRow.setObservacion(String.valueOf(Constantes.NRO_CERO));
			trxRecaudacionBoRow.setGlosa(String.valueOf(Constantes.NRO_CERO));
	
			return trxRecaudacionBoRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxRecaudacionBoRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionBoRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxRecaudacionBoRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionBoRow, Exception: " + e.getMessage());
		}
	}

	private List<TrxRecaudacionPrvBoRow> generarTrxRecaudacionPrv(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion) {
		try {
			List<TrxRecaudacionPrvBoRow> list = new ArrayList<>();
	
			Map<String, List<ArticuloVentaTramaDTE>> tramasMap =	inOrdenCompra.getArticuloVentas().stream()
					.filter(art -> Constantes.ES_MKP_MKP == art.getIdentificadorMarketplace().getEsMkp().intValue())
					.collect(Collectors.groupingBy(art -> art.getArticuloVenta().getOrdenMkp().trim()));
	
			for(List<ArticuloVentaTramaDTE> item : tramasMap.values()) {
	
				BigDecimal suma = item.stream()
						.map(i -> i.getArticuloVenta().getPrecio().multiply(BigDecimal.valueOf(i.getArticuloVenta().getUnidades())).subtract(i.getArticuloVenta().getMontoDescuento()))
						.reduce(BigDecimal.ZERO, BigDecimal::add);
	
	
				TrxRecaudacionPrvBoRow result = new TrxRecaudacionPrvBoRow();
				result.setCodigoCia(Integer.valueOf(Constantes.NRO_RECAUDACION));
				result.setComision(BigDecimal.ZERO);
				result.setProvRut(item.get(Constantes.NRO_CERO).getIdentificadorMarketplace().getRutProveedor());
				result.setSuborden(item.get(Constantes.NRO_CERO).getIdentificadorMarketplace().getOrdenMkp());
	
				result.setMonto(suma);
	
				list.add(result);
	
	
			}
	
			return list;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxRecaudacionPrv", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionPrv, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxRecaudacionPrv", "Exception" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionPrv, Exception: " + e.getMessage());
		}
	}

	private TrxMultipagosBoRow generarTrxMultipagosBoRow (TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) {
		try {
			TrxMultipagosBoRow trxMultipagosRow = new TrxMultipagosBoRow();
			String formaPago = Util.getFormaPago(inOrdenCompra);
			String nroTarjeta = inOrdenCompra.getTarjetaBancaria().getBinNumber() != null 
					&& inOrdenCompra.getTarjetaBancaria().getUltimosDigitosTarjeta() != null ? 
							inOrdenCompra.getTarjetaBancaria().getBinNumber() + "999999" + inOrdenCompra.getTarjetaBancaria().getUltimosDigitosTarjeta() 
							: null;
	
			if(Constantes.CODIGO_CONTRAEFECTIVO.equals(formaPago)) {
	
				trxMultipagosRow.setTipoPago(Constantes.NRO_VEINTIUNO);
				
				trxMultipagosRow.setIdProveedor(String.valueOf(inOrdenCompra.getDespacho().getRutDespacho()));
				try {
					trxMultipagosRow.setIdProveedor(String.valueOf( ordenCompraDAO.getIdProveedor(inOrdenCompra.getNotaVenta().getCorrelativoVenta())));
					
				} catch (Exception e) {
					trxMultipagosRow.setIdProveedor(String.valueOf(Constantes.NRO_CERO));
				}
				trxMultipagosRow.setIdTrx(String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
				trxMultipagosRow.setCodAut(String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
				trxMultipagosRow.setPlazo(Constantes.NRO_CERO);
				trxMultipagosRow.setDiferido(Constantes.NRO_CERO);
				trxMultipagosRow.setObservacion(String.valueOf(Constantes.NRO_CERO));
				trxMultipagosRow.setGlosa(String.valueOf(Constantes.NRO_CERO));
	
			} else if(Constantes.FORMA_PAGO_TIENDA.equals(formaPago)) {
	
				trxMultipagosRow.setTipoPago(Constantes.NRO_VEINTICINCO);
				trxMultipagosRow.setIdProveedor(String.valueOf(Constantes.NRO_CERO));
				trxMultipagosRow.setIdTrx(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador());
				trxMultipagosRow.setCodAut(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador());
				trxMultipagosRow.setPlazo(inOrdenCompra.getTarjetaBancaria().getPlazo());
				trxMultipagosRow.setDiferido(Integer.valueOf(inOrdenCompra.getTarjetaBancaria().getDiferido()));
				trxMultipagosRow.setObservacion(nroTarjeta);
				trxMultipagosRow.setGlosa(inOrdenCompra.getTarjetaBancaria().getGlosa().trim());
	
			} else {
	
				trxMultipagosRow.setTipoPago(Constantes.NRO_VEINTITRES);
				trxMultipagosRow.setIdProveedor(String.valueOf(Constantes.NRO_CERO));
				trxMultipagosRow.setIdTrx(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador());
				trxMultipagosRow.setCodAut(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador());
				trxMultipagosRow.setPlazo(inOrdenCompra.getTarjetaBancaria().getPlazo());
				trxMultipagosRow.setDiferido(Integer.valueOf(inOrdenCompra.getTarjetaBancaria().getDiferido()));
				trxMultipagosRow.setObservacion(nroTarjeta);
				trxMultipagosRow.setGlosa(inOrdenCompra.getTarjetaBancaria().getGlosa().trim());
	
			}
	
			trxMultipagosRow.setNroOc(String.valueOf(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
			trxMultipagosRow.setMonto(Util.getSubtotal(inOrdenCompra, esMkp ? Constantes.NRO_UNO : Constantes.NRO_CERO));
	
			return trxMultipagosRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxMultipagosBoRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxMultipagosBoRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxMultipagosBoRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxMultipagosBoRow, Exception: " + e.getMessage());
		}
	}

	private List<TrxDespachoRow> generarTrxDespachoRow (TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) {
		try {
			List<TrxDespachoRow> lista = new ArrayList<TrxDespachoRow>();
	
			for(ArticuloVentaTramaDTE art : inOrdenCompra.getArticuloVentas()) {
				if (!art.getArticuloVenta().getDescRipley().equalsIgnoreCase(Constantes.DESC_COSTO_ENVIO)
						&& esMkp
						&& art.getIdentificadorMarketplace().getEsMkp() == Constantes.ES_MKP_MKP) {
					TrxDespachoRow trxDespachoRow = new TrxDespachoRow();
	
					trxDespachoRow.setCodUniDes(Util.getCud(art));
					trxDespachoRow.setColor(art.getArticuloVenta().getColor());
					trxDespachoRow.setCubicaje(Constantes.NRO_UNO_B);
					trxDespachoRow.setRutDespacho(inOrdenCompra.getDespacho().getRutDespacho());
					trxDespachoRow.setNombreDespacho(inOrdenCompra.getDespacho().getNombreDespacho());
					trxDespachoRow.setFechaDespacho(String.format(Constantes.FORMATO_FECHA_4, inOrdenCompra.getDespacho().getFechaDespacho()));
					trxDespachoRow.setSucursalDespacho(inOrdenCompra.getDespacho().getSucursalDespacho());
					trxDespachoRow.setComunaDespacho(inOrdenCompra.getDespacho().getComunaDespacho());
					trxDespachoRow.setCiudadDespacho(Constantes.NRO_CERO);
					trxDespachoRow.setRegionDespacho(inOrdenCompra.getDespacho().getRegionDespacho());
					trxDespachoRow.setDireccionDespacho(String.format(Constantes.FORMATO_HASTA_40_CHR, inOrdenCompra.getDespacho().getDireccionDespacho()));
					trxDespachoRow.setTelefonoDespacho(inOrdenCompra.getDespacho().getTelefonoDespacho());
					trxDespachoRow.setSectorDespacho(Constantes.NRO_CERO);
					trxDespachoRow.setJornadaDespacho(inOrdenCompra.getDespacho().getJornadaDespacho());
					trxDespachoRow.setObservacion(inOrdenCompra.getDespacho().getObservacion());
					trxDespachoRow.setPaginaCtc(Constantes.NRO_UNO);
					trxDespachoRow.setCordCtc(Constantes.PCTC_COORDINATE);
					trxDespachoRow.setRutCliente(inOrdenCompra.getDespacho().getRutCliente());
					trxDespachoRow.setDireccCliente(String.format(Constantes.FORMATO_HASTA_40_CHR, inOrdenCompra.getDespacho().getDireccionCliente()));
					trxDespachoRow.setTelefonoCliente(inOrdenCompra.getDespacho().getTelefonoCliente());
					trxDespachoRow.setTipoCliente(inOrdenCompra.getDespacho().getTipoCliente());
					trxDespachoRow.setNombreCliente(inOrdenCompra.getDespacho().getNombreCliente());
	
					lista.add(trxDespachoRow);
				}
	
				if (!art.getArticuloVenta().getDescRipley().equalsIgnoreCase(Constantes.DESC_COSTO_ENVIO)
						&& !esMkp
						&& art.getIdentificadorMarketplace().getEsMkp() == Constantes.ES_MKP_RIPLEY) {
					TrxDespachoRow trxDespachoRow = new TrxDespachoRow();
	
					trxDespachoRow.setCodUniDes(Util.getCud(art));
					trxDespachoRow.setColor(art.getArticuloVenta().getColor());
					trxDespachoRow.setCubicaje(Constantes.NRO_UNO_B);
					trxDespachoRow.setRutDespacho(inOrdenCompra.getDespacho().getRutDespacho());
					trxDespachoRow.setNombreDespacho(inOrdenCompra.getDespacho().getNombreDespacho());
					trxDespachoRow.setFechaDespacho(String.format(Constantes.FORMATO_FECHA_4, inOrdenCompra.getDespacho().getFechaDespacho()));
					trxDespachoRow.setSucursalDespacho(inOrdenCompra.getDespacho().getSucursalDespacho());
					trxDespachoRow.setComunaDespacho(inOrdenCompra.getDespacho().getComunaDespacho());
					trxDespachoRow.setCiudadDespacho(Constantes.NRO_CERO);
					trxDespachoRow.setRegionDespacho(inOrdenCompra.getDespacho().getRegionDespacho());
					trxDespachoRow.setDireccionDespacho(String.format(Constantes.FORMATO_HASTA_40_CHR, inOrdenCompra.getDespacho().getDireccionDespacho()));
					trxDespachoRow.setTelefonoDespacho(inOrdenCompra.getDespacho().getTelefonoDespacho());
					trxDespachoRow.setSectorDespacho(Constantes.NRO_CERO);
					trxDespachoRow.setJornadaDespacho(inOrdenCompra.getDespacho().getJornadaDespacho());
					trxDespachoRow.setObservacion(inOrdenCompra.getDespacho().getObservacion());
					trxDespachoRow.setPaginaCtc(Constantes.NRO_UNO);
					trxDespachoRow.setCordCtc(Constantes.PCTC_COORDINATE);
					trxDespachoRow.setRutCliente(inOrdenCompra.getDespacho().getRutCliente());
					trxDespachoRow.setDireccCliente(String.format(Constantes.FORMATO_HASTA_40_CHR, inOrdenCompra.getDespacho().getDireccionCliente()));
					trxDespachoRow.setTelefonoCliente(inOrdenCompra.getDespacho().getTelefonoCliente());
					trxDespachoRow.setTipoCliente(inOrdenCompra.getDespacho().getTipoCliente());
					trxDespachoRow.setNombreCliente(inOrdenCompra.getDespacho().getNombreCliente());
	
					lista.add(trxDespachoRow);
				}
			}
	
			return lista;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxDespachoRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxDespachoRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxDespachoRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxDespachoRow, Exception: " + e.getMessage());
		}
	}

	private TrxTarBancariaRow generarTrxTarBancariaRow(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) {
		try {
			TrxTarBancariaRow trxTarBancariaRow = new TrxTarBancariaRow();
			String formaPago = Util.getFormaPago(inOrdenCompra);
	
			String codAutorizador = String.valueOf(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador() == null ? inOrdenCompra.getTarjetaBancaria().getUltimosDigitosTarjeta() : inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador());
			String numOrden = String.format("%08d", inOrdenCompra.getNotaVenta().getCorrelativoVenta());
	
	
			for (int i=0; i<=codAutorizador.length()-1;i++) {
	
				if (!(Character.isDigit(codAutorizador.charAt(i)))) {
					codAutorizador = codAutorizador.replace(codAutorizador.charAt(i), '9');
				}
			} 
	
			codAutorizador = String.format("%08d", Integer.valueOf(codAutorizador));	
	
			if(Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)) {
	
				String numTar = String.format(Constantes.FORMATO_10_ESPACIOS,Integer.valueOf(codAutorizador)).replace(Constantes.CHAR_SPACE, Constantes.CHAR_NUEVE);
				codAutorizador = inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador();
				trxTarBancariaRow.setNroTarjeta(Constantes.NRO_BIN_DICECIOCHO + numTar);
				trxTarBancariaRow.setCodAutorizador(Long.valueOf(codAutorizador));
	
			} else if(Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
				String numTar = null;
				numTar = String.format(Constantes.FORMATO_10_CEROS_IZQ, inOrdenCompra.getNotaVenta().getCorrelativoVenta());
				trxTarBancariaRow.setNroTarjeta(Constantes.BIN_DIECINUEVE + numTar);
				trxTarBancariaRow.setCodAutorizador(Long.valueOf(numOrden));
			} else {
	
				String numTar = Util.getPan(inOrdenCompra);
				trxTarBancariaRow.setNroTarjeta(numTar.replace(Constantes.CHAR_ASTERISCO, Constantes.CHAR_NUEVE));
	
				codAutorizador = inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador();
	
				for (int i=0; i<=codAutorizador.length()-1;i++) {
	
	
					if (!(Character.isDigit(codAutorizador.charAt(i)))) {
						codAutorizador = codAutorizador.replace(codAutorizador.charAt(i), Constantes.CHAR_NUEVE);
					}
				} 
	
				codAutorizador = String.format("%08d", Integer.valueOf(codAutorizador));	
	
				trxTarBancariaRow.setCodAutorizador(Long.valueOf(codAutorizador));
	
			}
	
			trxTarBancariaRow.setRutCliente((inOrdenCompra.getNotaVenta().getRutComprador()));
			trxTarBancariaRow.setMonto(Util.getSubtotal(inOrdenCompra, esMkp ? Constantes.NRO_UNO : Constantes.NRO_CERO));
			trxTarBancariaRow.setTipoCuota(Integer.valueOf(Constantes.NRO_CERO));
			trxTarBancariaRow.setNroCuotas(Integer.valueOf(Constantes.NRO_CERO));
			trxTarBancariaRow.setFechaVence(Integer.valueOf(Constantes.NRO_CERO));
			trxTarBancariaRow.setPorcParticip(new BigDecimal(Constantes.NRO_CERO));
			trxTarBancariaRow.setMontoIntereses(new BigDecimal(Constantes.NRO_CERO));
			trxTarBancariaRow.setValorCuota(new BigDecimal(Constantes.NRO_CERO));
			trxTarBancariaRow.setCodReferencia(Integer.valueOf(Constantes.NRO_CERO));
	
			return trxTarBancariaRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxTarBancariaRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxTarBancariaRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxTarBancariaRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxTarBancariaRow, Exception: " + e.getMessage());
		}
	}

	private  List<TrxArticulosRow> generarTrxArticulosRow(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) {
		try {
			List<TrxArticulosRow> listaArticulo = new ArrayList<TrxArticulosRow>();
			List<ArticuloVentaTramaDTE> listaArticulosRow = inOrdenCompra.getArticuloVentas();
			Integer rutVendedor = inOrdenCompra.getVendedor().getRutVendedor().isEmpty() ? Integer.valueOf(parametros.buscaValorPorNombre("RUT_VENDEDOR")) : Integer.valueOf(inOrdenCompra.getVendedor().getRutVendedor());
			boolean despachoDomicilioRpos = parametros.buscaValorPorNombre(Constantes.PARAM_DESPACHO_RPOS).equals(Constantes.STRING_UNO)?true:false;
			for (ArticuloVentaTramaDTE art : listaArticulosRow){
	
				TrxArticulosRow trxArticulosRow = null;
	
				if(Constantes.ES_MKP_MKP == art.getIdentificadorMarketplace().getEsMkp().intValue() && esMkp) {
					trxArticulosRow = new TrxArticulosRow();
					trxArticulosRow.setNroItem(art.getArticuloVenta().getCorrelativoItem());
					trxArticulosRow.setCodArticulo(art.getArticuloVenta().getCodArticulo());
					trxArticulosRow.setVendedor(rutVendedor);
					trxArticulosRow.setCodigoNovios(Constantes.NRO_CERO);
					trxArticulosRow.setSaleArticulo(Integer.valueOf(Constantes.NRO_CERO));
					trxArticulosRow.setPrecioArticulo(art.getArticuloVenta().getPrecio().multiply(new BigDecimal(art.getArticuloVenta().getUnidades())));
					trxArticulosRow.setUnidades(new BigDecimal(art.getArticuloVenta().getUnidades()));
					trxArticulosRow.setDescuento(art.getArticuloVenta().getMontoDescuento());
					trxArticulosRow.setTipoDescuento(art.getArticuloVenta().getTipoDescuento());
					trxArticulosRow.setCodUnDes(art.getArticuloVenta().getCodDespacho());
					trxArticulosRow.setProcParticip(new BigDecimal(Constantes.NRO_CERO));
					if(despachoDomicilioRpos)
						trxArticulosRow.setDespachoDomicilio(Constantes.NRO_CERO);
					else
						trxArticulosRow.setDespachoDomicilio(Constantes.NRO_CINCO);
					trxArticulosRow.setIndicador(Constantes.NRO_CERO);
					trxArticulosRow.setCodCombo(String.valueOf(Constantes.NRO_CERO));
					trxArticulosRow.setNumEvento(Constantes.NUM_EVENTO);
					trxArticulosRow.setPrioridad(Constantes.NRO_NOVENTAOCHO);
					trxArticulosRow.setSubPrioridad(Constantes.NRO_NOVENTAOCHO);
				}
	
				if(Constantes.ES_MKP_RIPLEY == art.getIdentificadorMarketplace().getEsMkp().intValue() && !esMkp) {
					trxArticulosRow = new TrxArticulosRow();
					trxArticulosRow.setNroItem(art.getArticuloVenta().getCorrelativoItem());
					trxArticulosRow.setCodArticulo(art.getArticuloVenta().getCodArticulo());
					trxArticulosRow.setVendedor(rutVendedor);
					trxArticulosRow.setCodigoNovios(Constantes.NRO_CERO);
					trxArticulosRow.setSaleArticulo(Integer.valueOf(Constantes.NRO_CERO));
					trxArticulosRow.setPrecioArticulo(art.getArticuloVenta().getPrecio().multiply(new BigDecimal(art.getArticuloVenta().getUnidades())));
					trxArticulosRow.setUnidades(new BigDecimal(art.getArticuloVenta().getUnidades()));
					trxArticulosRow.setDescuento(art.getArticuloVenta().getMontoDescuento());
					trxArticulosRow.setTipoDescuento(art.getArticuloVenta().getTipoDescuento());
					trxArticulosRow.setCodUnDes(art.getArticuloVenta().getCodDespacho());
					trxArticulosRow.setProcParticip(new BigDecimal(Constantes.NRO_CERO));
					if(despachoDomicilioRpos)
						trxArticulosRow.setDespachoDomicilio(Constantes.NRO_CERO);
					else
						trxArticulosRow.setDespachoDomicilio(Constantes.NRO_CINCO);
					trxArticulosRow.setIndicador(Constantes.NRO_CERO);
					trxArticulosRow.setCodCombo(String.valueOf(Constantes.NRO_CERO));
					trxArticulosRow.setNumEvento(Constantes.NUM_EVENTO);
					trxArticulosRow.setPrioridad(Constantes.NRO_NOVENTAOCHO);
					trxArticulosRow.setSubPrioridad(Constantes.NRO_NOVENTAOCHO);
				}
	
	
				if(trxArticulosRow != null) {
					listaArticulo.add(trxArticulosRow);
				}
	
			} 
	
			return  listaArticulo;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxArticulosRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxArticulosRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxArticulosRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxArticulosRow, Exception: " + e.getMessage());
		}
	}

	private TrxEticketRow generarTrxEticketRow(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, String boleta) {
		try {
			TrxEticketRow trxEticketRow = new TrxEticketRow();
	
			String[] bol = boleta.split(Constantes.GUION);	
			trxEticketRow.setEtkNroSerie(bol[Constantes.NRO_CERO]);
	
	
			trxEticketRow.setEtkCorrelativo(bol[Constantes.NRO_UNO]);
			trxEticketRow.setEtkTipoDoc(inOrdenCompra.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_BOLETA ?
					String.valueOf(Constantes.NRO_TRES) : String.valueOf(Constantes.NRO_UNO));
			trxEticketRow.setEtkNroSerieOrig(String.valueOf(Constantes.NRO_CERO));
			trxEticketRow.setEtkCorrelativoOrig(String.valueOf(Constantes.NRO_CERO));
	
			return trxEticketRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxEticketRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxEticketRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxEticketRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxEticketRow, Exception: " + e.getMessage());
		}
	}

	private TrxReferenciaAddBoRow generarTrxReferenciaAddBORow(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion)  {
		try {
			TrxReferenciaAddBoRow trxReferenciaAddBoRow = new TrxReferenciaAddBoRow();
			String formaPago = Util.getFormaPago(inOrdenCompra);
			
			if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
	
				trxReferenciaAddBoRow.setTipoPago(Constantes.NRO_TRES);
				trxReferenciaAddBoRow.setCodAut(String.valueOf(inOrdenCompra.getTarjetaRipley().getCodigoAutorizacion()));
				trxReferenciaAddBoRow.setNroTarjeta(String.valueOf(inOrdenCompra.getTarjetaRipley().getPan().longValue()));
			} else if(Constantes.FORMA_PAGO_DEBITO.equals(formaPago) 
					|| Constantes.FORMA_PAGO_TDC.equals(formaPago)
					|| Constantes.FORMA_PAGO_EFECTIVO.equals(formaPago)
					|| Constantes.FORMA_PAGO_PAYPAL.equals(formaPago)) {
	
				trxReferenciaAddBoRow.setTipoPago(Constantes.NRO_CUATRO);
				trxReferenciaAddBoRow.setCodAut(String.valueOf(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador()));
				String numTar = Constantes.VACIO;
				
				switch (formaPago) {
				case Constantes.FORMA_PAGO_EFECTIVO:
					numTar = Constantes.NRO_BIN_DICECIOCHO + String.format(Constantes.FORMATO_10_ESPACIOS, Long.valueOf(inOrdenCompra.getTarjetaBancaria().getCodigoAutorizador()))
																.replace(Constantes.CHAR_SPACE, Constantes.CHAR_NUEVE);
					break;
					
				case Constantes.FORMA_PAGO_PAYPAL:
					numTar = Constantes.BIN_DIECINUEVE + String.format(Constantes.FORMATO_10_CEROS_IZQ, inOrdenCompra.getNotaVenta().getCorrelativoVenta());
					break;
	
				default:
					numTar = Util.getPan(inOrdenCompra).replace(Constantes.CHAR_ASTERISCO, Constantes.CHAR_NUEVE);
					break;
				}
				
				trxReferenciaAddBoRow.setNroTarjeta(numTar);
	
			}
			
			trxReferenciaAddBoRow.setIdTrx(Long.toString(nroTransaccion));
	
			trxReferenciaAddBoRow.setNroOc(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
			trxReferenciaAddBoRow.setFechaPago(String.format(Constantes.FORMATO_FECHA_4, inOrdenCompra.getNotaVenta().getFechaCreacion()));
	
	
			return trxReferenciaAddBoRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxReferenciaAddBORow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxReferenciaAddBORow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxReferenciaAddBORow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxReferenciaAddBORow, Exception: " + e.getMessage());
		}
	}

	private TrxTarRipleyRow generarTrxTarRipleyRow (TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, boolean esMkp) {
		try {
			TrxTarRipleyRow trxTarRipleyRow = new TrxTarRipleyRow();
	
			trxTarRipleyRow.setAdminis(Constantes.NRO_CUATRO);
			trxTarRipleyRow.setEmisor(Constantes.NRO_UNO); 
			trxTarRipleyRow.setRutPoder(inOrdenCompra.getNotaVenta().getRutComprador());
			trxTarRipleyRow.setPlazo(inOrdenCompra.getTarjetaRipley().getPlazo());
			trxTarRipleyRow.setDiferido(inOrdenCompra.getTarjetaRipley().getDiferido());
			trxTarRipleyRow.setMontoCapital(Util.getSubtotal(inOrdenCompra, esMkp ? Constantes.NRO_UNO : Constantes.NRO_CERO));
			trxTarRipleyRow.setMontoPie(BigDecimal.ZERO);
			System.out.println("valor cuota: "+inOrdenCompra.getTarjetaRipley().getValorCuota());
			trxTarRipleyRow.setDesctoCar((Util.getSubtotal(inOrdenCompra, esMkp ? Constantes.NRO_UNO : Constantes.NRO_CERO).multiply(inOrdenCompra.getTarjetaRipley().getValorCuota())).divide(inOrdenCompra.getNotaVenta().getMontoVenta(),2,RoundingMode.HALF_UP));
			System.out.println("Descuento car: "+trxTarRipleyRow.getDesctoCar());
			trxTarRipleyRow.setCodigoDescto(Constantes.NRO_CERO);
			trxTarRipleyRow.setCodAutorizador(inOrdenCompra.getTarjetaRipley().getCodigoAutorizacion().longValue());
			trxTarRipleyRow.setPorcParticip(BigDecimal.ZERO);
			trxTarRipleyRow.setRutTitular(inOrdenCompra.getNotaVenta().getRutComprador());
			trxTarRipleyRow.setCuentaLarga(inOrdenCompra.getTarjetaRipley().getPan().longValue());
			return trxTarRipleyRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxTarRipleyRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxTarRipleyRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxTarRipleyRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxTarRipleyRow, Exception: " + e.getMessage());
		}
	}

	private TrxTransaccionAddRow generarTrxTransaccionAddRow(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, Long nroTrxAnterior, boolean esMkp, LocalDateTime ldtFechaTrx,
			boolean isRipleyProcessed) {
		try {
			TrxTransaccionAddRow trxTransaccionAddRow = new TrxTransaccionAddRow();
	
			if(esMkp == true) {
	
				if(Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {
					
					Optional<Long> optNroTrxAnterior = Optional.ofNullable(nroTrxAnterior);
					trxTransaccionAddRow.setAuxiliar5(optNroTrxAnterior.isPresent() ? optNroTrxAnterior.get().intValue() : nroTransaccion.intValue());
					trxTransaccionAddRow.setFechaAux1(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(isRipleyProcessed ? inOrdenCompra.getNotaVenta().getFechaBoleta().toLocalDateTime() : ldtFechaTrx));
					
				} else {
					
					trxTransaccionAddRow.setAuxiliar5(Constantes.NRO_CERO);
					trxTransaccionAddRow.setFechaAux1(Constantes.FECHA_ORIGINAL);
					
				}
				
				trxTransaccionAddRow.setAdicional2(Constantes.MKP);
			}
			else {
	
				trxTransaccionAddRow.setAuxiliar5(Constantes.NRO_CERO);
				if(Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {
					trxTransaccionAddRow.setAdicional2(Constantes.MKP);
				}else {
					trxTransaccionAddRow.setAdicional2(String.valueOf(Constantes.NRO_CERO));
				}
				trxTransaccionAddRow.setFechaAux1(Constantes.FECHA_ORIGINAL);
			}
	
			Optional<Integer> optSucursalMac = Optional.ofNullable(inOrdenCompra.getNotaVenta().getSucursalMac());
			
			trxTransaccionAddRow.setSucursalFis(optSucursalMac.isPresent() ? optSucursalMac.get() : Integer.valueOf(parametros.buscaValorPorNombre("SUCURSAL")));
			
			Optional<String> optPcMac = Optional.ofNullable(inOrdenCompra.getNotaVenta().getPcMac());
			
			trxTransaccionAddRow.setPcMac(optPcMac.isPresent() ? optPcMac.get() : Constantes.VACIO);
			trxTransaccionAddRow.setAuxiliar1(Constantes.NRO_CERO);
			trxTransaccionAddRow.setAuxiliar2(Constantes.NRO_CERO);
			trxTransaccionAddRow.setAuxiliar3(Constantes.NRO_CERO);
			trxTransaccionAddRow.setAuxiliar4(Constantes.NRO_CERO);
			List<String> tipoDespachoCodBodega = Util.getTipoDespachoCodBodega(inOrdenCompra);
			if(tipoDespachoCodBodega != null && !tipoDespachoCodBodega.isEmpty() 
					&& Constantes.NRO_DOS <= tipoDespachoCodBodega.size() 
					&& Constantes.BOL_TIPO_DES_RT.equalsIgnoreCase(tipoDespachoCodBodega.get(Constantes.NRO_CERO))) {
				String codBodega = tipoDespachoCodBodega.get(Constantes.NRO_UNO);
				int lengthCodBodega = codBodega.length();
				if(Constantes.NRO_DOS < lengthCodBodega) {
					codBodega = codBodega.substring(lengthCodBodega - Constantes.NRO_DOS, lengthCodBodega);
					trxTransaccionAddRow.setAdicional1(codBodega);
				}else {
					trxTransaccionAddRow.setAdicional1(codBodega);
				}
			}else {
				trxTransaccionAddRow.setAdicional1(Constantes.NRO_CERO+ Constantes.VACIO);
			}
			trxTransaccionAddRow.setAdicional3(Integer.toString(Constantes.NRO_CERO)); 
			trxTransaccionAddRow.setAdicional4(Integer.toString(Constantes.NRO_CERO));
			trxTransaccionAddRow.setAdicional5(Long.toString(inOrdenCompra.getNotaVenta().getCorrelativoVenta()));
			trxTransaccionAddRow.setFechaEmision(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(ldtFechaTrx));
	
			return trxTransaccionAddRow;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxTarRipleyRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxTarRipleyRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxTarRipleyRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxTarRipleyRow, Exception: " + e.getMessage());
		}
	}

	private Header generarHeader(TramaDTE inOrdenCompra, Parametros parametros, Long nroTransaccion, Long nroTrxAnterior, boolean esMkp, LocalDateTime ldtFechaTrx,
			boolean isRipleyProcessed, boolean isMkpProcessed) throws AligareException {
		try {
			Header header = new Header();
			Long rutVendedor = inOrdenCompra.getVendedor().getRutVendedor().isEmpty() ? Long.valueOf(parametros.buscaValorPorNombre("RUT_VENDEDOR")) : Long.valueOf(inOrdenCompra.getVendedor().getRutVendedor());
			if(esMkp == true ) {
	
				header.setTipoTrx(Constantes.NRO_VEINTISIETE);
				
				if(Constantes.IND_MKP_AMBAS.equals(inOrdenCompra.getNotaVenta().getIndicadorMkp())) {
			
					header.setSucursalOriginal(inOrdenCompra.getNotaVenta().getNumeroSucursal());
					header.setFechaOriginal(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(isRipleyProcessed ? inOrdenCompra.getNotaVenta().getFechaBoleta().toLocalDateTime() : ldtFechaTrx));
					header.setNroCajaOriginal(inOrdenCompra.getNotaVenta().getNumeroCaja());
					header.setNroDocumentoOriginal(nroTrxAnterior);
					header.setVendedorOriginal(rutVendedor);
				
				} else {
					
					header.setSucursalOriginal(Constantes.NRO_CERO);
					header.setFechaOriginal(Constantes.FECHA_ORIGINAL);
					header.setNroCajaOriginal(Constantes.NRO_CERO);
					header.setNroDocumentoOriginal(Long.valueOf(Constantes.NRO_CERO));
					header.setVendedorOriginal(Long.valueOf(Constantes.NRO_CERO));
					header.setTrxTipoDocIdentidad(Constantes.NRO_CERO);
					
				}
				
				header.setTrxTipoDocIdentidad(Integer.valueOf(inOrdenCompra.getTipoDoc().getDocOrigen().intValue() == Constantes.TIPO_DOC_IDENTIDAD ?
						String.valueOf(Constantes.NRO_UNO) : String.valueOf(Constantes.NRO_DOS)));
	
			}
			else {
	
				if(Util.getFormaPago(inOrdenCompra).equals(Constantes.FORMA_PAGO_TARJETA_RIPLEY)) {
	
					header.setTipoTrx(Constantes.NRO_DOS);
				}else {
	
					header.setTipoTrx(Constantes.NRO_UNO);
				}
	
				header.setSucursalOriginal(Constantes.NRO_CERO);
				header.setFechaOriginal(Constantes.FECHA_ORIGINAL);
				header.setNroCajaOriginal(Constantes.NRO_CERO);
				header.setNroDocumentoOriginal(Long.valueOf(Constantes.NRO_CERO));
				header.setVendedorOriginal(Long.valueOf(Constantes.NRO_CERO));
				header.setTrxTipoDocIdentidad(Constantes.NRO_CERO);
			}
			
			header.setNroDocumento(nroTransaccion);
			header.setFechaHoraTrx(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS.format(ldtFechaTrx));
			header.setSupervisor(Long.valueOf(parametros.buscaValorPorNombre("NRO_CAJA_BOLETAS")));
			header.setVendedor(rutVendedor);
			header.setMontoTrx(Util.getSubtotal(inOrdenCompra, esMkp ? Constantes.ES_MKP_MKP : Constantes.ES_MKP_RIPLEY));
			header.setOrigenTrx(Constantes.NRO_CERO);
			header.setRutCompraPaga(Long.valueOf(Constantes.NRO_CERO));
			header.setDescuentoEccsa(BigDecimal.ZERO);
			header.setDescuentoCar(BigDecimal.ZERO);
			header.setMontoPie(BigDecimal.ZERO);
			header.setEstado(Constantes.NRO_CERO);
			header.setTipoOrigen(Constantes.NRO_CERO);
			header.setTxtrnRunCajCro(Constantes.NRO_CERO);
			header.setTxtrnRunCajCroOri(Constantes.NRO_CERO);
			header.setTxtrnMntDnn(BigDecimal.ZERO);
			header.setTrxTipoDoc(inOrdenCompra.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_BOLETA ?
					Constantes.NRO_CINCO : Constantes.NRO_SIETE);
			header.setTrxTarBonus(Constantes.NRO_CERO);
			header.setTrxRuc(inOrdenCompra.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? 
					Long.valueOf(inOrdenCompra.getDatoFacturacion().getRutFactura()) : Long.valueOf(Constantes.NRO_CERO));
			header.setTrxMotNc(Constantes.NRO_CERO);
			header.setTrxTelefono(Constantes.NRO_CERO);
			header.setTrxDocIdentidad(Long.valueOf(inOrdenCompra.getNotaVenta().getRutComprador()).intValue());
			header.setNroCelular(Constantes.NRO_CERO);
			header.setNroTerminal(parametros.buscaValorPorNombre("TERMINAL"));
			header.setNroOrdenCompra(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
	
			return header;
		} catch(NullPointerException e) {
			LOG.traceInfo("generarHeader", "NullPointerException" + e.getMessage());
			throw new AligareException("generarHeaderv, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarHeader", "Exception" + e.getMessage());
			throw new AligareException("generarHeaderv, Exception: " + e.getMessage());
		}
	}

}
