package com.ripley.restservices.converters;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ripley.restservices.model.bcvRest.DpItemPromo;
import com.ripley.restservices.model.bcvRest.FiscalData;
import com.ripley.restservices.model.bcvRest.FiscalTotalData;
import com.ripley.restservices.model.bcvRest.Header;
import com.ripley.restservices.model.bcvRest.LineItem;
import com.ripley.restservices.model.bcvRest.Message;
import com.ripley.restservices.model.bcvRest.PagoVirtual;
import com.ripley.restservices.model.bcvRest.PdeCustomer;
import com.ripley.restservices.model.bcvRest.PrepareCallBoletaBCV;
import com.ripley.restservices.model.bcvRest.RequestBcv;
import com.ripley.restservices.model.bcvRest.StartSequence;
import com.ripley.restservices.model.bcvRest.Tender;
import com.ripley.restservices.model.bcvRest.Total;
import com.ripley.restservices.model.bcvRest.TransactionDiscount;
import com.ripley.restservices.model.bcvRest.header.Route;
import com.ripley.restservices.model.bcvRest.header.Transaction;
import com.ripley.restservices.model.bcvRest.header.TransactionXml;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

@Component
public class PrepareCallBoletaBCVToRequestBcvConverter implements Converter<PrepareCallBoletaBCV, RequestBcv> {
	
	private static final AriLog LOG = new AriLog(PrepareCallBoletaBCVToRequestBcvConverter.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	@Lazy
	private Marshaller bcvMarshaller1;
	
	@Autowired
	@Lazy
	private Marshaller bcvMarshaller2;

	@Override
	public RequestBcv convert(PrepareCallBoletaBCV s) {
		LOG.initTrace("convert", "PrepareCallBoletaBCV s", new KeyLog("PrepareCallBoletaBCV", String.valueOf(s)));
			try {
			TramaDTE trama = s.getTrama();
			Parametros pcv = s.getPcv();
			Long nroTrx = s.getNroTrx();
			String folioPPL = s.getFolioPPL();
			LocalDateTime ltdFechaTrx = s.getLdtFechaTrx();
			boolean flagRpos= s.isFlagRpos();
			
			Encoder e = Base64.getEncoder();
			
			String formaPago = Util.getFormaPago(trama);
			
			RequestBcv req = new RequestBcv();
			
			Message message = new Message();
			
			
			message.setType(Constantes.DCREQ);
			message.setStatus(Constantes.COMPLETE);
			message.setTentative(Constantes.FALSE_STRING);
			
			message.setPdeCustomer(new PdeCustomer());
			message.getPdeCustomer().setName(Constantes.VACIO);
			
			message.setHeader(new Header());
			
			Header header = message.getHeader();
			
			header.setFlag(Constantes.FLAG_HEADER_BCV);
			header.setBusinessMonthDay(Constantes.DATE_TIME_FORMATER_MMDD.format(ltdFechaTrx)); 
			Integer sucursal = trama.getNotaVenta().getNumeroSucursal();
			header.setRetailStoreID(String.format(Constantes.FORMATO_8_CEROS_IZQ, sucursal));
			header.setSegment(Constantes.SEGMENT_HEADER_BCV);
			header.setSystemID(Constantes.SYSTEM_ID_HEADER_BCV);
			header.setSequenceNumber(String.format(Constantes.FORMATO_8_CEROS_IZQ, nroTrx));
			header.setTransactionTypeCode(String.valueOf(trama.getTipoDoc().getCodPpl() == Constantes.TIPO_DOC_FACTURA ? Constantes.NRO_CINCUENTAYSEIS : Constantes.NRO_UNO));
			
			Integer nroCaja = trama.getNotaVenta().getNumeroCaja();
			
			header.setWorkstationID(String.format(Constantes.FORMATO_4_CEROS_IZQ, nroCaja));
			header.setBusinessYear(Constantes.DATE_TIME_FORMATER_YY.format(ltdFechaTrx));
			header.setTransactionStatus(Constantes.TRX_STATUS_HEADER_BCV);
			
			message.setStartSequence(new StartSequence());
			StartSequence ss = message.getStartSequence();
			
			Integer rutVendedor = Util.getVacioPorNulo(trama.getVendedor().getRutVendedor()).isEmpty() ? Constantes.NRO_CERO : Integer.valueOf(trama.getVendedor().getRutVendedor());
			ss.setOperatorID(String.format(Constantes.FORMATO_10_CEROS_IZQ, rutVendedor));
			String dateTime = Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_T_HH_MI_SS.format(ltdFechaTrx);
			ss.setBeginDateTime(dateTime);
			ss.setEndDateTime(dateTime);
			ss.setBusinessDayDate(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(ltdFechaTrx));
			ss.setCurrencyCode(String.valueOf(Constantes.NRO_CERO));
			ss.setCancelFlag(String.valueOf(Constantes.NRO_CERO));
			ss.setTrainingMode(String.valueOf(Constantes.NRO_CERO));
			ss.setSuspendFlag(String.valueOf(Constantes.NRO_CERO));
			ss.setTransactionTypeCode(String.valueOf(trama.getTipoDoc().getCodPpl() == Constantes.TIPO_DOC_FACTURA ? Constantes.NRO_CINCUENTAYSEIS : Constantes.NRO_UNO));
			ss.setCorrelativoNumber(String.format(Constantes.FORMATO_3_CEROS_IZQ,nroTrx));
			String vendedor = Constantes.USUARIO_TIENDA_VIRTUAL;
			ss.setCashierName(vendedor);
			ss.setVendorName(vendedor);
			ss.setVendorId(String.format(Constantes.FORMATO_10_CEROS_IZQ, rutVendedor));
			ss.setNroSeriePos(pcv.buscaValorPorNombre(Constantes.PARAM_TERMINAL)); 
			ss.setNota(String.valueOf(trama.getNotaVenta().getCorrelativoVenta()));//No aparece en documentacion
			ss.setDespachoDireccion(trama.getDespacho().getDireccionDespacho());//No aparece en documentacion
			ss.setDespachoDistrito(trama.getDespacho().getComunaDespachoDes());//No aparece en documentacion
			ss.setDespachoFecha(String.format(Constantes.FORMATO_FECHA, trama.getDespacho().getFechaDespacho()));//No aparece en documentacion
			ss.setCodigoSucursal(String.format(Constantes.FORMATO_3_CEROS_IZQ, Integer.valueOf(String.valueOf(Constantes.NRO_UNO) + trama.getDespacho().getSucursalDespacho())));
			ss.setSolesInDrawer(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			ss.setDolaresInDrawer(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			ss.setDolaresExchRate(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			
			String[] correlativoPPL = folioPPL.split(Constantes.GUION);
			
			ss.setElectronicSerialNumber(correlativoPPL[Constantes.NRO_CERO]);
			
			String electronicCorrelative = String.format(Constantes.FORMATO_6_CEROS_IZQ, Integer.valueOf(correlativoPPL[Constantes.NRO_UNO]));
			ss.setElectronicCorrelative(electronicCorrelative);
			ss.setElectronicFolio(ss.getElectronicSerialNumber() + Constantes.GUION + ss.getElectronicCorrelative());
			ss.setElectronicDocumentType(String.format(Constantes.FORMATO_2_CEROS_IZQ, trama.getTipoDoc().getCodPpl()));
			
			message.setFiscalTotalData(new FiscalTotalData());
			FiscalTotalData ftd = message.getFiscalTotalData();
			
			BigDecimal monto_venta = Util.getSubtotal(trama, Constantes.ES_MKP_RIPLEY);//trama.getNotaVenta().getMontoVenta();
			BigDecimal iva =  new BigDecimal(pcv.buscaValorPorNombre(Constantes.PARAM_VALOR_IVA_NAME));
			BigDecimal tasa_iva = iva.divide(new BigDecimal(Constantes.NRO_CIEN));
	
			BigDecimal montoTotalIvaD = monto_venta.subtract(monto_venta.divide(tasa_iva.add(new BigDecimal(Constantes.NRO_UNO)), 2, RoundingMode.HALF_UP));
			
			ftd.setTaxableAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));
			ftd.setNonTaxableAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			ftd.setVat1Amount(String.format(Locale.US, Constantes.FORMATO_MONTOS, montoTotalIvaD).replace(Constantes.PUNTO2, Constantes.VACIO));
			ftd.setInternalTaxAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			
			message.setFiscalData(new FiscalData());
			FiscalData fd = message.getFiscalData();
			
			fd.setCustomerDocumentType(String.valueOf(trama.getTipoDoc().getCodPpl() == Constantes.TIPO_DOC_FACTURA ? Constantes.NRO_SEIS : Constantes.NRO_UNO));
			
			fd.setCustomerDocumentNumber(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? 
					String.format(Constantes.FORMATO_8_CEROS_IZQ, Long.valueOf(trama.getDatoFacturacion().getRutFactura())) : String.format(Constantes.FORMATO_8_CEROS_IZQ, trama.getDespacho().getRutCliente()));
			
			fd.setCustomerName(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? 
					String.valueOf(trama.getDatoFacturacion().getRazonSocialFactura()) : 
						String.valueOf(trama.getDespacho().getNombreDespacho()));
			
			fd.setCustomerPhone(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA ? 
					String.valueOf(trama.getDatoFacturacion().getTelefonoFactura()) : 
						String.valueOf(trama.getDespacho().getTelefonoCliente()));

			if(trama.getTipoDoc().getCodPpl().intValue() == Constantes.TIPO_DOC_FACTURA) {
				fd.setInvoiceCustomerId(String.format(Constantes.FORMATO_8_CEROS_IZQ, Long.valueOf(trama.getDatoFacturacion().getRutFactura())));
				fd.setInvoiceCustomerName(String.valueOf(trama.getDatoFacturacion().getRazonSocialFactura()));
			}
			
			message.setLineItem(new ArrayList<LineItem>());
			List<LineItem> items = message.getLineItem();
			
			message.setDpItemPromo(new ArrayList<DpItemPromo>());
			List<DpItemPromo> promos = message.getDpItemPromo();
			
			BigDecimal discountTotal = BigDecimal.ZERO;
			
			for(ArticuloVentaTramaDTE art : trama.getArticuloVentas()) {
				if(art.getIdentificadorMarketplace().getEsMkp().intValue() == Constantes.ES_MKP_RIPLEY) {
					
					LineItem li = new LineItem();
	
					li.setEntryMethod(String.valueOf(Constantes.NRO_UNO));
	//				li.setPosItemID(String.valueOf(art.getArticuloVenta().getCorrelativoVenta()) + String.valueOf(art.getArticuloVenta().getCorrelativoItem()));
					li.setPosItemID(String.format(Constantes.FORMATO_14_CEROS_IZQ, Long.valueOf(art.getArticuloVenta().getCodArticulo())));
					li.setDepartment(art.getArticuloVenta().getDepto() != null ? art.getArticuloVenta().getDepto().trim() : String.valueOf(Constantes.NRO_CERO));
					li.setClass_(String.valueOf(Constantes.NRO_CERO));
					li.setCategoryCode(String.format(Constantes.FORMATO_4_CEROS_IZQ, Constantes.NRO_CERO));
					li.setDescription(art.getArticuloVenta().getDescRipley());
					li.setExtendedAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, art.getArticuloVenta().getPrecio().multiply(new BigDecimal(art.getArticuloVenta().getUnidades()))).replace(Constantes.PUNTO2, Constantes.VACIO));
					li.setQuantity(String.valueOf(art.getArticuloVenta().getUnidades() * Constantes.NRO_MIL));
					li.setReturnItem(String.valueOf(Constantes.NRO_CERO));
					li.setNonTaxable(String.valueOf(Constantes.NRO_UNO));
					li.setRelativeItemNumber(String.format(Constantes.FORMATO_8_CEROS_IZQ, art.getArticuloVenta().getCorrelativoItem()));
					li.setVatCode(String.valueOf(Constantes.NRO_UNO));
					li.setInternalTax(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
					li.setSupervisorOperationMark(String.format(Constantes.FORMATO_10_CEROS_IZQ, rutVendedor));
					li.setClassification(String.format(Constantes.FORMATO_53_CEROS_IZQ, Constantes.NRO_CERO));
					li.setSku(String.format(Constantes.FORMATO_13_CEROS_IZQ, Long.valueOf(art.getArticuloVenta().getCodArticulo())));
					li.setDespachoDomicilio(Util.getCud(art));//No aparece en documentacion
					li.setExtragarantiaChildNumber(String.valueOf(Constantes.NRO_CERO));
					li.setCodDespacho(li.getDespachoDomicilio());//No aparece en documentacion
	
					String codigoSunat = art.getArticuloVenta().getCodigoSunat();
					codigoSunat = Util.validarCodigoSunat(codigoSunat, pcv.buscaValorPorNombre("CODIGO_DEFAULT_PRODUCTO_SUNAT"));
					li.setCodProductoSUNAT(codigoSunat);			
	
					items.add(li);
	
					if(art.getArticuloVenta().getMontoDescuento() != null
							&& art.getArticuloVenta().getMontoDescuento().compareTo(BigDecimal.ZERO) > Constantes.NRO_CERO) {
	
						DpItemPromo promo = new DpItemPromo();
	
						promo.setDpPromotionCode(String.format(Constantes.FORMATO_3_CEROS_IZQ, art.getArticuloVenta().getTipoDescuento() != null ? art.getArticuloVenta().getTipoDescuento() : Constantes.NRO_CERO));
						promo.setDpPromotionDescription(art.getArticuloVenta().getDescRipley() != null ? art.getArticuloVenta().getDescRipley() : Constantes.VACIO);
						promo.setDpPromotionAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, art.getArticuloVenta().getMontoDescuento().negate()).replace(Constantes.PUNTO2, Constantes.VACIO));
						promo.setRelativeItemNumber(String.format(Constantes.FORMATO_8_CEROS_IZQ, art.getArticuloVenta().getCorrelativoItem()));
						promo.setDpRewardDepartment(art.getArticuloVenta().getDepto());
	
						promos.add(promo);
	
						discountTotal = discountTotal.subtract(art.getArticuloVenta().getMontoDescuento());
	
					}
				}
			}
			
			message.setTransactionDiscount(new TransactionDiscount());
			message.getTransactionDiscount().setTransactionDiscountReceiptTotal(String.format(Locale.US, Constantes.FORMATO_MONTOS, discountTotal).replace(Constantes.PUNTO2, Constantes.VACIO));
			
			message.setTender(new Tender());
			Tender tender = message.getTender();
			
			tender.setTenderID(pcv.buscaValorPorNombre(Constantes.PARAM_TENDER_ID_NAME));//Codigo de Forma de Pago (Efectivo=1)
			tender.setAmountDue(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));
			tender.setAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));
			tender.setAccountID(!Constantes.FORMA_PAGO_CENTREGA.equals(formaPago)
								&& !Constantes.FORMA_PAGO_CENTREGA_BANCARIA.equals(formaPago) ? 
										Util.getPan(trama).replace(Constantes.CHAR_ASTERISCO, Constantes.CHAR_NUEVE) : Constantes.VACIO);//No aparece en documentacion
			tender.setApprovalReference(String.format(Constantes.FORMATO_8_CEROS_IZQ, Constantes.NRO_CERO));//No aparece en documentacion
			tender.setTenderInFlag(String.valueOf(Constantes.NRO_UNO));
			
			
			if(Constantes.FORMA_PAGO_DEBITO.equals(formaPago) || Constantes.FORMA_PAGO_TDC.equals(formaPago)) {
				
				tender.setCardCreditDebit(String.valueOf(Constantes.NRO_UNO));//No aparece en documentacion
				
			} else {
				
				tender.setCardCreditDebit(String.valueOf(Constantes.NRO_CERO));//No aparece en documentacion
				
			}
			
			tender.setCardInstallments(String.valueOf(Constantes.NRO_DOCE));//No aparece en documentacion
			
			if(Constantes.FORMA_PAGO_TARJETA_RIPLEY.equals(formaPago)) {
				
				tender.setCardDeferred(String.valueOf(trama.getTarjetaRipley().getDiferido()));//No aparece en documentacion
				tender.setCardValorCuota(String.format(Locale.US, Constantes.FORMATO_MONTOS, trama.getTarjetaRipley().getValorCuota()).replace(Constantes.PUNTO2, Constantes.VACIO));//No aparece en documentacion
				
			} else {
				
				tender.setCardDeferred(String.valueOf(Constantes.NRO_CERO));//No aparece en documentacion
				tender.setCardValorCuota(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));//No aparece en documentacion
				
			}
			
			message.setTotal(new Total());
			Total total = message.getTotal();
			
			total.setAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));
			total.setTransactionAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, monto_venta).replace(Constantes.PUNTO2, Constantes.VACIO));
			total.setChangeAmount(String.format(Locale.US, Constantes.FORMATO_MONTOS, BigDecimal.ZERO).replace(Constantes.PUNTO2, Constantes.VACIO));
			//Falta CashAdjusmentRound pero no debe usarse. Aparece en el servidor
			
			message.setPagoVirtual(new PagoVirtual());
			PagoVirtual pv = message.getPagoVirtual();
			
			pv.setCajaReferencia(trama.getNotaVenta().getNumeroCaja());
			pv.setDesPagoVirtual(formaPago);
			pv.setFecReferencia(Constantes.DATE_TIME_FORMATER_DDMMYYYY.format(ltdFechaTrx));
			pv.setNroDocReferencia(trama.getNotaVenta().getNumeroBoleta());
			pv.setNroOrdenCompra(trama.getNotaVenta().getCorrelativoVenta());
			pv.setNroTrxReferencia(nroTrx);
			pv.setSucReferencia(sucursal);
			Integer tipoPagoVirtual = null;
			
			switch(formaPago) {
			
			case Constantes.FORMA_PAGO_CENTREGA:
				tipoPagoVirtual = Constantes.NRO_VEINTIUNO;
				break;
				
			case Constantes.FORMA_PAGO_DEBITO:
				tipoPagoVirtual = Constantes.NRO_CUATRO;
				break;
				
			case Constantes.FORMA_PAGO_TDC:
				tipoPagoVirtual = Constantes.NRO_CUATRO;
				break;
				
			case Constantes.FORMA_PAGO_EFECTIVO:
				tipoPagoVirtual = Constantes.NRO_VEINTICUATRO;
				break;
				
			case Constantes.FORMA_PAGO_PAYPAL:
				tipoPagoVirtual = Constantes.NRO_VEINTISEIS;
				break;
				
			case Constantes.FORMA_PAGO_TARJETA_RIPLEY:
				tipoPagoVirtual = Constantes.NRO_TRES;
				break;
				
			case Constantes.FORMA_PAGO_TIENDA:
				tipoPagoVirtual = Constantes.NRO_VEINTICINCO;
				break;
				
			default:
				break;
			
			}
			
			pv.setTipoPagoVirtual(tipoPagoVirtual);
			
			String messageXml = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				bcvMarshaller1.marshal(message, baos);
				messageXml = baos.toString(Constantes.CHARSET_UTF8);
			} catch (JAXBException | UnsupportedEncodingException ex) {
				LOG.traceError("convert", "Error message", ex);
			}
			
			LOG.traceInfo("convert", "XML Mensaje",new KeyLog("XML Mensaje", String.valueOf(messageXml)));
			
			Transaction transaction = new Transaction();
			String fechaTransaction = Constantes.DATE_TIME_FORMATER_YYYY_MM_DD.format(ltdFechaTrx);
			String horaTransaction = Constantes.DATE_TIME_FORMATER_HHMISS.format(ltdFechaTrx);
			transaction.setDate(fechaTransaction);
			transaction.setHour(horaTransaction);
			transaction.setTransaction(String.valueOf(nroTrx));
			transaction.setTerminal(String.format(Constantes.FORMATO_4_CEROS_IZQ, nroCaja));
			transaction.setTransactionXml(new TransactionXml());
			if(flagRpos)
				transaction.setStore(String.format(Constantes.FORMATO_8_CEROS_IZQ, sucursal));
			
			TransactionXml tx = transaction.getTransactionXml();
			tx.setRoute(new Route());
			
			Route route = tx.getRoute();
			route.setDate(Constantes.DATE_TIME_FORMATER_YYYY_MM_DD_HH_MI_SS.format(ltdFechaTrx));
			route.setTransaction(transaction.getTransaction());
			route.setTransactionXml(messageXml);
			route.setTerminal(String.format(Constantes.FORMATO_4_CEROS_IZQ, nroCaja));
			if(flagRpos)
				route.setStore(String.format(Constantes.FORMATO_8_CEROS_IZQ, sucursal));
			
			ByteArrayOutputStream baosXml = new ByteArrayOutputStream();
			
			try {
				bcvMarshaller2.marshal(transaction, baosXml);
			} catch (JAXBException ex) {
				LOG.traceError("convert", "Error marshal Transaction", ex);
			}
			
			String transactionXmlMessage = null;
			String encodedXml = null;
			try {
				transactionXmlMessage = baosXml.toString(Constantes.CHARSET_UTF8);
				encodedXml = e.encodeToString(transactionXmlMessage.getBytes(Constantes.CHARSET_UTF8));
			} catch (UnsupportedEncodingException e1) {
				LOG.traceError("convert", e1);
			}
			
			LOG.traceInfo("convert", "XML Transaction Encoded",new KeyLog("encodedXml", String.valueOf(transactionXmlMessage)));
			
			req.setMessage(encodedXml);
			
			LOG.endTrace("convert", "Finalizado", "Request BCV = " + req);
			return req;
		
		} catch(NullPointerException e) {
			LOG.traceInfo("generarTrxRecaudacionBoRow", "NullPointerException" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionBoRow, NullPointerException: " + e.getMessage());
		} catch(Exception e) {
			LOG.traceInfo("generarTrxRecaudacionBoRow", "Exception" + e.getMessage());
			throw new AligareException("generarTrxRecaudacionBoRow, Exception: " + e.getMessage());
		}
	}
}
