package com.ripley.restservices.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ripley.restservices.model.bigticket.LgbtckFunInaDbo;
import com.ripley.restservices.model.bigticket.ObjectBT;
import com.ripley.restservices.model.bigticket.PrepareCallBoletaBigTicketDTO;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.TramaDTE;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

@Component
public class PrepareCallBoletaBigTicketDTOToObjectBTConverter implements Converter<PrepareCallBoletaBigTicketDTO, ObjectBT> {

    private static final AriLog LOG = new AriLog(PrepareCallBoletaBigTicketDTOToObjectBTConverter.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

    @Override
    public ObjectBT convert(PrepareCallBoletaBigTicketDTO s) {
        LOG.initTrace("convert", Constantes.VACIO);
        try {
            TramaDTE inOrdenCompra = s.getInOrdenCompra();
            Long nroTransaccion = s.getNroTransaccion();
            ArticuloVentaTramaDTE art = s.getArt();
            LocalDateTime ldt = s.getFechaBoleta();

            ObjectBT objectBT = new ObjectBT();

            DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern(Constantes.FECHA_FORMATO_YYYY_MM_DD);

            String formatoFecha1 = dtf1.format(ldt);
            String fechaDespacho = dtf1.format(art.getDespacho().getFechaDespacho().toLocalDateTime());

            LgbtckFunInaDbo objectoBT = new LgbtckFunInaDbo();
            objectoBT.setPcodTrx(Long.valueOf(Constantes.NRO_UNO));
            objectoBT.setPnroBoleta(nroTransaccion);
            objectoBT.setPfechaVta(formatoFecha1);
            objectoBT.setPsucursalVta(inOrdenCompra.getNotaVenta().getNumeroSucursal());
            objectoBT.setPestado(Constantes.NRO_CERO);
            String rut = String.valueOf(art.getDespacho().getRutCliente() == null ? Constantes.NRO_CERO : art.getDespacho().getRutCliente());
            String rutDespacho = String.valueOf(art.getDespacho().getRutDespacho() == null ? Constantes.NRO_CERO : art.getDespacho().getRutDespacho());
            objectoBT.setPrutCliente(rut);
            objectoBT.setPrutDesp(rutDespacho);
            objectoBT.setPnombreDesp(art.getDespacho().getNombreDespacho());
            objectoBT.setPfechaDesp(fechaDespacho);
            objectoBT.setPnroLocBod(Long.valueOf(art.getArticuloVenta().getBodegaStock()));

            String cud = Util.getCud(art);

            objectoBT.setPcud(cud);
            objectoBT.setPrutVendedor(inOrdenCompra.getVendedor().getRutVendedor() == null ? String.valueOf(Constantes.NRO_CERO) : inOrdenCompra.getVendedor().getRutVendedor());
            objectoBT.setPcodigoVta(Long.valueOf(art.getArticuloVenta().getCodArticulo()));
            objectoBT.setPcodColor(String.valueOf(art.getArticuloVenta().getColor() == null ? Constantes.NRO_CERO : art.getArticuloVenta().getColor()));
            objectoBT.setPcodComuna(Integer.toString(art.getDespacho().getComunaDespacho()));
            objectoBT.setPcodRegion(Integer.toString(art.getDespacho().getRegionDespacho()));
            objectoBT.setPcodRegalo(inOrdenCompra.getDespacho().getCodigoRegalo() == null ? Long.valueOf(Constantes.NRO_CERO) : Long.valueOf(inOrdenCompra.getDespacho().getCodigoRegalo()));
            objectoBT.setPdireccionDesp(art.getDespacho().getDireccionDespacho());
            objectoBT.setPjornadaDesp(art.getDespacho().getJornadaDespacho());
            objectoBT.setPobservaciones(art.getDespacho().getObservacion());
            objectoBT.setPprecio(art.getArticuloVenta().getPrecio());
            objectoBT.setPcantidad(art.getArticuloVenta().getUnidades());
            objectoBT.setPcubicaje(Constantes.NRO_UNO);
            objectoBT.setPctcPage(Constantes.NRO_UNO);
            objectoBT.setPctcCoordinate(Constantes.PCTC_COORDINATE);
            objectoBT.setPtelefono(art.getDespacho().getTelefonoDespacho());
            objectoBT.setPmontoVta(Util.getSubtotal(inOrdenCompra, Constantes.NRO_CERO));
            objectoBT.setPnombreCliente(((art.getDespacho().getNombreCliente() != null ? art.getDespacho().getNombreCliente() : Constantes.VACIO).trim() + Constantes.ESPACIO
                    + (art.getDespacho().getApellidoPatCliente() != null ? art.getDespacho().getApellidoPatCliente() : Constantes.VACIO).trim() + Constantes.ESPACIO
                    + (art.getDespacho().getApellidoMatCliente() != null ? art.getDespacho().getApellidoMatCliente() : Constantes.VACIO).trim()).trim());

            objectoBT.setPdireccionCliente(art.getDespacho().getDireccionCliente());
            objectoBT.setPtelefonoCliente(art.getDespacho().getTelefonoCliente());
            objectoBT.setPtipoCliente(Constantes.NRO_CERO);
            objectoBT.setPorigenTrx(String.valueOf(Constantes.NRO_TRES));
            objectoBT.setPtipoVta(Constantes.NRO_TRES);
            objectoBT.setPemail(art.getDespacho().getEmailCliente());
            objectoBT.setWitipRgl(inOrdenCompra.getDespacho().getCodigoRegalo() != null
                    && inOrdenCompra.getDespacho().getCodigoRegalo().intValue() == Constantes.NRO_CERO
                    && inOrdenCompra.getDespacho().getMensajeTarjeta() != null
                    && !inOrdenCompra.getDespacho().getMensajeTarjeta().trim().isEmpty()
                    ? Constantes.NRO_NOVENTANUEVE : (inOrdenCompra.getDespacho().getCodigoRegalo() == null
                    ? Constantes.NRO_CERO : inOrdenCompra.getDespacho().getCodigoRegalo()));
            objectoBT.setpCaja(inOrdenCompra.getNotaVenta().getNumeroCaja());
            objectoBT.setPcomercio(Constantes.NRO_UNO);
            objectoBT.setPversionPgm(Constantes.PVERSION_PMG);
            objectoBT.setPordcmp(inOrdenCompra.getNotaVenta().getCorrelativoVenta());
            objectoBT.setPmedioPago(Constantes.VACIO);
            objectoBT.setPvalmediopago(String.valueOf(Constantes.NRO_CERO));
            objectoBT.setPfchvalmediopago(formatoFecha1);
            objectoBT.setPfchcreaoc(formatoFecha1);
            objectoBT.setPtipoDocumento(Constantes.NRO_UNO);
            objectoBT.setPemailcliente(art.getDespacho().getEmailCliente());
            objectoBT.setPrznsocial(art.getDespacho().getNombreDespacho());
            objectoBT.setPmsjregalo(art.getDespacho().getMensajeTarjeta());
            objectoBT.setPobservaciones3(art.getDespacho().getObservacion());
            objectoBT.setPobservaciones4(art.getDespacho().getObservacion());

            objectBT.setObjectoBT(objectoBT);
            LOG.endTrace("convert", "Finaliza", "objectoBT");

            return objectBT;
        } catch (NullPointerException e) {
            LOG.traceError("convert", "NullPointerException", e);
            throw new AligareException("convert, NullPointerException: " + e.getMessage());
        } catch (Exception e) {
            LOG.traceError("convert", "Exception", e);
            throw new AligareException("convert, Exception: " + e.getMessage());
        }
    }

}
