package com.ripley.restservices.logic;

import java.io.InputStream;

import com.ripley.restservices.model.generic.GenericResponse;

import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Interface de manejo codigos SUNAT
 *
 * @author Martin Corrales (Aligare).
 * @since 08-01-2019
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public interface ISunatService {

	/**Carga de tracking
	 *
	 * @param load archivo snt que contiene sublinea y codigo sunat
	 * @return
	 * @throws RestServiceTransactionException
	 *
	 * @author  Martin Corrales (Aligare).
	 * @since 08-01-2019
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 */
	GenericResponse uploadSunatFile(InputStream is) ;
	
}
