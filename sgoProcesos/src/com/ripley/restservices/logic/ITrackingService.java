package com.ripley.restservices.logic;

import java.io.InputStream;
import java.util.List;

import com.ripley.restservices.model.generic.GenericResponse;

import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**
 * Interface de manejo de Tracking
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 * <br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
// Se extrae funcionalidad "tracking" de SGO Rest Services y se define en trackingJob como parte de mejoras al proceso de sincronización de estados de OCs entre BT y ME.
@Deprecated
public interface ITrackingService {

    /**
     * Carga de tracking
     *
     * @param load XML con la data que se inserta en el Modelo Extendido
     * @return
     * @throws RestServiceTransactionException
     *
     * @author Jose Matias Ortuzar (Aligare).
     * @since 02-05-2018
     * <br/><br/>
     * Cambios:<br/>
     * <ul>
     * <li>Inicio</li>
     * </ul>
     */
    @Deprecated
    GenericResponse trackingLoad(String load) throws RestServiceTransactionException;

    @Deprecated
    GenericResponse trackingLoad(InputStream is, List<String> fallidos);

}
