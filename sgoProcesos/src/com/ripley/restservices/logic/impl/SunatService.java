package com.ripley.restservices.logic.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ripley.restservices.logic.ISunatService;
import com.ripley.restservices.logic.ITrackingService;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.dao.SunatDAO;
import cl.ripley.omnicanalidad.util.Constantes;

/**Implementación de {@linkplain ITrackingService}
 *
 * @author Martin Corrales (Aligare).
 * @since 08-01-2019
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class SunatService implements ISunatService {
	
	private static final AriLog LOG = new AriLog(SunatService.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private SunatDAO regulatorioSunatDAO;
	
	@Override
	public GenericResponse uploadSunatFile(InputStream is) {
		LOG.initTrace("uploadSunatFile", "Inicio");
		HashMap <String, String> codigosSunat  = new HashMap<String, String>();
		GenericResponse resp = new GenericResponse();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(is));
			String linea;
			int nLinea = 0;
			while ((linea = in.readLine()) != null) {
				linea = linea.trim();
				if(linea.length() == Constantes.NRO_QUINCE) {
					codigosSunat.put(linea.substring(Constantes.NRO_CERO, Constantes.NRO_SIETE), linea.substring(Constantes.NRO_SIETE, Constantes.NRO_QUINCE));
				} else {
					LOG.traceInfo("uploadSunatFile", "Linea no cumple las especificaciones, nLinea=" + nLinea + " linea=" + linea);
				}
				nLinea++;
			}
			in.close();
			if(codigosSunat != null && !codigosSunat.isEmpty()) {
				boolean resultado = regulatorioSunatDAO.insertCodigosSunat(codigosSunat);
				if(resultado) {
					resp.setCodigo(Constantes.NRO_CERO);
					resp.setMensaje(Constantes.OK);
				} else {
					resp.setCodigo(Constantes.NRO_MENOSUNO);
					resp.setMensaje("No se realizo la insercion de los codigos en la tabla CV_CODIGOS_SUNAT.");
				}
			}else {
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje("No hubo respuesta exitosa.");
			}
		}catch(AligareException e) {
			LOG.traceInfo("uploadSunatFile", "AligareException: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} catch (IOException e) {
			LOG.traceInfo("uploadSunatFile", "I/O Error: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} catch (Exception e) {
			LOG.traceInfo("uploadSunatFile", "Exception: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} 
		LOG.endTrace("uploadSunatFile", "Finalizado", "uploadSunatFile = " + String.valueOf(resp));
		return resp;
	}

}
