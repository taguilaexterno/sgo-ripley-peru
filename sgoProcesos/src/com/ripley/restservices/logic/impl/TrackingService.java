package com.ripley.restservices.logic.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ripley.dao.ITrackingDAO;
import com.ripley.dao.dto.ArticuloDTO;
import com.ripley.dao.dto.TrackingLoadResponse;
import com.ripley.restservices.logic.ITrackingService;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

/**Implementación de {@linkplain ITrackingService}
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 02-05-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Service
public class TrackingService implements ITrackingService {
	
	private static final AriLog LOG = new AriLog(TrackingService.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private ITrackingDAO trackingDAO;
	
	@Override
	@Transactional(rollbackFor = RestServiceTransactionException.class)
	public GenericResponse trackingLoad(String load) throws RestServiceTransactionException {
		LOG.initTrace("trackingLoad", "String load", new KeyLog("Load", String.valueOf(load)));
		
		GenericResponse resp = new GenericResponse();
		
		TrackingLoadResponse tr = null;
		
		try {
			
			tr = trackingDAO.trackingLoad(load);
			
		} catch (DataAccessException e) {
			
			throw new RestServiceTransactionException(e.getMessage(), Constantes.NRO_MENOSUNO, e);
			
		}
		
		if(tr == null) {
			
			LOG.traceInfo("trackingLoad", "tr = " + String.valueOf(tr));
			throw new RestServiceTransactionException("No hubo respuesta exitosa.", Constantes.NRO_MENOSUNO);
			
		}
		
		if(tr.getCodigo().intValue() != Constantes.NRO_UNO) {
			
			LOG.traceInfo("trackingLoad", "tr = " + String.valueOf(tr));
			throw new RestServiceTransactionException(tr.getMensaje(), tr.getCodigo());
			
		}
		
		LOG.traceInfo("trackingLoad", "tr = " + String.valueOf(tr));
		
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje(Constantes.OK);
		resp.setData(String.valueOf(tr));
		
		LOG.endTrace("trackingLoad", "Finalizado", "TrackingLoadResponse = " + String.valueOf(resp));
		return resp;
	}

	@Override
	public GenericResponse trackingLoad(InputStream is, List<String> fallidos) {
		LOG.initTrace("trackingLoad", "Inicio");
		GenericResponse resp = new GenericResponse();
		ArticuloDTO articulo=new ArticuloDTO ();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		int result=0;
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(is));
			int contador = 0;
			String linea =  in.readLine();
            while(linea != null){
            	if(contador>0) {
	                String[] values = linea.split(";");
	                contador++;
	                try {
	                	articulo.setCud(values[0].trim());
	                    articulo.setFechaCambio(new java.sql.Date(formato.parse(values[1]).getTime()));
	                    articulo.setCodEstado(values[2].trim());
	                    articulo.setDescripcionEstado(values[3].trim());
	                    articulo.setOrdCompra(values[4].trim());
	                    result=trackingDAO.registrarTracking(articulo);
	                    if(result==-1) {
	                    	fallidos.add(linea);
	                    }
	                    linea = in.readLine();
	                    
	                }catch(SQLException e) {
	                	fallidos.add(linea);
	                	linea = in.readLine();
	                }
            	}else {
            		contador++;
            		linea = in.readLine();
            	}
			}
			in.close();

			resp.setCodigo(Constantes.NRO_CERO);
			resp.setMensaje(Constantes.OK);

		}catch(AligareException e) {
			LOG.traceInfo("trackingLoad", "AligareException: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} catch (IOException e) {
			LOG.traceInfo("trackingLoad", "I/O Error: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} catch (Exception e) {
			LOG.traceInfo("trackingLoad", "Exception: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("No hubo respuesta exitosa.");
		} 
		LOG.endTrace("trackingLoad", "Finalizado", "trackingLoad = " + String.valueOf(resp));
		return resp;
	}	

}
