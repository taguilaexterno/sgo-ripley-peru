package com.ripley.restservices.model.anulacion;

import com.ripley.restservices.model.generic.GenericRequest;

/**Modelo de request para obtener OCs para impresión.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 19-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class ConsultaOcAnulacionRequest extends GenericRequest {

	private Integer cantidadOCs;

	public Integer getCantidadOCs() {
		return cantidadOCs;
	}

	public void setCantidadOCs(Integer cantidadOCs) {
		this.cantidadOCs = cantidadOCs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaOcImpresionRequest [cantidadOCs=").append(cantidadOCs).append(", getOc()=")
				.append(getOc()).append(", getNumCaja()=").append(getNumCaja()).append("]");
		return builder.toString();
	}
	
	
	
}
