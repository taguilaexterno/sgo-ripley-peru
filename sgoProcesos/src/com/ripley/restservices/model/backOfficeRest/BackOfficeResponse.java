package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BackOfficeResponse {

	@JsonProperty("o_cod_rpt")
	private Integer codigo;
	@JsonProperty("o_msg_rpt")
	private String mensaje;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BackOfficeResponse [codigo=").append(codigo).append(", mensaje=").append(mensaje).append("]");
		return builder.toString();
	}
	
	
	
}
