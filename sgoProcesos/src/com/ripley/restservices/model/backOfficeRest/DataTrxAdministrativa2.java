package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxAdministrativa2 extends Data {
	
	@JsonProperty("TRX_ADMINISTRATIVA2")
	private TrxAdministrativa2 trxAdministrativa2;
	
	
	public DataTrxAdministrativa2() {
		setTable("TRX_ADMINISTRATIVA2");
	}

	public TrxAdministrativa2 getTrxAdministrativa2() {
		return trxAdministrativa2;
	}

	public void setTrxAdministrativa2(TrxAdministrativa2 trxAdministrativa2) {
		this.trxAdministrativa2 = trxAdministrativa2;
	}

	@Override
	public String toString() {
		return "DataTrxAdministrativa2 [trxAdministrativa2=" + trxAdministrativa2 + ", getTrxAdministrativa2()="
				+ getTrxAdministrativa2() + ", getTable()=" + getTable() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	
}
