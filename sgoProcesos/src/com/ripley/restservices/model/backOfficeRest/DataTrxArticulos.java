package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxArticulos extends Data {
	
	@JsonProperty("TRX_ARTICULOS")
	private TrxArticulos trxArticulos;
	
	
	public DataTrxArticulos() {
		setTable("TRX_ARTICULOS");
	}


	public TrxArticulos getTrxArticulos() {
		return trxArticulos;
	}


	public void setTrxArticulos(TrxArticulos trxArticulos) {
		this.trxArticulos = trxArticulos;
	}


	@Override
	public String toString() {
		return "DataTrxArticulos [trxArticulos=" + trxArticulos + ", getTrxArticulos()=" + getTrxArticulos()
				+ ", getTable()=" + getTable() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	

}
