package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxBancaria extends Data {

	@JsonProperty("TRX_TAR_BANCARIA")
	private TrxTarBancaria trxTarBancaria;
	
	public DataTrxBancaria() {
		setTable("TRX_TAR_BANCARIA");
	}

	public TrxTarBancaria getTrxTarBancaria() {
		return trxTarBancaria;
	}

	public void setTrxTarBancaria(TrxTarBancaria trxTarBancaria) {
		this.trxTarBancaria = trxTarBancaria;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataTrxBancaria [trxTarBancaria=").append(trxTarBancaria).append("]");
		return builder.toString();
	}
	
	
}
