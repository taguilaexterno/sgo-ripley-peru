package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxDespacho extends Data {

	@JsonProperty("TRX_DESPACHO")
	private TrxDespacho trxDespacho;
	
	
	public DataTrxDespacho() {
		setTable("TRX_DESPACHO");
	}


	public TrxDespacho getTrxDespacho() {
		return trxDespacho;
	}


	public void setTrxDespacho(TrxDespacho trxDespacho) {
		this.trxDespacho = trxDespacho;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataTrxDespacho [trxDespacho=").append(trxDespacho).append("]");
		return builder.toString();
	}
	
	
}
