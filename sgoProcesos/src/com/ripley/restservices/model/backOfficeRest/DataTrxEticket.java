package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxEticket extends Data {
	
	@JsonProperty("TRX_ETICKET")
	private TrxEticket trxEticket;
	
	
	public DataTrxEticket() {
		setTable("TRX_ETICKET");
	}


	public TrxEticket getTrxEticket() {
		return trxEticket;
	}


	public void setTrxEticket(TrxEticket trxEticket) {
		this.trxEticket = trxEticket;
	}


	@Override
	public String toString() {
		return "DataTrxEticket [trxEticket=" + trxEticket + ", getTrxEticket()=" + getTrxEticket() + ", getTable()="
				+ getTable() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}
	
	
}
