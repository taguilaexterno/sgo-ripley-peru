package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxMultipagosBo extends Data {
	
	@JsonProperty("TRX_MULTIPAGOS_BO")
	private TrxMultipagosBo trxMultipagosBo;
	
	
	public DataTrxMultipagosBo() {
		setTable("TRX_MULTIPAGOS_BO");
	}


	public TrxMultipagosBo getTrxMultipagosBo() {
		return trxMultipagosBo;
	}


	public void setTrxMultipagosBo(TrxMultipagosBo trxMultipagosBo) {
		this.trxMultipagosBo = trxMultipagosBo;
	}


	@Override
	public String toString() {
		return "DataTrxMultipagosBo [trxMultipagosBo=" + trxMultipagosBo + ", getTrxMultipagosBo()="
				+ getTrxMultipagosBo() + ", getTable()=" + getTable() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	
}
