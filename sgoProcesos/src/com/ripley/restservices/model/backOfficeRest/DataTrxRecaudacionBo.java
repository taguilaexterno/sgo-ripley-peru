package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxRecaudacionBo extends Data{
	
	@JsonProperty("TRX_RECAUDACION_BO")
	private TrxRecaudacionBo trxRecaudacionBo;
	
	
	public DataTrxRecaudacionBo() {
		setTable("TRX_RECAUDACION_BO");
	}


	public TrxRecaudacionBo getTrxRecaudacionBo() {
		return trxRecaudacionBo;
	}


	public void setTrxRecaudacionBo(TrxRecaudacionBo trxRecaudacionBo) {
		this.trxRecaudacionBo = trxRecaudacionBo;
	}


	@Override
	public String toString() {
		return "DataTrxRecaudacionBo [trxRecaudacionBo=" + trxRecaudacionBo + ", getTrxRecaudacionBo()="
				+ getTrxRecaudacionBo() + ", getTable()=" + getTable() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}


	
}
