package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxRecaudacionPrvBo extends Data {

	@JsonProperty("TRX_RECAUDACION_PRV_BO")
	private TrxRecaudacionPrvBo trxRecaudacionPrvBo;
	
	public DataTrxRecaudacionPrvBo() {
		setTable("TRX_RECAUDACION_PRV_BO");
	}

	public TrxRecaudacionPrvBo getTrxRecaudacionPrvBo() {
		return trxRecaudacionPrvBo;
	}

	public void setTrxRecaudacionPrvBo(TrxRecaudacionPrvBo trxRecaudacionPrvBo) {
		this.trxRecaudacionPrvBo = trxRecaudacionPrvBo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataTrxRecaudacionPrvBo [trxRecaudacionPrvBo=").append(trxRecaudacionPrvBo).append("]");
		return builder.toString();
	}
	
	
}
