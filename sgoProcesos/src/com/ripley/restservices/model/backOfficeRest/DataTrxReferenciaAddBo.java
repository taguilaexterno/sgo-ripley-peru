package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxReferenciaAddBo extends Data {
	
	@JsonProperty("TRX_REFERENCIA_ADD_BO")
	private TrxReferenciaAddBo trxReferenciaAddBo;
	
	
	public DataTrxReferenciaAddBo() {
		setTable("TRX_REFERENCIA_ADD_BO");
	}


	public TrxReferenciaAddBo getTrxReferenciaAddBo() {
		return trxReferenciaAddBo;
	}


	public void setTrxReferenciaAddBo(TrxReferenciaAddBo trxReferenciaAddBo) {
		this.trxReferenciaAddBo = trxReferenciaAddBo;
	}


	@Override
	public String toString() {
		return "DataTrxReferenciaAddBo [trxReferenciaAddBo=" + trxReferenciaAddBo + ", getTrxReferenciaAddBo()="
				+ getTrxReferenciaAddBo() + ", getTable()=" + getTable() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
		
}
