package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxTarRipley extends Data {

	@JsonProperty("TRX_TAR_RIPLEY")
	private TrxTarRipley trxTarRipley;
	
	public DataTrxTarRipley() {
		setTable("TRX_TAR_RIPLEY");
	}

	public TrxTarRipley getTrxTarRipley() {
		return trxTarRipley;
	}

	public void setTrxTarRipley(TrxTarRipley trxTarRipley) {
		this.trxTarRipley = trxTarRipley;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataTrxTarRipley [trxTarRipley=").append(trxTarRipley).append("]");
		return builder.toString();
	}
	
	
}
