package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataTrxTransaccionAdd extends Data {
	
	@JsonProperty("TRX_TRANSACCION_ADD")
	private TrxTransaccionAdd trxTransaccionAdd;
	
	public DataTrxTransaccionAdd() {
		setTable("TRX_TRANSACCION_ADD");
	}

	public TrxTransaccionAdd getTrxTransaccionAdd() {
		return trxTransaccionAdd;
	}

	public void setTrxTransaccionAdd(TrxTransaccionAdd trxTransaccionAdd) {
		this.trxTransaccionAdd = trxTransaccionAdd;
	}

	@Override
	public String toString() {
		return "DataTrxTransaccionAdd [trxTransaccionAdd=" + trxTransaccionAdd + ", getTrxTransaccionAdd()="
				+ getTrxTransaccionAdd() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	
	

}
