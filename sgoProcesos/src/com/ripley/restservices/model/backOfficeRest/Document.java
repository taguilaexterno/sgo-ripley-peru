package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import cl.ripley.omnicanalidad.util.Constantes;

public class Document {

	@JsonProperty(value = "PRIMARY_KEY", required = true)
	private PrimaryKey primaryKey;
	@JsonProperty(value = "HEADER", required = true)
	private Header header;
	@JsonProperty(value = "DATA", required = true)
	private List<Data> data;
	
	public PrimaryKey getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(PrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
	}
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public List<Data> getData() {
		if(data == null) {
			data = new ArrayList<>(Constantes.NRO_VEINTE);
		}
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Document [primaryKey=").append(primaryKey).append(", header=").append(header).append(", data=")
				.append(data).append("]");
		return builder.toString();
	}
	
	
	
	
}
