package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Header {

	@JsonProperty("TIPO_TRX")
	private Integer tipoTrx;
	@JsonProperty("NRO_DOCUMENTO")
	private Long nroDocumento;
	
	/**
	 * formato YYYY-MM-DD HH24:MI:SS
	 */
	@JsonProperty("FECHA_HORA_TRX")
	private String fechaHoraTrx;
	@JsonProperty("SUPERVISOR")
	private Long supervisor;
	@JsonProperty("VENDEDOR")
	private Long vendedor;
	@JsonProperty("MONTO_TRX")
	private BigDecimal montoTrx;
	@JsonProperty("SUC_ORIGINAL")
	private Integer sucursalOriginal;
	
	/**
	 * fecha recaudacion original formato YYYY-MM-DD
	 */
	@JsonProperty("FECHA_ORIGINAL")
	private String fechaOriginal;
	@JsonProperty("NRO_CAJA_ORIGINAL")
	private Integer nroCajaOriginal;
	@JsonProperty("NRO_DOCTO_ORIGINAL")
	private Long nroDocumentoOriginal;
	@JsonProperty("VENDEDOR_ORIGINAL")
	private Long vendedorOriginal;
	@JsonProperty("ORIGEN_TRX")
	private Integer origenTrx;
	@JsonProperty("RUT_COMPRA_PAGA")
	private Long rutCompraPaga;
	@JsonProperty("DESCUENTO_ECCSA")
	private BigDecimal descuentoEccsa;
	@JsonProperty("DESCUENTO_CAR")
	private BigDecimal descuentoCar;
	@JsonProperty("MONTO_PIE")
	private BigDecimal montoPie;
	@JsonProperty("ESTADO")
	private Integer estado;
	@JsonProperty("TIPO_ORIGEN")
	private Integer tipoOrigen;
	@JsonProperty("TXTRN_RUN_CAJ_CRO")
	private Integer txtrnRunCajCro;
	@JsonProperty("TXTRN_RUN_CAJ_CRO_ORI")
	private Integer txtrnRunCajCroOri;
	@JsonProperty("TXTRN_MNT_DNN")
	private BigDecimal txtrnMntDnn;
	@JsonProperty("TRX_TIPO_DOC")
	private Integer trxTipoDoc;
	@JsonProperty("TRX_TAR_BONUS")
	private Integer trxTarBonus;
	@JsonProperty("TRX_RUC")
	private Long trxRuc;
	@JsonProperty("TRX_MOT_NC")
	private Integer trxMotNc;
	@JsonProperty("TRX_TELEFONO")
	private Integer trxTelefono;
	@JsonProperty("TRX_DOC_IDENTIDAD")
	private Integer trxDocIdentidad;
	@JsonProperty("TRX_TIPO_DOC_IDENTIDAD")
	private Integer trxTipoDocIdentidad;
	@JsonProperty("NRO_CELULAR")
	private Integer nroCelular;
	@JsonProperty("NRO_TERMINAL")
	private String nroTerminal;
	@JsonProperty("NRO_ORDEN_COMPRA")
	private Long nroOrdenCompra;
	public Integer getTipoTrx() {
		return tipoTrx;
	}
	public void setTipoTrx(Integer tipoTrx) {
		this.tipoTrx = tipoTrx;
	}
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getFechaHoraTrx() {
		return fechaHoraTrx;
	}
	public void setFechaHoraTrx(String fechaHoraTrx) {
		this.fechaHoraTrx = fechaHoraTrx;
	}
	public Long getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(Long supervisor) {
		this.supervisor = supervisor;
	}
	public Long getVendedor() {
		return vendedor;
	}
	public void setVendedor(Long vendedor) {
		this.vendedor = vendedor;
	}
	public BigDecimal getMontoTrx() {
		return montoTrx;
	}
	public void setMontoTrx(BigDecimal montoTrx) {
		this.montoTrx = montoTrx;
	}
	public Integer getSucursalOriginal() {
		return sucursalOriginal;
	}
	public void setSucursalOriginal(Integer sucursalOriginal) {
		this.sucursalOriginal = sucursalOriginal;
	}
	public String getFechaOriginal() {
		return fechaOriginal;
	}
	public void setFechaOriginal(String fechaOriginal) {
		this.fechaOriginal = fechaOriginal;
	}
	public Integer getNroCajaOriginal() {
		return nroCajaOriginal;
	}
	public void setNroCajaOriginal(Integer nroCajaOriginal) {
		this.nroCajaOriginal = nroCajaOriginal;
	}
	public Long getNroDocumentoOriginal() {
		return nroDocumentoOriginal;
	}
	public void setNroDocumentoOriginal(Long nroDocumentoOriginal) {
		this.nroDocumentoOriginal = nroDocumentoOriginal;
	}
	public Long getVendedorOriginal() {
		return vendedorOriginal;
	}
	public void setVendedorOriginal(Long vendedorOriginal) {
		this.vendedorOriginal = vendedorOriginal;
	}
	public Integer getOrigenTrx() {
		return origenTrx;
	}
	public void setOrigenTrx(Integer origenTrx) {
		this.origenTrx = origenTrx;
	}
	public Long getRutCompraPaga() {
		return rutCompraPaga;
	}
	public void setRutCompraPaga(Long rutCompraPaga) {
		this.rutCompraPaga = rutCompraPaga;
	}
	public BigDecimal getDescuentoEccsa() {
		return descuentoEccsa;
	}
	public void setDescuentoEccsa(BigDecimal descuentoEccsa) {
		this.descuentoEccsa = descuentoEccsa;
	}
	public BigDecimal getDescuentoCar() {
		return descuentoCar;
	}
	public void setDescuentoCar(BigDecimal descuentoCar) {
		this.descuentoCar = descuentoCar;
	}
	public BigDecimal getMontoPie() {
		return montoPie;
	}
	public void setMontoPie(BigDecimal montoPie) {
		this.montoPie = montoPie;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Integer getTipoOrigen() {
		return tipoOrigen;
	}
	public void setTipoOrigen(Integer tipoOrigen) {
		this.tipoOrigen = tipoOrigen;
	}
	public Integer getTxtrnRunCajCro() {
		return txtrnRunCajCro;
	}
	public void setTxtrnRunCajCro(Integer txtrnRunCajCro) {
		this.txtrnRunCajCro = txtrnRunCajCro;
	}
	public Integer getTxtrnRunCajCroOri() {
		return txtrnRunCajCroOri;
	}
	public void setTxtrnRunCajCroOri(Integer txtrnRunCajCroOri) {
		this.txtrnRunCajCroOri = txtrnRunCajCroOri;
	}
	public BigDecimal getTxtrnMntDnn() {
		return txtrnMntDnn;
	}
	public void setTxtrnMntDnn(BigDecimal txtrnMntDnn) {
		this.txtrnMntDnn = txtrnMntDnn;
	}
	public Integer getTrxTipoDoc() {
		return trxTipoDoc;
	}
	public void setTrxTipoDoc(Integer trxTipoDoc) {
		this.trxTipoDoc = trxTipoDoc;
	}
	public Integer getTrxTarBonus() {
		return trxTarBonus;
	}
	public void setTrxTarBonus(Integer trxTarBonus) {
		this.trxTarBonus = trxTarBonus;
	}
	public Long getTrxRuc() {
		return trxRuc;
	}
	public void setTrxRuc(Long trxRuc) {
		this.trxRuc = trxRuc;
	}
	public Integer getTrxMotNc() {
		return trxMotNc;
	}
	public void setTrxMotNc(Integer trxMotNc) {
		this.trxMotNc = trxMotNc;
	}
	public Integer getTrxTelefono() {
		return trxTelefono;
	}
	public void setTrxTelefono(Integer trxTelefono) {
		this.trxTelefono = trxTelefono;
	}
	public Integer getTrxDocIdentidad() {
		return trxDocIdentidad;
	}
	public void setTrxDocIdentidad(Integer trxDocIdentidad) {
		this.trxDocIdentidad = trxDocIdentidad;
	}
	public Integer getTrxTipoDocIdentidad() {
		return trxTipoDocIdentidad;
	}
	public void setTrxTipoDocIdentidad(Integer trxTipoDocIdentidad) {
		this.trxTipoDocIdentidad = trxTipoDocIdentidad;
	}
	public Integer getNroCelular() {
		return nroCelular;
	}
	public void setNroCelular(Integer nroCelular) {
		this.nroCelular = nroCelular;
	}
	public String getNroTerminal() {
		return nroTerminal;
	}
	public void setNroTerminal(String nroTerminal) {
		this.nroTerminal = nroTerminal;
	}
	public Long getNroOrdenCompra() {
		return nroOrdenCompra;
	}
	public void setNroOrdenCompra(Long nroOrdenCompra) {
		this.nroOrdenCompra = nroOrdenCompra;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTransaccionRow [tipoTrx=").append(tipoTrx).append(", nroDocumento=").append(nroDocumento)
				.append(", fechaHoraTrx=").append(fechaHoraTrx).append(", supervisor=").append(supervisor)
				.append(", vendedor=").append(vendedor).append(", montoTrx=").append(montoTrx)
				.append(", sucursalOriginal=").append(sucursalOriginal).append(", fechaOriginal=").append(fechaOriginal)
				.append(", nroCajaOriginal=").append(nroCajaOriginal).append(", nroDocumentoOriginal=")
				.append(nroDocumentoOriginal).append(", vendedorOriginal=").append(vendedorOriginal)
				.append(", origenTrx=").append(origenTrx).append(", rutCompraPaga=").append(rutCompraPaga)
				.append(", descuentoEccsa=").append(descuentoEccsa).append(", descuentoCar=").append(descuentoCar)
				.append(", montoPie=").append(montoPie).append(", estado=").append(estado).append(", tipoOrigen=")
				.append(tipoOrigen).append(", TxtrnRunCajCro=").append(txtrnRunCajCro).append(", TxtrnRunCajCroOri=")
				.append(txtrnRunCajCroOri).append(", TxtrnMntDnn=").append(txtrnMntDnn).append(", trxTipoDoc=")
				.append(trxTipoDoc).append(", trxTarBonus=").append(trxTarBonus).append(", trxRuc=").append(trxRuc)
				.append(", trxMotNc=").append(trxMotNc).append(", trxTelefono=").append(trxTelefono)
				.append(", trxDocIdentidad=").append(trxDocIdentidad).append(", trxTipoDocIdentidad=")
				.append(trxTipoDocIdentidad).append(", nroCelular=").append(nroCelular).append(", nroTerminal=")
				.append(nroTerminal).append(", nroOrdenCompra=").append(nroOrdenCompra).append("]");
		return builder.toString();
	}
	
	
	
}
