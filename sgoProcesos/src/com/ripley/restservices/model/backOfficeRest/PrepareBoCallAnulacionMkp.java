package com.ripley.restservices.model.backOfficeRest;

import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public class PrepareBoCallAnulacionMkp {

	private TramaDTE inOrdenCompra;
	private Parametros parametros;
	private Long nroTransaccion;
	private boolean esMkp;
	
	public TramaDTE getInOrdenCompra() {
		return inOrdenCompra;
	}
	public void setInOrdenCompra(TramaDTE inOrdenCompra) {
		this.inOrdenCompra = inOrdenCompra;
	}
	public Parametros getParametros() {
		return parametros;
	}
	public void setParametros(Parametros parametros) {
		this.parametros = parametros;
	}
	public Long getNroTransaccion() {
		return nroTransaccion;
	}
	public void setNroTransaccion(Long nroTransaccion) {
		this.nroTransaccion = nroTransaccion;
	}
	public boolean isEsMkp() {
		return esMkp;
	}
	public void setEsMkp(boolean esMkp) {
		this.esMkp = esMkp;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"inOrdenCompra\":\"").append(inOrdenCompra).append("\",\"parametros\":\"").append(parametros)
				.append("\",\"nroTransaccion\":\"").append(nroTransaccion).append("\",\"esMkp\":\"").append(esMkp)
				.append("\"}");
		return builder.toString();
	}
	
	
	
}
