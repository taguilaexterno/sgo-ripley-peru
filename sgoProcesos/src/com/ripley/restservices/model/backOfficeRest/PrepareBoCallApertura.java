package com.ripley.restservices.model.backOfficeRest;

import cl.ripley.omnicanalidad.bean.Parametros;

public class PrepareBoCallApertura {

	private Long trx;
	private Parametros pcv;
	private String fecha;
	private String fechaHora;
	
	public Long getTrx() {
		return trx;
	}
	public void setTrx(Long trx) {
		this.trx = trx;
	}
	public Parametros getPcv() {
		return pcv;
	}
	public void setPcv(Parametros pcv) {
		this.pcv = pcv;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"trx\":\"").append(trx).append("\",\"pcv\":\"").append(pcv).append("\",\"fecha\":\"")
				.append(fecha).append("\",\"fechaHora\":\"").append(fechaHora).append("\"}");
		return builder.toString();
	}
	
	
	
}
