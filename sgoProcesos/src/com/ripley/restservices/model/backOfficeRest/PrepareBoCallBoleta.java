package com.ripley.restservices.model.backOfficeRest;

import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public class PrepareBoCallBoleta {

	private TramaDTE inOrdenCompra;
	private Parametros parametros;
	private Long nroTransaccion;
	private Long nroTrxAnterior;
	private boolean esMkp;
	private String boleta;
	private boolean isRipleyProcessed;
	private boolean isMkpProcessed;
	
	public TramaDTE getInOrdenCompra() {
		return inOrdenCompra;
	}
	public void setInOrdenCompra(TramaDTE inOrdenCompra) {
		this.inOrdenCompra = inOrdenCompra;
	}
	public Parametros getParametros() {
		return parametros;
	}
	public void setParametros(Parametros parametros) {
		this.parametros = parametros;
	}
	public Long getNroTransaccion() {
		return nroTransaccion;
	}
	public void setNroTransaccion(Long nroTransaccion) {
		this.nroTransaccion = nroTransaccion;
	}
	public boolean isEsMkp() {
		return esMkp;
	}
	public void setEsMkp(boolean esMkp) {
		this.esMkp = esMkp;
	}
	public String getBoleta() {
		return boleta;
	}
	public void setBoleta(String boleta) {
		this.boleta = boleta;
	}
	public Long getNroTrxAnterior() {
		return nroTrxAnterior;
	}
	public void setNroTrxAnterior(Long nroTrxAnterior) {
		this.nroTrxAnterior = nroTrxAnterior;
	}
	public boolean isRipleyProcessed() {
		return isRipleyProcessed;
	}
	public void setRipleyProcessed(boolean isRipleyProcessed) {
		this.isRipleyProcessed = isRipleyProcessed;
	}
	public boolean isMkpProcessed() {
		return isMkpProcessed;
	}
	public void setMkpProcessed(boolean isMkpProcessed) {
		this.isMkpProcessed = isMkpProcessed;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"inOrdenCompra\":\"").append(inOrdenCompra).append("\",\"parametros\":\"").append(parametros)
				.append("\",\"nroTransaccion\":\"").append(nroTransaccion).append("\",\"nroTrxAnterior\":\"")
				.append(nroTrxAnterior).append("\",\"esMkp\":\"").append(esMkp).append("\",\"boleta\":\"")
				.append(boleta).append("\",\"isRipleyProcessed\":\"").append(isRipleyProcessed)
				.append("\",\"isMkpProcessed\":\"").append(isMkpProcessed).append("\"}");
		return builder.toString();
	}
	
	
}
