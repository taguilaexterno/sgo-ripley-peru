package com.ripley.restservices.model.backOfficeRest;

import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TotalesCierre;

public class PrepareBoCallCierre {

	private Long trx;
	private Parametros pcv;
	private String fecha;
	private String fechaHora;
	private TotalesCierre totalesCierre;
	
	public Long getTrx() {
		return trx;
	}
	public void setTrx(Long trx) {
		this.trx = trx;
	}
	public Parametros getPcv() {
		return pcv;
	}
	public void setPcv(Parametros pcv) {
		this.pcv = pcv;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	public TotalesCierre getTotalesCierre() {
		return totalesCierre;
	}
	public void setTotalesCierre(TotalesCierre totalesCierre) {
		this.totalesCierre = totalesCierre;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"trx\":\"").append(trx).append("\",\"pcv\":\"").append(pcv).append("\",\"fecha\":\"")
				.append(fecha).append("\",\"fechaHora\":\"").append(fechaHora).append("\",\"totalesCierre\":\"")
				.append(totalesCierre).append("\"}");
		return builder.toString();
	}
	
	
	
}
