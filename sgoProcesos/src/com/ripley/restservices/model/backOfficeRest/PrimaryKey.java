package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PrimaryKey {

	/**
	 * Fecha en formato YYYY-MM-DD
	 */
	@JsonProperty("FECHA_TRX")
	private String fechaTrx;
	@JsonProperty("SUCURSAL")
	private Integer sucursal;
	@JsonProperty("NRO_CAJA")
	private Integer nroCaja;
	@JsonProperty("NRO_TRANSACCION")
	private Long nroTransaccion;
	
	public String getFechaTrx() {
		return fechaTrx;
	}
	public void setFechaTrx(String fechaTrx) {
		this.fechaTrx = fechaTrx;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	public Integer getNroCaja() {
		return nroCaja;
	}
	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}
	public Long getNroTransaccion() {
		return nroTransaccion;
	}
	public void setNroTransaccion(Long nroTransaccion) {
		this.nroTransaccion = nroTransaccion;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrimaryKey [fechaTrx=").append(fechaTrx).append(", sucursal=").append(sucursal)
				.append(", nroCaja=").append(nroCaja).append(", nroTransaccion=").append(nroTransaccion).append("]");
		return builder.toString();
	}
	
	
}
