package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxAdministrativa2 {
	
	@JsonProperty("ROW")
	private List<TrxAdministrativa2Row> row;

	public List<TrxAdministrativa2Row> getRow() {
		if(row == null) {
			row = new ArrayList<TrxAdministrativa2Row>();
		}
		return row;
	}

	public void setRow(List<TrxAdministrativa2Row> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxAdministrativa2 [row=").append(row).append("]");
		return builder.toString();
	}
	
	

}
