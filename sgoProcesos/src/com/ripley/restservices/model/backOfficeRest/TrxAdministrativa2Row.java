package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cl.ripley.omnicanalidad.util.Constantes;

public class TrxAdministrativa2Row {
	
	@JsonProperty("TIPO_TRX")
	private Integer tipoTrx = Constantes.NRO_CERO;
	@JsonProperty("ORIGEN_TRX")
	private Integer origenTrx = Constantes.NRO_CERO;
	@JsonProperty("FECHA_HORA_TRX")
	private String fechaHoraTrx;
	@JsonProperty("SUPERVISOR")
	private Long supervisor;
	@JsonProperty("VENDEDOR")
	private Long vendedor;
	@JsonProperty("NRO_GAVETA")
	private Integer nroGaveta = Constantes.NRO_CERO;
	@JsonProperty("TOTAL_EFECTIVO")
	private Integer totalEfectivo = Constantes.NRO_CERO;
	@JsonProperty("CANT_CHEQUE_DIA")
	private Integer cantChequeDia = Constantes.NRO_CERO;
	@JsonProperty("TOTAL_CHEQUE_DIA")
	private BigDecimal totalChequeDia = BigDecimal.ZERO;
	@JsonProperty("CANT_CHEQUE_FECHA")
	private Integer cantChequeFecha = Constantes.NRO_CERO;
	@JsonProperty("TOTAL_CHEQUE_FECHA")
	private BigDecimal totalChequeFecha = BigDecimal.ZERO;
	@JsonProperty("TOTAL_MONEDA_EXTRANJERA")
	private BigDecimal totalMonedaExtranjera = BigDecimal.ZERO;
	@JsonProperty("FONDO_INICIAL")
	private BigDecimal fondoInicial = BigDecimal.ZERO;
	@JsonProperty("FONDO_FINAL")
	private BigDecimal fondoFinal = BigDecimal.ZERO;
	@JsonProperty("TOTAL_INGRESO_NETO")
	private BigDecimal totalIngresoNeto = BigDecimal.ZERO;
	@JsonProperty("TOTAL_VENTA_NETA")
	private BigDecimal totalVentaNeta = BigDecimal.ZERO;
	@JsonProperty("FONDO_ADICIONAL")
	private BigDecimal fondoAdicional = BigDecimal.ZERO;
	@JsonProperty("TOTAL_INGRESOS")
	private BigDecimal totalIngresos = BigDecimal.ZERO;
	@JsonProperty("TOTAL_ANULACIONES")
	private BigDecimal totalAnulaciones = BigDecimal.ZERO;
	@JsonProperty("TOTAL_EGRESOS")
	private BigDecimal totalEgresos = BigDecimal.ZERO;
	@JsonProperty("TOTAL_CARGOS")
	private BigDecimal totalCargos = BigDecimal.ZERO;
	@JsonProperty("TOTAL_NETOS")
	private BigDecimal totalNetos = BigDecimal.ZERO;
	@JsonProperty("TOTAL_RETIROS")
	private BigDecimal totalRetiros = BigDecimal.ZERO;
	@JsonProperty("TOT_PTS_VEND")
	private BigDecimal totPtsVend = BigDecimal.ZERO;
	@JsonProperty("TOT_PTS_CANJ")
	private BigDecimal totPtsCanj = BigDecimal.ZERO;
	@JsonProperty("TOTAL_CON_DESPACHO")
	private BigDecimal totalConDespacho = BigDecimal.ZERO;
	@JsonProperty("TOTAL_SIN_DESPACHO")
	private BigDecimal totalSinDespacho = BigDecimal.ZERO;
	@JsonProperty("TOTAL_VTA_NOTA_VENTA")
	private BigDecimal totalVtaNotaVenta = BigDecimal.ZERO;
	@JsonProperty("TOTAL_ING_NOTA_VENTA")
	private BigDecimal totalIngNotaVenta = BigDecimal.ZERO;
	@JsonProperty("TXADM_MTO_TOT_RGL_PEL")
	private BigDecimal txadmMtoTotRglPel = BigDecimal.ZERO;
	@JsonProperty("TXADM_MTO_ING_RGL_PEL")
	private BigDecimal txadmMtoIngRglPel = BigDecimal.ZERO;
	@JsonProperty("TXADM_RUN_CAJ_CRO")
	private BigDecimal txadmRunCajCro = BigDecimal.ZERO;
	@JsonProperty("TOTAL_RECAUDACION")
	private BigDecimal totalRecaudacion = BigDecimal.ZERO;
	@JsonProperty("TOTAL_REV_RECAUDACION")
	private BigDecimal totalRevRecaudacion = BigDecimal.ZERO;
	@JsonProperty("TC_COMPRA")
	private BigDecimal tcCompra = BigDecimal.ZERO;
	@JsonProperty("TC_VENTA")
	private BigDecimal tcVenta = BigDecimal.ZERO;
	@JsonProperty("TOTAL_REDONDEO")
	private BigDecimal totalRedondeo = BigDecimal.ZERO;
	
	public Integer getTipoTrx() {
		return tipoTrx;
	}
	public void setTipoTrx(Integer tipoTrx) {
		this.tipoTrx = tipoTrx;
	}
	public Integer getOrigenTrx() {
		return origenTrx;
	}
	public void setOrigenTrx(Integer origenTrx) {
		this.origenTrx = origenTrx;
	}
	public String getFechaHoraTrx() {
		return fechaHoraTrx;
	}
	public void setFechaHoraTrx(String fechaHoraTrx) {
		this.fechaHoraTrx = fechaHoraTrx;
	}
	public Long getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(Long supervisor) {
		this.supervisor = supervisor;
	}
	public Long getVendedor() {
		return vendedor;
	}
	public void setVendedor(Long vendedor) {
		this.vendedor = vendedor;
	}
	public Integer getNroGaveta() {
		return nroGaveta;
	}
	public void setNroGaveta(Integer nroGaveta) {
		this.nroGaveta = nroGaveta;
	}
	public Integer getTotalEfectivo() {
		return totalEfectivo;
	}
	public void setTotalEfectivo(Integer totalEfectivo) {
		this.totalEfectivo = totalEfectivo;
	}
	public Integer getCantChequeDia() {
		return cantChequeDia;
	}
	public void setCantChequeDia(Integer cantChequeDia) {
		this.cantChequeDia = cantChequeDia;
	}
	public BigDecimal getTotalChequeDia() {
		return totalChequeDia;
	}
	public void setTotalChequeDia(BigDecimal totalChequeDia) {
		this.totalChequeDia = totalChequeDia;
	}
	public Integer getCantChequeFecha() {
		return cantChequeFecha;
	}
	public void setCantChequeFecha(Integer cantChequeFecha) {
		this.cantChequeFecha = cantChequeFecha;
	}
	public BigDecimal getTotalChequeFecha() {
		return totalChequeFecha;
	}
	public void setTotalChequeFecha(BigDecimal totalChequeFecha) {
		this.totalChequeFecha = totalChequeFecha;
	}
	public BigDecimal getTotalMonedaExtranjera() {
		return totalMonedaExtranjera;
	}
	public void setTotalMonedaExtranjera(BigDecimal totalMonedaExtranjera) {
		this.totalMonedaExtranjera = totalMonedaExtranjera;
	}
	public BigDecimal getFondoInicial() {
		return fondoInicial;
	}
	public void setFondoInicial(BigDecimal fondoInicial) {
		this.fondoInicial = fondoInicial;
	}
	public BigDecimal getFondoFinal() {
		return fondoFinal;
	}
	public void setFondoFinal(BigDecimal fondoFinal) {
		this.fondoFinal = fondoFinal;
	}
	public BigDecimal getTotalIngresoNeto() {
		return totalIngresoNeto;
	}
	public void setTotalIngresoNeto(BigDecimal totalIngresoNeto) {
		this.totalIngresoNeto = totalIngresoNeto;
	}
	public BigDecimal getTotalVentaNeta() {
		return totalVentaNeta;
	}
	public void setTotalVentaNeta(BigDecimal totalVentaNeta) {
		this.totalVentaNeta = totalVentaNeta;
	}
	public BigDecimal getFondoAdicional() {
		return fondoAdicional;
	}
	public void setFondoAdicional(BigDecimal fondoAdicional) {
		this.fondoAdicional = fondoAdicional;
	}
	public BigDecimal getTotalIngresos() {
		return totalIngresos;
	}
	public void setTotalIngresos(BigDecimal totalIngresos) {
		this.totalIngresos = totalIngresos;
	}
	public BigDecimal getTotalAnulaciones() {
		return totalAnulaciones;
	}
	public void setTotalAnulaciones(BigDecimal totalAnulaciones) {
		this.totalAnulaciones = totalAnulaciones;
	}
	public BigDecimal getTotalEgresos() {
		return totalEgresos;
	}
	public void setTotalEgresos(BigDecimal totalEgresos) {
		this.totalEgresos = totalEgresos;
	}
	public BigDecimal getTotalCargos() {
		return totalCargos;
	}
	public void setTotalCargos(BigDecimal totalCargos) {
		this.totalCargos = totalCargos;
	}
	public BigDecimal getTotalNetos() {
		return totalNetos;
	}
	public void setTotalNetos(BigDecimal totalNetos) {
		this.totalNetos = totalNetos;
	}
	public BigDecimal getTotalRetiros() {
		return totalRetiros;
	}
	public void setTotalRetiros(BigDecimal totalRetiros) {
		this.totalRetiros = totalRetiros;
	}
	public BigDecimal getTotPtsVend() {
		return totPtsVend;
	}
	public void setTotPtsVend(BigDecimal totPtsVend) {
		this.totPtsVend = totPtsVend;
	}
	public BigDecimal getTotPtsCanj() {
		return totPtsCanj;
	}
	public void setTotPtsCanj(BigDecimal totPtsCanj) {
		this.totPtsCanj = totPtsCanj;
	}
	public BigDecimal getTotalConDespacho() {
		return totalConDespacho;
	}
	public void setTotalConDespacho(BigDecimal totalConDespacho) {
		this.totalConDespacho = totalConDespacho;
	}
	public BigDecimal getTotalSinDespacho() {
		return totalSinDespacho;
	}
	public void setTotalSinDespacho(BigDecimal totalSinDespacho) {
		this.totalSinDespacho = totalSinDespacho;
	}
	public BigDecimal getTotalVtaNotaVenta() {
		return totalVtaNotaVenta;
	}
	public void setTotalVtaNotaVenta(BigDecimal totalVtaNotaVenta) {
		this.totalVtaNotaVenta = totalVtaNotaVenta;
	}
	public BigDecimal getTotalIngNotaVenta() {
		return totalIngNotaVenta;
	}
	public void setTotalIngNotaVenta(BigDecimal totalIngNotaVenta) {
		this.totalIngNotaVenta = totalIngNotaVenta;
	}
	public BigDecimal getTxadmMtoTotRglPel() {
		return txadmMtoTotRglPel;
	}
	public void setTxadmMtoTotRglPel(BigDecimal txadmMtoTotRglPel) {
		this.txadmMtoTotRglPel = txadmMtoTotRglPel;
	}
	public BigDecimal getTxadmMtoIngRglPel() {
		return txadmMtoIngRglPel;
	}
	public void setTxadmMtoIngRglPel(BigDecimal txadmMtoIngRglPel) {
		this.txadmMtoIngRglPel = txadmMtoIngRglPel;
	}
	public BigDecimal getTxadmRunCajCro() {
		return txadmRunCajCro;
	}
	public void setTxadmRunCajCro(BigDecimal txadmRunCajCro) {
		this.txadmRunCajCro = txadmRunCajCro;
	}
	public BigDecimal getTotalRecaudacion() {
		return totalRecaudacion;
	}
	public void setTotalRecaudacion(BigDecimal totalRecaudacion) {
		this.totalRecaudacion = totalRecaudacion;
	}
	public BigDecimal getTotalRevRecaudacion() {
		return totalRevRecaudacion;
	}
	public void setTotalRevRecaudacion(BigDecimal totalRevRecaudacion) {
		this.totalRevRecaudacion = totalRevRecaudacion;
	}
	public BigDecimal getTcCompra() {
		return tcCompra;
	}
	public void setTcCompra(BigDecimal tcCompra) {
		this.tcCompra = tcCompra;
	}
	public BigDecimal getTcVenta() {
		return tcVenta;
	}
	public void setTcVenta(BigDecimal tcVenta) {
		this.tcVenta = tcVenta;
	}
	public BigDecimal getTotalRedondeo() {
		return totalRedondeo;
	}
	public void setTotalRedondeo(BigDecimal totalRedondeo) {
		this.totalRedondeo = totalRedondeo;
	}
	
	
	

}
