package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxArticulos {
	
	@JsonProperty("ROW")
	private List<TrxArticulosRow> row;

	public List<TrxArticulosRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxArticulosRow>();
		}
		return row;
	}

	public void setRow(List<TrxArticulosRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxArticulos [row=").append(row).append("]");
		return builder.toString();
	}
	
}
