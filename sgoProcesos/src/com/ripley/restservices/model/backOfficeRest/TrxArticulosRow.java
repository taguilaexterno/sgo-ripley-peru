package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxArticulosRow {
	
	@JsonProperty("NRO_ITEM")
	private Integer nroItem;
	@JsonProperty("COD_ARTICULO")
	private String codArticulo;
	@JsonProperty("VENDEDOR")
	private Integer vendedor;
	@JsonProperty("CODIGO_NOVIOS")
	private Integer codigoNovios;
	@JsonProperty("SALE_ARTICULO")
	private Integer saleArticulo;
	@JsonProperty("PRECIO_ARTICULO")
	private BigDecimal precioArticulo;
	@JsonProperty("UNIDADES")
	private BigDecimal unidades;
	@JsonProperty("DESCUENTO")
	private BigDecimal descuento;
	@JsonProperty("TIPO_DESCUENTO")
	private Integer tipoDescuento;
	@JsonProperty("COD_UN_DES")
	private String codUnDes;
	@JsonProperty("PORC_PARTICIP")
	private BigDecimal procParticip;
	@JsonProperty("DESPACHO_DOMICILIO")
	private Integer despachoDomicilio;
	@JsonProperty("INDICADOR")
	private Integer indicador;
	@JsonProperty("GARANTIA")
	private Integer garantia;
	@JsonProperty("COD_COMBO")
	private String codCombo;
	@JsonProperty("NUM_EVENTO")
	private String numEvento;
	@JsonProperty("PRIORIDAD")
	private Integer prioridad;
	@JsonProperty("SUB_PRIORIDAD")
	private Integer subPrioridad;
	public Integer getNroItem() {
		return nroItem;
	}
	public void setNroItem(Integer nroItem) {
		this.nroItem = nroItem;
	}
	public String getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}
	public Integer getVendedor() {
		return vendedor;
	}
	public void setVendedor(Integer vendedor) {
		this.vendedor = vendedor;
	}
	public Integer getCodigoNovios() {
		return codigoNovios;
	}
	public void setCodigoNovios(Integer codigoNovios) {
		this.codigoNovios = codigoNovios;
	}
	public Integer getSaleArticulo() {
		return saleArticulo;
	}
	public void setSaleArticulo(Integer saleArticulo) {
		this.saleArticulo = saleArticulo;
	}
	public BigDecimal getPrecioArticulo() {
		return precioArticulo;
	}
	public void setPrecioArticulo(BigDecimal precioArticulo) {
		this.precioArticulo = precioArticulo;
	}
	public BigDecimal getUnidades() {
		return unidades;
	}
	public void setUnidades(BigDecimal unidades) {
		this.unidades = unidades;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public Integer getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(Integer tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	public String getCodUnDes() {
		return codUnDes;
	}
	public void setCodUnDes(String codUnDes) {
		this.codUnDes = codUnDes;
	}
	public BigDecimal getProcParticip() {
		return procParticip;
	}
	public void setProcParticip(BigDecimal procParticip) {
		this.procParticip = procParticip;
	}
	public Integer getDespachoDomicilio() {
		return despachoDomicilio;
	}
	public void setDespachoDomicilio(Integer despachoDomicilio) {
		this.despachoDomicilio = despachoDomicilio;
	}
	public Integer getIndicador() {
		return indicador;
	}
	public void setIndicador(Integer indicador) {
		this.indicador = indicador;
	}
	public Integer getGarantia() {
		return garantia;
	}
	public void setGarantia(Integer garantia) {
		this.garantia = garantia;
	}
	public String getCodCombo() {
		return codCombo;
	}
	public void setCodCombo(String codCombo) {
		this.codCombo = codCombo;
	}
	public String getNumEvento() {
		return numEvento;
	}
	public void setNumEvento(String numEvento) {
		this.numEvento = numEvento;
	}
	public Integer getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}
	public Integer getSubPrioridad() {
		return subPrioridad;
	}
	public void setSubPrioridad(Integer subPrioridad) {
		this.subPrioridad = subPrioridad;
	}
	
	

}
