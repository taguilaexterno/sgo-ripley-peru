package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxDespacho {

	@JsonProperty("ROW")
	private List<TrxDespachoRow> row;

	public List<TrxDespachoRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxDespachoRow>();
		}
		return row;
	}

	public void setRow(List<TrxDespachoRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxDespacho [row=").append(row).append("]");
		return builder.toString();
	}
	
	
}
