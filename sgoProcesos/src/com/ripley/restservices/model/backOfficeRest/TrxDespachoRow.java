package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxDespachoRow {

	@JsonProperty("COD_UNI_DES")
	private String codUniDes;
	@JsonProperty("COLOR")
	private Integer color;
	@JsonProperty("CUBICAJE")
	private BigDecimal cubicaje;
	@JsonProperty("RUT_DESPACHO")
	private Integer rutDespacho;
	@JsonProperty("NOMBRE_DESPACHO")
	private String nombreDespacho;
	
	/**
	 * fecha despacho formato YYYY-MM-DD
	 */
	@JsonProperty("FECHA_DESPACHO")
	private String fechaDespacho;
	@JsonProperty("SUCURSAL_DESPACHO")
	private Integer sucursalDespacho;
	@JsonProperty("COMUNA_DESPACHO")
	private Integer comunaDespacho;
	@JsonProperty("CIUDAD_DESPACHO")
	private Integer ciudadDespacho;
	@JsonProperty("REGION_DESPACHO")
	private Integer regionDespacho;
	@JsonProperty("DIRECCION_DESPACHO")
	private String direccionDespacho;
	@JsonProperty("TELEFONO_DESPACHO")
	private String telefonoDespacho;
	@JsonProperty("SECTOR_DESPACHO")
	private Integer sectorDespacho;
	@JsonProperty("JORNADA_DESPACHO")
	private String jornadaDespacho;
	@JsonProperty("OBSERVACION")
	private String observacion;
	@JsonProperty("PAGINA_CTC")
	private Integer paginaCtc;
	@JsonProperty("CORD_CTC")
	private String cordCtc;
	@JsonProperty("RUT_CLIENTE")
	private Integer rutCliente;
	@JsonProperty("DIRECC_CLIENTE")
	private String direccCliente;
	@JsonProperty("TELEFONO_CLIENTE")
	private String telefonoCliente;
	@JsonProperty("TIPO_CLIENTE")
	private Integer tipoCliente;
	@JsonProperty("NOMBRE_CLIENTE")
	private String nombreCliente;
	public String getCodUniDes() {
		return codUniDes;
	}
	public void setCodUniDes(String codUniDes) {
		this.codUniDes = codUniDes;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}
	public BigDecimal getCubicaje() {
		return cubicaje;
	}
	public void setCubicaje(BigDecimal cubicaje) {
		this.cubicaje = cubicaje;
	}
	public Integer getRutDespacho() {
		return rutDespacho;
	}
	public void setRutDespacho(Integer rutDespacho) {
		this.rutDespacho = rutDespacho;
	}
	public String getNombreDespacho() {
		return nombreDespacho;
	}
	public void setNombreDespacho(String nombreDespacho) {
		this.nombreDespacho = nombreDespacho;
	}
	public String getFechaDespacho() {
		return fechaDespacho;
	}
	public void setFechaDespacho(String fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}
	public Integer getSucursalDespacho() {
		return sucursalDespacho;
	}
	public void setSucursalDespacho(Integer sucursalDespacho) {
		this.sucursalDespacho = sucursalDespacho;
	}
	public Integer getComunaDespacho() {
		return comunaDespacho;
	}
	public void setComunaDespacho(Integer comunaDespacho) {
		this.comunaDespacho = comunaDespacho;
	}
	public Integer getCiudadDespacho() {
		return ciudadDespacho;
	}
	public void setCiudadDespacho(Integer ciudadDespacho) {
		this.ciudadDespacho = ciudadDespacho;
	}
	public Integer getRegionDespacho() {
		return regionDespacho;
	}
	public void setRegionDespacho(Integer regionDespacho) {
		this.regionDespacho = regionDespacho;
	}
	public String getDireccionDespacho() {
		return direccionDespacho;
	}
	public void setDireccionDespacho(String direccionDespacho) {
		this.direccionDespacho = direccionDespacho;
	}
	public String getTelefonoDespacho() {
		return telefonoDespacho;
	}
	public void setTelefonoDespacho(String telefonoDespacho) {
		this.telefonoDespacho = telefonoDespacho;
	}
	public Integer getSectorDespacho() {
		return sectorDespacho;
	}
	public void setSectorDespacho(Integer sectorDespacho) {
		this.sectorDespacho = sectorDespacho;
	}
	public String getJornadaDespacho() {
		return jornadaDespacho;
	}
	public void setJornadaDespacho(String jornadaDespacho) {
		this.jornadaDespacho = jornadaDespacho;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public Integer getPaginaCtc() {
		return paginaCtc;
	}
	public void setPaginaCtc(Integer paginaCtc) {
		this.paginaCtc = paginaCtc;
	}
	public String getCordCtc() {
		return cordCtc;
	}
	public void setCordCtc(String cordCtc) {
		this.cordCtc = cordCtc;
	}
	public Integer getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Integer rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getDireccCliente() {
		return direccCliente;
	}
	public void setDireccCliente(String direccCliente) {
		this.direccCliente = direccCliente;
	}
	public String getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}
	public Integer getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(Integer tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxDespachoRow [codUniDes=").append(codUniDes).append(", color=").append(color)
				.append(", cubicaje=").append(cubicaje).append(", rutDespacho=").append(rutDespacho)
				.append(", nombreDespacho=").append(nombreDespacho).append(", fechaDespacho=").append(fechaDespacho)
				.append(", sucursalDespacho=").append(sucursalDespacho).append(", comunaDespacho=")
				.append(comunaDespacho).append(", ciudadDespacho=").append(ciudadDespacho).append(", regionDespacho=")
				.append(regionDespacho).append(", direccionDespacho=").append(direccionDespacho)
				.append(", telefonoDespacho=").append(telefonoDespacho).append(", sectorDespacho=")
				.append(sectorDespacho).append(", jornadaDespacho=").append(jornadaDespacho).append(", observacion=")
				.append(observacion).append(", paginaCtc=").append(paginaCtc).append(", cordCtc=").append(cordCtc)
				.append(", rutCliente=").append(rutCliente).append(", direccCliente=").append(direccCliente)
				.append(", telefonoCliente=").append(telefonoCliente).append(", tipoCliente=").append(tipoCliente)
				.append(", nombreCliente=").append(nombreCliente).append("]");
		return builder.toString();
	}
	
	
	
}
