package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxEticket {
	
	@JsonProperty("ROW")
	private List<TrxEticketRow> row;

	public List<TrxEticketRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxEticketRow>();
		}
		return row;
	}

	public void setRow(List<TrxEticketRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxEticket [row=").append(row).append("]");
		return builder.toString();
	}
	
}
