package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxEticketRow {
	
	@JsonProperty("ETK_NRO_SERIE")
	private String etkNroSerie;
	@JsonProperty("ETK_CORRELATIVO")
	private String etkCorrelativo;
	@JsonProperty("ETK_TIPO_DOC")
	private String etkTipoDoc;
	@JsonProperty("ETK_NRO_SERIE_ORIG")
	private String etkNroSerieOrig;
	@JsonProperty("ETK_CORRELATIVO_ORIG")
	private String etkCorrelativoOrig;
	public String getEtkNroSerie() {
		return etkNroSerie;
	}
	public void setEtkNroSerie(String etkNroSerie) {
		this.etkNroSerie = etkNroSerie;
	}
	public String getEtkCorrelativo() {
		return etkCorrelativo;
	}
	public void setEtkCorrelativo(String etkCorrelativo) {
		this.etkCorrelativo = etkCorrelativo;
	}
	public String getEtkTipoDoc() {
		return etkTipoDoc;
	}
	public void setEtkTipoDoc(String etkTipoDoc) {
		this.etkTipoDoc = etkTipoDoc;
	}
	public String getEtkNroSerieOrig() {
		return etkNroSerieOrig;
	}
	public void setEtkNroSerieOrig(String etkNroSerieOrig) {
		this.etkNroSerieOrig = etkNroSerieOrig;
	}
	public String getEtkCorrelativoOrig() {
		return etkCorrelativoOrig;
	}
	public void setEtkCorrelativoOrig(String etkCorrelativoOrig) {
		this.etkCorrelativoOrig = etkCorrelativoOrig;
	}
	
	

}
