package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxMultipagosBo {
	
	@JsonProperty("ROW")
	private List<TrxMultipagosBoRow> row;

	public List<TrxMultipagosBoRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxMultipagosBoRow>();
		}
		return row;
	}

	public void setRow(List<TrxMultipagosBoRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxMultipagosBo [row=").append(row).append("]");
		return builder.toString();
	}
	
	
}
