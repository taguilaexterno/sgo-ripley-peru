package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxMultipagosBoRow {
	
	@JsonProperty("TIPO_PAGO")
	private Integer tipoPago;
	@JsonProperty("ID_PROVEEDOR")
	private String idProveedor;
	@JsonProperty("ID_TRX")
	private String idTrx;
	@JsonProperty("COD_AUT")
	private String codAut;
	@JsonProperty("NRO_OC")
	private String nroOc;
	@JsonProperty("MONTO")
	private BigDecimal monto;
	@JsonProperty("PLAZO")
	private Integer plazo;
	@JsonProperty("DIFERIDO")
	private Integer diferido;
	@JsonProperty("OBSERVACION")
	private String observacion;
	@JsonProperty("GLOSA")
	private String glosa;
	public Integer getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getIdTrx() {
		return idTrx;
	}
	public void setIdTrx(String idTrx) {
		this.idTrx = idTrx;
	}
	public String getCodAut() {
		return codAut;
	}
	public void setCodAut(String codAut) {
		this.codAut = codAut;
	}
	public String getNroOc() {
		return nroOc;
	}
	public void setNroOc(String nroOc) {
		this.nroOc = nroOc;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public Integer getDiferido() {
		return diferido;
	}
	public void setDiferido(Integer diferido) {
		this.diferido = diferido;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	
	

}
