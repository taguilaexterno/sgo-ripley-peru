package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxRecaudacionBo {
	
	@JsonProperty("ROW")
	private List<TrxRecaudacionBoRow> row;

	public List<TrxRecaudacionBoRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxRecaudacionBoRow>();
		}
		return row;
	}

	public void setRow(List<TrxRecaudacionBoRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxRecaudacionBo [row=").append(row).append("]");
		return builder.toString();
	}
	
	
}
