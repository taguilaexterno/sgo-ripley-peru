package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxRecaudacionBoRow {
	
	@JsonProperty("CODIGO_CIA")
	private Integer codigoCia;
	@JsonProperty("CODIGO_PROD")
	private Integer codigoProd;
	@JsonProperty("CODIGO_SUBPROD")
	private Integer codigoSubprod;
	@JsonProperty("MONTO")
	private BigDecimal monto;
	@JsonProperty("CANTIDAD")
	private BigDecimal cantidad;
	@JsonProperty("OBSERVACION")
	private String observacion;
	@JsonProperty("GLOSA")
	private String glosa;
	public Integer getCodigoCia() {
		return codigoCia;
	}
	public void setCodigoCia(Integer codigoCia) {
		this.codigoCia = codigoCia;
	}
	public Integer getCodigoProd() {
		return codigoProd;
	}
	public void setCodigoProd(Integer codigoProd) {
		this.codigoProd = codigoProd;
	}
	public Integer getCodigoSubprod() {
		return codigoSubprod;
	}
	public void setCodigoSubprod(Integer codigoSubprod) {
		this.codigoSubprod = codigoSubprod;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	
}
