package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxRecaudacionPrvBo {
	
	@JsonProperty("ROW")
	private List<TrxRecaudacionPrvBoRow> row;

	public List<TrxRecaudacionPrvBoRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxRecaudacionPrvBoRow>();
		}
		return row;
	}

	public void setRow(List<TrxRecaudacionPrvBoRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxRecaudacionPrvBo [row=").append(row).append("]");
		return builder.toString();
	}

}
