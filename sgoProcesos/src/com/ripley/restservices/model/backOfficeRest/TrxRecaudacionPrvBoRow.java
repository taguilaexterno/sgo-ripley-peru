package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxRecaudacionPrvBoRow {

	@JsonProperty("CODIGO_CIA")
	private Integer codigoCia;
	@JsonProperty("PROV_RUT")
	private String provRut;
	@JsonProperty("SUBORDEN")
	private String suborden;
	@JsonProperty("MONTO")
	private BigDecimal monto;
	
	/**
	 * porcentaje comision
	 */
	@JsonProperty("COMISION")
	private BigDecimal comision;

	public Integer getCodigoCia() {
		return codigoCia;
	}

	public void setCodigoCia(Integer codigoCia) {
		this.codigoCia = codigoCia;
	}

	public String getProvRut() {
		return provRut;
	}

	public void setProvRut(String provRut) {
		this.provRut = provRut;
	}

	public String getSuborden() {
		return suborden;
	}

	public void setSuborden(String suborden) {
		this.suborden = suborden;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getComision() {
		return comision;
	}

	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxRecaudacionPrvBoRow [codigoCia=").append(codigoCia).append(", provRut=").append(provRut)
				.append(", suborden=").append(suborden).append(", monto=").append(monto).append(", comision=")
				.append(comision).append("]");
		return builder.toString();
	}
	
	
	
}
