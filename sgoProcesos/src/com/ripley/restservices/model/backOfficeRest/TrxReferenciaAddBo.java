package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxReferenciaAddBo {
	
	@JsonProperty("ROW")
	private List<TrxReferenciaAddBoRow> row;

	public List<TrxReferenciaAddBoRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxReferenciaAddBoRow>();
		}
		return row;
	}

	public void setRow(List<TrxReferenciaAddBoRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxReferenciaAddBo [row=").append(row).append("]");
		return builder.toString();
	}
	
}
