package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxReferenciaAddBoRow {
	
	@JsonProperty("NRO_TARJETA")
	private String nroTarjeta;
	@JsonProperty("TIPO_PAGO")
	private Integer tipoPago;
	@JsonProperty("ID_TRX")
	private String idTrx;
	@JsonProperty("COD_AUT")
	private String codAut;
	@JsonProperty("NRO_OC")
	private Long nroOc;
	@JsonProperty("FECHA_PAGO")
	private String fechaPago;
	@JsonProperty("GLOSA")
	private String glosa;
	
	
	public String getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public Integer getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getIdTrx() {
		return idTrx;
	}
	public void setIdTrx(String idTrx) {
		this.idTrx = idTrx;
	}
	public String getCodAut() {
		return codAut;
	}
	public void setCodAut(String codAut) {
		this.codAut = codAut;
	}
	public Long getNroOc() {
		return nroOc;
	}
	public void setNroOc(Long nroOc) {
		this.nroOc = nroOc;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	
	
	

}
