package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTarBancaria {

	@JsonProperty("ROW")
	private List<TrxTarBancariaRow> row;

	public List<TrxTarBancariaRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxTarBancariaRow>();
		}
		return row;
	}

	public void setRow(List<TrxTarBancariaRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTarBancaria [row=").append(row).append("]");
		return builder.toString();
	}
	
}
