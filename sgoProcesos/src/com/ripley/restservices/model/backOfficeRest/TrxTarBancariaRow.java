package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTarBancariaRow {

	@JsonProperty("NRO_TARJETA")
	private String nroTarjeta;
	@JsonProperty("RUT_CLIENTE")
	private Long rutCliente;
	@JsonProperty("MONTO")
	private BigDecimal monto;
	@JsonProperty("TIPO_CUOTA")
	private Integer tipoCuota;
	@JsonProperty("NRO_CUOTAS")
	private Integer nroCuotas;
	@JsonProperty("FECHA_VENCE")
	private Integer fechaVence;
	@JsonProperty("COD_AUTORIZADOR")
	private Long codAutorizador;
	@JsonProperty("PORC_PARTICIP")
	private BigDecimal porcParticip;
	@JsonProperty("MONTO_INTERESES")
	private BigDecimal montoIntereses;
	@JsonProperty("VALOR_CUOTA")
	private BigDecimal valorCuota;
	@JsonProperty("COD_REFERENCIA")
	private Integer codReferencia;
	public String getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public Long getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(Long rutCliente) {
		this.rutCliente = rutCliente;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Integer getTipoCuota() {
		return tipoCuota;
	}
	public void setTipoCuota(Integer tipoCuota) {
		this.tipoCuota = tipoCuota;
	}
	public Integer getNroCuotas() {
		return nroCuotas;
	}
	public void setNroCuotas(Integer nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	public Integer getFechaVence() {
		return fechaVence;
	}
	public void setFechaVence(Integer fechaVence) {
		this.fechaVence = fechaVence;
	}
	public Long getCodAutorizador() {
		return codAutorizador;
	}
	public void setCodAutorizador(Long codAutorizador) {
		this.codAutorizador = codAutorizador;
	}
	public BigDecimal getPorcParticip() {
		return porcParticip;
	}
	public void setPorcParticip(BigDecimal porcParticip) {
		this.porcParticip = porcParticip;
	}
	public BigDecimal getMontoIntereses() {
		return montoIntereses;
	}
	public void setMontoIntereses(BigDecimal montoIntereses) {
		this.montoIntereses = montoIntereses;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public Integer getCodReferencia() {
		return codReferencia;
	}
	public void setCodReferencia(Integer codReferencia) {
		this.codReferencia = codReferencia;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTarBancariaRow [nroTarjeta=").append(nroTarjeta).append(", rutCliente=").append(rutCliente)
				.append(", monto=").append(monto).append(", tipoCuota=").append(tipoCuota).append(", nroCuotas=")
				.append(nroCuotas).append(", fechaVence=").append(fechaVence).append(", codAutorizador=")
				.append(codAutorizador).append(", porcParticip=").append(porcParticip).append(", montoIntereses=")
				.append(montoIntereses).append(", valorCuota=").append(valorCuota).append(", codReferencia=")
				.append(codReferencia).append("]");
		return builder.toString();
	}
	
	
	
}
