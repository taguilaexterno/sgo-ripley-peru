package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTarRipley {

	@JsonProperty("ROW")
	private List<TrxTarRipleyRow> row;

	public List<TrxTarRipleyRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxTarRipleyRow>();
		}
		return row;
	}

	public void setRow(List<TrxTarRipleyRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTarRipley [row=").append(row).append("]");
		return builder.toString();
	}
	
}
