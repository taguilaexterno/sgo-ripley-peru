package com.ripley.restservices.model.backOfficeRest;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTarRipleyRow {

	@JsonProperty("ADMINIS")
	private Integer adminis;
	@JsonProperty("EMISOR")
	private Integer emisor;
	@JsonProperty("RUT_PODER")
	private Long rutPoder;
	@JsonProperty("PLAZO")
	private Integer plazo;
	@JsonProperty("DIFERIDO")
	private Integer diferido;
	@JsonProperty("MONTO_CAPITAL")
	private BigDecimal montoCapital;
	@JsonProperty("MONTO_PIE")
	private BigDecimal montoPie;
	@JsonProperty("DESCTO_CAR")
	private BigDecimal desctoCar;
	@JsonProperty("CODIGO_DESCTO")
	private Integer codigoDescto;
	@JsonProperty("COD_AUTORIZADOR")
	private Long codAutorizador;
	@JsonProperty("PORC_PATICIP")
	private BigDecimal porcParticip;
	@JsonProperty("RUT_TITULAR")
	private Long rutTitular;
	@JsonProperty("CUENTA_LARGA")
	private Long cuentaLarga;
	public Integer getAdminis() {
		return adminis;
	}
	public void setAdminis(Integer adminis) {
		this.adminis = adminis;
	}
	public Integer getEmisor() {
		return emisor;
	}
	public void setEmisor(Integer emisor) {
		this.emisor = emisor;
	}
	public Long getRutPoder() {
		return rutPoder;
	}
	public void setRutPoder(Long rutPoder) {
		this.rutPoder = rutPoder;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public Integer getDiferido() {
		return diferido;
	}
	public void setDiferido(Integer diferido) {
		this.diferido = diferido;
	}
	public BigDecimal getMontoCapital() {
		return montoCapital;
	}
	public void setMontoCapital(BigDecimal montoCapital) {
		this.montoCapital = montoCapital;
	}
	public BigDecimal getMontoPie() {
		return montoPie;
	}
	public void setMontoPie(BigDecimal montoPie) {
		this.montoPie = montoPie;
	}
	public BigDecimal getDesctoCar() {
		return desctoCar;
	}
	public void setDesctoCar(BigDecimal desctoCar) {
		this.desctoCar = desctoCar;
	}
	public Integer getCodigoDescto() {
		return codigoDescto;
	}
	public void setCodigoDescto(Integer codigoDescto) {
		this.codigoDescto = codigoDescto;
	}
	public Long getCodAutorizador() {
		return codAutorizador;
	}
	public void setCodAutorizador(Long codAutorizador) {
		this.codAutorizador = codAutorizador;
	}
	public BigDecimal getPorcParticip() {
		return porcParticip;
	}
	public void setPorcParticip(BigDecimal porcParticip) {
		this.porcParticip = porcParticip;
	}

	public Long getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(Long rutTitular) {
		this.rutTitular = rutTitular;
	}
	public Long getCuentaLarga() {
		return cuentaLarga;
	}
	public void setCuentaLarga(Long cuentaLarga) {
		this.cuentaLarga = cuentaLarga;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTarRipleyRow [adminis=").append(adminis).append(", emisor=").append(emisor)
				.append(", rutPoder=").append(rutPoder).append(", plazo=").append(plazo).append(", diferido=")
				.append(diferido).append(", montoCapital=").append(montoCapital).append(", montoPie=").append(montoPie)
				.append(", desctoCar=").append(desctoCar).append(", codigoDescto=").append(codigoDescto)
				.append(", codAutorizador=").append(codAutorizador).append(", porcParticip=").append(porcParticip)
				.append(", rutTitular=").append(rutTitular).append(", cuentaLarga=").append(cuentaLarga).append("]");
		return builder.toString();
	}
	
	
	
}
