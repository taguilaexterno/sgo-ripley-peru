package com.ripley.restservices.model.backOfficeRest;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTransaccionAdd {
	
	@JsonProperty("ROW")
	private List<TrxTransaccionAddRow> row;

	public List<TrxTransaccionAddRow> getRow() {
		if(row == null) {
			row = new ArrayList<TrxTransaccionAddRow>();
		}
		return row;
	}

	public void setRow(List<TrxTransaccionAddRow> row) {
		this.row = row;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrxTransaccionAdd [row=").append(row).append("]");
		return builder.toString();
	}
	
}
