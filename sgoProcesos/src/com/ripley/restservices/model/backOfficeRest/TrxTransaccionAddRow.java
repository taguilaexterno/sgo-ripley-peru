package com.ripley.restservices.model.backOfficeRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrxTransaccionAddRow {
	
	@JsonProperty("SUCURSAL_FIS")
	private Integer sucursalFis;
	@JsonProperty("PC_MAC")
	private String pcMac;
	@JsonProperty("AUXILIAR1")
	private Integer auxiliar1;
	@JsonProperty("AUXILIAR2")
	private Integer auxiliar2;
	@JsonProperty("AUXILIAR3")
	private Integer auxiliar3;
	@JsonProperty("AUXILIAR4")
	private Integer auxiliar4;
	@JsonProperty("AUXILIAR5")
	private Integer auxiliar5;
	@JsonProperty("ADICIONAL1")
	private String adicional1;
	@JsonProperty("ADICIONAL2")
	private String adicional2;
	@JsonProperty("ADICIONAL3")
	private String adicional3;
	@JsonProperty("ADICIONAL4")
	private String adicional4;
	@JsonProperty("ADICIONAL5")
	private String adicional5;
	@JsonProperty("FECHA_EMISION")
	private String fechaEmision;
	@JsonProperty("FECHA_AUX1")
	private String fechaAux1;
	
	public String getAdicional1() {
		return adicional1;
	}
	public void setAdicional1(String adicional1) {
		this.adicional1 = adicional1;
	}
	public String getAdicional2() {
		return adicional2;
	}
	public void setAdicional2(String adicional2) {
		this.adicional2 = adicional2;
	}
	public String getAdicional3() {
		return adicional3;
	}
	public void setAdicional3(String adicional3) {
		this.adicional3 = adicional3;
	}
	public String getAdicional4() {
		return adicional4;
	}
	public void setAdicional4(String adicional4) {
		this.adicional4 = adicional4;
	}
	public Integer getSucursalFis() {
		return sucursalFis;
	}
	public void setSucursalFis(Integer sucursalFis) {
		this.sucursalFis = sucursalFis;
	}
	public String getPcMac() {
		return pcMac;
	}
	public void setPcMac(String pcMac) {
		this.pcMac = pcMac;
	}
	public Integer getAuxiliar1() {
		return auxiliar1;
	}
	public void setAuxiliar1(Integer auxiliar1) {
		this.auxiliar1 = auxiliar1;
	}
	public Integer getAuxiliar2() {
		return auxiliar2;
	}
	public void setAuxiliar2(Integer auxiliar2) {
		this.auxiliar2 = auxiliar2;
	}
	public Integer getAuxiliar3() {
		return auxiliar3;
	}
	public void setAuxiliar3(Integer auxiliar3) {
		this.auxiliar3 = auxiliar3;
	}
	public Integer getAuxiliar4() {
		return auxiliar4;
	}
	public void setAuxiliar4(Integer auxiliar4) {
		this.auxiliar4 = auxiliar4;
	}
	public Integer getAuxiliar5() {
		return auxiliar5;
	}
	public void setAuxiliar5(Integer auxiliar5) {
		this.auxiliar5 = auxiliar5;
	}
	public String getAdicional5() {
		return adicional5;
	}
	public void setAdicional5(String adicional5) {
		this.adicional5 = adicional5;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaAux1() {
		return fechaAux1;
	}
	public void setFechaAux1(String fechaAux1) {
		this.fechaAux1 = fechaAux1;
	}
	

}
