package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DpItemPromo {

	@XmlElement(name = "DPPromotionCode")
	private String dpPromotionCode;
	@XmlElement(name = "DPPromotionDescription")
	private String dpPromotionDescription;
	@XmlElement(name = "DPPromotionAmount")
	private String dpPromotionAmount;
	@XmlElement(name = "RelativeItemNumber")
	private String relativeItemNumber;
	@XmlElement(name = "DPRewardDepartment")
	private String dpRewardDepartment;
	public String getDpPromotionCode() {
		return dpPromotionCode;
	}
	public void setDpPromotionCode(String dpPromotionCode) {
		this.dpPromotionCode = dpPromotionCode;
	}
	public String getDpPromotionDescription() {
		return dpPromotionDescription;
	}
	public void setDpPromotionDescription(String dpPromotionDescription) {
		this.dpPromotionDescription = dpPromotionDescription;
	}
	public String getDpPromotionAmount() {
		return dpPromotionAmount;
	}
	public void setDpPromotionAmount(String dpPromotionAmount) {
		this.dpPromotionAmount = dpPromotionAmount;
	}
	public String getRelativeItemNumber() {
		return relativeItemNumber;
	}
	public void setRelativeItemNumber(String relativeItemNumber) {
		this.relativeItemNumber = relativeItemNumber;
	}
	public String getDpRewardDepartment() {
		return dpRewardDepartment;
	}
	public void setDpRewardDepartment(String dpRewardDepartment) {
		this.dpRewardDepartment = dpRewardDepartment;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DpItemPromo [dpPromotionCode=").append(dpPromotionCode).append(", dpPromotionDescription=")
				.append(dpPromotionDescription).append(", dpPromotionAmount=").append(dpPromotionAmount)
				.append(", relativeItemNumber=").append(relativeItemNumber).append(", dpRewardDepartment=")
				.append(dpRewardDepartment).append("]");
		return builder.toString();
	}
	
	
	
}
