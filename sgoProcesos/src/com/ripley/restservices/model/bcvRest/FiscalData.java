package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
@XmlAccessorType(XmlAccessType.FIELD)
public class FiscalData {

	@XmlElement(name = "CustomerDocumentType")
	private String customerDocumentType;
	@XmlElement(name = "CustomerDocumentNumber")
	private String customerDocumentNumber;
	@XmlElement(name = "CustomerName")
	private String customerName;
	@XmlElement(name = "CustomerPhone")
	private String customerPhone;
	@XmlElement(name = "InvoiceCustomerId")
	private String invoiceCustomerId;
	@XmlElement(name = "InvoiceCustomerName")
	private String invoiceCustomerName;
	public String getCustomerDocumentType() {
		return customerDocumentType;
	}
	public void setCustomerDocumentType(String customerDocumentType) {
		this.customerDocumentType = customerDocumentType;
	}
	public String getCustomerDocumentNumber() {
		return customerDocumentNumber;
	}
	public void setCustomerDocumentNumber(String customerDocumentNumber) {
		this.customerDocumentNumber = customerDocumentNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getInvoiceCustomerId() {
		return invoiceCustomerId;
	}
	public void setInvoiceCustomerId(String invoiceCustomerId) {
		this.invoiceCustomerId = invoiceCustomerId;
	}
	public String getInvoiceCustomerName() {
		return invoiceCustomerName;
	}
	public void setInvoiceCustomerName(String invoiceCustomerName) {
		this.invoiceCustomerName = invoiceCustomerName;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FiscalData [customerDocumentType=").append(customerDocumentType)
				.append(", customerDocumentNumber=").append(customerDocumentNumber)
				.append(", customerName=").append(customerName)
				.append(", customerPhone=").append(customerPhone)
				.append(", invoiceCustomerId=").append(invoiceCustomerId)
				.append(", invoiceCustomerName=").append(invoiceCustomerName)
				.append("]");
		return builder.toString();
	}
	
}
