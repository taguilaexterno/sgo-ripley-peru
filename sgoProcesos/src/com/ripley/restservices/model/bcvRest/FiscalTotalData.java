package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class FiscalTotalData {

	@XmlElement(name = "TaxableAmount")
	private String taxableAmount;
	@XmlElement(name = "NonTaxableAmount")
	private String nonTaxableAmount;
	@XmlElement(name = "Vat1Amount")
	private String vat1Amount;
	@XmlElement(name = "InternalTaxAmount")
	private String internalTaxAmount;
	public String getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(String taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public String getNonTaxableAmount() {
		return nonTaxableAmount;
	}
	public void setNonTaxableAmount(String nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}
	public String getVat1Amount() {
		return vat1Amount;
	}
	public void setVat1Amount(String vat1Amount) {
		this.vat1Amount = vat1Amount;
	}
	public String getInternalTaxAmount() {
		return internalTaxAmount;
	}
	public void setInternalTaxAmount(String internalTaxAmount) {
		this.internalTaxAmount = internalTaxAmount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FiscalTotalData [taxableAmount=").append(taxableAmount).append(", nonTaxableAmount=")
				.append(nonTaxableAmount).append(", vat1Amount=").append(vat1Amount).append(", internalTaxAmount=")
				.append(internalTaxAmount).append("]");
		return builder.toString();
	}
	
}
