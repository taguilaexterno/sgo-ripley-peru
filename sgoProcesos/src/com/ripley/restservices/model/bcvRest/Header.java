package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Header {

	@XmlElement(name = "Flag")
	private String flag;
	@XmlElement(name = "BusinessMonthDay")
	private String businessMonthDay;
	@XmlElement(name = "RetailStoreID")
	private String retailStoreID;
	@XmlElement(name = "Segment")
	private String segment;
	@XmlElement(name = "SystemID")
	private String systemID;
	@XmlElement(name = "SequenceNumber")
	private String sequenceNumber;
	@XmlElement(name = "TransactionTypeCode")
	private String transactionTypeCode;
	@XmlElement(name = "WorkstationID")
	private String workstationID;
	@XmlElement(name = "BusinessYear")
	private String businessYear;
	@XmlElement(name = "TransactionStatus")
	private String transactionStatus;
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getBusinessMonthDay() {
		return businessMonthDay;
	}
	public void setBusinessMonthDay(String businessMonthDay) {
		this.businessMonthDay = businessMonthDay;
	}
	public String getRetailStoreID() {
		return retailStoreID;
	}
	public void setRetailStoreID(String retailStoreID) {
		this.retailStoreID = retailStoreID;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getTransactionTypeCode() {
		return transactionTypeCode;
	}
	public void setTransactionTypeCode(String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}
	public String getWorkstationID() {
		return workstationID;
	}
	public void setWorkstationID(String workstationID) {
		this.workstationID = workstationID;
	}
	public String getBusinessYear() {
		return businessYear;
	}
	public void setBusinessYear(String businessYear) {
		this.businessYear = businessYear;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Header [flag=").append(flag).append(", businessMonthDay=").append(businessMonthDay)
				.append(", retailStoreID=").append(retailStoreID).append(", segment=").append(segment)
				.append(", systemID=").append(systemID).append(", sequenceNumber=").append(sequenceNumber)
				.append(", transactionTypeCode=").append(transactionTypeCode).append(", workstationID=")
				.append(workstationID).append(", businessYear=").append(businessYear).append(", transactionStatus=")
				.append(transactionStatus).append("]");
		return builder.toString();
	}
	
	
}
