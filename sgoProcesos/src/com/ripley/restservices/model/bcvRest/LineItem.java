package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class LineItem {

	@XmlElement(name = "EntryMethod")
	private String entryMethod;
	@XmlElement(name = "POSItemID")
	private String posItemID;
	@XmlElement(name = "Department")
	private String department;
	@XmlElement(name = "Class")
	private String class_;
	@XmlElement(name = "CategoryCode")
	private String categoryCode;
	@XmlElement(name = "Description")
	private String description;
	@XmlElement(name = "ExtendedAmount")
	private String extendedAmount;
	@XmlElement(name = "Quantity")
	private String quantity;
	@XmlElement(name = "ReturnItem")
	private String returnItem;
	@XmlElement(name = "NonTaxable")
	private String nonTaxable;
	@XmlElement(name = "RelativeItemNumber")
	private String relativeItemNumber;
	@XmlElement(name = "VatCode")
	private String vatCode;
	@XmlElement(name = "InternalTax")
	private String internalTax;
	@XmlElement(name = "SupervisorOperationMark")
	private String supervisorOperationMark;
	@XmlElement(name = "Classification")
	private String classification;
	@XmlElement(name = "Sku")
	private String sku;
	@XmlElement(name = "DespachoDomicilio")
	private String despachoDomicilio;
	@XmlElement(name = "ExtragarantiaChildNumber")
	private String extragarantiaChildNumber;
	@XmlElement(name = "Cod_Despacho")
	private String codDespacho;
	@XmlElement(name = "CodProductoSUNAT")
	private String codProductoSUNAT;
	public String getEntryMethod() {
		return entryMethod;
	}
	public void setEntryMethod(String entryMethod) {
		this.entryMethod = entryMethod;
	}
	public String getPosItemID() {
		return posItemID;
	}
	public void setPosItemID(String posItemID) {
		this.posItemID = posItemID;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getClass_() {
		return class_;
	}
	public void setClass_(String class_) {
		this.class_ = class_;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExtendedAmount() {
		return extendedAmount;
	}
	public void setExtendedAmount(String extendedAmount) {
		this.extendedAmount = extendedAmount;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getReturnItem() {
		return returnItem;
	}
	public void setReturnItem(String returnItem) {
		this.returnItem = returnItem;
	}
	public String getNonTaxable() {
		return nonTaxable;
	}
	public void setNonTaxable(String nonTaxable) {
		this.nonTaxable = nonTaxable;
	}
	public String getRelativeItemNumber() {
		return relativeItemNumber;
	}
	public void setRelativeItemNumber(String relativeItemNumber) {
		this.relativeItemNumber = relativeItemNumber;
	}
	public String getVatCode() {
		return vatCode;
	}
	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}
	public String getInternalTax() {
		return internalTax;
	}
	public void setInternalTax(String internalTax) {
		this.internalTax = internalTax;
	}
	public String getSupervisorOperationMark() {
		return supervisorOperationMark;
	}
	public void setSupervisorOperationMark(String supervisorOperationMark) {
		this.supervisorOperationMark = supervisorOperationMark;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getDespachoDomicilio() {
		return despachoDomicilio;
	}
	public void setDespachoDomicilio(String despachoDomicilio) {
		this.despachoDomicilio = despachoDomicilio;
	}
	public String getExtragarantiaChildNumber() {
		return extragarantiaChildNumber;
	}
	public void setExtragarantiaChildNumber(String extragarantiaChildNumber) {
		this.extragarantiaChildNumber = extragarantiaChildNumber;
	}
	public String getCodDespacho() {
		return codDespacho;
	}
	public void setCodDespacho(String codDespacho) {
		this.codDespacho = codDespacho;
	}
	public String getCodProductoSUNAT() {
		return codProductoSUNAT;
	}
	public void setCodProductoSUNAT(String codProductoSUNAT) {
		this.codProductoSUNAT = codProductoSUNAT;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LineItem [entryMethod=").append(entryMethod).append(", posItemID=").append(posItemID)
				.append(", department=").append(department).append(", class_=").append(class_).append(", categoryCode=")
				.append(categoryCode).append(", description=").append(description).append(", extendedAmount=")
				.append(extendedAmount).append(", quantity=").append(quantity).append(", returnItem=")
				.append(returnItem).append(", nonTaxable=").append(nonTaxable).append(", relativeItemNumber=")
				.append(relativeItemNumber).append(", vatCode=").append(vatCode).append(", internalTax=")
				.append(internalTax).append(", supervisorOperationMark=").append(supervisorOperationMark)
				.append(", classification=").append(classification).append(", sku=").append(sku)
				.append(", despachoDomicilio=").append(despachoDomicilio).append(", extragarantiaChildNumber=")
				.append(extragarantiaChildNumber).append(", codDespacho=").append(codDespacho)
				.append(", codProductoSUNAT=").append(codProductoSUNAT)
				.append("]");
		return builder.toString();
	}
	
	
}
