package com.ripley.restservices.model.bcvRest;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "MESSAGE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
		"header",
		"startSequence",
		"fiscalTotalData",
		"fiscalData",
		"lineItem",
		"dpItemPromo",
		"transactionDiscount",
		"pdeCustomer",
		"tender",
		"total"
})
public class Message {

	@XmlAttribute
	private String type;
	@XmlAttribute
	private String status;
	@XmlAttribute
	private String tentative;
	@XmlElement(name = "HEADER")
	private Header header;
	@XmlElement(name = "StartSequence")
	private StartSequence startSequence;
	@XmlElement(name = "FiscalTotalData")
	private FiscalTotalData fiscalTotalData;
	@XmlElement(name = "FiscalData")
	private FiscalData fiscalData;
	@XmlElement(name = "LineItem")
	private List<LineItem> lineItem;
	@XmlElement(name = "DpItemPromo")
	private List<DpItemPromo> dpItemPromo;
	@XmlElement(name = "TransactionDiscount")
	private TransactionDiscount transactionDiscount;
	@XmlElement(name = "Tender")
	private Tender tender;
	@XmlElement(name = "Total")
	private Total total;
	@XmlTransient
	private PagoVirtual pagoVirtual;
	@XmlElement(name = "PdeCustomer")
	private PdeCustomer pdeCustomer;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTentative() {
		return tentative;
	}
	public void setTentative(String tentative) {
		this.tentative = tentative;
	}
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public StartSequence getStartSequence() {
		return startSequence;
	}
	public void setStartSequence(StartSequence startSequence) {
		this.startSequence = startSequence;
	}
	public FiscalTotalData getFiscalTotalData() {
		return fiscalTotalData;
	}
	public void setFiscalTotalData(FiscalTotalData fiscalTotalData) {
		this.fiscalTotalData = fiscalTotalData;
	}
	public FiscalData getFiscalData() {
		return fiscalData;
	}
	public void setFiscalData(FiscalData fiscalData) {
		this.fiscalData = fiscalData;
	}
	public List<LineItem> getLineItem() {
		return lineItem;
	}
	public void setLineItem(List<LineItem> lineItem) {
		this.lineItem = lineItem;
	}
	public List<DpItemPromo> getDpItemPromo() {
		return dpItemPromo;
	}
	public void setDpItemPromo(List<DpItemPromo> dpItemPromo) {
		this.dpItemPromo = dpItemPromo;
	}
	public TransactionDiscount getTransactionDiscount() {
		return transactionDiscount;
	}
	public void setTransactionDiscount(TransactionDiscount transactionDiscount) {
		this.transactionDiscount = transactionDiscount;
	}
	public Tender getTender() {
		return tender;
	}
	public void setTender(Tender tender) {
		this.tender = tender;
	}
	public Total getTotal() {
		return total;
	}
	public void setTotal(Total total) {
		this.total = total;
	}
	public PagoVirtual getPagoVirtual() {
		return pagoVirtual;
	}
	public void setPagoVirtual(PagoVirtual pagoVirtual) {
		this.pagoVirtual = pagoVirtual;
	}
	public PdeCustomer getPdeCustomer() {
		return pdeCustomer;
	}
	public void setPdeCustomer(PdeCustomer pdeCustomer) {
		this.pdeCustomer = pdeCustomer;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"type\":\"").append(type).append("\",status\":\"").append(status).append("\",tentative\":\"")
				.append(tentative).append("\",header\":\"").append(header).append("\",startSequence\":\"")
				.append(startSequence).append("\",fiscalTotalData\":\"").append(fiscalTotalData)
				.append("\",fiscalData\":\"").append(fiscalData).append("\",lineItem\":\"").append(lineItem)
				.append("\",dpItemPromo\":\"").append(dpItemPromo).append("\",transactionDiscount\":\"")
				.append(transactionDiscount).append("\",tender\":\"").append(tender).append("\",total\":\"")
				.append(total).append("\",pagoVirtual\":\"").append(pagoVirtual).append("\",pdeCustomer\":\"")
				.append(pdeCustomer).append("}");
		return builder.toString();
	}
	
	
}
