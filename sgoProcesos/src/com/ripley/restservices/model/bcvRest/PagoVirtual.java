package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class PagoVirtual {

	@XmlElement(name = "tipo_pago_virtual")
	private Integer tipoPagoVirtual;
	
	@XmlElement(name = "des_pago_virtual")
	private String desPagoVirtual;
	
	@XmlElement(name = "nro_orden_compra")
	private Long nroOrdenCompra;
	
	@XmlElement(name = "fec_referencia")
	private String fecReferencia;
	
	@XmlElement(name = "suc_referencia")
	private Integer sucReferencia;
	
	@XmlElement(name = "caja_referencia")
	private Integer cajaReferencia;
	
	@XmlElement(name = "nrotrx_referencia")
	private Long nroTrxReferencia;
	
	@XmlElement(name = "nrodoc_referencia")
	private Long nroDocReferencia;

	public Integer getTipoPagoVirtual() {
		return tipoPagoVirtual;
	}

	public void setTipoPagoVirtual(Integer tipoPagoVirtual) {
		this.tipoPagoVirtual = tipoPagoVirtual;
	}

	public String getDesPagoVirtual() {
		return desPagoVirtual;
	}

	public void setDesPagoVirtual(String desPagoVirtual) {
		this.desPagoVirtual = desPagoVirtual;
	}

	public Long getNroOrdenCompra() {
		return nroOrdenCompra;
	}

	public void setNroOrdenCompra(Long nroOrdenCompra) {
		this.nroOrdenCompra = nroOrdenCompra;
	}

	public String getFecReferencia() {
		return fecReferencia;
	}

	public void setFecReferencia(String fecReferencia) {
		this.fecReferencia = fecReferencia;
	}

	public Integer getSucReferencia() {
		return sucReferencia;
	}

	public void setSucReferencia(Integer sucReferencia) {
		this.sucReferencia = sucReferencia;
	}

	public Integer getCajaReferencia() {
		return cajaReferencia;
	}

	public void setCajaReferencia(Integer cajaReferencia) {
		this.cajaReferencia = cajaReferencia;
	}

	public Long getNroTrxReferencia() {
		return nroTrxReferencia;
	}

	public void setNroTrxReferencia(Long nroTrxReferencia) {
		this.nroTrxReferencia = nroTrxReferencia;
	}

	public Long getNroDocReferencia() {
		return nroDocReferencia;
	}

	public void setNroDocReferencia(Long nroDocReferencia) {
		this.nroDocReferencia = nroDocReferencia;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PagoVirtual [tipoPagoVirtual=").append(tipoPagoVirtual).append(", desPagoVirtual=")
				.append(desPagoVirtual).append(", nroOrdenCompra=").append(nroOrdenCompra).append(", fecReferencia=")
				.append(fecReferencia).append(", sucReferencia=").append(sucReferencia).append(", cajaReferencia=")
				.append(cajaReferencia).append(", nroTrxReferencia=").append(nroTrxReferencia)
				.append(", nroDocReferencia=").append(nroDocReferencia).append("]");
		return builder.toString();
	}
	
	
	
}
