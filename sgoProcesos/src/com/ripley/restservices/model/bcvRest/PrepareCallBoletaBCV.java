package com.ripley.restservices.model.bcvRest;

import java.time.LocalDateTime;

import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public class PrepareCallBoletaBCV {

	private TramaDTE trama;
	private Parametros pcv;
	private Long nroTrx;
	private String folioPPL;
	private LocalDateTime ldtFechaTrx;
	private boolean flagRpos=false;
	
	public boolean isFlagRpos() {
		return flagRpos;
	}
	public void setFlagRpos(boolean flagRpos) {
		this.flagRpos = flagRpos;
	}
	public TramaDTE getTrama() {
		return trama;
	}
	public void setTrama(TramaDTE trama) {
		this.trama = trama;
	}
	public Parametros getPcv() {
		return pcv;
	}
	public void setPcv(Parametros pcv) {
		this.pcv = pcv;
	}
	public Long getNroTrx() {
		return nroTrx;
	}
	public void setNroTrx(Long nroTrx) {
		this.nroTrx = nroTrx;
	}
	public String getFolioPPL() {
		return folioPPL;
	}
	public void setFolioPPL(String folioPPL) {
		this.folioPPL = folioPPL;
	}
	public LocalDateTime getLdtFechaTrx() {
		return ldtFechaTrx;
	}
	public void setLdtFechaTrx(LocalDateTime ldtFechaTrx) {
		this.ldtFechaTrx = ldtFechaTrx;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"trama\":\"").append(trama).append("\",\"pcv\":\"").append(pcv).append("\",\"nroTrx\":\"")
				.append(nroTrx).append("\",\"folioPPL\":\"").append(folioPPL).append("\",\"ldtFechaTrx\":\"")
				.append(ldtFechaTrx).append("\",\"flagRpos\":\"").append(flagRpos).append("\"}");
		return builder.toString();
	}
	
	
	
}
