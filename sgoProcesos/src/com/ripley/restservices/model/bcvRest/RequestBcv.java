package com.ripley.restservices.model.bcvRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestBcv {

	@JsonProperty
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestBcv [message=").append(message).append("]");
		return builder.toString();
	}

	
	
}
