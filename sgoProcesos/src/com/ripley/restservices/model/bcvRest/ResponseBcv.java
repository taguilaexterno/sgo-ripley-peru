package com.ripley.restservices.model.bcvRest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseBcv {

	@JsonProperty("Codigo")
	private String codigo;
	@JsonProperty("Mensaje")
	private String mensaje;
	@JsonProperty("Data")
	private Object data;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseBcv [codigo=").append(codigo).append(", mensaje=").append(mensaje).append(", data=")
				.append(data).append("]");
		return builder.toString();
	}
	
	
}
