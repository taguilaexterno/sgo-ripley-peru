package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class StartSequence {

	@XmlElement(name = "OperatorID")
	private String operatorID;
	@XmlElement(name = "BeginDateTime")
	private String beginDateTime;
	@XmlElement(name = "EndDateTime")
	private String endDateTime;
	@XmlElement(name = "BusinessDayDate")
	private String businessDayDate;
	@XmlElement(name = "CurrencyCode")
	private String currencyCode;
	@XmlElement(name = "CancelFlag")
	private String cancelFlag;
	@XmlElement(name = "TrainingMode")
	private String trainingMode;
	@XmlElement(name = "SuspendFlag")
	private String suspendFlag;
	@XmlElement(name = "TransactionTypeCode")
	private String transactionTypeCode;
	@XmlElement(name = "CorrelativoNumber")
	private String correlativoNumber;
	@XmlElement(name = "CashierName")
	private String cashierName;
	@XmlElement(name = "VendorName")
	private String vendorName;
	@XmlElement(name = "VendorId")
	private String vendorId;
	@XmlElement(name = "NroSeriePos")
	private String nroSeriePos;
	@XmlElement(name = "Nota")
	private String nota;
	@XmlElement(name = "DespachoDireccion")
	private String despachoDireccion;
	@XmlElement(name = "DespachoDistrito")
	private String despachoDistrito;
	@XmlElement(name = "DespachoFecha")
	private String despachoFecha;
	@XmlElement(name = "CodigoSucursal")
	private String codigoSucursal;
	@XmlElement(name = "SolesInDrawer")
	private String solesInDrawer;
	@XmlElement(name = "DolaresInDrawer")
	private String dolaresInDrawer;
	@XmlElement(name = "DolaresExchRate")
	private String dolaresExchRate;
	@XmlElement(name = "ElectronicSerialNumber")
	private String electronicSerialNumber;
	@XmlElement(name = "ElectronicCorrelative")
	private String electronicCorrelative;
	@XmlElement(name = "ElectronicFolio")
	private String electronicFolio;
	@XmlElement(name = "ElectronicDocumentType")
	private String electronicDocumentType;
	public String getOperatorID() {
		return operatorID;
	}
	public void setOperatorID(String operatorID) {
		this.operatorID = operatorID;
	}
	public String getBeginDateTime() {
		return beginDateTime;
	}
	public void setBeginDateTime(String beginDateTime) {
		this.beginDateTime = beginDateTime;
	}
	public String getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}
	public String getBusinessDayDate() {
		return businessDayDate;
	}
	public void setBusinessDayDate(String businessDayDate) {
		this.businessDayDate = businessDayDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCancelFlag() {
		return cancelFlag;
	}
	public void setCancelFlag(String cancelFlag) {
		this.cancelFlag = cancelFlag;
	}
	public String getTrainingMode() {
		return trainingMode;
	}
	public void setTrainingMode(String trainingMode) {
		this.trainingMode = trainingMode;
	}
	public String getSuspendFlag() {
		return suspendFlag;
	}
	public void setSuspendFlag(String suspendFlag) {
		this.suspendFlag = suspendFlag;
	}
	public String getTransactionTypeCode() {
		return transactionTypeCode;
	}
	public void setTransactionTypeCode(String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}
	public String getCorrelativoNumber() {
		return correlativoNumber;
	}
	public void setCorrelativoNumber(String correlativoNumber) {
		this.correlativoNumber = correlativoNumber;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getNroSeriePos() {
		return nroSeriePos;
	}
	public void setNroSeriePos(String nroSeriePos) {
		this.nroSeriePos = nroSeriePos;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getDespachoDireccion() {
		return despachoDireccion;
	}
	public void setDespachoDireccion(String despachoDireccion) {
		this.despachoDireccion = despachoDireccion;
	}
	public String getDespachoDistrito() {
		return despachoDistrito;
	}
	public void setDespachoDistrito(String despachoDistrito) {
		this.despachoDistrito = despachoDistrito;
	}
	public String getDespachoFecha() {
		return despachoFecha;
	}
	public void setDespachoFecha(String despachoFecha) {
		this.despachoFecha = despachoFecha;
	}
	public String getCodigoSucursal() {
		return codigoSucursal;
	}
	public void setCodigoSucursal(String codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}
	public String getSolesInDrawer() {
		return solesInDrawer;
	}
	public void setSolesInDrawer(String solesInDrawer) {
		this.solesInDrawer = solesInDrawer;
	}
	public String getDolaresInDrawer() {
		return dolaresInDrawer;
	}
	public void setDolaresInDrawer(String dolaresInDrawer) {
		this.dolaresInDrawer = dolaresInDrawer;
	}
	public String getDolaresExchRate() {
		return dolaresExchRate;
	}
	public void setDolaresExchRate(String dolaresExchRate) {
		this.dolaresExchRate = dolaresExchRate;
	}
	public String getElectronicSerialNumber() {
		return electronicSerialNumber;
	}
	public void setElectronicSerialNumber(String electronicSerialNumber) {
		this.electronicSerialNumber = electronicSerialNumber;
	}
	public String getElectronicCorrelative() {
		return electronicCorrelative;
	}
	public void setElectronicCorrelative(String electronicCorrelative) {
		this.electronicCorrelative = electronicCorrelative;
	}
	public String getElectronicFolio() {
		return electronicFolio;
	}
	public void setElectronicFolio(String electronicFolio) {
		this.electronicFolio = electronicFolio;
	}
	public String getElectronicDocumentType() {
		return electronicDocumentType;
	}
	public void setElectronicDocumentType(String electronicDocumentType) {
		this.electronicDocumentType = electronicDocumentType;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StartSequence [operatorID=").append(operatorID).append(", beginDateTime=").append(beginDateTime)
				.append(", endDateTime=").append(endDateTime).append(", businessDayDate=").append(businessDayDate)
				.append(", currencyCode=").append(currencyCode).append(", cancelFlag=").append(cancelFlag)
				.append(", trainingMode=").append(trainingMode).append(", suspendFlag=").append(suspendFlag)
				.append(", transactionTypeCode=").append(transactionTypeCode).append(", correlativoNumber=")
				.append(correlativoNumber).append(", cashierName=").append(cashierName).append(", vendorName=")
				.append(vendorName).append(", vendorId=").append(vendorId).append(", nroSeriePos=").append(nroSeriePos)
				.append(", nota=").append(nota).append(", despachoDireccion=").append(despachoDireccion)
				.append(", despachoDistrito=").append(despachoDistrito).append(", despachoFecha=").append(despachoFecha)
				.append(", codigoSucursal=").append(codigoSucursal).append(", solesInDrawer=").append(solesInDrawer)
				.append(", dolaresInDrawer=").append(dolaresInDrawer).append(", dolaresExchRate=")
				.append(dolaresExchRate).append(", electronicSerialNumber=").append(electronicSerialNumber)
				.append(", electronicCorrelative=").append(electronicCorrelative).append(", electronicFolio=")
				.append(electronicFolio).append(", electronicDocumentType=").append(electronicDocumentType).append("]");
		return builder.toString();
	}
	
}
