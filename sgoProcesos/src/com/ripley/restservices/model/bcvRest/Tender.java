package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Tender {

	@XmlElement(name = "TenderID")
	private String tenderID;
	@XmlElement(name = "AmountDue")
	private String amountDue;
	@XmlElement(name = "Amount")
	private String amount;
	@XmlElement(name = "AccountID")
	private String accountID;
	@XmlElement(name = "ApprovalReference")
	private String approvalReference;
	@XmlElement(name = "TenderInFlag")
	private String tenderInFlag;
	@XmlElement(name = "CardCreditDebit")
	private String cardCreditDebit;
	@XmlElement(name = "CardInstallments")
	private String cardInstallments;
	@XmlElement(name = "CardDeferred")
	private String cardDeferred;
	@XmlElement(name = "CardValorCuota")
	private String cardValorCuota;
	public String getTenderID() {
		return tenderID;
	}
	public void setTenderID(String tenderID) {
		this.tenderID = tenderID;
	}
	public String getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(String amountDue) {
		this.amountDue = amountDue;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public String getApprovalReference() {
		return approvalReference;
	}
	public void setApprovalReference(String approvalReference) {
		this.approvalReference = approvalReference;
	}
	public String getTenderInFlag() {
		return tenderInFlag;
	}
	public void setTenderInFlag(String tenderInFlag) {
		this.tenderInFlag = tenderInFlag;
	}
	public String getCardCreditDebit() {
		return cardCreditDebit;
	}
	public void setCardCreditDebit(String cardCreditDebit) {
		this.cardCreditDebit = cardCreditDebit;
	}
	public String getCardInstallments() {
		return cardInstallments;
	}
	public void setCardInstallments(String cardInstallments) {
		this.cardInstallments = cardInstallments;
	}
	public String getCardDeferred() {
		return cardDeferred;
	}
	public void setCardDeferred(String cardDeferred) {
		this.cardDeferred = cardDeferred;
	}
	public String getCardValorCuota() {
		return cardValorCuota;
	}
	public void setCardValorCuota(String cardValorCuota) {
		this.cardValorCuota = cardValorCuota;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tender [tenderID=").append(tenderID).append(", amountDue=").append(amountDue)
				.append(", amount=").append(amount).append(", accountID=").append(accountID)
				.append(", approvalReference=").append(approvalReference).append(", tenderInFlag=").append(tenderInFlag)
				.append(", cardCreditDebit=").append(cardCreditDebit).append(", cardInstallments=")
				.append(cardInstallments).append(", cardDeferred=").append(cardDeferred).append(", cardValorCuota=")
				.append(cardValorCuota).append("]");
		return builder.toString();
	}
	
}
