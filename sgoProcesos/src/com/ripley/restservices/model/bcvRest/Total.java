package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Total {

	@XmlElement(name = "TransactionAmount")
	private String transactionAmount;
	@XmlElement(name = "Amount")
	private String amount;
	@XmlElement(name = "ChangeAmount")
	private String changeAmount;
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getChangeAmount() {
		return changeAmount;
	}
	public void setChangeAmount(String changeAmount) {
		this.changeAmount = changeAmount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Total [transactionAmount=").append(transactionAmount).append(", amount=").append(amount)
				.append(", changeAmount=").append(changeAmount).append("]");
		return builder.toString();
	}
	
}
