package com.ripley.restservices.model.bcvRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionDiscount {

	@XmlElement(name = "TransactionDiscountReceiptTotal")
	private String transactionDiscountReceiptTotal;

	public String getTransactionDiscountReceiptTotal() {
		return transactionDiscountReceiptTotal;
	}

	public void setTransactionDiscountReceiptTotal(String transactionDiscountReceiptTotal) {
		this.transactionDiscountReceiptTotal = transactionDiscountReceiptTotal;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionDiscount [transactionDiscountReceiptTotal=").append(transactionDiscountReceiptTotal)
				.append("]");
		return builder.toString();
	}
	
}
