package com.ripley.restservices.model.bcvRest.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import cl.ripley.omnicanalidad.util.Constantes;

@XmlAccessorType(XmlAccessType.FIELD)
public class Route {

	@XmlElement
	private String system = Constantes.SYSTEM_XML_NAME;
	@XmlElement
	private String store = Constantes.STORE_XML_NAME;
	@XmlElement
	private String date;
	@XmlElement
	private String terminal;
	@XmlElement
	private String transaction;
	@XmlElement(name = "transaction-xml")
	private String transactionXml;
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getTransactionXml() {
		return transactionXml;
	}
	public void setTransactionXml(String transactionXml) {
		this.transactionXml = transactionXml;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Route [system=").append(system).append(", store=").append(store).append(", date=").append(date)
				.append(", terminal=").append(terminal).append(", transaction=").append(transaction)
				.append(", transactionXml=").append(transactionXml).append("]");
		return builder.toString();
	}
	
	
	
	
}
