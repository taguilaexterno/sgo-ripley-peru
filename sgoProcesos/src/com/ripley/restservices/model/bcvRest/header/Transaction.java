package com.ripley.restservices.model.bcvRest.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import cl.ripley.omnicanalidad.util.Constantes;

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {

	@XmlAttribute
	private String service = Constantes.TRX_XML_NAME;
	@XmlAttribute
	private String method = Constantes.METHOD_XML_NAME;
	@XmlAttribute
	private String date;
	@XmlAttribute
	private String hour;
	@XmlAttribute
	private String transaction;
	@XmlAttribute
	private String system = Constantes.SYSTEM_XML_NAME;
	@XmlAttribute
	private String store = Constantes.STORE_XML_NAME;
	@XmlAttribute
	private String terminal;
	@XmlAttribute
	private String checker = Constantes.CHECKER_XML_NAME;
	@XmlElement(name = "transaction-xml")
	private TransactionXml transactionXml;
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getChecker() {
		return checker;
	}
	public void setChecker(String checker) {
		this.checker = checker;
	}
	public TransactionXml getTransactionXml() {
		return transactionXml;
	}
	public void setTransactionXml(TransactionXml transactionXml) {
		this.transactionXml = transactionXml;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [service=").append(service).append(", method=").append(method).append(", date=")
				.append(date).append(", hour=").append(hour).append(", transaction=").append(transaction)
				.append(", system=").append(system).append(", store=").append(store).append(", terminal=")
				.append(terminal).append(", checker=").append(checker).append(", transactionXml=")
				.append(transactionXml).append("]");
		return builder.toString();
	}
	
	
	
}
