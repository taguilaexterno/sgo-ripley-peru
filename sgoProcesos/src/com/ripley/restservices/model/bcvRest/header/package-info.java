@javax.xml.bind.annotation.XmlSchema(
		xmlns = {
				@javax.xml.bind.annotation.XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
				@javax.xml.bind.annotation.XmlNs(prefix = "xsd", namespaceURI = "http://www.w3.org/2001/XMLSchema")
		},
		elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
		
		)
package com.ripley.restservices.model.bcvRest.header;