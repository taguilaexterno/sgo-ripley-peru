package com.ripley.restservices.model.bigticket;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LgbtckFunInaDbo {
	
	@JsonProperty("PCOD_TRX")
	private Long pcodTrx;
	@JsonProperty("PNRO_BOLETA")
	private Long pnroBoleta;
	@JsonProperty("PFECHA_VTA")
	private String pfechaVta;
	@JsonProperty("PSUCURSAL_VTA")
	private Integer psucursalVta;
	@JsonProperty("PESTADO")
	private Integer pestado;
	@JsonProperty("PRUT_CLIENTE")
	private String prutCliente;
	@JsonProperty("PRUT_DESP")
	private String prutDesp;
	@JsonProperty("PNOMBRE_DESP")
	private String pnombreDesp;
	@JsonProperty("PFECHA_DESP")
	private String pfechaDesp;
	@JsonProperty("PNRO_LOC_BOD")
	private Long pnroLocBod;
	@JsonProperty("PCUD")
	private String pcud;
	@JsonProperty("PRUT_VENDEDOR")
	private String prutVendedor;
	@JsonProperty("PCODIGO_VTA")
	private Long pcodigoVta;
	@JsonProperty("PCOD_COLOR")
	private String pcodColor;
	@JsonProperty("PCOD_COMUNA")
	private String pcodComuna;
	@JsonProperty("PCOD_REGION")
	private String pcodRegion;
	@JsonProperty("PCOD_REGALO")
	private Long pcodRegalo;
	@JsonProperty("PDIRECCION_DESP")
	private String pdireccionDesp;
	@JsonProperty("PJORNADA_DESP")
	private String pjornadaDesp;
	@JsonProperty("POBSERVACIONES")
	private String pobservaciones;
	@JsonProperty("PPRECIO")
	private BigDecimal pprecio;
	@JsonProperty("PCANTIDAD")
	private Integer pcantidad;
	@JsonProperty("PCUBICAJE")
	private Integer pcubicaje;
	@JsonProperty("PCTC_PAGE")
	private Integer pctcPage;
	@JsonProperty("PCTC_COORDINATE")
	private String pctcCoordinate;
	@JsonProperty("PTELEFONO")
	private String ptelefono;
	@JsonProperty("PMONTO_VTA")
	private BigDecimal pmontoVta;
	@JsonProperty("PNOMBRE_CLIENTE")
	private String pnombreCliente;
	@JsonProperty("PDIRECCION_CLIENTE")
	private String pdireccionCliente;
	@JsonProperty("PTELEFONO_CLIENTE")
	private String ptelefonoCliente;
	@JsonProperty("PTIPO_CLIENTE")
	private Integer ptipoCliente;
	@JsonProperty("PORIGEN_TRX")
	private String porigenTrx;
	@JsonProperty("PTIPO_VTA")
	private Integer ptipoVta;
	@JsonProperty("PEMAIL")
	private String pemail;
	@JsonProperty("WITIP_RGL")
	private Integer witipRgl;
	@JsonProperty("PCAJA")
	private Integer pCaja;
	@JsonProperty("PCOMERCIO")
	private Integer pcomercio;
	@JsonProperty("PVERSION_PGM")
	private String pversionPgm;
	@JsonProperty("PORDCMP")
	private Long pordcmp;
	@JsonProperty("PMEDIOPAGO")
	private String pmedioPago;
	@JsonProperty("PVALMEDIOPAGO")
	private String pvalmediopago;
	@JsonProperty("PFCHVALMEDIOPAGO")
	private String pfchvalmediopago;
	@JsonProperty("PFCHCREAOC")
	private String pfchcreaoc;
	@JsonProperty("PTIPO_DOCUMENTO")
	private Integer ptipoDocumento;
	@JsonProperty("PEMAILCIENTE")
	private String pemailcliente;
	@JsonProperty("PRZNSOCIAL")
	private String prznsocial;
	@JsonProperty("PMSJREGALO")
	private String pmsjregalo;
	@JsonProperty("POBSERVACIONES3")
	private String pobservaciones3;
	@JsonProperty("POBSERVACIONES4")
	private String pobservaciones4;
	
	
	public Long getPcodTrx() {
		return pcodTrx;
	}
	public void setPcodTrx(Long pcodTrx) {
		this.pcodTrx = pcodTrx;
	}
	public Long getPnroBoleta() {
		return pnroBoleta;
	}
	public void setPnroBoleta(Long pnroBoleta) {
		this.pnroBoleta = pnroBoleta;
	}
	public String getPfechaVta() {
		return pfechaVta;
	}
	public void setPfechaVta(String pfechaVta) {
		this.pfechaVta = pfechaVta;
	}
	public Integer getPsucursalVta() {
		return psucursalVta;
	}
	public void setPsucursalVta(Integer psucursalVta) {
		this.psucursalVta = psucursalVta;
	}
	public Integer getPestado() {
		return pestado;
	}
	public void setPestado(Integer pestado) {
		this.pestado = pestado;
	}
	public String getPrutCliente() {
		return prutCliente;
	}
	public void setPrutCliente(String prutCliente) {
		this.prutCliente = prutCliente;
	}
	public String getPrutDesp() {
		return prutDesp;
	}
	public void setPrutDesp(String prutDesp) {
		this.prutDesp = prutDesp;
	}
	public String getPnombreDesp() {
		return pnombreDesp;
	}
	public void setPnombreDesp(String pnombreDesp) {
		this.pnombreDesp = pnombreDesp;
	}
	public String getPfechaDesp() {
		return pfechaDesp;
	}
	public void setPfechaDesp(String pfechaDesp) {
		this.pfechaDesp = pfechaDesp;
	}
	public Long getPnroLocBod() {
		return pnroLocBod;
	}
	public void setPnroLocBod(Long pnroLocBod) {
		this.pnroLocBod = pnroLocBod;
	}
	public String getPcud() {
		return pcud;
	}
	public void setPcud(String pcud) {
		this.pcud = pcud;
	}
	public String getPrutVendedor() {
		return prutVendedor;
	}
	public void setPrutVendedor(String prutVendedor) {
		this.prutVendedor = prutVendedor;
	}
	public Long getPcodigoVta() {
		return pcodigoVta;
	}
	public void setPcodigoVta(Long pcodigoVta) {
		this.pcodigoVta = pcodigoVta;
	}
	public String getPcodColor() {
		return pcodColor;
	}
	public void setPcodColor(String pcodColor) {
		this.pcodColor = pcodColor;
	}
	public String getPcodComuna() {
		return pcodComuna;
	}
	public void setPcodComuna(String pcodComuna) {
		this.pcodComuna = pcodComuna;
	}
	public String getPcodRegion() {
		return pcodRegion;
	}
	public void setPcodRegion(String pcodRegion) {
		this.pcodRegion = pcodRegion;
	}
	public Long getPcodRegalo() {
		return pcodRegalo;
	}
	public void setPcodRegalo(Long pcodRegalo) {
		this.pcodRegalo = pcodRegalo;
	}
	public String getPdireccionDesp() {
		return pdireccionDesp;
	}
	public void setPdireccionDesp(String pdireccionDesp) {
		this.pdireccionDesp = pdireccionDesp;
	}
	public String getPjornadaDesp() {
		return pjornadaDesp;
	}
	public void setPjornadaDesp(String pjornadaDesp) {
		this.pjornadaDesp = pjornadaDesp;
	}
	public String getPobservaciones() {
		return pobservaciones;
	}
	public void setPobservaciones(String pobservaciones) {
		this.pobservaciones = pobservaciones;
	}
	public BigDecimal getPprecio() {
		return pprecio;
	}
	public void setPprecio(BigDecimal pprecio) {
		this.pprecio = pprecio;
	}
	public Integer getPcantidad() {
		return pcantidad;
	}
	public void setPcantidad(Integer pcantidad) {
		this.pcantidad = pcantidad;
	}
	public Integer getPcubicaje() {
		return pcubicaje;
	}
	public void setPcubicaje(Integer pcubicaje) {
		this.pcubicaje = pcubicaje;
	}
	public Integer getPctcPage() {
		return pctcPage;
	}
	public void setPctcPage(Integer pctcPage) {
		this.pctcPage = pctcPage;
	}
	public String getPctcCoordinate() {
		return pctcCoordinate;
	}
	public void setPctcCoordinate(String pctcCoordinate) {
		this.pctcCoordinate = pctcCoordinate;
	}
	public String getPtelefono() {
		return ptelefono;
	}
	public void setPtelefono(String ptelefono) {
		this.ptelefono = ptelefono;
	}
	public BigDecimal getPmontoVta() {
		return pmontoVta;
	}
	public void setPmontoVta(BigDecimal pmontoVta) {
		this.pmontoVta = pmontoVta;
	}
	public String getPnombreCliente() {
		return pnombreCliente;
	}
	public void setPnombreCliente(String pnombreCliente) {
		this.pnombreCliente = pnombreCliente;
	}
	public String getPdireccionCliente() {
		return pdireccionCliente;
	}
	public void setPdireccionCliente(String pdireccionCliente) {
		this.pdireccionCliente = pdireccionCliente;
	}
	public String getPtelefonoCliente() {
		return ptelefonoCliente;
	}
	public void setPtelefonoCliente(String ptelefonoCliente) {
		this.ptelefonoCliente = ptelefonoCliente;
	}
	public Integer getPtipoCliente() {
		return ptipoCliente;
	}
	public void setPtipoCliente(Integer ptipoCliente) {
		this.ptipoCliente = ptipoCliente;
	}
	public String getPorigenTrx() {
		return porigenTrx;
	}
	public void setPorigenTrx(String porigenTrx) {
		this.porigenTrx = porigenTrx;
	}
	public Integer getPtipoVta() {
		return ptipoVta;
	}
	public void setPtipoVta(Integer ptipoVta) {
		this.ptipoVta = ptipoVta;
	}
	public String getPemail() {
		return pemail;
	}
	public void setPemail(String pemail) {
		this.pemail = pemail;
	}
	public Integer getWitipRgl() {
		return witipRgl;
	}
	public void setWitipRgl(Integer witipRgl) {
		this.witipRgl = witipRgl;
	}
	public Integer getpCaja() {
		return pCaja;
	}
	public void setpCaja(Integer pCaja) {
		this.pCaja = pCaja;
	}
	public Integer getPcomercio() {
		return pcomercio;
	}
	public void setPcomercio(Integer pcomercio) {
		this.pcomercio = pcomercio;
	}
	public String getPversionPgm() {
		return pversionPgm;
	}
	public void setPversionPgm(String pversionPgm) {
		this.pversionPgm = pversionPgm;
	}
	public Long getPordcmp() {
		return pordcmp;
	}
	public void setPordcmp(Long pordcmp) {
		this.pordcmp = pordcmp;
	}
	public String getPmedioPago() {
		return pmedioPago;
	}
	public void setPmedioPago(String pmedioPago) {
		this.pmedioPago = pmedioPago;
	}
	public String getPvalmediopago() {
		return pvalmediopago;
	}
	public void setPvalmediopago(String pvalmediopago) {
		this.pvalmediopago = pvalmediopago;
	}
	public String getPfchvalmediopago() {
		return pfchvalmediopago;
	}
	public void setPfchvalmediopago(String pfchvalmediopago) {
		this.pfchvalmediopago = pfchvalmediopago;
	}
	public String getPfchcreaoc() {
		return pfchcreaoc;
	}
	public void setPfchcreaoc(String pfchcreaoc) {
		this.pfchcreaoc = pfchcreaoc;
	}
	public Integer getPtipoDocumento() {
		return ptipoDocumento;
	}
	public void setPtipoDocumento(Integer ptipoDocumento) {
		this.ptipoDocumento = ptipoDocumento;
	}
	public String getPemailcliente() {
		return pemailcliente;
	}
	public void setPemailcliente(String pemailcliente) {
		this.pemailcliente = pemailcliente;
	}
	public String getPrznsocial() {
		return prznsocial;
	}
	public void setPrznsocial(String prznsocial) {
		this.prznsocial = prznsocial;
	}
	public String getPmsjregalo() {
		return pmsjregalo;
	}
	public void setPmsjregalo(String pmsjregalo) {
		this.pmsjregalo = pmsjregalo;
	}
	public String getPobservaciones3() {
		return pobservaciones3;
	}
	public void setPobservaciones3(String pobservaciones3) {
		this.pobservaciones3 = pobservaciones3;
	}
	public String getPobservaciones4() {
		return pobservaciones4;
	}
	public void setPobservaciones4(String pobservaciones4) {
		this.pobservaciones4 = pobservaciones4;
	}
	
	
	

}
