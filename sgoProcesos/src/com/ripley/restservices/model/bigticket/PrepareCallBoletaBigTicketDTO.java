package com.ripley.restservices.model.bigticket;

import java.time.LocalDateTime;

import cl.ripley.omnicanalidad.bean.ArticuloVentaTramaDTE;
import cl.ripley.omnicanalidad.bean.TramaDTE;

public class PrepareCallBoletaBigTicketDTO {

	private TramaDTE inOrdenCompra;
	private Long nroTransaccion;
	private ArticuloVentaTramaDTE art;
	private LocalDateTime fechaBoleta;
	
	public TramaDTE getInOrdenCompra() {
		return inOrdenCompra;
	}
	public void setInOrdenCompra(TramaDTE inOrdenCompra) {
		this.inOrdenCompra = inOrdenCompra;
	}
	public Long getNroTransaccion() {
		return nroTransaccion;
	}
	public void setNroTransaccion(Long nroTransaccion) {
		this.nroTransaccion = nroTransaccion;
	}
	public ArticuloVentaTramaDTE getArt() {
		return art;
	}
	public void setArt(ArticuloVentaTramaDTE art) {
		this.art = art;
	}
	public LocalDateTime getFechaBoleta() {
		return fechaBoleta;
	}
	public void setFechaBoleta(LocalDateTime fechaBoleta) {
		this.fechaBoleta = fechaBoleta;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"inOrdenCompra\":\"").append(inOrdenCompra).append("\",\"nroTransaccion\":\"")
				.append(nroTransaccion).append("\",\"art\":\"").append(art).append("\",\"fechaBoleta\":\"")
				.append(fechaBoleta).append("\"}");
		return builder.toString();
	}
	
	
	
}
