package com.ripley.restservices.model.bigticket;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseBT {
	
	@JsonProperty("Codigo")
	private Integer codigo;
	@JsonProperty("Mensaje")
	private String mensaje;
	@JsonProperty("Data")
	private Object data;
	@JsonProperty("RESULT")
	private String result;
	
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"codigo\":\"").append(codigo).append("\",\"mensaje\":\"").append(mensaje)
				.append("\",\"data\":\"").append(data).append("\",\"result\":\"").append(result).append("\"}");
		return builder.toString();
	}
	
	

}
