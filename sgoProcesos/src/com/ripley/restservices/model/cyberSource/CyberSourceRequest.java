package com.ripley.restservices.model.cyberSource;

public class CyberSourceRequest {
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
    public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("CyberSourceRequest [content=").append(content).append("]");
            return builder.toString();
    }
}
