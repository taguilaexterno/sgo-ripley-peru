package com.ripley.restservices.model.cyberSource;

public class CyberSourceResponse {
	private String mensaje;
	private Integer codigo;
	//private String data;
	private Long oc;
	private String xml;
	private Integer items;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Long getOc() {
		return oc;
	}
	public void setOc(Long oc) {
		this.oc = oc;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	public Integer getItems() {
		return items;
	}
	public void setItems(Integer items) {
		this.items = items;
	}
}
