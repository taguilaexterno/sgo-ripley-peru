package com.ripley.restservices.model.generic;

/**Clase base para los requests de los servicios REST.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class GenericRequest {

	private Long oc;
	private Integer numCaja;
	
	public Long getOc() {
		return oc;
	}
	public void setOc(Long oc) {
		this.oc = oc;
	}
	public Integer getNumCaja() {
		return numCaja;
	}
	public void setNumCaja(Integer numCaja) {
		this.numCaja = numCaja;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GenericRequest [oc=").append(oc).append(", numCaja=").append(numCaja).append("]");
		return builder.toString();
	}
	
	
}
