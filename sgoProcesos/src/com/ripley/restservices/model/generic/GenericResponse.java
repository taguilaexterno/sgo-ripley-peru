package com.ripley.restservices.model.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**Clase base para las respuestas de los servicios REST
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericResponse {

	private String mensaje;
	private Integer codigo;
	private String data;
	
	public GenericResponse() {
		super();
		this.mensaje = "OK";
		this.codigo = 0;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"mensaje\":\"").append(mensaje).append("\",codigo\":\"").append(codigo)
				.append("\",data\":\"").append(data).append("}");
		return builder.toString();
	}
	
	
	
}
