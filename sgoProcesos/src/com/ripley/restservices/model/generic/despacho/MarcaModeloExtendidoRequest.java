package com.ripley.restservices.model.generic.despacho;

public class MarcaModeloExtendidoRequest {

	private MarcaModeloExtendidoRequestBody body;

	public MarcaModeloExtendidoRequestBody getBody() {
		return body;
	}

	public void setBody(MarcaModeloExtendidoRequestBody body) {
		this.body = body;
	}

}
