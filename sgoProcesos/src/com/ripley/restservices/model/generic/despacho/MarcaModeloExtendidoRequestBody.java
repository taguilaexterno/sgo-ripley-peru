package com.ripley.restservices.model.generic.despacho;

public class MarcaModeloExtendidoRequestBody {

	private String xml;

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	@Override
	public String toString() {
		return "MarcaModeloExtendidoRequestBody [xml=" + xml + "]";
	}
}
