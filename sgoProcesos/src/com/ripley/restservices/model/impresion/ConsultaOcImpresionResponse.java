package com.ripley.restservices.model.impresion;

import java.util.List;

import com.ripley.restservices.model.generic.GenericResponse;

/**Modelo response de obtención de OCs para impresión.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 19-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class ConsultaOcImpresionResponse extends GenericResponse {

	private List<Long> numerosOCs;

	public List<Long> getNumerosOCs() {
		return numerosOCs;
	}

	public void setNumerosOCs(List<Long> numerosOCs) {
		this.numerosOCs = numerosOCs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaOcImpresionResponse [numerosOCs=").append(numerosOCs).append(", getMensaje()=")
				.append(getMensaje()).append(", getCodigo()=").append(getCodigo()).append(", getData()=")
				.append(getData()).append("]");
		return builder.toString();
	}
	
	
	
}
