package com.ripley.restservices.model.impresion;

public class ImprimirOfflineRposRequest {

	private Integer jobJenkins;
	private Integer cantidadOCs;

	
	public Integer getJobJenkins() {
		return jobJenkins;
	}


	public void setJobJenkins(Integer jobJenkins) {
		this.jobJenkins = jobJenkins;
	}


	public Integer getCantidadOCs() {
		return cantidadOCs;
	}


	public void setCantidadOCs(Integer cantidadOCs) {
		this.cantidadOCs = cantidadOCs;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImprimirOfflineRposRequest [jobJenkins=").append(jobJenkins).append(", cantidadOCs=")
				.append(cantidadOCs).append("]");
		return builder.toString();
	}

}
