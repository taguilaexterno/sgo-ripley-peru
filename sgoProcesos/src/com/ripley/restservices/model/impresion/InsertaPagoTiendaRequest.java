package com.ripley.restservices.model.impresion;

import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.pagoTiendaRest.PagoEnTienda;

/**Modelo de request para procesar pago en tienda.
 * 
 * @author Boris Parra Luman [Aligare]
* @since 12-04-2018
*<br/><br/>
* Cambios:<br/>
* <ul>
* <li>Inicio</li>
* </ul>
 */

public class InsertaPagoTiendaRequest extends GenericRequest{
	
	private boolean isPagoTienda;
	private PagoEnTienda pagoEnTienda;
	private Integer sucursal;
	//Eliminar
	private String xml;
	

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public Integer getSucursal() {
		return sucursal;
	}

	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}

	public boolean isPagoTienda() {
		return isPagoTienda;
	}

	public void setPagoTienda(boolean isPagoTienda) {
		this.isPagoTienda = isPagoTienda;
	}

		
	public PagoEnTienda getPagoEnTienda() {
		return pagoEnTienda;
	}

	public void setPagoEnTienda(PagoEnTienda pagoEnTienda) {
		this.pagoEnTienda = pagoEnTienda;
	}

	@Override
	public String toString() {
		if(sucursal == 0){
			return "InsertaPagoTiendaRequest [isPagoTienda=" + isPagoTienda + ", pagoEnTienda=" + pagoEnTienda + "]";
		}else {
			return "InsertaPagoTiendaRequest [isPagoTienda=" + isPagoTienda + ", pagoEnTienda=" + pagoEnTienda + ", sucursal="+sucursal+"]";
		}
		
	}	
	

}
