package com.ripley.restservices.model.insertaModeloExtendido;

import com.ripley.restservices.model.generic.GenericRequest;

/**Modelo de request para insertar OC en Modelo Extendido.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class InsertaModeloExtendidoRequest extends GenericRequest {

	private String xml;

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaOcImpresionRequest [xml=").append(xml).append(", getOc()=").append(getOc())
				.append(", getNumCaja()=").append(getNumCaja()).append("]");
		return builder.toString();
	}
	
	
}
