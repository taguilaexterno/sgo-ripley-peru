package com.ripley.restservices.model.mail;

import com.ripley.restservices.model.generic.GenericRequest;

/**Modelo de request para obtener OCs para impresión.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 19-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class ConsultaOcMailRequest extends GenericRequest {

	private Integer cantidadOCs;
	private Integer jobUrlDoce;

	public Integer getCantidadOCs() {
		return cantidadOCs;
	}

	public void setCantidadOCs(Integer cantidadOCs) {
		this.cantidadOCs = cantidadOCs;
	}

	public Integer getJobUrlDoce() {
		return jobUrlDoce;
	}

	public void setJobUrlDoce(Integer jobUrlDoce) {
		this.jobUrlDoce = jobUrlDoce;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaOcMailRequest [cantidadOCs=").append(cantidadOCs).append(", jobUrlDoce=")
				.append(jobUrlDoce).append("]");
		return builder.toString();
	}
	
	
	
}
