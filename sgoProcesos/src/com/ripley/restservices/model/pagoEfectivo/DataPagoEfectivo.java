package com.ripley.restservices.model.pagoEfectivo;

import org.joda.time.DateTime;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DataPagoEfectivo {
	private String cip;
	private String currency;
	private Float amount; 
	private String paymentDate;
	private String transactionCode;
	
	public String getCip() {
		return cip;
	}
	public void setCip(String cip) {
		this.cip = cip;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	@Override
	public String toString() {
		//DecimalFormat decimalFormat = new DecimalFormat("#.00");
        //String numberAsString = decimalFormat.format(amount);
        String montoAsString;
        
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
        
        montoAsString = decimalFormat.format(amount);
        montoAsString = montoAsString.replaceAll(",", ".");
        //System.out.println ("montoAsString: " + montoAsString);        
        
	//	return "DataPagoEfectivo [cip=" + cip + ", currency=" + currency + ", amount=" + amount + "]";
		//return "{\"cip\":\"" + cip + "\",\"currency\":\"" + currency + "\",\"amount\":" + amount + "}";
		//return "{\"cip\":\"" + cip + "\",\"currency\":\"" + currency + "\",\"amount\":" + Float.toString(amount) + "}";
        return "{\"cip\":\"" + cip 
        		+ "\",\"currency\":\"" + currency 
        		+ "\",\"amount\":" + montoAsString 
        		+ ",\"paymentDate\":\"" + paymentDate 
        		+  "\",\"transactionCode\":\"" + transactionCode + "\"}";
        
	}
}
