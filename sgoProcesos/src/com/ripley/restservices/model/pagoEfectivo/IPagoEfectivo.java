package com.ripley.restservices.model.pagoEfectivo;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import com.ripley.restservices.model.generic.GenericResponse;

public interface IPagoEfectivo {
	public GenericResponse insertaPagoEfectivo (PagoEfectivo pagoEfectivo) throws Exception;
	public String encode(String key, String data);
	public String obtieneJson(PagoEfectivo pagoEfectivo);
	public String obtieneJson2(PagoEfectivo pagoEfectivo);
	public String hmacSha256Base64(String secretKey, String message) throws
	  NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException;
}
