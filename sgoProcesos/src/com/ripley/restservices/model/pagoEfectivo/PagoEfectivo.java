package com.ripley.restservices.model.pagoEfectivo;

//import com.ripley.restservices.model.generic.GenericRequest;

//public class PagoEfectivo extends GenericRequest {
public class PagoEfectivo {
	private String eventType;
	private String operationNumber;
	private DataPagoEfectivo data;
	//private PagoEfectivoBody body;
	
	
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	public DataPagoEfectivo getData() {
		return data;
	}
	public void setData(DataPagoEfectivo data) {
		this.data = data;
	}
	@Override
	public String toString() {
		//return "PagoEfectivo [eventType=" + eventType + ", operationNumber=" + operationNumber + ", data=" + data + "]";
		return "{\"eventType\":\"" + eventType + "\",\"operationNumber\":\"" + operationNumber + "\",\"data\":" + data + "}";
	}
	
}
