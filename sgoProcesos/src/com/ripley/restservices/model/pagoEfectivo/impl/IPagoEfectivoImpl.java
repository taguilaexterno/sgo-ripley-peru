package com.ripley.restservices.model.pagoEfectivo.impl;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ripley.dao.InsertaModeloExtendidoDAO;
import com.ripley.dao.dto.InsertaModelExtendDTO;
import com.ripley.dao.dto.InsertaPagoEfectivoDTO;
import com.ripley.dao.impl.InsertaModeloExtendidoDAOImpl;
import com.ripley.dao.util.Constantes;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.pagoEfectivo.IPagoEfectivo;
import com.ripley.restservices.model.pagoEfectivo.PagoEfectivo;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Util;

@Service
public class IPagoEfectivoImpl implements IPagoEfectivo {
	private static final AriLog LOG = new AriLog(IPagoEfectivoImpl.class, Constantes.CODIGO_APP, PlataformaType.JAVA);

	/*
	@Autowired
	@Qualifier("registraPagoEfectivo")
	private StoredProcedure registraPagoEfectivo;
	*/
	@Autowired
	private com.ripley.dao.PagoEfectivoDAO PagoEfectivoDAO;
	
	
	public GenericResponse insertaPagoEfectivo (PagoEfectivo pagoEfectivo) throws Exception {
		LOG.initTrace("insertaPagoEfectivo", "eventType, operationNumber, data", 
				new KeyLog("Tipo de Evento", String.valueOf(pagoEfectivo.getEventType())),
				new KeyLog("Número de Operación", String.valueOf(pagoEfectivo.getEventType())),
				new KeyLog("Data", String.valueOf(pagoEfectivo.getData())));
			
				
		GenericResponse resp = new GenericResponse();
		try {
			LOG.traceInfo("insertaPagoEfectivo", "Se inicia inserción de Pago Efectivo" );
			String cip = pagoEfectivo.getData().getCip();
			String moneda = pagoEfectivo.getData().getCurrency();
			Float monto_venta = pagoEfectivo.getData().getAmount();
			String estado_cip = pagoEfectivo.getEventType();
			String codigo_evento = pagoEfectivo.getOperationNumber();
			String fecha_pago = pagoEfectivo.getData().getPaymentDate();
			String codigo_transaccion = pagoEfectivo.getData().getTransactionCode();
			
			InsertaPagoEfectivoDTO dto = PagoEfectivoDAO.registraPagoEfectivo(cip, moneda, monto_venta, estado_cip, codigo_evento, fecha_pago, codigo_transaccion);
			
			//InsertaModelExtendDTO dto = insertaModeloExtendidoDAO.insertaModeloExtendido(xml, correlativoVenta);
			resp.setCodigo(dto.getCodigo());
			resp.setMensaje(dto.getMensaje());
		}catch (Exception e) {
			LOG.traceError("insertaPagoEfectivo", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("insertaPagoEfectivo", "Finalizado", "GenericResponse = " + String.valueOf(resp));
		return resp;
	}
	
	public String encode(String key, String data) {
		LOG.initTrace("encode", "key, data",
			new KeyLog("key", key),
			new KeyLog("data", data));
	    try {
	        Mac hmac = Mac.getInstance("HmacSHA256"); // OPTIONS= HmacSHA512, HmacSHA256, HmacSHA1, HmacMD5
	        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
	        hmac.init(secret_key);
	        return new String(Hex.encodeHex(hmac.doFinal(data.getBytes("UTF-8"))));
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public String hmacSha256Base64(String secretKey, String message) throws
	  NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
	   // Prepare hmac sha256 cipher algorithm with provided secretKey
	   Mac hmacSha256;
	   try {
	    hmacSha256 = Mac.getInstance("HmacSHA256");
	   } catch (NoSuchAlgorithmException nsae) {
	    hmacSha256 = Mac.getInstance("HMAC-SHA-256");
	   }
	   SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA256");
	   hmacSha256.init(secretKeySpec);
	   // Build and return signature
	   return Base64.getEncoder().encodeToString(hmacSha256.doFinal(message.getBytes("UTF-8")));
	  }

	public String obtieneJson(PagoEfectivo pagoEfectivo) {
		ObjectMapper om = new ObjectMapper();
		String json = Constantes.VACIO;
		try {
			json = om.writeValueAsString(pagoEfectivo);
		} catch (JsonProcessingException e) {
			LOG.traceError("error", e);
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("obtieneJson", Constantes.VACIO, "Respondiendo = " + json);
		return json;
	}
	
	public String obtieneJson2(PagoEfectivo pagoEfectivo) {
		String montoAsString;
		String cip = pagoEfectivo.getData().getCip();
		String moneda = pagoEfectivo.getData().getCurrency();
		Float monto_venta = pagoEfectivo.getData().getAmount();
		String estado_cip = pagoEfectivo.getEventType();
		String codigo_evento = pagoEfectivo.getOperationNumber();
		
		DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
        
        montoAsString = decimalFormat.format(monto_venta);
        montoAsString = montoAsString.replaceAll(",", ".");
        System.out.println ("montoAsString: " + montoAsString);
        //montoAsString = "1021.90";
        
		String retorno = "{\"eventType\":\"" + estado_cip + "\",\"operationNumber\":\"" + codigo_evento + "\",\"data\":{\"cip\":\"" + cip + "\",\"currency\":\"" + moneda + "\",\"amount\":" + montoAsString + "}}";
		// "{\"eventType\":\"cip.paid\",\"operationNumber\":\"691681\",\"data\":{\"cip\":\"2539089\",\"currency\":\"PEN\",\"amount\":1021.90}}";
		
		LOG.endTrace("obtieneJson2", Constantes.VACIO, "Respondiendo = " + retorno);
		return retorno;
	}
	
	

}
