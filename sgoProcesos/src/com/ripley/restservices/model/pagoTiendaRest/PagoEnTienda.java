package com.ripley.restservices.model.pagoTiendaRest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.math.BigDecimal;

public class PagoEnTienda implements Serializable{
	
	@JsonProperty("CORRELATIVO_VENTA")
	private Long correlativoVenta;
	@JsonProperty("FECHA_HORA_TRX")
	private String fechaHoraTrx;
	@JsonProperty("FECHA_TRX")
	private String fechaTrx;
	@JsonProperty("SUCURSAL")
	private Integer sucursal;
	@JsonProperty("NRO_CAJA")
	private Integer nroCaja;
	@JsonProperty("NRO_TRANSACCION")
	private Long nroTransaccion;
	@JsonProperty("NRO_DOCUMENTO")
	private Long nroDocumento;
	@JsonProperty("TIPO_TRX")
	private Integer tipoTrx;
	@JsonProperty("RECAUDADOR_NOMBRES")
	private String recaudadorNombres;
	@JsonProperty("RECAUDADOR_DNI")
	private String recaudadorDni;
	@JsonProperty("RECAUDADOR_CODIGO")
	private String recaudadorCodigo;
	@JsonProperty("GLOSA")
	private String glosa;
	@JsonProperty("MONTO_DESCUENTO_RIPLEY")
	private BigDecimal montoDestoRipley;
	@JsonProperty("PLAZO")
	private Integer plazo;
	@JsonProperty("DIFERIDO")
	private Integer diferido;
	@JsonProperty("VALOR_CUOTA")
	private BigDecimal valorCuota;
	@JsonProperty("FECHA_PRIMER_VENCTO")
	private String fechaPrimerVencio;
	@JsonProperty("PAN")
	private Long pan;
	@JsonProperty("TIPO_TARJETA")
	private Integer tipoTarjeta;
	@JsonProperty("TIPO_PASARELA")
	private String tipoPasarela;
	@JsonProperty("BIN_NUMBER")
	private String binNumber;
	@JsonProperty("ULTIMOS_DIGITOS")
	private String ultimosDigidos;
	@JsonProperty("ID_TRANSACCION_PAGO")
	private String idTrxPago;
	@JsonProperty("CODIGO_AUTORIZACION")
	private String codigoAutorizacion;
	@JsonProperty("TIPO_PAGO")
	private Integer tipoPago;

	
	
	public Long getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(Long correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public String getFechaHoraTrx() {
		return fechaHoraTrx;
	}
	public void setFechaHoraTrx(String fechaHoraTrx) {
		this.fechaHoraTrx = fechaHoraTrx;
	}
	public String getFechaTrx() {
		return fechaTrx;
	}
	public void setFechaTrx(String fechaTrx) {
		this.fechaTrx = fechaTrx;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	public Integer getNroCaja() {
		return nroCaja;
	}
	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}
	public Long getNroTransaccion() {
		return nroTransaccion;
	}
	public void setNroTransaccion(Long nroTransaccion) {
		this.nroTransaccion = nroTransaccion;
	}
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public Integer getTipoTrx() {
		return tipoTrx;
	}
	public void setTipoTrx(Integer tipoTrx) {
		this.tipoTrx = tipoTrx;
	}
	public String getRecaudadorNombres() {
		return recaudadorNombres;
	}
	public void setRecaudadorNombres(String recaudadorNombres) {
		this.recaudadorNombres = recaudadorNombres;
	}
	public String getRecaudadorDni() {
		return recaudadorDni;
	}
	public void setRecaudadorDni(String recaudadorDni) {
		this.recaudadorDni = recaudadorDni;
	}
	public String getRecaudadorCodigo() {
		return recaudadorCodigo;
	}
	public void setRecaudadorCodigo(String recaudadorCodigo) {
		this.recaudadorCodigo = recaudadorCodigo;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public BigDecimal getMontoDestoRipley() {
		return montoDestoRipley;
	}
	public void setMontoDestoRipley(BigDecimal montoDestoRipley) {
		this.montoDestoRipley = montoDestoRipley;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public Integer getDiferido() {
		return diferido;
	}
	public void setDiferido(Integer diferido) {
		this.diferido = diferido;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public String getFechaPrimerVencio() {
		return fechaPrimerVencio;
	}
	public void setFechaPrimerVencio(String fechaPrimerVencio) {
		this.fechaPrimerVencio = fechaPrimerVencio;
	}
	public Long getPan() {
		return pan;
	}
	public void setPan(Long pan) {
		this.pan = pan;
	}
	public Integer getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(Integer tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getTipoPasarela() {
		return tipoPasarela;
	}
	public void setTipoPasarela(String tipoPasarela) {
		this.tipoPasarela = tipoPasarela;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public String getUltimosDigidos() {
		return ultimosDigidos;
	}
	public void setUltimosDigidos(String ultimosDigidos) {
		this.ultimosDigidos = ultimosDigidos;
	}
	public String getIdTrxPago() {
		return idTrxPago;
	}
	public void setIdTrxPago(String idTrxPago) {
		this.idTrxPago = idTrxPago;
	}
	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public Integer getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(Integer tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	@Override
	public String toString() {
		return "PagoTienda [correlativoVenta=" + correlativoVenta + ", fechaHoraTrx=" + fechaHoraTrx + ", fechaTrx="
				+ fechaTrx + ", sucursal=" + sucursal + ", nroCaja=" + nroCaja + ", nroTransaccion=" + nroTransaccion
				+ ", nroDocumento=" + nroDocumento + ", tipoTrx=" + tipoTrx + ", recaudadorNombres=" + recaudadorNombres
				+ ", recaudadorDni=" + recaudadorDni + ", recaudadorCodigo=" + recaudadorCodigo + ", glosa=" + glosa
				+ ", montoDestoRipley=" + montoDestoRipley + ", plazo=" + plazo + ", diferido=" + diferido
				+ ", valorCuota=" + valorCuota + ", fechaPrimerVencio=" + fechaPrimerVencio + ", pan=" + pan
				+ ", tipoTarjeta=" + tipoTarjeta + ", tipoPasarela=" + tipoPasarela + ", binNumber=" + binNumber
				+ ", ultimosDigidos=" + ultimosDigidos + ", idTrxPago=" + idTrxPago + ", codigoAutorizacion="
				+ codigoAutorizacion + ", tipoPago=" + tipoPago + "]";
	}
	
	

}
