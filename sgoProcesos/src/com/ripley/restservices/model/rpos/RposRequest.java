package com.ripley.restservices.model.rpos;

public class RposRequest {
	private Long oc;
	private Integer numCaja;
	private Integer sucursal;
	
	public Long getOc() {
		return oc;
	}
	public void setOc(Long oc) {
		this.oc = oc;
	}
	public Integer getNumCaja() {
		return numCaja;
	}
	public void setNumCaja(Integer numCaja) {
		this.numCaja = numCaja;
	}
	public Integer getSucursal() {
		return sucursal;
	}
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RposRequest [oc=").append(oc).append(", numCaja=").append(numCaja).append(", sucursal=").append(sucursal).append("]");
		return builder.toString();
	}
	
	
}
