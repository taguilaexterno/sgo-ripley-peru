package com.ripley.restservices.model.testparametros;

import com.ripley.dao.dto.Parametros;
import com.ripley.restservices.model.generic.GenericResponse;

public class TestParametrosResponse extends GenericResponse {

	private Parametros parametros;

	public Parametros getParametros() {
		return parametros;
	}

	public void setParametros(Parametros parametros) {
		this.parametros = parametros;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TestParametrosResponse [parametros=").append(parametros).append(", getMensaje()=")
				.append(getMensaje()).append(", getCodigo()=").append(getCodigo()).append(", getData()=")
				.append(getData()).append("]");
		return builder.toString();
	}
	
	
	
}
