package com.ripley.restservices.model.tracking;

public class TrackingRequest {

	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" {\"data\":\"").append(data).append("}");
		return builder.toString();
	}
	
	
}
