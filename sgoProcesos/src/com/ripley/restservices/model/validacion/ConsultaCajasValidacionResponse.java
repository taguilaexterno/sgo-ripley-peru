package com.ripley.restservices.model.validacion;

import java.util.List;

import com.ripley.restservices.model.generic.GenericResponse;

/**Modelo response de obtención de OCs para impresión.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 19-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class ConsultaCajasValidacionResponse extends GenericResponse {

	private List<Integer> cajas;

	public List<Integer> getCajas() {
		return cajas;
	}

	public void setCajas(List<Integer> cajas) {
		this.cajas = cajas;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaCajasValidacionResponse [cajas=").append(cajas).append(", getMensaje()=")
				.append(getMensaje()).append(", getCodigo()=").append(getCodigo()).append(", getData()=")
				.append(getData()).append("]");
		return builder.toString();
	}

	
	
	
	
}
