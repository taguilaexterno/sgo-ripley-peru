package com.ripley.restservices.model.validacion;

import com.ripley.restservices.model.generic.GenericRequest;

/**Modelo de request para obtener OCs para impresión.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 19-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
public class ConsultaOcValidacionRequest extends GenericRequest {

	private Integer cantidadOCs;
	private Integer estado;

	public Integer getCantidadOCs() {
		return cantidadOCs;
	}

	public void setCantidadOCs(Integer cantidadOCs) {
		this.cantidadOCs = cantidadOCs;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaOcValidacionRequest [cantidadOCs=").append(cantidadOCs).append(", estado=")
				.append(estado).append(", getOc()=").append(getOc()).append(", getNumCaja()=").append(getNumCaja())
				.append("]");
		return builder.toString();
	}
	
	
	
}
