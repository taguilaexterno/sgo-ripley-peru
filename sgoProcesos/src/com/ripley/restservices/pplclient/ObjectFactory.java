
package com.ripley.restservices.pplclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.paperless.core.asp.online.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultaInformeReversionesRuc_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "ruc");
    private final static QName _ConsultaInformeReversionesLogin_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "login");
    private final static QName _ConsultaInformeReversionesClave_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "clave");
    private final static QName _ConsultaInformeReversionesNumTicketSunat_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "numTicketSunat");
    private final static QName _ConsultaInformeReversionesResponseReturn_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "return");
    private final static QName _GenInformeReversionesTramaTxt_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "tramaTxt");
    private final static QName _GenInformeReversionesPeriodo_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "periodo");
    private final static QName _OnlineGenerationDocTxt_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "docTxt");
    private final static QName _OnlineRecoveryFolio_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "folio");
    private final static QName _OnlineConsultaEstadoRucReceptor_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "rucReceptor");
    private final static QName _OnlineConsultaEstadoFechaInicio_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "fechaInicio");
    private final static QName _OnlineConsultaEstadoFechaFin_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "fechaFin");
    private final static QName _AddDocInfoInfoAdicional_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "infoAdicional");
    private final static QName _ListarAdjuntosRecibidosRucEmisor_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "rucEmisor");
    private final static QName _OnlineRecoveryRecListFecha_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "fecha");
    private final static QName _OnlineARCMensaje_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "mensaje");
    private final static QName _OnlineARCArgs8_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "args8");
    private final static QName _InformeResBoletasDiarioFechaInforme_QNAME = new QName("http://ws.online.asp.core.paperless.cl", "fechaInforme");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.paperless.core.asp.online.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaInformeReversiones }
     * 
     */
    public ConsultaInformeReversiones createConsultaInformeReversiones() {
        return new ConsultaInformeReversiones();
    }

    /**
     * Create an instance of {@link ConsultaInformeReversionesResponse }
     * 
     */
    public ConsultaInformeReversionesResponse createConsultaInformeReversionesResponse() {
        return new ConsultaInformeReversionesResponse();
    }

    /**
     * Create an instance of {@link GenInformeReversiones }
     * 
     */
    public GenInformeReversiones createGenInformeReversiones() {
        return new GenInformeReversiones();
    }

    /**
     * Create an instance of {@link GenInformeReversionesResponse }
     * 
     */
    public GenInformeReversionesResponse createGenInformeReversionesResponse() {
        return new GenInformeReversionesResponse();
    }

    /**
     * Create an instance of {@link OnlineGeneration }
     * 
     */
    public OnlineGeneration createOnlineGeneration() {
        return new OnlineGeneration();
    }

    /**
     * Create an instance of {@link OnlineGenerationResponse }
     * 
     */
    public OnlineGenerationResponse createOnlineGenerationResponse() {
        return new OnlineGenerationResponse();
    }

    /**
     * Create an instance of {@link OnlineRecovery }
     * 
     */
    public OnlineRecovery createOnlineRecovery() {
        return new OnlineRecovery();
    }

    /**
     * Create an instance of {@link OnlineRecoveryResponse }
     * 
     */
    public OnlineRecoveryResponse createOnlineRecoveryResponse() {
        return new OnlineRecoveryResponse();
    }

    /**
     * Create an instance of {@link OnlineEventos }
     * 
     */
    public OnlineEventos createOnlineEventos() {
        return new OnlineEventos();
    }

    /**
     * Create an instance of {@link OnlineEventosResponse }
     * 
     */
    public OnlineEventosResponse createOnlineEventosResponse() {
        return new OnlineEventosResponse();
    }

    /**
     * Create an instance of {@link OnlineConsultaEstado }
     * 
     */
    public OnlineConsultaEstado createOnlineConsultaEstado() {
        return new OnlineConsultaEstado();
    }

    /**
     * Create an instance of {@link OnlineConsultaEstadoResponse }
     * 
     */
    public OnlineConsultaEstadoResponse createOnlineConsultaEstadoResponse() {
        return new OnlineConsultaEstadoResponse();
    }

    /**
     * Create an instance of {@link AddDocInfo }
     * 
     */
    public AddDocInfo createAddDocInfo() {
        return new AddDocInfo();
    }

    /**
     * Create an instance of {@link AddDocInfoResponse }
     * 
     */
    public AddDocInfoResponse createAddDocInfoResponse() {
        return new AddDocInfoResponse();
    }

    /**
     * Create an instance of {@link ListarAdjuntosRecibidos }
     * 
     */
    public ListarAdjuntosRecibidos createListarAdjuntosRecibidos() {
        return new ListarAdjuntosRecibidos();
    }

    /**
     * Create an instance of {@link ListarAdjuntosRecibidosResponse }
     * 
     */
    public ListarAdjuntosRecibidosResponse createListarAdjuntosRecibidosResponse() {
        return new ListarAdjuntosRecibidosResponse();
    }

    /**
     * Create an instance of {@link OnlineRecoveryRec }
     * 
     */
    public OnlineRecoveryRec createOnlineRecoveryRec() {
        return new OnlineRecoveryRec();
    }

    /**
     * Create an instance of {@link OnlineRecoveryRecResponse }
     * 
     */
    public OnlineRecoveryRecResponse createOnlineRecoveryRecResponse() {
        return new OnlineRecoveryRecResponse();
    }

    /**
     * Create an instance of {@link OnlineRecoveryRecList }
     * 
     */
    public OnlineRecoveryRecList createOnlineRecoveryRecList() {
        return new OnlineRecoveryRecList();
    }

    /**
     * Create an instance of {@link OnlineRecoveryRecListResponse }
     * 
     */
    public OnlineRecoveryRecListResponse createOnlineRecoveryRecListResponse() {
        return new OnlineRecoveryRecListResponse();
    }

    /**
     * Create an instance of {@link OnlineARC }
     * 
     */
    public OnlineARC createOnlineARC() {
        return new OnlineARC();
    }

    /**
     * Create an instance of {@link OnlineARCResponse }
     * 
     */
    public OnlineARCResponse createOnlineARCResponse() {
        return new OnlineARCResponse();
    }

    /**
     * Create an instance of {@link InformeResBoletasDiario }
     * 
     */
    public InformeResBoletasDiario createInformeResBoletasDiario() {
        return new InformeResBoletasDiario();
    }

    /**
     * Create an instance of {@link InformeResBoletasDiarioResponse }
     * 
     */
    public InformeResBoletasDiarioResponse createInformeResBoletasDiarioResponse() {
        return new InformeResBoletasDiarioResponse();
    }

    /**
     * Create an instance of {@link CargaInformeBajas }
     * 
     */
    public CargaInformeBajas createCargaInformeBajas() {
        return new CargaInformeBajas();
    }

    /**
     * Create an instance of {@link CargaInformeBajasResponse }
     * 
     */
    public CargaInformeBajasResponse createCargaInformeBajasResponse() {
        return new CargaInformeBajasResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = ConsultaInformeReversiones.class)
    public JAXBElement<String> createConsultaInformeReversionesRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, ConsultaInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = ConsultaInformeReversiones.class)
    public JAXBElement<String> createConsultaInformeReversionesLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, ConsultaInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = ConsultaInformeReversiones.class)
    public JAXBElement<String> createConsultaInformeReversionesClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, ConsultaInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "numTicketSunat", scope = ConsultaInformeReversiones.class)
    public JAXBElement<String> createConsultaInformeReversionesNumTicketSunat(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesNumTicketSunat_QNAME, String.class, ConsultaInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = ConsultaInformeReversionesResponse.class)
    public JAXBElement<String> createConsultaInformeReversionesResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, ConsultaInformeReversionesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = GenInformeReversiones.class)
    public JAXBElement<String> createGenInformeReversionesRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, GenInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = GenInformeReversiones.class)
    public JAXBElement<String> createGenInformeReversionesLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, GenInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = GenInformeReversiones.class)
    public JAXBElement<String> createGenInformeReversionesClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, GenInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "tramaTxt", scope = GenInformeReversiones.class)
    public JAXBElement<String> createGenInformeReversionesTramaTxt(String value) {
        return new JAXBElement<String>(_GenInformeReversionesTramaTxt_QNAME, String.class, GenInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "periodo", scope = GenInformeReversiones.class)
    public JAXBElement<String> createGenInformeReversionesPeriodo(String value) {
        return new JAXBElement<String>(_GenInformeReversionesPeriodo_QNAME, String.class, GenInformeReversiones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = GenInformeReversionesResponse.class)
    public JAXBElement<String> createGenInformeReversionesResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, GenInformeReversionesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineGeneration.class)
    public JAXBElement<String> createOnlineGenerationRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineGeneration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineGeneration.class)
    public JAXBElement<String> createOnlineGenerationLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineGeneration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineGeneration.class)
    public JAXBElement<String> createOnlineGenerationClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineGeneration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "docTxt", scope = OnlineGeneration.class)
    public JAXBElement<String> createOnlineGenerationDocTxt(String value) {
        return new JAXBElement<String>(_OnlineGenerationDocTxt_QNAME, String.class, OnlineGeneration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineGenerationResponse.class)
    public JAXBElement<String> createOnlineGenerationResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineGenerationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineRecovery.class)
    public JAXBElement<String> createOnlineRecoveryRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineRecovery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineRecovery.class)
    public JAXBElement<String> createOnlineRecoveryLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineRecovery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineRecovery.class)
    public JAXBElement<String> createOnlineRecoveryClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineRecovery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "folio", scope = OnlineRecovery.class)
    public JAXBElement<String> createOnlineRecoveryFolio(String value) {
        return new JAXBElement<String>(_OnlineRecoveryFolio_QNAME, String.class, OnlineRecovery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineRecoveryResponse.class)
    public JAXBElement<String> createOnlineRecoveryResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineRecoveryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineEventos.class)
    public JAXBElement<String> createOnlineEventosRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineEventos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineEventos.class)
    public JAXBElement<String> createOnlineEventosLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineEventos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineEventos.class)
    public JAXBElement<String> createOnlineEventosClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineEventos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "docTxt", scope = OnlineEventos.class)
    public JAXBElement<String> createOnlineEventosDocTxt(String value) {
        return new JAXBElement<String>(_OnlineGenerationDocTxt_QNAME, String.class, OnlineEventos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineEventosResponse.class)
    public JAXBElement<String> createOnlineEventosResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineEventosResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "rucReceptor", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoRucReceptor(String value) {
        return new JAXBElement<String>(_OnlineConsultaEstadoRucReceptor_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "fechaInicio", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoFechaInicio(String value) {
        return new JAXBElement<String>(_OnlineConsultaEstadoFechaInicio_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "fechaFin", scope = OnlineConsultaEstado.class)
    public JAXBElement<String> createOnlineConsultaEstadoFechaFin(String value) {
        return new JAXBElement<String>(_OnlineConsultaEstadoFechaFin_QNAME, String.class, OnlineConsultaEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineConsultaEstadoResponse.class)
    public JAXBElement<String> createOnlineConsultaEstadoResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineConsultaEstadoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = AddDocInfo.class)
    public JAXBElement<String> createAddDocInfoRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, AddDocInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = AddDocInfo.class)
    public JAXBElement<String> createAddDocInfoLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, AddDocInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = AddDocInfo.class)
    public JAXBElement<String> createAddDocInfoClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, AddDocInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "folio", scope = AddDocInfo.class)
    public JAXBElement<String> createAddDocInfoFolio(String value) {
        return new JAXBElement<String>(_OnlineRecoveryFolio_QNAME, String.class, AddDocInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "infoAdicional", scope = AddDocInfo.class)
    public JAXBElement<String> createAddDocInfoInfoAdicional(String value) {
        return new JAXBElement<String>(_AddDocInfoInfoAdicional_QNAME, String.class, AddDocInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = AddDocInfoResponse.class)
    public JAXBElement<String> createAddDocInfoResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, AddDocInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = ListarAdjuntosRecibidos.class)
    public JAXBElement<String> createListarAdjuntosRecibidosRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, ListarAdjuntosRecibidos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = ListarAdjuntosRecibidos.class)
    public JAXBElement<String> createListarAdjuntosRecibidosLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, ListarAdjuntosRecibidos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = ListarAdjuntosRecibidos.class)
    public JAXBElement<String> createListarAdjuntosRecibidosClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, ListarAdjuntosRecibidos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "rucEmisor", scope = ListarAdjuntosRecibidos.class)
    public JAXBElement<String> createListarAdjuntosRecibidosRucEmisor(String value) {
        return new JAXBElement<String>(_ListarAdjuntosRecibidosRucEmisor_QNAME, String.class, ListarAdjuntosRecibidos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "folio", scope = ListarAdjuntosRecibidos.class)
    public JAXBElement<String> createListarAdjuntosRecibidosFolio(String value) {
        return new JAXBElement<String>(_OnlineRecoveryFolio_QNAME, String.class, ListarAdjuntosRecibidos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = ListarAdjuntosRecibidosResponse.class)
    public JAXBElement<String> createListarAdjuntosRecibidosResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, ListarAdjuntosRecibidosResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineRecoveryRec.class)
    public JAXBElement<String> createOnlineRecoveryRecRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineRecoveryRec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineRecoveryRec.class)
    public JAXBElement<String> createOnlineRecoveryRecLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineRecoveryRec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineRecoveryRec.class)
    public JAXBElement<String> createOnlineRecoveryRecClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineRecoveryRec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "rucEmisor", scope = OnlineRecoveryRec.class)
    public JAXBElement<String> createOnlineRecoveryRecRucEmisor(String value) {
        return new JAXBElement<String>(_ListarAdjuntosRecibidosRucEmisor_QNAME, String.class, OnlineRecoveryRec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "folio", scope = OnlineRecoveryRec.class)
    public JAXBElement<String> createOnlineRecoveryRecFolio(String value) {
        return new JAXBElement<String>(_OnlineRecoveryFolio_QNAME, String.class, OnlineRecoveryRec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineRecoveryRecResponse.class)
    public JAXBElement<String> createOnlineRecoveryRecResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineRecoveryRecResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineRecoveryRecList.class)
    public JAXBElement<String> createOnlineRecoveryRecListRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineRecoveryRecList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineRecoveryRecList.class)
    public JAXBElement<String> createOnlineRecoveryRecListLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineRecoveryRecList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineRecoveryRecList.class)
    public JAXBElement<String> createOnlineRecoveryRecListClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineRecoveryRecList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "fecha", scope = OnlineRecoveryRecList.class)
    public JAXBElement<String> createOnlineRecoveryRecListFecha(String value) {
        return new JAXBElement<String>(_OnlineRecoveryRecListFecha_QNAME, String.class, OnlineRecoveryRecList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineRecoveryRecListResponse.class)
    public JAXBElement<String> createOnlineRecoveryRecListResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineRecoveryRecListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "rucEmisor", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCRucEmisor(String value) {
        return new JAXBElement<String>(_ListarAdjuntosRecibidosRucEmisor_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "folio", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCFolio(String value) {
        return new JAXBElement<String>(_OnlineRecoveryFolio_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "mensaje", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCMensaje(String value) {
        return new JAXBElement<String>(_OnlineARCMensaje_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "args8", scope = OnlineARC.class)
    public JAXBElement<String> createOnlineARCArgs8(String value) {
        return new JAXBElement<String>(_OnlineARCArgs8_QNAME, String.class, OnlineARC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = OnlineARCResponse.class)
    public JAXBElement<String> createOnlineARCResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, OnlineARCResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = InformeResBoletasDiario.class)
    public JAXBElement<String> createInformeResBoletasDiarioRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, InformeResBoletasDiario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = InformeResBoletasDiario.class)
    public JAXBElement<String> createInformeResBoletasDiarioLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, InformeResBoletasDiario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = InformeResBoletasDiario.class)
    public JAXBElement<String> createInformeResBoletasDiarioClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, InformeResBoletasDiario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "fechaInforme", scope = InformeResBoletasDiario.class)
    public JAXBElement<String> createInformeResBoletasDiarioFechaInforme(String value) {
        return new JAXBElement<String>(_InformeResBoletasDiarioFechaInforme_QNAME, String.class, InformeResBoletasDiario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = InformeResBoletasDiarioResponse.class)
    public JAXBElement<String> createInformeResBoletasDiarioResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, InformeResBoletasDiarioResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "ruc", scope = CargaInformeBajas.class)
    public JAXBElement<String> createCargaInformeBajasRuc(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesRuc_QNAME, String.class, CargaInformeBajas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "login", scope = CargaInformeBajas.class)
    public JAXBElement<String> createCargaInformeBajasLogin(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesLogin_QNAME, String.class, CargaInformeBajas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "clave", scope = CargaInformeBajas.class)
    public JAXBElement<String> createCargaInformeBajasClave(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesClave_QNAME, String.class, CargaInformeBajas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "docTxt", scope = CargaInformeBajas.class)
    public JAXBElement<String> createCargaInformeBajasDocTxt(String value) {
        return new JAXBElement<String>(_OnlineGenerationDocTxt_QNAME, String.class, CargaInformeBajas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "periodo", scope = CargaInformeBajas.class)
    public JAXBElement<String> createCargaInformeBajasPeriodo(String value) {
        return new JAXBElement<String>(_GenInformeReversionesPeriodo_QNAME, String.class, CargaInformeBajas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.online.asp.core.paperless.cl", name = "return", scope = CargaInformeBajasResponse.class)
    public JAXBElement<String> createCargaInformeBajasResponseReturn(String value) {
        return new JAXBElement<String>(_ConsultaInformeReversionesResponseReturn_QNAME, String.class, CargaInformeBajasResponse.class, value);
    }

}
