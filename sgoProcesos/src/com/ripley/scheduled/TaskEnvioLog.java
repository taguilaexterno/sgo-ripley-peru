package com.ripley.scheduled;

import java.io.IOException;
import java.net.Inet4Address;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ripley.enviolog.EnvioSftp;
import com.ripley.enviolog.tar;
import com.ripley.restservices.logic.ISunatService;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

//import com.ripley.enviomail.App;
//import com.ripley.util.tar;

import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
public class TaskEnvioLog {
	private static final AriLog LOG = new AriLog(TaskEnvioLog.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
//	@Autowired
//	private static ParametrosDAO parametroDao;
//	private ParametrosDAO parametroDao;
	
	@Autowired
	private ParametrosDAO parametroDao;
	
	/*
	 *  cron=<second> <minute> <hour> <day-of-month> <month> <day-of-week> <year> <command>
	 *  @Scheduled(cron = "0 0 3 * * ?")
	 *  @Scheduled(cron ="0/10 * 8,9,10,11,12,13,14,15,16,17 * * ?")
	 **/
//	@Scheduled(cron ="*/5 * * * * ?")
//	@Scheduled(cron ="0 0/10 3,4,5,6 * * ?")
//	@Scheduled(cron ="0 0/5 * * * ?")	// Cada 5 minutos
//	@Scheduled(cron ="0 0/2 * * * ?")	// Cada 2 minutos
	
//	@Scheduled(cron ="0 0/10 3 * * ?")	// Cada 10 minutos a las 3
	@Scheduled(cron ="0 0/10 * * * ?")	// Cada 10 minutos
	public void envioLOG () throws IOException{
//	public static void envioLOG () throws IOException{
		String input = null;
        String ruta = null;
        String host = null;
        String user = null;
        String pass = null;
        Integer port = null;
        String ambiente = null;
      //String HostName2 = null;
        String HostName = null;
        String sNombre;
        String output;
        String filtro;
        
        LOG.initTrace("envioLOG", "[INI] Inicio del Proceso [envioLOG]");
        
        //Date date = new Date();
    	//DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	
        //HostName2 = Inet4Address.getLocalHost().getHostName();
    	HostName = Inet4Address.getLocalHost().getHostAddress();
    	//java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
    	//System.out.println("Hostname of local machine: " + localMachine.getHostName());
    	/*
    	sNombre = hourdateFormat.format(date);
    	sNombre = sNombre.replace("-", "");
    	sNombre = sNombre.replace(":", "");
    	sNombre = sNombre.replace(" ", "_");
    	*/
    	
    	//HostName = localMachine.getHostName();
    	if(HostName != null && !HostName.trim().isEmpty()) {
    		HostName = "_" + HostName.replace(".", "_");
    		//HostName = "_" + HostName;
    	} else {
    		HostName = "";
    	}
    	LOG.initTrace("envioLOG", "[INF] Hostname: " + HostName);
		
    	/*
    	sNombre = localMachine.getAddress();
    	if(sNombre = null || sNombre.isEmpty()) {
    		sNombre = "";
    	}
    	*/
    	
    	Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());
		parametroDao.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(),  Constantes.STRING_UNO);
		
		host = parametros.buscaParametroPorNombre("ENVIOLOG_HOST").getValor();
		user = parametros.buscaParametroPorNombre("ENVIOLOG_USER").getValor();
		pass = parametros.buscaParametroPorNombre("ENVIOLOG_PASS").getValor();
		port = Integer.parseInt(parametros.buscaParametroPorNombre("ENVIOLOG_PORT").getValor());
		input = parametros.buscaParametroPorNombre("ENVIOLOG_CARPETA").getValor();
		ambiente = parametros.buscaParametroPorNombre("ENVIOLOG_AMBIENTE").getValor();
		
		if(ambiente != null && !ambiente.trim().isEmpty()) {
    		ambiente = "_" + ambiente;
    	} else {
    		ambiente = "";
    	}
    	
    	sNombre = "Catalina" + "_Log" + ambiente + HostName + ".tar.gz";
    	
    	LOG.traceInfo("envioLOG", "[INF] Archivo a Generar: " + sNombre);
		LOG.traceInfo("envioLOG", "[INF] Parámetros: HOST [" + host + "], CARPETA [" + input + "], AMBIENTE [" + ambiente + "]");
		
    	/*
    	host = "ec2-52-67-147-12.sa-east-1.compute.amazonaws.com";
		user = "ftplog";
		pass = "ftplog";
		port = 22;
		input = "D:\\temp\\extras\\"; // "/extras/";
		ruta = "/extras/";
		//output = ruta + sNombre;
		*/
		
    	output = input + sNombre;
    	filtro = "catalina.out";
    	//tar.CreateTarGZ(input, output, filtro);
    	tar.CreateZIP(input, output, filtro);
    	EnvioSftp.envio02(host, user, pass, port, output);
    	tar.EliminaFile(output);
    	LOG.endTrace("envioLOG", "[FIN] Fin del Proceso [envioLOG] OK", "");
//    	System.out.println("[FIN] Fin del Proceso OK.");
	}
}

/*
public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
    
	public void run(ApplicationArguments args) throws Exception {
//    	/var/log/tomcat8/
    	String output;
    	String sNombre;
    	String vacio = "";
    	int iEstado = 0;
    	
    	List<String> argsInput = null;
    	List<String> argsRuta = null;
    	List<String> argsHost = null;
    	List<String> argsUser = null;
    	List<String> argsPass = null;
    	List<String> argsPort = null;
        String input = null;				// String input = "extras/Origen/";
        String ruta = null;					// String ruta = "extras/";  // "extras/Destino/";
        String host = null;
        String user = null;
        String pass = null;
        Integer port = null;
        
        System.out.println("[INI] Inicio del Proceso.");
        
        Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	sNombre = hourdateFormat.format(date);
    	sNombre = sNombre.replace("-", "");
    	sNombre = sNombre.replace(":", "");
    	sNombre = sNombre.replace(" ", "_");
    	sNombre = "LogCatalina_" + sNombre + ".tar.gz";
    	
    	argsHost = args.getOptionValues("host");
        if (argsHost != null && !argsHost.isEmpty()) {
        	host = argsHost.get(0).trim(); //.toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [host].");
        }
        
        argsUser = args.getOptionValues("user");
        if (argsUser != null && !argsUser.isEmpty()) {
        	user = argsUser.get(0).trim(); //.toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [user].");
        }
    	
        argsPass = args.getOptionValues("pass");
        if (argsPass != null && !argsPass.isEmpty()) {
        	pass = argsPass.get(0).trim(); // .toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [pass].");
        }
        
        argsPort = args.getOptionValues("port");
        if (argsPort != null && !argsPort.isEmpty()) {
        	//port = argsPort.get(0).trim(); // .toLowerCase();
        	port = Integer.parseInt(argsPort.get(0).trim()); // .toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [port].");
        }
        
    	argsInput = args.getOptionValues("carpeta");
        if (argsInput != null && !argsInput.isEmpty()) {
        	input = argsInput.get(0).trim().toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [carpeta].");
        }
                
        argsRuta = args.getOptionValues("ruta");
        if (argsRuta != null && !argsRuta.isEmpty()) {
        	ruta = argsRuta.get(0).trim().toLowerCase();
        	iEstado ++;
        } else {
            System.out.println("[ERR] Se requiere el argumento [ruta].");
        }
             
        if (iEstado == 6) {
        	output = ruta + sNombre;
        	tar.CreateTarGZ(input, output);
        	envio02(host, user, pass, port, output);		// envio02(output);
        	tar.EliminaFile(output);
        	System.out.println("[FIN] Fin del Proceso OK.");
        } else {
        	System.err.println("[ERR] Se requieren argumentos [carpeta, ruta].");
        }
    }
*/