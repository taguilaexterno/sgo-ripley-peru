package com.ripley.scheduled;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.ripley.restservices.logic.ISunatService;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.Util;

@Component
public class TaskSunat {
	
	private static final AriLog LOG = new AriLog(TaskSunat.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private ISunatService regulatorioSunatService;
	
	@Autowired
	private ParametrosDAO parametroDao;
	
	/** 
	 *  LOAD_FILE_SUNAT_ONOFF si este parametro permite apagar o encender la carga del archivo SUNAT 
	 *  Se agregan parametros para modificar el intervalo de timpo en el cual se realizara la carga del archivo SUNAT
	 *  SUCCESS_LOAD_FILE_SUNAT si esta en true no se realiza la carga del archivo, este se reinicia
	 *                          cuando la hora de inicio coincide con la hora actual (HH:mm)
	 *  START_TIME_LOAD_FILE_SUNAT hora de inicio del intervalo para realizar la carga
	 *  END_TIME_LOAD_FILE_SUNAT hora final del intervalo para realizar la carga
	 *  
	 *  cron=<second> <minute> <hour> <day-of-month> <month> <day-of-week> <year> <command>
	 *  @Scheduled(cron = "0 0 3 * * ?")
	 *  @Scheduled(cron ="0/10 * 8,9,10,11,12,13,14,15,16,17 * * ?")
	 **/
	@Scheduled(cron ="0 0/10 3,4,5,6 * * ?")
//	@Scheduled(cron ="0 0/5 * * * ?") // Prueba CHJ Cada 5 Min
    public void uploadFileWebAws() {
    	LOG.initTrace("uploadFileWebAws", "Inicio");
		boolean loadFileONOFF = false;
		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());
		parametroDao.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(),  Constantes.STRING_UNO);
		try {
			loadFileONOFF = Boolean.parseBoolean(parametros.buscaParametroPorNombre("LOAD_FILE_SUNAT_ONOFF").getValor());
		}catch (Exception e) {
			LOG.traceInfo("uploadFileWebAws", "LOAD_FILE_SUNAT_ONOFF variable no existe" );
		}
		if(loadFileONOFF) {
			LOG.traceInfo("uploadFileWebAws","Inicio");
			String horaInicio = null;
			String horaFin = null;
			String horaActual;
			boolean cargaExitosa = false;
			boolean estaEnHora = false;
			
			try {
				cargaExitosa = Boolean.parseBoolean(parametros.buscaParametroPorNombre("SUCCESS_LOAD_FILE_SUNAT").getValor());
				horaInicio = parametros.buscaParametroPorNombre("START_TIME_LOAD_FILE_SUNAT").getValor();
				horaFin = parametros.buscaParametroPorNombre("END_TIME_LOAD_FILE_SUNAT").getValor();

				DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constantes.FECHA_FORMATO_HH_MI_SS);
				horaActual = dateTimeFormatter.format(LocalDateTime.now());
				estaEnHora = FechaUtil.estaEnHora(horaInicio, horaFin, horaActual);
				
				if(horaInicio != null && horaActual != null && FechaUtil.horasMinutosIguales(horaInicio,horaActual)) {
					parametros.reemplazaValorLista("SUCCESS_LOAD_FILE_SUNAT", Constantes.STRING_FALSO);
					Parametro parametroLoad = parametros.buscaParametroPorNombre("SUCCESS_LOAD_FILE_SUNAT");
					parametroLoad.setPropertyTypeId(String.valueOf(Constantes.NRO_CIEN));
					parametroLoad.setValor(Constantes.STRING_FALSO);
					parametroDao.updateParametro(parametroLoad);
					cargaExitosa = false;					
				}
				
			}catch (Exception e) {
				LOG.traceInfo("uploadFileWebAws", "SUCCESS_LOAD_FILE_SUNAT, START_TIME_LOAD_FILE_SUNAT, END_TIME_LOAD_FILE_SUNAT validar variables");
				throw new AligareException("Alguno de estos parametros SUCCESS_LOAD_FILE_SUNAT, START_TIME_LOAD_FILE_SUNAT, END_TIME_LOAD_FILE_SUNAT validar variables");
			}
			LOG.traceInfo("uploadFileWebAws"," horaInicio:" + horaInicio + ", horaFin:" + horaFin + ", horaActual:" + horaActual + ", cargaExitosa:" + cargaExitosa + ", estaEnHora:" + estaEnHora);
			
			if(!cargaExitosa && estaEnHora){
		    	GenericResponse resp = new GenericResponse();
				try {
					String accessKey = null;
					String secretKey = null;
					String bucketName = null;
					String key = null;
					String region = null;
					try {
						accessKey = parametros.buscaParametroPorNombre("ACCESS_KEY_SUNAT").getValor();
						secretKey = parametros.buscaParametroPorNombre("SECRET_KEY_SUNAT").getValor();
						bucketName = parametros.buscaParametroPorNombre("BUCKET_NAME_SUNAT").getValor();
						key = parametros.buscaParametroPorNombre("KEY_SUNAT").getValor();
						region = parametros.buscaParametroPorNombre("REGION_SUNAT").getValor();
						String yearMonthDay = FechaUtil.getCurrentDateWithFormat(Constantes.FECHA_YYMMDD);
						key = key.replace(Constantes.FECHA_YYMMDD, yearMonthDay);
						
					}catch (Exception e) {
						LOG.traceInfo("uploadFileWebAws", "Alguno de estos parametros KEY_SUNAT, BUCKET_NAME_SUNAT, SECRET_KEY_SUNAT, ACCESS_KEY_SUNAT, REGION_SUNAT no exiten");
						throw new AligareException("Alguno de estos parametros KEY_SUNAT, BUCKET_NAME_SUNAT, SECRET_KEY_SUNAT, ACCESS_KEY_SUNAT, REGION_SUNAT no exiten");
					}
	
					LOG.traceInfo("uploadFileWebAws", "accessKey:" + (accessKey!=null) + ", secretKey:" + (secretKey!=null) +", bucketName:" + (bucketName!=null) + ", key:" + key + ", region:" + region);
					if(accessKey !=null && secretKey !=null && bucketName!=null && key!=null) {
						BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey); 
						AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
								.withRegion(Util.getRegionAws(region))
								.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
						S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, key));
						
						resp = regulatorioSunatService.uploadSunatFile(object.getObjectContent());
						object.close();
						if(resp.getCodigo().compareTo(new Integer(Constantes.STRING_CERO)) == 0) {
							LOG.traceInfo("uploadFileWebAws", "Se actualiza variable SUCCESS_LOAD_FILE_SUNAT");
							parametros.reemplazaValorLista("SUCCESS_LOAD_FILE_SUNAT", Constantes.STRING_VERDADERO);
							
							Parametro parametro = parametros.buscaParametroPorNombre("SUCCESS_LOAD_FILE_SUNAT");
							parametro.setPropertyTypeId(String.valueOf(Constantes.NRO_CIEN));
							parametro.setValor(Constantes.STRING_VERDADERO);
							parametroDao.updateParametro(parametro);
							
							DateTimeFormatter dateTimeFormatter2 = DateTimeFormatter.ofPattern(Constantes.FECHA_FORMATO_YYYY_MM_DD_HH_MI_SS);
							String fechaHoraActual = dateTimeFormatter2.format(LocalDateTime.now());
							parametros.reemplazaValorLista("SUCCESS_TIME_FILE_SUNAT", fechaHoraActual);
							Parametro parametroTime = parametros.buscaParametroPorNombre("SUCCESS_TIME_FILE_SUNAT");
							parametroTime.setPropertyTypeId(String.valueOf(Constantes.NRO_CIEN));
							parametroTime.setValor(fechaHoraActual);
							parametroDao.updateParametro(parametroTime);
							
						}
					} else {
						LOG.traceInfo("uploadFileWebAws", "Falta uno o mas parametros");
						resp.setCodigo(Constantes.NRO_MENOSUNO);
			    		resp.setMensaje("Falta uno o mas parametros");
					}
					
				} catch (AligareException e) {
					
					LOG.traceInfo("uploadFileWebAws", "AligareException:" + e.getMessage());
					resp.setCodigo(Constantes.NRO_MENOSUNO);
		    		resp.setMensaje("Exception:" + e.getMessage());
		    		
				} catch(AmazonServiceException e) {
					
					LOG.traceInfo("uploadFileWebAws", "AmazonServiceException:" + e.getMessage());
					resp.setCodigo(Constantes.NRO_MENOSUNO);
					resp.setMensaje("AmazonServiceException:" + e.getMessage());
					
			    } catch(SdkClientException e) {
			    	
					LOG.traceInfo("uploadFileWebAws", "SdkClientException:" + e.getMessage());
					resp.setCodigo(Constantes.NRO_MENOSUNO);
					resp.setMensaje("SdkClientException:" + e.getMessage());
					
			    } catch (Exception e) {
			    	
					LOG.traceInfo("uploadFileWebAws", "Exception:" + e.getMessage());
					resp.setCodigo(Constantes.NRO_MENOSUNO);
					resp.setMensaje("Exception:" + e.getMessage());
					
			    }
				LOG.endTrace("uploadFileWebAws", "resp = ", resp.getMensaje());
			} else {
				LOG.endTrace("uploadFileWebAws", "Esta fuera de horario o ya se realizo la carga exitosa del archivo SUNAT", Constantes.VACIO );
			}
		} else {
			LOG.traceInfo("uploadFileWebAws", "Carga de archivo SUNAT apagado" );	
		}
	}
}
