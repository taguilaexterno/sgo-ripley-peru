# [cybersource] - 2019/12/04

## SGO REST
- Se agrega el control del tag FLAG_VALIDATION que se incluye en el XML de las Ordenes
- Se agrega servicio POST para recibir los cambios de estados de Decision Manager.

## SGO Web
- Se agregan campos: Estado OC, COC y CS en la opción Confirma Medio de Pago

## Sincronizador CS
- Se agrega Job que lee y sincroniza estados de las ordenes con Decision Manager

## Asignacion Caja
- Se agrega control de estados de error de Cybersource: 92, 93, 94 y 95

## Validacion
- Se agrega control de estados de error de Cybersource: 92, 93, 94 y 95

## Otros
- Se agregan nuevos grupos de reglas para el proceso confirma
