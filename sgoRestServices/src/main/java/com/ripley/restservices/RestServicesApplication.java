package com.ripley.restservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;


@SpringBootApplication(scanBasePackages = {"com.ripley.restservices", "cl.ripley.omnicanalidad.dao", 
		"cl.ripley.omnicanalidad.util", "cl.ripley.omnicanalidad.logic", "com.ripley.dao", "com.ripley.config"})
public class RestServicesApplication {
	
	private static final AriLog LOG = new AriLog(RestServicesApplication.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	private static Environment env;
	
	@Autowired
	public void setEnv(Environment environment) {
		env = environment;
	}
	
	public static void main(String[] args) {
		LOG.initTrace("main", "String[] args");
		SpringApplication.run(RestServicesApplication.class, args);
		LOG.traceInfo("main", "Datasource = " + env.getProperty("spring.datasource.url"));
		LOG.traceInfo("main", "Datasource Username = " + env.getProperty("spring.datasource.username"));
		LOG.endTrace("main", "Aplicación inicializada con éxito", Constantes.VACIO);
	}
	
}
