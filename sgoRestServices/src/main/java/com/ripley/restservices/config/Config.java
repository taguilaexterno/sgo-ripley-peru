package com.ripley.restservices.config;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.ripley.restservices.pplclient.OnlinePortType;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.util.AppContextUtils;
import cl.ripley.omnicanalidad.util.Constantes;

/**Clase de configuracion de Spring Context.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@Configuration
public class Config {
	
	private static final AriLog LOG = new AriLog(Config.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private ParametrosDAO paramDAO;
	
	@Bean("parametrosCajaVirtual")
	public Parametros parametrosCajaVirtual(AppContextUtils utils) {
		LOG.initTrace("parametrosCajaVirtual", "utils = " + utils, new KeyLog("utils", utils.toString()));
		Parametros parametros = new Parametros();
		parametros.setParametros(new ArrayList<Parametro>());
		
		LOG.traceInfo("parametrosCajaVirtual", "Llenando parametros", 
						new KeyLog("Funcion Boleta", String.valueOf(Constantes.FUNCION_BOLETA)), 
						new KeyLog("parametrosList", parametros.toString()),
						new KeyLog("Balanceo Jar", "1"));
		
		paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), "1");
		
		LOG.endTrace("parametrosCajaVirtual", "parametros = " + parametros, parametros.toString());
		return parametros;
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder, ClientHttpRequestInterceptor logginInterceptor) {
		
		SimpleClientHttpRequestFactory srf = new SimpleClientHttpRequestFactory();
		srf.setOutputStreaming(Boolean.FALSE);
		srf.setConnectTimeout(Constantes.REST_TIMEOUT);
		srf.setReadTimeout(Constantes.REST_TIMEOUT);
		RestTemplateBuilder b = builder.requestFactory(new BufferingClientHttpRequestFactory(srf));
		b = b.interceptors(logginInterceptor);
		
		return b.build();
		
	}
	
	@Bean
	public JaxWsProxyFactoryBean pplServiceFactoryBean(Parametros parametros) {
		JaxWsProxyFactoryBean fb = new JaxWsProxyFactoryBean();
		fb.setServiceClass(OnlinePortType.class);
		fb.setAddress(parametros.buscaValorPorNombre("TARGETENDPOINT"));
		return fb;
	}
	
	@Bean
	public OnlinePortType pplService(JaxWsProxyFactoryBean fb) {
		
		return (OnlinePortType) fb.create();
		
	}
	
	@Bean
	public XPath xpath() {
		return XPathFactory.newInstance().newXPath();
	}
	
	
}
