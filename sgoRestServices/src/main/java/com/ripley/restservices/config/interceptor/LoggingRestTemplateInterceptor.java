package com.ripley.restservices.config.interceptor;

import java.io.IOException;
import java.util.Scanner;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;

@Component
public class LoggingRestTemplateInterceptor implements ClientHttpRequestInterceptor {
	
	private static final AriLog LOG = new AriLog(LoggingRestTemplateInterceptor.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		
		String cuerpo = body != null ? new String(body, "UTF-8") : Constantes.VACIO;
		
		LOG.initTrace("intercept", "request - body - execution", 
				new KeyLog("request", request.toString()), 
				new KeyLog("body", cuerpo), 
				new KeyLog("execution", execution.toString()));
		
		LOG.traceInfo("intercept", "Cuerpo del mensaje REST = " + cuerpo);
		
		ClientHttpResponse resp = execution.execute(request, body);
		
		String cuerpoResp = Constantes.VACIO;
		
		LOG.traceInfo("intercept", "Status Code de respuesta del servicio " + request.getURI() + " = " + resp.getStatusCode().value() + " ; Mensaje del Status= " + resp.getStatusText());
		
		if(!resp.getStatusCode().is4xxClientError() && !resp.getStatusCode().is5xxServerError()) {
			
			try (Scanner scanner = new Scanner(resp.getBody())) {
				scanner.useDelimiter("\\A");
				
				cuerpoResp = scanner.next();
			}
			
		}
		
		LOG.traceInfo("intercept", "Cuerpo del mensaje REST de respuesta = " + cuerpoResp);
		
		LOG.endTrace("intercept", "Interceptado", resp.toString());
		return resp;
	}

}
