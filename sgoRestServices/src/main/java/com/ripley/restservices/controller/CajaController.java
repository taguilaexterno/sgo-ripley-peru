package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.rpos.RposRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.caja.CajaBS;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;


/**Controlador de servicio REST para operaciones de Caja.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 08-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/caja")
public class CajaController {
	
	private static final AriLog LOG = new AriLog(CajaController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private CajaBS caja;

	
	/**Método que recibe un numero de caja para cerrar y luego aperturar para BOLETA
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 07-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@PostMapping(path = "/cerrarAperturarCaja")
	public GenericResponse cerrarAperturarCaja(@RequestBody GenericRequest req) {
		LOG.initTrace("cerrarAperturarCaja", "oc = " + String.valueOf(req.getOc()) + " - numCaja = " + String.valueOf(req.getNumCaja()), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));
		
		GenericResponse resp = new GenericResponse();
		try {
			resp = caja.cerrarAperturarCaja(req.getNumCaja(), Constantes.FUNCION_BOLETA);
		} catch (RestServiceTransactionException | DataAccessException e) {
			
			if(RestServiceTransactionException.class.isInstance(e)) {
				RestServiceTransactionException ex = (RestServiceTransactionException) e;
				LOG.traceError("cerrarAperturarCaja", ex.getMensaje(), e);
				resp.setCodigo(ex.getCodigo());
				resp.setMensaje(ex.getMensaje());
			} else {
				DataAccessException ex = (DataAccessException) e;
				LOG.traceError("cerrarAperturarCaja", ex.getMessage(), e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje(ex.getMessage());
			}
		}finally {
			Util.pasarGarbageCollector();
		}
		
		LOG.endTrace("cerrarAperturarCaja", "resp = " + String.valueOf(resp), String.valueOf(resp));
		return resp;
	}
	
	/**Método que recibe un numero de caja para cerrar y luego aperturar para NC
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 27-04-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@PostMapping(path = "/cerrarAperturarCajaNC")
	public GenericResponse cerrarAperturarCajaNC(@RequestBody GenericRequest req) {
		LOG.initTrace("cerrarAperturarCajaNC", "oc = " + String.valueOf(req.getOc()) + " - numCaja = " + String.valueOf(req.getNumCaja()), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));
		
		LOG.traceInfo("cerrarAperturarCajaNC", "Inicio proceso de cierre y apertura de caja = " + String.valueOf(req.getNumCaja()), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));
		
		GenericResponse resp = new GenericResponse();
		try {
			resp = caja.cerrarAperturarCaja(req.getNumCaja(), Constantes.FUNCION_NC);
		} catch (RestServiceTransactionException | DataAccessException e) {
			
			if(RestServiceTransactionException.class.isInstance(e)) {
				RestServiceTransactionException ex = (RestServiceTransactionException) e;
				LOG.traceError("cerrarAperturarCajaNC", ex.getMensaje(), e);
				resp.setCodigo(ex.getCodigo());
				resp.setMensaje(ex.getMensaje());
			} else {
				
				DataAccessException ex = (DataAccessException) e;
				LOG.traceError("cerrarAperturarCajaNC", ex.getMessage(), e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje(ex.getMessage());
				
			}
		}finally {
			Util.pasarGarbageCollector();
		}
		
		LOG.endTrace("cerrarAperturarCajaNC", "resp = " + String.valueOf(resp), String.valueOf(resp));
		return resp;
	}
}
