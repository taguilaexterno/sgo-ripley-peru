package com.ripley.restservices.controller;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;

import cl.ripley.omnicanalidad.logic.cyberSource.OrdenesCS;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

import com.ripley.restservices.model.generic.GenericResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cybersource")
public class CyberSourceController {
	private static final AriLog LOG = new AriLog(CyberSourceController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
    
    @Autowired
    private OrdenesCS OrdenesCS;

    @ResponseBody
    @RequestMapping(path = "/insertar", method = RequestMethod.POST)
    
    public GenericResponse insertaCS(@Valid @RequestBody String content) {
//  public GenericResponse insertaCS(@RequestBody CyberSourceRequest req) {
        LOG.initTrace("InsertaCS", "content = " + content);
        //LOG.initTrace("InsertaCS", "content = " + String.valueOf(req.getContent()));

        GenericResponse resp = new GenericResponse();
        GenericResponse resp2 = new GenericResponse();

        try {
            String valxml2 = content;   // String.valueOf(req.getContent()); // content
            //LOG.initTrace("InsertaCSY", "valxml2 = " + valxml2 );
            resp2 = OrdenesCS.actualizaEstado01CS(valxml2); //req.getContent());

            Integer cod = resp2.getCodigo();
            String msg = resp2.getMensaje();
            resp.setCodigo(cod);
            resp.setMensaje(msg);
            System.out.println("insertaCS(cod): "+cod); 
            System.out.println("insertaCS(msg): "+msg);
            LOG.initTrace("insertaCS", "Fin Proceso = [" + cod + "] " + msg );
                
        }catch (Exception e) {
                LOG.traceError("insertaCS", e);
                resp.setCodigo(Constantes.NRO_MENOSUNO);
                resp.setMensaje(e.getMessage());
        }
        LOG.endTrace("InsertaCS", "resp = " + resp, resp.toString());
        Util.pasarGarbageCollector();
        
        return resp;
    }
    
    
    @ResponseBody
    @RequestMapping(path = "/prueba", method = RequestMethod.POST)
    //public GenericResponse aprobarCC(@RequestBody GenericRequest req2, HttpServletRequest request) {
    //public GenericResponse aprobarCC(@Valid @RequestBody CyberSourceRequest req) {
    public GenericResponse aprobarCC(@Valid @RequestBody String req2) {
        //HttpSession session = request.getSession(true);
        // System.out.println("aprobarCC (1): "); 
        System.out.println("aprobarCC (2): " + req2); 
        GenericResponse respx = new GenericResponse();
        respx.setCodigo(0);
        respx.setMensaje("Mensaje_Prueba: " + req2);
        return respx;
    }
}
