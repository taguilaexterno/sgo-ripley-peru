package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.dao.IDespachoDAO;
import com.ripley.dao.dto.DespachoResponse;
import com.ripley.dao.dto.MarcaDespachoResponse;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.generic.despacho.MarcaModeloExtendidoRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

/**Controlador para proceso despacho BT
 * @author Boris Parra Luman [Aligare]
 * @since 25 may. 2018
 */
@RestController
@RequestMapping("/despacho")
public class DespachoController {
	
	
	private static final AriLog LOG = new AriLog(DespachoController.class,Constantes.CODIGO_APLICACION,PlataformaType.JAVA);

	@Autowired
	private IDespachoDAO despacho;
	
	@PostMapping("/marcarModeloExtendido")
	public GenericResponse marcarModeloExtendido(@RequestBody MarcaModeloExtendidoRequest req) {
		LOG.initTrace("marcarModeloExtendido","", new KeyLog("req", String.valueOf(req.getBody())));
		MarcaDespachoResponse resp = new MarcaDespachoResponse();
		resp = despacho.marcarModeloExtendido(req.getBody().getXml());
		GenericResponse gr = new GenericResponse();
		gr.setCodigo(resp.getCodError());
		gr.setMensaje(resp.getMsgError());
		gr.setData(null);
		LOG.endTrace("marcarModeloExtendido", "Finalizado", gr.toString());
		Util.pasarGarbageCollector();
		return gr;
	}
	
	
	@GetMapping("/obtenerOcPendienteDespacho")
	public DespachoResponse obtenerOcPendienteDespacho() throws Exception {
		LOG.initTrace("obtenerOcPendienteDespacho", "Sin parametros entrada");
		DespachoResponse resp = new DespachoResponse();
		resp = despacho.obtenerOcPendienteDespacho();
		LOG.endTrace("obtenerOcPendienteDespacho", "Repuesta= "+resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
		
	}
}
