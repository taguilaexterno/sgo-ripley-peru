package com.ripley.restservices.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.impresion.ConsultaOcImpresionRequest;
import com.ripley.restservices.model.impresion.ConsultaOcImpresionResponse;
import com.ripley.restservices.model.impresion.InsertaPagoTiendaRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.logic.boleta.EsclavoBoleta;
import cl.ripley.omnicanalidad.logic.mail.EsclavoMailBoleta;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;

import com.ripley.scheduled.*;

/**Controlador de servicio REST para impresión de boletas/facturas
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/imprimirBoleta")
public class ImprimirController {
	
	private static final AriLog LOG = new AriLog(ImprimirController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private EsclavoBoleta esclavoBoleta;
	
	@Autowired
	private RestTemplate rt;

	
	/**Método que recibe un numero de Orden de Compra (OC), el numero de caja para imprimir una boleta 
	 * Recibe flag para identificar Pago En Tienda, y los datos de PagoEnTienda.
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios: Se agrega pago en tienda<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(path = "/imprimir", method = RequestMethod.POST)
	public GenericResponse imprimir(@RequestBody InsertaPagoTiendaRequest req) {
		LOG.initTrace("imprimir", "Inicio proceso de impresion de OC " + req.getOc() + " - numCaja = " + req.getNumCaja(), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
				new KeyLog("Es pago en tienda", String.valueOf(req.isPagoTienda())),
				new KeyLog("Pago en Tienda",String.valueOf(req.getPagoEnTienda())));
		
//		LOG.traceInfo("imprimir", "Inicio proceso de impresion de OC = " + req.getOc() + " en caja = " + req.getNumCaja(), 
//				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
//				new KeyLog("Es pago en tienda", String.valueOf(req.isPagoTienda())),
//				new KeyLog("Pago en Tienda",String.valueOf(req.getPagoEnTienda())));
		
		GenericResponse resp = new GenericResponse();
		try {
			
			resp = esclavoBoleta.procesarNotaVenta(req.getOc(), Constantes.FUNCION_BOLETA, req.getNumCaja(), req.isPagoTienda(), req.getPagoEnTienda(), Constantes.NRO_CERO);
			
		} catch (RestServiceTransactionException | DataAccessException e) {
			
			if(RestServiceTransactionException.class.isInstance(e)) {
				RestServiceTransactionException ex = (RestServiceTransactionException) e;
				LOG.traceError("imprimir", ex.getMensaje(), e);
				resp.setCodigo(ex.getCodigo());
				resp.setMensaje(ex.getMensaje());
			} else {
				
				DataAccessException ex = (DataAccessException) e;
				LOG.traceError("imprimir", ex.getMessage(), e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje(ex.getMessage());
				
			}
		} catch (AligareException e) {
			LOG.traceError("imprimir", "AligareException: " + e.getMessage(), e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("AligareException: " + e.getMessage());
		} catch (Exception e) {
			LOG.traceError("imprimir", "Exception: " + e.getMessage(), e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("Exception: " + e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		
		LOG.endTrace("imprimir", "resp = " + resp, resp.toString());
		return resp;
	}
	
	/**Método que recibe un número de caja y un número de registros para retornar para luego ser procesados por la impresión.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 19-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain ConsultaOcImpresionResponse} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(path = "/consultaOCs", method = RequestMethod.POST)
	public GenericResponse consultaOCs(@RequestBody ConsultaOcImpresionRequest req) {
		LOG.initTrace("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
					+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
					+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
				new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));
		
//		LOG.traceInfo("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
//						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
//						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
//						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
//						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));
//		
		GenericResponse resp = esclavoBoleta.obtenerOCsImpresion(req.getNumCaja(), req.getCantidadOCs());
		
		LOG.endTrace("consultaOCs", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	@RequestMapping(path = "/imprimirXXX", method = RequestMethod.POST)
	public GenericResponse imprimirXXX(@Valid @RequestBody String content) {
        LOG.initTrace("[INI] Prueba", "Valor = " + content);
        //LOG.initTrace("InsertaCS", "content = " + String.valueOf(req.getContent()));
        long numOC = 13351025;
        Integer numCaja = 120;
        GenericResponse resp = new GenericResponse();
        
        InsertaPagoTiendaRequest req = new InsertaPagoTiendaRequest();
        req.setOc(numOC);
        req.setNumCaja(numCaja);
        this.imprimir(req);
        //resp = EsclavoMailBoleta.enviarEmailBoletaSilverPopXML(numOC);
	    return resp;
	}
	
	
	//envioLOG
	@RequestMapping(path = "/envioLOGXXX", method = RequestMethod.POST)
	public GenericResponse envioLOGXXX(@Valid @RequestBody String content) throws IOException {
        LOG.initTrace("[INI] Prueba", "Valor = " + content);
        //LOG.initTrace("InsertaCS", "content = " + String.valueOf(req.getContent()));
        long numOC = 13351025;
        Integer numCaja = 120;
                
        GenericResponse resp = new GenericResponse();
        //TaskEnvioLog.envioLOG();
        
	    return resp;
	}

}
