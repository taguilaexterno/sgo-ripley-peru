package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.insertaModeloExtendido.InsertaModeloExtendidoRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.insertaModeloExtendido.IInsertaModeloExtendidoService;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

/**Controller que se encarga de la inserción de OCs en el Modelo Extendido.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/oc")
public class InsertaModeloExtendidoController {

	private static final AriLog LOG = new AriLog(InsertaModeloExtendidoController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private IInsertaModeloExtendidoService insertaModeloExtendidoService;
	
	@PostMapping("/insertar")
	public GenericResponse inserta(@RequestBody InsertaModeloExtendidoRequest req) {
		
		LOG.initTrace("inserta", "oc = " + req.getOc() + " - numCaja = " + req.getNumCaja() + " - xml = " + req.getXml(), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
				new KeyLog("XML", String.valueOf(req.getXml())));

		GenericResponse resp = new GenericResponse();
		
		try {
			resp = insertaModeloExtendidoService.insertaModeloExtendido(req.getXml(), req.getOc());
		}catch (Exception e) {
			LOG.traceError("inserta", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("inserta", "resp = " + resp, resp.toString());
		return resp;
		
	}
	
	@GetMapping("/getLastId")
	public GenericResponse getLastId() {
		
		LOG.initTrace("getLastId", "Inicio");
		GenericResponse resp = new GenericResponse();
		
		try {
			
			resp = insertaModeloExtendidoService.getLastId();
			
		} catch (Exception e) {
			LOG.traceError("getLastId", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("getLastId", "resp = " + resp, resp.toString());
		return resp;
		
	}
	
	@PatchMapping("/updLastId/{lastId}")
	public GenericResponse updLastId(@PathVariable String lastId) {
		
		LOG.initTrace("updLastId", "String lastId", new KeyLog("lastId", String.valueOf(lastId)));
		
		GenericResponse resp = new GenericResponse();
		
		try {
			
			resp = insertaModeloExtendidoService.updLastId(lastId);
			
		} catch (Exception e) {
			LOG.traceError("updLastId", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("updLastId", "resp = " + resp, resp.toString());
		return resp;
	}
	
	@GetMapping("/getLastTime")
	public GenericResponse getLastTime() {
		
		LOG.initTrace("getLastTime", "Inicio");
		
		GenericResponse resp = new GenericResponse();
		
		try {
			
			resp = insertaModeloExtendidoService.getLastInsertOCDateTime();
			
		} catch (Exception e) {
			LOG.traceError("getLastTime", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("getLastTime", "resp = " + resp, resp.toString());
		return resp;
	}
	
	@PatchMapping("/updLastTime/{lastTime}")
	public GenericResponse updLastTime(@PathVariable String lastTime) {
		
		LOG.initTrace("updLastTime", "String lastTime", new KeyLog("lastTime", String.valueOf(lastTime)));
		
		GenericResponse resp = new GenericResponse();
		
		try {
			
			resp = insertaModeloExtendidoService.updLastInsertOCDateTime(lastTime);
			
		} catch (Exception e) {
			LOG.traceError("updLastTime", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("updLastTime", "resp = " + resp, resp.toString());
		return resp;
	}
	
}
