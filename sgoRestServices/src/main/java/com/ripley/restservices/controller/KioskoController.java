package com.ripley.restservices.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.dao.IOrdenCompraDAO;
import com.ripley.dao.dto.OcTraceRposDTO;
import com.ripley.dao.dto.TraceRposDTO;
import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.impresion.ImprimirOfflineRposRequest;
import com.ripley.restservices.model.impresion.InsertaPagoTiendaRequest;
import com.ripley.restservices.model.rpos.RposResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.bean.SucursalRpos;
import cl.ripley.omnicanalidad.logic.boleta.EsclavoBoleta;
import cl.ripley.omnicanalidad.logic.caja.OperacionesCaja;
import cl.ripley.omnicanalidad.logic.insertaModeloExtendido.IInsertaModeloExtendidoService;
import cl.ripley.omnicanalidad.logic.validacion.EsclavoValidacion;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;


/**Controlador de servicio REST para impresión de boletas/facturas para RPOS
 *
 * @author Pablo Salazar Osorio (Aligare).
 * @since 12-09-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/kiosko/oc")
public class KioskoController {
	
	private static final AriLog LOG = new AriLog(KioskoController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private EsclavoBoleta esclavoBoleta;
	
	@Autowired
	private IInsertaModeloExtendidoService insertaModeloExtendidoService;
	
	@Autowired
	private EsclavoValidacion esclavoValicacion;

	@Autowired
	private OperacionesCaja operacionesCaja;
	
	@Autowired
	private IOrdenCompraDAO ordenCompraDAO;
		
	/** 
	 * Método que recibe un numero de Orden de Compra (OC), el numero de caja y la sucursal para imprimir una boleta en RPOS
	 * Recibe flag para identificar Pago En Tienda, y los datos de PagoEnTienda.
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 12-09-2018
	 *<br/><br/>
	 * Cambios: Se agrega pago en tienda<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(path = "/imprimir", method = RequestMethod.POST)
	public RposResponse imprimir(@RequestBody InsertaPagoTiendaRequest req) {
		LOG.initTrace("imprimir", "oc = " + req.getOc() + " - numCaja = " + req.getNumCaja(), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
				new KeyLog("Sucursal", String.valueOf(req.getSucursal())),
				new KeyLog("Es pago en tienda", String.valueOf(req.isPagoTienda())),
				new KeyLog("Pago en Tienda",String.valueOf(req.getPagoEnTienda())));
		
		LOG.traceInfo("imprimir", "SGO REST Version: ", new KeyLog("version", Constantes.VERSION_SGO_REST));
		LOG.traceInfo("imprimir", "Inicio proceso de impresion de OC = " + req.getOc() + " en caja = " + req.getNumCaja(), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
				new KeyLog("Sucursal", String.valueOf(req.getSucursal())),
				new KeyLog("Es pago en tienda", String.valueOf(req.isPagoTienda())),
				new KeyLog("Pago en Tienda",String.valueOf(req.getPagoEnTienda())));
		
		Date fechaInicio = new Date();
		Date fechaTemporal=null, fechaTemporal2=null;
		int getOcTv=0, insertaOc=0, validaOc=0, imprimirOc=0, generaJson=0;
		String tiempoGetOcTv="", tiempoInsertaOc="", tiempoValidaOc="", tiempoImprimirOc="", tiempoGeneraJson="", xml="";
		GenericResponse respImpr = new GenericResponse(), respInsert = new GenericResponse(), respValid = new GenericResponse();
		Optional<TraceRposDTO> optTraceRpos;
		RposResponse resp = new RposResponse();
		try {
			optTraceRpos= esclavoBoleta.obtenerTraceRpos(req.getOc());
			if(!optTraceRpos.isPresent() || optTraceRpos.get().getGetOcTv() == Constantes.NRO_CERO) {
				esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson, 
						tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, null, false);
				xml = esclavoBoleta.obtenerXMLPorOC(req.getOc(), req.getNumCaja(), req.getSucursal());
				if(xml.equals(Constantes.URL_ERROR_OC_DATOS)) {
					resp.setMensaje("OC no fue encontrada por Servicio: "+ String.valueOf(req.getOc()));
					LOG.endTrace("imprimir", "ERROR resp = " + resp, resp.toString());
					return resp;
				}
				LOG.traceInfo("imprimir", "Xml rescatado por servicio = " + req.getOc(), new KeyLog("xml", xml));
				getOcTv=Constantes.NRO_UNO;
				fechaTemporal = new Date();
				tiempoGetOcTv = Util.formatMiliseg(fechaInicio, fechaTemporal);
				esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson, 
						tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, null, false);
			}else {
				resp.setMensaje("Trace GetOcTv [obtenerXMLPorOC] existe, OC: "+ String.valueOf(req.getOc()));
				return resp;
			}
			
			if(!optTraceRpos.isPresent() || optTraceRpos.get().getInsertaOc() == Constantes.NRO_CERO) {
				respInsert = insertaModeloExtendidoService.insertaModeloExtendido(xml, req.getOc());
				if(respInsert.getCodigo() == Constantes.NRO_UNO) {
					insertaOc=Constantes.NRO_UNO;
					fechaTemporal2= new Date();
					tiempoInsertaOc = Util.formatMiliseg(fechaTemporal, fechaTemporal2);
					esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson,
							tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, null, false);
				}else {
					resp.setMensaje("Xml no insertado, OC: "+ String.valueOf(req.getOc()));
					return resp;
				}
			}else {
				resp.setMensaje("Trace insertaOc [insertaModeloExtendido] existe, OC: "+ String.valueOf(req.getOc()));
				return resp;
			}

			if(!optTraceRpos.isPresent() || optTraceRpos.get().getValidaOc() == Constantes.NRO_CERO) {
				respValid = esclavoValicacion.ValidacionRPOS(req.getOc());
				if(respValid.getCodigo() == Constantes.NRO_UNO) {
					validaOc=Constantes.NRO_UNO;
					fechaTemporal = new Date();
					tiempoValidaOc = Util.formatMiliseg(fechaTemporal2, fechaTemporal);
					esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson, 
							tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, null, false);
				}else {
					resp.setMensaje("Xvalidación no realizada, OC: "+ String.valueOf(req.getOc()));
					return resp;
				}
			}else {
				resp.setMensaje("Trace validaOc [ValidacionRPOS] existe, OC: "+ String.valueOf(req.getOc()));
				return resp;
			}
			
			if(!optTraceRpos.isPresent() || optTraceRpos.get().getImprimirOc() == Constantes.NRO_CERO) {
				Map<String,Integer> cajaSuc= esclavoBoleta.getNumCajaRpos(req.getOc());
				respImpr = esclavoBoleta.procesarNotaVenta(req.getOc(), Constantes.FUNCION_BOLETA, cajaSuc.get(Constantes.PARAM_NUMERO_CAJA),
						req.isPagoTienda(), req.getPagoEnTienda(), cajaSuc.get(Constantes.PARAM_NUMERO_SUCURSAL));
				if(respImpr.getCodigo() == Constantes.NRO_CERO) {
					imprimirOc=Constantes.NRO_UNO;
					fechaTemporal2= new Date();
					tiempoImprimirOc = Util.formatMiliseg(fechaTemporal, fechaTemporal2);
					esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson, 
							tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, null, false);
				}else {
					resp.setMensaje("procesarNotaVenta no realizado, OC: "+ String.valueOf(req.getOc()));
					return resp;
				}
			}else {
				resp.setMensaje("Trace imprimirOc [procesarNotaVenta] existe, OC: "+ String.valueOf(req.getOc()));
				return resp;
			}
			if(!optTraceRpos.isPresent() || optTraceRpos.get().getGeneraJson() == Constantes.NRO_CERO) {
				resp=esclavoBoleta.generarJsonRpos(req.getOc());
				esclavoBoleta.cambiaEstadoRpos(req.getOc(), Constantes.NRO_UNO);
				if(resp.getMensaje() == null || !resp.getMensaje().equals(Constantes.ERROR)) {
					generaJson=Constantes.NRO_UNO;
					fechaTemporal= new Date();
					tiempoGeneraJson = Util.formatMiliseg(fechaTemporal2, fechaTemporal);
					esclavoBoleta.actualizarTraceRpos(optTraceRpos, req.getOc(), fechaInicio, getOcTv, insertaOc, validaOc, imprimirOc, generaJson, 
							tiempoGetOcTv, tiempoInsertaOc, tiempoValidaOc, tiempoImprimirOc, tiempoGeneraJson, new Date(), false);
				}else {
					resp.setMensaje("generarJsonRpos no realizado, OC: "+ String.valueOf(req.getOc()));
					return resp;
				}
			}else {
				resp.setMensaje("Trace generaJson [generarJsonRpos] existe, OC: "+ String.valueOf(req.getOc()));
				return resp;
			}
		} catch (RestServiceTransactionException | DataAccessException e) {
			if(RestServiceTransactionException.class.isInstance(e)) {
				RestServiceTransactionException ex = (RestServiceTransactionException) e;
				LOG.traceError("imprimir", ex.getMensaje(), e);
				resp.setMensaje(ex.getMensaje());
			} else {
				DataAccessException ex = (DataAccessException) e;
				LOG.traceError("imprimir", ex.getMessage(), e);
				resp.setMensaje(ex.getMessage());
			}
		}catch(Exception ex) {
			LOG.traceError("imprimir", ex);
			resp.setMensaje("Error, OC: "+ String.valueOf(req.getOc()));
			return resp;
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("imprimir", "resp = " + resp, resp.toString());
		return resp;
	}
	
	/** 
	 * Método que recibe un numero de Orden de Compra (OC)y retorna JSON para imprimir una boleta en RPOS
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 18-10-2018
	 *<br/><br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain RposResponse}
	 */
	@RequestMapping(path = "/getJson", method = RequestMethod.POST)
	public RposResponse getJson(@RequestBody GenericRequest req) {
		LOG.initTrace("getJson", "oc = " + req.getOc(), new KeyLog("Orden de Compra", String.valueOf(req.getOc())));
		RposResponse resp = new RposResponse();
		resp=esclavoBoleta.generarJsonRpos(req.getOc());
		LOG.endTrace("getJson", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	/** 
	 * Método que recibe una variable Job_jenkins y cantidad de OC para consultar OCs y reprocesar nota de ventas RPOS
	 * Esto se realizo ya que la la venta RPOS termina en el envío a PPL, con esto reenviaremos a BO,BT,BCV
	 * @author Pablo Salazar Osorio (Aligare).
	 * @since 26-10-2018
	 *<br/><br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain ImprimirOfflineRposRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(path = "/imprimirOfflineRpos", method = RequestMethod.POST)
	public GenericResponse imprimirOfflineRpos(@RequestBody ImprimirOfflineRposRequest req) {
		LOG.initTrace("imprimirOfflineRpos", "jobJenkins: "+String.valueOf(req.getJobJenkins())+", cantidadOC: "+String.valueOf(req.getCantidadOCs()),
				new KeyLog("Job_jenkins", String.valueOf(req.getJobJenkins()))
				,new KeyLog("Cantidad OCs", String.valueOf(req.getCantidadOCs())));
		GenericResponse resp = new GenericResponse();
		if(req.getJobJenkins()<Constantes.NRO_CERO) {
			List<SucursalRpos> list = new ArrayList<SucursalRpos>();
			int validaOc=0, imprimirOc=0, generaJson=0;
			try {
				list = operacionesCaja.cajaSucursalRpos(req.getJobJenkins()*Constantes.NRO_MENOSUNO);
			}catch(Exception e) {
				LOG.traceError("consultaOCImprime jobJenkins: "+req.getJobJenkins(), e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje("Error consultaOCImprime caja");
				return resp;
			}
			for(SucursalRpos cajaSuc: list) {
				try {
					List<OcTraceRposDTO> listOC = ordenCompraDAO.getOrdenesByEstadoAndCajaRpos(Constantes.NRO_UNO, cajaSuc.getCaja(), 
							req.getCantidadOCs(), cajaSuc.getSucursal());
					LOG.traceInfo("imprimirOfflineRpos", "listOC: "+listOC);
					for(OcTraceRposDTO ocTrace : listOC) {
						LOG.traceInfo("imprimirOfflineRpos", "procesarNotaVenta, oc: "+String.valueOf(ocTrace.getOc())+", Caja: "+
								String.valueOf(cajaSuc.getCaja())+", Sucursal: "+String.valueOf(cajaSuc.getSucursal()));
						
						if(ocTrace.getOcTV() == Constantes.NRO_UNO && ocTrace.getInsertaOC()== Constantes.NRO_UNO && ocTrace.getValidaOC()== Constantes.NRO_UNO
								&&ocTrace.getImprimeOC() == Constantes.NRO_UNO && ocTrace.getGeneraJson() == Constantes.NRO_UNO 
								&& ocTrace.getPpl() == Constantes.NRO_UNO) {
							//IMPRIMIR OFFLINE
							LOG.traceInfo("imprimirOfflineRpos", "imprimirOfflineRpos Rpos");
							try {
								esclavoBoleta.procesarNotaVenta(ocTrace.getOc(), Constantes.FUNCION_BOLETA, cajaSuc.getCaja(), false, null, 
										cajaSuc.getSucursal());
								LOG.traceInfo("imprimirOfflineRpos", "procesarNotaVenta OK");
							}catch (RestServiceTransactionException e) {
								LOG.traceError("imprimirOfflineRpos, Error procesarNotaVenta oc: "+String.valueOf(ocTrace.getOc()), e);
								continue;
							}catch(Exception e) {
								LOG.traceError("imprimirOfflineRpos, Error procesarNotaVenta oc: "+String.valueOf(ocTrace.getOc()), e);
								continue;
							}
						}else{
							//REPROCESO
							if(ocTrace.getOcTV() == Constantes.NRO_UNO && ocTrace.getInsertaOC()== Constantes.NRO_UNO && 
									ocTrace.getValidaOC()== Constantes.NRO_UNO && ocTrace.getImprimeOC() == Constantes.NRO_CERO && 
									ocTrace.getGeneraJson() == Constantes.NRO_CERO && ocTrace.getPpl() == Constantes.NRO_CERO) {
								
								//ImprimirOc
								LOG.traceInfo("reprocesoTraceRpos", "Reproceso Rpos ImprimirOc");
								GenericResponse respRepro = new GenericResponse();
								respRepro = esclavoBoleta.procesarNotaVenta(ocTrace.getOc(), Constantes.FUNCION_BOLETA, cajaSuc.getCaja(),
										false, null, cajaSuc.getSucursal());
								esclavoBoleta.cambiaEstadoCajaSuc(ocTrace.getOc(), Constantes.NRO_UNO, cajaSuc.getCaja(), cajaSuc.getSucursal());
								if(respRepro.getCodigo() == Constantes.NRO_CERO) {
									imprimirOc=Constantes.NRO_UNO;
									generaJson=Constantes.NRO_UNO;
									esclavoBoleta.actualizarTraceRpos(null, ocTrace.getOc(), null, 0, 0, ocTrace.getValidaOC(), imprimirOc, generaJson, 
											null, null, null, null, null, null, true);
								}else {
									LOG.traceInfo("reprocesoTraceRpos", "No se reprocesa ImprimirOc", new KeyLog("OC", String.valueOf(ocTrace.getOc())));
									continue;
								}
								
							}else if(ocTrace.getOcTV() == Constantes.NRO_UNO && ocTrace.getInsertaOC()== Constantes.NRO_UNO && 
									ocTrace.getValidaOC()== Constantes.NRO_CERO && ocTrace.getImprimeOC() == Constantes.NRO_CERO && 
									ocTrace.getGeneraJson() == Constantes.NRO_CERO && ocTrace.getPpl() == Constantes.NRO_CERO) {
								
								//ValidacionOC e ImprimirOc
								LOG.traceInfo("reprocesoTraceRpos", "Reproceso Rpos ValidacionOC e ImprimirOc");
								GenericResponse respRepro = new GenericResponse();
								respRepro = esclavoValicacion.ValidacionRPOS(ocTrace.getOc());
								if(respRepro.getCodigo() == Constantes.NRO_UNO) {
									validaOc=Constantes.NRO_UNO;
									imprimirOc=Constantes.NRO_CERO;
									generaJson=Constantes.NRO_CERO;
									esclavoBoleta.actualizarTraceRpos(null, ocTrace.getOc(), null, 0, 0, validaOc, imprimirOc, generaJson, 
											null, null, null, null, null, null, true);
								}else {
									LOG.traceInfo("reprocesoTraceRpos", "No se reprocesa ValidacionOC", new KeyLog("OC", String.valueOf(ocTrace.getOc())));
									continue;
								}
								if(respRepro.getCodigo() == Constantes.NRO_UNO) {
									respRepro = new GenericResponse();
									respRepro = esclavoBoleta.procesarNotaVenta(ocTrace.getOc(), Constantes.FUNCION_BOLETA, cajaSuc.getCaja(),
											false, null, cajaSuc.getSucursal());
									esclavoBoleta.cambiaEstadoCajaSuc(ocTrace.getOc(), Constantes.NRO_UNO, cajaSuc.getCaja(), cajaSuc.getSucursal());
									if(respRepro.getCodigo() == Constantes.NRO_CERO) {
										validaOc=Constantes.NRO_UNO;
										imprimirOc=Constantes.NRO_UNO;
										generaJson=Constantes.NRO_UNO;
										esclavoBoleta.actualizarTraceRpos(null, ocTrace.getOc(), null, 0, 0, validaOc, imprimirOc, generaJson, 
												null, null, null, null, null, null, true);
									}else {
										LOG.traceInfo("reprocesoTraceRpos", "No se reprocesa ImprimirOc", new KeyLog("OC", String.valueOf(ocTrace.getOc())));
										continue;
									}
								}
							}else {
								LOG.traceInfo("reprocesoTraceRpos", "No se reprocesa la OC valores erroneos", new KeyLog("OC", String.valueOf(ocTrace.getOc())));
								continue;
							}
						}
					}
				}catch(Exception e) {
					LOG.traceError("imprimirOfflineRpos, Error getOrdenesByEstadoAndCajaRpos caja: "+cajaSuc.getCaja()+", sucursal: "+cajaSuc.getSucursal(), e);
					continue;
				}
			}
			resp.setCodigo(Constantes.NRO_CERO);
			resp.setMensaje("OK");
		}else {
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("Error consultaOCImprime caja");
		}
		LOG.endTrace("imprimirOfflineRpos", "Finalizado OK","");
		return resp;
	}	
}
