package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.mail.ConsultaOcMailRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.mail.EsclavoMailBoleta;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
/**Controlador para procesos de Email de Boletas 
 * @author Aligare-Bparra
 *
 */
@RestController
@RequestMapping("/emailBoletas")
public class MailController {

	private static final AriLog LOG = new AriLog(MailController.class,Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private EsclavoMailBoleta esclavoMailBoleta;

	/**Envía el correo al cliente de la boleta.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param oc numero de orden de compra.
	 * @return {@linkplain GenericResponse}
	 */
	@GetMapping("/enviarMailBoleta/{oc}")
	public GenericResponse enviarMailBoleta(@PathVariable("oc") Long oc) {

		GenericResponse resp = new GenericResponse();

		System.out.println("[INI] Iniciando envío enviarEmailBoletaSilverPopXML");
		
		//resp = esclavoMailBoleta.enviarEmailBoletaSilverPopSMTP(oc); //Envío por SMTP
		resp = esclavoMailBoleta.enviarEmailBoletaSilverPopXML(oc);
		Util.pasarGarbageCollector();

		System.out.println("[FIN] Fin envío enviarEmailBoletaSilverPopXML");
		return resp;
	}
	/**Actualiza la URLDOCE de la orden de compra
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param oc número de la orden de compra
	 * @return {@linkplain GenericResponse}
	 */
	@PatchMapping("/updateUrlDoce/{oc}")
	public GenericResponse updateUrlDoce(@PathVariable("oc") Long oc) {

		GenericResponse resp = new GenericResponse();

		resp = esclavoMailBoleta.updateUrlDoce(oc);
		Util.pasarGarbageCollector();
		return resp;
	}

	@PostMapping("/consultaOCs")
	public GenericResponse consultaOCs(@RequestBody ConsultaOcMailRequest req) {
		LOG.initTrace("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

//		LOG.traceInfo("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
//						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
//						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
//						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
//						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

		GenericResponse resp = esclavoMailBoleta.obtenerOCsMail(req.getNumCaja(), req.getCantidadOCs());
		LOG.endTrace("consultaOCs", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	@PostMapping("/consultaOCsUpdateUrlDoce")
	public GenericResponse consultaOCsUpdateUrlDoce(@RequestBody ConsultaOcMailRequest req) {
		LOG.initTrace("consultaOCsUpdateUrlDoce", "oc = " + String.valueOf(req.getOc()) 
						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));
		
		LOG.traceInfo("consultaOCsUpdateUrlDoce", "oc = " + String.valueOf(req.getOc()) 
						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));
		
		GenericResponse resp = esclavoMailBoleta.obtenerOCsMailUpdateUrlDoce(req.getNumCaja(), req.getCantidadOCs());
		LOG.endTrace("consultaOCsUpdateUrlDoce", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	@PostMapping("/updateUrlDocePPL")
	public GenericResponse updateUrlDoce(@RequestBody ConsultaOcMailRequest req) {
		LOG.initTrace("updateUrlDocePPL", "- jobUrlDoce = " + String.valueOf(req.getJobUrlDoce())+" - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
						new KeyLog("JobUrlDoce", String.valueOf(req.getJobUrlDoce())),
						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));
		
		GenericResponse resp = esclavoMailBoleta.updateUrlDocePPL(req.getJobUrlDoce(), req.getCantidadOCs());
		LOG.endTrace("updateUrlDocePPL", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}

	// /emailBoletas
	@PostMapping("/enviarMailBoleta_Prueba")
	public GenericResponse enviarMailBoleta_Prueba(@RequestBody String req) {
		//long oc = 20432533;	//Boleta
		//long oc = 19524316;	//Factura
		//long oc = 13335153;		//Boleta URL_DOCE
		//long oc = 13082002; 
		//long oc = 15195829; 
		long oc = Long.parseLong(req);
		//long oc = 10992573;  // RT
		GenericResponse resp = new GenericResponse();
		resp = enviarMailBoleta(oc);
		//resp = esclavoMailBoleta.enviarEmailBoletaSilverPopXML(oc);
		Util.pasarGarbageCollector();
		return resp;
	}
}
