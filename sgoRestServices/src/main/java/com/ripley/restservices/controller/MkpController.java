package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.restservices.model.anulacion.ConsultaOcAnulacionRequest;
import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.notaCredito.EsclavoNotaCredito;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import cl.ripley.omnicanalidad.util.exception.RestServiceTransactionException;


/**Controlador de servicio REST para MKP
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 07-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/mkp")
public class MkpController {

	private static final AriLog LOG = new AriLog(MkpController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private EsclavoNotaCredito esclavoNotaCredito;


	/**Método que recibe un numero de Orden de Compra (OC) y el numero de caja para anular productos MKP
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 07-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req Objeto tipo {@linkplain GenericRequest} contenedor de los parametros necesarios para generar la boleta.
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(path = "/anularMkp", method = RequestMethod.POST)
	public GenericResponse anularMkp(@RequestBody GenericRequest req) {
		LOG.initTrace("anularMkp", "oc = " + req.getOc() + " - numCaja = " + req.getNumCaja(), 
				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));

//		LOG.traceInfo("anularMkp", "Inicio proceso de anulación MKP de OC = " + req.getOc() + " en caja = " + req.getNumCaja(), 
//				new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//				new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));
		GenericResponse resp = new GenericResponse();
		try {
			resp = esclavoNotaCredito.procesarAnulacionMkp(req.getOc(), Constantes.FUNCION_NC, req.getNumCaja());
		} catch (RestServiceTransactionException | DataAccessException e) {
			if(RestServiceTransactionException.class.isInstance(e)) {
				RestServiceTransactionException ex = (RestServiceTransactionException) e;
				LOG.traceError("anularMkp", ex.getMensaje(), e);
				resp.setCodigo(ex.getCodigo());
				resp.setMensaje(ex.getMensaje());
			} else {
				
				DataAccessException ex = (DataAccessException) e;
				LOG.traceError("anularMkp", ex.getMessage(), e);
				resp.setCodigo(Constantes.NRO_MENOSUNO);
				resp.setMensaje(ex.getMessage());
				
			}
		}finally {
			Util.pasarGarbageCollector();
		}

		LOG.endTrace("anularMkp", "resp = " + resp, resp.toString());
		return resp;
	}

	/**Obtiene números de OCs para anulación.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 20-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req
	 * @return
	 */
	@RequestMapping(path = "/consultaOCs", method = RequestMethod.POST)
	public GenericResponse consultaOCs(@RequestBody ConsultaOcAnulacionRequest req) {
		LOG.initTrace("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
		+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
		+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
		new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
		new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
		new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

//		LOG.traceInfo("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
//		+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
//		+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
//		new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//		new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
//		new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

		GenericResponse resp = esclavoNotaCredito.obtenerNumerosOCsAnulacion(req.getNumCaja(), req.getCantidadOCs());
		LOG.endTrace("consultaOCs", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}

}
