package com.ripley.restservices.controller;

import java.io.Reader;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestHeader;
//import org.json.*;

import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.insertaModeloExtendido.InsertaModeloExtendidoRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.insertaModeloExtendido.IInsertaModeloExtendidoService;

import com.ripley.restservices.model.pagoEfectivo.DataPagoEfectivo;
import com.ripley.restservices.model.pagoEfectivo.IPagoEfectivo;
import com.ripley.restservices.model.pagoEfectivo.PagoEfectivo;
import com.ripley.restservices.model.pagoEfectivo.impl.IPagoEfectivoImpl;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;
import oracle.jdbc.proxy.annotation.Post;

@RestController
@RequestMapping("/pagoefectivo")
public class PagoEfectivoController {
	private static final AriLog LOG = new AriLog(CyberSourceController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private IPagoEfectivo insertaPagoEfectivo;
	
	/*
	@ResponseBody
    @RequestMapping(path = "/insertar", method = RequestMethod.POST)
	public GenericResponse insertar(@Valid @RequestBody String content) {
	*/
	/*
	@Post
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	, @HeaderParam("PE-Signature") String token
	*/
	@PostMapping("/insertar")
	public GenericResponse insertar(@RequestBody PagoEfectivo req, @RequestHeader(value="PE-Signature") String signature) {
		LOG.initTrace("Insertar", "eventType = " + req.getEventType() + ", operationNumber = " + req.getOperationNumber() + ", data = " + req.getData(),
				new KeyLog("Tipo de Evento", String.valueOf(req.getEventType())),
				new KeyLog("Número de Operación", String.valueOf(req.getEventType())),
				new KeyLog("Data", String.valueOf(req.getData())));

		GenericResponse resp = new GenericResponse();
		
		try {
			String codigo = insertaPagoEfectivo.encode(Constantes.SECRETKEY, req.toString()); // SECRETKEY // "secretKey"
			
			if (signature.equals(codigo)) {
				LOG.traceInfo("insertar", "Iniciando insertaPagoEfectivo", new KeyLog("Tipo de Evento", String.valueOf(req.getEventType())),
						new KeyLog("Número de Operación", String.valueOf(req.getEventType())),
						new KeyLog("Data", String.valueOf(req.getData())));
				resp = insertaPagoEfectivo.insertaPagoEfectivo(req);
				//resp.setCodigo(Constantes.NRO_CERO);
				//resp.setMensaje("OK");
			} else {
				//LOG.traceInfo("insertar", mensaje, keys);
				resp.setCodigo(Constantes.NRO_MENOSDOS);
				resp.setMensaje("La firma recibida en el Header no es válida.");
				resp.setData("PE-Signature: [" + signature + "], Mensaje: [" + codigo + "]");
			}
			
		}catch (Exception e) {
			LOG.traceError("insertar", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("insertar", "resp = " + resp, resp.toString());
		return resp;
	}
	
	@PostMapping("/insertar_sh")
	public GenericResponse insertar_sh(@RequestBody PagoEfectivo req) {
		LOG.initTrace("Insertar", "eventType = " + req.getEventType() + ", operationNumber = " + req.getOperationNumber() + ", data = " + req.getData(),
				new KeyLog("Tipo de Evento", String.valueOf(req.getEventType())),
				new KeyLog("Número de Operación", String.valueOf(req.getEventType())),
				new KeyLog("Data", String.valueOf(req.getData())));

		GenericResponse resp = new GenericResponse();
		
		try {
			resp = insertaPagoEfectivo.insertaPagoEfectivo(req);
			//resp.setCodigo(Constantes.NRO_CERO);
			//resp.setMensaje("OK");
		}catch (Exception e) {
			LOG.traceError("insertar", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("insertar", "resp = " + resp, resp.toString());
		return resp;
	}
	
	/*
	public Response authenticate(@HeaderParam("authorization") String token) {
		return Response.ok("token="+token).build();
	}
	
	public Response authenticate (@Context HttpHeaders headers) {
		String result = "";
		for(Map.Entry entry: headers.getRequestHeaders().entrySet() ) {
			result += entry.getKey() + "=" + entry.getValue() + ", ";
		}
		
		return Response.ok(result).build();
	}
	*/
	
	/*
	@PostMapping("/insertar")
	public GenericResponse insertar(@RequestBody PagoEfectivo req, @RequestHeader(value="PE-Signature") String signature) {
		LOG.initTrace("Insertar", "eventType = " + req.getEventType() + ", operationNumber = " + req.getOperationNumber() + ", data = " + req.getData(),
				new KeyLog("Tipo de Evento", String.valueOf(req.getEventType())),
				new KeyLog("Número de Operación", String.valueOf(req.getEventType())),
				new KeyLog("Data", String.valueOf(req.getData())));

		GenericResponse resp = new GenericResponse();
		
		try {
			/*
			//req2.setCharacterEncoding("UTF-8");
			//Reader r = req2.getReader();
			//String cadena3 = req2.toString();
			
			//System.out.println ("req (1): " + req.toString());
			String cadena = String.valueOf(req.toString()); // req.toString();
			String cadena2 = "{\"eventType\":\"cip.paid\",\"operationNumber\":\"691681\",\"data\":{\"cip\":\"2539089\",\"currency\":\"PEN\",\"amount\":1021.90}}";
			String cadena3 = insertaPagoEfectivo.obtieneJson2(req);
			System.out.println ("cadena: " + cadena);
			System.out.println ("cadena2: " + cadena2);
			System.out.println ("cadena3: " + cadena3);
			if (cadena.equals(cadena2)) {
				System.out.println ("cadena = cadena2");
			} else {
				System.out.println ("cadena <> cadena2");
			}
			
			if (cadena2.equals(cadena3)) {
				System.out.println ("cadena2 = cadena3");
			} else {
				System.out.println ("cadena2 <> cadena3");
			}
			
			String codigo = insertaPagoEfectivo.encode(Constantes.SECRETKEY, req.toString()); // SECRETKEY // "secretKey"
			String codigo1 = insertaPagoEfectivo.encode(Constantes.SECRETKEY, cadena);
			String codigo2 = insertaPagoEfectivo.encode(Constantes.SECRETKEY, cadena2);
			String codigo3 = insertaPagoEfectivo.encode(Constantes.SECRETKEY, cadena3);
			System.out.println ("PE-Signature: " + signature);
			System.out.println ("codigo: " + codigo);
			System.out.println ("codigo (1): " + codigo1);
			System.out.println ("codigo (2): " + codigo2);
			System.out.println ("codigo (3): " + codigo3);
			//String codigo2 = insertaPagoEfectivo.hmacSha256Base64(Constantes.SECRETKEY, req.toString());
			//String codigo4 = insertaPagoEfectivo.hmacSha256Base64(Constantes.SECRETKEY, cadena);
			*//*
			
			String codigo = insertaPagoEfectivo.encode(Constantes.SECRETKEY, req.toString()); // SECRETKEY // "secretKey"
			
			if (signature.equals(codigo)) {
				LOG.traceInfo("insertar", "Iniciando insertaPagoEfectivo", new KeyLog("Tipo de Evento", String.valueOf(req.getEventType())),
						new KeyLog("Número de Operación", String.valueOf(req.getEventType())),
						new KeyLog("Data", String.valueOf(req.getData())));
				resp = insertaPagoEfectivo.insertaPagoEfectivo(req);
				//resp.setCodigo(Constantes.NRO_CERO);
				//resp.setMensaje("OK");
			} else {
				//LOG.traceInfo("insertar", mensaje, keys);
				resp.setCodigo(Constantes.NRO_MENOSDOS);
				resp.setMensaje("La firma recibida en el Header no es válida.");
				resp.setData("PE-Signature: [" + signature + "], Mensaje: [" + codigo + "]");
			}
			
		}catch (Exception e) {
			LOG.traceError("insertar", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("insertar", "resp = " + resp, resp.toString());
		return resp;
	}
	 */
}
