package com.ripley.restservices.controller;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.dao.IReservasDAO;
import com.ripley.dao.dto.DatosReservaResponse;
import com.ripley.dao.dto.EstadoReservaResponse;
import com.ripley.dao.dto.FechaVigenciaResponse;
import com.ripley.dao.dto.TopeProductoResponse;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;


/**Controlador REST para Reservas (Pago en Tienda)
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 25-04-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/reservas")
public class ReservasController {

	private static final AriLog LOG = new AriLog(ReservasController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private IReservasDAO reservasDAO;
	
	@PatchMapping("/updateReservasVencidas")
	public GenericResponse updateReservasVencidas() {
		LOG.initTrace("updateReservasVencidas", "Inicio");
		
		GenericResponse resp = new GenericResponse();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje(Constantes.OK);
		
		Map<String, Object> out = reservasDAO.updateReservasVencidas();
		
		BigDecimal codigo = (BigDecimal) out.get(com.ripley.dao.util.Constantes.COD_OUT_PARAM);
		String mensaje = (String) out.get(com.ripley.dao.util.Constantes.MENSAJE_OUT_PARAM);
		
		if(codigo.intValue() != Constantes.NRO_CERO) {
			
			resp.setCodigo(codigo.intValue());
			resp.setMensaje(mensaje);
			
		}
		LOG.endTrace("updateReservasVencidas", "Finalizado", String.valueOf(resp));
		Util.pasarGarbageCollector();
		return resp;
		
	}
	
	@GetMapping("/getFechaVigencia/{numeroReserva}")
	public FechaVigenciaResponse getFechaVigencia(@PathVariable(name = "numeroReserva", required = false) String numeroReserva) {

		LOG.initTrace("getFechaVigencia", "String numeroReserva: "+numeroReserva);
		FechaVigenciaResponse retorno = new FechaVigenciaResponse();
		retorno = reservasDAO.fechaVigencia(numeroReserva);
		LOG.endTrace("getFechaVigencia", "Finalizado", "retorno: "+ String.valueOf(retorno));
		Util.pasarGarbageCollector();
		return retorno;
	}
	
	@GetMapping("/getTopeProductos")
	public TopeProductoResponse getTopeProducto(){
		LOG.initTrace("getTopeProducto", null);
		TopeProductoResponse retorno = new TopeProductoResponse();
		retorno = reservasDAO.topeProductos();
		LOG.endTrace("getTopeProducto", "Finalizado", "retorno: "+String.valueOf(retorno));
		Util.pasarGarbageCollector();
		return retorno;
	}
	
	@GetMapping("/getDatosReservaOC/{numeroReserva}")
	public DatosReservaResponse getDatosReservaOC(@PathVariable(name = "numeroReserva", required = true) String numeroReserva){
		LOG.initTrace("getDatosReservaOC", "String numeroReserva: "+numeroReserva);
		DatosReservaResponse retorno = new DatosReservaResponse();
		retorno = reservasDAO.getReservaOC(numeroReserva);
		LOG.endTrace("getDatosReservaOC", "Finalizado", "retorno: "+String.valueOf(retorno));
		Util.pasarGarbageCollector();
		return retorno;
	}
	
	@GetMapping("/getDatosReservaRut/{rut}")
	public DatosReservaResponse getDatosReservaRut(@PathVariable(name = "rut", required = true) String rut){
		LOG.initTrace("getDatosReservaRut", "String rut: "+rut);
		DatosReservaResponse retorno = new DatosReservaResponse();
		retorno= reservasDAO.getReservaRut(rut);
		LOG.endTrace("getDatosReservaRut", "Finalizado", "retorno: "+String.valueOf(retorno));
		Util.pasarGarbageCollector();
		return retorno;
	}
	
	@GetMapping(value = "/getEstadoOC/{numeroReserva}")
	public EstadoReservaResponse getEstadoOC(@PathVariable(name = "numeroReserva", required = true) String numeroReserva){
		LOG.initTrace("getEstadoOC", "String numeroReserva: "+numeroReserva);
		EstadoReservaResponse retorno = new EstadoReservaResponse();
		retorno = reservasDAO.getEstadoOC(numeroReserva);
		LOG.endTrace("getEstadoOC", "Finalizado", "retorno: "+String.valueOf(retorno));
		Util.pasarGarbageCollector();
		return retorno; 
		
	}
	
}
