package com.ripley.restservices.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;

/**Controlador de errores para exceptions sin controlar de los servicios REST.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
public class RestErrorController implements ErrorController {


	private static final AriLog LOG = new AriLog(RestErrorController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	private static final String PATH = "/error";
	
	/**Metodo encargado de armar una respuesta de error para la exception sin controlar.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping("/error")
	public GenericResponse error() {
		LOG.initTrace("error", Constantes.VACIO);
		LOG.traceInfo("error", "Error inesperado en el servicio.");

		GenericResponse resp = new GenericResponse();

		resp.setCodigo(-1);
		resp.setMensaje("NOK");
		
		ObjectMapper om = new ObjectMapper();
		String json = Constantes.VACIO;
		try {
			json = om.writeValueAsString(resp);
		} catch (JsonProcessingException e) {
			LOG.traceError("error", e);
		}finally {
			Util.pasarGarbageCollector();
		}
		LOG.endTrace("error", Constantes.VACIO, "Respondiendo = " + json);
		return resp;
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}
}
