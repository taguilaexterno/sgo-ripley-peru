package com.ripley.restservices.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.ripley.restservices.logic.ISunatService;
import com.ripley.restservices.model.generic.GenericResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.aligare.ara.util.AligareException;
import cl.ripley.omnicanalidad.bean.Parametro;
import cl.ripley.omnicanalidad.bean.Parametros;
import cl.ripley.omnicanalidad.dao.ParametrosDAO;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.FechaUtil;
import cl.ripley.omnicanalidad.util.Util;

/**Controller que se encarga de obtener datos para insertar en la tabla CV_CODIGOS_SUNAT.
 *
 * @author Martin Corrales (Aligare).
 * @since 09-01-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/sunat")
public class SunatController {

	private static final AriLog LOG = new AriLog(SunatController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private ISunatService regulatorioSunatService;
	
	@Autowired
	private ParametrosDAO paramDAO;

    @PostMapping("/uploadFile")
    public GenericResponse fileUpload(@RequestParam("fileName") MultipartFile multipartFile) {
    	LOG.initTrace("uploadFile", "Inicio");
    	GenericResponse resp = new GenericResponse();
        if (multipartFile.isEmpty()) {
        	LOG.traceInfo("uploadFile", "El archivo esta vacio");
        	resp.setCodigo(Constantes.NRO_MENOSUNO);
    		resp.setMensaje("El archivo esta vacio");
        }else {
        	LOG.traceInfo("uploadFile", "El archivo tiene datos");
        	try {
        		resp = regulatorioSunatService.uploadSunatFile(multipartFile.getInputStream());
    		} catch (IOException e) {
    			LOG.traceInfo("uploadFile", "I/O Error: " + e.getMessage());
    			resp.setCodigo(Constantes.NRO_MENOSUNO);
        		resp.setMensaje("I/O Error: " + e.getMessage());
    		} catch (Exception e) {
    			LOG.traceInfo("uploadFile", "Exception:" + e.getMessage());
    			resp.setCodigo(Constantes.NRO_MENOSUNO);
        		resp.setMensaje("Exception:" + e.getMessage());
    		}finally {
    			Util.pasarGarbageCollector();
    		}
        }
        LOG.endTrace("uploadFile", "resp = ", resp.getMensaje());
        return resp;
    }

    @PostMapping("/uploadFileWeb")
	private GenericResponse uploadFileWeb(@RequestParam(value = "url", required = false) String urlFile) {
    	LOG.initTrace("uploadFileWeb", "Inicio");
    	GenericResponse resp = new GenericResponse();
		try {
			String urlFileSunat = null;
			if(urlFile != null && Constantes.NRO_CERO < urlFile.length()) {
				urlFileSunat = urlFile;
			}else {
				try {
					Parametros parametros = new Parametros();
					parametros.setParametros(new ArrayList<Parametro>());
					paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), Constantes.STRING_UNO);
					urlFileSunat = parametros.buscaParametroPorNombre("URL_FILE_SUNAT").getValor();
				}catch (Exception e) {
					LOG.traceInfo("uploadFileWeb", "Paramentro URL_FILE_SUNAT no existe");
				}
			}
			LOG.traceInfo("uploadFileWeb", "urlFileSunat:" + urlFileSunat);
			if(urlFileSunat != null) {
				String yearMonthDay = FechaUtil.getCurrentDateWithFormat(Constantes.FECHA_YYMMDD);
				urlFileSunat = urlFileSunat.replace(Constantes.FECHA_YYMMDD, yearMonthDay);
				URL url = new URL(urlFileSunat);
				resp = regulatorioSunatService.uploadSunatFile(url.openStream());
			} else {
				resp.setCodigo(Constantes.NRO_MENOSUNO);
	    		resp.setMensaje("urlFileSunat es igual a null");
			}
		} catch (MalformedURLException e) {
			LOG.traceInfo("uploadFileWeb", "Malformed URL: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
    		resp.setMensaje("Malformed URL: " + e.getMessage());
		} catch (IOException e) {
			LOG.traceInfo("uploadFileWeb", "I/O Error: " + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
    		resp.setMensaje("I/O Error: " + e.getMessage());
		} catch (Exception e) {
			LOG.traceInfo("uploadFileWeb", "Exception:" + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
    		resp.setMensaje("Exception:" + e.getMessage());
		}finally {
			Util.pasarGarbageCollector();
		}
        LOG.endTrace("uploadFileWeb", "resp = ", resp.getMensaje());
        return resp;
	}
    
    @PostMapping("/uploadFileWebAws")
	private GenericResponse uploadFileWebAws(@RequestParam(value = "fileName", required = false) String fileName) {
    	LOG.initTrace("uploadFileWebAws", "Inicio");
    	GenericResponse resp = new GenericResponse();
		try {
			String accessKey = null;
			String secretKey = null;
			String bucketName = null;
			String key = null;
			String region = null;
			try {
				Parametros parametros = new Parametros();
				parametros.setParametros(new ArrayList<Parametro>());
				paramDAO.getParametrosCajaVirtual(Constantes.FUNCION_BOLETA, parametros.getParametros(), Constantes.STRING_UNO);
				
				accessKey = parametros.buscaParametroPorNombre("ACCESS_KEY_SUNAT").getValor();
				secretKey = parametros.buscaParametroPorNombre("SECRET_KEY_SUNAT").getValor();
				bucketName = parametros.buscaParametroPorNombre("BUCKET_NAME_SUNAT").getValor();
				region = parametros.buscaParametroPorNombre("REGION_SUNAT").getValor();
				if(fileName != null && Constantes.NRO_CERO < fileName.length()) {
					key = fileName;
				}else {
					key = parametros.buscaParametroPorNombre("KEY_SUNAT").getValor();
					String yearMonthDay = FechaUtil.getCurrentDateWithFormat(Constantes.FECHA_YYMMDD);
					key = key.replace(Constantes.FECHA_YYMMDD, yearMonthDay);
				}
				
			}catch (Exception e) {
				LOG.traceInfo("uploadFileWebAws", "Alguno de estos parametros KEY_SUNAT, BUCKET_NAME_SUNAT, SECRET_KEY_SUNAT, ACCESS_KEY_SUNAT, REGION_SUNAT no exiten");
				throw new AligareException("Alguno de estos parametros KEY_SUNAT, BUCKET_NAME_SUNAT, SECRET_KEY_SUNAT, ACCESS_KEY_SUNAT, REGION_SUNAT no exiten");
			}
			
			LOG.traceInfo("uploadFileWebAws", "accessKey:" + (accessKey!=null) + ", secretKey:" + (secretKey!=null) +", bucketName:" + (bucketName!=null) + ", key:" + key + ", region:" + region);
			if(accessKey !=null && secretKey !=null && bucketName!=null && key!=null) {
				BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey); 
				
				AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
						.withRegion(Util.getRegionAws(region))
						.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
				S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, key));
				
				resp = regulatorioSunatService.uploadSunatFile(object.getObjectContent());
				object.close();
			} else {
				LOG.traceInfo("uploadFileWebAws", "Falta uno o mas parametros");
				resp.setCodigo(Constantes.NRO_MENOSUNO);
	    		resp.setMensaje("Falta uno o mas parametros");
			}
		} catch (AligareException e) {
			
			LOG.traceInfo("uploadFileWebAws", "AligareException:" + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
    		resp.setMensaje("Exception:" + e.getMessage());
    		
		} catch(AmazonServiceException e) {
			
			LOG.traceInfo("uploadFileWebAws", "AmazonServiceException:" + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("AmazonServiceException:" + e.getMessage());
			
	    } catch(SdkClientException e) {
	    	
			LOG.traceInfo("uploadFileWebAws", "SdkClientException:" + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("SdkClientException:" + e.getMessage());
			
	    } catch (Exception e) {
	    	
			LOG.traceInfo("uploadFileWebAws", "Exception:" + e.getMessage());
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje("Exception:" + e.getMessage());
			
	    }finally {
			Util.pasarGarbageCollector();
		} 
        LOG.endTrace("uploadFileWebAws", "resp = ", resp.getMensaje());
        return resp;
	}
}
