package com.ripley.restservices.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.dao.impl.TestDAO;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.impresion.InsertaPagoTiendaRequest;
import com.ripley.restservices.model.rpos.RposResponse;
import com.ripley.restservices.model.testparametros.TestParametrosResponse;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.dao.CajaVirtualDAO;
import cl.ripley.omnicanalidad.dao.GeneracionDTEDAO;
import cl.ripley.omnicanalidad.logic.boleta.EsclavoBoleta;
import cl.ripley.omnicanalidad.logic.insertaModeloExtendido.IInsertaModeloExtendidoService;
import cl.ripley.omnicanalidad.util.Constantes;

/**Controller que se encarga de obtener datos para TEST.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 26-03-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/test")
public class TestController {

	private static final AriLog LOG = new AriLog(TestController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);
	
	@Autowired
	private TestDAO testDAO;
	
	@Autowired
	private EsclavoBoleta esclavoBoleta;
	
	@Autowired
	private GeneracionDTEDAO generacionDTE;
	
	@Autowired
	private IInsertaModeloExtendidoService insertaModeloExtendidoService;
	
	@Autowired
	private CajaVirtualDAO caja;

	@GetMapping("/getParametros")
	public GenericResponse getParametros() {
		LOG.initTrace("getParametros", "Inicio");
		
		TestParametrosResponse resp = new TestParametrosResponse();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje(Constantes.OK);
		
		try {
			resp.setParametros(testDAO.getParametros());
			
		} catch (Exception e) {
			LOG.traceError("getParametros", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}
		
		LOG.endTrace("getParametros", "resp = " + resp, resp.toString());
		return resp;
		
	}
	
	@GetMapping("/testPAblo")
	public GenericResponse testPAblo() {
		LOG.initTrace("getParametros", "Inicio");
		
		TestParametrosResponse resp = new TestParametrosResponse();
		resp.setCodigo(Constantes.NRO_CERO);
		resp.setMensaje(Constantes.OK);
		
		try {
			esclavoBoleta.cambiaEstadoCajaSuc(13272100L, 2, 505, 25);
			
		} catch (Exception e) {
			LOG.traceError("getParametros", e);
			resp.setCodigo(Constantes.NRO_MENOSUNO);
			resp.setMensaje(e.getMessage());
		}
		
		LOG.endTrace("getParametros", "resp = " + resp, resp.toString());
		return resp;
		
	}
	
}
