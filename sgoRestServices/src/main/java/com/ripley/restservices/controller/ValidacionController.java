package com.ripley.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ripley.restservices.model.generic.GenericRequest;
import com.ripley.restservices.model.generic.GenericResponse;
import com.ripley.restservices.model.validacion.ConsultaOcValidacionRequest;

import cl.aligare.ara.api.AriLog;
import cl.aligare.ara.api.KeyLog;
import cl.aligare.ara.api.PlataformaType;
import cl.ripley.omnicanalidad.logic.validacion.EsclavoValidacion;
import cl.ripley.omnicanalidad.util.Constantes;
import cl.ripley.omnicanalidad.util.Util;



/**Controlador del servicio REST de validación de orden de compra.
 *
 * @author Jose Matias Ortuzar (Aligare).
 * @since 21-02-2018
 *<br/><br/>
 * Cambios:<br/>
 * <ul>
 * <li>Inicio</li>
 * </ul>
 */
@RestController
@RequestMapping("/validaciones")
public class ValidacionController {

	private static final AriLog LOG = new AriLog(ValidacionController.class, Constantes.CODIGO_APLICACION, PlataformaType.JAVA);

	@Autowired
	private EsclavoValidacion esclavoValicacion;
	

	/**Realiza la validación de la orden de compra
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-02-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param oc número de la orden de compra
	 * @return {@linkplain GenericResponse}
	 */
	@RequestMapping(value = "/validacionNotaVentaSinCaja/{oc}", method = RequestMethod.GET)
	public GenericResponse validar (@PathVariable("oc") Long oc) {


		GenericResponse resp = new GenericResponse();
		resp = esclavoValicacion.ValidacionNotaVentaSinCaja(oc);
		Util.pasarGarbageCollector();
		return resp;

	}

	@RequestMapping(value = "/validacionNotaVentaReglas/{oc}", method = RequestMethod.GET)
	public GenericResponse validarReglas (@PathVariable("oc") Long oc) {
		GenericResponse resp = new GenericResponse();
		resp = esclavoValicacion.ValidacionNotaVentaReglas(oc);
		Util.pasarGarbageCollector();
		return resp;
	}

	/**Obtiene lista de OCs por número de caja. La lista tendrá como largo el valor indicado por los parámetros.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/consultaOCs", method = RequestMethod.POST)
	public GenericResponse consultaOCs (@RequestBody ConsultaOcValidacionRequest req) {
		LOG.initTrace("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
						+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
						+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
						new KeyLog("Estado", String.valueOf(req.getEstado())),
						new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

//		LOG.traceInfo("consultaOCs", "oc = " + String.valueOf(req.getOc()) 
//		+ " - numCaja = " + String.valueOf(req.getNumCaja()) 
//		+ " - cantidadOCs = " + String.valueOf(req.getCantidadOCs()), 
//		new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
//		new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())),
//		new KeyLog("Estado", String.valueOf(req.getEstado())),
//		new KeyLog("Cantidad de OCs", String.valueOf(req.getCantidadOCs())));

		GenericResponse resp = esclavoValicacion.obtenerOCsByEstadoYCaja(req.getNumCaja(), req.getCantidadOCs(), req.getEstado());
		LOG.endTrace("consultaOCs", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	/**Obtiene lista de cajas de SGO.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/consultaCajas", method = RequestMethod.GET)
	public GenericResponse consultaCajas () {
		LOG.initTrace("consultaCajas", "Inicia método");
		LOG.traceInfo("consultaCajas", "Consulto cajas SGO");
		GenericResponse resp = esclavoValicacion.obtenerCajas();
		LOG.endTrace("consultaCajas", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}
	
	/**Obtiene lista de OCs por número de caja. La lista tendrá como largo el valor indicado por los parámetros.
	 *
	 * @author Jose Matias Ortuzar (Aligare).
	 * @since 21-03-2018
	 *<br/><br/>
	 * Cambios:<br/>
	 * <ul>
	 * <li>Inicio</li>
	 * </ul>
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/asignarCaja", method = RequestMethod.POST)
	public GenericResponse asignarCaja (@RequestBody GenericRequest req) {
		LOG.initTrace("asignarCaja", "oc = " + String.valueOf(req.getOc()) 
						+ " - numCaja = " + String.valueOf(req.getNumCaja()),
						new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
						new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));

		LOG.traceInfo("asignarCaja", "oc = " + String.valueOf(req.getOc()) 
		+ " - numCaja = " + String.valueOf(req.getNumCaja()),
		new KeyLog("Orden de Compra", String.valueOf(req.getOc())),
		new KeyLog("Numero de Caja", String.valueOf(req.getNumCaja())));
		GenericResponse resp = esclavoValicacion.asignarCaja(req.getNumCaja(), req.getOc());
		LOG.endTrace("consultaOCs", "resp = " + resp, resp.toString());
		Util.pasarGarbageCollector();
		return resp;
	}


}
